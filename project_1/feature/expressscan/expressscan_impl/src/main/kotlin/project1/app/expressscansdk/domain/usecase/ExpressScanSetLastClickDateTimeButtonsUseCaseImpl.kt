package project1.app.expressscansdk.domain.usecase

import org.threeten.bp.LocalDateTime
import project1.app.expressscan.api.domain.ExpressScanButtonsRepository
import project1.app.expressscan.api.domain.usecase.ExpressScanSetLastClickDateTimeButtonsUseCase

class ExpressScanSetLastClickDateTimeButtonsUseCaseImpl(
    private val expressScanSdkRepository: ExpressScanButtonsRepository
) : ExpressScanSetLastClickDateTimeButtonsUseCase {
    override suspend operator fun invoke() {
        val date = LocalDateTime.now()
        expressScanSdkRepository.setLastClickDateTimeButtonsExpressScan(date.toString())
    }
}