package project1.app.expressscansdk

import project1.app.base.ui.util.parcelableParametersBundleOf
import project1.app.expressscansdk.presentation.ExpressScanHolderFragment
import project1.app.expressscansdk.presentation.ExpressScanHolderParameters
import ru.terrakok.cicerone.android.support.FragmentParams
import ru.terrakok.cicerone.android.support.SupportAppScreen

class ExpressScanScreens {

    class ExpressScanHolder(
        private val parameters: ExpressScanHolderParameters
    ) : SupportAppScreen() {
        override fun getFragment() = ExpressScanHolderFragment()
        override fun getFragmentParams() = FragmentParams(
            ExpressScanHolderFragment::class.java,
            parcelableParametersBundleOf(parameters)
        )
    }
}