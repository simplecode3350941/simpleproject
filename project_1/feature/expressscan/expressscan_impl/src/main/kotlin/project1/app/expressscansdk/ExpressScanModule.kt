package project1.app.expressscansdk

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.bind
import org.koin.dsl.module
import project1.app.base.segmentation.base.domain.SegmentationToggleProvider
import project1.app.expressscan.api.domain.ExpressScanButtonsRepository
import project1.app.expressscan.api.domain.ExpressScanRepository
import project1.app.expressscan.api.domain.ExpressScanSdkAnalyticsInteractor
import project1.app.expressscan.api.domain.ExpressScanSegmentationRepository
import project1.app.expressscan.api.domain.usecase.ExpressScanButtonsIsClickedUseCase
import project1.app.expressscan.api.domain.usecase.ExpressScanButtonsSetClickedUseCase
import project1.app.expressscan.api.domain.usecase.ExpressScanClearRepositoryUseCase
import project1.app.expressscan.api.domain.usecase.ExpressScanGetBasketStatusAsFlowUseCase
import project1.app.expressscan.api.domain.usecase.ExpressScanGetCurrentTimeMinusFiveHoursUseCase
import project1.app.expressscan.api.domain.usecase.ExpressScanGetHostInstanceUseCase
import project1.app.expressscan.api.domain.usecase.ExpressScanGetLastClickDateTimeButtonsUseCase
import project1.app.expressscan.api.domain.usecase.ExpressScanGetQrCodeStatusAsFlowUseCase
import project1.app.expressscan.api.domain.usecase.ExpressScanInitIfEnabledUseCase
import project1.app.expressscan.api.domain.usecase.ExpressScanIsEnabledUseCase
import project1.app.expressscan.api.domain.usecase.ExpressScanSendStatusesRequestsUseCase
import project1.app.expressscan.api.domain.usecase.ExpressScanSetLastClickDateTimeButtonsUseCase
import project1.app.expressscan.api.domain.usecase.ExpressScanSetUserAuthStateUseCase
import project1.app.expressscansdk.data.ExpressScanButtonsRepositoryImpl
import project1.app.expressscansdk.data.ExpressScanRepositoryImpl
import project1.app.expressscansdk.data.ExpressScanSegmentationRepositoryImpl
import project1.app.expressscansdk.domain.ExpressScanSdkAnalyticsInteractorImpl
import project1.app.expressscansdk.domain.usecase.ExpressScanButtonsIsClickedUseCaseImpl
import project1.app.expressscansdk.domain.usecase.ExpressScanButtonsSetClickedUseCaseImpl
import project1.app.expressscansdk.domain.usecase.ExpressScanClearRepositoryUseCaseImpl
import project1.app.expressscansdk.domain.usecase.ExpressScanGetBasketStatusAsFlowUseCaseImpl
import project1.app.expressscansdk.domain.usecase.ExpressScanGetCurrentTimeMinusFiveHoursUseCaseImpl
import project1.app.expressscansdk.domain.usecase.ExpressScanGetHostInstanceUseCaseImpl
import project1.app.expressscansdk.domain.usecase.ExpressScanGetLastClickDateTimeButtonsUseCaseImpl
import project1.app.expressscansdk.domain.usecase.ExpressScanGetQrCodeStatusAsFlowUseCaseImpl
import project1.app.expressscansdk.domain.usecase.ExpressScanInitIfEnabledUseCaseImpl
import project1.app.expressscansdk.domain.usecase.ExpressScanIsEnabledUseCaseImpl
import project1.app.expressscansdk.domain.usecase.ExpressScanSendStatusesRequestsUseCaseImpl
import project1.app.expressscansdk.domain.usecase.ExpressScanSetLastClickDateTimeButtonsUseCaseImpl
import project1.app.expressscansdk.domain.usecase.ExpressScanSetUserAuthStateUseCaseImpl
import project1.app.expressscansdk.presentation.ExpressScanHolderParameters
import project1.app.expressscansdk.presentation.ExpressScanHolderViewModel
import ru.x5.expressscan.sdk.core.listener.part.ExpressScanBasketListener
import ru.x5.expressscan.sdk.core.listener.part.ExpressScanTokenListener

@Suppress("LongMethod")
fun expressScanModule() = module {
    single<ExpressScanRepository> {
        ExpressScanRepositoryImpl(
            refreshToken = get(),
            context = get(),
            settingsRepository = get(),
            expressScanAuthProvider = get(),
            mapKit = get(),
            appDispatchers = get()
        )
    } bind ExpressScanTokenListener::class bind ExpressScanBasketListener::class

    factory<ExpressScanSegmentationRepository> {
        ExpressScanSegmentationRepositoryImpl()
    } bind SegmentationToggleProvider::class
    factory<ExpressScanButtonsRepository> {
        ExpressScanButtonsRepositoryImpl(
            sharedPreferences = get(),
            appDispatchers = get()
        )
    }
    factory<ExpressScanSdkAnalyticsInteractor> {
        ExpressScanSdkAnalyticsInteractorImpl(analyticsInteractor = get())
    }
    factory<ExpressScanSendStatusesRequestsUseCase> {
        ExpressScanSendStatusesRequestsUseCaseImpl(expressScanRepository = get())
    }
    factory<ExpressScanGetBasketStatusAsFlowUseCase> {
        ExpressScanGetBasketStatusAsFlowUseCaseImpl(expressScanRepository = get())
    }
    factory<ExpressScanGetQrCodeStatusAsFlowUseCase> {
        ExpressScanGetQrCodeStatusAsFlowUseCaseImpl(expressScanRepository = get())
    }
    factory<ExpressScanInitIfEnabledUseCase> {
        ExpressScanInitIfEnabledUseCaseImpl(expressScanRepository = get(), isExpressScanEnabled = get())
    }
    factory<ExpressScanGetHostInstanceUseCase> {
        ExpressScanGetHostInstanceUseCaseImpl(expressScanRepository = get())
    }
    factory<ExpressScanIsEnabledUseCase> {
        ExpressScanIsEnabledUseCaseImpl(getToggleValueUseCase = get(), expressScanSegmentationRepository = get())
    }
    factory<ExpressScanSetUserAuthStateUseCase> {
        ExpressScanSetUserAuthStateUseCaseImpl(expressScanRepository = get())
    }
    factory<ExpressScanClearRepositoryUseCase> {
        ExpressScanClearRepositoryUseCaseImpl(expressScanRepository = get())
    }
    factory<ExpressScanGetLastClickDateTimeButtonsUseCase> {
        ExpressScanGetLastClickDateTimeButtonsUseCaseImpl(expressScanSdkRepository = get())
    }
    factory<ExpressScanButtonsIsClickedUseCase> {
        ExpressScanButtonsIsClickedUseCaseImpl(expressScanSdkRepository = get())
    }
    factory<ExpressScanButtonsSetClickedUseCase> {
        ExpressScanButtonsSetClickedUseCaseImpl(expressScanSdkRepository = get())
    }
    factory<ExpressScanSetLastClickDateTimeButtonsUseCase> {
        ExpressScanSetLastClickDateTimeButtonsUseCaseImpl(expressScanSdkRepository = get())
    }
    factory<ExpressScanGetCurrentTimeMinusFiveHoursUseCase> {
        ExpressScanGetCurrentTimeMinusFiveHoursUseCaseImpl()
    }

    viewModel { (parameters: ExpressScanHolderParameters) ->
        ExpressScanHolderViewModel(
            navigator = get(),
            parameters = parameters,
            getHostInstance = get(),
            analyticsInteractor = get()
        )
    }
}