package project1.app.expressscansdk.data

import android.content.Context
import com.yandex.mapkit.MapKit
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import project1.app.base.coroutines.AppDispatchers
import project1.app.base.coroutines.launchSafe
import project1.app.base.settings.domain.SettingsRepository
import project1.app.expressscan.api.domain.ExpressScanAuthProvider
import project1.app.expressscan.api.domain.ExpressScanRepository
import project1.app.expressscan.api.domain.model.ExpressScanBasketStatusModel
import project1.app.expressscan.api.domain.model.ExpressScanQrCodeStatusModel
import project1.app.expressscan.api.navigator.ExpressScanScreenType
import project1.app.x5id.authorization.domain.RefreshTokenUseCase
import ru.x5.expressscan.domain.app.InitialScreen
import ru.x5.expressscan.domain.basket.model.BasketStatus
import ru.x5.expressscan.domain.basket.model.ExitQrCodeStatus
import ru.x5.expressscan.sdk.SourceApplication
import ru.x5.expressscan.sdk.base.domain.environment.ExpressScanEnvironment
import ru.x5.expressscan.sdk.core.ExpressScanCore
import ru.x5.expressscan.sdk.core.listener.ExpressScanCoreListener
import ru.x5.expressscan.sdk.host.entry.ExpressScanHost
import ru.x5.expressscan.sdk.host.listener.ExpressScanHostListener
import ru.x5.expressscan.sdk.host.listener.part.ExpressScanBehaviorListener

class ExpressScanRepositoryImpl(
    private var refreshToken: RefreshTokenUseCase,
    context: Context,
    settingsRepository: SettingsRepository,
    private val expressScanAuthProvider: ExpressScanAuthProvider,
    private val mapKit: MapKit,
    appDispatchers: AppDispatchers
) : ExpressScanRepository {

    override val updateTokenPeriod = EXPRESS_SCAN_UPDATE_TOKEN_PERIOD

    private val expressScanEnvironment = if (!settingsRepository.isProdServer) {
        ExpressScanEnvironment.STAGE
    } else {
        ExpressScanEnvironment.PROD
    }

    private var expressScanCore: ExpressScanCore? =
        ExpressScanCore.initialize(
            context,
            SourceApplication.TC5,
            expressScanEnvironment,
            !settingsRepository.isProdServer
        )

    private val viewScope = CoroutineScope(appDispatchers.storage)

    private val accessToken: String
        get() = expressScanAuthProvider.getTokenSync() ?: ""

    private val expressScanBasketStatusFlow = MutableStateFlow<ExpressScanBasketStatusModel?>(null)

    private val expressScanQrCodeStatusFlow = MutableStateFlow<ExpressScanQrCodeStatusModel?>(null)

    override fun clearRepository() {
        expressScanCore = null
        viewScope.coroutineContext.cancel()
    }

    override fun initSdk() {
        expressScanCore?.setListener(
            object : ExpressScanCoreListener(this, this) {}
        )
        setUserAuthState(true)
    }

    override fun getHostInstance(
        context: Context,
        behaviorListener: ExpressScanBehaviorListener,
        screenType: ExpressScanScreenType
    ): ExpressScanHost? {
        val screenType = when (screenType) {
            ExpressScanScreenType.DEFAULT -> { InitialScreen.Default }
            ExpressScanScreenType.BASKET -> { InitialScreen.Basket }
            ExpressScanScreenType.EXIT_QR -> { InitialScreen.ExitQr }
        }
        return expressScanCore?.getHostInstance(
            context = context,
            hostListener = ExpressScanHostListener(behaviorListener),
            mapKit = mapKit,
            initialScreen = screenType
        )
    }

    override fun setUserAuthState(isAuthorized: Boolean) {
        expressScanCore?.getCoreInteractor()?.setIsUserAuthorized(isAuthorized)
    }

    override fun isTokenUpdatePossible(): Boolean {
        return true
    }

    override fun provideToken(): String {
        return accessToken
    }

    override fun updateToken() {
        viewScope.launchSafe {
            refreshToken()
            expressScanCore?.getCoreInteractor()?.setTokenUpdated()
        }
    }

    override fun onBasketStatusResult(status: BasketStatus) {
        basketStatusResult(status)
    }

    override fun onExitQrCodeStatusResult(qrCodeStatus: ExitQrCodeStatus) {
        exitQrCodeStatusResult(qrCodeStatus)
    }

    override fun sendExpressScanStatusesRequests() {
        expressScanCore?.getCoreInteractor()?.requestBasketStatus()
        expressScanCore?.getCoreInteractor()?.requestExitQrCodeStatus()
    }

    override fun getExpressScanBasketStatusLiveDataAsFlow(): Flow<ExpressScanBasketStatusModel?> =
        expressScanBasketStatusFlow

    override fun getExpressScanQrCodeStatusLiveDataAsFlow(): Flow<ExpressScanQrCodeStatusModel?> =
        expressScanQrCodeStatusFlow

    private fun basketStatusResult(status: BasketStatus) {
        when (status) {
            is BasketStatus.BasketNotExist -> {
                expressScanBasketStatusFlow.value = ExpressScanBasketStatusModel(false, null)
            }
            is BasketStatus.BasketIsOpen -> {
                expressScanBasketStatusFlow.value = ExpressScanBasketStatusModel(true, status.totalPrice)
            }
        }
    }

    private fun exitQrCodeStatusResult(qrCodeStatus: ExitQrCodeStatus) {
        when (qrCodeStatus) {
            is ExitQrCodeStatus.QrCodeNotExist -> {
                expressScanQrCodeStatusFlow.value = ExpressScanQrCodeStatusModel(false)
            }
            is ExitQrCodeStatus.ActiveQrCode -> {
                expressScanQrCodeStatusFlow.value = ExpressScanQrCodeStatusModel(true)
            }
        }
    }

    companion object {
        private const val EXPRESS_SCAN_UPDATE_TOKEN_PERIOD = 15
    }
}