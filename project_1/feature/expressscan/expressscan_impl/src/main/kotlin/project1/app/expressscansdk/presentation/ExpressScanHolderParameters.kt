package project1.app.expressscansdk.presentation

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import project1.app.expressscan.api.navigator.ExpressScanScreenType

@Parcelize
data class ExpressScanHolderParameters(
    val screenType: ExpressScanScreenType = ExpressScanScreenType.DEFAULT
) : Parcelable