package project1.app.expressscansdk.domain.usecase

import project1.app.expressscan.api.domain.ExpressScanButtonsRepository
import project1.app.expressscan.api.domain.usecase.ExpressScanButtonsSetClickedUseCase

class ExpressScanButtonsSetClickedUseCaseImpl(
    private val expressScanSdkRepository: ExpressScanButtonsRepository
) : ExpressScanButtonsSetClickedUseCase {
    override suspend operator fun invoke(isButtonsClickedValue: Boolean) {
        expressScanSdkRepository.setExpressScanButtonsClicked(isButtonsClickedValue)
    }
}