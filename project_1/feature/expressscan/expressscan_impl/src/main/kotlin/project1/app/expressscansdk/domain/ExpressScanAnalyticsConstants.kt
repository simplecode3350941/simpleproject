package project1.app.expressscansdk.domain

object ExpressScanAnalyticsConstants {
    const val EXPRESS_SCAN_MAIN_EVENT_NAME = "Main"
    const val EXPRESS_SCAN_MAIN = "Main_ExpressScan"

    const val EXPRESS_SCAN_EVENT_NAME = "ExpressScan"
    const val EXPRESS_SCAN_SHOP_CONTINUE_SHOW = "ExpressScan_ShopContinue_Show"
    const val EXPRESS_SCAN_SHOP_CONTINUE = "ExpressScan_ShopContinue"
    const val EXPRESS_SCAN_QR_EXIT_SHOW = "ExpressScan_QRExit_Show"
    const val EXPRESS_SCAN_QR_EXIT = "ExpressScan_QRExit"
    const val EXPRESS_SCAN_BACK = "ExpressScan_Back"
}