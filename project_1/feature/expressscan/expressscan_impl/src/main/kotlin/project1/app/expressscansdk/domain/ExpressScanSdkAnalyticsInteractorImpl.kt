package project1.app.expressscansdk.domain

import project1.app.base.analytics.domain.AnalyticsInteractor
import project1.app.expressscan.api.domain.ExpressScanSdkAnalyticsInteractor

class ExpressScanSdkAnalyticsInteractorImpl(
    analyticsInteractor: AnalyticsInteractor
) : ExpressScanSdkAnalyticsInteractor, AnalyticsInteractor by analyticsInteractor {

    override fun trackExpressScanMain() {
        trackEvent(
            ExpressScanAnalyticsConstants.EXPRESS_SCAN_MAIN_EVENT_NAME,
            ExpressScanAnalyticsConstants.EXPRESS_SCAN_MAIN
        )
    }

    override fun trackExpressScanShopContinueShow() {
        trackExpressScanEvent(ExpressScanAnalyticsConstants.EXPRESS_SCAN_SHOP_CONTINUE_SHOW)
    }

    override fun trackExpressScanShopContinue() {
        trackExpressScanEvent(ExpressScanAnalyticsConstants.EXPRESS_SCAN_SHOP_CONTINUE)
    }

    override fun trackExpressScanQrExitShow() {
        trackExpressScanEvent(ExpressScanAnalyticsConstants.EXPRESS_SCAN_QR_EXIT_SHOW)
    }

    override fun trackExpressScanQrExit() {
        trackExpressScanEvent(ExpressScanAnalyticsConstants.EXPRESS_SCAN_QR_EXIT)
    }

    override fun trackExpressScanBack() {
        trackExpressScanEvent(ExpressScanAnalyticsConstants.EXPRESS_SCAN_BACK)
    }

    private fun trackExpressScanEvent(action: String) {
        trackEvent(ExpressScanAnalyticsConstants.EXPRESS_SCAN_EVENT_NAME, action)
    }
}