package project1.app.expressscansdk.presentation

import android.content.Intent
import android.os.Bundle
import android.view.View
import org.koin.androidx.viewmodel.ext.android.viewModel
import project1.app.base.ui.extension.addSystemTopPadding
import project1.app.base.ui.extension.parcelableParametersOf
import project1.app.base.ui.presentation.BaseFragment
import project1.app.base.ui.util.StatusBarColor
import project1.app.express_scan.R
import ru.x5.expressscan.sdk.host.entry.ExpressScanHost
import ru.x5.expressscan.sdk.host.feature.ExpressScanFragment
import ru.x5.expressscan.sdk.host.listener.part.ExpressScanBehaviorListener

class ExpressScanHolderFragment : BaseFragment(),
    ExpressScanBehaviorListener {
    override val statusBarColor = StatusBarColor.WHITE
    override val layoutResId = R.layout.express_scan_holder_fragment
    override val viewModel by viewModel<ExpressScanHolderViewModel> {
        parcelableParametersOf<ExpressScanHolderParameters>()
    }

    private var expressScanFragment: ExpressScanFragment? = null
    private var expressScanHost: ExpressScanHost? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.addSystemTopPadding()

        expressScanHost = viewModel.getExpressScanHostInstance(requireContext().applicationContext, this)

        expressScanHost?.getHostInteractor()?.clearDatabase()

        expressScanFragment = expressScanHost?.getHostFragment()

        expressScanHost?.let { host ->
            expressScanFragment = host.getHostFragment()

            expressScanFragment?.let { fragment ->
                childFragmentManager
                    .beginTransaction()
                    .replace(R.id.expressScanContainer, fragment)
                    .commit()
            }
        }
    }

    override fun onAuthRequired() {
        viewModel.onX5AuthRequired()
    }

    override fun onSoftBackPressed() {
        viewModel.onCloseExpressScan()
    }

    override fun onUserBlocked() {
        // Nothing for now
    }

    override fun onReceiptsRequired() {
        viewModel.onOpenLatestTransaction()
    }

    override fun onBackPressed(): Boolean {
        expressScanHost?.let { host ->
            if (!host.getHostInteractor().onBackPressed())
                viewModel.onCloseExpressScan()
        }
        return true
    }

    override fun onDestroyView() {
        expressScanHost = null
        expressScanFragment = null
        super.onDestroyView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        expressScanHost?.getHostInteractor()?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onPause() {
        expressScanHost?.getHostInteractor()?.onPause(requireActivity())
        super.onPause()
    }

    override fun onResume() {
        expressScanHost?.getHostInteractor()?.onResume(requireActivity())
        super.onResume()
    }
}