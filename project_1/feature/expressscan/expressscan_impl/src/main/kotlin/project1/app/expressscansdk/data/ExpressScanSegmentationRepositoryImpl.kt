package project1.app.expressscansdk.data

import project1.app.base.segmentation.base.domain.model.SegmentationToggle
import project1.app.expressscan.api.domain.ExpressScanSegmentationRepository

class ExpressScanSegmentationRepositoryImpl : ExpressScanSegmentationRepository {

    override fun getExpressScanToggle() = SegmentationToggle(
        code = SEGMENTATION_TOGGLE_EXPRESS_SCAN,
        name = SEGMENTATION_TOGGLE_EXPRESS_SCAN_NAME
    )

    override suspend fun getToggles(): List<SegmentationToggle> = listOf(getExpressScanToggle())

    companion object {
        private const val SEGMENTATION_TOGGLE_EXPRESS_SCAN = "express_scan"
        private const val SEGMENTATION_TOGGLE_EXPRESS_SCAN_NAME = "express_scan"
    }
}