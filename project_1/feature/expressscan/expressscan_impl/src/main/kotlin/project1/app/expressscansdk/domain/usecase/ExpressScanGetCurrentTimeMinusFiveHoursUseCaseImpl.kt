package project1.app.expressscansdk.domain.usecase

import org.threeten.bp.LocalDateTime
import project1.app.expressscan.api.domain.usecase.ExpressScanGetCurrentTimeMinusFiveHoursUseCase

class ExpressScanGetCurrentTimeMinusFiveHoursUseCaseImpl : ExpressScanGetCurrentTimeMinusFiveHoursUseCase {
    override operator fun invoke(): LocalDateTime {
        return LocalDateTime.now().minusHours(HOURS_BACK.toLong())
    }

    private companion object {
        private const val HOURS_BACK = 5
    }
}