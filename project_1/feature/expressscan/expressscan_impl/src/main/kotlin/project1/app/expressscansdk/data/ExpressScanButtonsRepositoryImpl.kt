package project1.app.expressscansdk.data

import kotlinx.coroutines.withContext
import project1.app.base.coroutines.AppDispatchers
import project1.app.base.data.preferences.Preferences
import project1.app.expressscan.api.domain.ExpressScanButtonsRepository

class ExpressScanButtonsRepositoryImpl(
    private val sharedPreferences: Preferences,
    private val appDispatchers: AppDispatchers
) : ExpressScanButtonsRepository {

    override suspend fun isButtonsExpressScanClicked(): Boolean {
        return withContext(appDispatchers.storage) {
            sharedPreferences[IS_BUTTONS_EXPRESS_SCAN__CLICKED, Boolean::class] ?: false
        }
    }

    override suspend fun getLastClickDateTimeButtonsExpressScan(): String {
        return withContext(appDispatchers.storage) {
            sharedPreferences[LAST_CLICKED_DATE_TIME_BUTTONS_EXPRESS_SCAN, String::class] ?: ""
        }
    }

    override suspend fun setExpressScanButtonsClicked(isButtonsClickedValue: Boolean) {
        withContext(appDispatchers.storage) {
            sharedPreferences[IS_BUTTONS_EXPRESS_SCAN__CLICKED] = isButtonsClickedValue
        }
    }

    override suspend fun setLastClickDateTimeButtonsExpressScan(lastClickDateTimeButtonsExpressScan: String) {
        withContext(appDispatchers.storage) {
            sharedPreferences[LAST_CLICKED_DATE_TIME_BUTTONS_EXPRESS_SCAN] = lastClickDateTimeButtonsExpressScan
        }
    }

    private companion object {
        private const val IS_BUTTONS_EXPRESS_SCAN__CLICKED = "is_buttons_express_scan_clicked"
        private const val LAST_CLICKED_DATE_TIME_BUTTONS_EXPRESS_SCAN = "last_click_date_time_buttons_express_scan"
    }
}