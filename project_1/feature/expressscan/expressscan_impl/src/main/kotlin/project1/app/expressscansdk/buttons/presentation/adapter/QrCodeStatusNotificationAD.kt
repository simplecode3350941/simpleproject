package project1.app.expressscansdk.buttons.presentation.adapter

import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import project1.app.express_scan.databinding.ExpressQrFinishItemBinding
import project1.app.expressscan.api.domain.model.ExpressScanQrCodeStatusModel

fun qrCodeStatusNotificationAD(
    onClick: () -> Unit
) = adapterDelegateViewBinding<ExpressScanQrCodeStatusModel, Any, ExpressQrFinishItemBinding>(
    { layoutInflater, parent ->
        ExpressQrFinishItemBinding.inflate(layoutInflater, parent, false)
    }
) {
    binding.vExpressScanQrFinish.setOnClickListener {
        onClick.invoke()
    }
}