package project1.app.expressscansdk.domain.usecase

import kotlinx.coroutines.flow.Flow
import project1.app.expressscan.api.domain.ExpressScanRepository
import project1.app.expressscan.api.domain.model.ExpressScanBasketStatusModel
import project1.app.expressscan.api.domain.usecase.ExpressScanGetBasketStatusAsFlowUseCase

class ExpressScanGetBasketStatusAsFlowUseCaseImpl(
    private val expressScanRepository: ExpressScanRepository
) : ExpressScanGetBasketStatusAsFlowUseCase {
    override operator fun invoke(): Flow<ExpressScanBasketStatusModel?> {
        return expressScanRepository.getExpressScanBasketStatusLiveDataAsFlow()
    }
}