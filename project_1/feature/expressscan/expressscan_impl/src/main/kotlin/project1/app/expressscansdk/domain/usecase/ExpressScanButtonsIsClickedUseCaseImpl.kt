package project1.app.expressscansdk.domain.usecase

import project1.app.expressscan.api.domain.ExpressScanButtonsRepository
import project1.app.expressscan.api.domain.usecase.ExpressScanButtonsIsClickedUseCase

class ExpressScanButtonsIsClickedUseCaseImpl(
    private val expressScanSdkRepository: ExpressScanButtonsRepository
) : ExpressScanButtonsIsClickedUseCase {
    override suspend operator fun invoke(): Boolean {
        return expressScanSdkRepository.isButtonsExpressScanClicked()
    }
}