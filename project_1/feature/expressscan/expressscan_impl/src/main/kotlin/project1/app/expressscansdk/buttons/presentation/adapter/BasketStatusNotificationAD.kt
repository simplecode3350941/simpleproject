package project1.app.expressscansdk.buttons.presentation.adapter

import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import project1.app.base.domain.toPriceWith
import project1.app.express_scan.databinding.ExpressBasketStatusItemBinding
import project1.app.expressscan.api.domain.model.ExpressScanBasketStatusModel

fun basketStatusNotificationAD(
    onClick: () -> Unit
) =
    adapterDelegateViewBinding<ExpressScanBasketStatusModel, Any, ExpressBasketStatusItemBinding>(
        { layoutInflater, parent ->
            ExpressBasketStatusItemBinding.inflate(layoutInflater, parent, false)
        }
    ) {
        binding.vExpressScanBasketStatus.setOnClickListener {
            onClick.invoke()
        }

        bind {
            if (item.isExist && item.totalPriceBasket != null) {
                binding.vBasketSum.text = item.totalPriceBasket!!.toPriceWith()
            }
        }
    }