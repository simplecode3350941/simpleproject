package project1.app.expressscansdk.domain.usecase

import org.threeten.bp.LocalDateTime
import project1.app.expressscan.api.domain.ExpressScanButtonsRepository
import project1.app.expressscan.api.domain.usecase.ExpressScanGetLastClickDateTimeButtonsUseCase

class ExpressScanGetLastClickDateTimeButtonsUseCaseImpl(
    private val expressScanSdkRepository: ExpressScanButtonsRepository
) : ExpressScanGetLastClickDateTimeButtonsUseCase {
    override suspend operator fun invoke(): LocalDateTime {
        val lastClickDateTimeButtons = expressScanSdkRepository.getLastClickDateTimeButtonsExpressScan()
        return if (lastClickDateTimeButtons != "") {
            LocalDateTime.parse(lastClickDateTimeButtons)
        } else {
            LocalDateTime.now()
        }
    }
}