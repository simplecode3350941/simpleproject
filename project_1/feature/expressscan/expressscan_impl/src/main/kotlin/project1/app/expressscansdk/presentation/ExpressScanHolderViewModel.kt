package project1.app.expressscansdk.presentation

import android.content.Context
import project1.app.base.ui.presentation.BaseViewModel
import project1.app.expressscan.api.domain.ExpressScanSdkAnalyticsInteractor
import project1.app.expressscan.api.domain.usecase.ExpressScanGetHostInstanceUseCase
import project1.app.expressscan.api.navigator.ExpressScanNavigator
import ru.x5.expressscan.sdk.host.entry.ExpressScanHost
import ru.x5.expressscan.sdk.host.listener.part.ExpressScanBehaviorListener

class ExpressScanHolderViewModel(
    private val navigator: ExpressScanNavigator,
    private val parameters: ExpressScanHolderParameters,
    private val getHostInstance: ExpressScanGetHostInstanceUseCase,
    private val analyticsInteractor: ExpressScanSdkAnalyticsInteractor
) : BaseViewModel(analyticsInteractor) {

    fun getExpressScanHostInstance(context: Context, behaviorListener: ExpressScanBehaviorListener): ExpressScanHost? {
        return getHostInstance(context, behaviorListener, parameters.screenType)
    }

    fun onX5AuthRequired() {
        navigator.toX5IdAuth()
    }

    fun onCloseExpressScan() {
        analyticsInteractor.trackExpressScanBack()
        navigator.backToMainScreen()
    }

    fun onOpenLatestTransaction() {
        navigator.backToMainScreen()
        navigator.toLatestTransaction()
    }
}