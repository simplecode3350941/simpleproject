package project1.app.expressscansdk.domain.usecase

import kotlinx.coroutines.flow.Flow
import project1.app.expressscan.api.domain.ExpressScanRepository
import project1.app.expressscan.api.domain.model.ExpressScanQrCodeStatusModel
import project1.app.expressscan.api.domain.usecase.ExpressScanGetQrCodeStatusAsFlowUseCase

class ExpressScanGetQrCodeStatusAsFlowUseCaseImpl(
    private val expressScanRepository: ExpressScanRepository
) : ExpressScanGetQrCodeStatusAsFlowUseCase {
    override operator fun invoke(): Flow<ExpressScanQrCodeStatusModel?> {
        return expressScanRepository.getExpressScanQrCodeStatusLiveDataAsFlow()
    }
}