package project1.app.expressscansdk.domain.usecase

import project1.app.expressscan.api.domain.ExpressScanRepository
import project1.app.expressscan.api.domain.usecase.ExpressScanInitIfEnabledUseCase
import project1.app.expressscan.api.domain.usecase.ExpressScanIsEnabledUseCase

class ExpressScanInitIfEnabledUseCaseImpl(
    private val expressScanRepository: ExpressScanRepository,
    private val isExpressScanEnabled: ExpressScanIsEnabledUseCase
) : ExpressScanInitIfEnabledUseCase {
    override suspend operator fun invoke() {
        if (isExpressScanEnabled()) {
            expressScanRepository.initSdk()
        }
    }
}