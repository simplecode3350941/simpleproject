package project1.app.expressscansdk.domain.usecase

import project1.app.base.segmentation.base.domain.ToggleGetValueUseCase
import project1.app.expressscan.api.domain.ExpressScanSegmentationRepository
import project1.app.expressscan.api.domain.usecase.ExpressScanIsEnabledUseCase

class ExpressScanIsEnabledUseCaseImpl(
    private val getToggleValueUseCase: ToggleGetValueUseCase,
    private val expressScanSegmentationRepository: ExpressScanSegmentationRepository
) : ExpressScanIsEnabledUseCase {
    override suspend operator fun invoke(): Boolean {
        return getToggleValueUseCase(expressScanSegmentationRepository.getExpressScanToggle())
            .isEnabled()
    }
}