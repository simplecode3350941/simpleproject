package project1.app.expressscansdk.buttons.presentation.adapter

import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import project1.app.base.ui.presentation.BaseDiffCallback
import project1.app.expressscan.api.domain.model.ExpressScanBasketStatusModel
import project1.app.expressscan.api.domain.model.ExpressScanQrCodeStatusModel

class ExpressScanStatusNotificationsAdapter(
    onExpressScanBasketButtonClick: () -> Unit,
    onExpressScanQrCodeButtonClick: () -> Unit
) : AsyncListDifferDelegationAdapter<Any>(DiffCallback) {

    init {
        delegatesManager
            .addDelegate(basketStatusNotificationAD(onExpressScanBasketButtonClick))
            .addDelegate(qrCodeStatusNotificationAD(onExpressScanQrCodeButtonClick))
    }

    private companion object DiffCallback : BaseDiffCallback() {

        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean = when {
            oldItem is ExpressScanBasketStatusModel && newItem is ExpressScanBasketStatusModel -> {
                oldItem == newItem
            }
            oldItem is ExpressScanQrCodeStatusModel && newItem is ExpressScanQrCodeStatusModel -> {
                oldItem == newItem
            }
            else -> false
        }
    }
}