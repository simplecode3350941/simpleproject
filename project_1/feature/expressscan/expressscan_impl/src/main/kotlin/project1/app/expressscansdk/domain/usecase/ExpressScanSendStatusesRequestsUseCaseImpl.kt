package project1.app.expressscansdk.domain.usecase

import project1.app.expressscan.api.domain.ExpressScanRepository
import project1.app.expressscan.api.domain.usecase.ExpressScanSendStatusesRequestsUseCase

class ExpressScanSendStatusesRequestsUseCaseImpl(
    private val expressScanRepository: ExpressScanRepository
) : ExpressScanSendStatusesRequestsUseCase {
    override operator fun invoke() {
        expressScanRepository.sendExpressScanStatusesRequests()
    }
}