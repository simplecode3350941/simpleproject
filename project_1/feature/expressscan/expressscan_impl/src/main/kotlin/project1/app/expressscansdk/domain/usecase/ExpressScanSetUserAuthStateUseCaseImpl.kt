package project1.app.expressscansdk.domain.usecase

import project1.app.expressscan.api.domain.ExpressScanRepository
import project1.app.expressscan.api.domain.usecase.ExpressScanSetUserAuthStateUseCase

class ExpressScanSetUserAuthStateUseCaseImpl(
    private val expressScanRepository: ExpressScanRepository
) : ExpressScanSetUserAuthStateUseCase {
    override operator fun invoke(isAuthorized: Boolean) {
        expressScanRepository.setUserAuthState(isAuthorized)
    }
}