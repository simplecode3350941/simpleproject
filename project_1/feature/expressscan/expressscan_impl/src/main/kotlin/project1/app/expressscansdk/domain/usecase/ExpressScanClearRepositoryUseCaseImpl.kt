package project1.app.expressscansdk.domain.usecase

import project1.app.expressscan.api.domain.ExpressScanRepository
import project1.app.expressscan.api.domain.usecase.ExpressScanClearRepositoryUseCase

class ExpressScanClearRepositoryUseCaseImpl(
    private val expressScanRepository: ExpressScanRepository
) : ExpressScanClearRepositoryUseCase {
    override operator fun invoke() {
        expressScanRepository.clearRepository()
    }
}