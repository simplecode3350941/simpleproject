package project1.app.expressscansdk.buttons.presentation

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import project1.app.base.ui.extension.getDimensionPixelSizeKtx
import project1.app.base.ui.widget.recycler.decoration.SymmetricOffsetsItemDecoration
import project1.app.express_scan.R
import project1.app.expressscan.api.domain.model.ExpressScanBasketStatusModel
import project1.app.expressscan.api.domain.model.ExpressScanQrCodeStatusModel
import project1.app.expressscan.api.domain.model.ExpressScanStatusesModel
import project1.app.expressscansdk.buttons.presentation.adapter.ExpressScanStatusNotificationsAdapter

class ExpressScanStatusNotificationsListView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RecyclerView(context, attributeSet, defStyleAttr) {

    private var onExpressScanBasketButtonClick: (() -> Unit)? = null
    private var onExpressScanQrCodeButtonClick: (() -> Unit)? = null

    private val notificationAdapter = ExpressScanStatusNotificationsAdapter(
        onExpressScanBasketButtonClick = { onExpressScanBasketButtonClick?.invoke() },
        onExpressScanQrCodeButtonClick = { onExpressScanQrCodeButtonClick?.invoke() }
    )

    init {
        adapter = notificationAdapter
        layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
        addItemDecoration(
            SymmetricOffsetsItemDecoration(
                offsetHorizontal =
                context.getDimensionPixelSizeKtx(
                    R.dimen.express_scan_status_notification_vertical_margin
                )
            )
        )
        PagerSnapHelper().attachToRecyclerView(this)
        clipChildren = false
        clipToPadding = false
    }

    fun setStatusesClickListeners(
        basketClickListener: () -> Unit,
        qrCodeStatusClickLister: () -> Unit
    ) {
        onExpressScanBasketButtonClick = basketClickListener
        onExpressScanQrCodeButtonClick = qrCodeStatusClickLister
    }

    fun addStatus(model: Any) {
        when (model) {
            is ExpressScanBasketStatusModel -> {
                if (model.isExist) {
                    newItemExistPrepare(model)
                } else {
                    newItemNotExistPrepare(model)
                }
            }
            is ExpressScanQrCodeStatusModel -> {
                if (model.isExist) {
                    newItemExistPrepare(model)
                } else {
                    newItemNotExistPrepare(model)
                }
            }
        }
        notificationAdapter.notifyDataSetChanged()
    }

    private fun newItemExistPrepare(model: Any) {
        val newList = notificationAdapter.items.toMutableList()
        val index = getIndexOfContainsModelInAdapterItems(model, newList)
        if (index != -1) {
            newList[index] = model
            notificationAdapter.items = newList
        } else {
            if (notificationAdapter.items == null || notificationAdapter.items.isEmpty()) {
                notificationAdapter.items = ExpressScanStatusesModel(mutableListOf(model)).items
            } else {
                newList.add(model)
                notificationAdapter.items = newList
            }
        }
    }

    private fun newItemNotExistPrepare(model: Any) {
        val newList = notificationAdapter.items.toMutableList()
        val index = getIndexOfContainsModelInAdapterItems(model, newList)
        if (index != -1) {
            if (newList.size > 1) {
                newList.remove(index)
                val list = arrayListOf<Any>()
                newList.forEachIndexed { indexItem, item ->
                    if (indexItem != index) {
                        list.add(item)
                    }
                }
                notificationAdapter.items = list
            } else {
                notificationAdapter.items = arrayListOf()
            }
        }
    }

    private fun getIndexOfContainsModelInAdapterItems(model: Any, newList: MutableList<Any>): Int {
        var modelIndex = -1
        when (model) {
            is ExpressScanBasketStatusModel -> {
                newList.forEachIndexed { index, item ->
                    if (item is ExpressScanBasketStatusModel) {
                        modelIndex = index
                    }
                }
            }
            is ExpressScanQrCodeStatusModel -> {
                newList.forEachIndexed { index, item ->
                    if (item is ExpressScanQrCodeStatusModel) {
                        modelIndex = index
                    }
                }
            }
        }
        return modelIndex
    }
}