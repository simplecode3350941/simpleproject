package project1.app.expressscan.api.domain.usecase

import org.threeten.bp.LocalDateTime

interface ExpressScanGetLastClickDateTimeButtonsUseCase {
    suspend operator fun invoke(): LocalDateTime
}