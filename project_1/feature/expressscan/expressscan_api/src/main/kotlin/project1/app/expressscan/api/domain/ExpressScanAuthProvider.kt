package project1.app.expressscan.api.domain

interface ExpressScanAuthProvider {
    fun getTokenSync(): String?
    suspend fun getRefreshToken(): String?
    fun getRefreshTokenSync(): String?
}