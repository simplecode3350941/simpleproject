package project1.app.expressscan.api.domain

import project1.app.base.segmentation.base.domain.SegmentationToggleProvider
import project1.app.base.segmentation.base.domain.model.SegmentationToggle

interface ExpressScanSegmentationRepository : SegmentationToggleProvider {
    fun getExpressScanToggle(): SegmentationToggle
}