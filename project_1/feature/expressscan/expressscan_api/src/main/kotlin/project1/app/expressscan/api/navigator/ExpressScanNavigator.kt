package project1.app.expressscan.api.navigator

interface ExpressScanNavigator {
    fun backToMainScreen()
    fun toX5IdAuth()
    fun toExpressScan(screenType: ExpressScanScreenType)
    fun toLatestTransaction()
}