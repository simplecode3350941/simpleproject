package project1.app.expressscan.api.domain.usecase

interface ExpressScanIsEnabledUseCase {
    suspend operator fun invoke(): Boolean
}