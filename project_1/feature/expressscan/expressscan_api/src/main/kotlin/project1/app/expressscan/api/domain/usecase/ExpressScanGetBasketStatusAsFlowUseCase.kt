package project1.app.expressscan.api.domain.usecase

import kotlinx.coroutines.flow.Flow
import project1.app.expressscan.api.domain.model.ExpressScanBasketStatusModel

interface ExpressScanGetBasketStatusAsFlowUseCase {
    operator fun invoke(): Flow<ExpressScanBasketStatusModel?>
}