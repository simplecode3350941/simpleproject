package project1.app.expressscan.api.domain.usecase

interface ExpressScanButtonsIsClickedUseCase {
    suspend operator fun invoke(): Boolean
}