package project1.app.expressscan.api.domain.usecase

interface ExpressScanButtonsSetClickedUseCase {
    suspend operator fun invoke(isButtonsClickedValue: Boolean)
}