package project1.app.expressscan.api.domain.model

data class ExpressScanBasketStatusModel(
    val isExist: Boolean,
    val totalPriceBasket: Double?
)