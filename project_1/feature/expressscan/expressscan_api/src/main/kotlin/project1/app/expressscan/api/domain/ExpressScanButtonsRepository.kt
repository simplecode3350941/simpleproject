package project1.app.expressscan.api.domain

interface ExpressScanButtonsRepository {
    suspend fun isButtonsExpressScanClicked(): Boolean
    suspend fun getLastClickDateTimeButtonsExpressScan(): String
    suspend fun setExpressScanButtonsClicked(isButtonsClickedValue: Boolean)
    suspend fun setLastClickDateTimeButtonsExpressScan(lastClickDateTimeButtonsExpressScan: String)
}