package project1.app.expressscan.api.domain.usecase

interface ExpressScanSetUserAuthStateUseCase {
    operator fun invoke(isAuthorized: Boolean)
}