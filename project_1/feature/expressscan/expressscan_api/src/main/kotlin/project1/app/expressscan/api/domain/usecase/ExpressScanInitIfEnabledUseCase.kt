package project1.app.expressscan.api.domain.usecase

interface ExpressScanInitIfEnabledUseCase {
    suspend operator fun invoke()
}