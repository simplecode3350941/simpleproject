package project1.app.expressscan.api.domain.usecase

interface ExpressScanClearRepositoryUseCase {
    operator fun invoke()
}