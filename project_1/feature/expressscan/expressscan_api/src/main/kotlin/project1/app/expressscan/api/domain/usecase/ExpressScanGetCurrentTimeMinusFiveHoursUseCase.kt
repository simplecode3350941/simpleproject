package project1.app.expressscan.api.domain.usecase

import org.threeten.bp.LocalDateTime

interface ExpressScanGetCurrentTimeMinusFiveHoursUseCase {
    operator fun invoke(): LocalDateTime
}