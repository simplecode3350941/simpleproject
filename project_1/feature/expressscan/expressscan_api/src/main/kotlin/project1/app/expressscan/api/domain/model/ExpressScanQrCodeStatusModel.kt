package project1.app.expressscan.api.domain.model

data class ExpressScanQrCodeStatusModel(
    val isExist: Boolean
)