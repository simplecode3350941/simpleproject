package project1.app.expressscan.api.domain.usecase

import android.content.Context
import project1.app.expressscan.api.navigator.ExpressScanScreenType
import ru.x5.expressscan.sdk.host.entry.ExpressScanHost
import ru.x5.expressscan.sdk.host.listener.part.ExpressScanBehaviorListener

interface ExpressScanGetHostInstanceUseCase {
    operator fun invoke(
        context: Context,
        behaviorListener: ExpressScanBehaviorListener,
        screenType: ExpressScanScreenType
    ): ExpressScanHost?
}