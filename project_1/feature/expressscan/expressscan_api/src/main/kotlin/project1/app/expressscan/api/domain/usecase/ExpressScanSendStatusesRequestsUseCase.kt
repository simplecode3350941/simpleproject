package project1.app.expressscan.api.domain.usecase

interface ExpressScanSendStatusesRequestsUseCase {
    operator fun invoke()
}