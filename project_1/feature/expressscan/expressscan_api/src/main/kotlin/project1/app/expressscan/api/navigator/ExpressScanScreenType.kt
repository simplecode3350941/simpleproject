package project1.app.expressscan.api.navigator

enum class ExpressScanScreenType {
    DEFAULT,
    BASKET,
    EXIT_QR
}