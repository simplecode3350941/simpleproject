package project1.app.expressscan.api.domain

import android.content.Context
import kotlinx.coroutines.flow.Flow
import project1.app.expressscan.api.domain.model.ExpressScanBasketStatusModel
import project1.app.expressscan.api.domain.model.ExpressScanQrCodeStatusModel
import project1.app.expressscan.api.navigator.ExpressScanScreenType
import ru.x5.expressscan.sdk.core.listener.part.ExpressScanBasketListener
import ru.x5.expressscan.sdk.core.listener.part.ExpressScanTokenListener
import ru.x5.expressscan.sdk.host.entry.ExpressScanHost
import ru.x5.expressscan.sdk.host.listener.part.ExpressScanBehaviorListener

interface ExpressScanRepository : ExpressScanTokenListener, ExpressScanBasketListener {
    fun getExpressScanBasketStatusLiveDataAsFlow(): Flow<ExpressScanBasketStatusModel?>
    fun getExpressScanQrCodeStatusLiveDataAsFlow(): Flow<ExpressScanQrCodeStatusModel?>
    fun initSdk()
    fun setUserAuthState(isAuthorized: Boolean)
    fun clearRepository()
    fun sendExpressScanStatusesRequests()
    fun getHostInstance(
        context: Context,
        behaviorListener: ExpressScanBehaviorListener,
        screenType: ExpressScanScreenType
    ): ExpressScanHost?
}