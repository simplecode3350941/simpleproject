package project1.app.expressscan.api.domain.usecase

import kotlinx.coroutines.flow.Flow
import project1.app.expressscan.api.domain.model.ExpressScanQrCodeStatusModel

interface ExpressScanGetQrCodeStatusAsFlowUseCase {
    operator fun invoke(): Flow<ExpressScanQrCodeStatusModel?>
}