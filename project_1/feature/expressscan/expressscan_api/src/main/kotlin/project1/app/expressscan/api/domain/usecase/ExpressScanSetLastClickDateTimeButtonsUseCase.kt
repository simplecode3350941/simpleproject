package project1.app.expressscan.api.domain.usecase

interface ExpressScanSetLastClickDateTimeButtonsUseCase {
    suspend operator fun invoke()
}