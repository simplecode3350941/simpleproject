package project1.app.expressscan.api.domain

import project1.app.base.analytics.domain.AnalyticsInteractor

interface ExpressScanSdkAnalyticsInteractor : AnalyticsInteractor {
    fun trackExpressScanMain()
    fun trackExpressScanShopContinueShow()
    fun trackExpressScanShopContinue()
    fun trackExpressScanQrExitShow()
    fun trackExpressScanQrExit()
    fun trackExpressScanBack()
}