package project1.app.x5bank.domain

interface X5BankSetUserNotInterestedInLoanUseCase {
    suspend operator fun invoke()
}