package project1.app.x5bank.domain

object X5BankPromoConstants {
    const val X5BANK_PROMO_UPGRADE_CARD_UP = "x5bank_promo_upgrade_card_up"
    const val X5BANK_PROMO_UPGRADE_CARD_DOWN = "x5bank_promo_upgrade_card_down"
    const val X5BANK_PROMO_CARD_DETAIL = "x5bank_promo_card_detail"
    const val X5BANK_PROMO_CALLBACK = "x5bank_promo_callback"
    const val X5BANK_PROMO_PHONE_NUMBER = "8(800)505-20-00"

    const val X5BANK_PROMO_POPUP_BANNER_CODE = "bank_popup"
    const val X5BANK_PROMO_PROMO_BANNER_CODE = "promo_banner"
    const val X5BANK_PROMO_DEEPLINK_BANNER_CODE = "deeplink"
    const val X5BANK_PROMO_STORIES_BANNER_CODE = "bank_stories"

    const val X5BANK_LOAN_POPUP_BANNER_CODE = "microloan_popup"
    const val X5BANK_LOAN_STORIES_BANNER_CODE = "microloan_stories"

    const val X5BANK_CREDIT_CARD_POPUP_BANNER_CODE = "x5bank_credit_card_popup"
    const val X5BANK_CREDIT_CARD_STORIES_BANNER_CODE = "x5bank_credit_card_stories"
}