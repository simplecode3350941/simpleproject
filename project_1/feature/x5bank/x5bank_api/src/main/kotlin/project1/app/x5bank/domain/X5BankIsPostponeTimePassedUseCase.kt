package project1.app.x5bank.domain

interface X5BankIsPostponeTimePassedUseCase {
    suspend operator fun invoke(): Boolean
}