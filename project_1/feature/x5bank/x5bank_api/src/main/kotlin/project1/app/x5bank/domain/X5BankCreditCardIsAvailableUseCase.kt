package project1.app.x5bank.domain

interface X5BankCreditCardIsAvailableUseCase {
    suspend operator fun invoke(): Boolean
}