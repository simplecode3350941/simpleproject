package project1.app.x5bank.presentation.model

enum class X5BankUrlType {
    CARD_ISSUE,
    CARD_SETTINGS,
    MICROLOAN,
    CREDIT_CARD
}