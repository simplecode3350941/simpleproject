package project1.app.x5bank.domain

interface X5BankIsNewFlowAvailableUseCase {
    suspend operator fun invoke(): Boolean
}