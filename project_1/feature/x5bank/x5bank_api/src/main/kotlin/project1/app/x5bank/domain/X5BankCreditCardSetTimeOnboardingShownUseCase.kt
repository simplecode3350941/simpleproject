package project1.app.x5bank.domain

interface X5BankCreditCardSetTimeOnboardingShownUseCase {
    suspend operator fun invoke()
}