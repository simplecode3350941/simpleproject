package project1.app.x5bank.domain

import project1.app.base.ui.webview.UrlWithHeaders

interface X5BankGetNoLandingUrlUseCase {
    suspend operator fun invoke(bannerCode: String?): UrlWithHeaders
}