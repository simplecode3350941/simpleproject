package project1.app.x5bank.presentation.onboarding

import project1.app.base.ui.onboarding.OnboardingDelegate
import project1.app.x5bank.domain.X5BankLoanIsAvailableUseCase
import project1.app.x5bank.domain.X5BankLoanIsPostponeTimePassedUseCase
import project1.app.x5bank.domain.X5BankLoanSetTimeOnboardingShownUseCase
import project1.app.x5bank.navigator.X5BankNavigator

class X5BankLoanOnboardingDelegate(
    private val isX5BankLoanAvailable: X5BankLoanIsAvailableUseCase,
    private val isX5BankLoanPostponeTimePassed: X5BankLoanIsPostponeTimePassedUseCase,
    private val setTimeWhenX5BankLoanOnboardingShown: X5BankLoanSetTimeOnboardingShownUseCase,
    private val navigator: X5BankNavigator,
    priority: Int
) : OnboardingDelegate {

    override val onboardingPriority: Int = priority

    override suspend fun shouldShow(): Boolean {
        return isX5BankLoanAvailable() &&
            isX5BankLoanPostponeTimePassed()
    }

    override suspend fun showNow() {
        setTimeWhenX5BankLoanOnboardingShown()
        navigator.toX5BankLoanOnboarding()
    }
}