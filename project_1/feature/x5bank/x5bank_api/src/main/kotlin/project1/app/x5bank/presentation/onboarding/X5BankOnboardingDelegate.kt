package project1.app.x5bank.presentation.onboarding

import project1.app.base.ui.onboarding.OnboardingDelegate
import project1.app.x5bank.domain.X5BankIsAvailableUseCase
import project1.app.x5bank.domain.X5BankIsPostponeTimePassedUseCase
import project1.app.x5bank.domain.X5BankSetTimeOnboardingShownUseCase
import project1.app.x5bank.navigator.X5BankNavigator

class X5BankOnboardingDelegate(
    private val isX5BankAvailable: X5BankIsAvailableUseCase,
    private val isX5BankPostponeTimePassed: X5BankIsPostponeTimePassedUseCase,
    private val setTimeWhenX5BankOnboardingShown: X5BankSetTimeOnboardingShownUseCase,
    private val navigator: X5BankNavigator,
    priority: Int
) : OnboardingDelegate {

    override val onboardingPriority: Int = priority

    override suspend fun shouldShow(): Boolean {
        return isX5BankAvailable() &&
            isX5BankPostponeTimePassed()
    }

    override suspend fun showNow() {
        setTimeWhenX5BankOnboardingShown()
        navigator.toX5BankOnboarding()
    }
}