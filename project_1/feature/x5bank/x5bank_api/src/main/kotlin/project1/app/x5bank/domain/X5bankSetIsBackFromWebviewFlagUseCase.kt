package project1.app.x5bank.domain

interface X5bankSetIsBackFromWebviewFlagUseCase {
    suspend operator fun invoke(isBackFromX5bankWebviewFlagValue: Boolean)
}