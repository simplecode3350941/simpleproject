package project1.app.x5bank.domain

interface X5BankSetTimeOnboardingShownUseCase {
    suspend operator fun invoke()
}