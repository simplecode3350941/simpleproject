package project1.app.x5bank.domain

import project1.app.base.analytics.domain.AnalyticsInteractor

interface X5BankAnalyticsInteractor : AnalyticsInteractor {
    fun trackBankPopup()
    fun trackBankPopupClose()
    fun trackBankPopupMore()
    fun trackBankPopupLater()
    fun trackBankPromoBanner()
    fun trackBankPromoBannerClick()
    fun trackBankNewsBanner()
    fun trackBankNewsBannerClick()
    fun trackBankScreen()
    fun trackBankScreenClose()
    fun trackBankScreenUpdateUp()
    fun trackBankScreenUpdateDown()
    fun trackBankScreenMore()
    fun trackBankScreenContact()
    fun trackBankScreenPromotionMore()
    fun trackCreditCardPopup()
    fun trackCreditCardPopupMore()
    fun trackCreditCardPopupClose()
}