package project1.app.x5bank.navigator

import project1.app.x5bank.presentation.model.X5BankUrlType

interface X5BankNavigator {
    fun toX5Bank(bannerCode: String? = null, urlType: X5BankUrlType = X5BankUrlType.CARD_ISSUE)
    fun toX5BankPromo()
    fun toX5BankOnboarding()
    fun toX5BankLoanOnboarding()
    fun back()
    fun toCallBack(phoneNumber: String)
    fun toDocument(url: String)
    fun toX5BankCreditCardOnboarding()
}