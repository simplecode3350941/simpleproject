package project1.app.x5bank.domain

interface X5bankIsBackFromWebviewUseCase {
    suspend operator fun invoke(): Boolean
}