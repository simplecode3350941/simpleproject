package project1.app.x5bank.domain

interface X5BankLoanIsPostponeTimePassedUseCase {
    suspend operator fun invoke(): Boolean
}