package project1.app.x5bank.presentation.onboarding

import project1.app.base.ui.onboarding.OnboardingDelegate
import project1.app.x5bank.domain.X5BankCreditCardIsAvailableUseCase
import project1.app.x5bank.domain.X5BankCreditCardIsPostponeTimePassedUseCase
import project1.app.x5bank.domain.X5BankCreditCardSetTimeOnboardingShownUseCase
import project1.app.x5bank.navigator.X5BankNavigator

class X5BankCreditCardOnboardingDelegate(
    private val isCreditCardAvailable: X5BankCreditCardIsAvailableUseCase,
    private val isCreditCardPostponeTimePassed: X5BankCreditCardIsPostponeTimePassedUseCase,
    private val setTimeWhenCreditCardOnboardingShown: X5BankCreditCardSetTimeOnboardingShownUseCase,
    private val navigator: X5BankNavigator,
    priority: Int
) : OnboardingDelegate {

    override val onboardingPriority: Int = priority

    override suspend fun shouldShow(): Boolean {
        return isCreditCardAvailable() &&
            isCreditCardPostponeTimePassed()
    }

    override suspend fun showNow() {
        setTimeWhenCreditCardOnboardingShown()
        navigator.toX5BankCreditCardOnboarding()
    }
}