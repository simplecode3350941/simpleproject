package project1.app.x5bank.domain

interface X5BankIsAvailableUseCase {
    suspend operator fun invoke(): Boolean
}