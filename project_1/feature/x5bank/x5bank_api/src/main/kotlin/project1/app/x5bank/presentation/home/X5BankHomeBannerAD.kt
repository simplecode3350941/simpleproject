package project1.app.x5bank.presentation.home

import android.view.View
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import project1.app.base.ui.util.setupHorizontalItemView
import project1.app.x5bank.databinding.X5bankHomeBannerBinding
import project1.app.x5bank.presentation.home.model.X5BankHomeBannerUiModel

fun x5BankHomeBannerAd(
    isLargeItem: Boolean,
    onClick: (X5BankHomeBannerUiModel) -> Unit
) = adapterDelegateViewBinding<X5BankHomeBannerUiModel, Any, X5bankHomeBannerBinding>(
    { layoutInflater, parent ->
        X5bankHomeBannerBinding.inflate(layoutInflater, parent, false)
    }
) {
    val clickListener = View.OnClickListener {
        when (it.id) {
            binding.vButton.id, binding.vContainer.id -> {
                onClick(item)
            }
        }
    }
    binding.vContainerLayout.fitWidth = isLargeItem
    binding.vButton.setOnClickListener(clickListener)
    binding.vContainer.setOnClickListener(clickListener)

    setupHorizontalItemView(itemView)
}