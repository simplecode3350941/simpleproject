package project1.app.x5bank.navigator

import project1.app.x5bank.presentation.model.X5BankUrlType
import ru.terrakok.cicerone.android.support.SupportAppScreen

/**
 * Интерфейс, предоставляющий другим модулям доступ к экранам модуля x5Bank
 */
interface X5BankScreenProvider {
    fun provideX5BankPromoScreen(bannerCode: String? = null): SupportAppScreen
    fun provideX5BankOnboardingScreen(): SupportAppScreen
    fun provideX5BankScreen(
        bannerCode: String? = null,
        urlType: X5BankUrlType = X5BankUrlType.CARD_ISSUE
    ): SupportAppScreen
}