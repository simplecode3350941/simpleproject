package project1.app.x5bank.base.domain.usecase

class X5BankGetAgreementUseCase {
    operator fun invoke() = AGREEMENT_URL

    companion object {
        private const val AGREEMENT_URL = "https://x5bank.ru/static/files/docs/tariffs/5ka.pdf"
    }
}