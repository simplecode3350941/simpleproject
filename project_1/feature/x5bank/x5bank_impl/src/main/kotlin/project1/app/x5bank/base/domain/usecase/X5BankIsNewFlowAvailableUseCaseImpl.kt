package project1.app.x5bank.base.domain.usecase

import project1.app.base.segmentation.base.domain.ToggleGetValueUseCase
import project1.app.base.segmentation.base.domain.model.SegmentationToggleValueModel
import project1.app.x5bank.base.domain.X5BankSegmentationRepository
import project1.app.x5bank.domain.X5BankIsNewFlowAvailableUseCase

class X5BankIsNewFlowAvailableUseCaseImpl(
    private val segmentationRepository: X5BankSegmentationRepository,
    private val getToggleValue: ToggleGetValueUseCase
) : X5BankIsNewFlowAvailableUseCase {

    override suspend operator fun invoke(): Boolean {
        return getX5BankNoLandingToggleValue().isEnabled()
    }

    private suspend fun getX5BankNoLandingToggleValue(): SegmentationToggleValueModel {
        return getToggleValue(segmentationRepository.getX5BankNoLandingToggle())
    }
}