package project1.app.x5bank.webview

import android.annotation.TargetApi
import android.graphics.Bitmap
import android.os.Build
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient

class X5BankWebClient(
    private val callback: X5BankCallback
) : WebViewClient() {

    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
        super.onPageStarted(view, url, favicon)
        callback.onPageStarted()
    }

    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        return handleUrl(request?.url?.toString() ?: return false)
    }

    @Suppress("DEPRECATION")
    override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
        return handleUrl(url ?: return false)
    }

    @TargetApi(Build.VERSION_CODES.M)
    override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
        // Nothing
    }

    override fun onReceivedError(view: WebView?, errorCode: Int, description: String?, failingUrl: String?) {
        // Nothing
    }

    private fun handleUrl(url: String): Boolean = when {
        url.contains(X5BANK_CLOSE) -> {
            callback.onCloseWebView()
            true
        }
        url.contains(X5BANK_CARD_ISSUED) -> {
            callback.onCardIssued()
            true
        }
        else -> false
    }

    companion object {
        private const val X5BANK_CLOSE = "action/close"
        private const val X5BANK_CARD_ISSUED = "thanks"
    }
}