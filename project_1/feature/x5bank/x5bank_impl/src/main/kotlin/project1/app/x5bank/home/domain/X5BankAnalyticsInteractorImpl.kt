package project1.app.x5bank.home.domain

import project1.app.base.analytics.domain.AnalyticsInteractor
import project1.app.x5bank.domain.X5BankAnalyticsInteractor

internal class X5BankAnalyticsInteractorImpl(
    analyticsInteractor: AnalyticsInteractor
) : X5BankAnalyticsInteractor, AnalyticsInteractor by analyticsInteractor {

    override fun trackBankPopup() {
        trackBankEvent(X5BankAnalyticsConstants.BANK_POPUP)
    }

    override fun trackBankPopupClose() {
        trackBankEvent(X5BankAnalyticsConstants.BANK_POPUP_CLOSE)
    }

    override fun trackBankPopupMore() {
        trackBankEvent(X5BankAnalyticsConstants.BANK_POPUP_MORE)
    }

    override fun trackBankPopupLater() {
        trackBankEvent(X5BankAnalyticsConstants.BANK_POPUP_LATER)
    }

    override fun trackBankPromoBanner() {
        trackBankEvent(X5BankAnalyticsConstants.BANK_PROMO_BANNER)
    }

    override fun trackBankPromoBannerClick() {
        trackBankEvent(X5BankAnalyticsConstants.BANK_PROMO_BANNER_CLICK)
    }

    override fun trackBankNewsBanner() {
        trackBankEvent(X5BankAnalyticsConstants.BANK_NEWS_BANNER)
    }

    override fun trackBankNewsBannerClick() {
        trackBankEvent(X5BankAnalyticsConstants.BANK_NEWS_BANNER_CLICK)
    }

    override fun trackBankScreen() {
        trackBankEvent(X5BankAnalyticsConstants.BANK_SCREEN)
    }

    override fun trackBankScreenClose() {
        trackBankEvent(X5BankAnalyticsConstants.BANK_SCREEN_CLOSE)
    }

    override fun trackBankScreenUpdateUp() {
        trackBankEvent(X5BankAnalyticsConstants.BANK_SCREEN_UPDATE_UP)
    }

    override fun trackBankScreenUpdateDown() {
        trackBankEvent(X5BankAnalyticsConstants.BANK_SCREEN_UPDATE_DOWN)
    }

    override fun trackBankScreenMore() {
        trackBankEvent(X5BankAnalyticsConstants.BANK_SCREEN_MORE)
    }

    override fun trackBankScreenContact() {
        trackBankEvent(X5BankAnalyticsConstants.BANK_SCREEN_CONTACT)
    }

    override fun trackBankScreenPromotionMore() {
        trackBankEvent(X5BankAnalyticsConstants.BANK_SCREEN_PROMOTION_MORE)
    }

    override fun trackCreditCardPopup() {
        trackBankEvent(X5BankAnalyticsConstants.CREDIT_CARD_POPUP)
    }

    override fun trackCreditCardPopupMore() {
        trackBankEvent(X5BankAnalyticsConstants.CREDIT_CARD_MORE)
    }

    override fun trackCreditCardPopupClose() {
        trackBankEvent(X5BankAnalyticsConstants.CREDIT_CARD_POPUP_CLOSE)
    }

    private fun trackBankEvent(action: String) {
        trackEvent(X5BankAnalyticsConstants.BANK_EVENT_NAME, action)
    }
}