package project1.app.x5bank.home.domain

internal object X5BankAnalyticsConstants {
    const val BANK_EVENT_NAME = "Bank"
    const val BANK_POPUP = "Bank_Popup"
    const val BANK_POPUP_CLOSE = "Bank_Popup_Close"
    const val BANK_POPUP_MORE = "Bank_Popup_More"
    const val BANK_POPUP_LATER = "Bank_Popup_Later"
    const val BANK_PROMO_BANNER = "Bank_Promo_Banner"
    const val BANK_PROMO_BANNER_CLICK = "Bank_Promo_Banner_Click"
    const val BANK_NEWS_BANNER = "Bank_News_Banner"
    const val BANK_NEWS_BANNER_CLICK = "Bank_News_Banner_Click"
    const val BANK_SCREEN = "Bank_Screen"
    const val BANK_SCREEN_CLOSE = "Bank_Screen_Close"
    const val BANK_SCREEN_UPDATE_UP = "Bank_Screen_UpdateUp"
    const val BANK_SCREEN_UPDATE_DOWN = "Bank_Screen_UpdateDown"
    const val BANK_SCREEN_MORE = "Bank_Screen_More"
    const val BANK_SCREEN_CONTACT = "Bank_Screen_Contact"
    const val BANK_SCREEN_PROMOTION_MORE = "Bank_Screen_Promotion_More"

    const val CREDIT_CARD_POPUP = "Bank_CreditCard"
    const val CREDIT_CARD_MORE = "Bank_CreditCard_More"
    const val CREDIT_CARD_POPUP_CLOSE = "Bank_CreditCard_Close"
}