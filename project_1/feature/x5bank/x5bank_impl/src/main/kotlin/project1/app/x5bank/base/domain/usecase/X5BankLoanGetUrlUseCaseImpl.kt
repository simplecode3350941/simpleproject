package project1.app.x5bank.base.domain.usecase

import project1.app.base.ui.webview.UrlWithHeaders
import project1.app.x5bank.base.domain.X5BankRepository
import project1.app.x5bank.domain.X5BankLoanGetUrlUseCase

internal class X5BankLoanGetUrlUseCaseImpl(
    private val repository: X5BankRepository
) : X5BankLoanGetUrlUseCase {
    override suspend operator fun invoke(bannerCode: String?): UrlWithHeaders {
        val url = repository.loadLoanUrl(bannerCode)
        return UrlWithHeaders(
            requireNotNull(url)
        )
    }
}