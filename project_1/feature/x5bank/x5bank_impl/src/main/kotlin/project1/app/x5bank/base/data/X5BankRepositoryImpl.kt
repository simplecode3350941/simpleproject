package project1.app.x5bank.base.data

import kotlinx.coroutines.withContext
import project1.app.base.coroutines.AppDispatchers
import project1.app.base.data.preferences.Preferences
import project1.app.x5bank.base.data.remote.X5BankApi
import project1.app.x5bank.base.domain.X5BankRepository

class X5BankRepositoryImpl(
    private val api: X5BankApi,
    private val preferences: Preferences,
    private val dispatchers: AppDispatchers
) : X5BankRepository {

    override suspend fun loadUrl(bannerCode: String?): String? {
        val response = runCatching {
            api.getUrl(bannerCode)
        }.getOrNull()
        return response?.url
    }

    override suspend fun loadNoLandingUrl(bannerCode: String?): String? {
        val response = runCatching {
            api.getNoLandingUrl(bannerCode)
        }.getOrNull()
        return response?.url
    }

    override suspend fun loadLoanUrl(bannerCode: String?): String? {
        val response = runCatching {
            api.getLoanUrl(bannerCode)
        }.getOrNull()
        return response?.url
    }

    override suspend fun loadCreditCardUrl(bannerCode: String?): String? {
        val response = runCatching {
            api.getCreditCardUrl(bannerCode)
        }.getOrNull()
        return response?.url
    }

    override suspend fun getX5BankIssueState(): Boolean {
        val response = runCatching {
            api.getX5BankIssueState()
        }.getOrNull()
        return response?.isIssued ?: false
    }

    override suspend fun saveOnboardingTimestamp(timestamp: Long) {
        withContext(dispatchers.storage) {
            preferences[ONBOARDING_TIMESTAMP_KEY] = timestamp
        }
    }

    override suspend fun getOnboardingTimestamp(): Long {
        return withContext(dispatchers.storage) {
            preferences[ONBOARDING_TIMESTAMP_KEY, Long::class] ?: 0L
        }
    }

    override suspend fun saveLoanOnboardingTimestamp(timestamp: Long) {
        withContext(dispatchers.storage) {
            preferences[ONBOARDING_LOAN_TIMESTAMP_KEY] = timestamp
        }
    }

    override suspend fun getLoanOnboardingTimestamp(): Long {
        return withContext(dispatchers.storage) {
            preferences[ONBOARDING_LOAN_TIMESTAMP_KEY, Long::class] ?: 0L
        }
    }

    override suspend fun isBackFromX5bankWebview(): Boolean {
        return withContext(dispatchers.storage) {
            preferences[IS_BACK_FROM_X5BANK_WEBVIEW_KEY, Boolean::class] ?: false
        }
    }

    override suspend fun setIsBackFromX5bankWebviewFlag(isBackFromX5bankWebviewFlagValue: Boolean) {
        withContext(dispatchers.storage) {
            preferences[IS_BACK_FROM_X5BANK_WEBVIEW_KEY] = isBackFromX5bankWebviewFlagValue
        }
    }

    override suspend fun setX5BankCardIssued(): Boolean {
        val response = runCatching {
            api.setX5BankCardIssued()
        }.getOrNull()
        return response?.isSuccessful ?: false
    }

    override suspend fun isUserInterestedInLoan(): Boolean {
        return withContext(dispatchers.storage) {
            preferences[ONBOARDING_LOAN_USER_INTERESTED_KEY, Boolean::class] ?: true
        }
    }

    override suspend fun setUserNotInterestedInLoan() {
        withContext(dispatchers.storage) {
            preferences[ONBOARDING_LOAN_USER_INTERESTED_KEY] = false
        }
    }

    override suspend fun saveCreditCardOnboardingTimestamp(timestamp: Long) {
        withContext(dispatchers.storage) {
            preferences[ONBOARDING_CREDIT_CARD_TIMESTAMP_KEY] = timestamp
        }
    }

    override suspend fun getCreditCardOnboardingTimestamp(): Long {
        return withContext(dispatchers.storage) {
            preferences[ONBOARDING_CREDIT_CARD_TIMESTAMP_KEY, Long::class] ?: 0L
        }
    }

    companion object {
        private const val ONBOARDING_TIMESTAMP_KEY = "timestamp_when_x5bank_onboarding_was_shown_key"
        private const val ONBOARDING_LOAN_TIMESTAMP_KEY = "timestamp_when_x5bank_loan_onboarding_was_shown_key"
        private const val ONBOARDING_CREDIT_CARD_TIMESTAMP_KEY = "timestamp_when_credit_card_onboarding_was_shown_key"
        private const val ONBOARDING_LOAN_USER_INTERESTED_KEY = "x5bank_loan_onboarding_interested_key"
        private const val IS_BACK_FROM_X5BANK_WEBVIEW_KEY = "is_back_from_x5bank_webview_key"
    }
}