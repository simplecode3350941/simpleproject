package project1.app.x5bank.onboarding

import project1.app.base.ui.presentation.BaseViewModel
import project1.app.x5bank.domain.X5BankAnalyticsInteractor
import project1.app.x5bank.domain.X5BankPromoConstants
import project1.app.x5bank.navigator.X5BankNavigator
import project1.app.x5bank.presentation.model.X5BankUrlType

internal class X5BankCreditCardOnboardingViewModel(
    private val navigator: X5BankNavigator,
    private val analyticsInteractor: X5BankAnalyticsInteractor
) : BaseViewModel(analyticsInteractor) {

    init {
        trackOpenOnboarding()
    }

    fun onButtonNextClick() {
        trackClickButtonNext()
        navigator.toX5Bank(
            bannerCode = X5BankPromoConstants.X5BANK_CREDIT_CARD_POPUP_BANNER_CODE,
            urlType = X5BankUrlType.CREDIT_CARD
        )
    }

    private fun trackClickButtonNext() {
        analyticsInteractor.trackCreditCardPopupMore()
    }

    private fun trackOpenOnboarding() {
        analyticsInteractor.trackCreditCardPopup()
    }

    fun onClosed() {
        analyticsInteractor.trackCreditCardPopupClose()
    }
}