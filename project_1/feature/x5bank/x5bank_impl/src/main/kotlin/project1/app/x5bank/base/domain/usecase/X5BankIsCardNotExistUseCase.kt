package project1.app.x5bank.base.domain.usecase

import project1.app.personal.cards.root.domain.CardsInteractor
import project1.app.personal.cards.root.domain.model.Card
import project1.app.x5bank.base.domain.X5BankRepository

class X5BankIsCardNotExistUseCase(
    private val cardsInteractor: CardsInteractor,
    private val repository: X5BankRepository
) {
    suspend operator fun invoke(): Boolean {
        return if (
            getX5BankCard() == null &&
            getX5BankCardFromTc5Card() == null &&
            getX5BankCardFromVc5Card() == null
        ) {
            !repository.getX5BankIssueState()
        } else {
            false
        }
    }

    private suspend fun getX5BankCard(): Card? {
        return cardsInteractor.getLocalCards().firstOrNull {
            it.type.isX5BankCard()
        }
    }

    private suspend fun getX5BankCardFromTc5Card(): Card? {
        return cardsInteractor.getLocalCards().firstOrNull {
            it.type.isTc5Card() && it.ciFlag == true
        }
    }

    private suspend fun getX5BankCardFromVc5Card(): Card? {
        return cardsInteractor.getLocalCards().firstOrNull {
            it.type.isVc5Card() && it.ciFlag == true
        }
    }
}