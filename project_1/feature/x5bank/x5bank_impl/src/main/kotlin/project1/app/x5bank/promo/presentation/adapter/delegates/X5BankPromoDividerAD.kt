package project1.app.x5bank.promo.presentation.adapter.delegates

import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import project1.app.x5bank.R
import project1.app.x5bank.promo.presentation.model.X5BankPromoUiModel

fun x5BankPromoDividerAD() = adapterDelegate<X5BankPromoUiModel.Divider, Any>(
    R.layout.x5bank_promo_divider_item
) {}