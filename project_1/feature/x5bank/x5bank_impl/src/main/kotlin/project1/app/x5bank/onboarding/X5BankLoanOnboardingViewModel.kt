package project1.app.x5bank.onboarding

import project1.app.base.ui.presentation.BaseViewModel
import project1.app.x5bank.domain.X5BankAnalyticsInteractor
import project1.app.x5bank.domain.X5BankPromoConstants
import project1.app.x5bank.domain.X5BankSetUserNotInterestedInLoanUseCase
import project1.app.x5bank.navigator.X5BankNavigator
import project1.app.x5bank.presentation.model.X5BankUrlType

internal class X5BankLoanOnboardingViewModel(
    private val navigator: X5BankNavigator,
    private val setUserNotInterestedInLoan: X5BankSetUserNotInterestedInLoanUseCase,
    private val analyticsInteractor: X5BankAnalyticsInteractor
) : BaseViewModel(analyticsInteractor) {

    fun onButtonNextClick() {
        navigator.toX5Bank(
            bannerCode = X5BankPromoConstants.X5BANK_LOAN_POPUP_BANNER_CODE,
            urlType = X5BankUrlType.MICROLOAN
        )
    }

    fun onButtonCancelClicked() {
        launchJob {
            setUserNotInterestedInLoan()
            navigator.back()
        }
    }
}