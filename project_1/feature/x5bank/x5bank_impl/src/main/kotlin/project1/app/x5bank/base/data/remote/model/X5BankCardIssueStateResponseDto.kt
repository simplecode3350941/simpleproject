package project1.app.x5bank.base.data.remote.model

import com.google.gson.annotations.SerializedName

data class X5BankCardIssueStateResponseDto(
    @SerializedName("ciflag")
    val isIssued: Boolean?
)