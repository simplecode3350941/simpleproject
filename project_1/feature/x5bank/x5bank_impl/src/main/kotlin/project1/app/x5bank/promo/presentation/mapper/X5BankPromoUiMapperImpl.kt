package project1.app.x5bank.promo.presentation.mapper

import project1.app.base.ui.resources.domain.ResourceInteractor
import project1.app.base.ui.widget.button.model.ButtonBackgroundModel
import project1.app.base.ui.widget.button.model.ButtonFullUiModel
import project1.app.profile.card.create.presentation.detail.model.CreateCardDetailUiModel
import project1.app.x5bank.R
import project1.app.x5bank.domain.X5BankPromoConstants
import project1.app.x5bank.promo.presentation.model.X5BankPromoUiModel

class X5BankPromoUiMapperImpl(
    private val resourceInteractor: ResourceInteractor
) : X5BankPromoUiMapper {

    @Suppress("LongMethod")
    override fun map(): X5BankPromoUiModel.Content {
        return X5BankPromoUiModel.Content(
            ArrayList<Any>().apply {
                add(
                    X5BankPromoUiModel.Header
                )
                add(
                    CreateCardDetailUiModel.Info(
                        text = resourceInteractor.getString(R.string.x5bank_promo_header),
                        fontRes = R.font.project1_sans_design_black,
                        textSizeRes = R.dimen.text32,
                        textColorRes = R.color.white,
                        bottomOffsetRes = R.dimen.offset30
                    )
                )
                add(
                    ButtonFullUiModel(
                        id = X5BankPromoConstants.X5BANK_PROMO_UPGRADE_CARD_UP,
                        background = ButtonBackgroundModel.Color(
                            color = resourceInteractor.getColor(R.color.white)
                        ),
                        textColor = resourceInteractor.getColor((R.color.black)),
                        text = resourceInteractor.getString(R.string.x5bank_promo_button_text)
                    )
                )
                add(
                    X5BankPromoUiModel.Info(
                        text = resourceInteractor.getString(R.string.x5bank_promo_info_1),
                        fontRes = R.font.project1_sans_design_black,
                        fontDescriptionRes = R.font.project1_sans_design_regular,
                        textSizeRes = R.dimen.text18,
                        textDescriptionSizeRes = R.dimen.text16,
                        textColorRes = R.color.white,
                        textDescriptionColorRes = R.color.color_white_alpha_70,
                        drawableRes = R.drawable.x5bank_promo_free,
                        topOffsetRes = R.dimen.offset40
                    )
                )
                add(
                    X5BankPromoUiModel.Info(
                        text = resourceInteractor.getString(R.string.x5bank_promo_info_3),
                        description = resourceInteractor.getString(R.string.x5bank_promo_info_3_description),
                        fontRes = R.font.project1_sans_design_black,
                        fontDescriptionRes = R.font.project1_sans_design_regular,
                        textSizeRes = R.dimen.text18,
                        textDescriptionSizeRes = R.dimen.text16,
                        textColorRes = R.color.white,
                        textDescriptionColorRes = R.color.color_white_alpha_70,
                        drawableRes = R.drawable.x5bank_promo_money,
                        topOffsetRes = R.dimen.offset30
                    )
                )
                add(
                    X5BankPromoUiModel.Info(
                        text = resourceInteractor.getString(R.string.x5bank_promo_info_4),
                        fontRes = R.font.project1_sans_design_black,
                        fontDescriptionRes = R.font.project1_sans_design_regular,
                        textSizeRes = R.dimen.text18,
                        textDescriptionSizeRes = R.dimen.text16,
                        textColorRes = R.color.white,
                        textDescriptionColorRes = R.color.color_white_alpha_70,
                        drawableRes = R.drawable.x5bank_promo_loyalty,
                        topOffsetRes = R.dimen.offset30,
                        bottomOffsetRes = R.dimen.offset40
                    )
                )
                add(
                    ButtonFullUiModel(
                        id = X5BankPromoConstants.X5BANK_PROMO_UPGRADE_CARD_DOWN,
                        background = ButtonBackgroundModel.Color(
                            color = resourceInteractor.getColor(R.color.white)
                        ),
                        textColor = resourceInteractor.getColor((R.color.black)),
                        text = resourceInteractor.getString(R.string.x5bank_promo_button_text)
                    )
                )
                add(
                    X5BankPromoUiModel.MenuItem(
                        id = X5BankPromoConstants.X5BANK_PROMO_CARD_DETAIL,
                        textColorRes = R.color.white,
                        textSizeRes = R.dimen.text16,
                        fontRes = R.font.project1_sans_design_semibold,
                        text = resourceInteractor.getString(R.string.x5bank_promo_card_detail),
                        topOffsetRes = R.dimen.offset60
                    )
                )
                add(
                    X5BankPromoUiModel.Divider
                )
                add(
                    X5BankPromoUiModel.MenuItem(
                        id = X5BankPromoConstants.X5BANK_PROMO_CALLBACK,
                        textColorRes = R.color.white,
                        textSizeRes = R.dimen.text16,
                        fontRes = R.font.project1_sans_design_semibold,
                        text = resourceInteractor.getString(R.string.x5bank_promo_callback)
                    )
                )
                add(
                    X5BankPromoUiModel.Footer
                )
            }
        )
    }
}