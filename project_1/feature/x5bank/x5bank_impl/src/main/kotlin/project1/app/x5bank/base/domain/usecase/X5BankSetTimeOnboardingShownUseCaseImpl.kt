package project1.app.x5bank.base.domain.usecase

import project1.app.x5bank.base.domain.X5BankRepository
import project1.app.x5bank.domain.X5BankSetTimeOnboardingShownUseCase

class X5BankSetTimeOnboardingShownUseCaseImpl(
    private val repository: X5BankRepository
) : X5BankSetTimeOnboardingShownUseCase {
    override suspend operator fun invoke() = repository.saveOnboardingTimestamp(System.currentTimeMillis())
}