package project1.app.x5bank.base.domain

import project1.app.base.segmentation.base.domain.SegmentationToggleProvider
import project1.app.base.segmentation.base.domain.model.SegmentationToggle

interface X5BankSegmentationRepository : SegmentationToggleProvider {
    fun getX5BankToggle(): SegmentationToggle
    fun getX5BankNoLandingToggle(): SegmentationToggle
    fun getX5BankLoanToggle(): SegmentationToggle
    fun getX5BankCreditCardToggle(): SegmentationToggle
}