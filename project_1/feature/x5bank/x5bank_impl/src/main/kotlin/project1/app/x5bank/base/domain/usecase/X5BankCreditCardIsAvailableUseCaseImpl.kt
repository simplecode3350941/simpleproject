package project1.app.x5bank.base.domain.usecase

import project1.app.base.segmentation.base.domain.ToggleGetValueUseCase
import project1.app.base.segmentation.base.domain.model.SegmentationToggleValueModel
import project1.app.x5bank.base.domain.X5BankSegmentationRepository
import project1.app.x5bank.domain.X5BankCreditCardIsAvailableUseCase

internal class X5BankCreditCardIsAvailableUseCaseImpl(
    private val segmentationRepository: X5BankSegmentationRepository,
    private val getToggleValue: ToggleGetValueUseCase
) : X5BankCreditCardIsAvailableUseCase {

    override suspend operator fun invoke(): Boolean {
        return getX5BankCreditCardToggleValue().isEnabled()
    }

    private suspend fun getX5BankCreditCardToggleValue(): SegmentationToggleValueModel {
        return getToggleValue(segmentationRepository.getX5BankCreditCardToggle())
    }
}