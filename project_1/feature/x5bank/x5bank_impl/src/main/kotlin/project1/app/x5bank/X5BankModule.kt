package project1.app.x5bank

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.bind
import org.koin.dsl.module
import project1.app.base.segmentation.base.domain.SegmentationToggleProvider
import project1.app.base.settings.domain.SettingsRepository
import project1.app.main.network.provider.RetrofitProvider
import project1.app.personal.x5bank.base.domain.X5BankGetCardAsFlowUseCase
import project1.app.x5bank.base.data.X5BankRepositoryImpl
import project1.app.x5bank.base.data.X5BankSegmentationRepositoryImpl
import project1.app.x5bank.base.data.remote.X5BankApi
import project1.app.x5bank.base.domain.X5BankRepository
import project1.app.x5bank.base.domain.X5BankSegmentationRepository
import project1.app.x5bank.base.domain.usecase.X5BankCreditCardGetUrlUseCaseImpl
import project1.app.x5bank.base.domain.usecase.X5BankCreditCardIsAvailableUseCaseImpl
import project1.app.x5bank.base.domain.usecase.X5BankCreditCardIsPostponeTimePassedUseCaseImpl
import project1.app.x5bank.base.domain.usecase.X5BankCreditCardSetTimeOnboardingShownUseCaseImpl
import project1.app.x5bank.base.domain.usecase.X5BankGetAgreementUseCase
import project1.app.x5bank.base.domain.usecase.X5BankGetCardAsFlowUseCaseImpl
import project1.app.x5bank.base.domain.usecase.X5BankGetNoLandingUrlUseCaseImpl
import project1.app.x5bank.base.domain.usecase.X5BankGetUrlUseCase
import project1.app.x5bank.base.domain.usecase.X5BankIsAvailableUseCaseImpl
import project1.app.x5bank.base.domain.usecase.X5BankIsCardNotExistUseCase
import project1.app.x5bank.base.domain.usecase.X5BankIsNewFlowAvailableUseCaseImpl
import project1.app.x5bank.base.domain.usecase.X5BankIsOldFlowAvailableUseCase
import project1.app.x5bank.base.domain.usecase.X5BankIsPostponeTimePassedUseCaseImpl
import project1.app.x5bank.base.domain.usecase.X5BankLoanGetUrlUseCaseImpl
import project1.app.x5bank.base.domain.usecase.X5BankLoanIsAvailableUseCaseImpl
import project1.app.x5bank.base.domain.usecase.X5BankLoanIsPostponeTimePassedUseCaseImpl
import project1.app.x5bank.base.domain.usecase.X5BankLoanSetTimeOnboardingShownUseCaseImpl
import project1.app.x5bank.base.domain.usecase.X5BankSetCardIssuedUseCase
import project1.app.x5bank.base.domain.usecase.X5BankSetTimeOnboardingShownUseCaseImpl
import project1.app.x5bank.base.domain.usecase.X5BankSetUserNotInterestedInLoanUseCaseImpl
import project1.app.x5bank.base.domain.usecase.X5bankIsBackFromWebviewUseCaseImpl
import project1.app.x5bank.base.domain.usecase.X5bankSetIsBackFromWebviewFlagUseCaseImpl
import project1.app.x5bank.domain.X5BankAnalyticsInteractor
import project1.app.x5bank.domain.X5BankCreditCardGetUrlUseCase
import project1.app.x5bank.domain.X5BankCreditCardIsAvailableUseCase
import project1.app.x5bank.domain.X5BankCreditCardIsPostponeTimePassedUseCase
import project1.app.x5bank.domain.X5BankCreditCardSetTimeOnboardingShownUseCase
import project1.app.x5bank.domain.X5BankGetNoLandingUrlUseCase
import project1.app.x5bank.domain.X5BankIsAvailableUseCase
import project1.app.x5bank.domain.X5BankIsNewFlowAvailableUseCase
import project1.app.x5bank.domain.X5BankIsPostponeTimePassedUseCase
import project1.app.x5bank.domain.X5BankLoanGetUrlUseCase
import project1.app.x5bank.domain.X5BankLoanIsAvailableUseCase
import project1.app.x5bank.domain.X5BankLoanIsPostponeTimePassedUseCase
import project1.app.x5bank.domain.X5BankLoanSetTimeOnboardingShownUseCase
import project1.app.x5bank.domain.X5BankSetTimeOnboardingShownUseCase
import project1.app.x5bank.domain.X5BankSetUserNotInterestedInLoanUseCase
import project1.app.x5bank.domain.X5bankIsBackFromWebviewUseCase
import project1.app.x5bank.domain.X5bankSetIsBackFromWebviewFlagUseCase
import project1.app.x5bank.home.domain.X5BankAnalyticsInteractorImpl
import project1.app.x5bank.navigation.X5BankNavigatorImpl
import project1.app.x5bank.navigation.X5BankScreenProviderImpl
import project1.app.x5bank.navigator.X5BankNavigator
import project1.app.x5bank.navigator.X5BankScreenProvider
import project1.app.x5bank.onboarding.X5BankCreditCardOnboardingViewModel
import project1.app.x5bank.onboarding.X5BankLoanOnboardingViewModel
import project1.app.x5bank.onboarding.X5BankOnboardingViewModel
import project1.app.x5bank.promo.presentation.X5BankPromoParameters
import project1.app.x5bank.promo.presentation.X5BankPromoViewModel
import project1.app.x5bank.promo.presentation.mapper.X5BankPromoUiMapper
import project1.app.x5bank.promo.presentation.mapper.X5BankPromoUiMapperImpl
import project1.app.x5bank.webview.X5BankCallback
import project1.app.x5bank.webview.X5BankWebClient
import project1.app.x5bank.webview.presentation.X5BankParameters
import project1.app.x5bank.webview.presentation.X5BankViewModel
import retrofit2.Retrofit
import retrofit2.create

@Suppress("LongMethod")
fun x5BankModule() = module {
    single(X5BANK_QUALIFIER) {
        RetrofitProvider(
            okHttpClient = get(),
            converterFactory = get()
        ).provide(get<SettingsRepository>().x5BankUrl)
    }

    factory<X5BankApi> { get<Retrofit>(X5BANK_QUALIFIER).create() }
    factory<X5BankNavigator> { X5BankNavigatorImpl(router = get()) }
    factory { (callback: X5BankCallback) -> X5BankWebClient(callback = callback) }
    factory<X5BankPromoUiMapper> { X5BankPromoUiMapperImpl(resourceInteractor = get()) }

    factory { X5BankGetAgreementUseCase() }
    factory<X5BankGetCardAsFlowUseCase> { X5BankGetCardAsFlowUseCaseImpl(cardsInteractor = get()) }
    factory<X5BankIsAvailableUseCase> {
        X5BankIsAvailableUseCaseImpl(
            isX5BankOldFlowAvailable = get(),
            isX5BankNewFlowAvailable = get(),
            isX5BankCardNotExist = get()
        )
    }
    factory { X5BankGetUrlUseCase(repository = get()) }
    factory<X5BankGetNoLandingUrlUseCase> { X5BankGetNoLandingUrlUseCaseImpl(repository = get()) }
    factory<X5BankLoanGetUrlUseCase> { X5BankLoanGetUrlUseCaseImpl(repository = get()) }

    factory {
        X5BankIsOldFlowAvailableUseCase(
            segmentationRepository = get(),
            getToggleValue = get()
        )
    }
    factory<X5BankIsNewFlowAvailableUseCase> {
        X5BankIsNewFlowAvailableUseCaseImpl(
            segmentationRepository = get(),
            getToggleValue = get()
        )
    }
    factory {
        X5BankIsCardNotExistUseCase(
            cardsInteractor = get(),
            repository = get()
        )
    }
    factory<X5BankIsPostponeTimePassedUseCase> { X5BankIsPostponeTimePassedUseCaseImpl(repository = get()) }
    factory<X5BankSetTimeOnboardingShownUseCase> { X5BankSetTimeOnboardingShownUseCaseImpl(repository = get()) }
    factory { X5BankSetCardIssuedUseCase(repository = get()) }

    factory<X5BankLoanIsAvailableUseCase> {
        X5BankLoanIsAvailableUseCaseImpl(
            repository = get(),
            segmentationRepository = get(),
            getToggleValue = get()
        )
    }
    factory<X5BankLoanIsPostponeTimePassedUseCase> { X5BankLoanIsPostponeTimePassedUseCaseImpl(repository = get()) }
    factory<X5BankLoanSetTimeOnboardingShownUseCase> { X5BankLoanSetTimeOnboardingShownUseCaseImpl(repository = get()) }
    factory<X5BankSetUserNotInterestedInLoanUseCase> { X5BankSetUserNotInterestedInLoanUseCaseImpl(repository = get()) }

    factory<X5BankCreditCardIsAvailableUseCase> {
        X5BankCreditCardIsAvailableUseCaseImpl(
            segmentationRepository = get(),
            getToggleValue = get()
        )
    }
    factory<X5BankCreditCardIsPostponeTimePassedUseCase> {
        X5BankCreditCardIsPostponeTimePassedUseCaseImpl(repository = get())
    }
    factory<X5BankCreditCardSetTimeOnboardingShownUseCase> {
        X5BankCreditCardSetTimeOnboardingShownUseCaseImpl(repository = get())
    }
    factory<X5BankCreditCardGetUrlUseCase> {
        X5BankCreditCardGetUrlUseCaseImpl(repository = get())
    }

    factory<X5BankRepository> {
        X5BankRepositoryImpl(
            api = get(),
            preferences = get(),
            dispatchers = get()
        )
    }
    factory<X5BankSegmentationRepository> {
        X5BankSegmentationRepositoryImpl()
    } bind SegmentationToggleProvider::class
    factory<X5BankAnalyticsInteractor> { X5BankAnalyticsInteractorImpl(analyticsInteractor = get()) }
    factory<X5bankSetIsBackFromWebviewFlagUseCase> {
        X5bankSetIsBackFromWebviewFlagUseCaseImpl(x5bankRepository = get())
    }
    factory<X5bankIsBackFromWebviewUseCase> { X5bankIsBackFromWebviewUseCaseImpl(x5bankRepository = get()) }

    factory<X5BankScreenProvider> { X5BankScreenProviderImpl() }

    viewModel { (parameters: X5BankParameters) ->
        X5BankViewModel(
            parameters = parameters,
            getX5BankUrl = get(),
            getX5BankNoLandingUrl = get(),
            setCardIssued = get(),
            getCreditCardUrl = get(),
            navigator = get(),
            setIsBackFromX5bankWebviewFlag = get(),
            analyticsInteractor = get(),
            isX5BankNewFlowAvailable = get(),
            getX5BankLoanUrl = get()
        )
    }
    viewModel { (parameters: X5BankPromoParameters) ->
        X5BankPromoViewModel(
            parameters,
            navigator = get(),
            x5BankPromoUiMapper = get(),
            analyticsInteractor = get(),
            getX5BankAgreement = get()
        )
    }
    viewModel {
        X5BankOnboardingViewModel(
            navigator = get(),
            analyticsInteractor = get(),
            isX5BankNewFlowAvailable = get()
        )
    }
    viewModel {
        X5BankLoanOnboardingViewModel(
            navigator = get(),
            setUserNotInterestedInLoan = get(),
            analyticsInteractor = get()
        )
    }
    viewModel {
        X5BankCreditCardOnboardingViewModel(
            navigator = get(),
            analyticsInteractor = get()
        )
    }
}