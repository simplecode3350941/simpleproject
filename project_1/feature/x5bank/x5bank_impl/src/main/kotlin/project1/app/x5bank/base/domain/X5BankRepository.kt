package project1.app.x5bank.base.domain

interface X5BankRepository {
    suspend fun loadUrl(bannerCode: String?): String?
    suspend fun loadNoLandingUrl(bannerCode: String?): String?
    suspend fun loadCreditCardUrl(bannerCode: String?): String?
    suspend fun loadLoanUrl(bannerCode: String?): String?
    suspend fun getX5BankIssueState(): Boolean
    suspend fun saveOnboardingTimestamp(timestamp: Long)
    suspend fun getOnboardingTimestamp(): Long
    suspend fun isBackFromX5bankWebview(): Boolean
    suspend fun setIsBackFromX5bankWebviewFlag(isBackFromX5bankWebviewFlagValue: Boolean)
    suspend fun setX5BankCardIssued(): Boolean
    suspend fun saveLoanOnboardingTimestamp(timestamp: Long)
    suspend fun getLoanOnboardingTimestamp(): Long
    suspend fun isUserInterestedInLoan(): Boolean
    suspend fun setUserNotInterestedInLoan()
    suspend fun saveCreditCardOnboardingTimestamp(timestamp: Long)
    suspend fun getCreditCardOnboardingTimestamp(): Long
}