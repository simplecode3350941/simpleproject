package project1.app.x5bank.base.domain.usecase

import project1.app.base.ui.webview.UrlWithHeaders
import project1.app.x5bank.base.domain.X5BankRepository
import project1.app.x5bank.domain.X5BankGetNoLandingUrlUseCase

class X5BankGetNoLandingUrlUseCaseImpl(
    private val repository: X5BankRepository
) : X5BankGetNoLandingUrlUseCase {
    override suspend operator fun invoke(bannerCode: String?): UrlWithHeaders {
        val url = repository.loadNoLandingUrl(bannerCode)
        return UrlWithHeaders(
            requireNotNull(url)
        )
    }
}