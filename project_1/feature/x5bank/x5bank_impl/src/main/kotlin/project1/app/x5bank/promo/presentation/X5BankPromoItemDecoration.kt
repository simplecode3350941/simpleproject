package project1.app.x5bank.promo.presentation

import android.content.Context
import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import project1.app.base.ui.widget.button.model.ButtonFullUiModel
import project1.app.base.ui.widget.recycler.decoration.ItemTypeOffsetDecoration
import project1.app.personal.R
import project1.app.profile.card.create.presentation.detail.model.CreateCardDetailUiModel
import project1.app.x5bank.promo.presentation.model.X5BankPromoUiModel

class X5BankPromoItemDecoration(private val context: Context) : ItemTypeOffsetDecoration() {

    private val offset40 = context.resources.getDimensionPixelOffset(R.dimen.offset40)
    private val offset50 = context.resources.getDimensionPixelOffset(R.dimen.offset50)
    private val offset25 = context.resources.getDimensionPixelOffset(R.dimen.offset25)
    private val offset20 = context.resources.getDimensionPixelOffset(R.dimen.offset20)

    override fun getItemOffsets(outRect: Rect, item: Any?, parent: RecyclerView) {
        when (item) {
            is X5BankPromoUiModel.Header -> outRect.set(offset20, offset40, offset20, 0)
            is X5BankPromoUiModel.Divider -> outRect.set(offset20, 0, offset20, 0)
            is X5BankPromoUiModel.Footer -> outRect.set(offset20, offset25, offset20, offset50)
            is ButtonFullUiModel -> outRect.set(offset20, 0, offset20, 0)
            is X5BankPromoUiModel.Info -> {
                val topOffset = context.resources.getDimensionPixelOffset(item.topOffsetRes)
                val bottomOffset = context.resources.getDimensionPixelOffset(item.bottomOffsetRes)
                outRect.set(offset20, topOffset, offset20, bottomOffset)
            }
            is X5BankPromoUiModel.MenuItem -> {
                val topOffset = context.resources.getDimensionPixelOffset(item.topOffsetRes)
                val bottomOffset = context.resources.getDimensionPixelOffset(item.bottomOffsetRes)
                outRect.set(offset20, topOffset, offset20, bottomOffset)
            }
            is CreateCardDetailUiModel.Info -> {
                val topOffset = context.resources.getDimensionPixelOffset(item.topOffsetRes)
                val bottomOffset = context.resources.getDimensionPixelOffset(item.bottomOffsetRes)
                outRect.set(offset20, topOffset, offset20, bottomOffset)
            }
        }
    }
}