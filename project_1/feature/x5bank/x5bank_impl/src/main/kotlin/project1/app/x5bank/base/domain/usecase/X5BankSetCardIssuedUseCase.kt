package project1.app.x5bank.base.domain.usecase

import project1.app.x5bank.base.domain.X5BankRepository

class X5BankSetCardIssuedUseCase(
    private val repository: X5BankRepository
) {
    suspend operator fun invoke(): Boolean {
        return repository.setX5BankCardIssued()
    }
}