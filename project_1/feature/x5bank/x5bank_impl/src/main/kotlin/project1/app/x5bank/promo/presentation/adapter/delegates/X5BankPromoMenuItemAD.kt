package project1.app.x5bank.promo.presentation.adapter.delegates

import android.util.TypedValue
import androidx.core.content.res.ResourcesCompat
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import project1.app.base.ui.extension.getColorKtx
import project1.app.x5bank.databinding.X5bankMenuItemBinding
import project1.app.x5bank.promo.presentation.model.X5BankPromoUiModel

fun x5BankPromoMenuItemAD(
    onClick: (item: X5BankPromoUiModel.MenuItem) -> Unit
) = adapterDelegateViewBinding<X5BankPromoUiModel.MenuItem, Any, X5bankMenuItemBinding>(
    { layoutInflater, parent ->
        X5bankMenuItemBinding.inflate(layoutInflater, parent, false)
    }) {
    binding.vText.setOnClickListener {
        onClick.invoke(item)
    }
    bind {
        with(binding) {
            with(vText) {
                text = item.text
                setTextColor(context.getColorKtx(item.textColorRes))
                setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(item.textSizeRes))
                typeface = ResourcesCompat.getFont(context, item.fontRes)
                gravity = item.gravity
            }
        }
    }
}