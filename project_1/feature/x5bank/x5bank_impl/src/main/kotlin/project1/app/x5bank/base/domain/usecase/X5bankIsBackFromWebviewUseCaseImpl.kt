package project1.app.x5bank.base.domain.usecase

import project1.app.x5bank.base.domain.X5BankRepository
import project1.app.x5bank.domain.X5bankIsBackFromWebviewUseCase

class X5bankIsBackFromWebviewUseCaseImpl(
    private val x5bankRepository: X5BankRepository
) : X5bankIsBackFromWebviewUseCase {
    override suspend operator fun invoke(): Boolean {
        return x5bankRepository.isBackFromX5bankWebview()
    }
}