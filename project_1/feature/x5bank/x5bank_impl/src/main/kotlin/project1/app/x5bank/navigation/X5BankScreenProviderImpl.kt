package project1.app.x5bank.navigation

import project1.app.x5bank.X5BankScreens
import project1.app.x5bank.navigator.X5BankScreenProvider
import project1.app.x5bank.presentation.model.X5BankUrlType
import project1.app.x5bank.promo.presentation.X5BankPromoParameters
import project1.app.x5bank.webview.presentation.X5BankParameters

internal class X5BankScreenProviderImpl : X5BankScreenProvider {
    override fun provideX5BankPromoScreen(bannerCode: String?) = X5BankScreens.X5BankPromo(
        X5BankPromoParameters(bannerCode)
    )

    override fun provideX5BankScreen(bannerCode: String?, urlType: X5BankUrlType) = X5BankScreens.X5Bank(
        X5BankParameters(bannerCode, urlType)
    )

    override fun provideX5BankOnboardingScreen() = X5BankScreens.X5BankOnboarding
}