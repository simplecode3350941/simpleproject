package project1.app.x5bank.base.domain.usecase

import project1.app.x5bank.base.domain.X5BankRepository
import project1.app.x5bank.domain.X5BankIsPostponeTimePassedUseCase
import java.util.concurrent.TimeUnit

class X5BankIsPostponeTimePassedUseCaseImpl(
    private val repository: X5BankRepository
) : X5BankIsPostponeTimePassedUseCase {
    override suspend operator fun invoke(): Boolean {
        val diff = System.currentTimeMillis() - repository.getOnboardingTimestamp()
        return diff > POSTPONE_TIME
    }

    companion object {
        private val POSTPONE_TIME = TimeUnit.DAYS.toMillis(3L)
    }
}