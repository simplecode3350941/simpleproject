package project1.app.x5bank.onboarding

import android.os.Bundle
import android.view.View
import org.koin.androidx.viewmodel.ext.android.viewModel
import project1.app.base.ui.presentation.binding.viewBindingBS
import project1.app.base.ui.presentation.bottomsheet.CircularBottomSheetFragment
import project1.app.x5bank.R
import project1.app.x5bank.databinding.X5bankLoanOnboardingFragmentBinding

internal class X5BankLoanOnboardingFragment : CircularBottomSheetFragment(), View.OnClickListener {
    override val layoutResId = R.layout.x5bank_loan_onboarding_fragment
    override val viewModel by viewModel<X5BankLoanOnboardingViewModel>()
    override val backgroundColor = R.color.x5bank_loan_onboarding_background

    private val binding by viewBindingBS(X5bankLoanOnboardingFragmentBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setDraggerVisibility(true)
        binding.vButtonNext.setOnClickListener(this)
        binding.vButtonCancel.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            binding.vButtonNext.id -> viewModel.onButtonNextClick()
            binding.vButtonCancel.id -> viewModel.onButtonCancelClicked()
        }
    }
}