package project1.app.x5bank.base.domain.usecase

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import project1.app.personal.cards.root.domain.CardsInteractor
import project1.app.personal.cards.root.domain.model.getX5BankCard
import project1.app.personal.cards.root.domain.model.getX5BankCardFromTc5Card
import project1.app.personal.cards.root.domain.model.getX5BankCardFromVc5Card
import project1.app.personal.cards.root.domain.model.isX5BankCardReleased
import project1.app.personal.x5bank.base.domain.X5BankGetCardAsFlowUseCase
import project1.app.personal.x5bank.base.domain.model.X5BankCard

class X5BankGetCardAsFlowUseCaseImpl(
    private val cardsInteractor: CardsInteractor
) : X5BankGetCardAsFlowUseCase {
    override operator fun invoke(): Flow<X5BankCard?> {
        return cardsInteractor.getLocalCardsAsFlow().map { cards ->
            if (cards.getX5BankCard() != null) {
                cards.getX5BankCard()
            } else {
                val tc5Card = cards.getX5BankCardFromTc5Card()
                if (tc5Card != null && tc5Card.isX5BankCardReleased()) {
                    cards.getX5BankCardFromTc5Card()
                } else {
                    val vc5Card = cards.getX5BankCardFromVc5Card()
                    if (vc5Card != null && vc5Card.isX5BankCardReleased()) {
                        cards.getX5BankCardFromVc5Card()
                    } else { null }
                }
            }
        }
    }
}