package project1.app.x5bank.webview.presentation

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import project1.app.x5bank.presentation.model.X5BankUrlType

@Parcelize
data class X5BankParameters(
    val bannerCode: String? = null,
    val urlType: X5BankUrlType = X5BankUrlType.CARD_ISSUE
) : Parcelable