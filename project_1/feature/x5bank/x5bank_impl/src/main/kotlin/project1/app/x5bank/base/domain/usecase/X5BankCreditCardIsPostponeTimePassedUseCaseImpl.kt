package project1.app.x5bank.base.domain.usecase

import project1.app.x5bank.base.domain.X5BankRepository
import project1.app.x5bank.domain.X5BankCreditCardIsPostponeTimePassedUseCase
import java.util.concurrent.TimeUnit

internal class X5BankCreditCardIsPostponeTimePassedUseCaseImpl(
    private val repository: X5BankRepository
) : X5BankCreditCardIsPostponeTimePassedUseCase {
    override suspend operator fun invoke(): Boolean {
        val diff = System.currentTimeMillis() - repository.getCreditCardOnboardingTimestamp()
        return diff > POSTPONE_TIME
    }

    companion object {
        private val POSTPONE_TIME = TimeUnit.DAYS.toMillis(3L)
    }
}