package project1.app.x5bank.onboarding

import project1.app.base.ui.presentation.BaseViewModel
import project1.app.x5bank.domain.X5BankAnalyticsInteractor
import project1.app.x5bank.domain.X5BankIsNewFlowAvailableUseCase
import project1.app.x5bank.domain.X5BankPromoConstants
import project1.app.x5bank.navigator.X5BankNavigator

class X5BankOnboardingViewModel(
    private val navigator: X5BankNavigator,
    private val analyticsInteractor: X5BankAnalyticsInteractor,
    private val isX5BankNewFlowAvailable: X5BankIsNewFlowAvailableUseCase
) : BaseViewModel(analyticsInteractor) {

    init {
        trackOpenOnboarding()
    }

    fun onButtonNextClick() {
        trackClickButtonNext()
        launchJob {
            if (isX5BankNewFlowAvailable()) {
                navigator.toX5Bank(X5BankPromoConstants.X5BANK_PROMO_POPUP_BANNER_CODE)
            } else {
                navigator.toX5BankPromo()
            }
        }
    }

    fun onClosed() {
        analyticsInteractor.trackBankPopupClose()
    }

    private fun trackOpenOnboarding() {
        analyticsInteractor.trackBankPopup()
    }

    private fun trackClickButtonNext() {
        analyticsInteractor.trackBankPopupMore()
    }

    private fun trackClickButtonPostpone() {
        analyticsInteractor.trackBankPopupLater()
    }
}