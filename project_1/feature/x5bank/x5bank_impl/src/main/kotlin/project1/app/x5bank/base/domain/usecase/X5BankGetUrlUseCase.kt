package project1.app.x5bank.base.domain.usecase

import project1.app.base.ui.webview.UrlWithHeaders
import project1.app.x5bank.base.domain.X5BankRepository

class X5BankGetUrlUseCase(
    private val repository: X5BankRepository
) {
    suspend operator fun invoke(bannerCode: String?): UrlWithHeaders {
        val url = repository.loadUrl(bannerCode)
        return UrlWithHeaders(
            requireNotNull(url)
        )
    }
}