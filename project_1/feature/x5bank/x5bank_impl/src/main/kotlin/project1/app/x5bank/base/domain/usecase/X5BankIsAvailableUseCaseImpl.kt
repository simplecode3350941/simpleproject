package project1.app.x5bank.base.domain.usecase

import project1.app.x5bank.domain.X5BankIsAvailableUseCase
import project1.app.x5bank.domain.X5BankIsNewFlowAvailableUseCase

class X5BankIsAvailableUseCaseImpl(
    private val isX5BankOldFlowAvailable: X5BankIsOldFlowAvailableUseCase,
    private val isX5BankNewFlowAvailable: X5BankIsNewFlowAvailableUseCase,
    private val isX5BankCardNotExist: X5BankIsCardNotExistUseCase
) : X5BankIsAvailableUseCase {
    override suspend fun invoke(): Boolean {
        val isOldFlowActive = isX5BankOldFlowAvailable()
        val isNewFlowActive = isX5BankNewFlowAvailable()
        val isX5BankCardNotExist = isX5BankCardNotExist()
        return when {
            (isOldFlowActive && !isNewFlowActive && isX5BankCardNotExist) ||
                (!isOldFlowActive && isNewFlowActive && isX5BankCardNotExist) ||
                (isOldFlowActive && isNewFlowActive && isX5BankCardNotExist)
            -> {
                true
            }
            else -> { false }
        }
    }
}