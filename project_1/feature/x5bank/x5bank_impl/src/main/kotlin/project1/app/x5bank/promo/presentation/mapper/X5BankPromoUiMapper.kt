package project1.app.x5bank.promo.presentation.mapper

import project1.app.x5bank.promo.presentation.model.X5BankPromoUiModel

interface X5BankPromoUiMapper {
    fun map(): X5BankPromoUiModel.Content
}