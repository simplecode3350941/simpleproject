package project1.app.x5bank.promo.presentation.adapter.delegates

import android.util.TypedValue
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import project1.app.base.ui.extension.getColorKtx
import project1.app.base.ui.extension.setJustifyText
import project1.app.x5bank.databinding.X5bankInfoItemBinding
import project1.app.x5bank.promo.presentation.model.X5BankPromoUiModel

fun x5BankPromoInfoAD() = adapterDelegateViewBinding<X5BankPromoUiModel.Info, Any, X5bankInfoItemBinding>(
    { layoutInflater, parent ->
        X5bankInfoItemBinding.inflate(layoutInflater, parent, false)
    }) {
    binding.vText.setJustifyText()
    bind {
        with(binding) {
            with(vText) {
                text = item.text
                setTextColor(context.getColorKtx(item.textColorRes))
                setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(item.textSizeRes))
                typeface = ResourcesCompat.getFont(context, item.fontRes)
                gravity = item.gravity
            }
            with(vTextDescription) {
                text = item.description
                setTextColor(context.getColorKtx(item.textDescriptionColorRes))
                setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(item.textDescriptionSizeRes))
                typeface = ResourcesCompat.getFont(context, item.fontDescriptionRes)
                gravity = item.gravity
            }
            with(vImage) {
                isVisible = item.drawableRes != null
                item.drawableRes?.let { it ->
                    setImageResource(it)
                }
            }
        }
    }
}