package project1.app.x5bank.promo.presentation.adapter.delegates

import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import project1.app.base.ui.extension.setOnClickListener
import project1.app.x5bank.databinding.X5bankHeaderItemBinding
import project1.app.x5bank.promo.presentation.model.X5BankPromoUiModel

fun x5BankPromoHeaderAD(
    onCloseClick: () -> Unit
) = adapterDelegateViewBinding<X5BankPromoUiModel.Header, Any, X5bankHeaderItemBinding>(
    { layoutInflater, parent -> X5bankHeaderItemBinding.inflate(layoutInflater, parent, false) }
) {
    binding.vClose.setOnClickListener(onCloseClick)
}