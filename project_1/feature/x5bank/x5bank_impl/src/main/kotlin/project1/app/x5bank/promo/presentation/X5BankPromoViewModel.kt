package project1.app.x5bank.promo.presentation

import androidx.lifecycle.MutableLiveData
import project1.app.base.ui.presentation.BaseViewModel
import project1.app.base.ui.widget.button.model.ButtonFullUiModel
import project1.app.x5bank.base.domain.usecase.X5BankGetAgreementUseCase
import project1.app.x5bank.domain.X5BankAnalyticsInteractor
import project1.app.x5bank.domain.X5BankPromoConstants
import project1.app.x5bank.navigator.X5BankNavigator
import project1.app.x5bank.promo.presentation.mapper.X5BankPromoUiMapper
import project1.app.x5bank.promo.presentation.model.X5BankPromoUiModel

class X5BankPromoViewModel(
    private val parameters: X5BankPromoParameters,
    private val navigator: X5BankNavigator,
    private val x5BankPromoUiMapper: X5BankPromoUiMapper,
    private val analyticsInteractor: X5BankAnalyticsInteractor,
    private val getX5BankAgreement: X5BankGetAgreementUseCase
) : BaseViewModel(analyticsInteractor) {

    val content = MutableLiveData<X5BankPromoUiModel.Content>()

    init {
        content.value = getInitContent()
        trackOpenPromoFragment()
    }

    private fun getInitContent() = x5BankPromoUiMapper.map()

    fun onButtonClick(
        button: ButtonFullUiModel
    ) {
        when (button.id) {
            X5BankPromoConstants.X5BANK_PROMO_UPGRADE_CARD_UP -> {
                trackUpdateCardUpButtonClick()
                navigator.toX5Bank(parameters.bannerCode)
            }
            X5BankPromoConstants.X5BANK_PROMO_UPGRADE_CARD_DOWN -> {
                trackUpdateCardDownButtonClick()
                navigator.toX5Bank(parameters.bannerCode)
            }
        }
    }

    fun onMenuItemClick(
        menuItem: X5BankPromoUiModel.MenuItem
    ) {
        when (menuItem.id) {
            X5BankPromoConstants.X5BANK_PROMO_CALLBACK -> {
                trackContactUsButtonClick()
                navigator.toCallBack(X5BankPromoConstants.X5BANK_PROMO_PHONE_NUMBER)
            }
            X5BankPromoConstants.X5BANK_PROMO_CARD_DETAIL -> {
                trackMoreInfoAboutCardButtonClick()
                navigator.toDocument(getX5BankAgreement())
            }
        }
    }

    fun onCloseClick() {
        trackClosePromoFragment()
        navigator.back()
    }

    private fun trackOpenPromoFragment() {
        analyticsInteractor.trackBankScreen()
    }

    private fun trackClosePromoFragment() {
        analyticsInteractor.trackBankScreenClose()
    }

    private fun trackUpdateCardUpButtonClick() {
        analyticsInteractor.trackBankScreenUpdateUp()
    }

    private fun trackUpdateCardDownButtonClick() {
        analyticsInteractor.trackBankScreenUpdateDown()
    }

    private fun trackMoreInfoAboutCardButtonClick() {
        analyticsInteractor.trackBankScreenMore()
    }

    private fun trackMoreInfoAboutPromotionButtonClick() {
        analyticsInteractor.trackBankScreenPromotionMore()
    }

    private fun trackContactUsButtonClick() {
        analyticsInteractor.trackBankScreenContact()
    }
}