package project1.app.x5bank.promo.presentation

import android.os.Bundle
import android.view.View
import org.koin.androidx.viewmodel.ext.android.viewModel
import project1.app.base.ui.extension.disableItemChangeAnimation
import project1.app.base.ui.extension.parcelableParametersOf
import project1.app.base.ui.presentation.BaseFragment
import project1.app.base.ui.presentation.binding.viewBinding
import project1.app.base.ui.util.StatusBarColor
import project1.app.base.util.lazyUnsynchronized
import project1.app.x5bank.R
import project1.app.x5bank.databinding.X5bankPromoFragmentBinding
import project1.app.x5bank.promo.presentation.adapter.X5BankPromoAdapter
import project1.app.x5bank.promo.presentation.model.X5BankPromoUiModel

class X5BankPromoFragment : BaseFragment() {
    override val statusBarColor = StatusBarColor.TRANSPARENT_DARK
    override val layoutResId = R.layout.x5bank_promo_fragment
    override val viewModel by viewModel<X5BankPromoViewModel> {
        parcelableParametersOf<X5BankPromoParameters>()
    }

    private val binding by viewBinding(X5bankPromoFragmentBinding::bind)

    private val adapter by lazyUnsynchronized {
        X5BankPromoAdapter(
            onButtonClick = viewModel::onButtonClick,
            onMenuItemClick = viewModel::onMenuItemClick,
            onCloseClick = viewModel::onCloseClick
        )
    }

    private val x5BankPromoItemDecorator by lazyUnsynchronized {
        X5BankPromoItemDecoration(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding.vContent) {
            disableItemChangeAnimation()
            setHasFixedSize(true)
            adapter = this@X5BankPromoFragment.adapter
            addItemDecoration(x5BankPromoItemDecorator)
        }
    }

    override fun onObserveLiveData() {
        super.onObserveLiveData()
        viewModel.content.observe(viewLifecycleOwner, ::setUi)
    }

    private fun setUi(content: X5BankPromoUiModel.Content) {
        adapter.items = content.list
    }

    override fun onDestroyView() {
        with(binding.vContent) {
            adapter = null
            removeItemDecoration(x5BankPromoItemDecorator)
        }
        super.onDestroyView()
    }
}