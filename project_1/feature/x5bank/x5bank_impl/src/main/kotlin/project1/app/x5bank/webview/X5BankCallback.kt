package project1.app.x5bank.webview

interface X5BankCallback {
    fun onPageStarted()
    fun onReceivedError()
    fun onCloseWebView()
    fun onCardIssued()
}