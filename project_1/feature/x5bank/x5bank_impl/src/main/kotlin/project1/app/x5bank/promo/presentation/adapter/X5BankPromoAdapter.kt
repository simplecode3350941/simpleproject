package project1.app.x5bank.promo.presentation.adapter

import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import project1.app.base.ui.presentation.BaseDiffCallback
import project1.app.base.ui.widget.button.adapter.delegate.buttonAD
import project1.app.base.ui.widget.button.model.ButtonFullUiModel
import project1.app.profile.card.create.presentation.detail.adapter.delegates.createCardInfoAD
import project1.app.x5bank.promo.presentation.adapter.delegates.x5BankPromoDividerAD
import project1.app.x5bank.promo.presentation.adapter.delegates.x5BankPromoFooterAD
import project1.app.x5bank.promo.presentation.adapter.delegates.x5BankPromoHeaderAD
import project1.app.x5bank.promo.presentation.adapter.delegates.x5BankPromoInfoAD
import project1.app.x5bank.promo.presentation.adapter.delegates.x5BankPromoMenuItemAD
import project1.app.x5bank.promo.presentation.model.X5BankPromoUiModel

class X5BankPromoAdapter(
    onButtonClick: (ButtonFullUiModel) -> Unit,
    onMenuItemClick: (X5BankPromoUiModel.MenuItem) -> Unit,
    onCloseClick: () -> Unit
) : AsyncListDifferDelegationAdapter<Any>(DiffCallback) {

    init {
        delegatesManager
            .addDelegate(buttonAD(onButtonClick))
            .addDelegate(createCardInfoAD())
            .addDelegate(x5BankPromoHeaderAD(onCloseClick))
            .addDelegate(x5BankPromoInfoAD())
            .addDelegate(x5BankPromoMenuItemAD(onMenuItemClick))
            .addDelegate(x5BankPromoDividerAD())
            .addDelegate(x5BankPromoFooterAD())
    }

    private companion object DiffCallback : BaseDiffCallback() {

        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
            return oldItem.javaClass == newItem.javaClass
        }
    }
}