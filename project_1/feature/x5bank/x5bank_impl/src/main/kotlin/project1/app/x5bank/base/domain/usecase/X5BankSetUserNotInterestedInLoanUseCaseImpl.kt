package project1.app.x5bank.base.domain.usecase

import project1.app.x5bank.base.domain.X5BankRepository
import project1.app.x5bank.domain.X5BankSetUserNotInterestedInLoanUseCase

internal class X5BankSetUserNotInterestedInLoanUseCaseImpl(
    private val repository: X5BankRepository
) : X5BankSetUserNotInterestedInLoanUseCase {
    override suspend operator fun invoke() = repository.setUserNotInterestedInLoan()
}