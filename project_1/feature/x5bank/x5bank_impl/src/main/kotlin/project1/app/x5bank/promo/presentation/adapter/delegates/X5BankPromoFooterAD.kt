package project1.app.x5bank.promo.presentation.adapter.delegates

import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import project1.app.x5bank.databinding.X5bankPromoFooterItemBinding
import project1.app.x5bank.promo.presentation.model.X5BankPromoUiModel

fun x5BankPromoFooterAD(): AdapterDelegate<List<Any>> {
    return adapterDelegateViewBinding<X5BankPromoUiModel.Footer, Any, X5bankPromoFooterItemBinding>(
        { layoutInflater, parent ->
            X5bankPromoFooterItemBinding.inflate(layoutInflater, parent, false)
        }
    ) {}
}