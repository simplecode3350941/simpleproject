package project1.app.x5bank.base.data.remote.model

import com.google.gson.annotations.SerializedName

data class X5BankLinkResponseDto(
    @SerializedName("link")
    val url: String?
)