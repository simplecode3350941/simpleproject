package project1.app.x5bank.base.domain.usecase

import project1.app.base.segmentation.base.domain.ToggleGetValueUseCase
import project1.app.base.segmentation.base.domain.model.SegmentationToggleValueModel
import project1.app.x5bank.base.domain.X5BankRepository
import project1.app.x5bank.base.domain.X5BankSegmentationRepository
import project1.app.x5bank.domain.X5BankLoanIsAvailableUseCase

internal class X5BankLoanIsAvailableUseCaseImpl(
    private val repository: X5BankRepository,
    private val segmentationRepository: X5BankSegmentationRepository,
    private val getToggleValue: ToggleGetValueUseCase
) : X5BankLoanIsAvailableUseCase {

    override suspend operator fun invoke(): Boolean {
        val x5BankLoanIsEnabled = getX5BankLoanToggleValue().isEnabled()
        val isUserInterestedInLoan = isUserInterestedInLoan()
        return x5BankLoanIsEnabled && isUserInterestedInLoan
    }

    private suspend fun getX5BankLoanToggleValue(): SegmentationToggleValueModel {
        return getToggleValue(segmentationRepository.getX5BankLoanToggle())
    }

    private suspend fun isUserInterestedInLoan() = repository.isUserInterestedInLoan()
}