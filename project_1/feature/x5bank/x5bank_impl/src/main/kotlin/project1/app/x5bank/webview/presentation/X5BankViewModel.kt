package project1.app.x5bank.webview.presentation

import project1.app.base.analytics.domain.AnalyticsInteractor
import project1.app.base.ui.webview.withstate.WebViewWithStateViewModel
import project1.app.base.ui.webview.withstate.model.WebViewState
import project1.app.x5bank.base.domain.usecase.X5BankGetUrlUseCase
import project1.app.x5bank.base.domain.usecase.X5BankSetCardIssuedUseCase
import project1.app.x5bank.domain.X5BankCreditCardGetUrlUseCase
import project1.app.x5bank.domain.X5BankGetNoLandingUrlUseCase
import project1.app.x5bank.domain.X5BankIsNewFlowAvailableUseCase
import project1.app.x5bank.domain.X5BankLoanGetUrlUseCase
import project1.app.x5bank.domain.X5bankSetIsBackFromWebviewFlagUseCase
import project1.app.x5bank.navigator.X5BankNavigator
import project1.app.x5bank.presentation.model.X5BankUrlType
import project1.app.x5bank.webview.X5BankCallback

class X5BankViewModel(
    private val parameters: X5BankParameters,
    private val getX5BankUrl: X5BankGetUrlUseCase,
    private val getX5BankNoLandingUrl: X5BankGetNoLandingUrlUseCase,
    private val getCreditCardUrl: X5BankCreditCardGetUrlUseCase,
    private val getX5BankLoanUrl: X5BankLoanGetUrlUseCase,
    private val setCardIssued: X5BankSetCardIssuedUseCase,
    private val navigator: X5BankNavigator,
    private val setIsBackFromX5bankWebviewFlag: X5bankSetIsBackFromWebviewFlagUseCase,
    private val isX5BankNewFlowAvailable: X5BankIsNewFlowAvailableUseCase,
    analyticsInteractor: AnalyticsInteractor
) : WebViewWithStateViewModel(analyticsInteractor), X5BankCallback {

    init {
        loadUrl()
    }

    override fun onCloseClick() {
        navigator.back()
    }

    override fun onRetryClick() {
        loadUrl()
    }

    override fun onPageStarted() {
        state.value = WebViewState.Content
    }

    override fun onReceivedError() {
        state.value = WebViewState.Error()
    }

    override fun onCloseWebView() {
        launchJob {
            setIsBackFromX5bankWebviewFlag(isBackFromX5bankWebviewFlagValue = true)
        }
        navigator.back()
    }

    private fun loadUrl() {
        state.value = WebViewState.Loading
        launchErrorJob(onError = ::handleError) {
            loadUrl.value = when (parameters.urlType) {
                X5BankUrlType.CARD_ISSUE -> {
                    if (isX5BankNewFlowAvailable()) {
                        getX5BankNoLandingUrl(parameters.bannerCode)
                    } else getX5BankUrl(parameters.bannerCode)
                }
                X5BankUrlType.CARD_SETTINGS -> getX5BankUrl(parameters.bannerCode)
                X5BankUrlType.MICROLOAN -> getX5BankLoanUrl(parameters.bannerCode)
                X5BankUrlType.CREDIT_CARD -> getCreditCardUrl(parameters.bannerCode)
            }
        }
    }

    override fun onCardIssued() {
        launchJob {
            setCardIssued()
        }
    }

    private fun handleError(ignored: Throwable) {
        state.value = WebViewState.Error()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        navigator.back()
    }
}