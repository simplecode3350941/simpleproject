package project1.app.x5bank.base.domain.usecase

import project1.app.x5bank.base.domain.X5BankRepository
import project1.app.x5bank.domain.X5BankCreditCardSetTimeOnboardingShownUseCase

internal class X5BankCreditCardSetTimeOnboardingShownUseCaseImpl(
    private val repository: X5BankRepository
) : X5BankCreditCardSetTimeOnboardingShownUseCase {
    override suspend operator fun invoke() = repository.saveCreditCardOnboardingTimestamp(System.currentTimeMillis())
}