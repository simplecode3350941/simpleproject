package project1.app.x5bank.navigation

import project1.app.base.ui.navigation.BaseScreens
import project1.app.base.ui.navigation.cicerone.router.AppRouter
import project1.app.x5bank.X5BankScreens
import project1.app.x5bank.domain.X5BankPromoConstants
import project1.app.x5bank.navigator.X5BankNavigator
import project1.app.x5bank.presentation.model.X5BankUrlType
import project1.app.x5bank.promo.presentation.X5BankPromoParameters
import project1.app.x5bank.webview.presentation.X5BankParameters

internal class X5BankNavigatorImpl(private val router: AppRouter) : X5BankNavigator {
    override fun toX5Bank(bannerCode: String?, urlType: X5BankUrlType) {
        router.navigateTo(X5BankScreens.X5Bank(X5BankParameters(bannerCode, urlType)))
    }

    override fun toX5BankPromo() {
        router.navigateTo(
            X5BankScreens.X5BankPromo(
                X5BankPromoParameters(X5BankPromoConstants.X5BANK_PROMO_POPUP_BANNER_CODE)
            )
        )
    }

    override fun toX5BankOnboarding() {
        router.addFragment(X5BankScreens.X5BankOnboarding)
    }

    override fun toX5BankLoanOnboarding() {
        router.addFragment(X5BankScreens.X5BankLoanOnboarding)
    }

    override fun toX5BankCreditCardOnboarding() {
        router.addFragment(X5BankScreens.X5BankCreditCardOnboarding)
    }

    override fun back() {
        router.exit()
    }

    override fun toCallBack(phoneNumber: String) {
        router.navigateTo(BaseScreens.Dial(phoneNumber))
    }

    override fun toDocument(url: String) {
        router.navigateTo(BaseScreens.ExternalFlow(url))
    }
}