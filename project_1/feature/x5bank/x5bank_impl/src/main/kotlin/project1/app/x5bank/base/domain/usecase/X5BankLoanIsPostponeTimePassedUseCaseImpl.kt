package project1.app.x5bank.base.domain.usecase

import project1.app.x5bank.base.domain.X5BankRepository
import project1.app.x5bank.domain.X5BankLoanIsPostponeTimePassedUseCase
import java.util.concurrent.TimeUnit

internal class X5BankLoanIsPostponeTimePassedUseCaseImpl(
    private val repository: X5BankRepository
) : X5BankLoanIsPostponeTimePassedUseCase {
    override suspend operator fun invoke(): Boolean {
        val diff = System.currentTimeMillis() - repository.getLoanOnboardingTimestamp()
        return diff > POSTPONE_TIME
    }

    companion object {
        private val POSTPONE_TIME = TimeUnit.DAYS.toMillis(3L)
    }
}