package project1.app.x5bank.promo.presentation.model

import android.view.Gravity
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.annotation.FontRes
import project1.app.base.ui.widget.textview.model.ClickableTextModel

sealed class X5BankPromoUiModel {

    data class Content(
        val list: List<Any>
    ) : X5BankPromoUiModel()

    object Header : X5BankPromoUiModel()

    data class Info(
        val text: String,
        val description: CharSequence = "",
        val clickableTextModels: List<ClickableTextModel> = emptyList(),
        @DimenRes val textSizeRes: Int,
        @ColorRes val textColorRes: Int = project1.app.personal.R.color.black,
        @DimenRes val textDescriptionSizeRes: Int,
        @ColorRes val textDescriptionColorRes: Int = project1.app.personal.R.color.black,
        @DrawableRes val drawableRes: Int? = null,
        @FontRes val fontRes: Int = project1.app.personal.R.font.project1_sans_design_semibold,
        @FontRes val fontDescriptionRes: Int = project1.app.personal.R.font.project1_sans_design_semibold,
        @DimenRes val topOffsetRes: Int = project1.app.personal.R.dimen.offset15,
        @DimenRes val bottomOffsetRes: Int = project1.app.personal.R.dimen.nothing,
        val gravity: Int = Gravity.START
    ) : X5BankPromoUiModel()

    data class MenuItem(
        val id: Any,
        @ColorRes val textColorRes: Int,
        @DimenRes val textSizeRes: Int,
        @FontRes val fontRes: Int = project1.app.personal.R.font.project1_sans_design_semibold,
        val text: String,
        val gravity: Int = Gravity.START,
        @DimenRes val topOffsetRes: Int = project1.app.personal.R.dimen.nothing,
        @DimenRes val bottomOffsetRes: Int = project1.app.personal.R.dimen.nothing
    ) : X5BankPromoUiModel()

    object Divider : X5BankPromoUiModel()

    object Footer : X5BankPromoUiModel()
}