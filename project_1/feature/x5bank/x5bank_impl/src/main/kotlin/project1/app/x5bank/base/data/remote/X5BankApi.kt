package project1.app.x5bank.base.data.remote

import project1.app.x5bank.base.data.remote.model.X5BankCardIssueStateResponseDto
import project1.app.x5bank.base.data.remote.model.X5BankCardIssuedResponseDto
import project1.app.x5bank.base.data.remote.model.X5BankLinkResponseDto
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Query

interface X5BankApi {
    @GET("v1/link-no-landing")
    suspend fun getNoLandingUrl(@Query("banner") bannerCode: String?): X5BankLinkResponseDto?

    @GET("v1/link")
    suspend fun getUrl(@Query("banner") bannerCode: String?): X5BankLinkResponseDto?

    @GET("v1/ciflag")
    suspend fun getX5BankIssueState(): X5BankCardIssueStateResponseDto?

    @Headers("Content-Type: text/plain")
    @POST("v1/hook-mobile")
    suspend fun setX5BankCardIssued(): X5BankCardIssuedResponseDto?

    @GET("v1/microloan/learn-more")
    suspend fun getLoanUrl(@Query("entry-point") bannerCode: String?): X5BankLinkResponseDto?

    @GET("v1/credit-card/learn-more")
    suspend fun getCreditCardUrl(@Query("entry-point") bannerCode: String?): X5BankLinkResponseDto?
}