package project1.app.x5bank.base.data.remote.model

import com.google.gson.annotations.SerializedName

data class X5BankCardIssuedResponseDto(
    @SerializedName("is_successful")
    val isSuccessful: Boolean?
)