package project1.app.x5bank.promo.presentation

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class X5BankPromoParameters(
    val bannerCode: String? = null
) : Parcelable