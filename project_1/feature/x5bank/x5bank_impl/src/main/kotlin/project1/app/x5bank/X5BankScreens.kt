package project1.app.x5bank

import androidx.fragment.app.Fragment
import project1.app.base.ui.util.parcelableParametersBundleOf
import project1.app.x5bank.onboarding.X5BankCreditCardOnboardingFragment
import project1.app.x5bank.onboarding.X5BankLoanOnboardingFragment
import project1.app.x5bank.onboarding.X5BankOnboardingFragment
import project1.app.x5bank.promo.presentation.X5BankPromoFragment
import project1.app.x5bank.promo.presentation.X5BankPromoParameters
import project1.app.x5bank.webview.presentation.X5BankFragment
import project1.app.x5bank.webview.presentation.X5BankParameters
import ru.terrakok.cicerone.android.support.FragmentParams
import ru.terrakok.cicerone.android.support.SupportAppScreen

internal class X5BankScreens {

    class X5BankPromo(
        private val parameters: X5BankPromoParameters
    ) : SupportAppScreen() {
        override fun getFragment() = X5BankPromoFragment()
        override fun getFragmentParams() = FragmentParams(
            X5BankPromoFragment::class.java,
            parcelableParametersBundleOf(parameters)
        )
    }

    class X5Bank(
        private val parameters: X5BankParameters
    ) : SupportAppScreen() {
        override fun getFragment() = X5BankFragment()
        override fun getFragmentParams() = FragmentParams(
            X5BankFragment::class.java,
            parcelableParametersBundleOf(parameters)
        )
    }

    object X5BankOnboarding : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return X5BankOnboardingFragment()
        }
    }

    object X5BankLoanOnboarding : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return X5BankLoanOnboardingFragment()
        }
    }

    object X5BankCreditCardOnboarding : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return X5BankCreditCardOnboardingFragment()
        }
    }
}