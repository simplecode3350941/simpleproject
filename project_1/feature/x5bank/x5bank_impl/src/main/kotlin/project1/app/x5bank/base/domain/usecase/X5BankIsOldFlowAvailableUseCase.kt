package project1.app.x5bank.base.domain.usecase

import project1.app.base.segmentation.base.domain.ToggleGetValueUseCase
import project1.app.base.segmentation.base.domain.model.SegmentationToggleValueModel
import project1.app.x5bank.base.domain.X5BankSegmentationRepository

class X5BankIsOldFlowAvailableUseCase(
    private val segmentationRepository: X5BankSegmentationRepository,
    private val getToggleValue: ToggleGetValueUseCase
) {

    suspend operator fun invoke(): Boolean {
        return getX5BankToggleValue().isEnabled()
    }

    private suspend fun getX5BankToggleValue(): SegmentationToggleValueModel {
        return getToggleValue(segmentationRepository.getX5BankToggle())
    }
}