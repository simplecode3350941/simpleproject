package project1.app.x5bank.base.data

import project1.app.base.segmentation.base.domain.model.SegmentationToggle
import project1.app.x5bank.base.domain.X5BankSegmentationRepository

class X5BankSegmentationRepositoryImpl : X5BankSegmentationRepository {
    override fun getX5BankToggle() = SegmentationToggle(
        code = SEGMENTATION_TOGGLE_X5BANK,
        name = SEGMENTATION_TOGGLE_X5BANK_NAME
    )

    override fun getX5BankNoLandingToggle() = SegmentationToggle(
        code = SEGMENTATION_TOGGLE_X5BANK_NO_LANDING,
        name = SEGMENTATION_TOGGLE_X5BANK_NO_LANDING_NAME
    )

    override fun getX5BankLoanToggle() = SegmentationToggle(
        code = SEGMENTATION_TOGGLE_X5BANK_LOAN,
        name = SEGMENTATION_TOGGLE_X5BANK_LOAN_NAME
    )

    override fun getX5BankCreditCardToggle() = SegmentationToggle(
        code = SEGMENTATION_TOGGLE_X5BANK_CREDIT_CARD,
        name = SEGMENTATION_TOGGLE_X5BANK_CREDIT_CARD_NAME
    )

    override suspend fun getToggles(): List<SegmentationToggle> {
        return listOf(
            getX5BankToggle(),
            getX5BankLoanToggle(),
            getX5BankNoLandingToggle(),
            getX5BankCreditCardToggle()
        )
    }

    companion object {
        private const val SEGMENTATION_TOGGLE_X5BANK = "x5bank_CI"
        private const val SEGMENTATION_TOGGLE_X5BANK_NAME = "X5 Банк"
        private const val SEGMENTATION_TOGGLE_X5BANK_NO_LANDING = "x5bank_ci_no_landing"
        private const val SEGMENTATION_TOGGLE_X5BANK_NO_LANDING_NAME = "X5 Банк - новая анкета"
        private const val SEGMENTATION_TOGGLE_X5BANK_LOAN = "x5bank_microloan"
        private const val SEGMENTATION_TOGGLE_X5BANK_LOAN_NAME = "X5 Банк Микрозайм"
        private const val SEGMENTATION_TOGGLE_X5BANK_CREDIT_CARD = "x5bank_credit_card"
        private const val SEGMENTATION_TOGGLE_X5BANK_CREDIT_CARD_NAME = "X5 Банк Кредитная карта"
    }
}