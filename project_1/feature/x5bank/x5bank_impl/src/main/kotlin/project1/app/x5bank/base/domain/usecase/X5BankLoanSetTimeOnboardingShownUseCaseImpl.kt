package project1.app.x5bank.base.domain.usecase

import project1.app.x5bank.base.domain.X5BankRepository
import project1.app.x5bank.domain.X5BankLoanSetTimeOnboardingShownUseCase

internal class X5BankLoanSetTimeOnboardingShownUseCaseImpl(
    private val repository: X5BankRepository
) : X5BankLoanSetTimeOnboardingShownUseCase {
    override suspend operator fun invoke() = repository.saveLoanOnboardingTimestamp(System.currentTimeMillis())
}