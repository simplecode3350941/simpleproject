package project1.app.x5bank.base.domain.usecase

import project1.app.x5bank.base.domain.X5BankRepository
import project1.app.x5bank.domain.X5bankSetIsBackFromWebviewFlagUseCase

class X5bankSetIsBackFromWebviewFlagUseCaseImpl(
    private val x5bankRepository: X5BankRepository
) : X5bankSetIsBackFromWebviewFlagUseCase {
    override suspend operator fun invoke(isBackFromX5bankWebviewFlagValue: Boolean) {
        x5bankRepository.setIsBackFromX5bankWebviewFlag(isBackFromX5bankWebviewFlagValue)
    }
}