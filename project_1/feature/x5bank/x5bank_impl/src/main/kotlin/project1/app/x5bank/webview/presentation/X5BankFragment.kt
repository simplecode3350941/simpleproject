package project1.app.x5bank.webview.presentation

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import project1.app.base.ui.extension.hideKeyBoard
import project1.app.base.ui.extension.parcelableParametersOf
import project1.app.base.ui.webview.withstate.WebViewWithStateFragment
import project1.app.x5bank.webview.X5BankWebClient

class X5BankFragment : WebViewWithStateFragment() {
    override val viewModel by viewModel<X5BankViewModel> {
        parcelableParametersOf<X5BankParameters>()
    }

    private val webClient by inject<X5BankWebClient> {
        parametersOf(viewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbarBinding.vGroup.isVisible = false
        binding.vWebView.webViewClient = webClient
    }

    override fun removeCookies() {
        // Nothing
    }

    override fun onPause() {
        super.onPause()
        hideKeyBoard()
    }
}