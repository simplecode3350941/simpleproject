package com.project2.partner.points.impl.ui.list.di

import com.project2.core.presentation.di.ComponentDependenciesProviderModule
import com.project2.partner.points.impl.ui.list.ListPointsFragment
import dagger.Component

@Component(
    modules = [ComponentDependenciesProviderModule::class],
    dependencies = [ListPointsDependencies::class]
)
interface ListPointsComponent {
    fun inject(listPointsFragment: ListPointsFragment)
}
/*

@Component(
    modules = [
        PointsModule::class,
        PointsViewModelsModule::class,
        ComponentDependenciesProviderModule::class,
        ComponentDependenciesModule::class,
    ],
    dependencies = [
        PartnerPointsApi::class,
        PartnerPointsRepository::class,
        GetPartnerListPointsUseCase::class,
        CoroutineExceptionHandler::class
    ]
)
interface ListPointsDependenciesComponent: ListPointsDependencies*/
