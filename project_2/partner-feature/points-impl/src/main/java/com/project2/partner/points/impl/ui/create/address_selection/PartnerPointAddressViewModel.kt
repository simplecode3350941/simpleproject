package com.project2.partner.points.impl.ui.create.address_selection

import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.partner.points.api.domain.usecase.GetDadataAddressUseCase
import com.project2.partner.points.impl.ui.create.mapper.toPresentation
import com.project2.partner.points.impl.ui.create.models.DadataAddressUi
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject


@OptIn(FlowPreview::class)
class PartnerPointAddressViewModel @Inject constructor(
    private val getAddress: GetDadataAddressUseCase,
    override val exceptionHandler: CoroutineExceptionHandler,
): BaseViewModel() {

    private val _pointAddressFlow = MutableStateFlow<List<DadataAddressUi>>(listOf())
    val pointAddressFlow = _pointAddressFlow.asStateFlow()

    private val _uiState = MutableStateFlow<UiState?>(null)
    val uiState = _uiState.asStateFlow().filterNotNull()

    private val searchFlow = MutableStateFlow("")

    init {
        launchOnViewModelScope {
            searchFlow
                .filter { it.isNotEmpty() }
                .debounce(500)
                .distinctUntilChanged()
                .collectLatest { query ->
                    getAddress(query)
                        .onStart { _uiState.value = UiState.Loading }
                        .map { addressList -> addressList.map { it.toPresentation() } }
                        .catch { _uiState.value = UiState.Error(it) }
                        .collectLatest {
                            _pointAddressFlow.value = it
                            _uiState.value = UiState.Success
                        }
                }
        }
    }

    fun searchAddress(query: String?) {
        val queryStr = query?.trim()
        when {
            queryStr.isNullOrEmpty() -> _pointAddressFlow.value = listOf()
            else -> searchFlow.value = queryStr
        }
    }
}

sealed interface UiState {
    object Loading: UiState
    object Success: UiState
    data class Error(val error: Throwable): UiState
}