package com.project2.partner.points.impl.ui.create.mapper

import android.content.Context
import com.project2.core.utils.formatter.FormatUtils.toFormattedPhoneNumber
import com.project2.partner.points.api.domain.models.PointDetailDomain
import com.project2.partner.points.api.domain.models.PointTypeWithCategoryDomain
import com.project2.partner.points.impl.ui.create.communication.CreatePointCommunication
import com.project2.partner.points.impl.ui.create.communication.CreatePointUi
import com.project2.partner.points.impl.ui.create.work_schedule_selection.adapter.DayWeek
import com.project2.partner.points.impl.ui.create.work_schedule_selection.adapter.WorkScheduleAdapterItem
import com.project2.partner.points.impl.ui.list.CategoryUi
import com.project2.partner.points.impl.ui.list.PointTypeUi
import com.project2.partner.points.impl.ui.list.PointTypeWithCategoryUi
import com.project2.partner.points.impl.ui.list.ProductUi
import com.project2.partner.points.impl.ui.list.ScheduleUi
import com.project2.partner.points.impl.ui.list.mapper.toPresentation
import com.project2.partner.points.impl.ui.price_list.adapter.toAdapterItem

fun PointDetailDomain.toCreatePointUi(context: Context): CreatePointUi {
    val typesHash = toHashSetPointTypeUiWithNullId(types)
    val workSchedules = schedule.map { it.toPresentation().toWorkSchedule(context) }
    val typesWithCategories = toHashSetPointTypeUiWith(types)
    val categorizedProducts = toCategorizedProducts(types)
    val phoneNumber = phoneNumber.toFormattedPhoneNumber()
    return CreatePointUi(
        pointName = name,
        types = typesHash,
        phoneNumber = phoneNumber,
        latitude = latitude,
        longitude = longitude,
        address = address,
        workSchedules = workSchedules,
        typesWithCategories = typesWithCategories,
        categorizedProducts = categorizedProducts
    )
}

fun toCategorizedProducts(types: List<PointTypeWithCategoryDomain>): HashMap<CategoryUi, List<ProductUi>> {
    val categorizedProducts = hashMapOf<CategoryUi, List<ProductUi>>()
    val pointTypeWithCategoryUiList = mutableListOf<PointTypeWithCategoryUi>()
    types.forEach {
        pointTypeWithCategoryUiList.add(it.toPresentation())
    }

    val categoriesWithProducts = pointTypeWithCategoryUiList.flatMap { it.categories }
    categoriesWithProducts.map {
        val pointTypeUi = it.products.first().category.pointTypeUi
        val categoryAdapterItem = it.toAdapterItem(pointTypeUi = pointTypeUi)

        val categoryUi = CategoryUi(categoryAdapterItem.id, categoryAdapterItem.categoryName, pointTypeUi)
        val filledProducts = CreatePointCommunication.getPointUi().categorizedProducts[categoryUi]
        categorizedProducts[categoryUi] = filledProducts.orEmpty().ifEmpty { it.products }
    }
    return categorizedProducts
}

fun toHashSetPointTypeUiWithNullId(types: List<PointTypeWithCategoryDomain>): HashSet<PointTypeUi> {
    val typesHashSet = mutableListOf<PointTypeUi>()
    for (i in types.indices) {
        val item = types[i].toPresentation().type.copy(id = null)
        typesHashSet.add(item)
    }
    return typesHashSet.toHashSet()
}

fun toHashSetPointTypeUiWith(types: List<PointTypeWithCategoryDomain>): HashSet<PointTypeUi> {
    val typesHashSet = mutableListOf<PointTypeUi>()
    for (i in types.indices) {
        val item = types[i].toPresentation().type
        typesHashSet.add(item)
    }
    return typesHashSet.toHashSet()
}

fun ScheduleUi.toWorkSchedule(context: Context) = toSchedule(context)

fun ScheduleUi.toSchedule(context: Context) = WorkScheduleAdapterItem.Schedule(
    dayOfWeek = getDayWeekEnumValue(workingDay, context),
    startTime = timeFrom,
    endTime = timeTo,
    isDayOff = dayOff,
    friendlyDayWeek = workingDay
)

fun getDayWeekEnumValue(day: String, context: Context): DayWeek {
    return when (day) {
        context.getString(DayWeek.MONDAY.resId) -> { DayWeek.MONDAY }
        context.getString(DayWeek.TUESDAY.resId) -> { DayWeek.TUESDAY }
        context.getString(DayWeek.WEDNESDAY.resId) -> { DayWeek.WEDNESDAY }
        context.getString(DayWeek.THURSDAY.resId) -> { DayWeek.THURSDAY }
        context.getString(DayWeek.FRIDAY.resId) -> { DayWeek.FRIDAY }
        context.getString(DayWeek.SATURDAY.resId) -> { DayWeek.SATURDAY }
        context.getString(DayWeek.SUNDAY.resId) -> { DayWeek.SUNDAY }
        else -> DayWeek.MONDAY
    }
}