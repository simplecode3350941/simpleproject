package com.project2.partner.points.impl.ui.create.work_schedule_selection

import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.core.utils.common.toImmutableList
import com.project2.partner.points.impl.ui.create.communication.CreatePointCommunication
import com.project2.partner.points.impl.ui.create.work_schedule_selection.adapter.DayWeek
import com.project2.partner.points.impl.ui.create.work_schedule_selection.adapter.WorkScheduleAdapterItem
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class WorkScheduleSelectionViewModel @Inject constructor(
    override val exceptionHandler: CoroutineExceptionHandler,
): BaseViewModel() {

    private val _workSchedulesFlow = MutableStateFlow<List<WorkScheduleAdapterItem>>(listOf())
    val workSchedulesFlow = _workSchedulesFlow.asStateFlow()

    fun getWorkSchedules(getFriendlyDay: (DayWeek) -> (String)) {
        launchOnViewModelScope {
            withContext(coroutineContext) {
                val workSchedules = CreatePointCommunication.getPointUi()
                    .workSchedules
                    .map { it.toAdapterItem() }
                    .ifEmpty {
                        buildWorkSchedules(getFriendlyDay)
                    }
                _workSchedulesFlow.value = workSchedules
            }
        }
    }

    fun setTime(dayOfWeek: DayWeek, time: String, isStartTime: Boolean) {
        modifyItemAndEmit(dayOfWeek) {
            when {
                isStartTime -> WorkScheduleAdapterItem(it.day.copy(startTime = time))
                else -> WorkScheduleAdapterItem(it.day.copy(endTime = time))
            }
        }
    }

    fun setDayOff(dayOfWeek: DayWeek, isChecked: Boolean) {
        modifyItemAndEmit(dayOfWeek) {
            WorkScheduleAdapterItem(it.day.copy(isDayOff = isChecked))
        }
    }

    private fun modifyItemAndEmit(dayOfWeek: DayWeek, onModifyItem: (WorkScheduleAdapterItem) -> WorkScheduleAdapterItem) {
        launchOnViewModelScope {
            val schedules = _workSchedulesFlow.value.toMutableList()
            val workScheduleItem = schedules.find { it.id() == dayOfWeek }

            if(workScheduleItem != null) {
                val index = schedules.indexOf(workScheduleItem)
                schedules[index] = onModifyItem.invoke(workScheduleItem)

                withContext(coroutineContext) {
                    _workSchedulesFlow.value = schedules.toImmutableList()
                }
            }
        }
    }
}

fun WorkScheduleAdapterItem.Schedule.toAdapterItem() = WorkScheduleAdapterItem(this)

fun buildWorkSchedules(getFriendlyDay: (DayWeek) -> (String)): List<WorkScheduleAdapterItem> {
    return DayWeek.values().map { dayWeek ->
        WorkScheduleAdapterItem(
            WorkScheduleAdapterItem.Schedule(
                dayOfWeek = dayWeek,
                friendlyDayWeek = getFriendlyDay(dayWeek)
            )
        )
    }
}