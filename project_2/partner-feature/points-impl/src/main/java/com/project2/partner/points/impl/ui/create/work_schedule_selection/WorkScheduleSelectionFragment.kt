package com.project2.partner.points.impl.ui.create.work_schedule_selection

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.project2.core.presentation.adapter_delegate.MultiTypeAdapter
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.utils.common.lazyUnsafe
import com.project2.partner.points.impl.R
import com.project2.partner.points.impl.databinding.FragmentPointWorkScheduleSelectionBinding
import com.project2.partner.points.impl.ui.create.communication.CreatePointCommunication
import com.project2.partner.points.impl.ui.create.work_schedule_selection.adapter.DayWeek
import com.project2.partner.points.impl.ui.create.work_schedule_selection.adapter.WorkScheduleAdapterItem
import com.project2.partner.points.impl.ui.create.work_schedule_selection.adapter.WorkScheduleViewHolderDelegate
import com.project2.partner.points.impl.ui.create.work_schedule_selection.bottom_sheet.SetTimeBottomSheet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

class WorkScheduleSelectionFragment: BaseMvvmFragment() {

    private val viewModel: WorkScheduleSelectionViewModel by viewModels {
        viewModelFactory
    }

    private val workScheduleAdapter: MultiTypeAdapter by lazyUnsafe {
        MultiTypeAdapter.Builder()
            .add(
                WorkScheduleViewHolderDelegate(
                ::onDayOffClick,
                ::onStartTimeClick,
                ::onEndTimeClick
            )
            )
            .build()
    }

    override fun getLayoutResId(): Int = R.layout.fragment_point_work_schedule_selection

    override val synthetic = SyntheticBinding(FragmentPointWorkScheduleSelectionBinding::inflate)

    override fun initUi(savedInstanceState: Bundle?) {

        synthetic.binding.toolbar.setNavigationOnClickListener {
            popBack()
        }

        synthetic.binding.recyclerView.apply {
            adapter = workScheduleAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            setHasFixedSize(true)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.workSchedulesFlow.collectLatest(::submitWorkSchedules)
        }

        childFragmentManager.setFragmentResultListener(SetTimeBottomSheet.REQUEST_KEY, viewLifecycleOwner) { _, bundle ->
            val time = bundle.getString(SetTimeBottomSheet.ARGS_TIME)
            val dayWeek = bundle.getString(SetTimeBottomSheet.ARGS_DAY_OF_WEEK)
            val isStartTime = bundle.getBoolean(SetTimeBottomSheet.ARGS_IS_START_TIME)
            if(time != null && dayWeek != null) {
                viewModel.setTime(DayWeek.valueOf(dayWeek), time, isStartTime)
            }
        }

        viewModel.getWorkSchedules {
            getString(it.resId)
        }
    }

    private fun onStartTimeClick(dayWeek: DayWeek, time: String?) {
        showSetTimeBottomSheet(dayWeek, time, true)
    }

    private fun onEndTimeClick(dayWeek: DayWeek, time: String?) {
        showSetTimeBottomSheet(dayWeek, time, false)
    }

    private fun showSetTimeBottomSheet(dayWeek: DayWeek, time: String?, isStartTime: Boolean) {
        SetTimeBottomSheet.show(childFragmentManager, dayWeek.toString(), time, isStartTime)
    }

    private fun onDayOffClick(dayWeek: DayWeek, isChecked: Boolean) {
        viewModel.setDayOff(dayWeek, isChecked)
    }

    private fun submitWorkSchedules(workSchedules: List<WorkScheduleAdapterItem>) {
        Timber.d("workSchedules size – %s", workSchedules.size)
        workScheduleAdapter.submitList(workSchedules)
        lifecycleScope.launch {
            CreatePointCommunication.setWorkSchedules(workSchedules.map { it.day })
        }
    }
}