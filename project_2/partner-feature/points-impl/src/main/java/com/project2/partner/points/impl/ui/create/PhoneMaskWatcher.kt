package com.project2.partner.points.impl.ui.create

import android.text.Editable
import android.text.TextWatcher

private val PHONE_REGEX = Regex("\\D+")

class PhoneMaskWatcher : TextWatcher {
    private var isForwarding = false
    private var isDeleting = false

    override fun beforeTextChanged(
        charSequence: CharSequence,
        start: Int,
        count: Int,
        after: Int
    ) {
        isDeleting = count > after
    }

    override fun onTextChanged(
        charSequence: CharSequence,
        start: Int,
        before: Int,
        count: Int
    ) {
    }

    override fun afterTextChanged(editable: Editable) {
        if (isForwarding || isDeleting) {
            return
        }

        isForwarding = true

        val cleanNumber = PHONE_REGEX.replace(editable, "")
        editable.clear()
        editable.insert(0, cleanNumber)

        if (!editable.startsWith("+") && editable.isNotEmpty()) {
            editable.insert(0, "+")
        }

        if (editable.length > 1 && editable[1].toString() != "7") {
            editable.insert(1, "7")
        }

        if (editable.length > 2 && editable[2].toString() != " ") {
            editable.insert(2, " ")
        }

        if (editable.length > 3 && editable[3].toString() != "("){
            editable.insert(3, "(")
        }

        if (editable.length > 7 && editable[7].toString() != ")"){
            editable.insert(7, ")")
        }

        if (editable.length > 8 && editable[8].toString() != " ") {
            editable.insert(8, " ")
        }

        if (editable.length > 12 && editable[12].toString() != "-"){
            editable.insert(12, "-")
        }

        if (editable.length > 15 && editable[15].toString() != "-"){
            editable.insert(15, "-")
        }
        isForwarding = false
    }
}