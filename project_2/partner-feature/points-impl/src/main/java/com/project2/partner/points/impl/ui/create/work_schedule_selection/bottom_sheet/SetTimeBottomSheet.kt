package com.project2.partner.points.impl.ui.create.work_schedule_selection.bottom_sheet

import android.os.Bundle
import android.view.View
import android.widget.TimePicker
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResult
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.dialog.BaseBottomSheetDialogFragment
import com.project2.partner.points.impl.databinding.PartnerPointSetTimeBottomSheetBinding

class SetTimeBottomSheet: BaseBottomSheetDialogFragment() {

    private val isStartTime: Boolean
        get() = requireArguments().getBoolean(ARGS_IS_START_TIME)

    private val times: List<Int>
        get() {
            val argsTime = requireArguments().getString(ARGS_TIME)
            return argsTime?.split(":")?.map { it.toInt() } ?: run {
                if(isStartTime) listOf(9, 0) else listOf(18, 0)
            }
        }

    override val synthetic = SyntheticBinding(PartnerPointSetTimeBottomSheetBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        synthetic.binding.timePicker.setIs24HourView(true)

        synthetic.binding.timePicker.hour = times[0]
        synthetic.binding.timePicker.minute = times[1]

        synthetic.binding.buttonAdd.setOnClickListener {
            sendResult()
        }

        synthetic.binding.buttonCancel.setOnClickListener {
            dismiss()
        }
    }

    private fun sendResult() {
        val time = synthetic.binding.timePicker.getFriendlyTime()
        setFragmentResult(
            REQUEST_KEY,
            bundleOf(
                ARGS_TIME to time,
                ARGS_DAY_OF_WEEK to requireArguments().getString(ARGS_DAY_OF_WEEK),
                ARGS_IS_START_TIME to requireArguments().getBoolean(ARGS_IS_START_TIME)
            )
        )
        dismiss()
    }

    private fun TimePicker.getFriendlyTime(): String {
        val hour = formatValue(hour)
        val minute = formatValue(minute)
        return "$hour:$minute"
    }

    private fun formatValue(value: Int): String {
        return when (value) {
            in 0..9 -> "0$value"
            else -> value.toString()
        }
    }

    companion object {

        const val TAG = "SetTimeBottomSheet"
        const val REQUEST_KEY = "request_key_time"
        const val ARGS_TIME = "args_time"
        const val ARGS_DAY_OF_WEEK = "args_day_of_week"
        const val ARGS_IS_START_TIME = "args_is_start_time"

        fun show(manager: FragmentManager, dayOfWeek: String, time: String?, isStartTime: Boolean) {
            SetTimeBottomSheet().apply {
                arguments = bundleOf(
                    ARGS_TIME to time,
                    ARGS_DAY_OF_WEEK to dayOfWeek,
                    ARGS_IS_START_TIME to isStartTime
                )
            }.show(manager, TAG)
        }
    }
}