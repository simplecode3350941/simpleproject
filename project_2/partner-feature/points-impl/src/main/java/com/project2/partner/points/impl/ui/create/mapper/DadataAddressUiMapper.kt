package com.project2.partner.points.impl.ui.create.mapper

import com.project2.partner.points.api.domain.models.DadataAddressDomain
import com.project2.partner.points.impl.ui.create.models.DadataAddressUi

fun DadataAddressDomain.toPresentation() = DadataAddressUi(
    id = id,
    latitude = latitude,
    longitude = longitude,
    addressFull = addressFull,
    addressShort = addressShort,
    region = cityRegionCountry
)