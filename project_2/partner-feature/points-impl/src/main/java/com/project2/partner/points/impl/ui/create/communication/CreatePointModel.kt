package com.project2.partner.points.impl.ui.create.communication

import android.content.Context
import com.project2.partner.points.impl.ui.create.work_schedule_selection.adapter.DayWeek
import com.project2.partner.points.impl.ui.create.work_schedule_selection.adapter.WorkScheduleAdapterItem
import com.project2.partner.points.impl.ui.list.CategoryUi
import com.project2.partner.points.impl.ui.list.PointTypeUi
import com.project2.partner.points.impl.ui.list.ProductUi

data class CreatePointUi(
    val pointName: String = "",
    val types: HashSet<PointTypeUi> = hashSetOf(),
    val phoneNumber: String = "",
    val latitude: Double? = null,
    val longitude: Double? = null,
    val address: String? = null,
    val workSchedules: List<WorkScheduleAdapterItem.Schedule> = listOf(),
    val categorizedProducts: Map<CategoryUi, List<ProductUi>> = hashMapOf(),
    val typesWithCategories: HashSet<PointTypeUi> = hashSetOf(),
)

val CreatePointUi.friendlyPointTypes: String
    get() = when {
        types.isEmpty() -> ""
        else -> types.sortedBy { it.localizedName }.joinToString(", ") { it.localizedName }
    }

fun CreatePointUi.getFriendlyWorkSchedule(context: Context): String {
    val list = workSchedules.filter { it.startTime != null || it.endTime != null }

    if (list.isEmpty()) {
        return ""
    }

    return list.joinToString(", ") {
        val day = context.getString(it.dayOfWeek.resId)
        when {
            it.startTime != null && it.endTime != null -> "$day ${it.startTime}–${it.endTime}"
            else -> ""
        }
    }
}

fun CreatePointUi.workScheduleIsValid(): Boolean {
    val list = workSchedules.filter { it.startTime != null && it.endTime != null || it.isDayOff }
    return list.size == DayWeek.values().size
}