package com.project2.partner.points.impl.domain.price_list

import com.project2.partner.points.api.domain.models.PointTypeWithCategoryDomain
import com.project2.partner.points.api.domain.repository.PartnerPriceListRepository
import com.project2.partner.points.api.domain.usecase.price_list.GetPriceListForPaymentUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPriceListForPaymentUseCaseImpl @Inject constructor(
    private val repository: PartnerPriceListRepository
): GetPriceListForPaymentUseCase {

    override suspend fun invoke(pointId: String): Flow<List<PointTypeWithCategoryDomain>> {
        return repository.getPriceListByPointId(pointId)
    }
}
