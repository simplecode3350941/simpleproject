package com.project2.partner.points.impl.ui.create.communication

import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.partner.points.impl.ui.create.work_schedule_selection.adapter.WorkScheduleAdapterItem
import com.project2.partner.points.impl.ui.list.PointTypeUi
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CreatePointCommunicationViewModel @Inject constructor(
    override val exceptionHandler: CoroutineExceptionHandler
    ): BaseViewModel() {

    private val _createPointFlows = MutableStateFlow(CreatePointUi())
    val createPointFlows = _createPointFlows.asStateFlow()

    fun getPointTypes(): HashSet<PointTypeUi> {
        return _createPointFlows.value.types
    }

    fun setPointName(pointName: String) {
        modifyAndEmit {
            it.copy(pointName = pointName)
        }
    }

    fun setPointTypes(pointTypes: HashSet<PointTypeUi>) {
        modifyAndEmit {
            it.types.addAll(pointTypes)
            it.copy()
        }
    }

    fun setPhoneNumber(phoneNumber: String) {
        modifyAndEmit {
            it.copy(phoneNumber = phoneNumber)
        }
    }

    fun setWorkSchedules(workSchedules: List<WorkScheduleAdapterItem.Schedule>) {
        modifyAndEmit {
            it.copy(workSchedules = workSchedules)
        }
    }

    fun setAddress(latitude: Double, longitude: Double, address: String) {
        modifyAndEmit {
            it.copy(
                latitude = latitude,
                longitude = longitude,
                address = address,
            )
        }
    }

    private fun modifyAndEmit(onModifyPoint: (CreatePointUi) -> CreatePointUi) {
        launchOnViewModelScope {
            val pointUi = _createPointFlows.value
            withContext(coroutineContext) {
                _createPointFlows.value = onModifyPoint.invoke(pointUi)
            }
        }
    }
}