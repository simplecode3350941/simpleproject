package com.project2.partner.points.impl.domain.price_list

import com.project2.partner.points.api.domain.models.PointTypeDomain
import com.project2.partner.points.api.domain.models.PointTypeWithCategoryDomain
import com.project2.partner.points.api.domain.repository.PartnerPriceListRepository
import com.project2.partner.points.api.domain.usecase.price_list.GetPriceListByTypesUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPriceListByTypesUseCaseImpl @Inject constructor(
    private val repository: PartnerPriceListRepository
): GetPriceListByTypesUseCase {

    override suspend fun invoke(pointTypes: List<PointTypeDomain>): Flow<List<PointTypeWithCategoryDomain>> {
        return repository.getPriceListByTypes(pointTypes)
    }
}
