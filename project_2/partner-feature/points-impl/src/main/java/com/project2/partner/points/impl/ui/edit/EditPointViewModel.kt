package com.project2.partner.points.impl.ui.edit

import android.content.Context
import androidx.lifecycle.SavedStateHandle
import com.project2.core.presentation.di.assisted.AssistedViewModelFactory
import com.project2.partner.points.api.domain.usecase.GetPartnerDetailPointUseCase
import com.project2.partner.points.api.domain.usecase.UpdatePointUseCase
import com.project2.partner.points.impl.ui.create.AbsCreatePointViewModel
import com.project2.partner.points.impl.ui.create.UiState
import com.project2.partner.points.impl.ui.create.communication.CreatePointCommunication
import com.project2.partner.points.impl.ui.create.communication.CreatePointUi
import com.project2.partner.points.impl.ui.create.mapper.toCreatePointUi
import com.project2.partner.points.impl.ui.create.mapper.toDomain
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart

class EditPointViewModel @AssistedInject constructor(
    private val updatePoint: UpdatePointUseCase,
    override val exceptionHandler: CoroutineExceptionHandler,
    private val getDetailPoint: GetPartnerDetailPointUseCase,
    private val context: Context,
    @Assisted private val savedStateHandle: SavedStateHandle
): AbsCreatePointViewModel() {

    @AssistedFactory
    interface Factory : AssistedViewModelFactory<EditPointViewModel>

    val pointId: String
        get() = savedStateHandle[PARTNER_POINT_ID] ?: error("pointId is null")

    private val _pointUiState = MutableStateFlow<PointUiState>(PointUiState.Loading)
    val pointUiState = _pointUiState.asStateFlow().filterNotNull()

    private val _pointDetail = MutableSharedFlow<CreatePointUi>()
    val pointDetail = _pointDetail.asSharedFlow()

    init {
        getPoint(pointId)
    }

    override fun onSavePoint(point: CreatePointUi) {
        launchOnViewModelScope {
            updatePoint(point.toDomain(), pointId)
                .onStart { updateState(UiState.Loading) }
                .catch { updateState(UiState.Error(it)) }
                .collectLatest {
                    updateState(UiState.Success)
                }
        }
    }

    private fun getPoint(id: String) {
        launchOnViewModelScope {
            getDetailPoint(id)
                .onStart { _pointUiState.value = PointUiState.Loading }
                .map {
                    it.toCreatePointUi(context)
                }
                .catch {
                    _pointUiState.value = PointUiState.Error(it)
                }
                .collect { createPointUi ->
                    _pointDetail.emit(createPointUi)
                    _pointUiState.value = PointUiState.Success
                    CreatePointCommunication.setCreatePointUi(createPointUi)
                }
        }
    }

    companion object{
        const val PARTNER_POINT_ID = "partner_point_id"
    }
}

sealed interface PointUiState {
    object Loading : PointUiState
    object Success : PointUiState
    data class Error(val error: Throwable) : PointUiState
}