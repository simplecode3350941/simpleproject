package com.project2.partner.points.impl.ui.detail.bottom_sheet

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResult
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.dialog.BaseBottomSheetDialogFragment
import com.project2.partner.points.impl.R
import com.project2.partner.points.impl.databinding.PartnerPointMenuBottomSheetBinding
import com.project2.partner.points.impl.ui.detail.PointCommand

class PointMenuBottomSheet: BaseBottomSheetDialogFragment() {

    private val pointCommand: String?
        get() = requireArguments().getString(ARGS_ITEM, PointCommand.Activate.toString())

    override val synthetic = SyntheticBinding(PartnerPointMenuBottomSheetBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        synthetic.binding.textViewEdit.setOnClickListener {
            onClickItem(ITEM_EDIT)
        }

        synthetic.binding.textViewActivateHide.isVisible = pointCommand != null

        synthetic.binding.textViewActivateHide.setText(when(pointCommand) {
            PointCommand.Activate.toString() -> R.string.point_action_activate
            else -> R.string.point_action_hide
        })

        synthetic.binding.textViewActivateHide.setOnClickListener {
            onClickItem(pointCommand)
        }

        synthetic.binding.buttonCancel.setOnClickListener {
            dismiss()
        }
    }

    private fun onClickItem(action: String?) {
        setFragmentResult(
            REQUEST_KEY,
            bundleOf(ARGS_ITEM to action)
        )
        dismiss()
    }

    companion object {
        const val TAG = "PointMenuBottomSheet"

        const val REQUEST_KEY = "request_key_item"
        const val ARGS_ITEM = "args_item"

        const val ITEM_EDIT = "EDIT"

        fun show(manager: FragmentManager, pointCommand: String?) {
            PointMenuBottomSheet().apply {
                arguments = bundleOf(
                    ARGS_ITEM to pointCommand
                )
            }.show(manager, TAG)
        }
    }
}