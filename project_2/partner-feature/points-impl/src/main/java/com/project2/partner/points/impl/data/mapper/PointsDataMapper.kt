package com.project2.partner.points.impl.data.mapper

import com.project2.partner.points.api.data.remote.models.CategoryWithServicesData
import com.project2.partner.points.api.data.remote.models.PointDetailRequest
import com.project2.partner.points.api.data.remote.models.PointTimeData
import com.project2.partner.points.api.data.remote.models.PointTypeData
import com.project2.partner.points.api.data.remote.models.ScheduleData
import com.project2.partner.points.api.data.remote.models.ServiceData
import com.project2.partner.points.api.data.remote.models.TypeData
import com.project2.partner.points.api.domain.models.CategoryWithProductsDomain
import com.project2.partner.points.api.domain.models.CreatePointDomain
import com.project2.partner.points.api.domain.models.PointTimeDomain
import com.project2.partner.points.api.domain.models.PointTypeDomain
import com.project2.partner.points.api.domain.models.PointTypeWithCategoryDomain
import com.project2.partner.points.api.domain.models.ProductDomain
import com.project2.partner.points.api.domain.models.ScheduleDomain


fun CreatePointDomain.toData() = PointDetailRequest(
    id = id,
    address = address,
    latitude = latitude,
    longitude = longitude,
    name = name,
    phoneNumber = phoneNumber,
    status = null,
    schedule = schedule.map { it.toData() },
    pointTypes = types.map { it.toData() }
)

fun PointTypeWithCategoryDomain.toData() = TypeData(
    id = id,
    serviceTypes = categories.map { it.toData() },
    type = type.toData()
)

fun CategoryWithProductsDomain.toData() = CategoryWithServicesData(
    id = id,
    name = categoryName,
    services = products.map { it.toData() }
)

fun ProductDomain.toData() = ServiceData(
    id = id,
    name = name,
    price = price
)

fun ScheduleDomain.toData() = ScheduleData(
    dayOff = dayOff,
    timeFrom = timeFrom?.toData(),
    timeTo = timeTo?.toData(),
    workingDay = workingDay
)

fun PointTimeDomain.toData() = PointTimeData(
    hours = hours,
    minutes = minutes
)

fun PointTypeDomain.toData() = PointTypeData(
    localizedName = localizedName,
    type = type
)