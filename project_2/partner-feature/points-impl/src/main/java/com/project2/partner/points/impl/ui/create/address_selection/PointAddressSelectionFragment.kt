package com.project2.partner.points.impl.ui.create.address_selection

import android.os.Bundle
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.utils.common.lazyUnsafe
import com.project2.core.utils.view.hideKeyboard
import com.project2.core.utils.view.setOnClickDelayListener
import com.project2.core.utils.view.showKeyboard
import com.project2.partner.points.impl.R
import com.project2.partner.points.impl.databinding.FragmentPointAddressSelectionBinding
import com.project2.partner.points.impl.ui.create.communication.CreatePointCommunication
import com.project2.partner.points.impl.ui.create.models.DadataAddressUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

class PointAddressSelectionFragment: BaseMvvmFragment() {

    private val viewModel: PartnerPointAddressViewModel by viewModels {
        viewModelFactory
    }

    private val addressAdapter: PartnerPointAddressAdapter by lazyUnsafe {
        PartnerPointAddressAdapter(::onItemClick)
    }

    override fun getLayoutResId(): Int = R.layout.fragment_point_address_selection

    override val synthetic = SyntheticBinding(FragmentPointAddressSelectionBinding::inflate)

    override fun initUi(savedInstanceState: Bundle?) {

        synthetic.binding.buttonClear.setOnClickDelayListener {
            synthetic.binding.editTextSearch.text = null
        }

        synthetic.binding.textViewCancel.setOnClickDelayListener {
            popBack()
        }

        synthetic.binding.recyclerView.apply {
            adapter = addressAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            setHasFixedSize(true)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.uiState.collectLatest(::updateContentState)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.pointAddressFlow.collectLatest(::submitAddresses)
        }

        synthetic.binding.editTextSearch.doAfterTextChanged {
            viewModel.searchAddress(it.toString())
        }

        synthetic.binding.editTextSearch.requestFocus()
        synthetic.binding.editTextSearch.showKeyboard()

        CreatePointCommunication.getPointUi().address?.let {
            synthetic.binding.editTextSearch.setText(it)
        }
    }

    private fun onItemClick(item: DadataAddressUi) {
        lifecycleScope.launch {
            CreatePointCommunication.setAddress(item.latitude, item.longitude, item.addressFull)
        }
        popBack()
    }

    override fun onDestroyView() {
        synthetic.safeBinding?.editTextSearch?.hideKeyboard()
        super.onDestroyView()
    }

    private fun updateContentState(uiState: UiState) {
        Timber.d("updateContentState %s", uiState)
        when(uiState) {
            is UiState.Loading -> {
                synthetic.binding.progressBar.isVisible = true
                synthetic.binding.buttonClear.isInvisible = true
            }
            else -> {
                synthetic.binding.progressBar.isVisible = false
                synthetic.binding.buttonClear.isVisible = true
            }
        }
    }

    private fun submitAddresses(addressList: List<DadataAddressUi>) {
        Timber.d("submit list, size – %s", addressList.size)
        addressAdapter.submitList(addressList)
    }
}