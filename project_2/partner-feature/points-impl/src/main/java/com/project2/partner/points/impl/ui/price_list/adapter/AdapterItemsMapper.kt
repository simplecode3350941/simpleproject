package com.project2.partner.points.impl.ui.price_list.adapter

import com.project2.partner.points.impl.ui.list.CategoryWithProductsUi
import com.project2.partner.points.impl.ui.list.PointTypeUi
import com.project2.partner.points.impl.ui.list.ProductUi

fun ProductUi.toAdapterItem(isLast: Boolean) = ProductAdapterItem(
    product = this,
    isLastInCategory = isLast
)

fun CategoryWithProductsUi.toAdapterItem(pointTypeUi: PointTypeUi, isExpanded: Boolean = false) = CategoryAdapterItem(
    id = id,
    categoryName = categoryName,
    isExpanded = isExpanded,
    pointTypeUi = pointTypeUi
)