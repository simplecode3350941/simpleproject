package com.project2.partner.points.impl.ui

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.ColorRes
import com.project2.core.utils.common.lazyUnsafe
import com.project2.core.utils.view.getCompatTintedDrawable
import com.project2.partner.points.api.ui.list.models.PointStatus
import com.project2.partner.points.impl.R

private val Context.statusBgDrawable: Map<Int, Drawable?>
    get() {
        val drawables by lazyUnsafe {
            PointStatus.values().associate { status ->
                status.colorResId to getCompatTintedDrawable(
                    R.drawable.bg_rect_service_status,
                    status.colorResId
                )?.apply {
                    alpha = 32
                }
            }
        }

        return drawables
    }

fun Context.getStatusDrawable(@ColorRes colorResId: Int) = statusBgDrawable[colorResId]