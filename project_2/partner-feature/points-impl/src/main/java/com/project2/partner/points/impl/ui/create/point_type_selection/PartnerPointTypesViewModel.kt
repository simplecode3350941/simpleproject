package com.project2.partner.points.impl.ui.create.point_type_selection

import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.partner.points.api.domain.usecase.GetPointTypesUseCase
import com.project2.partner.points.impl.ui.list.PointTypeUi
import com.project2.partner.points.impl.ui.list.mapper.toPresentation
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

class PartnerPointTypesViewModel @Inject constructor(
    private val getPointTypesUseCase: GetPointTypesUseCase,
    override val exceptionHandler: CoroutineExceptionHandler,
): BaseViewModel() {

    private val _pointTypesFlow = MutableStateFlow<List<PointTypeUi>>(listOf())
    val pointTypesFlow = _pointTypesFlow.asStateFlow()

    private val _uiState = MutableStateFlow<UiState>(UiState.Loading)
    val uiState = _uiState.asStateFlow()

    init {
        getPointTypes()
    }

    fun getPointTypes() {
        launchOnViewModelScope {
            getPointTypesUseCase()
                .onStart { _uiState.value = UiState.Loading }
                .map { pointTypes ->
                    pointTypes.map { it.toPresentation() }
                }
                .catch { _uiState.value = UiState.Error(it) }
                .collect { pointTypes ->
                    _pointTypesFlow.value = pointTypes
                    _uiState.value = UiState.Success
                }
        }
    }
}

sealed interface UiState {
    object Loading: UiState
    object Success: UiState
    data class Error(val error: Throwable): UiState
}