package com.project2.partner.points.impl.ui.price_list

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.project2.core.presentation.adapter_delegate.DelegateAdapterItem
import com.project2.core.presentation.adapter_delegate.MultiTypeAdapter
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.utils.common.lazyUnsafe
import com.project2.partner.points.impl.R
import com.project2.partner.points.impl.databinding.FragmentPriceListBinding
import com.project2.partner.points.impl.ui.detail.di.DetailPointDependencies
import com.project2.partner.points.impl.ui.price_list.adapter.PriceListCategoryViewHolderDelegate
import com.project2.partner.points.impl.ui.price_list.adapter.PriceListProductViewHolderDelegate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

class PointPriceListFragment: BaseMvvmFragment() {

    private val viewModel: PointPriceListViewModel by assistedViewModel {
        findComponentDependencies<DetailPointDependencies>()
            .getPriceListViewModelFactory()
    }

    private val priceListAdapter: MultiTypeAdapter by lazyUnsafe {
        MultiTypeAdapter.Builder()
            .add(PriceListCategoryViewHolderDelegate())
            .add(PriceListProductViewHolderDelegate())
            .build()
    }

    private val args by navArgs<PointPriceListFragmentArgs>()

    private val pointTypeName: String
        get() = args.localizedName

    override fun getLayoutResId(): Int = R.layout.fragment_price_list

    override val synthetic = SyntheticBinding(FragmentPriceListBinding::inflate)

    override fun initUi(savedInstanceState: Bundle?) {

        synthetic.binding.toolbar.title = pointTypeName
        synthetic.binding.toolbar.setNavigationOnClickListener {
            popBack()
        }

        synthetic.binding.recyclerView.apply {
            adapter = priceListAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            setHasFixedSize(true)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.uiState.collectLatest(::updateContentState)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.priceListFlow.collectLatest(::submitPriceList)
        }
    }

    private fun updateContentState(uiState: UiState) {
        Timber.d("updateContentState %s", uiState)
        when(uiState) {
            is UiState.Loading -> showHideLoading(true)
            is UiState.Error -> {
                Timber.e(uiState.error)
                showHideLoading(false)
                showErrorView()
            }

            is UiState.Success -> {
                showHideLoading(false)
            }
        }
    }

    private fun showHideLoading(show: Boolean) {
        val recyclerView = synthetic.binding.recyclerView
        val progressBar = synthetic.binding.progressBar
        recyclerView.isVisible = !show
        progressBar.isVisible = show
    }

    private fun showErrorView() {
        val errorView = synthetic.binding.includeErrorView.errorView
        errorView.isVisible = true
        synthetic.binding.includeErrorView.buttonRefresh.setOnClickListener {
            errorView.isVisible = false
            viewModel.getPriceListByPayment()
        }
    }

    private fun submitPriceList(priceList: List<DelegateAdapterItem>) {
        priceListAdapter.submitList(priceList)
    }
}