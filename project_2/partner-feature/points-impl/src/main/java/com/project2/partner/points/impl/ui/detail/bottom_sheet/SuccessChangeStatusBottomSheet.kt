package com.project2.partner.points.impl.ui.detail.bottom_sheet

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.dialog.BaseBottomSheetDialogFragment
import com.project2.partner.points.impl.R
import com.project2.partner.points.impl.databinding.PartnerPointSuccessChangeStatusBottomSheetBinding
import com.project2.partner.points.impl.ui.detail.PointCommand

class SuccessChangeStatusBottomSheet: BaseBottomSheetDialogFragment() {

    private val command: String
        get() = requireArguments().getString(ARGS_COMMAND, PointCommand.Activate.toString())

    override val synthetic = SyntheticBinding(PartnerPointSuccessChangeStatusBottomSheetBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        synthetic.binding.buttonClose.setOnClickListener {
            dismiss()
        }

        when(command) {
            PointCommand.Activate.toString() -> {
                synthetic.binding.textViewMessage.setText(R.string.point_action_activate_success)
                synthetic.binding.imageView.setImageResource(R.drawable.ic_visibility_48dp)
            }
            PointCommand.Hide.toString() -> {
                synthetic.binding.textViewMessage.setText(R.string.point_action_hide_success)
                synthetic.binding.imageView.setImageResource(R.drawable.ic_visibility_off_48dp)
            }
        }
    }

    companion object {

        const val TAG = "SuccessChangeStatusBottomSheet"
        const val ARGS_COMMAND = "args_command"


        fun show(manager: FragmentManager, command: String) {
            SuccessChangeStatusBottomSheet().apply {
                arguments = bundleOf(
                    ARGS_COMMAND to command
                )
            }.show(manager, TAG)
        }
    }
}