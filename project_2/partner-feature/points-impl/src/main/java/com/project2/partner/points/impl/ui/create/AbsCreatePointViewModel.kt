package com.project2.partner.points.impl.ui.create

import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.partner.points.impl.ui.create.communication.CreatePointCommunication
import com.project2.partner.points.impl.ui.create.communication.CreatePointUi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filterNotNull

abstract class AbsCreatePointViewModel: BaseViewModel() {

    private val _uiState = MutableStateFlow<UiState?>(null)
    val uiState = _uiState.asStateFlow().filterNotNull()

    fun resetUiState() {
        _uiState.value = null
    }

    protected fun updateState(state: UiState) {
        _uiState.value = state
    }

    abstract fun onSavePoint(point: CreatePointUi)

    override fun onCleared() {
        super.onCleared()
        CreatePointCommunication.clear()
    }
}

sealed interface UiState {
    object Loading : UiState
    object Success : UiState
    data class Error(val error: Throwable) : UiState
}