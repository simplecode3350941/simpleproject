package com.project2.partner.points.impl.ui.detail.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.partner.points.impl.databinding.PartnerPointTypeItemViewBinding
import com.project2.partner.points.impl.ui.list.PointTypeUi


class PointTypesAdapter(private val onClickItemListener: (PointTypeUi) -> Unit): ListAdapter<PointTypeUi, PointTypesAdapter.ViewHolder>(
    DIFF_CALLBACK
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), onClickItemListener)
    }

    inner class ViewHolder(parent: ViewGroup) : BindingViewHolder<PartnerPointTypeItemViewBinding>(parent, PartnerPointTypeItemViewBinding::inflate) {

        fun bind(item: PointTypeUi, onClickItemListener: (PointTypeUi) -> Unit) = with(binding) {
            textViewPointType.text = item.localizedName
            itemView.setOnClickListener {
                onClickItemListener(item)
            }
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<PointTypeUi>() {
            override fun areItemsTheSame(oldItem: PointTypeUi, newItem: PointTypeUi): Boolean {
                return oldItem.type == newItem.type
            }

            override fun areContentsTheSame(oldItem: PointTypeUi, newItem: PointTypeUi): Boolean {
                return oldItem == newItem
            }
        }
    }
}