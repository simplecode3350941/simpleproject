package com.project2.partner.points.impl.domain.create

import com.project2.partner.points.api.domain.models.DadataAddressDomain
import com.project2.partner.points.api.domain.repository.PartnerDadataRepository
import com.project2.partner.points.api.domain.usecase.GetDadataAddressUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetDadataAddressUseCaseImpl @Inject constructor(private val repository: PartnerDadataRepository):
    GetDadataAddressUseCase {

    override suspend fun invoke(query: String): Flow<List<DadataAddressDomain>> {
        return repository.getAddress(query)
    }
}