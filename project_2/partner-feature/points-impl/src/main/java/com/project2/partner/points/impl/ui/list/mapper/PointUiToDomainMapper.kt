package com.project2.partner.points.impl.ui.list.mapper

import com.project2.partner.points.api.domain.models.PointTypeDomain
import com.project2.partner.points.impl.ui.list.PointTypeUi

fun PointTypeUi.toDomain() = PointTypeDomain(
    id = id,
    localizedName = localizedName,
    type = type
)
