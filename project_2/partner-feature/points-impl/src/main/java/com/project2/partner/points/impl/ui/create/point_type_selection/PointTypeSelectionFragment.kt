package com.project2.partner.points.impl.ui.create.point_type_selection

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.utils.common.lazyUnsafe
import com.project2.partner.points.impl.R
import com.project2.partner.points.impl.databinding.FragmentPointTypeSelectionBinding
import com.project2.partner.points.impl.ui.create.communication.CreatePointCommunication
import com.project2.partner.points.impl.ui.list.PointTypeUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

class PointTypeSelectionFragment: BaseMvvmFragment() {

    private val viewModel: PartnerPointTypesViewModel by viewModels {
        viewModelFactory
    }

    private val pointTypesAdapter: PartnerPointTypesAdapter by lazyUnsafe {
        val types = CreatePointCommunication.getPointTypes()
        PartnerPointTypesAdapter(types, ::onItemStateChanged)
    }

    private fun onItemStateChanged(item: PointTypeUi, isChecked: Boolean) {
        lifecycleScope.launch {
            CreatePointCommunication.setPointTypes(pointTypesAdapter.getCheckedItems())
        }
    }

    override fun getLayoutResId(): Int = R.layout.fragment_point_type_selection

    override val synthetic = SyntheticBinding(FragmentPointTypeSelectionBinding::inflate)

    override fun initUi(savedInstanceState: Bundle?) {

        synthetic.binding.toolbar.setNavigationOnClickListener {
            popBack()
        }

        synthetic.binding.buttonSave.setOnClickListener {
            popBack()
        }

        synthetic.binding.recyclerView.apply {
            adapter = pointTypesAdapter
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
            setHasFixedSize(true)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.uiState.collectLatest(::updateContentState)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.pointTypesFlow.collectLatest(::submitPointsTypes)
        }
    }

    private fun updateContentState(uiState: UiState) {
        Timber.d("updateContentState %s", uiState)
        when(uiState) {
            is UiState.Loading -> showHideLoading(true)
            is UiState.Error -> {
                Timber.e(uiState.error)
                showHideLoading(false)
                showErrorView()
            }

            is UiState.Success -> {
                showHideLoading(false)
            }
        }
    }

    private fun showHideLoading(show: Boolean) {
        val recyclerView = synthetic.binding.recyclerView
        val progressBar = synthetic.binding.progressBar
        recyclerView.isVisible = !show
        progressBar.isVisible = show
    }

    private fun showErrorView() {
        val errorView = synthetic.binding.includeErrorView.errorView
        errorView.isVisible = true
        synthetic.binding.includeErrorView.buttonRefresh.setOnClickListener {
            errorView.isVisible = false
            viewModel.getPointTypes()
        }
    }

    private fun submitPointsTypes(pointTypes: List<PointTypeUi>) {
        Timber.d("submitPointsTypes size – %s", pointTypes.size)
        pointTypesAdapter.submitList(pointTypes)
    }
}