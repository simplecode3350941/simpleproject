package com.project2.partner.points.impl.ui.create.work_schedule_selection.adapter

import android.view.View
import android.view.ViewGroup
import com.project2.core.presentation.adapter_delegate.ItemViewDelegate
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.partner.points.impl.R
import com.project2.partner.points.impl.databinding.PartnerPointWorkScheduleSelectionItemViewBinding

class WorkScheduleViewHolderDelegate(
    private val onDayOffClick: (DayWeek, Boolean) -> Unit,
    private val onStartTimeClick: (DayWeek, String?) -> Unit,
    private val onEndTimeClick: (DayWeek, String?) -> Unit,
) : ItemViewDelegate<WorkScheduleAdapterItem, WorkScheduleViewHolderDelegate.ViewHolder>(
    WorkScheduleAdapterItem::class.java
) {

    override fun onCreateViewHolder(parent: ViewGroup) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, item: WorkScheduleAdapterItem) {
        holder.bind(item)
    }

    inner class ViewHolder(parent: ViewGroup) : BindingViewHolder<PartnerPointWorkScheduleSelectionItemViewBinding>(
        parent,
        PartnerPointWorkScheduleSelectionItemViewBinding::inflate
    ) {

        fun bind(item: WorkScheduleAdapterItem) = with(binding) {
            val day = item.day

            textViewDayOfWeek.text = item.day.friendlyDayWeek
            textViewStartTime.text = day.startTime ?: getString(R.string.create_point_work_schedule_start_time)
            textViewEndTime.text = day.endTime ?: getString(R.string.create_point_work_schedule_end_time)

            textViewStartTime.startAlpha(if(day.isDayOff) 0.5f else 1f)
            textViewEndTime.startAlpha(if(day.isDayOff) 0.5f else 1f)

            checkboxDayOff.setOnCheckedChangeListener(null)
            checkboxDayOff.isChecked = day.isDayOff
            checkboxDayOff.setOnCheckedChangeListener { _, isChecked ->
                onDayOffClick.invoke(day.dayOfWeek, isChecked)
            }

            textViewStartTime.isEnabled = !day.isDayOff
            textViewEndTime.isEnabled = !day.isDayOff

            if(!day.isDayOff) {
                textViewStartTime.setOnClickListener {
                    onStartTimeClick.invoke(day.dayOfWeek, day.startTime)
                }
                textViewEndTime.setOnClickListener {
                    onEndTimeClick.invoke(day.dayOfWeek, day.endTime)
                }
            } else {
                textViewStartTime.setOnClickListener(null)
                textViewEndTime.setOnClickListener(null)
            }
        }

        private fun View.startAlpha(alpha: Float) {
            animate()
                .alpha(alpha)
                .setDuration(250)
                .start()
        }
    }

}