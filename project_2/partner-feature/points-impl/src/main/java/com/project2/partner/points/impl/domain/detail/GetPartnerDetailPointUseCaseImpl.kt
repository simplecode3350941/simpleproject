package com.project2.partner.points.impl.domain.detail

import com.project2.partner.points.api.domain.models.PointDetailDomain
import com.project2.partner.points.api.domain.repository.PartnerPointsRepository
import com.project2.partner.points.api.domain.usecase.GetPartnerDetailPointUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPartnerDetailPointUseCaseImpl @Inject constructor(
    private val pointsRepository: PartnerPointsRepository
    ): GetPartnerDetailPointUseCase {

    override suspend operator fun invoke(id: String?): Flow<PointDetailDomain> {
        return pointsRepository.getPartnerDetailPoint(id)
    }
}