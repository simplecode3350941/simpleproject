package com.project2.partner.points.impl.ui.list.di

import com.project2.core.presentation.di.ComponentDependencies
import com.project2.partner.points.api.data.remote.api.PartnerPointsApi
import com.project2.partner.points.api.domain.repository.PartnerPointsRepository
import com.project2.partner.points.api.domain.usecase.GetPartnerListPointsUseCase

interface ListPointsDependencies: ComponentDependencies {
    fun pointsApi(): PartnerPointsApi
    fun pointsRepository(): PartnerPointsRepository
    fun getPointsUseCases(): GetPartnerListPointsUseCase
}