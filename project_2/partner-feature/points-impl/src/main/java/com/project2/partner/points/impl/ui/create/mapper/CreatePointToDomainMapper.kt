package com.project2.partner.points.impl.ui.create.mapper

import com.project2.partner.points.api.domain.models.CategoryWithProductsDomain
import com.project2.partner.points.api.domain.models.CreatePointDomain
import com.project2.partner.points.api.domain.models.PointTimeDomain
import com.project2.partner.points.api.domain.models.PointTypeDomain
import com.project2.partner.points.api.domain.models.PointTypeWithCategoryDomain
import com.project2.partner.points.api.domain.models.ProductDomain
import com.project2.partner.points.api.domain.models.ScheduleDomain
import com.project2.partner.points.impl.ui.create.communication.CreatePointUi
import com.project2.partner.points.impl.ui.create.work_schedule_selection.adapter.WorkScheduleAdapterItem
import com.project2.partner.points.impl.ui.list.CategoryUi
import com.project2.partner.points.impl.ui.list.PointTypeUi
import com.project2.partner.points.impl.ui.list.ProductUi

fun CreatePointUi.toDomain() = CreatePointDomain(
    id = null,
    address = address ?: "",
    latitude = latitude ?: 0.0,
    longitude = longitude ?: 0.0,
    name = pointName,
    phoneNumber = phoneNumber,
    schedule = workSchedules.map { it.toDomain() },
    types = typesWithCategories.map { type -> type.toDomain(categorizedProducts) }
)

fun PointTypeUi.toDomain(categorizedProductMap: Map<CategoryUi, List<ProductUi>>): PointTypeWithCategoryDomain {
    val categoryUi = categorizedProductMap.keys.filter { it.pointTypeUi == this }

    val categorizedProducts = categoryUi.map {
        CategoryWithProductsDomain(
            id = it.id,
            categoryName = it.categoryName,
            products = categorizedProductMap[it]
                ?.filter { it.price != null }
                ?.map { it.toDomain() }
                ?: listOf()
        )
    }
    .filter { it.products.isNotEmpty() }

    return PointTypeWithCategoryDomain(
        id = requireNotNull(id),
        type = PointTypeDomain(id, localizedName, type),
        categories = categorizedProducts
    )
}

fun ProductUi.toDomain() = ProductDomain(
    id = id,
    name = name,
    price = price
)

fun WorkScheduleAdapterItem.Schedule.toDomain() = ScheduleDomain(
    dayOff = isDayOff,
    timeFrom = startTime?.toPointTimeDomain(),
    timeTo = endTime?.toPointTimeDomain(),
    workingDay = friendlyDayWeek
)

private fun String.toPointTimeDomain(): PointTimeDomain {
    val times = split(":").map { it.toInt() }
    return PointTimeDomain(
        hours = times[0],
        minutes = times[1]
    )
}