package com.project2.partner.points.impl.di

import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.core.network.BuildConfig
import com.project2.partner.points.api.data.remote.api.PartnerDadataApi
import com.project2.partner.points.api.data.remote.api.PartnerPointsApi
import com.project2.partner.points.api.data.remote.api.PartnerPriceListApi
import com.project2.partner.points.api.domain.models.PointType
import com.project2.partner.points.api.domain.repository.PartnerDadataRepository
import com.project2.partner.points.api.domain.repository.PartnerPointsRepository
import com.project2.partner.points.api.domain.repository.PartnerPriceListRepository
import com.project2.partner.points.api.domain.usecase.ActivatePointUseCase
import com.project2.partner.points.api.domain.usecase.GetDadataAddressUseCase
import com.project2.partner.points.api.domain.usecase.GetPartnerDetailPointUseCase
import com.project2.partner.points.api.domain.usecase.GetPartnerListPointsUseCase
import com.project2.partner.points.api.domain.usecase.GetPointTypesUseCase
import com.project2.partner.points.api.domain.usecase.HidePointUseCase
import com.project2.partner.points.api.domain.usecase.UpdatePointUseCase
import com.project2.partner.points.api.domain.usecase.point.CreatePointUseCase
import com.project2.partner.points.api.domain.usecase.price_list.GetPriceListByTypesUseCase
import com.project2.partner.points.api.domain.usecase.price_list.GetPriceListForPaymentUseCase
import com.project2.partner.points.impl.domain.create.UpdatePointUseCaseImpl
import com.project2.partner.points.impl.data.repository.PartnerDadataRepositoryImpl
import com.project2.partner.points.impl.data.repository.PartnerPointsRepositoryImpl
import com.project2.partner.points.impl.data.repository.PartnerPriceListRepositoryImpl
import com.project2.partner.points.impl.domain.create.CreatePointUseCaseImpl
import com.project2.partner.points.impl.domain.create.GetDadataAddressUseCaseImpl
import com.project2.partner.points.impl.domain.detail.ActivatePointUseCaseImpl
import com.project2.partner.points.impl.domain.detail.GetPartnerDetailPointUseCaseImpl
import com.project2.partner.points.impl.domain.detail.HidePointUseCaseImpl
import com.project2.partner.points.impl.domain.list.GetPartnerListPointsUseCaseImpl
import com.project2.partner.points.impl.domain.price_list.GetPriceListByTypesUseCaseImpl
import com.project2.partner.points.impl.domain.price_list.GetPriceListForPaymentUseCaseImpl
import com.project2.partner.points.impl.domain.selection.GetPointTypesUseCaseImpl
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.EnumJsonAdapter
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

inline fun <reified T> createApi(client: OkHttpClient, converterFactory: MoshiConverterFactory): T {
    val builder = Retrofit.Builder()
        .client(client)
        .baseUrl(BuildConfig.project2_API_URL)
        .addConverterFactory(converterFactory)
        .build()

    return builder.create(T::class.java)
}

@Module
class PointsModule {

    @Provides
    @Singleton
    @Named("points_moshi_converter")
    fun provideMoshiConverter(@Named("moshi") baseMoshi: Moshi): MoshiConverterFactory {
        val moshi = baseMoshi.newBuilder()
            .add(
                PointType::class.java,
                EnumJsonAdapter.create(PointType::class.java)
                    .withUnknownFallback(PointType.UNKNOWN)
            )
            .build()

        return MoshiConverterFactory.create(moshi)
    }

    @Provides
    @Singleton
    fun providePartnerPointsApi(
        @Named("partner_api") okHttpClient: OkHttpClient,
        @Named("points_moshi_converter") moshiConverterFactory: MoshiConverterFactory
    ): PartnerPointsApi {
        return createApi(okHttpClient, moshiConverterFactory)
    }

    @Provides
    @Singleton
    fun providePartnerPriceListApi(
        @Named("partner_api") okHttpClient: OkHttpClient,
        @Named("points_moshi_converter") moshiConverterFactory: MoshiConverterFactory
    ): PartnerPriceListApi {
        return createApi(okHttpClient, moshiConverterFactory)
    }

    @Provides
    @Singleton
    fun providePartnerDadataApi(
        @Named("partner_api") okHttpClient: OkHttpClient,
        @Named("points_moshi_converter") moshiConverterFactory: MoshiConverterFactory
    ): PartnerDadataApi {
        return createApi(okHttpClient, moshiConverterFactory)
    }

    @Provides
    fun providePointsRepository(
        pointsApi: PartnerPointsApi,
        @IoDispatcher dispatcher: CoroutineDispatcher
    ) : PartnerPointsRepository {
        return PartnerPointsRepositoryImpl(pointsApi, dispatcher)
    }

    @Provides
    fun providePriceListRepository(
        priceListApi: PartnerPriceListApi,
        @IoDispatcher dispatcher: CoroutineDispatcher
    ) : PartnerPriceListRepository {
        return PartnerPriceListRepositoryImpl(priceListApi, dispatcher)
    }

    @Provides
    fun provideDadataRepository(
        dadataApi: PartnerDadataApi,
        @IoDispatcher dispatcher: CoroutineDispatcher
    ) : PartnerDadataRepository {
        return PartnerDadataRepositoryImpl(dadataApi, dispatcher)
    }

    @Provides
    fun provideGetPointsUseCases(repository: PartnerPointsRepository): GetPartnerListPointsUseCase {
        return GetPartnerListPointsUseCaseImpl(repository)
    }

    @Provides
    fun provideGetPartnerDetailPointUseCase(
        repository: PartnerPointsRepository
    ): GetPartnerDetailPointUseCase {
        return GetPartnerDetailPointUseCaseImpl(repository)
    }

    @Provides
    fun provideHidePointUseCase(repository: PartnerPointsRepository): HidePointUseCase {
        return HidePointUseCaseImpl(repository)
    }

    @Provides
    fun provideActivatePointUseCase(repository: PartnerPointsRepository): ActivatePointUseCase {
        return ActivatePointUseCaseImpl(repository)
    }

    @Provides
    fun provideGetPointTypesUseCase(repository: PartnerPriceListRepository): GetPointTypesUseCase {
        return GetPointTypesUseCaseImpl(repository)
    }

    @Provides
    fun provideGetPriceListByTypesUseCase(
        repository: PartnerPriceListRepository
    ): GetPriceListByTypesUseCase {
        return GetPriceListByTypesUseCaseImpl(repository)
    }

    @Provides
    fun provideGetPriceListForPaymentUseCase(
        repository: PartnerPriceListRepository
    ): GetPriceListForPaymentUseCase {
        return GetPriceListForPaymentUseCaseImpl(repository)
    }

    @Provides
    fun provideGetAddressUseCase(repository: PartnerDadataRepository): GetDadataAddressUseCase {
        return GetDadataAddressUseCaseImpl(repository)
    }

    @Provides
    fun provideCreatePointUseCase(repository: PartnerPointsRepository): CreatePointUseCase {
        return CreatePointUseCaseImpl(repository)
    }

    @Provides
    fun provideUpdatePointUseCase(repository: PartnerPointsRepository): UpdatePointUseCase {
        return UpdatePointUseCaseImpl(repository)
    }
}