package com.project2.partner.points.impl.ui.list

import android.text.Spannable
import com.project2.core.utils.formatter.FormatUtils
import com.project2.partner.points.api.domain.models.PointType
import com.project2.partner.points.api.ui.list.models.PointStatus
import com.project2.partner.profile.api.data.remote.models.PartnerRole

data class PointDetailUi(
    val id: String,
    val address: String,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val phoneNumber: String,
    val photos: List<String>, // photo url`s
    val schedule: List<ScheduleUi>,
    val status: PointStatus,
    val types: List<PointTypeUi>,
    val partnerRole: PartnerRole
)

val PointDetailUi.friendlyName: String
    get() {
        val dots = "..."
        val maxLength = 50
        return when {
            name.length > maxLength -> name.replaceRange(maxLength, name.length, dots)
            else -> name
        }
    }

val PointDetailUi.pointTypes: String
    get() = types.joinToString(", ") { it.localizedName }


data class PointTypeWithCategoryUi(
    val id: String,
    val type: PointTypeUi,
    val categories: List<CategoryWithProductsUi>
)

data class CategoryWithProductsUi(
    val id: String,
    val categoryName: String,
    val products: List<ProductUi>
)

data class ProductUi(
    val id: String,
    val name: String,
    val price: Double?,
    val category: CategoryUi
)

val ProductUi.friendlyPrice: Spannable?
    get() = price?.let {
        FormatUtils.formattedFractionalPrice(it, sizeOfFractional = 12)
    }

data class CategoryUi(
    val id: String,
    val categoryName: String,
    val pointTypeUi: PointTypeUi
)

data class PointTypeUi(
    val id: String?,
    val localizedName : String,
    val type: PointType
)

data class ScheduleUi(
    val dayOff: Boolean,
    val timeFrom: String?,
    val timeTo: String?,
    val workingDay: String
)

val ScheduleUi.timeRange: String
    get() = "$timeFrom—$timeTo"

val ScheduleUi.is24hours: Boolean
    get() = timeFrom == "0:00" && timeTo == "0:00"