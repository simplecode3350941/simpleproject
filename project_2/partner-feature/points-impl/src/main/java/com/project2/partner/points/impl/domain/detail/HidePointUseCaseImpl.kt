package com.project2.partner.points.impl.domain.detail

import com.project2.partner.points.api.domain.models.PointDetailDomain
import com.project2.partner.points.api.domain.repository.PartnerPointsRepository
import com.project2.partner.points.api.domain.usecase.HidePointUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class HidePointUseCaseImpl @Inject constructor(
    private val pointsRepository: PartnerPointsRepository,
    ): HidePointUseCase {

    override suspend operator fun invoke(pointId: String): Flow<PointDetailDomain> {
        return pointsRepository.hidePoint(pointId)
    }
}