package com.project2.partner.points.impl.ui.price_list.adapter

import com.project2.core.presentation.adapter_delegate.DelegateAdapterItem
import com.project2.partner.points.impl.ui.list.PointTypeUi
import com.project2.partner.points.impl.ui.list.ProductUi

data class CategoryAdapterItem(
    val id: String,
    val categoryName: String,
    val isExpanded: Boolean,
    val pointTypeUi: PointTypeUi
) : DelegateAdapterItem {

    override fun id() = id

    override fun content() = "$categoryName$isExpanded"
}

data class ProductAdapterItem(
    var product: ProductUi,
    val isLastInCategory: Boolean
): DelegateAdapterItem {

    override fun id() = product.id

    override fun content() = product
}