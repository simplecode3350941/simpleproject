package com.project2.partner.points.impl.ui.list.mapper

import com.project2.partner.points.api.domain.models.CategoryWithProductsDomain
import com.project2.partner.points.api.domain.models.PointDetailDomain
import com.project2.partner.points.api.domain.models.PointShortDomain
import com.project2.partner.points.api.domain.models.PointTimeDomain
import com.project2.partner.points.api.domain.models.PointTypeDomain
import com.project2.partner.points.api.domain.models.PointTypeWithCategoryDomain
import com.project2.partner.points.api.domain.models.ProductDomain
import com.project2.partner.points.api.domain.models.ScheduleDomain
import com.project2.partner.points.api.ui.list.models.MODERATION_STATUSES
import com.project2.partner.points.api.ui.list.models.PointShortUi
import com.project2.partner.points.api.ui.list.models.PointStatus
import com.project2.partner.points.impl.ui.list.CategoryUi
import com.project2.partner.points.impl.ui.list.CategoryWithProductsUi
import com.project2.partner.points.impl.ui.list.PointDetailUi
import com.project2.partner.points.impl.ui.list.PointTypeUi
import com.project2.partner.points.impl.ui.list.PointTypeWithCategoryUi
import com.project2.partner.points.impl.ui.list.ProductUi
import com.project2.partner.points.impl.ui.list.ScheduleUi
import com.project2.partner.profile.api.data.remote.models.PartnerRole

fun String.toPointStatus(): PointStatus {
    return when (this) {
        "ACTIVE" -> PointStatus.ACTIVE
        "DECLINED" -> PointStatus.DECLINED
        "HIDDEN" -> PointStatus.HIDDEN
        in MODERATION_STATUSES -> PointStatus.MODERATION
        else -> PointStatus.ACTIVE
    }
}

fun PointShortDomain.toPresentation() = PointShortUi(
    id = id,
    name = name,
    address = address,
    pointTypes = pointTypes,
    formattedPointTypes = pointTypes.joinToString(", "),
    status = status.toPointStatus()
)

fun PointDetailDomain.toPresentation(partnerRole: PartnerRole) = PointDetailUi(
    id = id,
    address = address,
    latitude = latitude,
    longitude = longitude,
    name = name,
    phoneNumber = phoneNumber,
    photos = photos,
    schedule = schedule.map { schedule ->
       schedule.toPresentation()
    },
    status = status.toPointStatus(),
    types = types.map { pointType ->
        pointType.type.toPresentation()
    },
    partnerRole = partnerRole
)

fun PointTypeWithCategoryDomain.toPresentation() = PointTypeWithCategoryUi(
    id = id,
    type = type.toPresentation(),
    categories = categories.map { it.toPresentation(type.toPresentation()) }
)

fun CategoryWithProductsDomain.toPresentation(pointTypeUi: PointTypeUi) = CategoryWithProductsUi(
    id = id,
    categoryName = categoryName,
    products = products.map { it.toPresentation(CategoryUi(id, categoryName, pointTypeUi)) }
)

fun ProductDomain.toPresentation(category: CategoryUi) = ProductUi(
    id = id,
    name = name,
    price = price,
    category = category
)

fun PointTypeDomain.toPresentation() = PointTypeUi(
    id = id,
    localizedName = localizedName,
    type = type
)

fun ScheduleDomain.toPresentation(): ScheduleUi {

    fun PointTimeDomain.toPresentation(): String {

        val h = when (hours) {
            in 1..9 -> "0$hours"
            else -> hours.toString()
        }

        val m = when (minutes) {
            in 0..9 -> "0$minutes"
            else -> minutes.toString()
        }

        return "${h}:${m}"
    }

    return ScheduleUi(
        dayOff = dayOff,
        timeFrom = timeFrom?.toPresentation(),
        timeTo = timeTo?.toPresentation(),
        workingDay = workingDay,
    )
}