package com.project2.partner.points.impl.ui.list

import com.project2.core.presentation.paging.Paginator
import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.core.utils.common.toImmutableList
import com.project2.partner.points.api.domain.usecase.GetPartnerListPointsUseCase
import com.project2.partner.points.api.ui.list.models.PointShortUi
import com.project2.partner.points.impl.ui.list.mapper.toPresentation
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import timber.log.Timber
import javax.inject.Inject

class PartnerListServicesViewModel @Inject constructor(
    private val getListServicesUseCase: GetPartnerListPointsUseCase,
    override val exceptionHandler: CoroutineExceptionHandler
) : BaseViewModel() {

    private val _updateState = MutableStateFlow<UiState>(UiState.Loading)
    val updateState = _updateState.asStateFlow()

    private val _paginationState = MutableStateFlow<PaginationState?>(null)
    val paginationState = _paginationState.asStateFlow().filterNotNull()

    private val _pointsFlow = MutableStateFlow<List<PointShortUi>>(listOf())
    val pointsFlow = _pointsFlow.asStateFlow()

    private val points: List<PointShortUi>
        get() = _pointsFlow.value.toImmutableList()

    private val paginator = Paginator()
    private val isPagination: Boolean
        get() = paginator.getCurrentPage() > 0

    private fun getPoints() {
        launchOnViewModelScope {
            getListServicesUseCase(paginator.getCurrentPage(), paginator.pageSize)
                .loadingState()
                .map { services -> services.map { it.toPresentation() } }
                .errorState()
                .successState()
        }
    }

    fun refreshData() {
        paginator.reset()
        getPoints()
    }

    fun paginate() {
        if(paginator.hasData()) {
            getPoints()
        }
    }

    private fun <T> Flow<T>.loadingState(): Flow<T> {
        return onStart {
            when {
                isPagination -> _paginationState.value = PaginationState.Loading
                else -> _updateState.value = UiState.Loading
            }
        }
    }

    private fun <T> Flow<T>.errorState(): Flow<T> {
        return catch {
            when {
                isPagination -> _paginationState.value = PaginationState.Error(it)
                else -> _updateState.value = UiState.Error(it)
            }
        }
    }

    private suspend fun Flow<List<PointShortUi>>.successState() {
        collect {
            Timber.d("collect - %s", it.size)
            if (isPagination) {
                _paginationState.value = PaginationState.Success
                paginator.nextPage(it.size)
                _pointsFlow.value =  (points + it).toImmutableList()
            } else {
                if (it.isEmpty()) {
                    _updateState.value = UiState.Empty
                } else {
                    paginator.nextPage(it.size)
                    _updateState.value = UiState.Success
                    _pointsFlow.value = it
                }
            }
        }
    }
}

sealed interface UiState {
    object Loading: UiState
    object Success: UiState
    data class Error(val error: Throwable): UiState
    object Empty: UiState
}

sealed interface PaginationState {
    object Loading: PaginationState
    object Success: PaginationState
    data class Error(val error: Throwable): PaginationState
}