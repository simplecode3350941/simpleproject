package com.project2.partner.points.impl.ui.create.pricelist_selection.adapter

import android.view.ViewGroup
import androidx.core.view.isVisible
import com.project2.core.presentation.adapter_delegate.ItemViewDelegate
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.partner.points.impl.R
import com.project2.partner.points.impl.databinding.PartnerPriceListCategoryItemViewBinding
import com.project2.partner.points.impl.ui.price_list.adapter.CategoryAdapterItem

class CategoryViewHolderDelegate(private val onCategoryItemClick: (CategoryAdapterItem) -> Unit) :
    ItemViewDelegate<CategoryAdapterItem, CategoryViewHolderDelegate.ViewHolder>(
        CategoryAdapterItem::class.java
    ) {

    override fun onCreateViewHolder(parent: ViewGroup) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, item: CategoryAdapterItem) {
        holder.bind(item)
    }

    inner class ViewHolder(parent: ViewGroup) : BindingViewHolder<PartnerPriceListCategoryItemViewBinding>(
        parent,
        PartnerPriceListCategoryItemViewBinding::inflate
    ) {

        fun bind(item: CategoryAdapterItem) = with(binding) {
            textViewCategory.text = item.categoryName

            val iconResId = when {
                item.isExpanded -> R.drawable.ic_arrow_top_16dp
                else -> R.drawable.ic_arrow_down_16dp
            }

            textViewCategory.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, iconResId, 0)
            divider.isVisible = !item.isExpanded

            itemView.setOnClickListener {
                onCategoryItemClick.invoke(item)
            }
        }
    }

}