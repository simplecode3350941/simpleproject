package com.project2.partner.points.impl.ui.detail.di

import com.project2.core.presentation.di.ComponentDependencies
import com.project2.partner.points.api.data.remote.api.PartnerPointsApi
import com.project2.partner.points.api.domain.repository.PartnerPointsRepository
import com.project2.partner.points.api.domain.usecase.GetPartnerDetailPointUseCase
import com.project2.partner.points.api.domain.usecase.price_list.GetPriceListForPaymentUseCase
import com.project2.partner.points.impl.ui.detail.DetailPointViewModel
import com.project2.partner.points.impl.ui.edit.EditPointViewModel
import com.project2.partner.points.impl.ui.price_list.PointPriceListViewModel
import kotlinx.coroutines.CoroutineExceptionHandler

interface DetailPointDependencies: ComponentDependencies {
    fun getDetailPointViewModelFactory(): DetailPointViewModel.Factory
    fun getPriceListViewModelFactory(): PointPriceListViewModel.Factory
    fun getEditPointViewModelFactory(): EditPointViewModel.Factory
    fun pointsApi(): PartnerPointsApi
    fun pointsRepository(): PartnerPointsRepository
    fun getDetailPointUseCases(): GetPartnerDetailPointUseCase
    fun getPriceListForPaymentUseCase(): GetPriceListForPaymentUseCase
    fun coroutineExceptionHandler(): CoroutineExceptionHandler
}