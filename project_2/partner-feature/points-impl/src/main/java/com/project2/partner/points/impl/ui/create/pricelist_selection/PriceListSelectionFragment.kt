package com.project2.partner.points.impl.ui.create.pricelist_selection

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.project2.core.presentation.adapter_delegate.DelegateAdapterItem
import com.project2.core.presentation.adapter_delegate.MultiTypeAdapter
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.utils.common.lazyUnsafe
import com.project2.partner.points.impl.R
import com.project2.partner.points.impl.databinding.FragmentPriceListSelectionBinding
import com.project2.partner.points.impl.ui.create.communication.CreatePointCommunication
import com.project2.partner.points.impl.ui.create.pricelist_selection.adapter.CategoryViewHolderDelegate
import com.project2.partner.points.impl.ui.create.pricelist_selection.adapter.ProductViewHolderDelegate
import com.project2.partner.points.impl.ui.create.pricelist_selection.bottom_sheet.SetPriceBottomSheet
import com.project2.partner.points.impl.ui.price_list.UiState
import com.project2.partner.points.impl.ui.price_list.adapter.CategoryAdapterItem
import com.project2.partner.points.impl.ui.price_list.adapter.ProductAdapterItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

class PriceListSelectionFragment: BaseMvvmFragment() {

    private val viewModel: PriceListSelectionViewModel by viewModels {
        viewModelFactory
    }

    private val priceListAdapter: MultiTypeAdapter by lazyUnsafe {
        MultiTypeAdapter.Builder()
            .add(CategoryViewHolderDelegate(::onCategoryItemClick))
            .add(ProductViewHolderDelegate(::onAddPriceClick, ::onRemovePriceClick))
            .build()
    }

    private fun onCategoryItemClick(item: CategoryAdapterItem) {
        viewModel.onCategoryClicked(item)
    }

    private fun onAddPriceClick(item: ProductAdapterItem) {
        val currentPrice = item.product.price?.toString()
        SetPriceBottomSheet.show(childFragmentManager, currentPrice, item.product.name, item.id())
    }

    private fun onRemovePriceClick(item: ProductAdapterItem) {
        viewModel.setItemPrice(item.id(), price = null)
    }

    override fun getLayoutResId(): Int = R.layout.fragment_price_list_selection

    override val synthetic = SyntheticBinding(FragmentPriceListSelectionBinding::inflate)

    override fun initUi(savedInstanceState: Bundle?) {

        synthetic.binding.toolbar.setNavigationOnClickListener {
            popBack()
        }

        synthetic.binding.buttonSave.setOnClickListener {
            popBack()
        }

        synthetic.binding.recyclerView.apply {
            adapter = priceListAdapter
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            setHasFixedSize(true)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.uiState.collectLatest(::updateContentState)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.priceListFlow.collectLatest(::submitPriceList)
        }


        childFragmentManager.setFragmentResultListener(SetPriceBottomSheet.REQUEST_KEY, viewLifecycleOwner) { _, bundle ->
            val price = bundle.getDouble(SetPriceBottomSheet.ARGS_PRICE).takeIf { it > 0.0 }
            val productId = bundle.getString(SetPriceBottomSheet.ARGS_PRODUCT_ID)
            if(productId != null) {
                viewModel.setItemPrice(productId, price)
            }
        }

        viewModel.getPriceListByTypes(CreatePointCommunication.getPointTypes().toList())
    }

    private fun updateContentState(uiState: UiState) {
        Timber.d("updateContentState %s", uiState)
        when(uiState) {
            is UiState.Loading -> showHideLoading(true)
            is UiState.Error -> {
                Timber.e(uiState.error)
                showHideLoading(false)
                showErrorView()
            }

            is UiState.Success -> {
                showHideLoading(false)
            }
        }
    }

    private fun showHideLoading(show: Boolean) {
        val recyclerView = synthetic.binding.recyclerView
        val progressBar = synthetic.binding.progressBar
        recyclerView.isVisible = !show
        progressBar.isVisible = show
    }

    private fun showErrorView() {
        val errorView = synthetic.binding.includeErrorView.errorView
        errorView.isVisible = true
        synthetic.binding.includeErrorView.buttonRefresh.setOnClickListener {
            errorView.isVisible = false
            //viewModel.getPriceListByTypes(listOf())
        }
    }

    private fun submitPriceList(pointTypes: List<DelegateAdapterItem>) {
        Timber.d("submitPointsTypes size – %s", pointTypes.size)
        priceListAdapter.submitList(pointTypes)
    }
}