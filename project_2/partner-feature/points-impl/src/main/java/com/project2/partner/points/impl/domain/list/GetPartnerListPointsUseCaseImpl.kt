package com.project2.partner.points.impl.domain.list

import com.project2.partner.points.api.domain.models.PointShortDomain
import com.project2.partner.points.api.domain.repository.PartnerPointsRepository
import com.project2.partner.points.api.domain.usecase.GetPartnerListPointsUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPartnerListPointsUseCaseImpl @Inject constructor(
    private val repository: PartnerPointsRepository
) : GetPartnerListPointsUseCase {

    override suspend operator fun invoke(
        pageNumber: Int,
        pageSize: Int,
        status: String?,
        global: String?
    ): Flow<List<PointShortDomain>> {
        return repository.getPartnerListPoints(pageNumber, pageSize, status, global)
    }
}