package com.project2.partner.points.impl.ui.create.point_type_selection

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.partner.points.impl.databinding.PartnerPointTypeSelectionItemViewBinding
import com.project2.partner.points.impl.ui.list.PointTypeUi

class PartnerPointTypesAdapter(
    items: Collection<PointTypeUi>,
    private val onItemChecked: (PointTypeUi, Boolean) -> Unit
) : ListAdapter<PointTypeUi, PartnerPointTypesAdapter.ViewHolder>(
    DIFF_CALLBACK
) {
    private val checkedItems = items.toHashSet()

    fun getCheckedItems() = checkedItems

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(parent: ViewGroup) : BindingViewHolder<PartnerPointTypeSelectionItemViewBinding>(
        parent,
        PartnerPointTypeSelectionItemViewBinding::inflate
    ) {
        fun bind(item: PointTypeUi) = with(binding) {
            checkbox.text = item.localizedName
            checkbox.isChecked = checkedItems.contains(item)

            checkbox.setOnCheckedChangeListener { _, isChecked ->
                if (checkedItems.contains(item)) {
                    checkedItems.remove(item)
                } else {
                    checkedItems.add(item)
                }
                onItemChecked(item, isChecked)
            }
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<PointTypeUi>() {
            override fun areItemsTheSame(oldItem: PointTypeUi, newItem: PointTypeUi): Boolean {
                return oldItem.type == newItem.type
            }

            override fun areContentsTheSame(oldItem: PointTypeUi, newItem: PointTypeUi): Boolean {
                return oldItem == newItem
            }
        }
    }
}