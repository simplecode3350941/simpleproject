package com.project2.partner.points.impl.domain.create

import com.project2.partner.points.api.domain.models.CreatePointDomain
import com.project2.partner.points.api.domain.repository.PartnerPointsRepository
import com.project2.partner.points.api.domain.usecase.UpdatePointUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class UpdatePointUseCaseImpl @Inject constructor(
    private val repository: PartnerPointsRepository
) : UpdatePointUseCase {

    override suspend fun invoke(point: CreatePointDomain, pointId: String): Flow<Boolean> {
        return repository.updatePoint(point, pointId)
    }
}