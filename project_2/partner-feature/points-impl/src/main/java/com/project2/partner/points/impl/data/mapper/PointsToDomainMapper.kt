package com.project2.partner.points.impl.data.mapper

import com.project2.partner.points.api.data.remote.models.CategoryWithServicesData
import com.project2.partner.points.api.data.remote.models.PointDetailData
import com.project2.partner.points.api.data.remote.models.PointShortData
import com.project2.partner.points.api.data.remote.models.PointTimeData
import com.project2.partner.points.api.data.remote.models.PointTypeData
import com.project2.partner.points.api.data.remote.models.ScheduleData
import com.project2.partner.points.api.data.remote.models.ServiceData
import com.project2.partner.points.api.data.remote.models.TypeData
import com.project2.partner.points.api.domain.models.CategoryWithProductsDomain
import com.project2.partner.points.api.domain.models.PointDetailDomain
import com.project2.partner.points.api.domain.models.PointShortDomain
import com.project2.partner.points.api.domain.models.PointTimeDomain
import com.project2.partner.points.api.domain.models.PointTypeDomain
import com.project2.partner.points.api.domain.models.PointTypeWithCategoryDomain
import com.project2.partner.points.api.domain.models.ProductDomain
import com.project2.partner.points.api.domain.models.ScheduleDomain

fun PointShortData.toDomain() = PointShortDomain(
    id = id,
    name = name,
    address = address,
    pointTypes = types.map { it.localizedName },
    status = status
)

fun PointDetailData.toDomain() = PointDetailDomain(
    id = id,
    address = address,
    latitude = latitude,
    longitude = longitude,
    name = name,
    phoneNumber = phoneNumber,
    photos = photos?.map { it.imageURI } ?: listOf(),
    schedule = schedule.map { it.toDomain() },
    status = status,
    types = pointTypes.map { it.toDomain() }
)

fun TypeData.toDomain() = PointTypeWithCategoryDomain(
    id = id,
    type = type.toDomain(id),
    categories = serviceTypes.map { it.toDomain() }
)

fun CategoryWithServicesData.toDomain() = CategoryWithProductsDomain(
    id = id,
    categoryName = name,
    products = services.map { it.toDomain() }
)

fun ServiceData.toDomain() = ProductDomain(
    id = id,
    name = name,
    price = price
)

fun ScheduleData.toDomain() = ScheduleDomain(
    dayOff = dayOff,
    timeFrom = timeFrom?.toDomain(),
    timeTo = timeTo?.toDomain(),
    workingDay = workingDay
)

fun PointTimeData.toDomain() = PointTimeDomain(
    hours = hours,
    minutes = minutes
)

fun PointTypeData.toDomain(id: String?) = PointTypeDomain(
    id = id,
    localizedName = localizedName,
    type = type
)