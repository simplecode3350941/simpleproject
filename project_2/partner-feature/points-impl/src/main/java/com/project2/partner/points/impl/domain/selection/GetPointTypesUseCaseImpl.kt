package com.project2.partner.points.impl.domain.selection

import com.project2.partner.points.api.domain.models.PointTypeDomain
import com.project2.partner.points.api.domain.repository.PartnerPriceListRepository
import com.project2.partner.points.api.domain.usecase.GetPointTypesUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPointTypesUseCaseImpl @Inject constructor(
    private val priceListRepository: PartnerPriceListRepository
    ): GetPointTypesUseCase {

    override suspend operator fun invoke(): Flow<List<PointTypeDomain>> {
        return priceListRepository.getPointsType()
    }
}