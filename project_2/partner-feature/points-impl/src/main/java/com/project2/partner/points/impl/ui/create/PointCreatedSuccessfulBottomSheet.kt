package com.project2.partner.points.impl.ui.create

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResult
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.dialog.BaseBottomSheetDialogFragment
import com.project2.partner.points.impl.databinding.PartnerPointCreatedSuccessfulBottomSheetBinding

class PointCreatedSuccessfulBottomSheet: BaseBottomSheetDialogFragment() {

    override val synthetic = SyntheticBinding(PartnerPointCreatedSuccessfulBottomSheetBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (dialog as? BottomSheetDialog)?.let {
            it.setOnCancelListener { sendResult() }
        }

        synthetic.binding.buttonClose.setOnClickListener {
            sendResult()
        }
    }

    private fun sendResult() {
        setFragmentResult(REQUEST_KEY, bundleOf())
        dismiss()
    }

    companion object {

        const val TAG = "PointCreatedSuccessBottomSheet"
        const val REQUEST_KEY = "request_key"


        fun show(manager: FragmentManager) {
            PointCreatedSuccessfulBottomSheet().show(manager, TAG)
        }
    }
}