package com.project2.partner.points.impl.ui.create.pricelist_selection

import com.project2.core.presentation.adapter_delegate.DelegateAdapterItem
import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.core.utils.common.toImmutableList
import com.project2.partner.points.api.domain.usecase.price_list.GetPriceListByTypesUseCase
import com.project2.partner.points.impl.ui.create.communication.CreatePointCommunication
import com.project2.partner.points.impl.ui.list.CategoryUi
import com.project2.partner.points.impl.ui.list.PointTypeUi
import com.project2.partner.points.impl.ui.list.mapper.toDomain
import com.project2.partner.points.impl.ui.list.mapper.toPresentation
import com.project2.partner.points.impl.ui.price_list.UiState
import com.project2.partner.points.impl.ui.price_list.adapter.CategoryAdapterItem
import com.project2.partner.points.impl.ui.price_list.adapter.ProductAdapterItem
import com.project2.partner.points.impl.ui.price_list.adapter.toAdapterItem
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PriceListSelectionViewModel @Inject constructor(
    private val getPriceListByTypes: GetPriceListByTypesUseCase,
    override val exceptionHandler: CoroutineExceptionHandler,
): BaseViewModel() {

    private val _priceListFlow = MutableStateFlow<List<DelegateAdapterItem>>(listOf())
    val priceListFlow = _priceListFlow.asStateFlow()

    private val _uiState = MutableStateFlow<UiState>(UiState.Loading)
    val uiState = _uiState.asStateFlow()

    private var typesWithCategories = mutableListOf<PointTypeUi>()
    private val categorizedProducts = hashMapOf<CategoryUi, List<ProductAdapterItem>>()

    private fun getCurrentList() = _priceListFlow.value.toMutableList()

    fun getPriceListByTypes(types: List<PointTypeUi>) {
        launchOnViewModelScope {
            val pointTypes = types.map { it.toDomain() }

            getPriceListByTypes(pointTypes)
                .onStart { _uiState.value = UiState.Loading }
                .map { it.map { pointType -> pointType.toPresentation() } }
                .map { typesAndCategories ->
                    typesWithCategories.clear()
                    categorizedProducts.clear()
                    typesWithCategories.addAll(typesAndCategories.map { it.type })

                    val categoriesWithProducts = typesAndCategories.flatMap { it.categories }

                    categoriesWithProducts.map {
                        val pointTypeUi = it.products.first().category.pointTypeUi

                        val products = it.products.mapIndexed { index, productUi ->
                            productUi.toAdapterItem(index == it.products.size - 1)
                        }

                        val categoryAdapterItem = it.toAdapterItem(pointTypeUi = pointTypeUi)

                        val categoryUi = CategoryUi(categoryAdapterItem.id, categoryAdapterItem.categoryName, pointTypeUi)
                        val filledProducts = CreatePointCommunication.getPointUi().categorizedProducts[categoryUi]

                        categorizedProducts[categoryUi] = products
                        filledProducts?.forEach { product ->
                            categorizedProducts[categoryUi]?.forEachIndexed { index, item ->
                                if (item.product.id == product.id) {
                                    item.product = product
                                }
                            }
                        }
                        categoryAdapterItem
                    }

                }
                .catch { _uiState.value = UiState.Error(it) }
                .collect { priceList ->
                    _priceListFlow.value = priceList
                    _uiState.value = UiState.Success
                }
        }
    }

    fun onCategoryClicked(category: CategoryAdapterItem) {
        launchOnViewModelScope {
            val productItems = categorizedProducts[category.toCategoryUi()]

            productItems?.let { products ->
                val currentList = getCurrentList()
                val categoryIndex = currentList.indexOf(category)
                currentList[categoryIndex] = category.copy(isExpanded = !category.isExpanded)
                if(category.isExpanded) {
                    currentList.removeAll(products)
                } else {
                    currentList.addAll(categoryIndex + 1, products)
                }

                withContext(coroutineContext) {
                    _priceListFlow.value = currentList.toImmutableList()
                }
            }
        }
    }

    fun setItemPrice(itemId: String, price: Double?) {
        launchOnViewModelScope {
            val currentList = getCurrentList()
            val index = currentList.indexOfFirst { it.id() == itemId }
            val newProduct = (currentList.find { it.id() == itemId } as? ProductAdapterItem)?.let {
                it.copy(product = it.product.copy(price = price))
            }

            newProduct?.let { newProductItem ->
                currentList[index] = newProductItem
                categorizedProducts[newProductItem.product.category]?.let { items ->
                    val products = items.toMutableList()
                    val product = products.find { it.id() == itemId }
                    product?.let {
                        products[products.indexOf(it)] = newProductItem
                    }

                    categorizedProducts[newProductItem.product.category] = products.toImmutableList()
                }
            }

            withContext(coroutineContext) {
                sendPriceList()
                _priceListFlow.value = currentList.toImmutableList()
            }
        }
    }

    private suspend fun sendPriceList() {
        val products = categorizedProducts.mapValues  { it.value.map { it.product } }.toMutableMap()
        products.forEach { entry ->
            val newValues = entry.value.filter { it.price != null }
            products[entry.key] = newValues
        }

        CreatePointCommunication.setPriceList(typesWithCategories.toHashSet(), products)
    }

    private fun CategoryAdapterItem.toCategoryUi() = CategoryUi(
        id = id,
        categoryName = categoryName,
        pointTypeUi = pointTypeUi
    )
}