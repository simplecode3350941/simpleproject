package com.project2.partner.points.impl.ui.create.models

data class DadataAddressUi(
    val id: Long,
    val latitude: Double,
    val longitude: Double,
    val addressFull: String,
    val addressShort: String,
    val region: String
)

val DadataAddressUi.address: String
    get() = addressFull/*addressShort.ifEmpty {
        addressFull
    }*/