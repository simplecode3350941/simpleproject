package com.project2.partner.points.impl.ui.create

import com.project2.partner.points.impl.ui.create.communication.CreatePointUi

interface CreatePointView {

    fun getPointViewModel(): AbsCreatePointViewModel

    fun onInitViews()
    fun onClickTypeSelection()
    fun onClickAddressSelection()
    fun onClickWorkScheduleSelection()
    fun onClickPriceListSelection()
    fun onSavePoint(point: CreatePointUi)
    fun onSuccess()
}