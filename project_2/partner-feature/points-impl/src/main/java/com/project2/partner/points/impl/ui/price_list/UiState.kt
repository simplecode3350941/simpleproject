package com.project2.partner.points.impl.ui.price_list

sealed interface UiState {
    object Loading: UiState
    object Success: UiState
    data class Error(val error: Throwable): UiState
}