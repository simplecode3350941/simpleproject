package com.project2.partner.points.impl.ui.create.pricelist_selection.bottom_sheet

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResult
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.dialog.BaseBottomSheetDialogFragment
import com.project2.core.utils.formatter.Currency
import com.project2.core.utils.formatter.FormatUtils.toFormattedPrice
import com.project2.core.utils.view.showKeyboard
import com.project2.partner.points.impl.R
import com.project2.partner.points.impl.databinding.PartnerPointSetPriceBottomSheetBinding

class SetPriceBottomSheet: BaseBottomSheetDialogFragment() {

    private val productName: String
        get() = requireArguments().getString(ARGS_PRODUCT_NAME, "")

    private val currentPrice: String
        get() = requireArguments().getString(ARGS_PRICE) ?: ""

    private val priceHint: String
        get() {
            val minPrice = MIN_PRICE.toFormattedPrice(Currency.RUB.toString())
            val maxPrice = MAX_PRICE.toFormattedPrice(Currency.RUB.toString())

            return getString(R.string.create_point_price_list_selection_hint, minPrice, maxPrice)
        }

    override val synthetic = SyntheticBinding(PartnerPointSetPriceBottomSheetBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        synthetic.binding.textViewTitle.text = productName
        val buttonAdd =  synthetic.binding.buttonAdd

        buttonAdd.setOnClickListener {
            sendResult()
        }

        val inputLayout = synthetic.binding.inputLayout
        inputLayout.hint = priceHint
        synthetic.binding.editTextPrice.requestFocus()
        synthetic.binding.editTextPrice.showKeyboard()

        synthetic.binding.editTextPrice.setText(currentPrice)
        synthetic.binding.editTextPrice.doAfterTextChanged { text ->
            val price = text?.ifEmpty { "0" }.toString().toDouble()
            buttonAdd.isEnabled = price in MIN_PRICE..MAX_PRICE
            inputLayout.error = when {
                buttonAdd.isEnabled  -> null
                else -> getString(R.string.create_point_price_list_selection_error)
            }
        }
    }

    private fun sendResult() {
        val value = synthetic.binding.editTextPrice.text
        val price = when {
            value.isNullOrEmpty() -> null
            else -> value.toString().toDouble()
        }
        setFragmentResult(
            REQUEST_KEY,
            bundleOf(
                ARGS_PRICE to price,
                ARGS_PRODUCT_ID to requireArguments().getString(ARGS_PRODUCT_ID),
            )
        )
        dismiss()
    }

    companion object {

        const val TAG = "SetPriceBottomSheet"
        const val REQUEST_KEY = "request_key_price"
        const val ARGS_PRODUCT_NAME = "args_product_name"
        const val ARGS_PRODUCT_ID = "args_product_id"
        const val ARGS_PRICE = "args_price"
        const val MIN_PRICE = 100.0
        const val MAX_PRICE = 1_000_000.0

        fun show(manager: FragmentManager, currentPrice: String?, productName: String, productId: String) {
            SetPriceBottomSheet().apply {
                arguments = bundleOf(
                    ARGS_PRODUCT_NAME to productName,
                    ARGS_PRODUCT_ID to productId,
                    ARGS_PRICE to currentPrice
                )
            }.show(manager, TAG)
        }
    }
}