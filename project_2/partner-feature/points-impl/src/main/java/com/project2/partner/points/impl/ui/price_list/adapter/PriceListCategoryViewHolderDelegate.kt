package com.project2.partner.points.impl.ui.price_list.adapter

import android.view.ViewGroup
import com.project2.core.presentation.adapter_delegate.ItemViewDelegate
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.partner.points.impl.databinding.PartnerDetailPointPriceListCategoryItemViewBinding

class PriceListCategoryViewHolderDelegate: ItemViewDelegate<CategoryAdapterItem, PriceListCategoryViewHolderDelegate.ViewHolder>(
    CategoryAdapterItem::class.java) {

    override fun onCreateViewHolder(parent: ViewGroup) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, item: CategoryAdapterItem) {
        holder.bind(item)
    }

    inner class ViewHolder(parent: ViewGroup) : BindingViewHolder<PartnerDetailPointPriceListCategoryItemViewBinding>(
        parent,
        PartnerDetailPointPriceListCategoryItemViewBinding::inflate
    ) {

        fun bind(item: CategoryAdapterItem) = with(binding) {
            textViewCategory.text = item.categoryName
        }
    }

}