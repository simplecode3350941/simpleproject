package com.project2.partner.points.impl.ui.edit

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.lifecycleScope
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.utils.formatter.FormatUtils.toFormattedPhoneNumber
import com.project2.partner.points.impl.R
import com.project2.partner.points.impl.ui.create.CreatePointFragment
import com.project2.partner.points.impl.ui.create.communication.CreatePointCommunication
import com.project2.partner.points.impl.ui.create.communication.CreatePointUi
import com.project2.partner.points.impl.ui.detail.di.DetailPointDependencies
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

class EditPointFragment: CreatePointFragment() {

    private val viewModel: EditPointViewModel by assistedViewModel {
        findComponentDependencies<DetailPointDependencies>()
            .getEditPointViewModelFactory()
    }

    override fun getPointViewModel() = viewModel

    private var originalCreatePointUi = CreatePointUi()

    override fun initUi(savedInstanceState: Bundle?) {
        super.initUi(savedInstanceState)
        viewLifecycleOwner.lifecycleScope.launch {
            getPointViewModel().pointDetail.collectLatest(::showPointInfo)
        }

        viewLifecycleOwner.lifecycleScope.launch {
            getPointViewModel().pointUiState.collectLatest(::updateLoadPointInfoUiState)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            CreatePointCommunication.subscribe {
                synthetic.binding.buttonCreatePoint.isVisible = isButtonSaveVisible()
            }
        }
    }

    override fun onInitViews() {
        super.onInitViews()
        synthetic.binding.apply {
            toolbar.title = getString(R.string.edit)
            buttonCreatePoint.text = getString(R.string.save)
            buttonCreatePoint.isVisible = false

            editTextPointName.addTextChangedListener {
                buttonCreatePoint.isVisible = isButtonSaveVisible()
            }

            editTextContactPhone.addTextChangedListener {
                buttonCreatePoint.isVisible = isButtonSaveVisible()
            }
        }
    }

    override fun onClickTypeSelection() {
        navigateTo(R.id.action_editPoint_to_pointTypeSelection)
    }

    override fun onClickAddressSelection() {
        navigateTo(R.id.action_editPoint_to_pointAddressSelection)
    }

    override fun onClickPriceListSelection() {
        navigateTo(R.id.action_editPoint_to_priceListSelection)
    }

    override fun onClickWorkScheduleSelection() {
        navigateTo(R.id.action_editPoint_to_workScheduleSelection)
    }

    override fun onSuccess() {
        popBack()
    }

    private fun isButtonSaveVisible(): Boolean {
        val currentPointName = synthetic.binding.editTextPointName.text.toString()
        val currentPointPhone = synthetic.binding.editTextContactPhone.text.toString().toFormattedPhoneNumber()

        val currentUiModel = CreatePointCommunication.getPointUi().copy(
            pointName = currentPointName,
            phoneNumber = currentPointPhone
        )

        return currentUiModel != originalCreatePointUi
    }

    private fun showPointInfo(point: CreatePointUi) {
        originalCreatePointUi = point
        synthetic.binding.apply {
            buttonCreatePoint.isVisible = false
            editTextPointName.setText(point.pointName)
            editTextContactPhone.setText(point.phoneNumber)
        }
    }

    private fun updateLoadPointInfoUiState(pointState: PointUiState) {
        Timber.d("pointState $pointState")
        when(pointState) {
            is PointUiState.Loading -> {
                showHideProgressBar(true)
            }
            is PointUiState.Error -> {
                showHideProgressBar(false)
                showCommonErrorSnack(pointState.error)
            }
            is PointUiState.Success -> {
                showHideProgressBar(false)
            }
        }
    }
}