package com.project2.partner.points.impl.ui.detail

import androidx.lifecycle.SavedStateHandle
import com.project2.core.presentation.di.assisted.AssistedViewModelFactory
import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.partner.points.api.domain.models.PointDetailDomain
import com.project2.partner.points.api.domain.usecase.ActivatePointUseCase
import com.project2.partner.points.api.domain.usecase.GetPartnerDetailPointUseCase
import com.project2.partner.points.api.domain.usecase.HidePointUseCase
import com.project2.partner.points.impl.ui.list.PointDetailUi
import com.project2.partner.points.impl.ui.list.mapper.toPresentation
import com.project2.partner.profile.api.domain.usecase.GetPartnerProfileUseCase
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.onStart

class DetailPointViewModel @AssistedInject constructor(
    private val getDetailPoint: GetPartnerDetailPointUseCase,
    private val getPartnerProfile: GetPartnerProfileUseCase,
    private val hidePoint: HidePointUseCase,
    private val activatePoint: ActivatePointUseCase,
    @Assisted private val savedStateHandle: SavedStateHandle,
    override val exceptionHandler: CoroutineExceptionHandler,
): BaseViewModel() {

    @AssistedFactory
    interface Factory : AssistedViewModelFactory<DetailPointViewModel>

    private val _pointDetail = MutableSharedFlow<PointDetailUi>(replay = 1)
    val pointDetail = _pointDetail.asSharedFlow()

    private val _uiState = MutableStateFlow<UiState>(UiState.Loading)
    val uiState = _uiState.asStateFlow()

    private val _processingState = MutableStateFlow<ProcessingUiState?>(null)
    val processingState = _processingState.asStateFlow().filterNotNull()

    private val pointId: String?
        get() = savedStateHandle[ARGS_POINT_ID]

    fun getPoint() {
        launchOnViewModelScope {
            getDetailPoint(pointId)
                .collectPoint(
                    onStart = { _uiState.value = UiState.Loading },
                    onError = {  _uiState.value = UiState.Error(it) },
                    onSuccess = { _uiState.value = UiState.Success }
                )
        }
    }

    fun handleCommand(action: PointCommand) {
        when(action) {
            is PointCommand.Hide -> hidePoint()
            is PointCommand.Activate -> activatePoint()
        }
    }

    private fun hidePoint() {
        pointId?.let { id ->
            launchOnViewModelScope {
                hidePoint(id)
                    .collectPoint(
                        onStart = { _processingState.value = ProcessingUiState.Loading },
                        onError = {  _processingState.value = ProcessingUiState.Error(it) },
                        onSuccess = {
                            _processingState.value = ProcessingUiState.Success(PointCommand.Hide)
                        }
                    )
            }
        }
    }

    private fun activatePoint() {
        pointId?.let { id ->
            launchOnViewModelScope {
                activatePoint(id)
                    .collectPoint(
                        onStart = { _processingState.value = ProcessingUiState.Loading },
                        onError = {  _processingState.value = ProcessingUiState.Error(it) },
                        onSuccess = {
                            _processingState.value =
                                ProcessingUiState.Success(PointCommand.Activate)
                        }
                    )
            }
        }
    }

    private suspend fun Flow<PointDetailDomain>.collectPoint(
        onStart: () -> Unit,
        onError: (Throwable) -> Unit,
        onSuccess: (PointDetailUi) -> Unit
    ) {
        onStart { onStart.invoke() }
            .combine(getPartnerProfile()) { pointDomain, partnerProfile ->
                pointDomain.toPresentation(partnerProfile.role)
            }
            .catch { onError.invoke(it) }
            .collect { pointDetail ->
                onSuccess.invoke(pointDetail)
                _pointDetail.emit(pointDetail)
            }
    }

    companion object {
        const val ARGS_POINT_ID = "point_id"
    }
}

sealed class PointCommand {

    override fun toString(): String = javaClass.simpleName.uppercase()

    object Hide: PointCommand()
    object Activate: PointCommand()

    companion object {

        fun create(action: String) = when(action) {
            Hide.toString() -> Hide
            Activate.toString() -> Activate
            else -> error("Unknown command – $action")
        }
    }
}

sealed interface UiState {
    object Loading: UiState
    object Success: UiState
    data class Error(val error: Throwable): UiState
}

sealed interface ProcessingUiState {
    object Loading: ProcessingUiState
    data class Success(val command: PointCommand): ProcessingUiState
    data class Error(val error: Throwable): ProcessingUiState
}