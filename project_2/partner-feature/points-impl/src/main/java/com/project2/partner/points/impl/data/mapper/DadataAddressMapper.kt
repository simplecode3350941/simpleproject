package com.project2.partner.points.impl.data.mapper

import com.project2.partner.points.api.data.remote.models.DadataAddressResponse
import com.project2.partner.points.api.domain.models.DadataAddressDomain

fun DadataAddressResponse.toDomain() = DadataAddressDomain(
    id = id,
    latitude = latitude,
    longitude = longitude,
    addressFull = addressFull,
    addressShort = addressShort,
    cityRegionCountry = cityRegionCountry
)