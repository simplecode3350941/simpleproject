package com.project2.partner.points.impl.ui.create.address_selection

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.core.utils.view.setOnClickDelayListener
import com.project2.partner.points.impl.databinding.PartnerPointAddressItemViewBinding
import com.project2.partner.points.impl.ui.create.models.DadataAddressUi
import com.project2.partner.points.impl.ui.create.models.address


class PartnerPointAddressAdapter(private val onItemClickListener: (DadataAddressUi) -> Unit) : ListAdapter<DadataAddressUi, PartnerPointAddressAdapter.ViewHolder>(
    DIFF_CALLBACK
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(parent: ViewGroup) : BindingViewHolder<PartnerPointAddressItemViewBinding>(
        parent,
        PartnerPointAddressItemViewBinding::inflate
    ) {

        fun bind(item: DadataAddressUi) = with(binding) {
            textViewAddress.text = item.address
            textViewRegion.text = item.region

            itemView.setOnClickDelayListener {
                onItemClickListener.invoke(item)
            }
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<DadataAddressUi>() {
            override fun areItemsTheSame(oldItem: DadataAddressUi, newItem: DadataAddressUi): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: DadataAddressUi, newItem: DadataAddressUi): Boolean {
                return oldItem == newItem
            }
        }
    }
}