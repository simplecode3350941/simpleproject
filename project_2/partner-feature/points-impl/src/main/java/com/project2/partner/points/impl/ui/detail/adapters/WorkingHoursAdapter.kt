package com.project2.partner.points.impl.ui.detail.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.partner.points.impl.R
import com.project2.partner.points.impl.databinding.PartnerPointSheduleItemViewBinding
import com.project2.partner.points.impl.ui.list.ScheduleUi
import com.project2.partner.points.impl.ui.list.is24hours
import com.project2.partner.points.impl.ui.list.timeRange


class WorkingHoursAdapter : ListAdapter<ScheduleUi, WorkingHoursAdapter.ViewHolder>(
    DIFF_CALLBACK
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(parent: ViewGroup) : BindingViewHolder<PartnerPointSheduleItemViewBinding>(parent, PartnerPointSheduleItemViewBinding::inflate) {

        fun bind(item: ScheduleUi) = with(binding) {
            textViewKey.text = item.workingDay
            textViewValue.text = when {
                item.dayOff -> getString(R.string.point_day_off)
                item.is24hours -> getString(R.string.point_is_24_hours)
                else -> item.timeRange
            }
        }
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ScheduleUi>() {
            override fun areItemsTheSame(oldItem: ScheduleUi, newItem: ScheduleUi): Boolean {
                return oldItem.workingDay == newItem.workingDay
            }

            override fun areContentsTheSame(oldItem: ScheduleUi, newItem: ScheduleUi): Boolean {
                return oldItem == newItem
            }
        }
    }
}