package com.project2.partner.points.impl.ui.create.work_schedule_selection.adapter

import androidx.annotation.Keep
import androidx.annotation.StringRes
import com.project2.core.presentation.adapter_delegate.DelegateAdapterItem
import com.project2.partner.points.impl.R

@Keep
enum class DayWeek(@StringRes val resId: Int) {
    MONDAY(R.string.monday),
    TUESDAY(R.string.tuesday),
    WEDNESDAY(R.string.wednesday),
    THURSDAY(R.string.thursday),
    FRIDAY(R.string.friday),
    SATURDAY(R.string.saturday),
    SUNDAY(R.string.sunday)
}

data class WorkScheduleAdapterItem(val day: Schedule): DelegateAdapterItem {

    override fun id() = day.dayOfWeek

    override fun content() = day

    data class Schedule(
        val dayOfWeek: DayWeek,
        val startTime: String? = null,
        val endTime: String? = null,
        val isDayOff: Boolean = false,
        val friendlyDayWeek: String
    )
}