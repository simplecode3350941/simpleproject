package com.project2.partner.points.impl.ui

import androidx.lifecycle.ViewModel
import com.project2.core.commons.di.viewmodule.ViewModelKey
import com.project2.partner.points.impl.ui.create.CreatePointViewModel
import com.project2.partner.points.impl.ui.create.address_selection.PartnerPointAddressViewModel
import com.project2.partner.points.impl.ui.create.communication.CreatePointCommunicationViewModel
import com.project2.partner.points.impl.ui.create.point_type_selection.PartnerPointTypesViewModel
import com.project2.partner.points.impl.ui.create.pricelist_selection.PriceListSelectionViewModel
import com.project2.partner.points.impl.ui.create.work_schedule_selection.WorkScheduleSelectionViewModel
import com.project2.partner.points.impl.ui.list.PartnerListServicesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PointsViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(PartnerListServicesViewModel::class)
    abstract fun partnerListServicesViewModel(viewModel: PartnerListServicesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PartnerPointTypesViewModel::class)
    abstract fun partnerPointTypesViewModel(viewModel: PartnerPointTypesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PartnerPointAddressViewModel::class)
    abstract fun partnerPointAddressViewModel(viewModel: PartnerPointAddressViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PriceListSelectionViewModel::class)
    abstract fun partnerPriceListSelectionViewModel(viewModel: PriceListSelectionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WorkScheduleSelectionViewModel::class)
    abstract fun workScheduleSelectionViewModel(viewModel: WorkScheduleSelectionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CreatePointCommunicationViewModel::class)
    abstract fun createPointCommunicationViewModel(viewModel: CreatePointCommunicationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CreatePointViewModel::class)
    abstract fun createPointViewModel(viewModel: CreatePointViewModel): ViewModel
}