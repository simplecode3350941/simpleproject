package com.project2.partner.points.impl.data.repository

import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.partner.points.api.data.remote.api.PartnerDadataApi
import com.project2.partner.points.api.domain.models.DadataAddressDomain
import com.project2.partner.points.api.domain.repository.PartnerDadataRepository
import com.project2.partner.points.impl.data.mapper.toDomain
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PartnerDadataRepositoryImpl @Inject constructor(private val dadataApi: PartnerDadataApi,
                                                      @IoDispatcher
                                                      private val dispatcher: CoroutineDispatcher):
    PartnerDadataRepository {

    override suspend fun getAddress(query: String): Flow<List<DadataAddressDomain>> {
        return flow {
            val listAddress = dadataApi.getAddress(query)
            emit(listAddress)
        }
            .map { listAddress -> listAddress.map { it.toDomain() } }
            .flowOn(dispatcher)
    }
}