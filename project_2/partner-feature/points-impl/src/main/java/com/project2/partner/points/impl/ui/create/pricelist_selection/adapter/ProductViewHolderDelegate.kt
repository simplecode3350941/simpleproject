package com.project2.partner.points.impl.ui.create.pricelist_selection.adapter

import android.view.ViewGroup
import androidx.core.view.isVisible
import com.project2.core.presentation.adapter_delegate.ItemViewDelegate
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.partner.points.impl.R
import com.project2.partner.points.impl.databinding.PartnerPriceListProductItemViewBinding
import com.project2.partner.points.impl.ui.list.friendlyPrice
import com.project2.partner.points.impl.ui.price_list.adapter.ProductAdapterItem

class ProductViewHolderDelegate(
    private val onAddPriceClick: (ProductAdapterItem) -> Unit,
    private val onRemovePriceClick: (ProductAdapterItem) -> Unit,
) : ItemViewDelegate<ProductAdapterItem, ProductViewHolderDelegate.ViewHolder>(ProductAdapterItem::class.java) {

    override fun onCreateViewHolder(parent: ViewGroup) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, item: ProductAdapterItem) {
        holder.bind(item)
    }

    inner class ViewHolder(parent: ViewGroup) : BindingViewHolder<PartnerPriceListProductItemViewBinding>(
        parent,
        PartnerPriceListProductItemViewBinding::inflate
    ) {

        fun bind(item: ProductAdapterItem) = with(binding) {
            val product = item.product

            val price = product.friendlyPrice
            textViewProductName.text = product.name
            textViewPrice.text = price
            textViewPrice.isVisible = price != null
            textViewPrice.setOnClickListener {
                onAddPriceClick.invoke(item)
            }

            val iconDrawable = when(product.price) {
                null -> getTintedDrawable(R.drawable.ic_plus, R.color.grey_dark)
                else -> getTintedDrawable(R.drawable.ic_close_16dp, R.color.red)
            }

            buttonAddPrice.setImageDrawable(iconDrawable)

            divider.isVisible = item.isLastInCategory
            buttonAddPrice.setOnClickListener {
                when(product.price) {
                    null -> onAddPriceClick.invoke(item)
                    else -> onRemovePriceClick.invoke(item)
                }
            }
        }
    }

}