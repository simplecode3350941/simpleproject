package com.project2.partner.points.impl.ui.detail

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.ComponentDependenciesProvider
import com.project2.core.presentation.di.HasComponentDependencies
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.dialog.InfoBottomSheet
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.utils.common.lazyUnsafe
import com.project2.core.utils.view.getCompatColor
import com.project2.partner.points.api.ui.list.models.PointStatus
import com.project2.partner.points.impl.R
import com.project2.partner.points.impl.databinding.FragmentDetailPointBinding
import com.project2.partner.points.impl.ui.detail.adapters.PointTypesAdapter
import com.project2.partner.points.impl.ui.detail.adapters.WorkingHoursAdapter
import com.project2.partner.points.impl.ui.detail.bottom_sheet.PointMenuBottomSheet
import com.project2.partner.points.impl.ui.detail.bottom_sheet.SuccessChangeStatusBottomSheet
import com.project2.partner.points.impl.ui.detail.di.DaggerDetailPointComponent
import com.project2.partner.points.impl.ui.detail.di.DetailPointDependencies
import com.project2.partner.points.impl.ui.getStatusDrawable
import com.project2.partner.points.impl.ui.list.PointDetailUi
import com.project2.partner.points.impl.ui.list.PointTypeUi
import com.project2.partner.points.impl.ui.list.friendlyName
import com.project2.partner.points.impl.ui.list.pointTypes
import com.project2.partner.profile.api.data.remote.models.PartnerRole
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class DetailPointFragment: BaseMvvmFragment(), HasComponentDependencies {

    @Inject
    override lateinit var dependencies: ComponentDependenciesProvider

    private val partnerRole: PartnerRole
        get() {
            val role = requireArguments().getString(PARTNER_ROLE)
            checkNotNull(role)
            return PartnerRole.valueOf(role)
        }

    private val viewModel: DetailPointViewModel by assistedViewModel {
        findComponentDependencies<DetailPointDependencies>()
            .getDetailPointViewModelFactory()
    }

    private val workingHoursAdapter by lazyUnsafe {
        WorkingHoursAdapter()
    }

    private val pointTypesAdapter by lazyUnsafe {
        PointTypesAdapter(::onCategoryItemClick)
    }

    override fun inject() {
        DaggerDetailPointComponent.builder()
            .detailPointDependencies(findComponentDependencies())
            .build()
            .inject(this)
    }

    private var pointId: String? = null

    override fun getLayoutResId(): Int = R.layout.fragment_detail_point

    override val synthetic = SyntheticBinding(FragmentDetailPointBinding::inflate)

    override fun initUi(savedInstanceState: Bundle?) {

        synthetic.binding.toolbar.apply {
            isVisible = partnerRole == PartnerRole.OWNER
            menu.findItem(R.id.action_menu)?.isVisible = false
            setNavigationOnClickListener {
                popBack()
            }
        }

        childFragmentManager.setFragmentResultListener(
            PointMenuBottomSheet.REQUEST_KEY, viewLifecycleOwner
        ) { _, bundle ->
            val argsItem = bundle.getString(PointMenuBottomSheet.ARGS_ITEM)
            argsItem?.let { item ->
                when(item) {
                    PointMenuBottomSheet.ITEM_EDIT -> {
                        findNavController().navigate(R.id.navigation_edit_point,
                            bundleOf(
                                PARTNER_POINT_ID to pointId
                            )
                        )
                    }
                    else -> viewModel.handleCommand(PointCommand.create(item))
                }
            }
        }

        synthetic.binding.recyclerViewWorkingHours.apply {
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = workingHoursAdapter
        }

        synthetic.binding.recyclerViewCategories.apply {
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = pointTypesAdapter
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.uiState.collectLatest(::updateUiState)
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.processingState.collectLatest(::updateProcessingState)
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.pointDetail.collectLatest(::showPoint)
        }

        synthetic.binding.imageViewPriceListInfo.setOnClickListener {
            showPriceListInfo()
        }
    }

    private fun showPriceListInfo() {
        InfoBottomSheet.Builder(getString(R.string.point_price_list_info))
            .setTitle(getString(R.string.note_attention))
            .setButtonStyle(InfoBottomSheet.PRIMARY_BUTTON)
            .show(childFragmentManager)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getPoint()
    }

    private fun showPoint(point: PointDetailUi) {
        pointId = point.id

        with(synthetic.binding) {
            val content = synthetic.binding.scrollView
            content.isVisible = true

            updateToolbarMenu(point.status)

            textViewPointName.text = point.friendlyName
            textViewPointAddress.text = point.address
            textViewPointTypes.text = point.pointTypes
            textViewPointStatus.setText(point.status.stringResId)
            textViewPointStatus.setTextColor(
                requireContext().getCompatColor(
                    point.status.colorResId
                )
            )
            textViewPointStatus.background =
                requireContext().getStatusDrawable(point.status.colorResId)
            workingHoursAdapter.submitList(point.schedule)
            pointTypesAdapter.submitList(point.types)
        }
    }

    private fun updateToolbarMenu(pointStatus: PointStatus) {
        with(synthetic.binding.toolbar) {
            menu.findItem(R.id.action_menu)?.apply {
                isVisible = partnerRole == PartnerRole.OWNER && pointStatus != PointStatus.MODERATION
            }

            val action = when(pointStatus) {
                PointStatus.ACTIVE -> PointCommand.Hide.toString()
                PointStatus.HIDDEN -> PointCommand.Activate.toString()
                else -> null
            }

            setOnMenuItemClickListener {
                when(it.itemId) {
                    R.id.action_menu -> {
                        PointMenuBottomSheet.show(childFragmentManager, action)
                        true
                    }
                    else -> false
                }
            }
        }
    }

    private fun updateProcessingState(state: ProcessingUiState) {
        Timber.d("state $state")
        when(state) {
            is ProcessingUiState.Loading -> startLoading()
            is ProcessingUiState.Success -> {
                SuccessChangeStatusBottomSheet.show(childFragmentManager, state.command.toString())
                stopLoading()
            }
            is ProcessingUiState.Error -> {
                showCommonErrorSnack(state.error)
                stopLoading()
            }
        }
    }

    private fun updateUiState(state: UiState) {
        Timber.d("state $state")
        when(state) {
            is UiState.Loading -> showLoading()
            is UiState.Error -> showError()
            is UiState.Success -> hideLoading()
        }
    }

    private fun showError() {
        hideLoading()
        val content = synthetic.binding.scrollView
        content.isVisible = false

        with(synthetic.binding.includeErrorView) {
            errorView.isVisible = true
            buttonRefresh.setOnClickListener {
                errorView.isVisible = false
                viewModel.getPoint()
            }
        }
    }

    private fun showLoading() {
        val shimmer = synthetic.binding.includePlaceholder.layoutShimmer
        val content = synthetic.binding.scrollView
        shimmer.showShimmer(true)
        shimmer.isVisible = true
        content.isVisible = false
    }

    private fun hideLoading() {
        val shimmer = synthetic.binding.includePlaceholder.layoutShimmer
        shimmer.hideShimmer()
        shimmer.isVisible = false
    }

    private fun onCategoryItemClick(item: PointTypeUi) {
        pointId?.let {
            findNavController().navigate(R.id.action_detailPoint_to_priceList,
                bundleOf(
                    "pointId" to it,
                    "localizedName" to item.localizedName,
                    "type" to item.type.toString(),
                )
            )
        }
    }

    companion object {
        const val PARTNER_POINT_ID = "partner_point_id"
        const val PARTNER_ROLE = "partner_role"
    }
}