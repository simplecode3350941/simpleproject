package com.project2.partner.points.impl.ui.create

import com.project2.partner.points.api.domain.usecase.point.CreatePointUseCase
import com.project2.partner.points.impl.ui.create.communication.CreatePointUi
import com.project2.partner.points.impl.ui.create.mapper.toDomain
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

class CreatePointViewModel @Inject constructor(
    private val createPoint: CreatePointUseCase,
    override val exceptionHandler: CoroutineExceptionHandler
): AbsCreatePointViewModel() {

    override fun onSavePoint(point: CreatePointUi) {
        launchOnViewModelScope {
            createPoint(point.toDomain())
                .onStart { updateState(UiState.Loading) }
                .catch {
                    updateState(UiState.Error(it))
                }
                .collectLatest {
                    updateState(UiState.Success)
                }
        }
    }
}