package com.project2.partner.points.impl.ui.list.adapter

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.core.utils.view.setOnClickDelayListener
import com.project2.partner.points.api.ui.list.models.PointShortUi
import com.project2.partner.points.impl.databinding.PartnerPointItemViewBinding
import com.project2.partner.points.impl.ui.getStatusDrawable

class PartnerPointsAdapter(
    private val onClickListener: (String) -> Unit
): PagingDataAdapter<PointShortUi, PartnerPointsAdapter.ViewHolder>(
    DIFF_CALLBACK
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, onClickListener)
        }
    }

    inner class ViewHolder(parent: ViewGroup) : BindingViewHolder<PartnerPointItemViewBinding>(parent, PartnerPointItemViewBinding::inflate) {

        fun bind(item: PointShortUi, onClickListener: (String) -> Unit) = with(binding) {
            textViewPointName.text = item.name
            textViewPointTypes.text = item.formattedPointTypes
            textViewPointAddress.text = item.address

            textViewPointStatus.text = getString(item.status.stringResId)
            textViewPointStatus.setTextColor(getColor(item.status.colorResId))

            textViewPointStatus.background = context.getStatusDrawable(item.status.colorResId)

            textViewPointAddress.text = item.address

            itemView.setOnClickDelayListener {
                onClickListener(item.id)
            }
        }
    }

    companion object {

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<PointShortUi>() {
            override fun areItemsTheSame(oldItem: PointShortUi, newItem: PointShortUi): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: PointShortUi, newItem: PointShortUi): Boolean {
                return oldItem == newItem
            }
        }
    }
}