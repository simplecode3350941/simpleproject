package com.project2.partner.points.impl.domain.create

import com.project2.partner.points.api.domain.models.CreatePointDomain
import com.project2.partner.points.api.domain.repository.PartnerPointsRepository
import com.project2.partner.points.api.domain.usecase.point.CreatePointUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CreatePointUseCaseImpl @Inject constructor(private val repository: PartnerPointsRepository) :
    CreatePointUseCase {

    override suspend fun invoke(point: CreatePointDomain): Flow<Boolean> {
        return repository.createPoint(point)
    }
}