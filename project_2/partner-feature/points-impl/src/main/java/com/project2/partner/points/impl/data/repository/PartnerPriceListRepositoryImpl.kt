package com.project2.partner.points.impl.data.repository

import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.partner.points.api.data.remote.api.PartnerPriceListApi
import com.project2.partner.points.api.domain.models.PointType
import com.project2.partner.points.api.domain.models.PointTypeDomain
import com.project2.partner.points.api.domain.models.PointTypeWithCategoryDomain
import com.project2.partner.points.api.domain.repository.PartnerPriceListRepository
import com.project2.partner.points.impl.data.mapper.toData
import com.project2.partner.points.impl.data.mapper.toDomain
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PartnerPriceListRepositoryImpl @Inject constructor(private val priceListApi: PartnerPriceListApi,
                                                         @IoDispatcher
                                                         private val dispatcher: CoroutineDispatcher):
    PartnerPriceListRepository {

    override suspend fun getPointsType(): Flow<List<PointTypeDomain>> {
        return flow {
            val pointsType = priceListApi.getPointsType()
            emit(pointsType.filter { it.type != PointType.UNKNOWN })
        }
            .map { it.map { it.toDomain(null) } }
            .flowOn(dispatcher)
    }

    override suspend fun getPriceListByTypes(types: List<PointTypeDomain>): Flow<List<PointTypeWithCategoryDomain>> {
        return flow {
                val typesRequest = types.map { it.toData() }
                val priceList = priceListApi.getPriceListByTypes(typesRequest)
                emit(priceList)
            }
            .map {
                it.map { it.toDomain() }
            }
            .flowOn(dispatcher)
    }

    override suspend fun getPriceListByPointId(pointId: String): Flow<List<PointTypeWithCategoryDomain>> {
        return flow {
            val priceList = priceListApi.getPriceListForPayment(pointId)
            emit(priceList)
        }
            .map {
                it.map { it.toDomain() }
            }
            .flowOn(dispatcher)
    }
}