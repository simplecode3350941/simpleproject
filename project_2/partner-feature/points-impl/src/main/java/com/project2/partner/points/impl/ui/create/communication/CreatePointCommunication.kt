package com.project2.partner.points.impl.ui.create.communication

import com.project2.partner.points.impl.ui.create.work_schedule_selection.adapter.WorkScheduleAdapterItem
import com.project2.partner.points.impl.ui.list.CategoryUi
import com.project2.partner.points.impl.ui.list.PointTypeUi
import com.project2.partner.points.impl.ui.list.ProductUi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.flow.updateAndGet

object CreatePointCommunication {

    private val flow = MutableStateFlow(CreatePointUi())
    private val createPointFlow = flow.asStateFlow()

    fun clear() {
        flow.value = CreatePointUi()
    }

    suspend fun subscribe(onStateChanged: (CreatePointUi) -> Unit) {
        createPointFlow.collectLatest {
            onStateChanged.invoke(it)
        }
    }

    fun getPointUi() = flow.value

    fun getPointTypes(): HashSet<PointTypeUi> {
        return flow.value.types
    }

    suspend fun setCreatePointUi(createPointUi: CreatePointUi) {
        modifyAndEmit {
            createPointUi.copy()
        }
    }

    fun setNameAndPhone(pointName: String, contactPhone: String) {
        modify {
            it.copy(
                pointName = pointName,
                phoneNumber = contactPhone
            )
        }
    }

    suspend fun setPointName(pointName: String) {
        modifyAndEmit {
            it.copy(pointName = pointName)
        }
    }

    suspend fun setPointTypes(pointTypes: HashSet<PointTypeUi>) {
        modifyAndEmit {
            it.copy(types = pointTypes)
        }
    }

    suspend fun setPhoneNumber(phoneNumber: String) {
        modifyAndEmit {
            it.copy(phoneNumber = phoneNumber)
        }
    }

    suspend fun setWorkSchedules(workSchedules: List<WorkScheduleAdapterItem.Schedule>) {
        modifyAndEmit {
            it.copy(workSchedules = workSchedules)
        }
    }

    suspend fun setPriceList(
        types: HashSet<PointTypeUi>,
        categorizedProducts: Map<CategoryUi, List<ProductUi>>
    ) {
        modifyAndEmit {
            it.copy(
                categorizedProducts = categorizedProducts,
                typesWithCategories = types)
        }
    }

    suspend fun setAddress(latitude: Double, longitude: Double, address: String) {
        modifyAndEmit {
            it.copy(
                latitude = latitude,
                longitude = longitude,
                address = address
            )
        }
    }

    private suspend fun modifyAndEmit(onModifyPoint: (CreatePointUi) -> CreatePointUi) {
        val createPoint = flow.updateAndGet { onModifyPoint(it) }
        flow.emit(createPoint)
    }

    private fun modify(onModifyPoint: (CreatePointUi) -> CreatePointUi) {
        flow.update { onModifyPoint.invoke(it) }
    }
}