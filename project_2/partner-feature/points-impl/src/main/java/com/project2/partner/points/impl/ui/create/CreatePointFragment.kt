package com.project2.partner.points.impl.ui.create

import android.os.Bundle
import android.text.SpannableString
import android.widget.TextView
import androidx.annotation.CallSuper
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.ComponentDependenciesProvider
import com.project2.core.presentation.di.HasComponentDependencies
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.dialog.InfoBottomSheet
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.utils.common.lazyUnsafe
import com.project2.core.utils.formatter.FormatUtils.toFormattedPhoneNumber
import com.project2.core.utils.view.getCompatColor
import com.project2.core.utils.view.tintLastSymbol
import com.project2.partner.points.impl.R
import com.project2.partner.points.impl.databinding.FragmentCreatePointBinding
import com.project2.partner.points.impl.ui.create.communication.CreatePointCommunication
import com.project2.partner.points.impl.ui.create.communication.CreatePointUi
import com.project2.partner.points.impl.ui.create.communication.friendlyPointTypes
import com.project2.partner.points.impl.ui.create.communication.getFriendlyWorkSchedule
import com.project2.partner.points.impl.ui.create.communication.workScheduleIsValid
import com.project2.partner.points.impl.ui.detail.di.DaggerDetailPointComponent
import com.project2.partner.points.impl.ui.list.PointTypeUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

open class CreatePointFragment : BaseMvvmFragment(), CreatePointView, HasComponentDependencies {

    @Inject
    override lateinit var dependencies: ComponentDependenciesProvider

    private val viewModel: CreatePointViewModel by viewModels {
        viewModelFactory
    }

    override fun getPointViewModel(): AbsCreatePointViewModel = viewModel

    private val phoneMask = PhoneMaskWatcher()

    private val redColor by lazyUnsafe {
        requireContext().getCompatColor(R.color.red)
    }

    private val blackColor by lazyUnsafe {
        requireContext().getCompatColor(R.color.black)
    }

    private val greyColor by lazyUnsafe {
        requireContext().getCompatColor(R.color.grey_middle)
    }

    override fun inject() {
        DaggerDetailPointComponent.builder()
            .detailPointDependencies(findComponentDependencies())
            .build()
            .inject(this)
    }

    override fun getLayoutResId(): Int = R.layout.fragment_create_point

    override val synthetic = SyntheticBinding(FragmentCreatePointBinding::inflate)

    @CallSuper
    override fun initUi(savedInstanceState: Bundle?) {
        onInitViews()
        synthetic.binding.apply {
            buttonCreatePoint.setOnClickListener {
                createPoint()
            }

            layoutPointPriceList.setOnClickListener {
                navigateToPriceList()
            }

            layoutPointType.setOnClickListener {
                onClickTypeSelection()
            }

            layoutPointAddress.setOnClickListener {
                onClickAddressSelection()
            }

            layoutPointWorkingHours.setOnClickListener {
                onClickWorkScheduleSelection()
            }

            viewLifecycleOwner.lifecycleScope.launch {
                getPointViewModel().uiState.collectLatest(::updateUiState)
            }

            viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
                CreatePointCommunication.subscribe(::onUpdatePointData)
            }
        }
    }

    private fun navigateToPriceList() {
        val pointUi = CreatePointCommunication.getPointUi()
        if(pointUi.types.isEmpty()) {
            InfoBottomSheet.Builder(getString(R.string.create_point_price_list_warning))
                .setTitle(getString(R.string.attention))
                .setButtonText(getString(R.string.dialog_ok))
                .show(childFragmentManager)
        } else {
            onClickPriceListSelection()
        }
    }

    @CallSuper
    override fun onInitViews() {
        synthetic.binding.apply {
            toolbar.setNavigationOnClickListener {
                popBack()
            }

            synthetic.binding.buttonCreatePoint.isVisible = true

            editTextContactPhone.removeTextChangedListener(phoneMask)
            editTextContactPhone.addTextChangedListener(phoneMask)

            childFragmentManager.setFragmentResultListener(
                PointCreatedSuccessfulBottomSheet.REQUEST_KEY, viewLifecycleOwner
            ) { _, _ ->
                popBack()
            }
        }
    }

    override fun onClickTypeSelection() {
        navigateTo(R.id.action_createPoint_to_pointTypeSelection)
    }

    override fun onClickAddressSelection() {
        navigateTo(R.id.action_createPoint_to_pointAddressSelection)
    }

    override fun onClickPriceListSelection() {
        navigateTo(R.id.action_createPoint_to_priceListSelection)
    }

    override fun onClickWorkScheduleSelection() {
        navigateTo(R.id.action_createPoint_to_workScheduleSelection)
    }

    override fun onSavePoint(point: CreatePointUi) {
        getPointViewModel().onSavePoint(point)
    }

    override fun onSuccess() {
        PointCreatedSuccessfulBottomSheet.show(childFragmentManager)
    }

    private fun isRequiredFieldsFilled(): Boolean {
        val pointName = synthetic.binding.editTextPointName.text.toString()
        val contactPhone = synthetic.binding.editTextContactPhone.text.toString()

        val pointUi = CreatePointCommunication.getPointUi()

        val errorMessage = getString(R.string.create_point_required_field_error)

        val fieldsIsValid = mutableListOf<Boolean>()

        fieldsIsValid.add(pointName.isNotEmpty())
        if (pointName.isEmpty()) {
            synthetic.binding.inputLayoutPointName.error = errorMessage
        }

        val phoneIsValid =
            contactPhone.isNotEmpty() && contactPhone.length == PARTNER_CREATE_POINT_PHONE_NUMBER_LENGTH
        fieldsIsValid.add(phoneIsValid)
        if (!phoneIsValid) {
            synthetic.binding.inputLayoutContactPhone.error = errorMessage
        }

        fieldsIsValid.add(pointUi.types.isNotEmpty())
        synthetic.binding.textViewPointTypeError.isVisible = pointUi.types.isEmpty()

        val workScheduleIsValid = pointUi.workScheduleIsValid()
        fieldsIsValid.add(workScheduleIsValid)
        synthetic.binding.textViewPointWorkSchedulesError.isVisible = !workScheduleIsValid

        val addressIsValid = pointUi.address != null
        fieldsIsValid.add(addressIsValid)
        synthetic.binding.textViewPointAddressError.isVisible = !addressIsValid

        var isAllTypesHasPrice = true
        val typesWithCategoriesWithNullId = HashSet<PointTypeUi>()
        pointUi.typesWithCategories.forEach {
            typesWithCategoriesWithNullId.add(it.copy(id = null))
        }
        pointUi.types.forEach {
            if (!typesWithCategoriesWithNullId.contains(it)) {
                isAllTypesHasPrice = false
            }
        }
        val priceListIsValid = pointUi.categorizedProducts.filterValues {
            it.isNotEmpty()
        }.isNotEmpty() && isAllTypesHasPrice
        fieldsIsValid.add(priceListIsValid)
        synthetic.binding.textViewPointPriceListError.isVisible = !priceListIsValid

        val isValid = !fieldsIsValid.contains(false)

        if (isValid) {
            CreatePointCommunication.setNameAndPhone(
                pointName,
                contactPhone.toFormattedPhoneNumber()
            )
        }

        return isValid
    }

    private fun createPoint() {
        if (isRequiredFieldsFilled()) {
            val point = CreatePointCommunication.getPointUi()
            onSavePoint(point)
        }
    }

    private fun updateUiState(state: UiState) {
        Timber.d("state $state")
        when(state) {
            is UiState.Loading -> startLoading()
            is UiState.Error -> {
                stopLoading()
                showCommonErrorSnack(state.error)
                getPointViewModel().resetUiState()
            }
            is UiState.Success -> {
                stopLoading()
                onSuccess()
            }
        }
    }

    protected fun showHideProgressBar(show: Boolean) {
        val progressBar = synthetic.binding.progressBarPointEdit
        progressBar.isVisible = show
    }

    private fun onUpdatePointData(pointUi: CreatePointUi) {
        synthetic.binding.apply {
            inputLayoutPointName.hint = getHintColored(R.string.create_point_hint_point_name)
            inputLayoutContactPhone.hint = getHintColored(R.string.create_point_hint_contact_phone)

            val addressHint = getHintColored(R.string.create_point_hint_point_address)
            val address = pointUi.address ?: ""
            textViewAddress.setActiveColorIf { address.isNotEmpty() }
            textViewAddress.text = address.ifEmpty { addressHint }
            textViewPointAddressHint.text = addressHint
            textViewPointAddressHint.isInvisible = address.isEmpty()

            val pointTypeHint = getHintColored(R.string.create_point_hint_point_type)
            val pointTypes = pointUi.friendlyPointTypes
            textViewPointType.setActiveColorIf { pointTypes.isNotEmpty() }
            textViewPointType.text = pointTypes.ifEmpty { pointTypeHint }
            textViewPointTypeTitle.text = pointTypeHint
            textViewPointTypeTitle.isInvisible = pointTypes.isEmpty()

            val workScheduleHint = getHintColored(R.string.create_point_hint_point_working_hours)
            val workSchedule = pointUi.getFriendlyWorkSchedule(requireContext())

            textViewWorkingHours.setActiveColorIf { workSchedule.isNotEmpty() }
            textViewWorkingHours.text = workSchedule.ifEmpty { workScheduleHint }
            textViewPointWorkingHoursHint.text = workScheduleHint
            textViewPointWorkingHoursHint.isInvisible = workSchedule.isEmpty()

            inputLayoutPointName.error = null
            inputLayoutContactPhone.error = null
            textViewPointTypeError.isVisible = false
            textViewPointWorkSchedulesError.isVisible = false
            textViewPointAddressError.isVisible = false
            textViewPointPriceListError.isVisible = false
        }
    }

    private fun getHintColored(@StringRes resId: Int): SpannableString {
        return getString(resId).tintLastSymbol(redColor)
    }

    private fun TextView.setActiveColorIf(predicate: () -> Boolean) {
        setTextColor(
            when {
                predicate() -> blackColor
                else -> greyColor
            }
        )
    }

    protected fun navigateTo(@IdRes resId: Int) {
        findNavController().navigate(resId)
    }

    companion object {
        const val PARTNER_CREATE_POINT_PHONE_NUMBER_LENGTH = 18
    }
}