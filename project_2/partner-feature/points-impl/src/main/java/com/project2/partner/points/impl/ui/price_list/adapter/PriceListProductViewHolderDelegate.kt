package com.project2.partner.points.impl.ui.price_list.adapter

import android.view.ViewGroup
import com.project2.core.presentation.adapter_delegate.ItemViewDelegate
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.partner.points.impl.databinding.PartnerDetailPointPriceListProductItemViewBinding
import com.project2.partner.points.impl.ui.list.friendlyPrice

class PriceListProductViewHolderDelegate: ItemViewDelegate<ProductAdapterItem, PriceListProductViewHolderDelegate.ViewHolder>(
    ProductAdapterItem::class.java) {

    override fun onCreateViewHolder(parent: ViewGroup) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, item: ProductAdapterItem) {
        holder.bind(item)
    }

    inner class ViewHolder(parent: ViewGroup) : BindingViewHolder<PartnerDetailPointPriceListProductItemViewBinding>(
        parent,
        PartnerDetailPointPriceListProductItemViewBinding::inflate
    ) {

        fun bind(item: ProductAdapterItem) = with(binding) {
            val product = item.product

            val price = product.friendlyPrice
            textViewProductName.text = product.name
            textViewPrice.text = price
        }
    }
}