package com.project2.partner.points.impl.ui.detail.di

import com.project2.core.presentation.di.ComponentDependenciesProviderModule
import com.project2.partner.points.impl.ui.create.CreatePointFragment
import com.project2.partner.points.impl.ui.detail.DetailPointFragment
import dagger.Component

@Component(
    modules = [ComponentDependenciesProviderModule::class],
    dependencies = [DetailPointDependencies::class]
)
interface DetailPointComponent {
    fun inject(detailPointFragment: DetailPointFragment)

    fun inject(createPointFragment: CreatePointFragment)
}
