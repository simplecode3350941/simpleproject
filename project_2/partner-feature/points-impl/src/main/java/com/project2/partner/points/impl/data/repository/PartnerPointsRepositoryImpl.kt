package com.project2.partner.points.impl.data.repository

import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.partner.points.api.data.remote.api.PartnerPointsApi
import com.project2.partner.points.api.domain.models.CreatePointDomain
import com.project2.partner.points.api.domain.models.PointDetailDomain
import com.project2.partner.points.api.domain.models.PointShortDomain
import com.project2.partner.points.api.domain.repository.PartnerPointsRepository
import com.project2.partner.points.impl.data.mapper.toData
import com.project2.partner.points.impl.data.mapper.toDomain
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PartnerPointsRepositoryImpl @Inject constructor(
    private val pointsApi: PartnerPointsApi,
    @IoDispatcher
    private val dispatcher: CoroutineDispatcher
) : PartnerPointsRepository {

    override suspend fun getPartnerListPoints(
        pageNumber: Int,
        pageSize: Int,
        status: String?,
        global: String?
    ): Flow<List<PointShortDomain>> {
        return flow {
            val points = pointsApi.getAllPoints(pageNumber, pageSize, status, global).content
            emit(points)
        }
            .map { points ->
                points.map { it.toDomain() }
            }
            .flowOn(dispatcher)
    }

    override suspend fun getPartnerDetailPoint(id: String?): Flow<PointDetailDomain> {
        return flow {
            val point = pointsApi.getPoint(id)
            emit(point)
        }
        .map { point -> point.toDomain() }
        .flowOn(dispatcher)
    }

    override suspend fun hidePoint(pointId: String): Flow<PointDetailDomain> {
        return flow {
            val point = pointsApi.hidePoint(pointId)
            emit(point)
        }
            .map { point -> point.toDomain() }
            .flowOn(dispatcher)
    }

    override suspend fun activatePoint(pointId: String): Flow<PointDetailDomain> {
        return flow {
            val point = pointsApi.activatePoint(pointId)
            emit(point)
        }
            .map { point -> point.toDomain() }
            .flowOn(dispatcher)
    }

    override suspend fun createPoint(point: CreatePointDomain): Flow<Boolean> {
        return flow {
            val pointResponse = pointsApi.createPoint(point.toData())
            emit(pointResponse)
        }
            .map { true }
            .flowOn(dispatcher)
    }

    override suspend fun updatePoint(point: CreatePointDomain, pointId: String): Flow<Boolean> {
        return flow {
            val pointResponse = pointsApi.updatePoint(point.toData(), pointId)
            emit(pointResponse)
        }
            .map { true }
            .flowOn(dispatcher)
    }
}