package com.project2.partner.points.impl.ui.price_list

import androidx.lifecycle.SavedStateHandle
import com.project2.core.presentation.adapter_delegate.DelegateAdapterItem
import com.project2.core.presentation.di.assisted.AssistedViewModelFactory
import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.core.utils.common.toImmutableList
import com.project2.partner.points.api.domain.models.PointType
import com.project2.partner.points.api.domain.usecase.price_list.GetPriceListForPaymentUseCase
import com.project2.partner.points.impl.ui.list.mapper.toPresentation
import com.project2.partner.points.impl.ui.price_list.adapter.toAdapterItem
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart

class PointPriceListViewModel @AssistedInject constructor(
    private val getPriceListForPayment: GetPriceListForPaymentUseCase,
    @Assisted val savedStateHandle: SavedStateHandle,
    override val exceptionHandler: CoroutineExceptionHandler,
): BaseViewModel() {

    @AssistedFactory
    interface Factory : AssistedViewModelFactory<PointPriceListViewModel>

    private val _priceListFlow = MutableStateFlow<List<DelegateAdapterItem>>(listOf())
    val priceListFlow = _priceListFlow.asStateFlow()

    private val _uiState = MutableStateFlow<UiState>(UiState.Loading)
    val uiState = _uiState.asStateFlow()

    private val args = PointPriceListFragmentArgs.fromSavedStateHandle(savedStateHandle)

    private val pointId: String
        get() = args.pointId

    private val pointType: PointType
        get() = PointType.valueOf(args.type)

    init {
        getPriceListByPayment()
    }

    fun getPriceListByPayment() {
        launchOnViewModelScope {
            getPriceListForPayment(pointId)
                .onStart { _uiState.value = UiState.Loading }
                .map { it.map { pointType -> pointType.toPresentation() } }
                .map { typesAndCategories ->
                    val categoriesWithProducts = typesAndCategories
                        .filter { it.type.type == pointType }
                        .flatMap { it.categories }

                    val priceList = mutableListOf<DelegateAdapterItem>()

                    categoriesWithProducts.forEach {
                        val pointTypeUi = it.products.first().category.pointTypeUi
                        val categoryAdapterItem = it.toAdapterItem(pointTypeUi = pointTypeUi)

                        priceList.add(categoryAdapterItem)

                        val products = it.products.mapIndexed { index, productUi ->
                            productUi.toAdapterItem(index == it.products.size - 1)
                        }

                        priceList.addAll(products)
                    }
                    priceList.toImmutableList()
                }
                .catch { _uiState.value = UiState.Error(it) }
                .collect { priceList ->
                    _priceListFlow.value = priceList
                    _uiState.value = UiState.Success
                }
        }
    }
}