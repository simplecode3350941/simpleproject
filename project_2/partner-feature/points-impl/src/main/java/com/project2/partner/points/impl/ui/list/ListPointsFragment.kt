package com.project2.partner.points.impl.ui.list

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.ComponentDependenciesProvider
import com.project2.core.presentation.di.HasComponentDependencies
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.presentation.paging.FooterAdapter
import com.project2.core.presentation.paging.addOnInfiniteScrollListener
import com.project2.core.utils.common.lazyUnsafe
import com.project2.core.utils.view.setOnClickDelayListener
import com.project2.partner.points.api.ui.list.models.PointShortUi
import com.project2.partner.points.impl.R
import com.project2.partner.points.impl.databinding.FragmentListPointsBinding
import com.project2.partner.points.impl.ui.list.adapter.PartnerPointsAdapter
import com.project2.partner.points.impl.ui.list.di.DaggerListPointsComponent
import com.project2.partner.profile.api.data.remote.models.PartnerRole
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class ListPointsFragment: BaseMvvmFragment(), HasComponentDependencies {

    @Inject
    override lateinit var dependencies: ComponentDependenciesProvider

    private val viewModel: PartnerListServicesViewModel by viewModels {
        viewModelFactory
    }

    private val pointsAdapter: PartnerPointsAdapter by lazyUnsafe {
        PartnerPointsAdapter(::onPointClick)
    }

    private val footerAdapter: FooterAdapter by lazyUnsafe {
        FooterAdapter(onEndReached)
    }

    override fun inject() {
        DaggerListPointsComponent.builder()
            .listPointsDependencies(findComponentDependencies())
            .build()
            .inject(this)
    }

    private val onEndReached: () -> Unit = { viewModel.paginate() }

    override fun getLayoutResId(): Int = R.layout.fragment_list_points

    override val synthetic = SyntheticBinding(FragmentListPointsBinding::inflate)

    override fun initUi(savedInstanceState: Bundle?) {
        synthetic.binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = pointsAdapter.withLoadStateFooter(footerAdapter)
            setHasFixedSize(true)
            clearOnScrollListeners()
            addOnInfiniteScrollListener(onEndReached)
        }

        synthetic.binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.refreshData()
        }

        synthetic.binding.floatButtonCreatePoint.setOnClickDelayListener {
            findNavController().navigate(R.id.navigation_create_point)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.updateState.collectLatest(::updateContentState)
        }
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.paginationState.collectLatest(::updatePaginationState)
        }
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.pointsFlow.collectLatest(::submitPoints)
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.refreshData()
    }

    private fun onPointClick(id: String) {
        findNavController().navigate(R.id.action_listPoints_to_detailPoint,
            bundleOf(
                "point_id" to id,
                "partner_role" to PartnerRole.OWNER.name
            )
        )
    }

    private fun updateContentState(uiState: UiState) {
        Timber.d("updateContentState %s", uiState)
        when(uiState) {
            is UiState.Loading -> {
                showHideLoading(true)
                showHideContent(false)
            }
            is UiState.Error -> {
                Timber.e(uiState.error)
                showHideLoading(false)
                showErrorView()
                showHideContent(false)
            }
            is UiState.Empty -> {
                showHideLoading(false)
                showEmptyView()
                showHideContent(false)
            }
            is UiState.Success -> {
                showHideLoading(false)
                showHideContent(true)
            }
        }
    }

    private fun updatePaginationState(paginationState: PaginationState) {
        Timber.d("updatePaginationState %s", paginationState)
        val swipeRefreshLayout = synthetic.binding.swipeRefreshLayout

        when(paginationState) {
            is PaginationState.Loading -> footerAdapter.loadState = LoadState.Loading
            is PaginationState.Error -> {
                swipeRefreshLayout.isRefreshing = false
                footerAdapter.loadState = LoadState.Error(paginationState.error)
            }
            is PaginationState.Success -> {
                swipeRefreshLayout.isRefreshing = false
                footerAdapter.loadState = LoadState.NotLoading(false)
            }
        }
    }

    private fun showHideContent(show: Boolean) {
        synthetic.binding.recyclerView.isVisible = show
        synthetic.binding.floatButtonCreatePoint.isVisible = show
    }

    private fun showHideLoading(show: Boolean) {
        val shimmer = synthetic.binding.includePointsLoadingView.shimmerView
        val emptyView = synthetic.binding.includeEmptyView.emptyView
        val swipeRefreshLayout = synthetic.binding.swipeRefreshLayout

        if(show) {
            emptyView.isVisible = false
            shimmer.showShimmer(true)
        } else {
            swipeRefreshLayout.isRefreshing = false
            shimmer.hideShimmer()
        }
        shimmer.isVisible = show
    }

    private fun showErrorView() {
        val errorView = synthetic.binding.includeErrorView.errorView
        errorView.isVisible = true
        synthetic.binding.includeErrorView.buttonRefresh.setOnClickListener {
            errorView.isVisible = false
            viewModel.refreshData()
        }
    }

    private fun showEmptyView() {
        val emptyView = synthetic.binding.includeEmptyView.emptyView
        emptyView.isVisible = true
    }

    private fun submitPoints(points: List<PointShortUi>) {
        Timber.d("showPoints size %s", points.size)
        pointsAdapter.submitData(viewLifecycleOwner.lifecycle, PagingData.from(points))
    }
}