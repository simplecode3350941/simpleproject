package com.project2.partner.orders.impl.domain.usecase

import com.project2.partner.orders.api.domain.models.PartnerOrderDetailDomainModel
import com.project2.partner.orders.api.domain.repository.PartnerOrdersRepository
import com.project2.partner.orders.api.domain.usecase.GetPartnerOrderDetailUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPartnerOrderDetailUseCaseImpl @Inject constructor(
    private val repository: PartnerOrdersRepository
) : GetPartnerOrderDetailUseCase {
    override suspend fun invoke(id: String): Flow<PartnerOrderDetailDomainModel> {
        return repository.getOrderDetail(id)
    }
}