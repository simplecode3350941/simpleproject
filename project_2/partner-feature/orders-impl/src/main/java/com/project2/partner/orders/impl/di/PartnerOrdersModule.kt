package com.project2.partner.orders.impl.di

import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.core.network.BuildConfig
import com.project2.partner.orders.api.data.remote.api.PartnerOrdersApi
import com.project2.partner.orders.api.domain.repository.PartnerOrdersRepository
import com.project2.partner.orders.api.domain.usecase.CreateOrderUseCase
import com.project2.partner.orders.api.domain.usecase.GetPartnerOrderDetailUseCase
import com.project2.partner.orders.api.domain.usecase.GetPartnerOrdersUseCase
import com.project2.partner.orders.api.domain.usecase.PayOrderUseCase
import com.project2.partner.orders.impl.data.repository.PartnerOrdersRepositoryImpl
import com.project2.partner.orders.impl.domain.usecase.CreateOrderUseCaseImpl
import com.project2.partner.orders.impl.domain.usecase.GetPartnerOrderDetailUseCaseImpl
import com.project2.partner.orders.impl.domain.usecase.GetPartnerOrdersUseCaseImpl
import com.project2.partner.orders.impl.domain.usecase.PayOrderUseCaseImpl
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class PartnerOrdersModule {
    @Provides
    @Singleton
    fun providePartnerOrdersApi(
        @Named("partner_api") okHttpClient: OkHttpClient,
        @Named("moshi_converter") moshiConverterFactory: MoshiConverterFactory
    ): PartnerOrdersApi {
        val builder = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.project2_API_URL)
            .addConverterFactory(moshiConverterFactory)
            .build()

        return builder.create(PartnerOrdersApi::class.java)
    }

    @Provides
    fun providePartnerOrdersRepository(
        contractApi: PartnerOrdersApi,
        @IoDispatcher dispatcher: CoroutineDispatcher
    ): PartnerOrdersRepository {
        return PartnerOrdersRepositoryImpl(contractApi, dispatcher)
    }

    @Provides
    fun provideGetPartnerOrdersUseCase(
        repository: PartnerOrdersRepository
    ): GetPartnerOrdersUseCase {
        return GetPartnerOrdersUseCaseImpl(repository)
    }

    @Provides
    fun provideGetPartnerOrderDetailUseCase(
        repository: PartnerOrdersRepository
    ): GetPartnerOrderDetailUseCase {
        return GetPartnerOrderDetailUseCaseImpl(repository)
    }

    @Provides
    fun provideCreateOrderUseCase(
        repository: PartnerOrdersRepository
    ): CreateOrderUseCase {
        return CreateOrderUseCaseImpl(repository)
    }

    @Provides
    fun providePayOrderUseCase(
        repository: PartnerOrdersRepository
    ): PayOrderUseCase {
        return PayOrderUseCaseImpl(repository)
    }
}