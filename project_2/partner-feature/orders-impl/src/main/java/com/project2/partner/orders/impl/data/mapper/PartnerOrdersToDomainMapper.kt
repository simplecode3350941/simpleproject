package com.project2.partner.orders.impl.data.mapper

import com.project2.partner.orders.api.data.remote.models.CreatedOrderResponse
import com.project2.partner.orders.api.data.remote.models.PartnerOrderDetailResponse
import com.project2.partner.orders.api.data.remote.models.PartnerOrderResponse
import com.project2.partner.orders.api.data.remote.models.PointResponse
import com.project2.partner.orders.api.data.remote.models.PointTypeResponse
import com.project2.partner.orders.api.data.remote.models.ServicesResponse
import com.project2.partner.orders.api.domain.models.CreatedOrderDomainModel
import com.project2.partner.orders.api.domain.models.PartnerOrderDetailDomainModel
import com.project2.partner.orders.api.domain.models.PartnerOrderDomainModel
import com.project2.partner.orders.api.domain.models.PointDomainModel
import com.project2.partner.orders.api.domain.models.PointTypeDomainModel
import com.project2.partner.orders.api.domain.models.ServicesDomainModel

fun PartnerOrderResponse.toDomain() = PartnerOrderDomainModel(
    id = id,
    date = date,
    number = number,
    status = status,
    amount = amount,
    pointName = pointName
)

fun PartnerOrderDetailResponse.toDomain() = PartnerOrderDetailDomainModel(
    id = id,
    date = date,
    number = number,
    status = status,
    amount = amount,
    comment = comment,
    pan = pan,
    point = point.toDomain(),
    services = services.map {it.toDomain()}
)

fun PointResponse.toDomain() = PointDomainModel(
    id = id,
    name = name,
    types = types.map { it.toDomain() },
    address = address
)

fun ServicesResponse.toDomain() = ServicesDomainModel(
    name = name,
    price = price,
    quantity = quantity,
    serviceID = serviceID
)

fun PointTypeResponse.toDomain() = PointTypeDomainModel(
    localizedName = localizedName,
    type = type
)

fun CreatedOrderResponse.toDomain() = CreatedOrderDomainModel(
    id = id,
    date = date,
    point = point.toDomain(),
    comment = comment,
    number = number,
    status = status,
    amount = amount,
    pan = pan,
    services = services.map { it.toDomain() }
)