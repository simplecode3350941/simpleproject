package com.project2.partner.orders.impl.ui.list.adapter

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.core.utils.view.setOnClickDelayListener
import com.project2.partner.orders.impl.databinding.PartnerOrderItemBinding
import com.project2.partner.orders.impl.ui.models.PartnerOrderUiModel
import com.project2.partner.orders.impl.ui.models.formattedAmount
import com.project2.partner.orders.impl.ui.models.friendlyStatus
import com.project2.partner.orders.impl.ui.models.getFriendlyDate
import com.project2.partner.orders.impl.ui.models.statusBackground
import com.project2.partner.orders.impl.ui.models.statusTextColor

class PartnerOrderAdapter(
    private val onClickListener: (String) -> Unit
) : PagingDataAdapter<PartnerOrderUiModel, PartnerOrderAdapter.ViewHolder>(
    DIFF_CALLBACK
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, onClickListener)
        }
    }

    inner class ViewHolder(parent: ViewGroup) :
        BindingViewHolder<PartnerOrderItemBinding>(
            parent,
            PartnerOrderItemBinding::inflate
        ) {

        fun bind(item: PartnerOrderUiModel, onClickListener: (String) -> Unit) = with(binding) {
            textViewOrderData.text = item.getFriendlyDate(textViewOrderData.context)
            textViewOrderNumber.text = item.number
            textViewAmount.text = item.formattedAmount
            textViewOrderStatus.text = item.friendlyStatus(textViewOrderStatus.context)
            textViewOrderStatus.background = item.statusBackground(textViewOrderStatus.context)
            textViewOrderStatus.setTextColor(item.statusTextColor(textViewOrderStatus.context))

            itemView.setOnClickDelayListener {
                onClickListener(item.id)
            }
        }
    }

    companion object {

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<PartnerOrderUiModel>() {
            override fun areItemsTheSame(
                oldItem: PartnerOrderUiModel,
                newItem: PartnerOrderUiModel
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: PartnerOrderUiModel,
                newItem: PartnerOrderUiModel
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}