package com.project2.partner.orders.impl.ui.mapper

import com.project2.partner.orders.api.domain.models.CreateOrderRequestDomainModel
import com.project2.partner.orders.api.domain.models.PointTypeDomainModel
import com.project2.partner.orders.api.domain.models.PointTypesDomainModel
import com.project2.partner.orders.api.domain.models.ServiceTypesDomainModel
import com.project2.partner.orders.api.domain.models.ServicesDomainModel
import com.project2.partner.orders.impl.ui.models.CreateOrderRequestUi
import com.project2.partner.orders.impl.ui.models.PointTypeUi
import com.project2.partner.orders.impl.ui.models.PointTypesUi
import com.project2.partner.orders.impl.ui.models.ServiceTypesUi
import com.project2.partner.orders.impl.ui.models.ServicesUiModel

fun CreateOrderRequestUi.toDomain() = CreateOrderRequestDomainModel(
    pointID = pointID,
    comment = comment,
    pointTypes = pointTypes.map { it.toDomain() }
)

fun PointTypesUi.toDomain() = PointTypesDomainModel(
    staticID = staticID,
    type = type.toDomain(),
    serviceTypes = serviceTypes.map {it.toDomain()}
)

fun PointTypeUi.toDomain() = PointTypeDomainModel(
    localizedName = localizedName,
    type = type
)

fun ServiceTypesUi.toDomain() = ServiceTypesDomainModel(
    staticID = staticID,
    name = name,
    services = services.map { it.toDomain() }
)

fun ServicesUiModel.toDomain() = ServicesDomainModel(
    name = name,
    price = price,
    quantity = quantity,
    serviceID = serviceID
)