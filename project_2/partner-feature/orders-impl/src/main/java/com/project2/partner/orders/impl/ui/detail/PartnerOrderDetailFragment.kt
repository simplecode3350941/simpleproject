package com.project2.partner.orders.impl.ui.detail

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.ComponentDependenciesProvider
import com.project2.core.presentation.di.HasComponentDependencies
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.utils.common.lazyUnsafe
import com.project2.partner.orders.api.enums.OrderStatus
import com.project2.partner.orders.impl.R
import com.project2.partner.orders.impl.databinding.FragmentPartnerOrderDetailBinding
import com.project2.partner.orders.impl.ui.detail.adapter.PartnerOrderDetailServicesAdapter
import com.project2.partner.orders.impl.ui.detail.pay_status_bottom_sheet.PartnerPayOrderStatusBottomSheet
import com.project2.partner.orders.impl.ui.di.DaggerPartnerOrdersComponent
import com.project2.partner.orders.impl.ui.di.PartnerOrdersDependencies
import com.project2.partner.orders.impl.ui.models.PartnerOrderDetailUiModel
import com.project2.partner.orders.impl.ui.models.allPointTypes
import com.project2.partner.orders.impl.ui.models.formattedAmount
import com.project2.partner.orders.impl.ui.models.friendlyStatus
import com.project2.partner.orders.impl.ui.models.getFriendlyDate
import com.project2.partner.orders.impl.ui.models.statusBackground
import com.project2.partner.orders.impl.ui.models.statusTextColor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class PartnerOrderDetailFragment : BaseMvvmFragment(), HasComponentDependencies {

    @Inject
    override lateinit var dependencies: ComponentDependenciesProvider

    private val viewModel: PartnerOrderDetailViewModel by assistedViewModel {
        findComponentDependencies<PartnerOrdersDependencies>()
            .getDetailOrderViewModelFactory()
    }

    private val orderServicesAdapter: PartnerOrderDetailServicesAdapter by lazyUnsafe {
        PartnerOrderDetailServicesAdapter()
    }

    private val orderId: String
        get() = requireArguments().getString(PARTNER_ORDER_ID).toString()

    private var orderNumber = ""

    override fun inject() {
        DaggerPartnerOrdersComponent.builder()
            .partnerOrdersDependencies(findComponentDependencies())
            .build()
            .inject(this)
    }

    override fun getLayoutResId(): Int = R.layout.fragment_partner_order_detail

    override val synthetic = SyntheticBinding(FragmentPartnerOrderDetailBinding::inflate)

    override fun initUi(savedInstanceState: Bundle?) {
        viewModel.onLoadOrderDetailInfo(orderId)

        synthetic.binding.includeViewPartnerOrderAppbar.textViewNavbarTitle.text =
            getString(R.string.partners_order)

        synthetic.binding.includeViewPartnerOrderAppbar.imgViewArrowBack.setOnClickListener {
            popBack()
        }

        synthetic.binding.rvViewOrderServices.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = orderServicesAdapter
        }

        synthetic.binding.buttonOrderPay.setOnClickListener {
            findNavController().navigate(R.id.action_orderDetail_to_qrCodePayOrder)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.updateState.collectLatest(::bindUiStates)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.paymentUpdateState.collectLatest(::bindPaymentUiStates)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.orderDetail.collectLatest(::showOrderDetail)
        }
    }

    private fun showOrderPayStatusBottomSheet(isOrderPay: Boolean) {
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            PartnerPayOrderStatusBottomSheet.show(
                childFragmentManager,
                isPaySuccess = isOrderPay,
                payOrderNumber = orderNumber
            )
        }
    }

    override fun onPause() {
        super.onPause()
        viewModel.onResetPayInfo()
    }

    private fun bindUiStates(uiState: UiState) {
        Timber.d("Partner order detail UiState - $uiState")
        when (uiState) {
            is UiState.Loading -> {
                showHideLoading(true)
            }
            is UiState.Error -> {
                Timber.e(uiState.error)
                showHideProgressBar(false)
                showHideLoading(false)
                showErrorView(uiState.error.localizedMessage)
            }

            is UiState.Success -> {
                showHideProgressBar(false)
                showHideLoading(false)
            }
        }
    }

    private fun bindPaymentUiStates(paymentUiState: PaymentUiState) {
        Timber.d("Partner payment order PaymentUiState - $paymentUiState")
        when (paymentUiState) {
            is PaymentUiState.Loading -> {
                showHideProgressBar(true)
            }

            is PaymentUiState.Error -> {
                showHideProgressBar(false)
                showOrderPayStatusBottomSheet(false)
            }

            is PaymentUiState.Success -> {
                showHideProgressBar(false)
                viewModel.onLoadOrderDetailInfo(orderId)
                showOrderPayStatusBottomSheet(true)
            }
        }
    }

    private fun showHideProgressBar(show: Boolean) {
        synthetic.binding.layoutProgressBar.isVisible = show
    }

    private fun showHideLoading(show: Boolean) {
        val shimmer = synthetic.binding.includeOrderDetailLoadingView.shimmerView
        if (show) {
            shimmer.showShimmer(true)
            synthetic.binding.scrollViewOrderDetail.isVisible = false
        } else {
            shimmer.hideShimmer()
            synthetic.binding.scrollViewOrderDetail.isVisible = true
        }
        shimmer.isVisible = show
    }

    private fun showErrorView(error: String?) {
        val errorView = synthetic.binding.includeOrderDetailErrorView.errorView
        if (!error.isNullOrEmpty()) {
            synthetic.binding.includeOrderDetailErrorView.textViewErrorMessage.text = error
        }
        synthetic.binding.scrollViewOrderDetail.isVisible = false
        errorView.isVisible = true

        synthetic.binding.layoutProgressBar.isVisible = false
        synthetic.binding.buttonOrderPay.isVisible = false

        synthetic.binding.includeOrderDetailErrorView.buttonRefresh.setOnClickListener {
            errorView.isVisible = false
            synthetic.binding.buttonOrderPay.isVisible = true
            viewModel.onLoadOrderDetailInfo(orderId)
        }
    }

    private fun showOrderDetail(orderDetail: PartnerOrderDetailUiModel) {

        synthetic.binding.textOrderDate.text = orderDetail.getFriendlyDate(requireContext())
        synthetic.binding.textOrderPointName.text = orderDetail.point.name
        synthetic.binding.textOrderPointType.text = orderDetail.allPointTypes

        synthetic.binding.textOrderPrice.text = orderDetail.formattedAmount
        synthetic.binding.textViewOrderStatus.text = orderDetail.friendlyStatus(requireContext())
        synthetic.binding.textViewOrderStatus.background =
            orderDetail.statusBackground(requireContext())
        synthetic.binding.textViewOrderStatus.setTextColor(
            orderDetail.statusTextColor(
                requireContext()
            )
        )
        orderNumber = orderDetail.number
        synthetic.binding.textOrderValue.text = orderDetail.number
        synthetic.binding.textOrderAddressValue.text = orderDetail.point.address

        orderServicesAdapter.submitList(orderDetail.services)

        synthetic.binding.layoutViewComment.isVisible = !orderDetail.comment.isNullOrEmpty()
        synthetic.binding.textOrderCommentValue.text = orderDetail.comment

        synthetic.binding.buttonOrderPay.isVisible =
            !(orderDetail.status == OrderStatus.PAID || orderDetail.status == OrderStatus.REFUND)
    }

    companion object{
        const val PARTNER_ORDER_ID = "partner_order_id"
    }
}