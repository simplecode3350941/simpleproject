package com.project2.partner.orders.impl.ui.create.add_comment

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResult
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.dialog.BaseBottomSheetDialogFragment
import com.project2.partner.orders.impl.R
import com.project2.partner.orders.impl.databinding.PartnerAddCommentBottomSheetBinding

class PartnerCreateOrderAddCommentBottomSheet: BaseBottomSheetDialogFragment() {

    override val synthetic = SyntheticBinding(PartnerAddCommentBottomSheetBinding::inflate)

    private val comment: String
        get() { return synthetic.binding.editTextCommentValue.text.trim().toString()}

    private val commentToEdit: String?
        get() = arguments?.getString(PARTNER_COMMENT_KEY)


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        dialog.setOnShowListener {
            (dialog.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout?)?.let {
                BottomSheetBehavior.from(it).state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (commentToEdit != null) {
            synthetic.binding.editTextCommentValue.setText(commentToEdit)
        }

        synthetic.binding.buttonAdd.setOnClickListener {
            setFragmentResult(
                PARTNER_ADD_COMMENT_KEY,
                bundleOf(PARTNER_COMMENT_KEY to comment)
            )
            dismiss()
        }
    }

    companion object {
        const val TAG = "ADD_COMMENT_BOTTOM_SHEET"
        const val PARTNER_ADD_COMMENT_KEY = "partner_add_comment_key"
        const val PARTNER_COMMENT_KEY = "comment"

        fun show(manager: FragmentManager) {
            PartnerCreateOrderAddCommentBottomSheet().show(manager, TAG)
        }
        fun show(manager: FragmentManager, comment: String?) {
            PartnerCreateOrderAddCommentBottomSheet().apply {
                arguments = bundleOf(
                    PARTNER_COMMENT_KEY to comment
                )
            }.show(manager, TAG)
        }
    }
}