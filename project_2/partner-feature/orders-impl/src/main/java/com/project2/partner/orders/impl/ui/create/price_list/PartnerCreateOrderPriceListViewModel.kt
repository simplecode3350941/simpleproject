package com.project2.partner.orders.impl.ui.create.price_list

import com.project2.core.presentation.adapter_delegate.DelegateAdapterItem
import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.core.utils.common.toImmutableList
import com.project2.partner.orders.impl.ui.mapper.toPresentation
import com.project2.partner.orders.impl.ui.models.CategoryWithProductsUi
import com.project2.partner.orders.impl.ui.models.ProductUi
import com.project2.partner.orders.impl.ui.models.ServicesSelectedUiModel
import com.project2.partner.points.api.domain.usecase.price_list.GetPriceListForPaymentUseCase
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PartnerCreateOrderPriceListViewModel @Inject constructor(
    private val getPriceListByPointId: GetPriceListForPaymentUseCase,
    override val exceptionHandler: CoroutineExceptionHandler,
) : BaseViewModel() {

    private val _priceListFlow = MutableStateFlow<List<DelegateAdapterItem>>(listOf())
    val priceListFlow = _priceListFlow.asStateFlow()

    private val _uiState = MutableStateFlow<UiState>(UiState.Loading)
    val uiState = _uiState.asStateFlow()

    private var categoryProducts = mutableListOf<CategoryWithProductsUi>()

    private fun getCurrentList() = _priceListFlow.value.toMutableList()

    private var servicesSelectedList = mutableListOf<ServicesSelectedUiModel>()

    fun onLoadPriceByPointId(pointId: String) {
        launchOnViewModelScope {
            getPriceListByPointId(pointId)
                .onStart { _uiState.value = UiState.Loading }
                .map { it.map { it.toPresentation() } }
                .map {
                    categoryProducts.clear()
                    val categoryItemList = mutableListOf<CategoryItem>()
                    it.forEachIndexed { index, categoryItem ->
                        categoryItem.categories.forEach {
                            categoryProducts.add(
                                CategoryWithProductsUi(
                                    id = it.id,
                                    categoryName = it.categoryName,
                                    products = it.products,
                                    badgeValue = it.badgeValue,
                                    categoryTypeId = categoryItem.id,
                                    categoryType = categoryItem.type
                                )
                            )
                            categoryItemList.add(
                                CategoryItem(
                                    item = CategoryItem.Item(
                                        id = it.id,
                                        categoryName = it.categoryName,
                                        badgeValue = 0,
                                        isExpanded = false,
                                        products = it.products
                                    )
                                )
                            )
                        }
                    }
                    categoryItemList.toImmutableList()
                }
                .catch { _uiState.value = UiState.Error(it) }
                .collect { priceList ->
                    _priceListFlow.value = priceList
                    _uiState.value = UiState.Success
                }
        }
    }

    fun onCategoryClicked(itemCat: CategoryItem) {
        launchOnViewModelScope {
            val item = itemCat.item
            categoryProducts.find { it.id == item.id }?.products?.let { productsUi ->
                val products = productsUi.mapIndexed { index, product ->
                    ProductItem(
                        item = ProductItem.Item(
                            product = product,
                            parentCategoryId = item.id,
                            isLastInCategory = index == productsUi.size - 1
                        )
                    )
                }

                val currentList = _priceListFlow.value.toMutableList()

                val categoryIndex = currentList.indexOf(itemCat)
                currentList[categoryIndex] = itemCat.copy(
                    item = itemCat.item.copy(isExpanded = !item.isExpanded)
                )

                if (item.isExpanded) {
                    currentList.removeAll(products)
                } else {
                    currentList.addAll(categoryIndex + 1, products)
                }

                withContext(coroutineContext) {
                    _priceListFlow.value = currentList.toImmutableList()
                }
            }
        }
    }

    fun onMinusClick(itemProd: ProductItem) {
        updateListWithQuantityAndBadge(isQuantityIncrease = false, itemProd = itemProd)
    }

    fun onPlusClick(itemProd: ProductItem) {
        updateListWithQuantityAndBadge(isQuantityIncrease = true, itemProd = itemProd)
    }

    fun onLoadServicesList() = servicesSelectedList

    fun onResetUserSelectedData() {
        launchOnViewModelScope {
            servicesSelectedList = mutableListOf()
            val clearedList = mutableListOf<DelegateAdapterItem>()

            categoryProducts.forEachIndexed { index, catItem ->
                val productsList = mutableListOf<ProductUi>()
                catItem.products.forEach { prodItem ->
                    productsList.add(prodItem.copy(quantity = 0))
                }
                categoryProducts[index] = CategoryWithProductsUi(
                    id = catItem.id,
                    categoryName = catItem.categoryName,
                    products = productsList,
                    badgeValue = 0,
                    categoryTypeId = catItem.categoryTypeId,
                    categoryType = catItem.categoryType
                )
                val itemCategory = CategoryItem(
                    item = CategoryItem.Item(
                        id = catItem.id,
                        categoryName = catItem.categoryName,
                        badgeValue = 0,
                        isExpanded = false,
                        products = productsList
                    )
                )
                clearedList.add(itemCategory)
            }

            withContext(coroutineContext) {
                _priceListFlow.value = clearedList.toImmutableList()
            }
        }
    }

    fun onSearchData(query: String?) {
        launchOnViewModelScope {
            val queryStr = query?.trim()
            val resultCategoryProducts = mutableListOf<DelegateAdapterItem>()

            when {
                queryStr.isNullOrEmpty() -> {
                    loadAllCategories()
                }
                else -> {
                    categoryProducts.forEachIndexed { _, catItem ->
                        catItem.products.forEach {
                            if (catItem.categoryName.contains(queryStr, true) ||
                                it.name.contains(queryStr, true)
                            ) {
                                resultCategoryProducts.add(
                                    CategoryItem(
                                        item = CategoryItem.Item(
                                            id = catItem.id,
                                            categoryName = catItem.categoryName,
                                            badgeValue = catItem.badgeValue,
                                            isExpanded = false,
                                            products = catItem.products
                                        )
                                    )
                                )
                            }
                        }
                    }
                }
            }

            withContext(coroutineContext) {
                _priceListFlow.value = resultCategoryProducts.toImmutableList()
            }
        }
    }

    fun setItemPrice(itemId: String, price: Double?) {
        launchOnViewModelScope {
            val currentList = getCurrentList()
            val index = currentList.indexOfFirst { it.id() == itemId }
            val newProduct = (currentList.find { it.id() == itemId } as? ProductItem)?.let {
                it.copy(item = it.item.copy(product = it.item.product.copy(price = price)))
            }

            var currentCategoryProducts: MutableList<ProductUi>
            val parentCategoryIndex = currentList.indexOfFirst { it.id() == newProduct?.item?.parentCategoryId }

            val newCategoryItem = (currentList.find { it.id() == newProduct?.item?.parentCategoryId } as? CategoryItem)?.let { categoryItem ->
                val productIndex = categoryItem.item.products.indexOfFirst { it.id == itemId }
                val newCategoryProductItem = categoryItem.item.products.find { it.id == itemId }?.copy(price = price)
                currentCategoryProducts = categoryItem.item.products.toMutableList()
                if (newCategoryProductItem != null) {
                    currentCategoryProducts[productIndex] = newCategoryProductItem
                }
                categoryItem.copy(item = categoryItem.item.copy(products = currentCategoryProducts))
            }

            newProduct?.let { newProductItem ->
                currentList[index] = newProductItem

                newCategoryItem?.let {
                    currentList[parentCategoryIndex] = it
                }

                categoryProducts.forEachIndexed { index, categoryItem ->
                    val products = categoryItem.products.toMutableList()
                    val product = products.find { it.id == itemId }
                    product?.let {
                        products[products.indexOf(product)] = newProductItem.item.product
                        categoryProducts[index] = categoryItem.copy(
                            products = products
                        )
                    }
                }
            }

            if (newProduct != null && price != null) {
                updateServicesList(
                    newQuantity = newProduct.item.product.quantity.takeIf { it > 0 },
                    price = price,
                    prodId = newProduct.item.product.id,
                    prodName = newProduct.item.product.name,
                    parentCategoryId = newProduct.item.parentCategoryId
                )
            }

            withContext(coroutineContext) {
                _priceListFlow.value = currentList.toImmutableList()
            }
        }
    }

    private fun loadAllCategories() {
        launchOnViewModelScope {
            val resultCategoryProducts = mutableListOf<DelegateAdapterItem>()
            categoryProducts.forEach {
                resultCategoryProducts.add(
                    CategoryItem(
                        item = CategoryItem.Item(
                            id = it.id,
                            categoryName = it.categoryName,
                            badgeValue = it.badgeValue,
                            isExpanded = false,
                            products = it.products
                        )
                    )
                )
            }
            withContext(coroutineContext) {
                _priceListFlow.value = resultCategoryProducts.toImmutableList()
            }
        }
    }

    private fun updateListWithQuantityAndBadge(
        isQuantityIncrease: Boolean,
        itemProd: ProductItem
    ) {
        launchOnViewModelScope {
            val item = itemProd.item
            val currentList: MutableList<DelegateAdapterItem> = getCurrentList()
            val index = currentList.indexOfFirst { it.id() == itemProd.id() }
            var newQuantity = 0L
            val newProduct = (currentList.find { it.id() == itemProd.id() } as? ProductItem)?.let {
                newQuantity = it.item.product.quantity
                when (isQuantityIncrease) {
                    true -> newQuantity++
                    false -> {
                        if (it.item.product.quantity != 0L) {
                            newQuantity--
                        }
                    }
                }
                ProductItem(
                    item = ProductItem.Item(
                        product = it.item.product.copy(quantity = newQuantity),
                        parentCategoryId = it.item.parentCategoryId,
                        isLastInCategory = it.item.isLastInCategory
                    )
                )
            }

            newProduct?.let { newProductItem ->
                currentList[index] = newProductItem

                //-------update category with new badge value
                val categoryIndex = currentList.indexOfFirst { it.id() == item.parentCategoryId }
                val category = currentList.find { it.id() == item.parentCategoryId } as? CategoryItem
                var selectedItem = 0
                val categoryId = currentList[categoryIndex].id()
                currentList.forEach {
                    if (it is ProductItem && it.item.parentCategoryId == categoryId) {
                        if (it.item.product.quantity > 0) {
                            selectedItem++
                        }
                    }
                }
                val newCategory = category?.let {
                    getUpdatedCategoryItem(it.item, selectedItem)
                }
                if (newCategory != null) {
                    currentList[categoryIndex] = newCategory
                }

                categoryProducts.forEachIndexed { index, categoryItem ->
                    val products = categoryItem.products.toMutableList()
                    val product = products.find { it.id == itemProd.id() }
                    product?.let {
                        products[products.indexOf(product)] = newProductItem.item.product
                        categoryProducts[index] = categoryItem.copy(
                            products = products,
                            badgeValue = newCategory?.item?.badgeValue ?: 0
                        )
                    }
                }
            }
            if (itemProd.item.product.price != null) {
                updateServicesList(
                    newQuantity = newQuantity,
                    price = itemProd.item.product.price,
                    prodId = itemProd.item.product.id,
                    prodName = itemProd.item.product.name,
                    parentCategoryId = itemProd.item.parentCategoryId
                )
            }

            withContext(coroutineContext) {
                _priceListFlow.value = currentList.toImmutableList()
            }
        }
    }

    private fun getUpdatedCategoryItem(oldItem: CategoryItem.Item, badges: Int): CategoryItem {
        return CategoryItem(
            item = CategoryItem.Item(
                id = oldItem.id,
                categoryName = oldItem.categoryName,
                badgeValue = badges,
                isExpanded = oldItem.isExpanded,
                products = oldItem.products
            )
        )
    }

    private fun updateServicesList(
        newQuantity: Long? = null,
        price: Double,
        prodId: String,
        prodName: String,
        parentCategoryId: String
    ) {
        if (newQuantity != null) {
            var productIndex = -1
            servicesSelectedList.forEachIndexed { index, serviceItem ->
                if (serviceItem.productId == prodId) {
                    productIndex = index
                }
            }
            if (productIndex >= 0) {
                if (newQuantity > 0) {
                    servicesSelectedList[productIndex].quantity = newQuantity
                    servicesSelectedList[productIndex].price = price
                } else {
                    servicesSelectedList.removeAt(productIndex)
                }
            } else {
                categoryProducts.find { it.id == parentCategoryId }.let {
                    if (it?.categoryType != null) {
                        servicesSelectedList.add(
                            ServicesSelectedUiModel(
                                productId = prodId,
                                name = prodName,
                                price = price,
                                quantity = newQuantity,
                                parentCategoryId = parentCategoryId,
                                categoryTypeId = it.categoryTypeId,
                                categoryType = it.categoryType,
                                categoryName = it.categoryName
                            )
                        )
                    } else {
                        _uiState.value = UiState.Error(Throwable("Can not add service"))
                    }
                }
            }
        }
    }
}

data class CategoryItem(
    val item: Item
) : DelegateAdapterItem {
    override fun id() = item.id
    override fun content() = item
    data class Item(
        val id: String,
        val categoryName: String,
        val badgeValue: Int,
        val products: List<ProductUi>,
        var isExpanded: Boolean
    )
}

data class ProductItem(
    val item: Item
) : DelegateAdapterItem {
    override fun id() = item.product.id
    override fun content() = item
    data class Item(
        val product: ProductUi,
        val parentCategoryId: String,
        val isLastInCategory: Boolean
    )
}

sealed interface UiState {
    object Loading : UiState
    object Success : UiState
    data class Error(val error: Throwable) : UiState
}