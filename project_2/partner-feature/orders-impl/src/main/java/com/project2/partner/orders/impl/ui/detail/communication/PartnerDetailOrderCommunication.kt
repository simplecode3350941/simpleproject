package com.project2.partner.orders.impl.ui.detail.communication

import com.project2.partner.orders.impl.ui.models.QrCodeRawValueUiModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.updateAndGet

object PartnerDetailOrderCommunication {

    private val qrCodeRawValueFlow = MutableStateFlow(QrCodeRawValueUiModel())
    private val qrCodeRawValueAsFlow = qrCodeRawValueFlow.asStateFlow()

    fun clear() {
        qrCodeRawValueFlow.value = QrCodeRawValueUiModel()
    }

    suspend fun subscribe(onStateChanged: (QrCodeRawValueUiModel) -> Unit) {
        qrCodeRawValueAsFlow.collectLatest {
            onStateChanged.invoke(it)
        }
    }

    suspend fun setQrCodeValue(qrCodeValue: String) {
        modifyAndEmit {
            it.copy(qrCodeValue = qrCodeValue, isQrCodeValueReceived = true)
        }
    }

    private suspend fun modifyAndEmit(onModifyPoint: (QrCodeRawValueUiModel) -> QrCodeRawValueUiModel) {
        val createPoint = qrCodeRawValueFlow.updateAndGet { onModifyPoint(it) }
        qrCodeRawValueFlow.emit(createPoint)
    }
}