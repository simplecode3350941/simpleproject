package com.project2.partner.orders.impl.domain.usecase

import com.project2.partner.orders.api.domain.repository.PartnerOrdersRepository
import com.project2.partner.orders.api.domain.usecase.PayOrderUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PayOrderUseCaseImpl @Inject constructor(
    private val repository: PartnerOrdersRepository
) : PayOrderUseCase {
    override suspend fun invoke(id: String, qrCode: String): Flow<Boolean> {
        return repository.payOrder(id, qrCode).map { true }
    }
}