package com.project2.partner.orders.impl.ui.detail.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.partner.orders.impl.databinding.PartnerOrderDetailServicesItemBinding
import com.project2.partner.orders.impl.ui.models.ServicesUiModel
import com.project2.partner.orders.impl.ui.models.priceWithQuantity

class PartnerOrderDetailServicesAdapter :
    ListAdapter<ServicesUiModel, PartnerOrderDetailServicesAdapter.ViewHolder>(
        DIFF_CALLBACK
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    inner class ViewHolder(parent: ViewGroup) :
        BindingViewHolder<PartnerOrderDetailServicesItemBinding>(
            parent,
            PartnerOrderDetailServicesItemBinding::inflate
        ) {

        fun bind(item: ServicesUiModel) = with(binding) {
            textTransactionServiceTitle.text = item.name
            textTransactionServicePrice.text = item.priceWithQuantity
        }
    }

    companion object {

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ServicesUiModel>() {
            override fun areItemsTheSame(
                oldItem: ServicesUiModel,
                newItem: ServicesUiModel
            ): Boolean {
                return oldItem.name == newItem.name
            }

            override fun areContentsTheSame(
                oldItem: ServicesUiModel,
                newItem: ServicesUiModel
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}