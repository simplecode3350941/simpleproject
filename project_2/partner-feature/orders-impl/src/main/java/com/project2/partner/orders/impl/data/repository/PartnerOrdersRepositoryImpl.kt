package com.project2.partner.orders.impl.data.repository

import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.partner.orders.api.data.remote.api.PartnerOrdersApi
import com.project2.partner.orders.api.domain.models.CreateOrderRequestDomainModel
import com.project2.partner.orders.api.domain.models.CreatedOrderDomainModel
import com.project2.partner.orders.api.domain.models.PartnerOrderDetailDomainModel
import com.project2.partner.orders.api.domain.models.PartnerOrderDomainModel
import com.project2.partner.orders.api.domain.repository.PartnerOrdersRepository
import com.project2.partner.orders.impl.data.mapper.toData
import com.project2.partner.orders.impl.data.mapper.toDomain
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PartnerOrdersRepositoryImpl @Inject constructor(
    private val ordersApi: PartnerOrdersApi,
    @IoDispatcher
    private val dispatcher: CoroutineDispatcher
) : PartnerOrdersRepository {
    override suspend fun getOrders(
        pageNumber: Int,
        pageSize: Int
    ): Flow<List<PartnerOrderDomainModel>> {
        return flow {
            val orders = ordersApi.getOrders(pageNumber, pageSize).content
            emit(orders)
        }
            .map { orders ->
                orders.map { it.toDomain() }
            }
            .flowOn(dispatcher)
    }

    override suspend fun getOrderDetail(id: String): Flow<PartnerOrderDetailDomainModel> {
        return flow {
            val order = ordersApi.getOrderDetail(id)
            emit(order)
        }
            .map { order -> order.toDomain() }
            .flowOn(dispatcher)
    }

    override suspend fun createOrder(
        createOrderRequest: CreateOrderRequestDomainModel
    ): Flow<CreatedOrderDomainModel> {
        return flow {
            val createdOrder = ordersApi.createOrder(createOrderRequest.toData())
            emit(createdOrder)
        }
            .map { it.toDomain() }
            .flowOn(dispatcher)
    }

    override suspend fun payOrder(id: String, qrCode: String): Flow<Unit> {
        return flow {
            ordersApi.payOrder(id, qrCode)
            emit(Unit)
        }.flowOn(dispatcher)
    }
}