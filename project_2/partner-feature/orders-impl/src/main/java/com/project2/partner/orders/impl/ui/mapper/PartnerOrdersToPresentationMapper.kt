package com.project2.partner.orders.impl.ui.mapper

import com.project2.core.utils.common.toImmutableList
import com.project2.partner.orders.api.domain.models.CreatedOrderDomainModel
import com.project2.partner.orders.api.domain.models.PartnerOrderDetailDomainModel
import com.project2.partner.orders.api.domain.models.PartnerOrderDomainModel
import com.project2.partner.orders.api.domain.models.PointDomainModel
import com.project2.partner.orders.api.domain.models.ServicesDomainModel
import com.project2.partner.orders.impl.ui.models.CategoryWithProductsUi
import com.project2.partner.orders.impl.ui.models.CreateOrderRequestUi
import com.project2.partner.orders.impl.ui.models.CreatedOrderUiModel
import com.project2.partner.orders.impl.ui.models.PartnerOrderDetailUiModel
import com.project2.partner.orders.impl.ui.models.PartnerOrderUiModel
import com.project2.partner.orders.impl.ui.models.PointTypeUi
import com.project2.partner.orders.impl.ui.models.PointTypeWithCategoryUi
import com.project2.partner.orders.impl.ui.models.PointTypesUi
import com.project2.partner.orders.impl.ui.models.PointUiModel
import com.project2.partner.orders.impl.ui.models.ProductUi
import com.project2.partner.orders.impl.ui.models.ServiceTypesUi
import com.project2.partner.orders.impl.ui.models.ServicesSelectedUiModel
import com.project2.partner.orders.impl.ui.models.ServicesUiModel
import com.project2.partner.points.api.domain.models.CategoryWithProductsDomain
import com.project2.partner.points.api.domain.models.PointShortDomain
import com.project2.partner.points.api.domain.models.PointTypeDomain
import com.project2.partner.points.api.domain.models.PointTypeWithCategoryDomain
import com.project2.partner.points.api.domain.models.ProductDomain
import com.project2.partner.points.api.ui.list.models.MODERATION_STATUSES
import com.project2.partner.points.api.ui.list.models.PointShortUi
import com.project2.partner.points.api.ui.list.models.PointStatus

fun PartnerOrderDomainModel.toPresentation() = PartnerOrderUiModel(
    id = id,
    date = date,
    number = number,
    status = status,
    amount = amount,
    pointName = pointName
)

fun PartnerOrderDetailDomainModel.toPresentation() = PartnerOrderDetailUiModel(
    id = id,
    date = date,
    number = number,
    status = status,
    amount = amount,
    comment = comment ?: "",
    pan = pan ?: "",
    point = point.toPresentation(),
    services = services.map {it.toPresentation()}
)

fun PointDomainModel.toPresentation() = PointUiModel(
    id = id,
    name = name,
    types = types.map { it.localizedName },
    address = address
)

fun ServicesDomainModel.toPresentation() = ServicesUiModel(
    name = name,
    price = price,
    quantity = quantity,
    serviceID = serviceID
)

fun PointTypeWithCategoryDomain.toPresentation() = PointTypeWithCategoryUi(
    id = id,
    type = type.toPresentation(),
    categories = categories.map { it.toPresentation() }
)

fun PointTypeDomain.toPresentation() = PointTypeUi(
    localizedName = localizedName,
    type = type
)

fun CategoryWithProductsDomain.toPresentation() = CategoryWithProductsUi(
    id = id,
    categoryName = categoryName,
    products = products.map { it.toPresentation() },
    badgeValue = 0,
    categoryTypeId = "",
    categoryType = null
)

fun ProductDomain.toPresentation() = ProductUi(
    id = id,
    name = name,
    price = price,
    quantity = 0
)

fun CreatedOrderDomainModel.toPresentation() = CreatedOrderUiModel(
    id = id,
    date = date,
    point = point.toPresentation(),
    comment = comment,
    number = number,
    status = status,
    amount = amount,
    pan = pan,
    services = services.map { it.toPresentation() }
)

fun String.toPointStatus(): PointStatus {
    return when (this) {
        "ACTIVE" -> PointStatus.ACTIVE
        "DECLINED" -> PointStatus.DECLINED
        "HIDDEN" -> PointStatus.HIDDEN
        in MODERATION_STATUSES -> PointStatus.MODERATION
        else -> PointStatus.ACTIVE
    }
}

fun getCreateOrderRequestUi(
    services: List<ServicesSelectedUiModel>,
    comment: String?,
    pointId: String
): CreateOrderRequestUi {
    val pointTypesList = mutableListOf<PointTypesUi>()

    services.forEachIndexed { index, item ->
        val serviceItem = ServicesUiModel(
            name = item.name,
            price = item.price,
            quantity = item.quantity,
            serviceID = item.productId
        )
        val serviceTypesItem = ServiceTypesUi(
            staticID = item.parentCategoryId,
            name = item.categoryName,
            services = listOf(serviceItem)
        )

        //проверяем есть ли уже в pointTypesList элемент с текущим categoryTypeId
        var pointsIndex = -1
        pointTypesList.forEachIndexed { pointTypeIndex, pointTypesItem ->
            if (pointTypesItem.staticID == item.categoryTypeId) {
                pointsIndex = pointTypeIndex
            }
        }

        if (pointsIndex >= 0) {
            // проверяем есть ли в servicesTypes элемент с текущим parentCategoryId
            var serviceTypeIndex = -1
            pointTypesList[pointsIndex].serviceTypes.forEachIndexed { serviceIndex, serviceTypesItem ->
                if (serviceTypesItem.staticID == item.parentCategoryId) {
                    serviceTypeIndex = serviceIndex
                }
            }
            if (serviceTypeIndex >= 0) {
                // добавляем услугу к существующему элементу serviceTypes
                val updatedServiceTypesItem =
                    pointTypesList[pointsIndex].serviceTypes[serviceTypeIndex]
                val updatedServices = updatedServiceTypesItem.services.toMutableList()
                updatedServices.add(serviceItem)
                pointTypesList[pointsIndex].serviceTypes[serviceTypeIndex].services =
                    updatedServices
            } else {
                // формируем новый элемент в serviceTypes
                val updatedServicesTypes =
                    pointTypesList[pointsIndex].serviceTypes.toMutableList()
                updatedServicesTypes.add(serviceTypesItem)
                pointTypesList[pointsIndex].serviceTypes = updatedServicesTypes
            }
        } else {
            // в pointTypesList нет элемента с текущим categoryTypeId
            // добавляем новый элемент в pointTypesList
            pointTypesList.add(
                PointTypesUi(
                    staticID = item.categoryTypeId,
                    type = item.categoryType,
                    serviceTypes = listOf(serviceTypesItem)
                )
            )
        }
    }
    return CreateOrderRequestUi(
        pointID = pointId,
        comment = comment ?: "",
        pointTypes = pointTypesList.toImmutableList()
    )
}