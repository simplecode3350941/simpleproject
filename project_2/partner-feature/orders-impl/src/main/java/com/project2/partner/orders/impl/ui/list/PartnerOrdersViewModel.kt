package com.project2.partner.orders.impl.ui.list

import com.project2.core.presentation.paging.Paginator
import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.core.utils.common.toImmutableList
import com.project2.partner.orders.api.domain.usecase.GetPartnerOrdersUseCase
import com.project2.partner.orders.impl.ui.mapper.toPointStatus
import com.project2.partner.orders.impl.ui.mapper.toPresentation
import com.project2.partner.orders.impl.ui.models.PartnerOrderUiModel
import com.project2.partner.points.api.domain.usecase.GetPartnerDetailPointUseCase
import com.project2.partner.points.api.ui.list.models.PointStatus
import com.project2.partner.profile.api.data.remote.models.PartnerRole
import com.project2.partner.profile.api.domain.usecase.GetPartnerProfileUseCase
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import timber.log.Timber
import javax.inject.Inject

class PartnerOrdersViewModel @Inject constructor(
    private val getOrders: GetPartnerOrdersUseCase,
    private val getPartnerProfileUseCase: GetPartnerProfileUseCase,
    private val getPointUseCase: GetPartnerDetailPointUseCase,
    override val exceptionHandler: CoroutineExceptionHandler
) : BaseViewModel() {

    private val _updateState = MutableStateFlow<UiState>(UiState.Loading)
    val updateState = _updateState.asStateFlow()

    private val _paginationState = MutableStateFlow<PaginationState?>(null)
    val paginationState = _paginationState.asStateFlow().filterNotNull()

    private val _ordersFlow = MutableStateFlow<List<PartnerOrderUiModel>>(listOf())
    val ordersFlow = _ordersFlow.asStateFlow()

    private val _actionsFlow = MutableSharedFlow<Action>()
    val actionsFlow = _actionsFlow.asSharedFlow()

    private val orders: List<PartnerOrderUiModel>
        get() = _ordersFlow.value.toImmutableList()

    private val paginator = Paginator()
    private val isPagination: Boolean
        get() = paginator.getCurrentPage() > 0

    init {
        onRefreshData()
    }

    fun onRefreshData() {
        paginator.reset()
        getOrders()
    }

    fun onPaginate() {
        if(paginator.hasData()) {
            getOrders()
        }
    }

    private fun getOrders() {
        launchOnViewModelScope {
            getOrders(paginator.getCurrentPage(), paginator.pageSize)
                .loadingState()
                .map { orders -> orders.map { it.toPresentation() } }
                .errorState()
                .successState()
        }
    }

    fun onCheckPartnerRole() {
        launchOnViewModelScope {
             getPartnerProfileUseCase()
                 .map { it.role }
                 .collectLatest { partnerRole ->
                     when(partnerRole) {
                         PartnerRole.OWNER -> _actionsFlow.emit(Action.PointSelection)
                         PartnerRole.OPERATOR -> getPoint()
                     }
                 }
        }
    }

    private fun getPoint() {
        launchOnViewModelScope {
            getPointUseCase(null)
                .collectLatest { point ->
                    val action = when(val pointStatus = point.status.toPointStatus()) {
                        PointStatus.ACTIVE -> Action.PriceListSelection(
                            pointId = point.id,
                            pointName = point.name,
                            pointAddress = point.address,
                            pointStatus = pointStatus
                        )
                        else -> Action.PointNotActive(status = pointStatus)
                    }
                    _actionsFlow.emit(action)
                }
        }
    }

    private fun <T> Flow<T>.loadingState(): Flow<T> {
        return onStart {
            when {
                isPagination -> _paginationState.value = PaginationState.Loading
                else -> _updateState.value = UiState.Loading
            }
        }
    }

    private fun <T> Flow<T>.errorState(): Flow<T> {
        return catch {
            when {
                isPagination -> _paginationState.value = PaginationState.Error(it)
                else -> _updateState.value = UiState.Error(it)
            }
        }
    }

    private suspend fun Flow<List<PartnerOrderUiModel>>.successState() {
        collect {
            Timber.d("collect - ${it.size}")
            if (isPagination) {
                _paginationState.value = PaginationState.Success
                paginator.nextPage(it.size)
                _ordersFlow.value =  (orders + it).toImmutableList()
            } else {
                if (it.isEmpty()) {
                    _updateState.value = UiState.Empty
                } else {
                    paginator.nextPage(it.size)
                    _updateState.value = UiState.Success
                    _ordersFlow.value = it
                }
            }
        }
    }
}

sealed interface Action {
    object PointSelection: Action
    data class PointNotActive(val status: PointStatus): Action
    data class PriceListSelection(
        val pointId: String,
        val pointName: String,
        val pointAddress: String,
        val pointStatus: PointStatus
    ): Action
}

sealed interface UiState {
    object Loading: UiState
    object Success: UiState
    data class Error(val error: Throwable): UiState
    object Empty: UiState
}

sealed interface PaginationState {
    object Loading: PaginationState
    object Success: PaginationState
    data class Error(val error: Throwable): PaginationState
}