package com.project2.partner.orders.impl.ui.create.price_list.adapter

import android.view.ViewGroup
import androidx.core.view.isVisible
import com.project2.core.presentation.adapter_delegate.ItemViewDelegate
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.partner.orders.impl.R
import com.project2.partner.orders.impl.databinding.PartnerCreateOrderPriceListItemBinding
import com.project2.partner.orders.impl.ui.create.price_list.ProductItem
import com.project2.partner.orders.impl.ui.models.formattedPrice

class PriceListProductViewHolderDelegate(
    private val onMinusClick: (ProductItem) -> Unit,
    private val onPlusClick: (ProductItem) -> Unit,
    private val onPriceClick: (ProductItem) -> Unit
) : ItemViewDelegate<ProductItem, PriceListProductViewHolderDelegate.ViewHolder>(ProductItem::class.java) {

    override fun onCreateViewHolder(parent: ViewGroup): PriceListProductViewHolderDelegate.ViewHolder {
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, item: ProductItem) {
        holder.bind(item)
    }

    inner class ViewHolder(parent: ViewGroup) : BindingViewHolder<PartnerCreateOrderPriceListItemBinding>(
        parent,
        PartnerCreateOrderPriceListItemBinding::inflate
    ) {
        fun bind(item: ProductItem) = with(binding) {
            val product = item.item.product

            textViewServiceSubcategoryName.text = product.name
            textViewServiceSubcategoryPrice.text = product.formattedPrice.toString()
            textViewServiceSubcategoryPrice.isVisible = product.price != null

            textViewCountValue.text = product.quantity.toString()

            divider.isVisible = item.item.isLastInCategory

            val quantity = Integer.parseInt(textViewCountValue.text.toString())

            val iconMinusDrawable = when (quantity) {
                0 -> getDrawable(R.drawable.ic_minus_grey)
                else -> getDrawable(R.drawable.ic_minus_black)
            }

            imgViewCountPlus.setOnClickListener {
                onPlusClick(item)
            }

            imgViewCountMinus.setImageDrawable(iconMinusDrawable)

            imgViewCountMinus.setOnClickListener {
                if (quantity > 0L) {
                    onMinusClick(item)
                }
            }
            textViewServiceSubcategoryPrice.setOnClickListener {
                onPriceClick(item)
            }

        }
    }
}