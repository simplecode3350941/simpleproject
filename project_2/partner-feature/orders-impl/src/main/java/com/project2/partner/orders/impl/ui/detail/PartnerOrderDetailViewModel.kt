package com.project2.partner.orders.impl.ui.detail

import androidx.lifecycle.SavedStateHandle
import com.project2.core.presentation.di.assisted.AssistedViewModelFactory
import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.partner.orders.api.domain.usecase.GetPartnerOrderDetailUseCase
import com.project2.partner.orders.api.domain.usecase.PayOrderUseCase
import com.project2.partner.orders.impl.ui.detail.communication.PartnerDetailOrderCommunication
import com.project2.partner.orders.impl.ui.mapper.toPresentation
import com.project2.partner.orders.impl.ui.models.PartnerOrderDetailUiModel
import com.project2.partner.orders.impl.ui.models.QrCodeRawValueUiModel
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import timber.log.Timber

class PartnerOrderDetailViewModel @AssistedInject constructor(
    private val getOrderDetail: GetPartnerOrderDetailUseCase,
    private val payOrder: PayOrderUseCase,
    @Assisted private val savedStateHandle: SavedStateHandle,
    override val exceptionHandler: CoroutineExceptionHandler
) : BaseViewModel() {

    @AssistedFactory
    interface Factory : AssistedViewModelFactory<PartnerOrderDetailViewModel>

    private val _paymentUpdateState = MutableStateFlow<PaymentUiState?>(null)
    val paymentUpdateState = _paymentUpdateState.asStateFlow().filterNotNull()

    private val _updateState = MutableStateFlow<UiState>(UiState.Loading)
    val updateState = _updateState.asStateFlow()

    private val _orderDetail = MutableStateFlow<PartnerOrderDetailUiModel?>(null)
    val orderDetail = _orderDetail.asStateFlow().filterNotNull()

    private val orderId: String?
        get() = savedStateHandle[PARTNER_ORDER_ID]

    init {
        subscribeOnQrCodeValue()
    }

    override fun onCleared() {
        PartnerDetailOrderCommunication.clear()
        super.onCleared()
    }

    fun onResetPayInfo() {
        PartnerDetailOrderCommunication.clear()
    }

    fun onLoadOrderDetailInfo(id: String) {
        launchOnViewModelScope {
            getOrderDetail(id)
                .onStart { _updateState.value = UiState.Loading }
                .map { it.toPresentation() }
                .catch { _updateState.value = UiState.Error(it) }
                .collect {
                    Timber.d("collect - $it")
                    _orderDetail.value = it
                    _updateState.value = UiState.Success
                }
        }
    }

    private fun sendPayOrder(qrCodeRawValueUiModel: QrCodeRawValueUiModel) {
        if (qrCodeRawValueUiModel.qrCodeValue.isNotEmpty()) {
            orderId?.let {
                launchOnViewModelScope {
                    payOrder(it, qrCodeRawValueUiModel.qrCodeValue)
                        .onStart { _paymentUpdateState.value = PaymentUiState.Loading }
                        .catch {
                            Timber.d("catch ERROR - ${it.localizedMessage}")
                            _paymentUpdateState.value = PaymentUiState.Error(it)
                        }
                        .collect {
                            Timber.d("collect - $it")
                            _paymentUpdateState.value = PaymentUiState.Success
                        }
                }
            }
        }
    }

    private fun subscribeOnQrCodeValue() {
        launchOnViewModelScope {
            PartnerDetailOrderCommunication.subscribe(::sendPayOrder)
        }
    }

    companion object {
        const val PARTNER_ORDER_ID = "partner_order_id"
    }
}

sealed interface UiState {
    object Loading: UiState
    object Success: UiState
    data class Error(val error: Throwable): UiState
}

sealed interface PaymentUiState {
    object Loading: PaymentUiState
    object Success: PaymentUiState
    data class Error(val error: Throwable): PaymentUiState
}