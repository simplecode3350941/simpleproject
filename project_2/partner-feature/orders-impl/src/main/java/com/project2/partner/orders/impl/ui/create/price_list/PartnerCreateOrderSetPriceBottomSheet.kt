package com.project2.partner.orders.impl.ui.create.price_list

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResult
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.dialog.BaseBottomSheetDialogFragment
import com.project2.core.utils.formatter.Currency
import com.project2.core.utils.formatter.FormatUtils.toFormattedPrice
import com.project2.core.utils.view.showKeyboard
import com.project2.partner.orders.impl.R
import com.project2.partner.orders.impl.databinding.PartnerOrderSetPriceBottomSheetBinding

class PartnerCreateOrderSetPriceBottomSheet: BaseBottomSheetDialogFragment() {

    private val productName: String
        get() = requireArguments().getString(ARGS_PRODUCT_NAME, "")

    private val currentPrice: String
        get() = requireArguments().getString(ARGS_PRICE) ?: ""

    private val priceHint: String
        get() {
            val minPrice = MIN_PRICE.toFormattedPrice(Currency.RUB.toString())
            val maxPrice = MAX_PRICE.toFormattedPrice(Currency.RUB.toString())

            return getString(R.string.partners_create_order_set_price_hint, minPrice, maxPrice)
        }

    override val synthetic = SyntheticBinding(PartnerOrderSetPriceBottomSheetBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        synthetic.binding.textViewTitle.text = productName
        val buttonAdd = synthetic.binding.buttonAdd
        buttonAdd.setOnClickListener {
            sendResult()
        }

        synthetic.binding.editTextPrice.setText(currentPrice)

        val inputLayout = synthetic.binding.inputLayoutPrice
        inputLayout.hint = priceHint
        synthetic.binding.editTextPrice.requestFocus()
        synthetic.binding.editTextPrice.showKeyboard()

        synthetic.binding.editTextPrice.setText(currentPrice)
        synthetic.binding.editTextPrice.doAfterTextChanged { text ->
            val price = text?.ifEmpty { "0" }.toString().toDouble()
            buttonAdd.isEnabled = price in MIN_PRICE..MAX_PRICE
            inputLayout.error = when {
                buttonAdd.isEnabled  -> null
                else -> getString(R.string.partners_create_order_edit_price_error)
            }
        }
    }

    private fun sendResult() {
        val value = synthetic.binding.editTextPrice.text
        val price = when {
            value.isNullOrEmpty() -> null
            else -> value.toString().toDouble()
        }
        setFragmentResult(
            REQUEST_KEY,
            bundleOf(
                ARGS_PRICE to price,
                ARGS_PRODUCT_ID to requireArguments().getString(ARGS_PRODUCT_ID),
            )
        )
        dismiss()
    }

    companion object {

        const val TAG = "PartnerCreateOrderSetPriceBottomSheet"
        const val REQUEST_KEY = "request_key_order_price"
        const val ARGS_PRODUCT_NAME = "args_order_product_name"
        const val ARGS_PRODUCT_ID = "args_order_product_id"
        const val ARGS_PRICE = "args_order_price"
        const val MIN_PRICE = 100.0
        const val MAX_PRICE = 1_000_000.0

        fun show(manager: FragmentManager, currentPrice: String?, productName: String, productId: String) {
            PartnerCreateOrderSetPriceBottomSheet().apply {
                arguments = bundleOf(
                    ARGS_PRODUCT_NAME to productName,
                    ARGS_PRODUCT_ID to productId,
                    ARGS_PRICE to currentPrice
                )
            }.show(manager, TAG)
        }
    }
}