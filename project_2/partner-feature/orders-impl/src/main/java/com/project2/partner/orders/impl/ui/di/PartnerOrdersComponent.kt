package com.project2.partner.orders.impl.ui.di

import com.project2.core.presentation.di.ComponentDependenciesProviderModule
import com.project2.partner.orders.impl.ui.create.add_comment.PartnerCreateOrderAddCommentBottomSheet
import com.project2.partner.orders.impl.ui.create.create_order.PartnerCreateOrderBottomSheet
import com.project2.partner.orders.impl.ui.detail.PartnerOrderDetailFragment
import com.project2.partner.orders.impl.ui.detail.pay_status_bottom_sheet.PartnerPayOrderStatusBottomSheet
import com.project2.partner.orders.impl.ui.detail.qr_code_scanner.PartnerPayQrCodeFragment
import com.project2.partner.orders.impl.ui.list.PartnerOrdersFragment
import dagger.Component

@Component(
    modules = [ComponentDependenciesProviderModule::class],
    dependencies = [PartnerOrdersDependencies::class]
)
interface PartnerOrdersComponent {

    fun inject(ordersFragment: PartnerOrdersFragment)

    fun inject(ordersDetailFragment: PartnerOrderDetailFragment)

    fun inject(createOrderBottomSheet: PartnerCreateOrderBottomSheet)

    fun inject(addCommentBottomSheet: PartnerCreateOrderAddCommentBottomSheet)

    fun inject(partnerPayQrCodeFragment: PartnerPayQrCodeFragment)

    fun inject(partnerPayOrderStatusBottomSheet: PartnerPayOrderStatusBottomSheet)
}