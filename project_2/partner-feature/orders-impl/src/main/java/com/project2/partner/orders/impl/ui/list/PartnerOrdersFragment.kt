package com.project2.partner.orders.impl.ui.list

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.ComponentDependenciesProvider
import com.project2.core.presentation.di.HasComponentDependencies
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.presentation.paging.FooterAdapter
import com.project2.core.presentation.paging.addOnInfiniteScrollListener
import com.project2.core.utils.common.lazyUnsafe
import com.project2.partner.orders.impl.R
import com.project2.partner.orders.impl.databinding.FragmentPartnerOrdersBinding
import com.project2.partner.orders.impl.ui.create.price_list.PartnerCreateOrderPriceListFragment
import com.project2.partner.orders.impl.ui.di.DaggerPartnerOrdersComponent
import com.project2.partner.orders.impl.ui.list.adapter.PartnerOrderAdapter
import com.project2.partner.orders.impl.ui.models.PartnerOrderUiModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class PartnerOrdersFragment : BaseMvvmFragment(), HasComponentDependencies {

    @Inject
    override lateinit var dependencies: ComponentDependenciesProvider

    private val viewModel by viewModels<PartnerOrdersViewModel> {
        viewModelFactory
    }

    private val orderAdapter: PartnerOrderAdapter by lazyUnsafe {
        PartnerOrderAdapter(onOrderClick)
    }

    private val footerAdapter: FooterAdapter by lazyUnsafe {
        FooterAdapter(onEndReached)
    }

    private val onEndReached: () -> Unit = { viewModel.onPaginate() }

    override fun inject() {
        DaggerPartnerOrdersComponent.builder()
            .partnerOrdersDependencies(findComponentDependencies())
            .build()
            .inject(this)
    }

    override fun getLayoutResId(): Int = R.layout.fragment_partner_orders

    override val synthetic = SyntheticBinding(FragmentPartnerOrdersBinding::inflate)

    override fun initUi(savedInstanceState: Bundle?) {
        synthetic.binding.rvViewPartnerOrders.apply {
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = orderAdapter.withLoadStateFooter(footerAdapter)
            setHasFixedSize(true)
            clearOnScrollListeners()
            addOnInfiniteScrollListener(onEndReached)
        }

        synthetic.binding.swipeViewOrdersLayout.apply {
            setOnRefreshListener {
                viewModel.onRefreshData()
                synthetic.binding.swipeViewOrdersLayout.isRefreshing = false
            }
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.updateState.collectLatest(::updateContentState)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.paginationState.collectLatest(::updatePaginationState)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.ordersFlow.collectLatest(::submitOrders)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main.immediate) {
            viewModel.actionsFlow
                .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .distinctUntilChanged()
                .collect(::onAction)
        }

        synthetic.binding.floatButtonCreatePartnerOrder.setOnClickListener {
            viewModel.onCheckPartnerRole()
        }
    }

    private fun onAction(action: Action) {
        when(action) {
            is Action.PointSelection -> findNavController().navigate(R.id.action_listOrders_to_pointSelection)
            is Action.PriceListSelection -> {
                findNavController().navigate(
                    R.id.action_listOrders_to_priceList,
                    bundleOf(
                        PartnerCreateOrderPriceListFragment.PARTNER_POINT_ID_KEY to action.pointId,
                        PartnerCreateOrderPriceListFragment.PARTNER_POINT_NAME_KEY to action.pointName,
                        PartnerCreateOrderPriceListFragment.PARTNER_POINT_ADDRESS_KEY to action.pointAddress
                    )
                )
            }
            is Action.PointNotActive -> {
                val pointStatus = getString(action.status.stringResId)
                val errorMessage = getString(R.string.partners_сreate_order_unavailable, pointStatus)
                showErrorDialog(errorMessage)
            }
        }
    }

    private val onOrderClick: (String) -> Unit = { id ->
        findNavController().navigate(
            R.id.action_listOrders_to_detailOrder,
            bundleOf(PARTNER_ORDER_ID to id)
        )
    }

    private fun updateContentState(uiState: UiState) {
        Timber.d("updateContentState $uiState")
        when (uiState) {
            is UiState.Loading -> {
                showHideLoading(true)
                showHideContentView(false)
            }
            is UiState.Error -> {
                Timber.e(uiState.error)
                showHideLoading(false)
                showHideContentView(false)
                showErrorView(uiState.error.localizedMessage)
            }

            is UiState.Empty -> {
                showHideLoading(false)
                showHideContentView(false)
                showEmptyView()
            }

            is UiState.Success -> {
                showHideContentView(true)
                showHideLoading(false)
            }
        }
    }

    private fun updatePaginationState(paginationState: PaginationState) {
        Timber.d("updatePaginationState $paginationState")
        val swipeRefreshLayout = synthetic.binding.swipeViewOrdersLayout

        when (paginationState) {
            is PaginationState.Loading -> footerAdapter.loadState = LoadState.Loading
            is PaginationState.Error -> {
                swipeRefreshLayout.isRefreshing = false
                footerAdapter.loadState = LoadState.Error(paginationState.error)
            }

            is PaginationState.Success -> {
                swipeRefreshLayout.isRefreshing = false
                footerAdapter.loadState = LoadState.NotLoading(false)
            }
        }
    }

    private fun showHideLoading(show: Boolean) {
        val shimmer = synthetic.binding.includeViewOrdersLoadingView.shimmerViewOrders
        val emptyView = synthetic.binding.includeViewOrdersEmptyView.emptyView
        if (show) {
            emptyView.isVisible = false
            shimmer.showShimmer(true)
        } else {
            shimmer.hideShimmer()
        }
        shimmer.isVisible = show
    }

    private fun showErrorView(error: String?) {
        val errorView = synthetic.binding.includeViewOrdersErrorView.errorView
        if (!error.isNullOrEmpty()) {
            synthetic.binding.includeViewOrdersErrorView.textViewErrorMessage.text = error
        }
        errorView.isVisible = true
        synthetic.binding.includeViewOrdersErrorView.buttonRefresh.setOnClickListener {
            errorView.isVisible = false
            viewModel.onRefreshData()
        }
    }

    private fun showEmptyView() {
        val emptyView = synthetic.binding.includeViewOrdersEmptyView.emptyView
        emptyView.isVisible = true
        synthetic.binding.rvViewPartnerOrders.isVisible = false
        synthetic.binding.floatButtonCreatePartnerOrder.isVisible = true
    }

    private fun showHideContentView(show: Boolean) {
        synthetic.binding.rvViewPartnerOrders.isVisible = show
        synthetic.binding.floatButtonCreatePartnerOrder.isVisible = show
    }

    private fun submitOrders(orders: List<PartnerOrderUiModel>) {
        orderAdapter.submitData(viewLifecycleOwner.lifecycle, PagingData.from(orders))
    }

    companion object{
        const val PARTNER_ORDER_ID = "partner_order_id"
    }
}