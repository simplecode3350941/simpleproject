package com.project2.partner.orders.impl.ui.create.price_list

import android.os.Bundle
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.project2.core.presentation.adapter_delegate.DelegateAdapterItem
import com.project2.core.presentation.adapter_delegate.MultiTypeAdapter
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.utils.common.lazyUnsafe
import com.project2.partner.orders.impl.R
import com.project2.partner.orders.impl.databinding.FragmentPartnerCreateOrderPriceListBinding
import com.project2.partner.orders.impl.ui.create.create_order.PartnerCreateOrderBottomSheet
import com.project2.partner.orders.impl.ui.create.price_list.adapter.PriceListCategoryViewHolderDelegate
import com.project2.partner.orders.impl.ui.create.price_list.adapter.PriceListProductViewHolderDelegate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

class PartnerCreateOrderPriceListFragment : BaseMvvmFragment() {

    override val synthetic = SyntheticBinding(FragmentPartnerCreateOrderPriceListBinding::inflate)

    private val viewModel: PartnerCreateOrderPriceListViewModel by viewModels {
        viewModelFactory
    }

    private val pointId: String
        get() {
            return requireArguments()
                .getString(PARTNER_POINT_ID_KEY)
                .toString()
        }

    private val pointName: String
        get() {
            return requireArguments()
                .getString(PARTNER_POINT_NAME_KEY)
                .toString()
        }

    private val pointAddress: String
        get() {
            return requireArguments()
                .getString(PARTNER_POINT_ADDRESS_KEY)
                .toString()
        }

    private val priceListAdapter: MultiTypeAdapter by lazyUnsafe {
        MultiTypeAdapter.Builder()
            .add(PriceListCategoryViewHolderDelegate(::onCategoryItemClick))
            .add(PriceListProductViewHolderDelegate(::onMinusClick, ::onPlusClick, ::onPriceClick))
            .build()
    }

    override fun getLayoutResId(): Int = R.layout.fragment_partner_create_order_price_list

    override fun initUi(savedInstanceState: Bundle?) {
        synthetic.binding.includeViewPartnerPriceListAppbar.textViewNavbarTitle.text =
            getString(R.string.partner_order_price_list_title)

        synthetic.binding.includeViewPartnerPriceListAppbar.textViewRight.text =
            getString(R.string.partners_app_bar_action_title)

        synthetic.binding.includeViewPartnerPriceListAppbar.textViewRight.isVisible = true

        synthetic.binding.includeViewPartnerPriceListAppbar.imgViewArrowBack.setOnClickListener {
            popBack()
        }

        synthetic.binding.includeViewPartnerPriceListAppbar.textViewRight.setOnClickListener {
            viewModel.onResetUserSelectedData()
        }

        synthetic.binding.recyclerViewPartnerPriceList.apply {
            adapter = priceListAdapter
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
            setHasFixedSize(true)
        }

        synthetic.binding.buttonGoToOrder.setOnClickListener {
            val selectedServices = viewModel.onLoadServicesList()
            if (selectedServices.size > 0) {
                PartnerCreateOrderBottomSheet.show(
                    childFragmentManager,
                    pointId = pointId,
                    pointName = pointName,
                    pointAddress = pointAddress,
                    selectedServices = selectedServices
                )
            } else {
                Toast.makeText(
                    requireContext(),
                    getString(R.string.partner_order_price_list_noselected_services),
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.uiState.collectLatest(::updateContentState)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.priceListFlow.collectLatest(::submitPriceList)
        }

        synthetic.binding.editTextSearch.doAfterTextChanged { text ->
            viewModel.onSearchData(text.toString())
        }

        viewModel.onLoadPriceByPointId(pointId)

        childFragmentManager.setFragmentResultListener(
            PartnerCreateOrderSetPriceBottomSheet.REQUEST_KEY,
            viewLifecycleOwner
        ) { _, bundle ->
            val price = bundle.getDouble(PartnerCreateOrderSetPriceBottomSheet.ARGS_PRICE)
                .takeIf { it > 0.0 }
            val productId = bundle.getString(PartnerCreateOrderSetPriceBottomSheet.ARGS_PRODUCT_ID)
            if (productId != null && price != null) {
                viewModel.setItemPrice(productId, price)
            }
        }
    }

    private fun onPriceClick(item: ProductItem) {
        val currentPrice = item.item.product.price.toString()
        PartnerCreateOrderSetPriceBottomSheet.show(
            childFragmentManager,
            currentPrice,
            item.item.product.name,
            item.id()
        )
    }

    private fun updateContentState(uiState: UiState) {
        Timber.d("updateContentState $uiState")
        when (uiState) {
            is UiState.Loading -> showHideLoading(true)
            is UiState.Error -> {
                Timber.e(uiState.error)
                showHideLoading(false)
                showErrorView(uiState.error.localizedMessage)
            }
            is UiState.Success -> {
                showHideLoading(false)
            }
        }
    }

    private fun showHideLoading(show: Boolean) {
        val recyclerView = synthetic.binding.recyclerViewPartnerPriceList
        val progressBar = synthetic.binding.progressBarPartnerPriceList
        recyclerView.isVisible = !show
        progressBar.isVisible = show
    }

    private fun showErrorView(error: String?) {
        val errorView = synthetic.binding.includeErrorView.errorView
        if (!error.isNullOrEmpty()) {
            synthetic.binding.includeErrorView.textViewErrorMessage.text = error
        }
        errorView.isVisible = true
        synthetic.binding.includeErrorView.buttonRefresh.setOnClickListener {
            errorView.isVisible = false
            viewModel.onLoadPriceByPointId(pointId)
        }
    }

    private fun onMinusClick(item: ProductItem) {
        viewModel.onMinusClick(item)
    }

    private fun onPlusClick(item: ProductItem) {
        viewModel.onPlusClick(item)
    }
    private fun submitPriceList(pointTypes: List<DelegateAdapterItem>) {
        priceListAdapter.submitList(pointTypes)
    }

    private fun onCategoryItemClick(item: CategoryItem) {
        viewModel.onCategoryClicked(item)
    }

    companion object {
        const val PARTNER_POINT_ID_KEY = "partner_point_id_key"
        const val PARTNER_POINT_NAME_KEY = "partner_point_name_key"
        const val PARTNER_POINT_ADDRESS_KEY = "partner_point_address_key"
    }
}