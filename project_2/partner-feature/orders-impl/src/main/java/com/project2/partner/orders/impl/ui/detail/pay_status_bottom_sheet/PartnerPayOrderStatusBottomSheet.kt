package com.project2.partner.orders.impl.ui.detail.pay_status_bottom_sheet

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.dialog.BaseBottomSheetDialogFragment
import com.project2.partner.orders.impl.R
import com.project2.partner.orders.impl.databinding.PartnerPayOrderBottomSheetBinding

class PartnerPayOrderStatusBottomSheet: BaseBottomSheetDialogFragment() {

    override val synthetic = SyntheticBinding(PartnerPayOrderBottomSheetBinding::inflate)

    private val isPaySuccess: Boolean
        get() = requireNotNull(arguments?.getBoolean(PARTNER_PAY_STATUS))

    private val payOrderNumber: String
        get() = requireNotNull(arguments?.getString(PARTNER_PAY_ORDER_NUMBER))

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        dialog.setOnShowListener {
            (dialog.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout?)?.let {
                BottomSheetBehavior.from(it).state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        synthetic.binding.textViewOrder.text =
            getString(R.string.partners_pay_order_number, payOrderNumber)

        if (isPaySuccess) {

            synthetic.binding.imgViewPayStatus.setImageDrawable(
                getDrawable(requireContext(), R.drawable.ic_success_round_48_dp)
            )
            synthetic.binding.textViewStatusMessage.text =
                getString(R.string.partners_pay_order_success)
        } else {
            synthetic.binding.imgViewPayStatus.setImageDrawable(
                getDrawable(requireContext(), R.drawable.ic_error_round_48_dp)
            )
            synthetic.binding.textViewStatusMessage.text =
                getString(R.string.partners_pay_order_error)
        }

        synthetic.binding.buttonClose.setOnClickListener {
            dismiss()
        }
    }

    companion object {
        const val TAG = "PAY_ORDER_BOTTOM_SHEET"
        const val PARTNER_PAY_STATUS = "PARTNER_PAY_STATUS"
        const val PARTNER_PAY_ORDER_NUMBER = "PARTNER_PAY_ORDER_NUMBER"
        const val PARTNER_PAY_STATUS_ERROR_MESSAGE = "PARTNER_PAY_STATUS_ERROR_MESSAGE"

        fun show(
            manager: FragmentManager,
            isPaySuccess: Boolean,
            payOrderNumber: String
        ) {
            PartnerPayOrderStatusBottomSheet().apply {
                arguments = bundleOf(
                    PARTNER_PAY_STATUS to isPaySuccess,
                    PARTNER_PAY_ORDER_NUMBER to payOrderNumber
                )
            }.show(manager, TAG)
        }
    }
}