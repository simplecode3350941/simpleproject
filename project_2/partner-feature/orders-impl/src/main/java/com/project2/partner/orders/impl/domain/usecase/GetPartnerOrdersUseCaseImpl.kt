package com.project2.partner.orders.impl.domain.usecase

import com.project2.partner.orders.api.domain.models.PartnerOrderDomainModel
import com.project2.partner.orders.api.domain.repository.PartnerOrdersRepository
import com.project2.partner.orders.api.domain.usecase.GetPartnerOrdersUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPartnerOrdersUseCaseImpl @Inject constructor(
    private val repository: PartnerOrdersRepository
) : GetPartnerOrdersUseCase {

    override suspend operator fun invoke(
        pageNumber: Int,
        pageSize: Int
    ): Flow<List<PartnerOrderDomainModel>> {
        return repository.getOrders(pageNumber, pageSize)
    }
}