package com.project2.partner.orders.impl.ui.create.create_order

import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.dialog.BaseMvvmBottomSheetDialogFragment
import com.project2.core.utils.common.lazyUnsafe
import com.project2.core.utils.formatter.FormatUtils
import com.project2.partner.orders.impl.R
import com.project2.partner.orders.impl.databinding.PartnerOrderCreateBottomSheetBinding
import com.project2.partner.orders.impl.ui.create.add_comment.PartnerCreateOrderAddCommentBottomSheet
import com.project2.partner.orders.impl.ui.create.create_order.adapter.PartnerCreateOrderServicesAdapter
import com.project2.partner.orders.impl.ui.models.CreateOrderServicesUi
import com.project2.partner.orders.impl.ui.models.CreatedOrderUiModel
import com.project2.partner.orders.impl.ui.models.ServicesSelectedUiModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

class PartnerCreateOrderBottomSheet : BaseMvvmBottomSheetDialogFragment() {

    override val synthetic = SyntheticBinding(PartnerOrderCreateBottomSheetBinding::inflate)

    private val viewModel by viewModels<PartnerCreateOrderViewModel> {
        viewModelFactory
    }

    private val serviceServicesAdapter: PartnerCreateOrderServicesAdapter by lazyUnsafe {
        PartnerCreateOrderServicesAdapter()
    }

    private val pointId: String
        get() = requireNotNull(arguments?.getString(PARTNER_POINT_ID))

    private val pointName: String
        get() = requireNotNull(arguments?.getString(PARTNER_POINT_NAME))

    private val pointAddress: String
        get() = requireNotNull(arguments?.getString(PARTNER_POINT_ADDRESS))

    private val selectedServices: List<ServicesSelectedUiModel>
        get() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            requireNotNull(arguments?.getParcelableArrayList(
                PARTNER_SELECTED_SERVICES,
                ServicesSelectedUiModel::class.java
            ) as List<ServicesSelectedUiModel>)
        } else {
            requireNotNull(
                arguments?.getSerializable(PARTNER_SELECTED_SERVICES)
                        as List<ServicesSelectedUiModel>
            )
        }

    private var comment: String? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        dialog.setOnShowListener {
            (dialog.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout?)?.let {
                BottomSheetBehavior.from(it).state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        synthetic.binding.layoutProgressBar.isVisible = false

        synthetic.binding.rvServices.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = serviceServicesAdapter
        }

        synthetic.binding.buttonCreateOrder.setOnClickListener {
            viewModel.onSendCreateOrderRequest(selectedServices, comment, pointId)
        }

        synthetic.binding.textViewAddComment.setOnClickListener {
            PartnerCreateOrderAddCommentBottomSheet.show(childFragmentManager, comment)
        }

        childFragmentManager.setFragmentResultListener(
            PARTNER_ADD_COMMENT_KEY,
            viewLifecycleOwner
        ) { _, bundle ->
            comment = bundle.getString(PARTNER_COMMENT_KEY)?.trim()
            if (!comment.isNullOrEmpty()) {
                synthetic.binding.textViewAddComment.isVisible = false
                synthetic.binding.layoutComment.isVisible = true
                synthetic.binding.textViewCommentValue.text = comment
            }
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.updateState.collectLatest(::bindUiStates)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.createdOrder.collectLatest(::onOrderCreated)
        }

        synthetic.binding.layoutComment.setOnClickListener {
            PartnerCreateOrderAddCommentBottomSheet.show(childFragmentManager, comment)
        }

        val content = CreateOrderServicesUi(
            id = pointId,
            name = pointName,
            address = pointAddress,
            services = selectedServices
        )
        showContent(content)
    }

    private fun onOrderCreated(createdOrder: CreatedOrderUiModel) {
        showToast(getString(R.string.partners_create_order_success))
        findNavController().navigate(R.id.action_priceList_to_start)
    }

    private fun bindUiStates(uiState: UiState) {
        Timber.d("updateContentState $uiState")
        when(uiState) {
            is UiState.Loading ->
                showHideLoading(true)
            is UiState.Error -> {
                Timber.e(uiState.error)
                showHideLoading(false)
                uiState.error.localizedMessage?.let { showToast(it) }
            }

            is UiState.Success -> {
                showHideLoading(false)
            }
            is UiState.Empty -> {
                showHideLoading(false)
                showToast(getString(R.string.partners_create_order_error))
            }
        }
    }

    private fun showToast(messages: String) {
        Toast.makeText(requireContext(), messages, Toast.LENGTH_LONG).show()
    }

    private fun showHideLoading(show: Boolean) {
        synthetic.binding.layoutProgressBar.isVisible = show
    }

    private fun showContent(content: CreateOrderServicesUi) {
        synthetic.binding.textViewServiceNameValue.text = content.name
        synthetic.binding.textViewAddressValue.text = content.address

        var total = 0.0
        content.services.forEach {
            total += it.quantity * it.price
        }
        synthetic.binding.textViewTotalValue.text =
            FormatUtils.formattedFractionalPrice(price = total, sizeOfFractional = 14)

        serviceServicesAdapter.submitList(content.services)
    }

    companion object {
        const val TAG = "PARTNER_CREATE_ORDER_BOTTOM_SHEET"
        const val PARTNER_POINT_ID = "partner_point_id"
        const val PARTNER_POINT_NAME = "partner_point_name"
        const val PARTNER_POINT_ADDRESS = "partner_point_address"
        const val PARTNER_SELECTED_SERVICES = "partner_selected_services"
        const val PARTNER_ADD_COMMENT_KEY = "partner_add_comment_key"
        const val PARTNER_COMMENT_KEY = "comment"

        fun show(
            manager: FragmentManager,
            pointId: String,
            pointName: String,
            pointAddress: String,
            selectedServices: List<ServicesSelectedUiModel>
        ) {
            PartnerCreateOrderBottomSheet().apply {
                arguments = bundleOf(
                    PARTNER_POINT_ID to pointId,
                    PARTNER_POINT_NAME to pointName,
                    PARTNER_POINT_ADDRESS to pointAddress,
                    PARTNER_SELECTED_SERVICES to selectedServices
                )
            }.show(manager, TAG)
        }
    }
}