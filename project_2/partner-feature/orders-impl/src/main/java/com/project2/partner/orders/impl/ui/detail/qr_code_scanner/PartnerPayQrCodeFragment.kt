package com.project2.partner.orders.impl.ui.detail.qr_code_scanner

import android.Manifest
import android.content.Context
import android.media.Image
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.CameraSelector
import androidx.camera.core.ExperimentalGetImage
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.barcode.common.Barcode
import com.google.mlkit.vision.common.InputImage
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.ComponentDependenciesProvider
import com.project2.core.presentation.di.HasComponentDependencies
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.partner.orders.impl.R
import com.project2.partner.orders.impl.databinding.FragmentPartnerOrderQrCodeScanningBinding
import com.project2.partner.orders.impl.ui.detail.communication.PartnerDetailOrderCommunication
import com.project2.partner.orders.impl.ui.detail.communication.PartnerDetailOrderCommunication.setQrCodeValue
import com.project2.partner.orders.impl.ui.di.DaggerPartnerOrdersComponent
import com.project2.partner.orders.impl.ui.models.QrCodeRawValueUiModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.concurrent.Executors
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

@ExperimentalGetImage
class PartnerPayQrCodeFragment : BaseMvvmFragment(), HasComponentDependencies {

    @Inject
    override lateinit var dependencies: ComponentDependenciesProvider

    override val synthetic = SyntheticBinding(FragmentPartnerOrderQrCodeScanningBinding::inflate)

    private val detector = BarcodeScanning.getClient(
        BarcodeScannerOptions.Builder()
            .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
            .build()
    )

    private var cameraExecutor = Executors.newSingleThreadExecutor()

    override fun inject() {
        DaggerPartnerOrdersComponent.builder()
            .partnerOrdersDependencies(findComponentDependencies())
            .build()
            .inject(this)
    }

    override fun getLayoutResId(): Int = R.layout.fragment_partner_order_qr_code_scanning

    override fun initUi(savedInstanceState: Bundle?) {
        hideSystemUI()

        synthetic.binding.imgViewClose.setOnClickListener {
            findNavController().popBackStack()
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            PartnerDetailOrderCommunication.subscribe(::checkQrCodeValue)
        }

        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            when {
                granted -> {
                    startCamera()
                }

                else -> {
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.partners_pay_order_camera_permission_denied),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }.launch(Manifest.permission.CAMERA)
    }

    override fun onDestroyView() {
        showSystemUI()
        super.onDestroyView()
        cameraExecutor.shutdown()
    }

    private fun checkQrCodeValue(qrCodeRawValueUiModel: QrCodeRawValueUiModel) {
        if (qrCodeRawValueUiModel.isQrCodeValueReceived) {
            findNavController().popBackStack()
        }
    }

    private fun hideSystemUI() {
        WindowCompat.setDecorFitsSystemWindows(requireActivity().window, false)
        WindowInsetsControllerCompat(
            requireActivity().window,
            synthetic.binding.root
        ).let { controller ->
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior =
                WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
    }

    private fun showSystemUI() {
        WindowCompat.setDecorFitsSystemWindows(requireActivity().window, true)
        WindowInsetsControllerCompat(requireActivity().window, synthetic.binding.root).show(
            WindowInsetsCompat.Type.systemBars()
        )
    }

    private fun startCamera() {
        val preview = Preview.Builder()
            .build()
            .also {
                it.setSurfaceProvider(synthetic.binding.previewView.surfaceProvider)
            }

        val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            val cameraProviderFuture = requireContext().getCameraProvider()

            val imageAnalysis = ImageAnalysis.Analyzer { imageProxy ->
                val image = imageProxy.image
                if (image != null) {
                    val inputImage =
                        InputImage.fromMediaImage(image, imageProxy.imageInfo.rotationDegrees)
                    detectingQrCode(inputImage, imageProxy, image)
                }
            }

            val imageAnalyzer = ImageAnalysis.Builder()
                .setTargetRotation(synthetic.binding.previewView.display.rotation)
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build()
                .also {
                    it.setAnalyzer(cameraExecutor, imageAnalysis)
                }

            try {
                cameraProviderFuture.unbindAll()
                cameraProviderFuture.bindToLifecycle(
                    viewLifecycleOwner, cameraSelector, preview, imageAnalyzer
                )

            } catch (ex: Exception) {
                Timber.d("${ex.printStackTrace()}")
            }
        }
    }

    private suspend fun Context.getCameraProvider(): ProcessCameraProvider =
        suspendCoroutine { continuation ->
            ProcessCameraProvider.getInstance(this).also { future ->
                future.addListener({
                    continuation.resume(future.get())
                }, ContextCompat.getMainExecutor(this))
            }
        }

    private fun detectingQrCode(
        inputImage: InputImage,
        imageProxy: ImageProxy,
        image: Image
    ) {
        detector.process(inputImage)
            .addOnSuccessListener { barcodes ->
                barcodes.firstOrNull()?.let {
                    Timber.d("Qr-code rawValue = ${it.rawValue}")
                    it.rawValue?.let {
                        lifecycleScope.launch(Dispatchers.Main) {
                            setQrCodeValue(it)
                        }
                    }
                }
            }
            .addOnCompleteListener {
                imageProxy.close()
                image.close()
            }
            .addOnFailureListener {
                Timber.d("Detector qr code error: ${it.localizedMessage}")
            }
    }
}