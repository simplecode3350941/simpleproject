package com.project2.partner.orders.impl.ui.di

import com.project2.core.presentation.di.ComponentDependencies
import com.project2.partner.orders.api.data.remote.api.PartnerOrdersApi
import com.project2.partner.orders.api.domain.repository.PartnerOrdersRepository
import com.project2.partner.orders.api.domain.usecase.CreateOrderUseCase
import com.project2.partner.orders.api.domain.usecase.GetPartnerOrderDetailUseCase
import com.project2.partner.orders.api.domain.usecase.GetPartnerOrdersUseCase
import com.project2.partner.orders.api.domain.usecase.PayOrderUseCase
import com.project2.partner.orders.impl.ui.detail.PartnerOrderDetailViewModel
import com.project2.partner.points.api.domain.usecase.GetPartnerDetailPointUseCase
import com.project2.partner.profile.api.domain.usecase.GetPartnerProfileUseCase

interface PartnerOrdersDependencies : ComponentDependencies {

    fun ordersApi(): PartnerOrdersApi

    fun ordersRepository(): PartnerOrdersRepository

    fun getOrdersUseCase(): GetPartnerOrdersUseCase

    fun getOrderDetailUseCase(): GetPartnerOrderDetailUseCase

    fun createOrderUseCase(): CreateOrderUseCase

    fun payOrderUseCase(): PayOrderUseCase

    fun profileUseCase(): GetPartnerProfileUseCase

    fun detailPointUseCase(): GetPartnerDetailPointUseCase

    fun getDetailOrderViewModelFactory(): PartnerOrderDetailViewModel.Factory
}