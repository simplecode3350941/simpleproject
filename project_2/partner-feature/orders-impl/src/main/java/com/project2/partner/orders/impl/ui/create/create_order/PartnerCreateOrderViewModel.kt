package com.project2.partner.orders.impl.ui.create.create_order

import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.partner.orders.api.domain.usecase.CreateOrderUseCase
import com.project2.partner.orders.impl.ui.mapper.getCreateOrderRequestUi
import com.project2.partner.orders.impl.ui.mapper.toDomain
import com.project2.partner.orders.impl.ui.mapper.toPresentation
import com.project2.partner.orders.impl.ui.models.CreatedOrderUiModel
import com.project2.partner.orders.impl.ui.models.ServicesSelectedUiModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import timber.log.Timber
import javax.inject.Inject

class PartnerCreateOrderViewModel @Inject constructor(
    private val createOrder: CreateOrderUseCase,
    override val exceptionHandler: CoroutineExceptionHandler
) : BaseViewModel() {
    private val _updateState = MutableStateFlow<UiState?>(null)
    val updateState = _updateState.asStateFlow().filterNotNull()

    private val _createdOrder = MutableStateFlow<CreatedOrderUiModel?>(null)
    val createdOrder = _createdOrder.asStateFlow().filterNotNull()

    fun onSendCreateOrderRequest(
        services: List<ServicesSelectedUiModel>,
        comment: String?,
        pointId: String
    ) {
        val bodyRequestModel = getCreateOrderRequestUi(services, comment, pointId)

        launchOnViewModelScope {
            createOrder(bodyRequestModel.toDomain())
                .onStart {
                    _updateState.value = UiState.Loading
                }
                .map { it.toPresentation() }
                .catch {
                    _updateState.value = UiState.Error(it)
                }
                .collect {
                    Timber.d("collect - $it")
                    _createdOrder.value = it
                    _updateState.value = UiState.Success
                }
        }
    }

}

sealed interface UiState {
    object Loading : UiState
    object Success : UiState
    object Empty : UiState
    data class Error(val error: Throwable) : UiState
}