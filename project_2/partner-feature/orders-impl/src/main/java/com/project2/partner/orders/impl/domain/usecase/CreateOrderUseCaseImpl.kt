package com.project2.partner.orders.impl.domain.usecase

import com.project2.partner.orders.api.domain.models.CreateOrderRequestDomainModel
import com.project2.partner.orders.api.domain.models.CreatedOrderDomainModel
import com.project2.partner.orders.api.domain.repository.PartnerOrdersRepository
import com.project2.partner.orders.api.domain.usecase.CreateOrderUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CreateOrderUseCaseImpl @Inject constructor(
    private val repository: PartnerOrdersRepository
): CreateOrderUseCase {
    override suspend operator fun invoke(
        createOrderRequest: CreateOrderRequestDomainModel
    ): Flow<CreatedOrderDomainModel> {
        return repository.createOrder(createOrderRequest)
    }
}