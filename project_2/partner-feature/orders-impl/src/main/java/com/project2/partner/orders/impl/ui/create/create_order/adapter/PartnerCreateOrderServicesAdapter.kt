package com.project2.partner.orders.impl.ui.create.create_order.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.partner.orders.impl.databinding.PartnerCreateOrderServicesItemBinding
import com.project2.partner.orders.impl.ui.models.ServicesSelectedUiModel
import com.project2.partner.orders.impl.ui.models.priceWithQuantity

class PartnerCreateOrderServicesAdapter :
    ListAdapter<ServicesSelectedUiModel, PartnerCreateOrderServicesAdapter.ViewHolder>(
        DIFF_CALLBACK
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    inner class ViewHolder(parent: ViewGroup) :
        BindingViewHolder<PartnerCreateOrderServicesItemBinding>(
            parent,
            PartnerCreateOrderServicesItemBinding::inflate
        ) {

        fun bind(item: ServicesSelectedUiModel) = with(binding) {
            textServiceTitle.text = item.name
            textServicePrice.text = item.priceWithQuantity
        }
    }

    companion object {

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ServicesSelectedUiModel>() {
            override fun areItemsTheSame(
                oldItem: ServicesSelectedUiModel,
                newItem: ServicesSelectedUiModel
            ): Boolean {
                return oldItem.name == newItem.name
            }

            override fun areContentsTheSame(
                oldItem: ServicesSelectedUiModel,
                newItem: ServicesSelectedUiModel
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}