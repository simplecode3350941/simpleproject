package com.project2.partner.orders.impl.data.mapper

import com.project2.partner.orders.api.data.remote.models.CreateOrderRequest
import com.project2.partner.orders.api.data.remote.models.PointTypeResponse
import com.project2.partner.orders.api.data.remote.models.PointTypesDataModel
import com.project2.partner.orders.api.data.remote.models.ServiceTypesDataModel
import com.project2.partner.orders.api.data.remote.models.ServicesResponse
import com.project2.partner.orders.api.domain.models.CreateOrderRequestDomainModel
import com.project2.partner.orders.api.domain.models.PointTypeDomainModel
import com.project2.partner.orders.api.domain.models.PointTypesDomainModel
import com.project2.partner.orders.api.domain.models.ServiceTypesDomainModel
import com.project2.partner.orders.api.domain.models.ServicesDomainModel

fun CreateOrderRequestDomainModel.toData() = CreateOrderRequest(
    pointID = pointID,
    comment = comment,
    pointTypes = pointTypes.map { it.toData() }
)

fun PointTypesDomainModel.toData() = PointTypesDataModel(
    staticID = staticID,
    type = type.toData(),
    serviceTypes = serviceTypes.map { it.toData() }
)

fun PointTypeDomainModel.toData() = PointTypeResponse(
    localizedName = localizedName,
    type = type
)

fun ServiceTypesDomainModel.toData() = ServiceTypesDataModel(
    staticID = staticID,
    name = name,
    services = services.map { it.toData() }
)

fun ServicesDomainModel.toData() = ServicesResponse(
    name = name,
    price = price,
    quantity = quantity,
    serviceID = serviceID
)