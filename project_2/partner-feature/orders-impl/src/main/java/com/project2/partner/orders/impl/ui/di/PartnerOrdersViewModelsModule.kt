package com.project2.partner.orders.impl.ui.di

import androidx.lifecycle.ViewModel
import com.project2.core.commons.di.viewmodule.ViewModelKey
import com.project2.partner.orders.impl.ui.create.create_order.PartnerCreateOrderViewModel
import com.project2.partner.orders.impl.ui.create.price_list.PartnerCreateOrderPriceListViewModel
import com.project2.partner.orders.impl.ui.list.PartnerOrdersViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PartnerOrdersViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(PartnerOrdersViewModel::class)
    abstract fun partnerOrdersViewModel(viewModel: PartnerOrdersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PartnerCreateOrderViewModel::class)
    abstract fun partnerCreateOrderViewModel(viewModel: PartnerCreateOrderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PartnerCreateOrderPriceListViewModel::class)
    abstract fun partnerCreateOrderPriceListViewModel(
        viewModel: PartnerCreateOrderPriceListViewModel
    ): ViewModel
}