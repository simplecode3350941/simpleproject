package com.project2.partner.orders.impl.ui.models

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Parcelable
import android.text.Spannable
import android.text.SpannableStringBuilder
import androidx.appcompat.content.res.AppCompatResources
import com.project2.core.utils.date.DateUtils
import com.project2.core.utils.formatter.FormatUtils
import com.project2.partner.orders.api.enums.OrderStatus
import com.project2.partner.orders.impl.R
import com.project2.partner.points.api.domain.models.PointType
import kotlinx.parcelize.Parcelize
import java.util.*

data class PartnerOrderUiModel(
    val id: String,
    val date: String,
    val number: String,
    val status: OrderStatus,
    val amount: Double,
    val pointName: String
)

data class PartnerOrderDetailUiModel(
    val id: String,
    val date: String,
    val comment: String?,
    val number: String,
    val status: OrderStatus,
    val amount: Double,
    val pan: String?,
    val point: PointUiModel,
    val services: List<ServicesUiModel>
)

data class PointUiModel(
    val id: String,
    val name: String,
    val types: List<String>,
    val address: String
)
data class PointTypesUi(
    val staticID: String,
    val type: PointTypeUi,
    var serviceTypes: List<ServiceTypesUi>
)

data class ServiceTypesUi(
    val staticID: String,
    val name: String,
    var services: List<ServicesUiModel>
)

data class ServicesUiModel(
    val name: String,
    val price: Double,
    val quantity: Long,
    val serviceID: String
)

data class CreateOrderRequestUi(
    val pointID: String,
    val comment: String,
    val pointTypes: List<PointTypesUi>
)

@Parcelize
data class ServicesSelectedUiModel(
    val categoryTypeId: String,
    val parentCategoryId: String,
    val categoryType: PointTypeUi,
    val categoryName: String,
    val productId: String,
    val name: String,
    var price: Double,
    var quantity: Long
): Parcelable

data class CategoryWithProductsUi(
    val id: String,
    val categoryName: String,
    val products: List<ProductUi>,
    val badgeValue: Int,
    val categoryTypeId: String,
    val categoryType: PointTypeUi?
)

data class CreateOrderServicesUi(
    val id: String,
    val name: String,
    val address: String,
    val services: List<ServicesSelectedUiModel>
)

data class PointTypeWithCategoryUi(
    val id: String,
    val type: PointTypeUi,
    val categories: List<CategoryWithProductsUi>
)

@Parcelize
data class PointTypeUi(
    val localizedName : String,
    val type: PointType // Enum
): Parcelable

data class ProductUi(
    val id: String,
    val name: String,
    val price: Double?,
    val quantity: Long
)

data class CreatedOrderUiModel(
    val id: String,
    val date: String,
    val point: PointUiModel,
    val comment: String,
    val number: String,
    val status: OrderStatus,
    val amount: Double,
    val pan: String?,
    val services: List<ServicesUiModel>
)

data class QrCodeRawValueUiModel(
    val qrCodeValue: String = "",
    val isQrCodeValueReceived: Boolean = false
)

val ProductUi.formattedPrice: Spannable?
    get() {
        return price?.let {
            FormatUtils.formattedFractionalPrice(price = it, sizeOfFractional = 16)
        }
    }

val ServicesSelectedUiModel.priceWithQuantity: Spannable
    get() {
        return getFormattedAmount(amount = price, quantity = quantity, sizeOfFractional = 14)
    }

val ServicesUiModel.priceWithQuantity: Spannable
    get() {
        return getFormattedAmount(amount = price, quantity = quantity, sizeOfFractional = 14)
    }

val PartnerOrderUiModel.formattedAmount: Spannable
    get() {
        return getFormattedAmount(amount = amount, sizeOfFractional = 14)
    }

val PartnerOrderDetailUiModel.formattedAmount: Spannable
    get() {
        return getFormattedAmount(amount = amount, sizeOfFractional = 23)
    }

fun PartnerOrderUiModel.friendlyStatus(context: Context) : String {
        return getFriendlyStatus(context, status)
    }

fun PartnerOrderDetailUiModel.friendlyStatus(context: Context) : String {
    return getFriendlyStatus(context, status)
}

fun PartnerOrderUiModel.statusBackground(context: Context) : Drawable? {
    return getStatusBackground(context, status)
}

fun PartnerOrderDetailUiModel.statusBackground(context: Context) : Drawable? {
    return getStatusBackground(context, status)
}

fun PartnerOrderUiModel.statusTextColor(context: Context) : Int {
    return getStatusTextColor(context, status)
}

fun PartnerOrderDetailUiModel.statusTextColor(context: Context) : Int {
    return getStatusTextColor(context, status)
}

fun PartnerOrderUiModel.getFriendlyDate(context: Context) : String {
    return getFriendlyTransactionDate(date, context)
}

fun PartnerOrderDetailUiModel.getFriendlyDate(context: Context) : String {
    return getFriendlyTransactionDate(date, context)
}

val PartnerOrderDetailUiModel.allPointTypes: String
    get() {
        return getAllPointTypes(point.types)
    }

private fun getAllPointTypes(pointTypes: List<String>): String {
    var allPointTypesResult = pointTypes.first()
    for (i in 1 until pointTypes.size) {
        allPointTypesResult = "$allPointTypesResult, ${pointTypes[i]}"
    }
    return allPointTypesResult
}

private fun getFriendlyTransactionDate(fullDate: String, context: Context): String {
    val date = DateUtils.toDate(fullDate)
    return if (date == Date()) {
        val time = DateUtils.toFriendlyDate(date = date, pattern = "HH:mm")
        "${context.getString(R.string.partner_orders_today)}, $time"
    } else {
        DateUtils.toFriendlyDate(date = date, pattern = "d MMMM, HH:mm")
    }
}

private fun getFormattedAmount(
    amount: Double,
    quantity: Long = 0,
    sizeOfFractional: Int = 18
): Spannable {
    var quantityString = ""
    if (quantity > 1 ) {
        quantityString = "$quantity х "
    }
    return SpannableStringBuilder()
        .append(quantityString)
        .append(
            FormatUtils.formattedFractionalPrice(
                price = amount,
                sizeOfFractional = sizeOfFractional
            )
        )
}

fun getStatusTextColor(context: Context, status: OrderStatus) : Int {
    return when(status) {
        OrderStatus.PAID ->
            context.getColor(R.color.green_light2)
        OrderStatus.AWAITING_PAYMENT ->
            context.getColor(R.color.orange)
        OrderStatus.REFUND ->
            context.getColor(R.color.orange)
    }
}

fun getStatusBackground(context: Context, status: OrderStatus) : Drawable? {
    return when(status) {
        OrderStatus.PAID ->
            AppCompatResources.getDrawable(context, R.drawable.bg_order_success_status)
        OrderStatus.AWAITING_PAYMENT ->
            AppCompatResources.getDrawable(context, R.drawable.bg_order_waiting_status)
        OrderStatus.REFUND ->
            AppCompatResources.getDrawable(context, R.drawable.bg_order_refund_status)
    }
}

fun getFriendlyStatus(context: Context, status: OrderStatus) : String {
    return when(status) {
        OrderStatus.PAID -> context.getString(R.string.partner_orders_status_paid)
        OrderStatus.AWAITING_PAYMENT -> context.getString(R.string.partner_orders_status_await)
        OrderStatus.REFUND -> context.getString(R.string.partner_orders_status_refund)
    }
}