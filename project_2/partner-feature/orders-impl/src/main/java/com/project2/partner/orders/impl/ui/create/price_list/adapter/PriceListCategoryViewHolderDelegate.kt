package com.project2.partner.orders.impl.ui.create.price_list.adapter

import android.view.ViewGroup
import androidx.core.view.isVisible
import com.project2.core.presentation.adapter_delegate.ItemViewDelegate
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.partner.orders.impl.R
import com.project2.partner.orders.impl.databinding.PartnerCreateOrderPriceListCategoryItemBinding
import com.project2.partner.orders.impl.ui.create.price_list.CategoryItem

class PriceListCategoryViewHolderDelegate(
    private val onCategoryItemClick: (CategoryItem) -> Unit
) : ItemViewDelegate<CategoryItem, PriceListCategoryViewHolderDelegate.ViewHolder>(CategoryItem::class.java) {

    override fun onCreateViewHolder(parent: ViewGroup) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, item: CategoryItem) {
        holder.bind(item)
    }

    inner class ViewHolder(
        parent: ViewGroup
    ) : BindingViewHolder<PartnerCreateOrderPriceListCategoryItemBinding>(
        parent,
        PartnerCreateOrderPriceListCategoryItemBinding::inflate
    ) {

        fun bind(item: CategoryItem) = with(binding) {

            textViewCategory.text = item.item.categoryName

            val iconResId = when {
                item.item.isExpanded -> getDrawable(R.drawable.ic_arrow_top_16dp)
                else -> getDrawable(R.drawable.ic_arrow_down_16dp)
            }
            imgViewArrow.setImageDrawable(iconResId)
            divider.isVisible = !item.item.isExpanded

            textViewBadgeValue.isVisible = item.item.badgeValue > 0
            textViewBadgeValue.text = item.item.badgeValue.toString()

            itemView.setOnClickListener {
                onCategoryItemClick.invoke(item)
            }
        }
    }
}