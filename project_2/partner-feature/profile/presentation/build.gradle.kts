@file:Suppress("UnstableApiUsage")

@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.kapt)
}

android {
    namespace = "com.project2.partner.profile.impl.ui"
    compileSdk = libs.versions.compileSdk.get().toInt()

    defaultConfig {
        minSdk = libs.versions.minSdk.get().toInt()
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    packagingOptions {
        resources.excludes += "META-INF/notice.txt"
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_11.toString()
    }

    kapt {
        correctErrorTypes = true
        generateStubs = true
    }

    viewBinding {
        android.buildFeatures.viewBinding = true
    }
}

dependencies {
    implementation(project(":partner-feature:profile:core-impl"))
    implementation(project(":partner-feature:auth-api"))
    implementation(project(":core:presentation"))
    implementation(project(":core:network"))
    implementation(project(":core:resources"))
    implementation(project(":core:session"))

    implementation(libs.data.store)
    implementation(libs.kotlin.stdlib)
    implementation(libs.core.ktx)
    implementation(libs.app.compat)
    implementation(libs.constraintlayout)
    implementation(libs.coordinatorlayout)
    implementation(libs.recyclerview)
    implementation(libs.cardview)
    implementation(libs.material)
    implementation(libs.swiperefreshlayout)
    implementation(libs.dynamic.animation)
    implementation(libs.navigation.fragment.ktx)
    implementation(libs.navigation.ui.ktx)
    implementation(libs.preference.ktx)
    implementation(libs.activity.ktx)
    implementation(libs.fragment.ktx)
    implementation(libs.paging.runtime.ktx)

    implementation(platform(libs.okhttp.bom))
    implementation(libs.bundles.networking)
    implementation(libs.bundles.moshi)
    kapt(libs.moshi.kotlin.kapt)

    // Lifecycle components
    implementation(libs.lifecycle.runtime.ktx)
//    implementation(libs.bundles.androidx.lifecycle)
//    kapt(libs.lifecycle.compiler)

    api(libs.dagger.api)
    kapt(libs.dagger.compiler.kapt)

    implementation(libs.shimmer)
    implementation(libs.timber)

    testImplementation(libs.junit)
    androidTestImplementation(libs.bundles.android.testing)
}