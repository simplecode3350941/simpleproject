package com.project2.partner.profile.ui.di

import com.project2.core.presentation.di.ComponentDependenciesProviderModule
import com.project2.partner.profile.ui.PartnerProfileFragment
import com.project2.partner.profile.ui.help.PartnerHelpFragment
import dagger.Component

@Component(
    modules = [ComponentDependenciesProviderModule::class],
    dependencies = [PartnerProfileDependencies::class]
)
interface PartnerProfileComponent {
    fun inject(profileFragment: PartnerProfileFragment)

    fun inject(partnerHelpFragment: PartnerHelpFragment)
}
