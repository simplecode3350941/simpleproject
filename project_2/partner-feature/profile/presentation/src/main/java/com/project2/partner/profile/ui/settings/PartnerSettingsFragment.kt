package com.project2.partner.profile.ui.settings

import android.os.Bundle
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.partner.profile.impl.ui.R
import com.project2.partner.profile.impl.ui.databinding.FragmentPartnerSettingsBinding

class PartnerSettingsFragment : BaseMvvmFragment() {

    override fun getLayoutResId() = R.layout.fragment_partner_settings

    override val synthetic = SyntheticBinding(FragmentPartnerSettingsBinding::inflate)

    override fun initUi(savedInstanceState: Bundle?) {

    }
}