package com.project2.partner.profile.ui.help

import android.os.Bundle
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.presentation.paging.FooterAdapter
import com.project2.core.presentation.paging.addOnInfiniteScrollListener
import com.project2.core.utils.common.lazyUnsafe
import com.project2.partner.profile.impl.ui.R
import com.project2.partner.profile.impl.ui.databinding.FragmentParnterHelpFaqBinding
import com.project2.partner.profile.ui.models.FaqItemUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

class PartnerHelpFaqFragment : BaseMvvmFragment() {

    override val synthetic = SyntheticBinding(FragmentParnterHelpFaqBinding::inflate)

    private val viewModel by viewModels<PartnerFaqViewModel> {
        viewModelFactory
    }

    private val faqItemsAdapter: PartnerFaqAdapter by lazyUnsafe {
        PartnerFaqAdapter()
    }

    private val footerAdapter: FooterAdapter by lazyUnsafe {
        FooterAdapter(onEndReached)
    }

    private val onEndReached: () -> Unit = { viewModel.onPaginate() }

    override fun getLayoutResId(): Int = R.layout.fragment_parnter_help_faq

    override fun initUi(savedInstanceState: Bundle?) {
        synthetic.binding.helpToolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }

        synthetic.binding.helpRecycler.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = faqItemsAdapter.withLoadStateFooter(footerAdapter)
            setHasFixedSize(true)
            clearOnScrollListeners()
            addOnInfiniteScrollListener(onEndReached)
        }

        synthetic.binding.swipeRefreshLayout.apply {
            setOnRefreshListener {
                viewModel.onRefreshData()
                synthetic.binding.swipeRefreshLayout.isRefreshing = false
            }
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.faqItemsFlow.collectLatest(::submitFaqItems)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.updateState.collectLatest(::updateContentState)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.paginationState.collectLatest(::updatePaginationState)
        }

        synthetic.binding.helpSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.searchFaq(newText.toString())
                return true
            }
        })
    }

    private fun submitFaqItems(faqItems: List<FaqItemUi>) {
        Timber.d("show FAQ items size %s", faqItems.size)
        faqItemsAdapter.submitData(viewLifecycleOwner.lifecycle, PagingData.from(faqItems))
    }

    private fun updateContentState(uiState: UiState) {
        Timber.d("updateContentState: $uiState")

        when (uiState) {
            is UiState.Loading -> showHideLoading(true)
            is UiState.Error -> {
                Timber.e(uiState.error)
                showHideLoading(false)
                showErrorView(uiState.error.localizedMessage)
            }

            is UiState.Empty -> {
                showHideLoading(false)
            }

            is UiState.Success -> {
                showHideLoading(false)
            }
        }
    }

    private fun updatePaginationState(paginationState: PaginationState) {
        Timber.d("updatePaginationState: $paginationState")
        val swipeRefreshLayout = synthetic.binding.swipeRefreshLayout

        when (paginationState) {
            is PaginationState.Loading -> footerAdapter.loadState = LoadState.Loading
            is PaginationState.Error -> {
                swipeRefreshLayout.isRefreshing = false
                footerAdapter.loadState = LoadState.Error(paginationState.error)
            }

            is PaginationState.Success -> {
                swipeRefreshLayout.isRefreshing = false
                footerAdapter.loadState = LoadState.NotLoading(false)
            }
        }
    }

    private fun showHideLoading(show: Boolean) {
        val shimmer = synthetic.binding.shimmerViewHelp
        val swipeRefreshLayout = synthetic.binding.swipeRefreshLayout
        val recyclerView = synthetic.binding.helpRecycler

        if (show) {
            shimmer.showShimmer(true)
        } else {
            swipeRefreshLayout.isRefreshing = false
            shimmer.hideShimmer()
        }
        recyclerView.isVisible = !show
        shimmer.isVisible = show
    }

    private fun showErrorView(error: String?) {
        val errorView = synthetic.binding.includeErrorView.errorView
        if (!error.isNullOrEmpty()) {
            synthetic.binding.includeErrorView.textViewErrorMessage.text = error
        }
        errorView.isVisible = true
        synthetic.binding.helpRecycler.isVisible = false
        synthetic.binding.includeErrorView.buttonRefresh.setOnClickListener {
            errorView.isVisible = false
            viewModel.onRefreshData()
        }
    }
}
