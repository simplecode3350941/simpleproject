package com.project2.partner.profile.ui.di

import androidx.lifecycle.ViewModel
import com.project2.core.commons.di.viewmodule.ViewModelKey
import com.project2.partner.profile.ui.PartnerProfileViewModel
import com.project2.partner.profile.ui.help.PartnerFaqViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PartnerProfileViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(PartnerProfileViewModel::class)
    abstract fun partnerProfileViewModel(viewModel: PartnerProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PartnerFaqViewModel::class)
    abstract fun partnerFaqViewModel(viewModel: PartnerFaqViewModel): ViewModel
}