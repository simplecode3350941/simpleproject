package com.project2.partner.profile.ui.mapper

import com.project2.partner.profile.api.domain.models.ProfileDomain
import com.project2.partner.profile.ui.models.PartnerProfileUi

fun ProfileDomain.toPresentation() = PartnerProfileUi(
    firstName = firstName,
    middleName = middleName,
    lastName = lastName,
    role = role,
    email = email
)
