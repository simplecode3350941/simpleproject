package com.project2.partner.profile.ui.about_app

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import androidx.annotation.StringRes
import com.project2.core.network.UrlHelper.Companion.getPrivacyPolicyUrl
import com.project2.core.network.UrlHelper.Companion.getTermsOfUseUrl
import com.project2.core.presentation.activity.WebActivity
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.utils.view.setOnClickDelayListener
import com.project2.partner.profile.impl.ui.BuildConfig
import com.project2.partner.profile.impl.ui.R
import com.project2.partner.profile.impl.ui.databinding.FragmentPartnerAboutAppBinding

class PartnerAboutAppFragment : BaseMvvmFragment() {

    override fun getLayoutResId(): Int = R.layout.fragment_partner_about_app

    override val synthetic = SyntheticBinding(FragmentPartnerAboutAppBinding::inflate)

    override fun initUi(savedInstanceState: Bundle?) = with(synthetic.binding) {
        aboutAppToolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }

        profileAppVersionTextView.text = getString(R.string.app_version, BuildConfig.VERSION_NAME)

        setupClickListeners()
    }

    private fun setupClickListeners(): Unit = with(synthetic.binding) {
        partnerTextViewPrivacyPolicy.setOnClickDelayListener {
            openPdf(R.string.privacy_policy, getPrivacyPolicyUrl())
        }

        partnerTextViewTermsOfUse.setOnClickDelayListener {
            openPdf(R.string.terms_of_use, getTermsOfUseUrl())
        }

        partnerTextViewRateApp.setOnClickDelayListener {
            val intent = Intent(Intent.ACTION_VIEW)
                .setData(Uri.parse("$GOOGLE_PLAY_LINK${requireContext().packageName}"))
            startActivity(intent)
        }
    }

    private fun openPdf(@StringRes titleResId: Int, url: String) {
        val pdfIntent = Intent(Intent.ACTION_VIEW)
            .setDataAndType(Uri.parse(url), "application/pdf")

        val pdfChooser = Intent.createChooser(pdfIntent, getString(R.string.open_pdf))
            .setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

        if (isPdfIntentSupported(pdfIntent)) {
            startActivity(pdfChooser)
        } else {
            WebActivity.start(requireActivity(), url = url, title = getString(titleResId))
        }
    }

    private fun isPdfIntentSupported(pdfIntent: Intent): Boolean {
        val packageManager: PackageManager = requireContext().packageManager
        val list = packageManager.queryIntentActivities(pdfIntent, PackageManager.MATCH_DEFAULT_ONLY)
        return list.size > 0
    }

    companion object {
        const val GOOGLE_PLAY_LINK = "https://play.google.com/store/apps/details?id="
    }
}
