package com.project2.partner.profile.ui.help

import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.core.utils.view.ViewUtils
import com.project2.core.utils.view.setOnClickDelayListener
import com.project2.partner.profile.impl.ui.databinding.PartnerFaqItemViewBinding
import com.project2.partner.profile.ui.models.FaqItemUi

class PartnerFaqAdapter : PagingDataAdapter<FaqItemUi, PartnerFaqAdapter.ViewHolder>(DIFF_CALLBACK) {

    private val expandedFaqIds = mutableSetOf<Long>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    inner class ViewHolder(parent: ViewGroup) : BindingViewHolder<PartnerFaqItemViewBinding>(parent, PartnerFaqItemViewBinding::inflate) {

        private fun isExpanded(itemId: Long) = expandedFaqIds.contains(itemId)

        fun bind(item: FaqItemUi) = with(binding) {
            helpItemNameTextView.text = item.question
            helpItemAnswerTextView.text = item.answer

            itemView.setOnClickDelayListener {

                val isExpanded = isExpanded(item.id)
                val needExpand = !isExpanded

                if (isExpanded) {
                    expandedFaqIds.remove(item.id)
                } else {
                    expandedFaqIds.add(item.id)
                }

                ViewUtils.toggleLayout(helpItemAnswerTextView, needExpand)
                ViewUtils.toggleArrow(helpItemArrowImage, needExpand, true)
            }

            // restore state
            helpItemAnswerTextView.isVisible = isExpanded(item.id)

            if (helpItemAnswerTextView.isVisible) {
                helpItemAnswerTextView.measure(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                helpItemAnswerTextView.alpha = 1F
                helpItemAnswerTextView.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            }

            ViewUtils.toggleArrow(helpItemArrowImage, isExpanded(item.id), false)
        }
    }

    companion object {

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<FaqItemUi>() {
            override fun areItemsTheSame(oldItem: FaqItemUi, newItem: FaqItemUi): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: FaqItemUi, newItem: FaqItemUi): Boolean {
                return oldItem == newItem
            }
        }
    }
}
