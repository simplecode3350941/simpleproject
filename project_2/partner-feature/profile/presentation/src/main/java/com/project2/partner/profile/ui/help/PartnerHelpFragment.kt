package com.project2.partner.profile.ui.help

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.project2.core.presentation.activity.OnboardingStarter
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.resources.databinding.ProfileItemViewBinding
import com.project2.core.utils.common.SystemUtils.sendEmail
import com.project2.core.utils.view.setOnClickDelayListener
import com.project2.partner.profile.impl.ui.R
import com.project2.partner.profile.impl.ui.databinding.FragmentPartnerHelpBinding
import com.project2.partner.profile.ui.di.DaggerPartnerProfileComponent
import javax.inject.Inject

class PartnerHelpFragment : BaseMvvmFragment() {

    @Inject
    lateinit var onboardingStarter: OnboardingStarter
    override fun getLayoutResId() = R.layout.fragment_partner_help

    override val synthetic = SyntheticBinding(FragmentPartnerHelpBinding::inflate)

    override fun inject() {
        DaggerPartnerProfileComponent.builder()
            .partnerProfileDependencies(findComponentDependencies())
            .build()
            .inject(this)
    }

    override fun initUi(savedInstanceState: Bundle?) {
        synthetic.binding.apply {

            toolbar.setNavigationOnClickListener {
                requireActivity().onBackPressed()
            }

            buttonSupportNumber.setOnClickDelayListener {
                sendEmail(getString(R.string.project2_service_email))
            }

            setupFaqMenuItem()
            setupOnboardingMenuItem()
        }
    }

    private fun setupFaqMenuItem() {
        setupMenuItem(
            synthetic.binding.menuItemFaq,
            textId = R.string.faq
        ) {
            findNavController().navigate(R.id.actionPartnerHelp_to_HelpFaq)
        }
    }

    private fun setupOnboardingMenuItem() {
        setupMenuItem(
            synthetic.binding.menuItemOnboarding,
            textId = R.string.onboarding_title
        ) {
            (requireActivity() as? AppCompatActivity)?.let {
                onboardingStarter.onStart(it)
            }
        }
    }

    private fun setupMenuItem(
        menuItemView: ProfileItemViewBinding,
        textId: Int,
        onClick: (v: View) -> Unit
    ) {
        val menuItemParent = menuItemView.profileMenuItemContainer
        val profileItemName =
            menuItemParent.findViewById(R.id.profile_item_title_text_view) as TextView
        val profileItemDivider =
            menuItemParent.findViewById(R.id.profile_item_divider_view) as View
        profileItemDivider.visibility = View.INVISIBLE
        profileItemName.text = getString(textId)

        menuItemParent.setOnClickDelayListener {
            onClick(it)
        }
    }
}
