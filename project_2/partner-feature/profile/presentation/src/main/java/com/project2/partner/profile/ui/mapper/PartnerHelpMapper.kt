package com.project2.partner.profile.ui.mapper

import com.project2.partner.profile.api.domain.models.FaqItemDomain
import com.project2.partner.profile.ui.models.FaqItemUi

fun FaqItemDomain.toPresentation() = FaqItemUi(
    id = id,
    answer = answer,
    question = question
)

