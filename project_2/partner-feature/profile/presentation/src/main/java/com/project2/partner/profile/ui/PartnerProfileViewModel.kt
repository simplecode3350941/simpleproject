package com.project2.partner.profile.ui

import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.partner.auth.api.domain.usecase.SignOutPartnerUseCase
import com.project2.partner.profile.api.domain.usecase.GetPartnerProfileUseCase
import com.project2.partner.profile.ui.mapper.toPresentation
import com.project2.partner.profile.ui.models.PartnerProfileUi
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PartnerProfileViewModel @Inject constructor(
    private val getPartnerProfileUseCase: GetPartnerProfileUseCase,
    private val signOutPartnerUseCase: SignOutPartnerUseCase,
    override val exceptionHandler: CoroutineExceptionHandler,
) : BaseViewModel() {

    private val _profile = MutableStateFlow<PartnerProfileUi?>(null)
    val profileFlow: Flow<PartnerProfileUi> = _profile.asStateFlow().filterNotNull()

    private val _logout = MutableStateFlow(false)
    val logoutFlow: Flow<Boolean> get() = _logout.asStateFlow()

    fun getProfile() {
        launchOnViewModelScope {
            getPartnerProfileUseCase()
                .map { it.toPresentation() }
                .collect {
                    _profile.value = it
                }
        }
    }

    fun logout() {
        launchOnViewModelScope {
            signOutPartnerUseCase()
                .collect {
                    _logout.value = it
                }
        }
    }
}
