package com.project2.partner.profile.ui.models

import com.project2.partner.profile.api.data.remote.models.PartnerRole

class PartnerProfileUi(
    val firstName: String,
    val middleName: String?,
    val lastName: String,
    val role: PartnerRole,
    val email: String
)

val PartnerProfileUi.fullName: String
    get() {
        return if (middleName.isNullOrEmpty()) {
            "$firstName $lastName"
        } else {
            "$firstName $middleName $lastName"
        }
    }
