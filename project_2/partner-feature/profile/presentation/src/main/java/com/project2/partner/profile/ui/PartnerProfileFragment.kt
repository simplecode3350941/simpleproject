package com.project2.partner.profile.ui

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.project2.core.commons.managers.Events
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.ComponentDependenciesProvider
import com.project2.core.presentation.di.HasComponentDependencies
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.session.SessionListener
import com.project2.core.utils.common.lazyUnsafe
import com.project2.core.utils.view.inflate
import com.project2.core.utils.view.setOnClickDelayListener
import com.project2.core.utils.view.setStatusBarColor
import com.project2.partner.profile.api.data.remote.models.PartnerRole
import com.project2.partner.profile.impl.ui.R
import com.project2.partner.profile.impl.ui.databinding.FragmentPartnerProfileLayoutBinding
import com.project2.partner.profile.ui.di.DaggerPartnerProfileComponent
import com.project2.partner.profile.ui.models.PartnerProfileUi
import com.project2.partner.profile.ui.models.fullName
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

class PartnerProfileFragment : BaseMvvmFragment(), HasComponentDependencies {

    @Inject
    override lateinit var dependencies: ComponentDependenciesProvider

    @Inject
    lateinit var sessionListener: SessionListener

    private val viewModel by viewModels<PartnerProfileViewModel> {
        viewModelFactory
    }

    override fun inject() {
        DaggerPartnerProfileComponent.builder()
            .partnerProfileDependencies(findComponentDependencies())
            .build()
            .inject(this)
    }

    override fun getLayoutResId() = R.layout.fragment_partner_profile_layout

    override val synthetic = SyntheticBinding(FragmentPartnerProfileLayoutBinding::inflate)

    private val menuItems by lazyUnsafe {
        listOfNotNull(
            MenuItem.Help(),
            // TODO: implement settings
            // MenuItem.Settings(),
            MenuItem.AboutApp(),
        )
    }

    override fun onStart() {
        super.onStart()
        //RED StatusBar
        requireActivity().setStatusBarColor(ContextCompat.getColor(requireContext(), R.color.red))
    }

    override fun initUi(savedInstanceState: Bundle?) {

        logEvent(Events.MORE_PRESENT)

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.profileFlow.collectLatest(::showProfileData)
        }

        viewModel.getProfile()

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.logoutFlow.collectLatest { isLogout ->
                if (isLogout) {
                    context?.let { ctx ->
                        sessionListener.onForceLogout(ctx)
                    }
                }
            }
        }

        buildProfileMenu()
    }

    private fun showProfileData(profile: PartnerProfileUi) = with(synthetic.binding) {
        textViewName.text = profile.fullName
        textViewRole.text = when (profile.role) {
            PartnerRole.OWNER -> getString(R.string.partner_role_owner)
            PartnerRole.OPERATOR -> getString(R.string.partner_role_operator)
        }
        textViewEmail.text = profile.email

        textViewLogout.setOnClickListener {
            showLogoutDialog(profile.email)
        }
    }

    override fun onStop() {
        //WHITE StatusBar
        requireActivity().setStatusBarColor(ContextCompat.getColor(requireContext(), R.color.white))
        super.onStop()
    }

    private fun buildProfileMenu() {
        val menuLayout = synthetic.binding.layoutMenu
        menuLayout.removeAllViews()
        menuItems.forEach { item ->
            menuLayout.addView(buildMenuItemView(item, View.generateViewId()))
        }
    }

    private fun buildMenuItemView(menuItem: MenuItem, viewId: Int): View {
        val menuLayout = synthetic.binding.layoutMenu
        val profileMenuItem = menuLayout.inflate(R.layout.profile_item_view, false)
        val profileMenuItemContainer = profileMenuItem.findViewById(R.id.profile_menu_item_container) as ConstraintLayout
        val profileItemName = profileMenuItem.findViewById(R.id.profile_item_title_text_view) as TextView

        profileItemName.text = getString(menuItem.resIdTitle)
        profileMenuItemContainer.setOnClickDelayListener {
            onClickMenuItem(menuItem)
        }
        profileMenuItem.id = viewId
        return profileMenuItem
    }

    private fun onClickMenuItem(item: MenuItem) {
        item.resIdNavigate?.let { idRes ->
            findNavController().navigate(idRes)
        }
    }

    private fun showLogoutDialog(account: String) {
        MaterialAlertDialogBuilder(requireContext(), R.style.MaterialDialogStyle)
            .setTitle(R.string.confirm_action)
            .setMessage(getString(R.string.logout_message, account))
            .setNegativeButton(R.string.cancel) { _, _ -> }
            .setPositiveButton(R.string.signout) { dialog, _ ->
                dialog.dismiss()
                viewModel.logout()
            }
            .show()
    }

    sealed class MenuItem {
        abstract val resIdTitle: Int
        abstract val resIdNavigate: Int?

        data class Help(override val resIdTitle: Int = R.string.help_title,
                        override val resIdNavigate: Int = R.id.actionProfile_to_helping) : MenuItem()

        data class Settings(override val resIdTitle: Int = R.string.settings_pin,
                            override val resIdNavigate: Int = R.id.actionProfile_to_settings) : MenuItem()

        data class AboutApp(override val resIdTitle: Int = R.string.about_app,
                            override val resIdNavigate: Int = R.id.actionProfile_to_aboutApp) : MenuItem()
    }

    companion object {

        const val TAG = "PartnerProfileFragment"
        fun newInstance() = PartnerProfileFragment()
    }
}
