package com.project2.partner.profile.ui.help

import com.project2.core.presentation.paging.Paginator
import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.core.utils.common.toImmutableList
import com.project2.partner.profile.api.domain.usecase.GetPartnerFaqUseCase
import com.project2.partner.profile.ui.mapper.toPresentation
import com.project2.partner.profile.ui.models.FaqItemUi
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import timber.log.Timber
import javax.inject.Inject

class PartnerFaqViewModel @Inject constructor(
    private val getPartnerFaqUseCase: GetPartnerFaqUseCase,
    override val exceptionHandler: CoroutineExceptionHandler
) : BaseViewModel() {

    private val _updateState = MutableStateFlow<UiState>(UiState.Loading)
    val updateState = _updateState.asStateFlow()

    private val _paginationState = MutableStateFlow<PaginationState?>(null)
    val paginationState = _paginationState.asStateFlow().filterNotNull()

    private val _faqItemsFlow = MutableStateFlow<List<FaqItemUi>>(listOf())
    val faqItemsFlow: Flow<List<FaqItemUi>> = _faqItemsFlow.asStateFlow()

    private val faqItems: List<FaqItemUi>
        get() = _faqItemsFlow.value.toImmutableList()

    private val paginator = Paginator()
    private val isPagination: Boolean
        get() = paginator.getCurrentPage() > 0

    private val searchFlow = MutableStateFlow("")

    init {
        onRefreshData()

        launchOnViewModelScope {
            searchFlow
                .drop(1) // skip initial search key ""
                .debounce(500)
                .distinctUntilChanged()
                .collectLatest {
                    getFaqItems()
                }
        }
    }

    fun onRefreshData() {
        paginator.reset()
        getFaqItems()
    }

    fun onPaginate() {
        if (paginator.hasData()) {
            getFaqItems()
        }
    }

    private fun getFaqItems() {
        launchOnViewModelScope {
            getPartnerFaqUseCase(
                pageNumber = paginator.getCurrentPage(),
                pageSize = paginator.pageSize,
                searchKey = searchFlow.value
            ).loadingState()
                .map { faqItems -> faqItems.map { it.toPresentation() } }
                .errorState()
                .successState()
        }
    }

    private fun <T> Flow<T>.loadingState(): Flow<T> {
        return onStart {
            when {
                isPagination -> _paginationState.value = PaginationState.Loading
                else -> _updateState.value = UiState.Loading
            }
        }
    }

    private fun <T> Flow<T>.errorState(): Flow<T> {
        return catch {
            when {
                isPagination -> _paginationState.value = PaginationState.Error(it)
                else -> _updateState.value = UiState.Error(it)
            }
        }
    }

    private suspend fun Flow<List<FaqItemUi>>.successState() {
        collect {
            Timber.d("collect - %s", it.size)
            if (isPagination) {
                _paginationState.value = PaginationState.Success
                paginator.nextPage(it.size)
                _faqItemsFlow.value = (faqItems + it).toImmutableList()
            } else {
                if (it.isEmpty()) {
                    _updateState.value = UiState.Empty
                } else {
                    paginator.nextPage(it.size)
                    _updateState.value = UiState.Success
                    _faqItemsFlow.value = it
                }
            }
        }
    }

    fun searchFaq(query: String?) {
        query?.trim()?.let {
            searchFlow.value = it
        }
    }
}

sealed interface UiState {
    object Loading: UiState
    object Success: UiState
    data class Error(val error: Throwable): UiState
    object Empty: UiState
}

sealed interface PaginationState {
    object Loading: PaginationState
    object Success: PaginationState
    data class Error(val error: Throwable): PaginationState
}
