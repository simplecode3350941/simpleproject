package com.project2.partner.profile.ui.models

data class FaqItemUi(
    val id: Long,
    val answer: String,
    val question: String,
)
