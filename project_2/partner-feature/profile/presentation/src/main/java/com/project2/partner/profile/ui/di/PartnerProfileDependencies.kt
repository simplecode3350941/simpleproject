package com.project2.partner.profile.ui.di

import com.project2.core.presentation.activity.OnboardingStarter
import com.project2.core.presentation.di.ComponentDependencies
import com.project2.core.session.SessionListener
import com.project2.partner.profile.api.data.remote.api.PartnerHelpApi


interface PartnerProfileDependencies: ComponentDependencies {
    fun getSessionListener(): SessionListener

    fun partnerHelpApi(): PartnerHelpApi

    fun getOnboardingStarter(): OnboardingStarter
}
