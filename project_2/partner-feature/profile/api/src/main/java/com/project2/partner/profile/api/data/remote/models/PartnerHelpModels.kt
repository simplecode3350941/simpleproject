package com.project2.partner.profile.api.data.remote.models

class FaqResponse(
    val content: List<FaqItem>,
    val number: Int,
    val size: Int,
    val totalElements: Int,
    val totalPages: Int,
    val hasContent: Boolean,
    val numberOfElements: Int,
    val first: Boolean,
    val last: Boolean
)

class FaqItem(
    val id: Long,
    val answer: String,
    val question: String
)
