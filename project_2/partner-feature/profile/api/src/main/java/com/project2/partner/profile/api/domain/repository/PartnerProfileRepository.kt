package com.project2.partner.profile.api.domain.repository

import com.project2.partner.profile.api.data.remote.models.PartnerRole
import com.project2.partner.profile.api.domain.models.ProfileDomain
import kotlinx.coroutines.flow.Flow

interface PartnerProfileRepository {
    suspend fun getAndSaveProfile(): ProfileDomain
    suspend fun getPartnerProfile(): Flow<ProfileDomain>
    suspend fun getPartnerRole(): PartnerRole
    suspend fun clearLocalData()
}