package com.project2.partner.profile.api.domain.models

import com.project2.partner.profile.api.data.remote.models.PartnerRole

class ProfileDomain(
    val firstName: String,
    val middleName: String?,
    val lastName: String,
    val role: PartnerRole,
    val email: String
)
