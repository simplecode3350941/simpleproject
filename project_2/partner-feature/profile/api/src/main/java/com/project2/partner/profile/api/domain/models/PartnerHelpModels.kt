package com.project2.partner.profile.api.domain.models

class FaqItemDomain(
    val id: Long,
    val answer: String,
    val question: String
)
