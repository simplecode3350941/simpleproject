package com.project2.partner.profile.api.data.remote.models

import androidx.annotation.Keep

class ProfileResponse(
    val firstName: String,
    val middleName: String?,
    val lastName: String,
    val role: PartnerRole,
    val email: String
)

@Keep
enum class PartnerRole {
    OWNER,
    OPERATOR
}
