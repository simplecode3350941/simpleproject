package com.project2.partner.profile.api.data.local.models

import com.project2.partner.profile.api.data.remote.models.PartnerRole

class ProfileEntity(
    val firstName: String,
    val middleName: String?,
    val lastName: String,
    val role: PartnerRole,
    val email: String
)

fun ProfileEntity.isEmpty(): Boolean {
    return firstName.isEmpty() && middleName.isNullOrEmpty() && lastName.isEmpty() && email.isEmpty()
}
