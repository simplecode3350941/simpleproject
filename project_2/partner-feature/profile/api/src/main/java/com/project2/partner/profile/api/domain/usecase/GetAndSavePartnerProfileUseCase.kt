package com.project2.partner.profile.api.domain.usecase

import com.project2.partner.profile.api.domain.models.ProfileDomain

interface GetAndSavePartnerProfileUseCase {
    suspend operator fun invoke(): ProfileDomain
}