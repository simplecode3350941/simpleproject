package com.project2.partner.profile.api.domain.usecase

import com.project2.partner.profile.api.domain.models.ProfileDomain
import kotlinx.coroutines.flow.Flow

interface GetPartnerProfileUseCase {
    suspend operator fun invoke(): Flow<ProfileDomain>
}