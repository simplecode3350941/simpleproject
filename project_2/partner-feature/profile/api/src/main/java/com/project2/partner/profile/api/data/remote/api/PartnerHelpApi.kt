package com.project2.partner.profile.api.data.remote.api

import com.project2.partner.profile.api.data.remote.models.FaqResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface PartnerHelpApi {

    @GET("/partners/questions/list")
    suspend fun getQuestionsList(
        @Query("page") pageNumber: Int,
        @Query("size") size: Int,
        @Query("global") searchKey: String? = null,
    ): FaqResponse
}
