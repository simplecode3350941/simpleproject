package com.project2.partner.profile.api.data.remote.api

import com.project2.partner.profile.api.data.remote.models.ProfileResponse
import retrofit2.http.GET

interface PartnerProfileApi {

    @GET("/partners/registration/partner/profile")
    suspend fun getProfile(): ProfileResponse
}