package com.project2.partner.profile.api.domain.repository

import com.project2.partner.profile.api.domain.models.FaqItemDomain
import kotlinx.coroutines.flow.Flow

interface PartnerHelpRepository {

    suspend fun getFaqItems(
        pageNumber: Int,
        pageSize: Int,
        searchKey: String?
    ): Flow<List<FaqItemDomain>>

}
