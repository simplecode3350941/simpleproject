package com.project2.partner.profile.api.data.local.storage

import com.project2.partner.profile.api.data.local.models.ProfileEntity
import kotlinx.coroutines.flow.Flow

interface PartnerProfileLocalDataSource {
    suspend fun getPartnerProfile(): Flow<ProfileEntity>
    suspend fun updateProfile(profile: ProfileEntity)
    suspend fun clearData()
}