package com.project2.partner.profile.api.domain.usecase

import com.project2.partner.profile.api.domain.models.FaqItemDomain
import kotlinx.coroutines.flow.Flow

interface GetPartnerFaqUseCase {

    suspend operator fun invoke(
        pageNumber: Int,
        pageSize: Int,
        searchKey: String? = null
    ): Flow<List<FaqItemDomain>>

}
