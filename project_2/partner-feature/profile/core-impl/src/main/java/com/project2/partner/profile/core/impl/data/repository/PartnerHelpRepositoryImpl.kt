package com.project2.partner.profile.core.impl.data.repository

import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.partner.profile.api.data.remote.api.PartnerHelpApi
import com.project2.partner.profile.api.domain.models.FaqItemDomain
import com.project2.partner.profile.api.domain.repository.PartnerHelpRepository
import com.project2.partner.profile.core.impl.data.mapper.toDomain
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PartnerHelpRepositoryImpl @Inject constructor(
    private val helpApi: PartnerHelpApi,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
): PartnerHelpRepository {

    override suspend fun getFaqItems(
        pageNumber: Int,
        pageSize: Int,
        searchKey: String?
    ): Flow<List<FaqItemDomain>> {
        return flow {
            val faqItems = helpApi.getQuestionsList(
                pageNumber = pageNumber,
                size = pageSize,
                searchKey = searchKey
            ).content
            emit(faqItems)
        }.map { questions ->
            questions.map { it.toDomain() }
        }.flowOn(dispatcher)
    }
}
