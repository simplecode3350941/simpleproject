package com.project2.partner.profile.core.impl.data.mapper

import com.project2.partner.profile.api.data.local.models.ProfileEntity
import com.project2.partner.profile.api.data.remote.models.ProfileResponse
import com.project2.partner.profile.api.domain.models.ProfileDomain

fun ProfileResponse.toDomain() = ProfileDomain(
    firstName = firstName,
    middleName = middleName,
    lastName = lastName,
    role = role,
    email = email
)

fun ProfileResponse.toEntity() = ProfileEntity(
    firstName = firstName,
    middleName = middleName,
    lastName = lastName,
    role = role,
    email = email
)

fun ProfileEntity.toDomain() = ProfileDomain(
    firstName = firstName,
    middleName = middleName,
    lastName = lastName,
    role = role,
    email = email
)