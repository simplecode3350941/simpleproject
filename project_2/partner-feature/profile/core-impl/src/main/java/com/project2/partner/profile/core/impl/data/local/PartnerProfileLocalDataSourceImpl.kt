package com.project2.partner.profile.core.impl.data.local

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.project2.partner.profile.api.data.local.models.ProfileEntity
import com.project2.partner.profile.api.data.local.storage.PartnerProfileLocalDataSource
import com.project2.partner.profile.api.data.remote.models.PartnerRole
import com.project2.partner.profile.core.impl.data.local.PartnerProfileLocalDataSourceImpl.Companion.PROFILE_PREFERENCES
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PartnerProfileLocalDataSourceImpl @Inject constructor(context: Context) : PartnerProfileLocalDataSource {

    private val dataStore: DataStore<Preferences> = context.dataStore

    private val partnerProfile: Flow<ProfileEntity>
        get() = dataStore.data
            .map { preferences ->
                ProfileEntity(
                    firstName = preferences[FIRST_NAME] ?: "",
                    middleName = preferences[MIDDLE_NAME] ?: "",
                    lastName = preferences[LAST_NAME] ?: "",
                    role = preferences[ROLE]?.let {
                        PartnerRole.valueOf(it)
                    } ?: PartnerRole.OWNER,
                    email = preferences[EMAIL] ?: ""
                )
            }

    override suspend fun getPartnerProfile() = partnerProfile

    override suspend fun updateProfile(profile: ProfileEntity) {
        dataStore.edit { preferences ->
            preferences[FIRST_NAME] = profile.firstName
            preferences[MIDDLE_NAME] = profile.middleName ?: ""
            preferences[LAST_NAME] = profile.lastName
            preferences[ROLE] = profile.role.name
            preferences[EMAIL] = profile.email
        }
    }

    override suspend fun clearData() {
        dataStore.edit { preferences ->
            preferences.clear()
        }
    }

    companion object {
        const val PROFILE_PREFERENCES = "partner_profile_preferences"

        val FIRST_NAME = stringPreferencesKey("first_name")
        val MIDDLE_NAME = stringPreferencesKey("middle_name")
        val LAST_NAME = stringPreferencesKey("last_name")
        val ROLE = stringPreferencesKey("role")
        val EMAIL = stringPreferencesKey("email")
    }
}

private val Context.dataStore by preferencesDataStore(
    name = PROFILE_PREFERENCES
)
