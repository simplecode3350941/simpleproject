package com.project2.partner.profile.core.impl.data.repository

import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.partner.profile.api.data.local.models.isEmpty
import com.project2.partner.profile.api.data.local.storage.PartnerProfileLocalDataSource
import com.project2.partner.profile.api.data.remote.api.PartnerProfileApi
import com.project2.partner.profile.api.data.remote.models.PartnerRole
import com.project2.partner.profile.api.domain.models.ProfileDomain
import com.project2.partner.profile.api.domain.repository.PartnerProfileRepository
import com.project2.partner.profile.core.impl.data.mapper.toDomain
import com.project2.partner.profile.core.impl.data.mapper.toEntity
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class PartnerProfileRepositoryImpl @Inject constructor(
    private val profileApi: PartnerProfileApi,
    private val localDataSource: PartnerProfileLocalDataSource,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
    ): PartnerProfileRepository {

    override suspend fun getPartnerProfile(): Flow<ProfileDomain> {
        return flow {
            val localProfile = localDataSource.getPartnerProfile()
                .firstOrNull()

            val profileDomain = if(localProfile == null || localProfile.isEmpty()) {
                getAndSaveProfile()
            } else {
                localProfile.toDomain()
            }

            emit(profileDomain)
        }.flowOn(dispatcher)
    }

    override suspend fun getPartnerRole(): PartnerRole {
        return getPartnerProfile().first().role
    }

    override suspend fun clearLocalData() {
        localDataSource.clearData()
    }

    override suspend fun getAndSaveProfile(): ProfileDomain {
        val remoteProfile = profileApi.getProfile()
        localDataSource.updateProfile(remoteProfile.toEntity())

        return remoteProfile.toDomain()
    }
}