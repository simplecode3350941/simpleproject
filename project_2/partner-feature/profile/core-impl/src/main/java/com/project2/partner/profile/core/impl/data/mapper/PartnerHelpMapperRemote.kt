package com.project2.partner.profile.core.impl.data.mapper

import com.project2.partner.profile.api.data.remote.models.FaqItem
import com.project2.partner.profile.api.domain.models.FaqItemDomain

fun FaqItem.toDomain() = FaqItemDomain(
    id = id,
    answer = answer,
    question = question
)
