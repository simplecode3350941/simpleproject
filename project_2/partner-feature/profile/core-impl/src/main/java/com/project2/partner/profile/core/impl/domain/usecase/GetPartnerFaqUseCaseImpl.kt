package com.project2.partner.profile.core.impl.domain.usecase

import com.project2.partner.profile.api.domain.models.FaqItemDomain
import com.project2.partner.profile.api.domain.repository.PartnerHelpRepository
import com.project2.partner.profile.api.domain.usecase.GetPartnerFaqUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPartnerFaqUseCaseImpl @Inject constructor(
    private val repository: PartnerHelpRepository
): GetPartnerFaqUseCase {
    override suspend fun invoke(
        pageNumber: Int,
        pageSize: Int,
        searchKey: String?
    ): Flow<List<FaqItemDomain>> {
        return repository.getFaqItems(
            pageNumber = pageNumber,
            pageSize = pageSize,
            searchKey = searchKey
        )
    }
}
