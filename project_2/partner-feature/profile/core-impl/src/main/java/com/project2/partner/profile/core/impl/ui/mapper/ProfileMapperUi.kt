package com.project2.partner.profile.impl.ui.mapper

import com.project2.partner.profile.api.domain.models.ProfileDomain
import com.project2.partner.profile.core.impl.ui.models.ProfileUi

fun ProfileDomain.toPresentation() = ProfileUi(
    firstName = firstName,
    middleName = middleName,
    lastName = lastName,
    role = role,
    email = email
)