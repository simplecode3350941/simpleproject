package com.project2.partner.profile.core.impl.domain.usecase

import com.project2.partner.profile.api.domain.models.ProfileDomain
import com.project2.partner.profile.api.domain.repository.PartnerProfileRepository
import com.project2.partner.profile.api.domain.usecase.GetPartnerProfileUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPartnerProfileUseCaseImpl @Inject constructor (private val repository: PartnerProfileRepository)
    : GetPartnerProfileUseCase {

    override suspend operator fun invoke(): Flow<ProfileDomain> {
        return repository.getPartnerProfile()
    }
}