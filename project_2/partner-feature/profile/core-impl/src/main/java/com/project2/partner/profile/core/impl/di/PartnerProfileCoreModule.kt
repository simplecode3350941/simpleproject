package com.project2.partner.profile.core.impl.di

import android.content.Context
import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.core.network.BuildConfig
import com.project2.partner.profile.api.data.local.storage.PartnerProfileLocalDataSource
import com.project2.partner.profile.api.data.remote.api.PartnerHelpApi
import com.project2.partner.profile.api.data.remote.api.PartnerProfileApi
import com.project2.partner.profile.api.domain.repository.PartnerHelpRepository
import com.project2.partner.profile.api.domain.repository.PartnerProfileRepository
import com.project2.partner.profile.api.domain.usecase.GetAndSavePartnerProfileUseCase
import com.project2.partner.profile.api.domain.usecase.GetPartnerFaqUseCase
import com.project2.partner.profile.api.domain.usecase.GetPartnerProfileUseCase
import com.project2.partner.profile.core.impl.data.local.PartnerProfileLocalDataSourceImpl
import com.project2.partner.profile.core.impl.data.repository.PartnerHelpRepositoryImpl
import com.project2.partner.profile.core.impl.data.repository.PartnerProfileRepositoryImpl
import com.project2.partner.profile.core.impl.domain.usecase.GetAndSavePartnerProfileUseCaseImpl
import com.project2.partner.profile.core.impl.domain.usecase.GetPartnerFaqUseCaseImpl
import com.project2.partner.profile.core.impl.domain.usecase.GetPartnerProfileUseCaseImpl
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class PartnerProfileCoreModule {

    @Provides
    @Singleton
    fun providePartnerContractApi(
        @Named("partner_api") okHttpClient: OkHttpClient,
        @Named("moshi_converter") moshiConverterFactory: MoshiConverterFactory
    ): PartnerProfileApi {
        val builder = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.project2_API_URL)
            .addConverterFactory(moshiConverterFactory)
            .build()

        return builder.create(PartnerProfileApi::class.java)
    }

    @Provides
    @Singleton
    fun providePartnerHelpApi(
        @Named("partner_api") okHttpClient: OkHttpClient,
        @Named("moshi_converter") moshiConverterFactory: MoshiConverterFactory
    ): PartnerHelpApi {
        val builder = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.project2_API_URL)
            .addConverterFactory(moshiConverterFactory)
            .build()

        return builder.create(PartnerHelpApi::class.java)
    }

    @Provides
    fun providePartnerLocalDataSource(context: Context) : PartnerProfileLocalDataSource {
        return PartnerProfileLocalDataSourceImpl(context)
    }

    @Provides
    fun providePartnerProfileRepository(profileApi: PartnerProfileApi, localDataSource: PartnerProfileLocalDataSource, @IoDispatcher dispatcher: CoroutineDispatcher) : PartnerProfileRepository {
        return PartnerProfileRepositoryImpl(profileApi, localDataSource, dispatcher)
    }

    @Provides
    fun providePartnerHelpRepository(
        helpApi: PartnerHelpApi,
        @IoDispatcher dispatcher: CoroutineDispatcher
    ): PartnerHelpRepository {
        return PartnerHelpRepositoryImpl(helpApi, dispatcher)
    }

    @Provides
    fun provideGetPartnerProfileUseCase(repository: PartnerProfileRepository): GetPartnerProfileUseCase {
        return GetPartnerProfileUseCaseImpl(repository)
    }

    @Provides
    fun provideGetPartnerFaqUseCase(repository: PartnerHelpRepository): GetPartnerFaqUseCase {
        return GetPartnerFaqUseCaseImpl(repository)
    }

    @Provides
    fun provideGetAndSavePartnerProfileUseCase(repository: PartnerProfileRepository): GetAndSavePartnerProfileUseCase {
        return GetAndSavePartnerProfileUseCaseImpl(repository)
    }
}