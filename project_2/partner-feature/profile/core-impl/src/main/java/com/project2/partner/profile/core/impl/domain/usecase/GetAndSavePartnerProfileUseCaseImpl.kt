package com.project2.partner.profile.core.impl.domain.usecase

import com.project2.partner.profile.api.domain.models.ProfileDomain
import com.project2.partner.profile.api.domain.repository.PartnerProfileRepository
import com.project2.partner.profile.api.domain.usecase.GetAndSavePartnerProfileUseCase
import javax.inject.Inject

class GetAndSavePartnerProfileUseCaseImpl @Inject constructor (private val repository: PartnerProfileRepository)
    : GetAndSavePartnerProfileUseCase {

    override suspend operator fun invoke(): ProfileDomain {
        return repository.getAndSaveProfile()
    }
}