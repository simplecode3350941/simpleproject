package com.project2.partner.profile.core.impl.ui.models

import com.project2.partner.profile.api.data.remote.models.PartnerRole

class ProfileUi(
    val firstName: String,
    val middleName: String?,
    val lastName: String,
    val role: PartnerRole,
    val email: String
)
