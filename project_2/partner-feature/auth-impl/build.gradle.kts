@file:Suppress("UnstableApiUsage")

@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.kapt)
}

android {
    namespace = "com.project2.partner.auth.impl"
    compileSdk = libs.versions.compileSdk.get().toInt()

    defaultConfig {
        minSdk = libs.versions.minSdk.get().toInt()
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    packagingOptions {
        resources.excludes += "META-INF/notice.txt"
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_11.toString()
    }

    kapt {
        correctErrorTypes = true
        generateStubs = true
    }

    viewBinding {
        android.buildFeatures.viewBinding = true
    }
}

dependencies {
    api(project(":partner-feature:profile:api"))
    implementation(project(":partner-feature:auth-api"))

    implementation(project(":core:auth"))
    implementation(project(":core:session"))
    implementation(project(":core:presentation"))
    implementation(project(":core:network"))
    implementation(project(":core:resources"))
    implementation(libs.retrofit)
    implementation(libs.kotlinx.coroutines.android)

    implementation(libs.kotlin.stdlib)
    implementation(libs.core.ktx)
    implementation(libs.fragment.ktx)
    implementation(libs.app.compat)
    implementation(libs.bundles.androidx.ui.components)
    implementation(libs.shimmer)

    // Lifecycle components
    implementation(libs.lifecycle.runtime.ktx)
//    implementation(libs.bundles.androidx.lifecycle)
//    kapt(libs.lifecycle.compiler)

    api(libs.dagger.api)
    kapt(libs.dagger.compiler.kapt)
    implementation(libs.timber)

    implementation(platform(libs.okhttp.bom))
    implementation(libs.bundles.networking)
    implementation(libs.bundles.moshi)
    kapt(libs.moshi.kotlin.kapt)
}