package com.project2.partner.auth.impl.di

import androidx.lifecycle.ViewModel
import com.project2.core.commons.di.viewmodule.ViewModelKey
import com.project2.partner.auth.impl.ui.PartnerAuthViewModel
import com.project2.partner.auth.impl.ui.change_password.PartnerChangePasswordViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class AuthViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(PartnerAuthViewModel::class)
    abstract fun partnerAuthViewModel(viewModel: PartnerAuthViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PartnerChangePasswordViewModel::class)
    abstract fun partnerChangePasswordViewModel(viewModel: PartnerChangePasswordViewModel): ViewModel
}
