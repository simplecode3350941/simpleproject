package com.project2.partner.auth.impl.ui

import android.os.Bundle
import android.view.WindowManager
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.project2.core.auth.AuthResultViewModel
import com.project2.core.auth.AuthStatus
import com.project2.core.auth.role.UserRole
import com.project2.core.auth.utils.KeyboardListener
import com.project2.core.network.call_adapter.ApiError
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.ComponentDependenciesProvider
import com.project2.core.presentation.di.HasComponentDependencies
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.utils.common.SystemUtils.sendEmail
import com.project2.core.utils.common.isValidEmail
import com.project2.core.utils.mask.AuthMailMaskWatcher
import com.project2.core.utils.view.fadeIn
import com.project2.core.utils.view.fadeOut
import com.project2.partner.auth.impl.R
import com.project2.partner.auth.impl.databinding.FragmentAuthPartnerBinding
import com.project2.partner.auth.impl.di.DaggerAuthComponent
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

class PartnerAuthFragment: BaseMvvmFragment(), HasComponentDependencies {

    @Inject
    override lateinit var dependencies: ComponentDependenciesProvider

    private val viewModel by viewModels<PartnerAuthViewModel> {
        viewModelFactory
    }

    private val authActivityViewModel by activityViewModels<AuthResultViewModel> {
        viewModelFactory
    }

    private val mailMask = AuthMailMaskWatcher()

    override val synthetic = SyntheticBinding(FragmentAuthPartnerBinding::inflate)

    override fun inject() {
        DaggerAuthComponent.builder()
            .authDependencies(findComponentDependencies())
            .build()
            .inject(this)
    }

    override fun getLayoutResId() = R.layout.fragment_auth_partner

    override fun initUi(savedInstanceState: Bundle?) = with(synthetic.binding) {

        partnerButtonSignContract.setOnClickListener {
            //buttonsListener.onClickSignContract(AuthActivity.AUTH_PARTNER)
        }

        textViewSupportEmail.setOnClickListener {
            sendEmail(getString(R.string.support_email))
        }

        partnerTextViewForgotPassword.setOnClickListener {
            //buttonsListener.onClickForgotPassword()
        }

        partnerEditTextLogin.removeTextChangedListener(mailMask)
        partnerEditTextLogin.addTextChangedListener(mailMask)

        partnerEditTextLogin.doAfterTextChanged {
            validateForm()
        }

        partnerEditTextPassword.doAfterTextChanged {
            validateForm()
        }

        lifecycleScope.launch {
            viewModel.updateState.collectLatest(::updateUi)
        }

        partnerButtonSignIn.setOnClickListener {
            signIn()
        }

        validateForm()

        initKeyboardListener()
    }

    private fun validateForm() {
        with(synthetic.binding) {
            val login = partnerEditTextLogin.text.toString()
            val password = partnerEditTextPassword.text.toString()

            val loginIsValid = isValidEmail(login)
            val passwordIsValid = password.isNotEmpty()

            partnerInputLayoutPassword.isEndIconVisible = passwordIsValid
            partnerButtonSignIn.isEnabled = loginIsValid && passwordIsValid
        }
    }

    private fun updateUi(state: UiState) {
        when (state) {
            is UiState.Loading -> startLoading()
            is UiState.Success -> {
                stopLoading()
                authActivityViewModel.emitResult(AuthStatus.Success(UserRole.PARTNER))
            }
            is UiState.Error -> {
                stopLoading()
                val error = state.error
                when {
                    error is ApiError && error.type == PASSWORD_CHANGE_REQUIRED -> changePasswordFlow()
                    error is ApiError -> showErrorDialog(error.message)
                    else -> showErrorDialog()
                }
            }
        }
    }

    private fun changePasswordFlow() {
        authActivityViewModel.emitResult(
            AuthStatus.RequiredChangePassword(
                role = UserRole.PARTNER,
                userName = synthetic.binding.partnerEditTextLogin.text.toString(),
                oldPassword = synthetic.binding.partnerEditTextPassword.text.toString()
            )
        )
    }

    private fun signIn() {
        val login = synthetic.binding.partnerEditTextLogin.text.toString()
        val password = synthetic.binding.partnerEditTextPassword.text.toString()
        viewModel.requestSignIn(login, password)
    }

    private fun initKeyboardListener() {
        val rootLayout = synthetic.binding.rootLayout
        val buttonSignIn = synthetic.binding.partnerButtonSignIn

        requireActivity().window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        KeyboardListener(lifecycle, rootLayout, ::onStateChanged)
    }

    private fun onStateChanged(isShowing: Boolean) {
        if(isShowing) {
            synthetic.safeBinding?.textViewSupportEmail?.fadeOut(duration = 100)
        } else {
            synthetic.safeBinding?.textViewSupportEmail?.fadeIn()
        }
    }

    companion object {

        const val PASSWORD_CHANGE_REQUIRED = "PASSWORD_CHANGE_REQUIRED"

        fun newInstance(): PartnerAuthFragment {
            return PartnerAuthFragment()
        }
    }
}
