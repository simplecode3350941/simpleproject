package com.project2.partner.auth.impl.di

import com.project2.core.presentation.di.ComponentDependencies
import com.project2.core.session.data.SessionRepository
import com.project2.partner.auth.api.data.remote.api.PartnerAuthApi
import com.project2.partner.auth.api.domain.repository.PartnerAuthRepository
import com.project2.partner.auth.api.domain.usecase.SignInPartnerUseCase

interface AuthDependencies: ComponentDependencies {
    fun authApi(): PartnerAuthApi
    fun partnerAuthRepository(): PartnerAuthRepository
    fun sessionRepository(): SessionRepository
    fun signInPartnerUseCase(): SignInPartnerUseCase
}