package com.project2.partner.auth.impl.domain

import com.project2.partner.auth.api.domain.repository.PartnerAuthRepository
import com.project2.partner.auth.api.domain.usecase.ChangePasswordPartnerUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ChangePasswordPartnerUseCaseImpl @Inject constructor(
    private val repository: PartnerAuthRepository
    ) : ChangePasswordPartnerUseCase {

    override fun invoke(
        username: String,
        oldPassword: String,
        newPassword: String
    ): Flow<Boolean> {
        return repository.changePassword(
            username = username,
            oldPassword = oldPassword,
            newPassword = newPassword
        ).map { true }
    }
}
