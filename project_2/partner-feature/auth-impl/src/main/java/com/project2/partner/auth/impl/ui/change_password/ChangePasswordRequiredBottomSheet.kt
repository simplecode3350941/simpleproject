package com.project2.partner.auth.impl.ui.change_password

import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResult
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.dialog.BaseBottomSheetDialogFragment
import com.project2.partner.auth.impl.databinding.BottomSheetChangePasswordRequiredBinding

class ChangePasswordRequiredBottomSheet : BaseBottomSheetDialogFragment() {

    override val synthetic = SyntheticBinding(BottomSheetChangePasswordRequiredBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isCancelable = false
        dialog?.setCanceledOnTouchOutside(false)

        synthetic.binding.buttonOk.setOnClickListener {
            setFragmentResult(CHANGE_PASSWORD_REQUEST_KEY, Bundle())
            dismiss()
        }
    }

    companion object {
        const val TAG = "ChangePasswordRequiredBottomSheet"

        const val CHANGE_PASSWORD_REQUEST_KEY = "change_password"
        fun show(manager: FragmentManager) {
            ChangePasswordRequiredBottomSheet().show(manager, TAG)
        }
    }
}
