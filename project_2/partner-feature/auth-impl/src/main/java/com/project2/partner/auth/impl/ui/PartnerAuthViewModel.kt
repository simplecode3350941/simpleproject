package com.project2.partner.auth.impl.ui

import com.project2.core.auth.BaseAuthViewModel
import com.project2.partner.auth.api.domain.usecase.SignInPartnerUseCase
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.onStart
import timber.log.Timber
import javax.inject.Inject

class PartnerAuthViewModel @Inject constructor(
    private val signInPartnerUseCase: SignInPartnerUseCase,
    override val exceptionHandler: CoroutineExceptionHandler
): BaseAuthViewModel() {

    private val _updateState = MutableStateFlow<UiState?>(null)
    val updateState = _updateState.asStateFlow().filterNotNull()

    override fun requestSignIn(login: String, password: String) {
        launchOnViewModelScope {
            signInPartnerUseCase(login, password)
                .onStart { _updateState.value = UiState.Loading }
                .catch { _updateState.value = UiState.Error(it) }
                .collect {
                    Timber.d("collect - %s", it)
                    _updateState.value = UiState.Success
                }
        }
    }
}
