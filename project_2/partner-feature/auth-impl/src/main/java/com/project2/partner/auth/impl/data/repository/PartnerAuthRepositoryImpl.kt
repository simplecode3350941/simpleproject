package com.project2.partner.auth.impl.data.repository

import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.core.network.call_adapter.successOrError
import com.project2.partner.auth.api.data.remote.api.PartnerAuthApi
import com.project2.partner.auth.api.data.remote.models.AuthRequestBody
import com.project2.partner.auth.api.data.remote.models.ChangePasswordRequestBody
import com.project2.partner.auth.api.data.remote.models.SignOutRequestBody
import com.project2.partner.auth.api.domain.models.AuthTokensDomain
import com.project2.partner.auth.api.domain.repository.PartnerAuthRepository
import com.project2.partner.auth.impl.data.mapper.toDomain
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PartnerAuthRepositoryImpl @Inject constructor(
    private val authApi: PartnerAuthApi,
    @IoDispatcher
    private val dispatcher: CoroutineDispatcher
): PartnerAuthRepository {

    override fun signIn(login: String, password: String): Flow<AuthTokensDomain> {
        return flow {
            val response = authApi.signIn(AuthRequestBody(login, password, "PASSWORD"))
            emit(response.successOrError().toDomain())
        }
        .flowOn(dispatcher)
    }

    override fun singOut(refreshToken: String): Flow<Unit> {
        return flow {
            authApi.signOut(SignOutRequestBody(refreshToken))
            emit(Unit)
        }
        .flowOn(dispatcher)
    }

    override fun changePassword(
        username: String,
        oldPassword: String,
        newPassword: String
    ): Flow<Unit> {
        return flow {
            authApi.changePassword(
                ChangePasswordRequestBody(
                    username = username,
                    oldPassword = oldPassword,
                    newPassword = newPassword
                )
            )
            emit(Unit)
        }
        .flowOn(dispatcher)
    }
}
