package com.project2.partner.auth.impl.ui.change_password

import androidx.lifecycle.viewModelScope
import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.partner.auth.api.domain.usecase.ChangePasswordPartnerUseCase
import com.project2.partner.auth.impl.ui.UiState
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import java.util.regex.Matcher
import java.util.regex.Pattern
import javax.inject.Inject

class PartnerChangePasswordViewModel @Inject constructor(
    private val changePasswordPartnerUseCase: ChangePasswordPartnerUseCase,
    override val exceptionHandler: CoroutineExceptionHandler
) : BaseViewModel() {

    private val _isPasswordValidFlow = MutableSharedFlow<Boolean>()
    val isPasswordValidFlow = _isPasswordValidFlow.asSharedFlow()
    private var isPasswordValid = false

    private val _isRepeatPasswordValidFlow = MutableSharedFlow<Boolean>()
    val isRepeatPasswordValidFlow = _isRepeatPasswordValidFlow.asSharedFlow()
    private var isRepeatPasswordValid = false

    private val _updateState = MutableStateFlow<UiState?>(null)
    val updateState = _updateState.asStateFlow().filterNotNull()

    private fun validateNewPassword(newPassword: String) {
        val passwordValidationRegEx: String = "^" +
                "(?=.*[0-9])" +         //at least 1 digit
                "(?=.*[a-z])" +         //at least 1 lower case letter
                "(?=.*[A-Z])" +         //at least 1 upper case letter
                "(?=.*[-@%\\[}+'!/#$^?:;,(\")~`.*=&{>\\]<_])" + //at least 1 special character
                "(?=\\S+$)" +           //no white spaces
                ".{8,}" +               //at least 8 characters
                "$"

        val passwordPattern: Pattern = Pattern.compile(passwordValidationRegEx)
        val matcher: Matcher = passwordPattern.matcher(newPassword)

        viewModelScope.launch {
            val isValid = matcher.matches()
            _isPasswordValidFlow.emit(isValid)
            isPasswordValid = isValid
        }
    }

    private fun validateRepeatPassword(
        newPassword: String,
        repeatPassword: String
    ) {
        viewModelScope.launch {
            val isValid = (newPassword == repeatPassword)
            _isRepeatPasswordValidFlow.emit(isValid)
            isRepeatPasswordValid = isValid
        }
    }

    fun onSaveClick(
        userName: String,
        oldPassword: String,
        newPassword: String,
        repeatPassword: String
    ) {
        validateNewPassword(newPassword)
        validateRepeatPassword(
            newPassword = newPassword,
            repeatPassword = repeatPassword
        )

        if (isPasswordValid && isRepeatPasswordValid) {
            launchOnViewModelScope {
                changePasswordPartnerUseCase(
                    username = userName,
                    oldPassword = oldPassword,
                    newPassword = newPassword
                )
                    .onStart { _updateState.value = UiState.Loading }
                    .catch { _updateState.value = UiState.Error(it) }
                    .collect {
                        _updateState.value = UiState.Success
                    }
            }
        }
    }
}
