package com.project2.partner.auth.impl.domain

import com.project2.core.session.data.SessionRepository
import com.project2.partner.auth.api.domain.repository.PartnerAuthRepository
import com.project2.partner.auth.api.domain.usecase.SignOutPartnerUseCase
import com.project2.partner.profile.api.domain.repository.PartnerProfileRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class SignOutPartnerUseCaseImpl @Inject constructor(private val sessionRepository: SessionRepository,
                                                    private val partnerAuthRepository: PartnerAuthRepository,
                                                    private val partnerProfileRepository: PartnerProfileRepository
) : SignOutPartnerUseCase {

    override fun invoke(): Flow<Boolean> {
        val refreshToken: String? = sessionRepository.getRefreshToken()

        val signOutFlow: Flow<Unit> = refreshToken?.let {
            partnerAuthRepository.singOut(refreshToken = it)
        } ?: flowOf(Unit)

        return signOutFlow.map {
            partnerProfileRepository.clearLocalData()
            sessionRepository.clearData()
            true
        }
    }
}
