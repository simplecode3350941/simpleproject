package com.project2.partner.auth.impl.ui.change_password

import android.os.Bundle
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.textfield.TextInputLayout
import com.project2.core.auth.AuthResultViewModel
import com.project2.core.auth.AuthStatus
import com.project2.core.auth.role.UserRole
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.ComponentDependenciesProvider
import com.project2.core.presentation.di.HasComponentDependencies
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.partner.auth.impl.R
import com.project2.partner.auth.impl.databinding.FragmentChangePasswordPartnerBinding
import com.project2.partner.auth.impl.di.DaggerAuthComponent
import com.project2.partner.auth.impl.ui.UiState
import com.project2.partner.auth.impl.ui.change_password.PasswordChangedBottomSheet.Companion.PASSWORD_CHANGED_REQUEST_KEY
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

class PartnerChangePasswordFragment : BaseMvvmFragment(), HasComponentDependencies {

    @Inject
    override lateinit var dependencies: ComponentDependenciesProvider

    private val viewModel by viewModels<PartnerChangePasswordViewModel> {
        viewModelFactory
    }

    private val authActivityViewModel by activityViewModels<AuthResultViewModel> {
        viewModelFactory
    }

    override fun inject() {
        DaggerAuthComponent.builder()
            .authDependencies(findComponentDependencies())
            .build()
            .inject(this)
    }

    override val synthetic = SyntheticBinding(FragmentChangePasswordPartnerBinding::inflate)

    override fun getLayoutResId(): Int = R.layout.fragment_change_password_partner

    override fun initUi(savedInstanceState: Bundle?) {
        childFragmentManager.setFragmentResultListener(PASSWORD_CHANGED_REQUEST_KEY, this) { _, _ ->
            authActivityViewModel.emitResult(AuthStatus.PasswordIsChanged(UserRole.PARTNER))
        }

        with(synthetic.binding) {
            changePasswordToolbar.setNavigationOnClickListener { popBack() }

            buttonSave.setOnClickListener {
                viewModel.onSaveClick(
                    userName = requireNotNull(arguments?.getString(ARG_KEY_USER_NAME)),
                    oldPassword = requireNotNull(arguments?.getString(ARG_KEY_OLD_PASSOWRD)),
                    newPassword = partnerEditTextPassword.text.toString(),
                    repeatPassword = partnerEditTextRepeatPassword.text.toString()
                )
            }

            partnerEditTextPassword.addTextChangedListener {
                resetError(partnerInputLayoutPassword)
            }

            partnerEditTextRepeatPassword.addTextChangedListener {
                resetError(partnerInputLayoutRepeatPassword)
            }

            lifecycleScope.launch {
                viewModel.updateState.collectLatest(::updateUi)
            }

            lifecycleScope.launch {
                viewModel.isPasswordValidFlow.collectLatest(::handlePasswordValidState)
            }

            lifecycleScope.launch {
                viewModel.isRepeatPasswordValidFlow.collectLatest(::handleRepeatPasswordValidState)
            }
        }
    }

    private fun updateUi(state: UiState) {
        when (state) {
            is UiState.Loading -> startLoading()
            is UiState.Success -> {
                stopLoading()
                PasswordChangedBottomSheet.show(childFragmentManager)
            }
            is UiState.Error -> stopLoading()
        }
    }

    private fun handlePasswordValidState(isPasswordValid: Boolean) = with(synthetic.binding) {
        if (!isPasswordValid) {
            partnerInputLayoutPassword.error = getString(R.string.partner_change_password_too_simple_password)
        } else {
            resetError(partnerInputLayoutPassword)
        }
    }

    private fun handleRepeatPasswordValidState(isRepeatPasswordValid: Boolean) = with(synthetic.binding) {
        if (!isRepeatPasswordValid) {
            partnerInputLayoutRepeatPassword.error = getString(R.string.partner_change_password_passwords_are_different)
        } else {
            resetError(partnerInputLayoutRepeatPassword)
        }
    }
    private fun resetError(textInputLayout: TextInputLayout) {
        if (textInputLayout.error != null) {
            textInputLayout.error = null
        }
    }

    companion object {
        const val ARG_KEY_USER_NAME = "user_name"
        const val ARG_KEY_OLD_PASSOWRD = "old_password"
    }
}
