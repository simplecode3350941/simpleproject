package com.project2.partner.auth.impl.domain

import com.project2.core.session.data.SessionRepository
import com.project2.partner.auth.api.domain.repository.PartnerAuthRepository
import com.project2.partner.auth.api.domain.usecase.SignInPartnerUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class SignInPartnerUseCaseImpl @Inject constructor(private val sessionRepository: SessionRepository,
                                                   private val repository: PartnerAuthRepository
) : SignInPartnerUseCase {

    override fun invoke(login: String, password: String): Flow<Boolean> {
        return repository.signIn(login, password)
            .onEach { authTokensDomain ->
                sessionRepository.saveTokens(
                    accessToken = authTokensDomain.accessToken,
                    refreshToken = authTokensDomain.refreshToken
                )
            }
            .map { true }
    }
}