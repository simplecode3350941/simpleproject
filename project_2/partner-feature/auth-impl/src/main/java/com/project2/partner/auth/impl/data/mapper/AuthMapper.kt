package com.project2.partner.auth.impl.data.mapper

import com.project2.partner.auth.api.data.remote.models.AuthTokenResponse
import com.project2.partner.auth.api.domain.models.AuthTokensDomain

fun AuthTokenResponse.toDomain() = AuthTokensDomain(
    accessToken = accessToken,
    refreshToken = refreshToken
)