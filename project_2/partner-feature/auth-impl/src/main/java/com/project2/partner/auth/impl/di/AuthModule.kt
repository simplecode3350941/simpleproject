package com.project2.partner.auth.impl.di

import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.core.network.BuildConfig
import com.project2.core.network.call_adapter.ResponseCallAdapterFactory
import com.project2.core.session.data.SessionRepository
import com.project2.partner.auth.api.data.remote.api.PartnerAuthApi
import com.project2.partner.auth.api.domain.repository.PartnerAuthRepository
import com.project2.partner.auth.api.domain.usecase.ChangePasswordPartnerUseCase
import com.project2.partner.auth.api.domain.usecase.SignInPartnerUseCase
import com.project2.partner.auth.api.domain.usecase.SignOutPartnerUseCase
import com.project2.partner.auth.impl.data.repository.PartnerAuthRepositoryImpl
import com.project2.partner.auth.impl.domain.ChangePasswordPartnerUseCaseImpl
import com.project2.partner.auth.impl.domain.SignInPartnerUseCaseImpl
import com.project2.partner.auth.impl.domain.SignOutPartnerUseCaseImpl
import com.project2.partner.profile.api.domain.repository.PartnerProfileRepository
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class AuthModule {

    @Provides
    @Singleton
    fun providePartnerAuthApi(
        @Named("partner_api") okHttpClient: OkHttpClient,
        @Named("moshi_converter") moshiConverterFactory: MoshiConverterFactory
    ): PartnerAuthApi {
        val builder = Retrofit.Builder()
            .client(okHttpClient)
            .addCallAdapterFactory(ResponseCallAdapterFactory())
            .baseUrl(BuildConfig.project2_API_URL)
            .addConverterFactory(moshiConverterFactory)
            .build()

        return builder.create(PartnerAuthApi::class.java)
    }

    @Provides
    fun providePointsRepository(pointsApi: PartnerAuthApi, @IoDispatcher dispatcher: CoroutineDispatcher) : PartnerAuthRepository {
        return PartnerAuthRepositoryImpl(pointsApi, dispatcher)
    }

    @Provides
    fun provideSignInUseCases(sessionRepository: SessionRepository, authRepository: PartnerAuthRepository): SignInPartnerUseCase {
        return SignInPartnerUseCaseImpl(sessionRepository, authRepository)
    }

    @Provides
    fun provideSignOutUseCases(
        sessionRepository: SessionRepository,
        authRepository: PartnerAuthRepository,
        partnerProfileRepository: PartnerProfileRepository
    ): SignOutPartnerUseCase {
        return SignOutPartnerUseCaseImpl(sessionRepository, authRepository, partnerProfileRepository)
    }

    @Provides
    fun provideChangePassword(
        authRepository: PartnerAuthRepository
    ): ChangePasswordPartnerUseCase {
        return ChangePasswordPartnerUseCaseImpl(authRepository)
    }
}
