package com.project2.partner.auth.impl.ui.change_password

import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResult
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.dialog.BaseBottomSheetDialogFragment
import com.project2.partner.auth.impl.databinding.BottomSheetPasswordChangedBinding

class PasswordChangedBottomSheet :  BaseBottomSheetDialogFragment() {

    override val synthetic = SyntheticBinding(BottomSheetPasswordChangedBinding::inflate)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isCancelable = false
        dialog?.setCanceledOnTouchOutside(false)

        synthetic.binding.buttonOk.setOnClickListener {
            setFragmentResult(PASSWORD_CHANGED_REQUEST_KEY, Bundle())
            dismiss()
        }
    }

    companion object {
        const val TAG = "PasswordChangedBottomSheet"

        const val PASSWORD_CHANGED_REQUEST_KEY = "password_changed"
        fun show(manager: FragmentManager) {
            PasswordChangedBottomSheet().show(manager, TAG)
        }
    }
}
