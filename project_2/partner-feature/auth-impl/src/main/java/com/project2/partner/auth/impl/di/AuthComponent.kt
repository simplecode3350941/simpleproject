package com.project2.partner.auth.impl.di

import com.project2.core.presentation.di.ComponentDependenciesProviderModule
import com.project2.partner.auth.impl.ui.PartnerAuthFragment
import com.project2.partner.auth.impl.ui.change_password.PartnerChangePasswordFragment
import dagger.Component

@Component(
    modules = [ComponentDependenciesProviderModule::class],
    dependencies = [AuthDependencies::class]
)
interface AuthComponent {
    fun inject(authFragment: PartnerAuthFragment)

    fun inject(changePasswordFragment: PartnerChangePasswordFragment)
}
