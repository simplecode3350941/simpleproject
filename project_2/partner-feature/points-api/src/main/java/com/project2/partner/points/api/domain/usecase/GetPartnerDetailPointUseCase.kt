package com.project2.partner.points.api.domain.usecase

import com.project2.partner.points.api.domain.models.PointDetailDomain
import kotlinx.coroutines.flow.Flow

interface GetPartnerDetailPointUseCase {
    suspend operator fun invoke(id: String?): Flow<PointDetailDomain>
}