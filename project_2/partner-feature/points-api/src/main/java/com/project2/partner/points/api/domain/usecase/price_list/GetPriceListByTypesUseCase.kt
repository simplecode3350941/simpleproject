package com.project2.partner.points.api.domain.usecase.price_list

import com.project2.partner.points.api.domain.models.PointTypeDomain
import com.project2.partner.points.api.domain.models.PointTypeWithCategoryDomain
import kotlinx.coroutines.flow.Flow

interface GetPriceListByTypesUseCase {
    suspend operator fun invoke(pointTypes: List<PointTypeDomain>): Flow<List<PointTypeWithCategoryDomain>>
}
