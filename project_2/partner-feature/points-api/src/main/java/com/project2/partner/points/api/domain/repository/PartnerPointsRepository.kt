package com.project2.partner.points.api.domain.repository

import com.project2.partner.points.api.domain.models.CreatePointDomain
import com.project2.partner.points.api.domain.models.PointDetailDomain
import com.project2.partner.points.api.domain.models.PointShortDomain
import kotlinx.coroutines.flow.Flow

interface PartnerPointsRepository {

    suspend fun getPartnerListPoints(
        pageNumber: Int,
        pageSize: Int,
        status: String?,
        global: String?
    ): Flow<List<PointShortDomain>>
    suspend fun getPartnerDetailPoint(id: String?): Flow<PointDetailDomain>
    suspend fun hidePoint(pointId: String): Flow<PointDetailDomain>
    suspend fun activatePoint(pointId: String): Flow<PointDetailDomain>
    suspend fun createPoint(point: CreatePointDomain): Flow<Boolean>
    suspend fun updatePoint(point: CreatePointDomain, pointId: String): Flow<Boolean>
}