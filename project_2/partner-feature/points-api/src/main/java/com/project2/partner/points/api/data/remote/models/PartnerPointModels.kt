package com.project2.partner.points.api.data.remote.models

import com.project2.partner.points.api.domain.models.PointType
import com.squareup.moshi.Json

class ItemsEnvelope<T>(val content: List<T>,
                       val totalElements: Int)

class PointShortData(
    val id: String,
    val name: String,
    val address: String,
    val types: List<PointTypeData>,
    val status: String // Enum
)

class PointDetailData(
    val id: String,
    val address: String,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val phoneNumber: String,
    val photos: List<PhotoData>?,
    val schedule: List<ScheduleData>,
    val status: String,
    val pointTypes: List<TypeData>
)

class PointDetailRequest(
    val id: String?,
    val address: String,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val phoneNumber: String,
    val schedule: List<ScheduleData>,
    val status: String?,
    val pointTypes: List<TypeData>
)

class PointTypeData(
    val localizedName : String,
    val type: PointType // Enum
)

class PhotoData(
    val id: String,
    val imageURI: String
)

class ScheduleData(
    val dayOff: Boolean,
    val timeFrom: PointTimeData?,
    val timeTo: PointTimeData?,
    val workingDay: String
)

class TypeData(
    @Json(name = "staticID")
    val id: String,
    val serviceTypes: List<CategoryWithServicesData>,
    val type: PointTypeData
)

class PointTimeData(
    val hours: Int,
    val minutes: Int
)

class CategoryWithServicesData(
    @Json(name = "staticID")
    val id: String,
    val name: String,
    val services: List<ServiceData>
)

class ServiceData(
    @Json(name = "serviceID")
    val id: String,
    val name: String,
    val price: Double?
)

