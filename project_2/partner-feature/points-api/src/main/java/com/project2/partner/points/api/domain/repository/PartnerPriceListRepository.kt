package com.project2.partner.points.api.domain.repository

import com.project2.partner.points.api.domain.models.PointTypeDomain
import com.project2.partner.points.api.domain.models.PointTypeWithCategoryDomain
import kotlinx.coroutines.flow.Flow

interface PartnerPriceListRepository {
    suspend fun getPointsType(): Flow<List<PointTypeDomain>>
    suspend fun getPriceListByTypes(types: List<PointTypeDomain>): Flow<List<PointTypeWithCategoryDomain>>
    suspend fun getPriceListByPointId(pointId: String): Flow<List<PointTypeWithCategoryDomain>>
}