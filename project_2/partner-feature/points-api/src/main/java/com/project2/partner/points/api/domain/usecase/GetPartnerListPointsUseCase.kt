package com.project2.partner.points.api.domain.usecase

import com.project2.partner.points.api.domain.models.PointShortDomain
import kotlinx.coroutines.flow.Flow

interface GetPartnerListPointsUseCase {
    suspend operator fun invoke(
        pageNumber: Int,
        pageSize: Int,
        status: String? = null,
        global: String? = null
    ): Flow<List<PointShortDomain>>
}