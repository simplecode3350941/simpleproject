package com.project2.partner.points.api.domain.usecase.point

import com.project2.partner.points.api.domain.models.CreatePointDomain
import kotlinx.coroutines.flow.Flow

interface CreatePointUseCase {
    suspend operator fun invoke(point: CreatePointDomain): Flow<Boolean>
}