package com.project2.partner.points.api.data.remote.api

import com.project2.partner.points.api.data.remote.models.ItemsEnvelope
import com.project2.partner.points.api.data.remote.models.PointDetailData
import com.project2.partner.points.api.data.remote.models.PointDetailRequest
import com.project2.partner.points.api.data.remote.models.PointShortData
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Query

interface PartnerPointsApi {

    @GET("/partners/points/list")
    suspend fun getAllPoints(
        @Query("page") pageNumber: Int,
        @Query("size") pageSize: Int,
        @Query("status") status: String?,
        @Query("global") global: String?
    ): ItemsEnvelope<PointShortData>

    @GET("/partners/points/point")
    suspend fun getPoint(@Query("pointId") pointId: String?): PointDetailData

    @PUT("/partners/points/point/hide")
    suspend fun hidePoint(@Query("pointId") pointId: String): PointDetailData

    @PUT("/partners/points/point/activate")
    suspend fun activatePoint(@Query("pointId") pointId: String): PointDetailData

    @POST("/partners/points/point")
    suspend fun createPoint(@Body body: PointDetailRequest): PointDetailData

    @PUT("/partners/points/point/")
    suspend fun updatePoint(
        @Body body: PointDetailRequest,
        @Query("pointId") pointId: String
    ): PointDetailData
}