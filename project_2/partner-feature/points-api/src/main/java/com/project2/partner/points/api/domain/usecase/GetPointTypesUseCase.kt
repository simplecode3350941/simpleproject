package com.project2.partner.points.api.domain.usecase

import com.project2.partner.points.api.domain.models.PointTypeDomain
import kotlinx.coroutines.flow.Flow

interface GetPointTypesUseCase {
    suspend operator fun invoke(): Flow<List<PointTypeDomain>>
}