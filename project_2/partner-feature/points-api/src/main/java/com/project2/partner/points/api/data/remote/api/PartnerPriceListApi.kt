package com.project2.partner.points.api.data.remote.api

import com.project2.partner.points.api.data.remote.models.PointTypeData
import com.project2.partner.points.api.data.remote.models.TypeData
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface PartnerPriceListApi {

    @GET("/partners/priceList/pointTypes")
    suspend fun getPointsType(): List<PointTypeData>

    @GET("/partners/points/point/priceList/payment")
    suspend fun getPriceListForPayment(@Query("pointId") pointId: String): List<TypeData>

    @POST("/partners/priceList/pointTypes/services")
    suspend fun getPriceListByTypes(@Body types: List<PointTypeData>): List<TypeData>
}