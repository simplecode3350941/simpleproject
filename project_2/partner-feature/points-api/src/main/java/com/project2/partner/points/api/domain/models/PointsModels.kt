package com.project2.partner.points.api.domain.models

import androidx.annotation.Keep

class PointShortDomain(
    val id: String,
    val name: String,
    val address: String,
    val pointTypes: List<String>,
    val status: String // Enum
)

class PointDetailDomain(
    val id: String,
    val address: String,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val phoneNumber: String,
    val photos: List<String>, // photo url`s
    val schedule: List<ScheduleDomain>,
    val status: String,
    val types: List<PointTypeWithCategoryDomain>,
)

class CreatePointDomain(
    val id: String?,
    val address: String,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val phoneNumber: String,
    val schedule: List<ScheduleDomain>,
    val types: List<PointTypeWithCategoryDomain>,
)

class PointTypeWithCategoryDomain(
    val id: String,
    val type: PointTypeDomain,
    val categories: List<CategoryWithProductsDomain>
)

class CategoryWithProductsDomain(
    val id: String,
    val categoryName: String,
    val products: List<ProductDomain>
)

class ProductDomain(
    val id: String,
    val name: String,
    val price: Double?
)

class PointTypeDomain(
    val id: String?,
    val localizedName : String,
    val type: PointType // Enum
)

class ScheduleDomain(
    val dayOff: Boolean,
    val timeFrom: PointTimeDomain?,
    val timeTo: PointTimeDomain?,
    val workingDay: String
)

class PointTimeDomain(
    val hours: Int,
    val minutes: Int
)

@Keep
enum class PointType {
    CARWASH,
    TIREFITTING,
    CARSERVICE,
    PARKING,
    HOTEL,
    UNKNOWN
}