package com.project2.partner.points.api.ui.list.models

import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import com.project2.core.utils.common.immutableListOf
import com.project2.partner.points.api.R

val MODERATION_STATUSES = immutableListOf(
    "MODERATION_TO_CREATE",
    "MODERATION_TO_UPDATE",
    "MODERATION_ACTIVE_TO_UPDATE",
    "MODERATION_HIDDEN_TO_UPDATE",
    "MODERATION_DECLINED_TO_UPDATE",
    "MODERATION_TO_HIDE",
    "MODERATION_TO_ACTIVATE"
)

data class PointShortUi(
    val id: String,
    val name: String,
    val address: String,
    val pointTypes: List<String>,
    val formattedPointTypes: String,
    val status: PointStatus
)

enum class PointStatus(@StringRes val stringResId: Int,
                       @ColorRes val colorResId: Int) {

    ACTIVE(R.string.point_status_active, R.color.green_light2),
    DECLINED(R.string.point_status_declined, R.color.red),
    MODERATION(R.string.point_status_moderation, R.color.orange),
    HIDDEN(R.string.point_status_hidden, R.color.grey_middle)
}