package com.project2.partner.points.api.domain.models

class DadataAddressDomain(
    val id: Long,
    val latitude: Double,
    val longitude: Double,
    val addressFull: String,
    val addressShort: String,
    val cityRegionCountry: String
)