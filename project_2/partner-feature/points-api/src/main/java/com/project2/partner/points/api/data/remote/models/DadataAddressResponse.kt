package com.project2.partner.points.api.data.remote.models

class DadataAddressResponse(
    val id: Long,
    val latitude: Double,
    val longitude: Double,
    val addressFull: String,
    val addressShort: String,
    val cityRegionCountry: String
)