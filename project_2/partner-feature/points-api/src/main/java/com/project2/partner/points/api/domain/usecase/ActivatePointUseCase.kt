package com.project2.partner.points.api.domain.usecase

import com.project2.partner.points.api.domain.models.PointDetailDomain
import kotlinx.coroutines.flow.Flow

interface ActivatePointUseCase {
    suspend operator fun invoke(pointId: String): Flow<PointDetailDomain>
}