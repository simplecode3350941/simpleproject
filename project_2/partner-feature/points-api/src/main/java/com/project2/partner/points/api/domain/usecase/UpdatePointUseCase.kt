package com.project2.partner.points.api.domain.usecase

import com.project2.partner.points.api.domain.models.CreatePointDomain
import kotlinx.coroutines.flow.Flow

interface UpdatePointUseCase {
    suspend operator fun invoke(point: CreatePointDomain, pointId: String): Flow<Boolean>
}