package com.project2.partner.points.api.domain.usecase.price_list

import com.project2.partner.points.api.domain.models.PointTypeWithCategoryDomain
import kotlinx.coroutines.flow.Flow

interface GetPriceListForPaymentUseCase {
    suspend operator fun invoke(pointId: String): Flow<List<PointTypeWithCategoryDomain>>
}