package com.project2.partner.points.api.domain.usecase

import com.project2.partner.points.api.domain.models.DadataAddressDomain
import kotlinx.coroutines.flow.Flow

interface GetDadataAddressUseCase {
    suspend operator fun invoke(query: String): Flow<List<DadataAddressDomain>>
}