package com.project2.partner.points.api.data.remote.api

import com.project2.partner.points.api.data.remote.models.DadataAddressResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface PartnerDadataApi {

    @GET("/partners/dadata/address/{query}")
    suspend fun getAddress(@Path("query") query: String): List<DadataAddressResponse>
}