package com.project2.partner.points.api.domain.repository

import com.project2.partner.points.api.domain.models.DadataAddressDomain
import kotlinx.coroutines.flow.Flow

interface PartnerDadataRepository {
    suspend fun getAddress(query: String): Flow<List<DadataAddressDomain>>
}