package com.project2.partner.contract.impl.data.mapper

import com.project2.partner.contract.api.data.remote.models.EmployeeDto
import com.project2.partner.contract.api.data.remote.models.EmployeePoint
import com.project2.partner.contract.api.domain.models.EmployeeDomain
import com.project2.partner.contract.api.domain.models.EmployeePointDomain

fun EmployeeDto.toDomain() = EmployeeDomain(
    firstName = firstName,
    middleName = middleName,
    lastName = lastName,
    email = email,
    role = role,
    point = point.toDomain()
)

fun EmployeePoint.toDomain() = EmployeePointDomain(
    id = id,
    name = name
)