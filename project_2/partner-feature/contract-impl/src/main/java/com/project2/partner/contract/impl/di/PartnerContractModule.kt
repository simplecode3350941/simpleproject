package com.project2.partner.contract.impl.di

import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.core.network.BuildConfig
import com.project2.partner.contract.api.data.remote.api.PartnerContractApi
import com.project2.partner.contract.api.domain.repository.PartnerContractRepository
import com.project2.partner.contract.api.domain.usecase.GetPartnerContractUseCase
import com.project2.partner.contract.impl.data.repository.PartnerContractRepositoryImpl
import com.project2.partner.contract.impl.domain.GetPartnerContractUseCaseImpl
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class PartnerContractModule {

    @Provides
    @Singleton
    fun providePartnerContractApi(
        @Named("partner_api") okHttpClient: OkHttpClient,
        @Named("moshi_converter") moshiConverterFactory: MoshiConverterFactory
    ): PartnerContractApi {
        val builder = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.project2_API_URL)
            .addConverterFactory(moshiConverterFactory)
            .build()

        return builder.create(PartnerContractApi::class.java)
    }

    @Provides
    fun providePartnerContractRepository(
        contractApi: PartnerContractApi,
        @IoDispatcher dispatcher: CoroutineDispatcher
    ): PartnerContractRepository {
        return PartnerContractRepositoryImpl(contractApi, dispatcher)
    }

    @Provides
    fun provideGetPartnerContractUseCase(repository: PartnerContractRepository): GetPartnerContractUseCase {
        return GetPartnerContractUseCaseImpl(repository)
    }
}