package com.project2.partner.contract.impl.ui.di.employees

import com.project2.core.presentation.di.ComponentDependencies
import com.project2.partner.contract.api.data.remote.api.PartnerEmployeesApi
import com.project2.partner.contract.api.domain.repository.PartnerEmployeesRepository
import com.project2.partner.contract.api.domain.usecase.ChangePasswordEmployeeUseCase
import com.project2.partner.contract.api.domain.usecase.DeleteEmployeeUseCase
import com.project2.partner.contract.api.domain.usecase.SaveEmployeeUseCase
import com.project2.partner.contract.api.domain.usecase.GetEmployeeDetailUseCase
import com.project2.partner.contract.api.domain.usecase.GetEmployeesUseCase
import com.project2.partner.contract.api.domain.usecase.UpdateEmployeeUseCase
import com.project2.partner.contract.impl.ui.employees.edit.PartnerEmployeeEditViewModel

interface PartnerEmployeesDependencies: ComponentDependencies {
    fun employeesApi(): PartnerEmployeesApi

    fun employeesRepository(): PartnerEmployeesRepository

    fun getEmployeesUseCase(): GetEmployeesUseCase

    fun getEmployeesDetailUseCase(): GetEmployeeDetailUseCase

    fun saveEmployeeUseCase(): SaveEmployeeUseCase

    fun updateEmployeeUseCase(): UpdateEmployeeUseCase

    fun deleteEmployeeUseCase(): DeleteEmployeeUseCase

    fun changePasswordEmployeeUseCase(): ChangePasswordEmployeeUseCase

    fun getPartnerEmployeeEditViewModelFactory(): PartnerEmployeeEditViewModel.Factory
}