package com.project2.partner.contract.impl.ui.employees.change_password

import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.partner.contract.api.domain.usecase.ChangePasswordEmployeeUseCase
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

class PartnerEmployeeChangePasswordConfirmViewModel @Inject constructor(
    val changePasswordEmployee: ChangePasswordEmployeeUseCase,
    override val exceptionHandler: CoroutineExceptionHandler
) : BaseViewModel() {

    private val _updateState = MutableStateFlow<UiState?>(null)
    val updateState = _updateState.asStateFlow().filterNotNull()

    fun onChangePasswordEmployee(email: String) {
        launchOnViewModelScope {
            changePasswordEmployee(email)
                .onStart {
                    _updateState.value = UiState.Loading
                }
                .catch {
                    _updateState.value = UiState.Error(it)
                }
                .collectLatest {
                    _updateState.value = UiState.Success
                }
        }
    }
}

sealed interface UiState {
    object Loading : UiState
    object Success : UiState
    data class Error(val error: Throwable) : UiState
}