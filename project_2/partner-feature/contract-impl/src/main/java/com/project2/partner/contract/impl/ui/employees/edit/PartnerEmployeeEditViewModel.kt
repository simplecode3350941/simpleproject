package com.project2.partner.contract.impl.ui.employees.edit

import androidx.lifecycle.SavedStateHandle
import com.project2.core.presentation.di.assisted.AssistedViewModelFactory
import com.project2.partner.contract.api.domain.usecase.GetEmployeeDetailUseCase
import com.project2.partner.contract.api.domain.usecase.UpdateEmployeeUseCase
import com.project2.partner.contract.impl.ui.employees.create.PartnerEmployeeAbsCreateViewModel
import com.project2.partner.contract.impl.ui.employees.create.UiState
import com.project2.partner.contract.impl.ui.employees.mapper.toDomain
import com.project2.partner.contract.impl.ui.employees.mapper.toPresentation
import com.project2.partner.contract.impl.ui.employees.model.PartnerEmployeeUi
import com.project2.partner.point.selection.ui.select_service.communication.PointSelectionCommunication
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import timber.log.Timber

class PartnerEmployeeEditViewModel @AssistedInject constructor(
    private val updateEmployee: UpdateEmployeeUseCase,
    override val exceptionHandler: CoroutineExceptionHandler,
    private val getEmployeeDetailInfo: GetEmployeeDetailUseCase,
    @Assisted private val savedStateHandle: SavedStateHandle
) : PartnerEmployeeAbsCreateViewModel() {

    @AssistedFactory
    interface Factory : AssistedViewModelFactory<PartnerEmployeeEditViewModel>

    private val employeeEmail: String
        get() = savedStateHandle[PARTNER_EMPLOYEE_EMAIL] ?: error("Employee email is null")

    private val _employeeDetail = MutableSharedFlow<PartnerEmployeeUi>()
    val employeeDetail = _employeeDetail.asSharedFlow()

    private val _employeeUiState = MutableStateFlow<EmployeeUiState>(EmployeeUiState.Loading)
    val employeeUiState = _employeeUiState.asStateFlow()

    init {
        getEmployee(employeeEmail)
    }

    override fun onSaveEmployee(employee: PartnerEmployeeUi) {
        launchOnViewModelScope {
            updateEmployee(employee.toDomain())
                .onStart { updateState(UiState.Loading) }
                .catch { updateState(UiState.Error(it)) }
                .collectLatest {
                    updateState(UiState.Success)
                }
        }
    }

    private fun getEmployee(email: String) {
        launchOnViewModelScope {
            getEmployeeDetailInfo(email)
                .onStart { _employeeUiState.value = EmployeeUiState.Loading }
                .map { it.toPresentation() }
                .catch { _employeeUiState.value = EmployeeUiState.Error(it) }
                .collect {
                    Timber.d("collect - %s", it)
                    _employeeDetail.emit(it)
                    _employeeUiState.value = EmployeeUiState.Success
                    PointSelectionCommunication.setPointSelectedIdAndName(it.point.id, it.point.name)
                }
        }
    }

    companion object {
        const val PARTNER_EMPLOYEE_EMAIL = "partner_employee_email"
    }
}

sealed interface EmployeeUiState {
    object Loading : EmployeeUiState
    object Success : EmployeeUiState
    data class Error(val error: Throwable) : EmployeeUiState
}