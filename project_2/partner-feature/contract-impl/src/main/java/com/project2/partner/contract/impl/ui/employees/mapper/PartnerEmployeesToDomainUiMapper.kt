package com.project2.partner.contract.impl.ui.employees.mapper

import com.project2.partner.contract.api.domain.models.EmployeeDomain
import com.project2.partner.contract.api.domain.models.EmployeePointDomain
import com.project2.partner.contract.impl.ui.employees.model.PartnerEmployeePointUi
import com.project2.partner.contract.impl.ui.employees.model.PartnerEmployeeUi

fun PartnerEmployeeUi.toDomain() = EmployeeDomain(
    firstName = firstName,
    middleName = middleName,
    lastName = lastName,
    email = email,
    role = role,
    point = point.toDomain()
)

private fun PartnerEmployeePointUi.toDomain() = EmployeePointDomain(
    id = id,
    name = name
)