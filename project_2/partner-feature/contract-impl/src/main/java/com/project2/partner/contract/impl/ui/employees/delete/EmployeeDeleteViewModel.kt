package com.project2.partner.contract.impl.ui.employees.delete

import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.partner.contract.api.domain.usecase.DeleteEmployeeUseCase
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

class EmployeeDeleteViewModel @Inject constructor(
    val deleteEmployee: DeleteEmployeeUseCase,
    override val exceptionHandler: CoroutineExceptionHandler
) : BaseViewModel() {

    private val _updateState = MutableStateFlow<UiDeleteState?>(null)
    val updateState = _updateState.asStateFlow().filterNotNull()

    fun onDeleteEmployee(email: String) {
        launchOnViewModelScope {
            deleteEmployee(email)
                .onStart {
                    _updateState.value = UiDeleteState.Loading
                }
                .catch {
                    _updateState.value = UiDeleteState.Error(it)
                }
                .collectLatest {
                    _updateState.value = UiDeleteState.Success
                }
        }
    }
}

sealed interface UiDeleteState {
    object Loading : UiDeleteState
    object Success : UiDeleteState
    object Empty : UiDeleteState
    data class Error(val error: Throwable) : UiDeleteState
}