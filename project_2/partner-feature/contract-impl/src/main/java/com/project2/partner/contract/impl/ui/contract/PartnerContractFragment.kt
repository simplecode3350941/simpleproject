package com.project2.partner.contract.impl.ui.contract

import android.os.Bundle
import androidx.core.net.toUri
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.facebook.shimmer.ShimmerFrameLayout
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.ComponentDependenciesProvider
import com.project2.core.presentation.di.HasComponentDependencies
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.partner.contract.impl.R
import com.project2.partner.contract.impl.databinding.FragmentPartnerContractBinding
import com.project2.partner.contract.impl.ui.contract.models.ContractWithProfileUi
import com.project2.partner.contract.impl.ui.di.contract.DaggerPartnerContractComponent
import com.project2.partner.profile.api.data.remote.models.PartnerRole
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class PartnerContractFragment: BaseMvvmFragment(), HasComponentDependencies {

    @Inject
    override lateinit var dependencies: ComponentDependenciesProvider

    private val viewModel by viewModels<PartnerContractViewModel> {
        viewModelFactory
    }

    override fun inject() {
        DaggerPartnerContractComponent.builder()
            .partnerContractDependencies(findComponentDependencies())
            .build()
            .inject(this)
    }

    override fun getLayoutResId(): Int = R.layout.fragment_partner_contract

    override val synthetic = SyntheticBinding(FragmentPartnerContractBinding::inflate)

    override fun initUi(savedInstanceState: Bundle?) {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.updateState.collectLatest(::bindUiStates)
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.contractFlow.collectLatest(::showContract)
        }

        synthetic.binding.layoutEmployeesAction.setOnClickListener {
            onEmployeesClick()
        }

        synthetic.binding.layoutTransactionsAction.setOnClickListener {
            onTransactionsClick()
        }

        synthetic.binding.swipeContractLayout.setOnRefreshListener {
            viewModel.getContract()
        }

        synthetic.binding.includeErrorViewContract.buttonRefresh.setOnClickListener {
            viewModel.getContract()
        }
    }

    private fun bindUiStates(uiState: UiState) {
        Timber.d("UiState - %s", uiState)
        val shimmer = synthetic.binding.includeLoadingView.shimmerView
        val swipeRefreshLayout = synthetic.binding.swipeContractLayout
        val contentLayout = synthetic.binding.layoutContent
        val errorView = synthetic.binding.includeErrorViewContract.errorView

        val isVisibleShimmer = uiState == UiState.Loading
        if (uiState != UiState.Loading && swipeRefreshLayout.isRefreshing) {
            swipeRefreshLayout.isRefreshing = false
        }
        shimmer.setVisibleLoading(isVisibleShimmer)
        errorView.isVisible = uiState is UiState.Error
        contentLayout.isVisible = uiState == UiState.Success
    }

    private fun showContract(data: ContractWithProfileUi) = with(synthetic.binding) {
        val contract = data.contractUi
        val profile = data.profileUi
        val point = contract.point

        textViewTitle.text = point?.name ?: contract.organizationName
        textViewSubtitle.text = point?.address ?: contract.contractNumber

        textViewDailyRevenueValue.text = contract.dailyRevenue
        textViewMonthlyRevenueValue.text = contract.monthlyRevenue

        textViewDailyRevenueTitle.text = when(profile.role) {
            PartnerRole.OWNER -> getString(R.string.partner_contract_daily_revenue_all_services_title)
            PartnerRole.OPERATOR -> getString(R.string.partner_contract_daily_revenue_title)
        }

        textViewMonthlyRevenueTitle.text = when(profile.role) {
            PartnerRole.OWNER -> getString(R.string.partner_contract_monthly_revenue_all_services_title)
            PartnerRole.OPERATOR -> getString(R.string.partner_contract_monthly_revenue_title)
        }

        layoutEmployeesAction.isVisible = profile.role == PartnerRole.OWNER
    }

    private fun onEmployeesClick() {
        val employeesFlowUri = getString(R.string.partner_employees_navigation_deeplink).toUri()
        findNavController().navigate(employeesFlowUri)
    }

    private fun onTransactionsClick() {
        val employeesFlowUri = getString(R.string.partner_transactions_navigation_deeplink).toUri()
        findNavController().navigate(employeesFlowUri)
    }

    private fun ShimmerFrameLayout.setVisibleLoading(isVisibleLoading: Boolean) {
        if(isVisibleLoading) {
            showShimmer(true)
        } else {
            hideShimmer()
        }
        isVisible = isVisibleLoading
    }
}