package com.project2.partner.contract.impl.ui.employees.list

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.ComponentDependenciesProvider
import com.project2.core.presentation.di.HasComponentDependencies
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.presentation.paging.FooterAdapter
import com.project2.core.presentation.paging.addOnInfiniteScrollListener
import com.project2.core.utils.common.lazyUnsafe
import com.project2.core.utils.view.setOnClickDelayListener
import com.project2.partner.contract.impl.R
import com.project2.partner.contract.impl.databinding.FragmentPartnerEmployeesBinding
import com.project2.partner.contract.impl.ui.di.employees.DaggerPartnerEmployeesComponent
import com.project2.partner.contract.impl.ui.employees.list.adapter.PartnerEmployeesAdapter
import com.project2.partner.contract.impl.ui.employees.model.PartnerEmployeeUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class PartnerEmployeesFragment : BaseMvvmFragment(), HasComponentDependencies {

    @Inject
    override lateinit var dependencies: ComponentDependenciesProvider

    override val synthetic = SyntheticBinding(FragmentPartnerEmployeesBinding::inflate)

    private val viewModel by viewModels<PartnerEmployeesViewModel> {
        viewModelFactory
    }

    private val employeesAdapter: PartnerEmployeesAdapter by lazyUnsafe {
        PartnerEmployeesAdapter(onEmployeeClick)
    }

    private val footerAdapter: FooterAdapter by lazyUnsafe {
        FooterAdapter(onEndReached)
    }

    private val onEndReached: () -> Unit = { viewModel.onPaginate() }

    override fun getLayoutResId(): Int = R.layout.fragment_partner_employees

    override fun inject() {
        DaggerPartnerEmployeesComponent.builder()
            .partnerEmployeesDependencies(findComponentDependencies())
            .build()
            .inject(this)
    }

    override fun initUi(savedInstanceState: Bundle?) {
        synthetic.binding.topbarPartnerEmployees.textViewNavbarTitle.text =
            getString(R.string.partner_employees_title)

        synthetic.binding.topbarPartnerEmployees.imgViewArrowBack.setOnClickListener {
            popBack()
        }

        synthetic.binding.rvEmployees.apply {
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = employeesAdapter.withLoadStateFooter(footerAdapter)
            setHasFixedSize(true)
            clearOnScrollListeners()
            addOnInfiniteScrollListener(onEndReached)
        }

        synthetic.binding.swipeEmployeesLayout.apply {
            setOnRefreshListener {
                viewModel.onRefreshData()
                synthetic.binding.swipeEmployeesLayout.isRefreshing = false
            }
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.updateState.collectLatest(::updateContentState)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.paginationState.collectLatest(::updatePaginationState)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.employeesFlow.collectLatest(::submitEmployees)
        }

        synthetic.binding.floatButtonCreateEmployee.setOnClickDelayListener {
            findNavController().navigate(R.id.action_partner_employees_to_partner_employee_create)
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.onRefreshData()
    }
    private val onEmployeeClick: (String) -> Unit = { email ->
        findNavController().navigate(
            R.id.partner_employee_detail,
            bundleOf(PARTNER_EMPLOYEE_EMAIL to email)
        )
    }

    private fun updateContentState(uiState: UiState) {
        Timber.d("updateContentState: $uiState")
        when (uiState) {
            is UiState.Loading -> {
                showHideLoading(true)
            }
            is UiState.Error -> {
                Timber.e(uiState.error)
                showHideLoading(false)
                showErrorView(uiState.error.localizedMessage)
            }

            is UiState.Empty -> {
                showHideLoading(false)
                showEmptyView()
            }

            is UiState.Success -> {
                showHideLoading(false)
            }
        }
    }

    private fun updatePaginationState(paginationState: PaginationState) {
        Timber.d("updateEmployeesState: $paginationState")
        val swipeRefreshLayout = synthetic.binding.swipeEmployeesLayout

        when (paginationState) {
            is PaginationState.Loading -> footerAdapter.loadState = LoadState.Loading
            is PaginationState.Error -> {
                swipeRefreshLayout.isRefreshing = false
                footerAdapter.loadState = LoadState.Error(paginationState.error)
            }

            is PaginationState.Success -> {
                swipeRefreshLayout.isRefreshing = false
                footerAdapter.loadState = LoadState.NotLoading(false)
            }
        }
    }

    private fun showHideLoading(show: Boolean) {
        val shimmer = synthetic.binding.includeEmployeesLoadingView.shimmerView
        val emptyView = synthetic.binding.includeEmptyView.employeersEmptyView
        val swipeRefreshLayout = synthetic.binding.swipeEmployeesLayout
        val recyclerView = synthetic.binding.rvEmployees
        val buttonCreate = synthetic.binding.floatButtonCreateEmployee

        if (show) {
            emptyView.isVisible = false
            shimmer.showShimmer(true)
        } else {
            swipeRefreshLayout.isRefreshing = false
            shimmer.hideShimmer()
        }
        recyclerView.isVisible = !show
        buttonCreate.isVisible = !show
        shimmer.isVisible = show
    }

    private fun showErrorView(error: String?) {
        val errorView = synthetic.binding.includeErrorView.errorView
        if (!error.isNullOrEmpty()) {
            synthetic.binding.includeErrorView.textViewErrorMessage.text = error
        }
        errorView.isVisible = true
        synthetic.binding.rvEmployees.isVisible = false
        synthetic.binding.includeErrorView.buttonRefresh.setOnClickListener {
            errorView.isVisible = false
            viewModel.onRefreshData()
        }
    }

    private fun showEmptyView() {
        val emptyView = synthetic.binding.includeEmptyView.employeersEmptyView
        emptyView.isVisible = true
        synthetic.binding.rvEmployees.isVisible = false
    }

    private fun submitEmployees(employees: List<PartnerEmployeeUi>) {
        Timber.d("showEmployees size: ${employees.size}")
        employeesAdapter.submitData(viewLifecycleOwner.lifecycle, PagingData.from(employees))
    }

    companion object{
        const val PARTNER_EMPLOYEE_EMAIL = "partner_employee_email"
    }
}