package com.project2.partner.contract.impl.ui.transaction.list

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.ComponentDependenciesProvider
import com.project2.core.presentation.di.HasComponentDependencies
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.presentation.paging.FooterAdapter
import com.project2.core.presentation.paging.addOnInfiniteScrollListener
import com.project2.core.utils.common.lazyUnsafe
import com.project2.partner.contract.impl.R
import com.project2.partner.contract.impl.databinding.FragmentPartnerTransactionsBinding
import com.project2.partner.contract.impl.ui.di.transaction.DaggerPartnerTransactionsComponent
import com.project2.partner.contract.impl.ui.transaction.detail.PartnerTransactionDetailBottomSheet
import com.project2.partner.contract.impl.ui.transaction.list.adapter.PartnerTransactionsAdapter
import com.project2.partner.contract.impl.ui.transaction.model.PartnerTransactionUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class PartnerTransactionsFragment : BaseMvvmFragment(), HasComponentDependencies {

    @Inject
    override lateinit var dependencies: ComponentDependenciesProvider

    override val synthetic = SyntheticBinding(FragmentPartnerTransactionsBinding::inflate)

    private val viewModel by viewModels<PartnerTransactionsViewModel> {
        viewModelFactory
    }

    private val transactionsAdapter: PartnerTransactionsAdapter by lazyUnsafe {
        PartnerTransactionsAdapter(::onTransactionClick)
    }

    private val footerAdapter: FooterAdapter by lazyUnsafe {
        FooterAdapter(onEndReached)
    }

    private val onEndReached: () -> Unit = { viewModel.onPaginate() }

    override fun getLayoutResId(): Int = R.layout.fragment_partner_transactions

    override fun inject() {
        DaggerPartnerTransactionsComponent.builder()
            .partnerTransactionsDependencies(findComponentDependencies())
            .build()
            .inject(this)
    }

    override fun initUi(savedInstanceState: Bundle?) {
        synthetic.binding.topbarPartnerTransaction.textViewNavbarTitle.text =
            getString(R.string.partner_transactions_title)

        synthetic.binding.topbarPartnerTransaction.imgViewArrowBack.setOnClickListener {
            popBack()
        }

        synthetic.binding.rvTransaction.apply {
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = transactionsAdapter.withLoadStateFooter(footerAdapter)
            setHasFixedSize(true)
            clearOnScrollListeners()
            addOnInfiniteScrollListener(onEndReached)
        }

        synthetic.binding.swipeTransactionLayout.apply {
            setOnRefreshListener {
                viewModel.onRefreshData()
                synthetic.binding.swipeTransactionLayout.isRefreshing = false
            }
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.updateState.collectLatest(::updateContentState)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.paginationState.collectLatest(::updatePaginationState)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.transactionsFlow.collectLatest(::submitTransactions)
        }
    }

    private fun onTransactionClick(id: String) {
        PartnerTransactionDetailBottomSheet.show(childFragmentManager, id)
    }

    private fun updateContentState(uiState: UiState) {
        Timber.d("updateContentState $uiState")
        when (uiState) {
            is UiState.Loading -> showHideLoading(true)
            is UiState.Error -> {
                Timber.e(uiState.error)
                showHideLoading(false)
                showErrorView(uiState.error.localizedMessage)
            }

            is UiState.Empty -> {
                showHideLoading(false)
                showEmptyView()
            }

            is UiState.Success -> {
                showHideLoading(false)
            }
        }
    }

    private fun updatePaginationState(paginationState: PaginationState) {
        Timber.d("updatePaginationState %s", paginationState)
        val swipeRefreshLayout = synthetic.binding.swipeTransactionLayout

        when (paginationState) {
            is PaginationState.Loading -> footerAdapter.loadState = LoadState.Loading
            is PaginationState.Error -> {
                swipeRefreshLayout.isRefreshing = false
                footerAdapter.loadState = LoadState.Error(paginationState.error)
            }

            is PaginationState.Success -> {
                swipeRefreshLayout.isRefreshing = false
                footerAdapter.loadState = LoadState.NotLoading(false)
            }
        }
    }

    private fun showHideLoading(show: Boolean) {
        val shimmer = synthetic.binding.includeTransactionLoadingView.shimmerViewTransaction
        val emptyView = synthetic.binding.includeEmptyView.transactionEmptyView
        val swipeRefreshLayout = synthetic.binding.swipeTransactionLayout
        val recyclerView = synthetic.binding.rvTransaction

        if (show) {
            emptyView.isVisible = false
            shimmer.showShimmer(true)
        } else {
            swipeRefreshLayout.isRefreshing = false
            shimmer.hideShimmer()
        }
        recyclerView.isVisible = !show
        shimmer.isVisible = show
    }

    private fun showErrorView(error: String?) {
        val errorView = synthetic.binding.includeErrorView.errorView
        if (!error.isNullOrEmpty()) {
            synthetic.binding.includeErrorView.textViewErrorMessage.text = error
        }
        errorView.isVisible = true
        synthetic.binding.rvTransaction.isVisible = false
        synthetic.binding.includeErrorView.buttonRefresh.setOnClickListener {
            errorView.isVisible = false
            viewModel.onRefreshData()
        }
    }

    private fun showEmptyView() {
        val emptyView = synthetic.binding.includeEmptyView.transactionEmptyView
        emptyView.isVisible = true
        synthetic.binding.rvTransaction.isVisible = false
    }

    private fun submitTransactions(transactions: List<PartnerTransactionUi>) {
        Timber.d("showPoints size ${transactions.size}")
        transactionsAdapter.submitData(viewLifecycleOwner.lifecycle, PagingData.from(transactions))
    }
}