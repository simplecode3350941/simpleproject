package com.project2.partner.contract.impl.domain

import com.project2.partner.contract.api.domain.models.PartnerContractDomain
import com.project2.partner.contract.api.domain.repository.PartnerContractRepository
import com.project2.partner.contract.api.domain.usecase.GetPartnerContractUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPartnerContractUseCaseImpl @Inject constructor(private val repository: PartnerContractRepository):
    GetPartnerContractUseCase {

    override operator fun invoke(): Flow<PartnerContractDomain> {
        return repository.getContract()
    }
}