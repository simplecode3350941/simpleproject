package com.project2.partner.contract.impl.ui.employees.create

import com.project2.partner.contract.api.domain.usecase.SaveEmployeeUseCase
import com.project2.partner.contract.impl.ui.employees.mapper.toDomain
import com.project2.partner.contract.impl.ui.employees.model.PartnerEmployeeUi
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

class PartnerEmployeeCreateViewModel @Inject constructor(
    private val saveEmployee: SaveEmployeeUseCase,
    override val exceptionHandler: CoroutineExceptionHandler
) : PartnerEmployeeAbsCreateViewModel() {

    private val _employeeSavedEmail = MutableStateFlow<String?>(null)
    val employeeSavedEmail = _employeeSavedEmail.asStateFlow().filterNotNull()

    override fun onSaveEmployee(employee: PartnerEmployeeUi) {
        launchOnViewModelScope {
            saveEmployee(employee.toDomain())
                .onStart { updateState(UiState.Loading) }
                .catch { updateState(UiState.Error(it)) }
                .collectLatest {
                    _employeeSavedEmail.value = it
                    updateState(UiState.Success)
                }
        }
    }
}