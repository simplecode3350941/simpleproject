package com.project2.partner.contract.impl.di

import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.core.network.BuildConfig
import com.project2.partner.contract.api.data.remote.api.PartnerTransactionsApi
import com.project2.partner.contract.api.domain.repository.PartnerTransactionsRepository
import com.project2.partner.contract.api.domain.usecase.GetPartnerTransactionDetailUseCase
import com.project2.partner.contract.api.domain.usecase.GetPartnerTransactionsUseCase
import com.project2.partner.contract.impl.data.repository.PartnerTransactionsRepositoryImpl
import com.project2.partner.contract.impl.domain.GetPartnerTransactionDetailUseCaseImpl
import com.project2.partner.contract.impl.domain.GetPartnerTransactionsUseCaseImpl
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class PartnerTransactionsModule {
    @Provides
    @Singleton
    fun providePartnerTransactionsApi(
        @Named("partner_api") okHttpClient: OkHttpClient,
        @Named("moshi_converter") moshiConverterFactory: MoshiConverterFactory
    ): PartnerTransactionsApi {
        val builder = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.project2_API_URL)
            .addConverterFactory(moshiConverterFactory)
            .build()

        return builder.create(PartnerTransactionsApi::class.java)
    }

    @Provides
    fun provideTransactionsRepository(
        transitionApi: PartnerTransactionsApi,
        @IoDispatcher dispatcher: CoroutineDispatcher
    ): PartnerTransactionsRepository {
        return PartnerTransactionsRepositoryImpl(transitionApi, dispatcher)
    }

    @Provides
    fun provideGetTransactionsListUseCase(
        repository: PartnerTransactionsRepository
    ): GetPartnerTransactionsUseCase{
        return GetPartnerTransactionsUseCaseImpl(repository)
    }

    @Provides
    fun provideGetTransactionsDetailInfoUseCase(
        repository: PartnerTransactionsRepository
    ): GetPartnerTransactionDetailUseCase {
        return GetPartnerTransactionDetailUseCaseImpl(repository)
    }
}