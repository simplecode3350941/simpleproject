package com.project2.partner.contract.impl.ui.employees.detail

import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.ComponentDependenciesProvider
import com.project2.core.presentation.di.HasComponentDependencies
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.utils.common.SystemUtils.sendEmail
import com.project2.partner.contract.impl.R
import com.project2.partner.contract.impl.databinding.FragmentPartnerEmployeeDetailBinding
import com.project2.partner.contract.impl.ui.di.employees.DaggerPartnerEmployeesComponent
import com.project2.partner.contract.impl.ui.employees.change_password.PartnerEmployeeChangePasswordConfirmBottomSheet
import com.project2.partner.contract.impl.ui.employees.model.PartnerEmployeeUi
import com.project2.partner.contract.impl.ui.employees.model.fullName
import com.project2.partner.contract.impl.ui.employees.model.getRole
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class PartnerEmployeeDetailInfoFragment: BaseMvvmFragment(), HasComponentDependencies {

    @Inject
    override lateinit var dependencies: ComponentDependenciesProvider

    override val synthetic = SyntheticBinding(FragmentPartnerEmployeeDetailBinding::inflate)

    private val viewModel by viewModels<PartnerEmployeeDetailInfoViewModel> {
        viewModelFactory
    }

    private val employeeEmail: String
        get() {
            return requireArguments().getString(PARTNER_EMPLOYEE_EMAIL).toString()
        }

    override fun inject() {
        DaggerPartnerEmployeesComponent.builder()
            .partnerEmployeesDependencies(findComponentDependencies())
            .build()
            .inject(this)
    }

    override fun getLayoutResId(): Int = R.layout.fragment_partner_employee_detail

    override fun initUi(savedInstanceState: Bundle?) {

        synthetic.binding.topbarPartnerEmployees.textViewNavbarTitle.text =
            getString(R.string.partner_employee_title)

        synthetic.binding.topbarPartnerEmployees.imgViewArrowBack.setOnClickListener {
            findNavController().popBackStack()
        }

        synthetic.binding.topbarPartnerEmployees.textViewRight.isVisible = true
        synthetic.binding.topbarPartnerEmployees.textViewRight.text =
            getString(R.string.partner_employees_edit)

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.updateState.collectLatest(::bindUiStates)
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.employee.collectLatest(::showEmployee)
        }
        synthetic.binding.textEmployeeDetailEmail.setOnClickListener {
            sendEmail(synthetic.binding.textEmployeeDetailEmail.text.toString())
        }

        synthetic.binding.textEmployeeChangePassword.setOnClickListener {
            PartnerEmployeeChangePasswordConfirmBottomSheet.show(
                childFragmentManager, employeeEmail)
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.onLoadEmployeeInfo(employeeEmail)
    }

    private fun bindUiStates(uiState: UiState) {
        Timber.d("Employees UiState - $uiState")
        when (uiState) {
            is UiState.Loading -> showHideLoading(true)
            is UiState.Error -> {
                Timber.e(uiState.error)
                showHideLoading(false)
                showErrorView(uiState.error.localizedMessage)
            }
            is UiState.Success -> {
                showHideLoading(false)
            }
        }
    }

    private fun showHideLoading(show: Boolean) {
        val shimmer = synthetic.binding.includeEmployeeDetailLoadingView.shimmerView
        if(show) {
            shimmer.showShimmer(true)
        } else {
            shimmer.hideShimmer()

        }
        synthetic.binding.contentLayoutEmployeeDetail.isVisible = !show
        shimmer.isVisible = show

    }

    private fun showErrorView(error: String?) {
        val errorView = synthetic.binding.includeErrorView.errorView
        if (!error.isNullOrEmpty()) {
            synthetic.binding.includeErrorView.textViewErrorMessage.text = error
        }
        errorView.isVisible = true
        synthetic.binding.contentLayoutEmployeeDetail.isVisible = false
        synthetic.binding.includeErrorView.buttonRefresh.setOnClickListener {
            errorView.isVisible = false
            viewModel.onLoadEmployeeInfo(employeeEmail)
        }
    }

    private fun showEmployee(employee: PartnerEmployeeUi) {
        synthetic.binding.textEmployeeDetailFio.text = employee.fullName
        synthetic.binding.textEmployeeDetailRoleValue.text = employee.getRole(requireContext())
        synthetic.binding.textEmployeeDetailServiceValue.text = employee.point.name
        synthetic.binding.textEmployeeDetailEmail.text = employee.email

        synthetic.binding.topbarPartnerEmployees.textViewRight.setOnClickListener {
            findNavController().navigate(
                R.id.action_employee_detail_to_employee_edit,
                bundleOf(
                    PARTNER_EMPLOYEE_EMAIL to employeeEmail
                )
            )
        }
    }

    companion object{
        const val PARTNER_EMPLOYEE_EMAIL = "partner_employee_email"
    }
}