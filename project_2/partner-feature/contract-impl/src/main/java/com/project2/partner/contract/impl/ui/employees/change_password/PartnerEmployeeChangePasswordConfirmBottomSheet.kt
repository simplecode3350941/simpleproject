package com.project2.partner.contract.impl.ui.employees.change_password

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.dialog.BaseMvvmBottomSheetDialogFragment
import com.project2.partner.contract.impl.R
import com.project2.partner.contract.impl.databinding.PartnerEmoloyeeChangePasswordConfirmBottomSheetBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

class PartnerEmployeeChangePasswordConfirmBottomSheet: BaseMvvmBottomSheetDialogFragment() {

    override val synthetic =
        SyntheticBinding(PartnerEmoloyeeChangePasswordConfirmBottomSheetBinding::inflate)

    private val viewModel by viewModels<PartnerEmployeeChangePasswordConfirmViewModel> {
        viewModelFactory
    }

    private var currentFragmentManager: FragmentManager? = null

    private val employeeEmail: String
        get() = requireNotNull(arguments?.getString(PARTNER_EMPLOYEE_EMAIL))

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        dialog.setOnShowListener {
            (dialog.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout?)?.let {
                BottomSheetBehavior.from(it).state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        synthetic.binding.buttonChangePasswordEmployeeCancel.setOnClickListener {
            dismiss()
        }

        synthetic.binding.buttonChangePasswordEmployeeOk.setOnClickListener {
            viewModel.onChangePasswordEmployee(employeeEmail)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.updateState.collectLatest(::bindUiStates)
        }
    }
    private fun showToast(messages: String) {
        Toast.makeText(requireContext(), messages, Toast.LENGTH_LONG).show()
    }

    private fun bindUiStates(uiState: UiState) {
        when(uiState) {
            is UiState.Loading ->
                showHideLoading(true)
            is UiState.Error -> {
                Timber.e(uiState.error)
                showHideLoading(false)
                uiState.error.localizedMessage?.let { showToast(it) }
            }
            is UiState.Success -> {
                showHideLoading(false)
                currentFragmentManager?.let {
                    PartnerEmployeeChangePasswordSuccessfulBottomSheet.show(it, employeeEmail)
                }
            }
        }
    }

    private fun showHideLoading(show: Boolean) {
        synthetic.binding.layoutChangePasswordProgressBar.isVisible = show
    }

    companion object {
        const val TAG = "EMPLOYEE_CHANGE_PASSWORD_BOTTOM_SHEET"
        private const val PARTNER_EMPLOYEE_EMAIL = "PARTNER_EMPLOYEE_EMAIL"

        fun show(manager: FragmentManager, employeeEmail: String) {
            PartnerEmployeeChangePasswordConfirmBottomSheet().apply {
                arguments = bundleOf(PARTNER_EMPLOYEE_EMAIL to employeeEmail)
                currentFragmentManager = manager
            }.show(manager, TAG)
        }
    }
}