package com.project2.partner.contract.impl.domain

import com.project2.partner.contract.api.domain.models.EmployeeDomain
import com.project2.partner.contract.api.domain.repository.PartnerEmployeesRepository
import com.project2.partner.contract.api.domain.usecase.GetEmployeeDetailUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetEmployeeDetailUseCaseImpl @Inject constructor(
    private val repository: PartnerEmployeesRepository
) : GetEmployeeDetailUseCase {

    override suspend fun invoke(email: String): Flow<EmployeeDomain> {
        return repository.getEmployeeDetailInfo(email)
    }
}