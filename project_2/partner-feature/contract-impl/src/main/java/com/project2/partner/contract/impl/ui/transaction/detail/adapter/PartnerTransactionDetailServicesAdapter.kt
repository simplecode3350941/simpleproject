package com.project2.partner.contract.impl.ui.transaction.detail.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.partner.contract.impl.databinding.PartnerTransactionDetailServicesItemBinding
import com.project2.partner.contract.impl.ui.transaction.model.TransactionServicesUi
import com.project2.partner.contract.impl.ui.transaction.model.priceWithQuantity

class PartnerTransactionDetailServicesAdapter :
    ListAdapter<TransactionServicesUi, PartnerTransactionDetailServicesAdapter.ViewHolder>(
        DIFF_CALLBACK
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    inner class ViewHolder(parent: ViewGroup) :
        BindingViewHolder<PartnerTransactionDetailServicesItemBinding>(
            parent,
            PartnerTransactionDetailServicesItemBinding::inflate
        ) {

        fun bind(item: TransactionServicesUi) = with(binding) {
            textTransactionServiceTitle.text = item.name
            textTransactionServicePrice.text = item.priceWithQuantity
        }
    }

    companion object {

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<TransactionServicesUi>() {
            override fun areItemsTheSame(
                oldItem: TransactionServicesUi,
                newItem: TransactionServicesUi
            ): Boolean {
                return oldItem.name == newItem.name
            }

            override fun areContentsTheSame(
                oldItem: TransactionServicesUi,
                newItem: TransactionServicesUi
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}