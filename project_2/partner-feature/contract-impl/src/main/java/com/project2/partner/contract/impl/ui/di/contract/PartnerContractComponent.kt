package com.project2.partner.contract.impl.ui.di.contract

import com.project2.core.presentation.di.ComponentDependenciesProviderModule
import com.project2.partner.contract.impl.ui.contract.PartnerContractFragment
import dagger.Component

@Component(
    modules = [ComponentDependenciesProviderModule::class],
    dependencies = [PartnerContractDependencies::class]
)
interface PartnerContractComponent {
    fun inject(contractFragment: PartnerContractFragment)
}