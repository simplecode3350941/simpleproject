package com.project2.partner.contract.impl.ui.employees.model

import android.content.Context
import com.project2.partner.contract.api.role.PartnerRole
import com.project2.partner.contract.impl.R

data class PartnerEmployeeUi(
    val firstName: String,
    val middleName: String?,
    val lastName: String,
    val email: String,
    val role: PartnerRole,
    val point: PartnerEmployeePointUi
)

class PartnerEmployeePointUi(
    val id: String,
    val name: String
)

val PartnerEmployeeUi.fullName: String
    get() {
        val lastNames = when {
            middleName.isNullOrEmpty() -> lastName
            else -> "$middleName $lastName"
        }
        return "$firstName $lastNames"
    }

fun PartnerEmployeeUi.getRole(context: Context): String {
    return when (role) {
        PartnerRole.OPERATOR -> context.getString(R.string.partner_employees_operator)
        PartnerRole.OWNER -> context.getString(R.string.partner_employees_owner)
    }
}

fun initPartnerEmployeeUi() = PartnerEmployeeUi(
    firstName = "",
    middleName = null,
    lastName = "",
    email = "",
    role = PartnerRole.OWNER,
    point = PartnerEmployeePointUi(id = "", name = "")
)