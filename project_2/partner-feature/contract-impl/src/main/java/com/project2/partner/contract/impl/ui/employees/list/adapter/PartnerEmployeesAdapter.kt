package com.project2.partner.contract.impl.ui.employees.list.adapter

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.core.utils.view.setOnClickDelayListener
import com.project2.partner.contract.api.role.PartnerRole
import com.project2.partner.contract.impl.R
import com.project2.partner.contract.impl.databinding.PartnerEmployeeItemBinding
import com.project2.partner.contract.impl.ui.employees.model.PartnerEmployeeUi
import com.project2.partner.contract.impl.ui.employees.model.fullName

class PartnerEmployeesAdapter(
    private val onClickListener: (String) -> Unit
) : PagingDataAdapter<PartnerEmployeeUi, PartnerEmployeesAdapter.ViewHolder>(
    DIFF_CALLBACK
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, onClickListener)
        }
    }

    inner class ViewHolder(parent: ViewGroup) :
        BindingViewHolder<PartnerEmployeeItemBinding>(
            parent,
            PartnerEmployeeItemBinding::inflate
        ) {

        fun bind(item: PartnerEmployeeUi, onClickListener: (String) -> Unit) = with(binding) {

            textEmployeeFio.text = item.fullName

            textEmployeeRole.text = when(item.role) {
                PartnerRole.OPERATOR -> getString(R.string.partner_employees_operator)
                PartnerRole.OWNER -> getString(R.string.partner_employees_owner)
            }

            itemView.setOnClickDelayListener {
                onClickListener(item.email)
            }
        }
    }

    companion object {

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<PartnerEmployeeUi>() {
            override fun areItemsTheSame(
                oldItem: PartnerEmployeeUi,
                newItem: PartnerEmployeeUi
            ): Boolean {
                return oldItem.email == newItem.email
            }

            override fun areContentsTheSame(
                oldItem: PartnerEmployeeUi,
                newItem: PartnerEmployeeUi
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}