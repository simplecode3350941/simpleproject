package com.project2.partner.contract.impl.ui.employees.change_password

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.dialog.BaseBottomSheetDialogFragment
import com.project2.partner.contract.impl.R
import com.project2.partner.contract.impl.databinding.PartnerEmployeeChangePasswordSuccessBottomSheetBinding

class PartnerEmployeeChangePasswordSuccessfulBottomSheet : BaseBottomSheetDialogFragment() {

    override val synthetic =
        SyntheticBinding(PartnerEmployeeChangePasswordSuccessBottomSheetBinding::inflate)

    private var currentFragmentManager: FragmentManager? = null

    private val employeeEmail: String
        get() = requireNotNull(arguments?.getString(PARTNER_EMPLOYEE_EMAIL))

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        dialog.setOnShowListener {
            (dialog.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout?)?.let {
                BottomSheetBehavior.from(it).state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        synthetic.binding.textViewChangePasswordSuccessEmployeeMessage.text =
            getString(R.string.partner_employees_change_password_success_message, employeeEmail)

        synthetic.binding.buttonChangePasswordSuccessEmployeeCancel.setOnClickListener {
            closeAllBottomSheetDialogs()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        closeAllBottomSheetDialogs()
    }

    private fun closeAllBottomSheetDialogs() {
        currentFragmentManager?.let {manager ->
            manager.fragments.takeIf { it.isNotEmpty() }
                ?.map {(it as? BaseBottomSheetDialogFragment)?.dismiss() }
        }
    }

    companion object {
        const val TAG = "EMPLOYEE_CHANGE_PASSWORD_SUCCESS_BOTTOM_SHEET"
        private const val PARTNER_EMPLOYEE_EMAIL = "PARTNER_EMPLOYEE_EMAIL"

        fun show(manager: FragmentManager, employeeEmail: String) {
            PartnerEmployeeChangePasswordSuccessfulBottomSheet().apply {
                arguments = bundleOf(PARTNER_EMPLOYEE_EMAIL to employeeEmail)
                currentFragmentManager = manager
            }.show(manager, TAG)
        }
    }
}