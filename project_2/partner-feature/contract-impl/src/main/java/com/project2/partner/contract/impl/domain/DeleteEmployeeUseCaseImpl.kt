package com.project2.partner.contract.impl.domain

import com.project2.partner.contract.api.domain.repository.PartnerEmployeesRepository
import com.project2.partner.contract.api.domain.usecase.DeleteEmployeeUseCase
import kotlinx.coroutines.flow.Flow
import timber.log.Timber
import javax.inject.Inject

class DeleteEmployeeUseCaseImpl @Inject constructor(
    private val repository: PartnerEmployeesRepository
) : DeleteEmployeeUseCase {
    override suspend fun invoke(email: String): Flow<Boolean> {
        return repository.deleteEmployee(email)
    }
}