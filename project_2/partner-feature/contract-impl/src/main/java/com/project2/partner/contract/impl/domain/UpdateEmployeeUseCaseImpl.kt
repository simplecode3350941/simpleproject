package com.project2.partner.contract.impl.domain

import com.project2.partner.contract.api.domain.models.EmployeeDomain
import com.project2.partner.contract.api.domain.repository.PartnerEmployeesRepository
import com.project2.partner.contract.api.domain.usecase.UpdateEmployeeUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class UpdateEmployeeUseCaseImpl @Inject constructor(
    private val repository: PartnerEmployeesRepository
) : UpdateEmployeeUseCase {
    override suspend fun invoke(employee: EmployeeDomain): Flow<Boolean> {
        return repository.updateEmployee(employee)
    }
}