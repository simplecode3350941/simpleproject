package com.project2.partner.contract.impl.ui.employees.edit

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.utils.common.isValidEmail
import com.project2.partner.contract.impl.R
import com.project2.partner.contract.impl.ui.di.employees.PartnerEmployeesDependencies
import com.project2.partner.contract.impl.ui.employees.create.PartnerEmployeeAbsCreateViewModel
import com.project2.partner.contract.impl.ui.employees.create.PartnerEmployeeCreateFragment
import com.project2.partner.contract.impl.ui.employees.delete.EmployeeDeleteBottomSheet
import com.project2.partner.contract.impl.ui.employees.model.PartnerEmployeeUi
import com.project2.partner.contract.impl.ui.employees.model.getRole
import com.project2.partner.contract.impl.ui.employees.model.initPartnerEmployeeUi
import com.project2.partner.point.selection.ui.select_service.communication.PointSelectionCommunication
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

class PartnerEmployeeEditFragment : PartnerEmployeeCreateFragment() {

    private val viewModel: PartnerEmployeeEditViewModel by assistedViewModel {
        findComponentDependencies<PartnerEmployeesDependencies>()
            .getPartnerEmployeeEditViewModelFactory()
    }

    private var originalPartnerEmployeeUi = initPartnerEmployeeUi()

    override fun getEmployeeViewModel() = viewModel

    override fun initUi(savedInstanceState: Bundle?) {
        super.initUi(savedInstanceState)
        viewLifecycleOwner.lifecycleScope.launch {
            getEmployeeViewModel().employeeDetail.collectLatest {
                showEmployeeEditData(it)
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            getEmployeeViewModel().employeeUiState.collectLatest(::updateLoadEmployeeUiState)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            PointSelectionCommunication.subscribe {
                synthetic.binding.buttonCreateEmployee.isVisible = isSaveButtonVisible()
            }
        }
    }


    override fun onSetFragmentResultListener() {
        childFragmentManager.setFragmentResultListener(
            EmployeeDeleteBottomSheet.REQUEST_KEY, viewLifecycleOwner
        ) { _, _ ->
            findNavController().navigate(R.id.action_employee_edit_to_employee_list)
        }
    }

    override fun onSaveEmployee(employee: PartnerEmployeeUi) {
        getEmployeeViewModel().onSaveEmployee(employee)
    }

    override fun onClickEmployeePoint() {
        navigateTo(R.id.action_edit_employees_to_point_selection)
    }

    override fun onClickEmployeeRole() {
        // TODO This when role selection is realized. Navigation to BS with role list
    }

    override fun onInitTextListenersAndViews() {
        synthetic.binding.apply {
            editTextEmployeeFirstName.addTextChangedListener {
                if (!it.isNullOrEmpty()) {
                    inputLayoutEmployeeFirstName.error = null
                }
                buttonCreateEmployee.isVisible = isSaveButtonVisible()
            }
            editTextEmployeeLastName.addTextChangedListener {
                if (!it.isNullOrEmpty()) {
                    inputLayoutEmployeeLastName.error = null
                }
                buttonCreateEmployee.isVisible = isSaveButtonVisible()
            }
            editTextEmployeeMiddleName.addTextChangedListener {
                buttonCreateEmployee.isVisible = isSaveButtonVisible()
            }
            editTextEmployeeEmail.addTextChangedListener {
                if (!it.isNullOrEmpty() && isValidEmail(it)) {
                    inputLayoutEmployeeEmail.error = null
                }
                buttonCreateEmployee.isVisible = isSaveButtonVisible()
            }

            textViewDeleteEmployee.setOnClickListener {
                EmployeeDeleteBottomSheet.show(
                    childFragmentManager,
                    editTextEmployeeEmail.text.toString()
                )
            }

            topbarPartnerEmployeeCreate.textViewRight.setOnClickListener {
                popBack()
            }

            textViewPersonalDataTitle.text =
                getString(R.string.partner_employees_edit_title)

            textViewDeleteEmployee.isVisible = true

            topbarPartnerEmployeeCreate.textViewRight.text =
                getString(R.string.partner_employees_edit_cancel)

            topbarPartnerEmployeeCreate.textViewRight.isVisible = true
        }
    }

    private fun showEmployeeEditData(partnerEmployeeEditUi: PartnerEmployeeUi) {
        originalPartnerEmployeeUi = partnerEmployeeEditUi
        if (partnerEmployeeEditUi.middleName.isNullOrEmpty()) {
            originalPartnerEmployeeUi = partnerEmployeeEditUi.copy(middleName = "")
        }
        synthetic.binding.apply {
            editTextEmployeeFirstName.setText(partnerEmployeeEditUi.firstName)
            editTextEmployeeLastName.setText(partnerEmployeeEditUi.lastName)
            editTextEmployeeMiddleName.setText(partnerEmployeeEditUi.middleName)
            editTextEmployeeEmail.setText(partnerEmployeeEditUi.email)
            textViewEmployeeRole.text = partnerEmployeeEditUi.getRole(requireContext())
            textViewEmployeePoint.text = partnerEmployeeEditUi.point.name
            buttonCreateEmployee.isVisible = false
            onUpdatePointColor()
        }
    }

    private fun isSaveButtonVisible(): Boolean {
        // TODO Added role verification when role selection is realized.
        return synthetic.binding.editTextEmployeeFirstName.text.toString() != originalPartnerEmployeeUi.firstName ||
                synthetic.binding.editTextEmployeeLastName.text.toString() != originalPartnerEmployeeUi.lastName ||
                synthetic.binding.editTextEmployeeMiddleName.text.toString() != originalPartnerEmployeeUi.middleName ||
                synthetic.binding.editTextEmployeeEmail.text.toString() != originalPartnerEmployeeUi.email ||
                synthetic.binding.textViewEmployeePoint.text.toString() != originalPartnerEmployeeUi.point.name

    }

    override fun onSuccess() {
        popBack()
    }

    private fun updateLoadEmployeeUiState(employState: EmployeeUiState) {
        Timber.d("employState $employState")
        when (employState) {
            is EmployeeUiState.Loading -> {
                showHideProgressBar(true)
            }

            is EmployeeUiState.Error -> {
                showHideProgressBar(false)
                showCommonErrorSnack(employState.error)
            }

            is EmployeeUiState.Success -> {
                showHideProgressBar(false)
            }
        }
    }
}