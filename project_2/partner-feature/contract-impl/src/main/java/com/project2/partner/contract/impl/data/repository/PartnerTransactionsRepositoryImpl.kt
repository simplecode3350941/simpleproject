package com.project2.partner.contract.impl.data.repository

import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.partner.contract.api.data.remote.api.PartnerTransactionsApi
import com.project2.partner.contract.api.domain.models.TransactionDetailDomain
import com.project2.partner.contract.api.domain.models.TransactionDomain
import com.project2.partner.contract.api.domain.repository.PartnerTransactionsRepository
import com.project2.partner.contract.impl.data.mapper.toDomain
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PartnerTransactionsRepositoryImpl @Inject constructor(
    private val transactionsApi: PartnerTransactionsApi,
    @IoDispatcher
    private val dispatcher: CoroutineDispatcher
) : PartnerTransactionsRepository {

    override suspend fun getTransactionsList(
        pageNumber: Int,
        pageSize: Int
    ): Flow<List<TransactionDomain>> {
        return flow {
            val transactions = transactionsApi.getTransactions(pageNumber, pageSize).content
            emit(transactions)
        }
            .map { transactions ->
                transactions.map { it.toDomain() }
            }
            .flowOn(dispatcher)
    }


    override suspend fun getTransactionDetailInfo(id: String): Flow<TransactionDetailDomain> {
        return flow {
            val transaction = transactionsApi.getTransactionDetailInfo(id)
            emit(transaction)
        }
            .map { transaction -> transaction.toDomain() }
            .flowOn(dispatcher)
    }
}