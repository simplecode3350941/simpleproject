package com.project2.partner.contract.impl.ui.transaction.mapper

import com.project2.partner.contract.api.domain.models.TransactionDetailDomain
import com.project2.partner.contract.api.domain.models.TransactionDetailPointDomain
import com.project2.partner.contract.api.domain.models.TransactionDomain
import com.project2.partner.contract.api.domain.models.TransactionServicesDomain
import com.project2.partner.contract.impl.ui.transaction.model.PartnerTransactionDetailUi
import com.project2.partner.contract.impl.ui.transaction.model.PartnerTransactionUi
import com.project2.partner.contract.impl.ui.transaction.model.TransactionDetailPointUi
import com.project2.partner.contract.impl.ui.transaction.model.TransactionServicesUi

fun TransactionDomain.toPresentation() = PartnerTransactionUi(
    id = id,
    pointName = pointName,
    pointTypes = pointTypes,
    amount = amount,
    date = date
)

fun TransactionDetailDomain.toPresentation() = PartnerTransactionDetailUi(
    id = id,
    point = point.toPresentation(),
    orderNumber = orderNumber,
    pan = pan,
    amount = amount,
    date = date,
    services = services.map {it.toPresentation()}

)

fun TransactionDetailPointDomain.toPresentation() = TransactionDetailPointUi(
    id = id,
    name = name,
    types = types,
    address = address
)

fun TransactionServicesDomain.toPresentation() = TransactionServicesUi(
    name = name,
    price = price,
    quantity = quantity,
    serviceID = serviceID
)