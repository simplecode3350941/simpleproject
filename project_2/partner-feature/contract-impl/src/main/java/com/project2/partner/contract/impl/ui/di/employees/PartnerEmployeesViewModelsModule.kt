package com.project2.partner.contract.impl.ui.di.employees

import androidx.lifecycle.ViewModel
import com.project2.core.commons.di.viewmodule.ViewModelKey
import com.project2.partner.contract.impl.ui.employees.change_password.PartnerEmployeeChangePasswordConfirmViewModel
import com.project2.partner.contract.impl.ui.employees.create.PartnerEmployeeCreateViewModel
import com.project2.partner.contract.impl.ui.employees.delete.EmployeeDeleteViewModel
import com.project2.partner.contract.impl.ui.employees.detail.PartnerEmployeeDetailInfoViewModel
import com.project2.partner.contract.impl.ui.employees.list.PartnerEmployeesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PartnerEmployeesViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(PartnerEmployeesViewModel::class)
    abstract fun partnerEmployeesViewModel(viewModel: PartnerEmployeesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PartnerEmployeeDetailInfoViewModel::class)
    abstract fun partnerEmployeeDetailInfoViewModel(
        viewModel: PartnerEmployeeDetailInfoViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EmployeeDeleteViewModel::class)
    abstract fun partnerEmployeeDeleteViewModel(
        viewModel: EmployeeDeleteViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PartnerEmployeeChangePasswordConfirmViewModel::class)
    abstract fun partnerPartnerEmployeeChangePasswordConfirmViewModel(
        viewModel: PartnerEmployeeChangePasswordConfirmViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PartnerEmployeeCreateViewModel::class)
    abstract fun partnerPartnerEmployeeCreateViewModel(
        viewModel: PartnerEmployeeCreateViewModel
    ): ViewModel
}