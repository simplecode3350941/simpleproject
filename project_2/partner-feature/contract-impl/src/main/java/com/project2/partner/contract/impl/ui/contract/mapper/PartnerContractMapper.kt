package com.project2.partner.contract.impl.ui.contract.mapper

import com.project2.core.utils.formatter.FormatUtils
import com.project2.partner.contract.api.domain.models.PartnerContractDomain
import com.project2.partner.contract.impl.domain.models.ContractWithProfileDomain
import com.project2.partner.contract.impl.ui.contract.models.ContractWithProfileUi
import com.project2.partner.contract.impl.ui.contract.models.PartnerContractUi
import com.project2.partner.points.api.domain.models.PointShortDomain
import com.project2.partner.points.api.ui.list.models.MODERATION_STATUSES
import com.project2.partner.points.api.ui.list.models.PointShortUi
import com.project2.partner.points.api.ui.list.models.PointStatus
import com.project2.partner.profile.impl.ui.mapper.toPresentation

fun PartnerContractDomain.toPresentation() = PartnerContractUi(
    dailyRevenue = FormatUtils.formattedFractionalPrice(dailyRevenue),
    monthlyRevenue = FormatUtils.formattedFractionalPrice(monthlyRevenue),
    contractNumber = contractNumber,
    organizationName = organizationName,
    point = point?.toPresentation()
)

fun PointShortDomain.toPresentation() = PointShortUi(
    id = id,
    name = name,
    address = address,
    pointTypes = pointTypes,
    formattedPointTypes = pointTypes.joinToString(", "),
    status = status.toPointStatus()
)

fun ContractWithProfileDomain.toPresentation() = ContractWithProfileUi(
    contractUi = contract.toPresentation(),
    profileUi = profile.toPresentation()
)

fun String.toPointStatus(): PointStatus {
    return when (this) {
        "ACTIVE" -> PointStatus.ACTIVE
        "DECLINED" -> PointStatus.DECLINED
        "HIDDEN" -> PointStatus.HIDDEN
        in MODERATION_STATUSES -> PointStatus.MODERATION
        else -> PointStatus.ACTIVE
    }
}