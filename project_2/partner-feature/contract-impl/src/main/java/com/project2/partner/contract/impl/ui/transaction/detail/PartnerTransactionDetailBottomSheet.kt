package com.project2.partner.contract.impl.ui.transaction.detail

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.dialog.BaseMvvmBottomSheetDialogFragment
import com.project2.core.utils.common.lazyUnsafe
import com.project2.partner.contract.impl.databinding.PartnerTransactionBottomSheetBinding
import com.project2.partner.contract.impl.ui.transaction.detail.adapter.PartnerTransactionDetailServicesAdapter
import com.project2.partner.contract.impl.ui.transaction.model.PartnerTransactionDetailUi
import com.project2.partner.contract.impl.ui.transaction.model.allPointTypes
import com.project2.partner.contract.impl.ui.transaction.model.formattedAmount
import com.project2.partner.contract.impl.ui.transaction.model.getFriendlyDate
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

class PartnerTransactionDetailBottomSheet : BaseMvvmBottomSheetDialogFragment() {

    override val synthetic = SyntheticBinding(PartnerTransactionBottomSheetBinding::inflate)

    private val viewModel by viewModels<PartnerTransactionDetailViewModel> {
        viewModelFactory
    }

    private val transactionServicesAdapter: PartnerTransactionDetailServicesAdapter by lazyUnsafe {
        PartnerTransactionDetailServicesAdapter()
    }

    private val transitionId: String
        get() = arguments?.getString(PARTNER_TRANSACTION_ID, "") ?: ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.updateState.collectLatest(::bindUiStates)
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.transaction.collectLatest(::showTransaction)
        }
        synthetic.binding.rvServices.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = transactionServicesAdapter
        }

        viewModel.onLoadTransactionDetail(transitionId)
    }

    private fun bindUiStates(uiState: UiState) {
        Timber.d("Partner transaction UiState - $uiState")
        when (uiState) {
            is UiState.Loading -> {
                showHideLoading(true)
                (dialog as? BottomSheetDialog)?.behavior?.state = STATE_EXPANDED
            }
            is UiState.Error -> {
                Timber.e(uiState.error)
                showHideLoading(false)
                showErrorView(uiState.error.localizedMessage)
            }
            is UiState.Success -> {
                showHideLoading(false)
            }
        }
    }

    private fun showHideLoading(show: Boolean) {
        val shimmer = synthetic.binding.includeTransactionDetailLoadingView.shimmerView
        if (show) {
            shimmer.showShimmer(true)
        } else {
            shimmer.hideShimmer()
            synthetic.binding.scrollViewTransactionDetail.isVisible = true
        }
        synthetic.binding.contentLayoutTransactionDetail.isVisible = !show
        shimmer.isVisible = show
    }

    private fun showErrorView(error: String?) {
        val errorView = synthetic.binding.includeTransactionDetailErrorView.errorView
        if (!error.isNullOrEmpty()) {
            synthetic.binding.includeTransactionDetailErrorView.textViewErrorMessage.text = error
        }
        errorView.isVisible = true
        synthetic.binding.contentLayoutTransactionDetail.isVisible = false
        synthetic.binding.includeTransactionDetailErrorView.buttonRefresh.setOnClickListener {
            errorView.isVisible = false
            viewModel.onLoadTransactionDetail(transitionId)
        }
    }

    private fun showTransaction(transaction: PartnerTransactionDetailUi) {
        synthetic.binding.textTransactionDate.text = transaction.getFriendlyDate(requireContext())
        synthetic.binding.textTransactionPointName.text = transaction.point.name
        synthetic.binding.textTransactionPointType.text = transaction.allPointTypes
        synthetic.binding.textTransactionPrice.text = transaction.formattedAmount
        synthetic.binding.textTransactionOrderValue.text = transaction.orderNumber
        synthetic.binding.textTransactionPanValue.text = transaction.pan
        synthetic.binding.textTransactionAddressValue.text = transaction.point.address

        transactionServicesAdapter.submitList(transaction.services)
    }

    companion object {
        const val TAG = "PARTNER_TRANSACTION_BOTTOM_SHEET"
        const val PARTNER_TRANSACTION_ID = "partner_transaction_id"

        fun show(manager: FragmentManager, transactionId: String) {
            PartnerTransactionDetailBottomSheet().apply {
                arguments = bundleOf(
                    PARTNER_TRANSACTION_ID to transactionId
                )
            }.show(manager, TAG)
        }
    }
}