package com.project2.partner.contract.impl.domain.models

import com.project2.partner.contract.api.domain.models.PartnerContractDomain
import com.project2.partner.profile.api.domain.models.ProfileDomain

class ContractWithProfileDomain(
    val contract: PartnerContractDomain,
    val profile: ProfileDomain
)