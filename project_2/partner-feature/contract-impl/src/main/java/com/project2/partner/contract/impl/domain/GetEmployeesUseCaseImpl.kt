package com.project2.partner.contract.impl.domain

import com.project2.partner.contract.api.domain.models.EmployeeDomain
import com.project2.partner.contract.api.domain.repository.PartnerEmployeesRepository
import com.project2.partner.contract.api.domain.usecase.GetEmployeesUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetEmployeesUseCaseImpl @Inject constructor(
    private val repository: PartnerEmployeesRepository
) : GetEmployeesUseCase {

    override suspend operator fun invoke(
        pageNumber: Int,
        pageSize: Int
    ): Flow<List<EmployeeDomain>> {
        return repository.getEmployeesList(pageNumber, pageSize)
    }
}