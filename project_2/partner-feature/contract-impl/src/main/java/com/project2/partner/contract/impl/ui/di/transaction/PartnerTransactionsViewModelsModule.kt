package com.project2.partner.contract.impl.ui.di.transaction

import androidx.lifecycle.ViewModel
import com.project2.core.commons.di.viewmodule.ViewModelKey
import com.project2.partner.contract.impl.ui.transaction.detail.PartnerTransactionDetailViewModel
import com.project2.partner.contract.impl.ui.transaction.list.PartnerTransactionsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PartnerTransactionsViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(PartnerTransactionsViewModel::class)
    abstract fun partnerTransactionViewModel(viewModel: PartnerTransactionsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PartnerTransactionDetailViewModel::class)
    abstract fun partnerTransactionDetailViewModel(
        viewModel: PartnerTransactionDetailViewModel
    ): ViewModel
}