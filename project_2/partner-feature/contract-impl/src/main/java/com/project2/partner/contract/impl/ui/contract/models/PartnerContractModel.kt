package com.project2.partner.contract.impl.ui.contract.models

import android.text.Spannable
import com.project2.partner.points.api.ui.list.models.PointShortUi
import com.project2.partner.profile.core.impl.ui.models.ProfileUi

class PartnerContractUi(
    val dailyRevenue: Spannable,
    val monthlyRevenue: Spannable,
    val contractNumber: String,
    val organizationName: String,
    val point: PointShortUi?
)

class ContractWithProfileUi(
    val contractUi: PartnerContractUi,
    val profileUi: ProfileUi
)