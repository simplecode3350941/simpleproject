package com.project2.partner.contract.impl.domain

import com.project2.partner.contract.api.domain.models.EmployeeDomain
import com.project2.partner.contract.api.domain.repository.PartnerEmployeesRepository
import com.project2.partner.contract.api.domain.usecase.SaveEmployeeUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SaveEmployeeUseCaseImpl @Inject constructor(
    private val repository: PartnerEmployeesRepository
) : SaveEmployeeUseCase {
    override suspend fun invoke(employee: EmployeeDomain): Flow<String> {
        return repository.saveEmployee(employee)
    }
}