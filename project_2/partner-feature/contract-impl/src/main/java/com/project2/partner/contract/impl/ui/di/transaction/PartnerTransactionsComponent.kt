package com.project2.partner.contract.impl.ui.di.transaction

import com.project2.core.presentation.di.ComponentDependenciesProviderModule
import com.project2.partner.contract.impl.ui.transaction.detail.PartnerTransactionDetailBottomSheet
import com.project2.partner.contract.impl.ui.transaction.list.PartnerTransactionsFragment
import dagger.Component

@Component(
    modules = [ComponentDependenciesProviderModule::class],
    dependencies = [PartnerTransactionsDependencies::class]
)
interface PartnerTransactionsComponent {
    fun inject(transactionsFragment: PartnerTransactionsFragment)

    fun inject(transactionBottomSheet: PartnerTransactionDetailBottomSheet)
}