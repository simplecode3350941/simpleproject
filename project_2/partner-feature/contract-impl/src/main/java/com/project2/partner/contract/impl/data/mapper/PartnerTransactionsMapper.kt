package com.project2.partner.contract.impl.data.mapper

import com.project2.partner.contract.api.data.remote.models.transactions.TransactionDetailPoint
import com.project2.partner.contract.api.data.remote.models.transactions.TransactionDetailResponse
import com.project2.partner.contract.api.data.remote.models.transactions.TransactionResponse
import com.project2.partner.contract.api.data.remote.models.transactions.TransactionServices
import com.project2.partner.contract.api.domain.models.TransactionDetailDomain
import com.project2.partner.contract.api.domain.models.TransactionDetailPointDomain
import com.project2.partner.contract.api.domain.models.TransactionDomain
import com.project2.partner.contract.api.domain.models.TransactionServicesDomain

fun TransactionResponse.toDomain() = TransactionDomain(
    id = id,
    pointName = pointName,
    pointTypes = pointTypes.map { it.localizedName },
    amount = amount,
    date = date
)

fun TransactionDetailResponse.toDomain() = TransactionDetailDomain(
    id = id,
    point = point.toDomain(),
    orderNumber = orderNumber,
    pan = pan,
    amount = amount,
    date = date,
    services = services.map { it.toDomain() }
)

fun TransactionDetailPoint.toDomain() = TransactionDetailPointDomain(
    id = id,
    name = name,
    types = types.map { it.localizedName },
    address = address
)

fun TransactionServices.toDomain() = TransactionServicesDomain(
    name = name,
    price = price,
    quantity = quantity,
    serviceID = serviceID
)