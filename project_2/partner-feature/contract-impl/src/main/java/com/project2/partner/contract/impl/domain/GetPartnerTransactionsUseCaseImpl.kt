package com.project2.partner.contract.impl.domain

import com.project2.partner.contract.api.domain.models.TransactionDomain
import com.project2.partner.contract.api.domain.repository.PartnerTransactionsRepository
import com.project2.partner.contract.api.domain.usecase.GetPartnerTransactionsUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPartnerTransactionsUseCaseImpl@Inject constructor(
    private val repository: PartnerTransactionsRepository
): GetPartnerTransactionsUseCase {

    override suspend operator fun invoke(pageNumber: Int, pageSize: Int): Flow<List<TransactionDomain>> {
        return repository.getTransactionsList(pageNumber, pageSize)
    }
}