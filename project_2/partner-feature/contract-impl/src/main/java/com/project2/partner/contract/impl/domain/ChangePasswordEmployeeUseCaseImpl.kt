package com.project2.partner.contract.impl.domain

import com.project2.partner.contract.api.domain.repository.PartnerEmployeesRepository
import com.project2.partner.contract.api.domain.usecase.ChangePasswordEmployeeUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ChangePasswordEmployeeUseCaseImpl @Inject constructor(
    private val repository: PartnerEmployeesRepository
) : ChangePasswordEmployeeUseCase {
    override suspend fun invoke(email: String): Flow<Boolean> {
        return repository.changePasswordEmployee(email)
    }
}