package com.project2.partner.contract.impl.ui.employees.list

import com.project2.core.presentation.paging.Paginator
import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.core.utils.common.toImmutableList
import com.project2.partner.contract.api.domain.usecase.GetEmployeesUseCase
import com.project2.partner.contract.impl.ui.employees.mapper.toPresentation
import com.project2.partner.contract.impl.ui.employees.model.PartnerEmployeeUi
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import timber.log.Timber
import javax.inject.Inject

class PartnerEmployeesViewModel @Inject constructor(
    private val getEmployees: GetEmployeesUseCase,
    override val exceptionHandler: CoroutineExceptionHandler
) : BaseViewModel() {

    private val _updateState = MutableStateFlow<UiState>(UiState.Loading)
    val updateState = _updateState.asStateFlow()

    private val _paginationState = MutableStateFlow<PaginationState?>(null)
    val paginationState = _paginationState.asStateFlow().filterNotNull()

    private val _employeesFlow = MutableStateFlow<List<PartnerEmployeeUi>>(listOf())
    val employeesFlow = _employeesFlow.asStateFlow()

    private val listEmployees: List<PartnerEmployeeUi>
        get() = employeesFlow.value.toImmutableList()

    private val paginator = Paginator()

    private val isPagination: Boolean
        get() = paginator.getCurrentPage() > 0

    init {
        onRefreshData()
    }

    fun onRefreshData() {
        paginator.reset()
        getEmployees()
    }

    fun onPaginate() {
        if(paginator.hasData()) {
            getEmployees()
        }
    }

    private fun getEmployees() {
        launchOnViewModelScope {
            getEmployees(paginator.getCurrentPage(), paginator.pageSize)
                .loadingState()
                .map { employees -> employees.map { it.toPresentation() } }
                .errorState()
                .successState()
        }
    }

    private fun <T> Flow<T>.loadingState(): Flow<T> {
        return onStart {
            when {
                isPagination -> _paginationState.value = PaginationState.Loading
                else -> _updateState.value = UiState.Loading
            }
        }
    }

    private fun <T> Flow<T>.errorState(): Flow<T> {
        return catch {
            when {
                isPagination -> _paginationState.value = PaginationState.Error(it)
                else -> _updateState.value = UiState.Error(it)
            }
        }
    }

    private suspend fun Flow<List<PartnerEmployeeUi>>.successState() {
        collect {
            Timber.d("collect - $it.size")
            if (isPagination) {
                _paginationState.value = PaginationState.Success
                paginator.nextPage(it.size)
                _employeesFlow.value =  (listEmployees + it).toImmutableList()
            } else {
                if (it.isEmpty()) {
                    _updateState.value = UiState.Empty
                } else {
                    paginator.nextPage(it.size)
                    _updateState.value = UiState.Success
                    _employeesFlow.value = it
                }
            }
        }
    }
}

sealed interface UiState {
    object Loading: UiState
    object Success: UiState
    data class Error(val error: Throwable): UiState
    object Empty: UiState
}

sealed interface PaginationState {
    object Loading: PaginationState
    object Success: PaginationState
    data class Error(val error: Throwable): PaginationState
}