package com.project2.partner.contract.impl.ui.transaction.list

import com.project2.core.presentation.paging.Paginator
import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.core.utils.common.toImmutableList
import com.project2.partner.contract.api.domain.usecase.GetPartnerTransactionsUseCase
import com.project2.partner.contract.impl.ui.transaction.mapper.toPresentation
import com.project2.partner.contract.impl.ui.transaction.model.PartnerTransactionUi
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import timber.log.Timber
import javax.inject.Inject

class PartnerTransactionsViewModel @Inject constructor(
    private val getTransactions: GetPartnerTransactionsUseCase,
    override val exceptionHandler: CoroutineExceptionHandler
) : BaseViewModel() {

    private val _updateState = MutableStateFlow<UiState>(UiState.Loading)
    val updateState = _updateState.asStateFlow()

    private val _paginationState = MutableStateFlow<PaginationState?>(null)
    val paginationState = _paginationState.asStateFlow().filterNotNull()

    private val _transactionsFlow = MutableStateFlow<List<PartnerTransactionUi>>(listOf())
    val transactionsFlow = _transactionsFlow.asStateFlow()

    private val transactions: List<PartnerTransactionUi>
        get() = _transactionsFlow.value.toImmutableList()

    private val paginator = Paginator()
    private val isPagination: Boolean
        get() = paginator.getCurrentPage() > 0

    init {
        onRefreshData()
    }

    fun onRefreshData() {
        paginator.reset()
        getTransactions()
    }

    fun onPaginate() {
        if(paginator.hasData()) {
            getTransactions()
        }
    }

    private fun getTransactions() {
        launchOnViewModelScope {
            getTransactions(paginator.getCurrentPage(), paginator.pageSize)
                .loadingState()
                .map { transactions -> transactions.map { it.toPresentation() } }
                .errorState()
                .successState()
        }
    }

    private fun <T> Flow<T>.loadingState(): Flow<T> {
        return onStart {
            when {
                isPagination -> _paginationState.value = PaginationState.Loading
                else -> _updateState.value = UiState.Loading
            }
        }
    }

    private fun <T> Flow<T>.errorState(): Flow<T> {
        return catch {
            when {
                isPagination -> _paginationState.value = PaginationState.Error(it)
                else -> _updateState.value = UiState.Error(it)
            }
        }
    }

    private suspend fun Flow<List<PartnerTransactionUi>>.successState() {
        collect {
            Timber.d("collect - ${it.size}")
            if (isPagination) {
                _paginationState.value = PaginationState.Success
                paginator.nextPage(it.size)
                _transactionsFlow.value =  (transactions + it).toImmutableList()
            } else {
                if (it.isEmpty()) {
                    _updateState.value = UiState.Empty
                } else {
                    paginator.nextPage(it.size)
                    _updateState.value = UiState.Success
                    _transactionsFlow.value = it
                }
            }
        }
    }
}

sealed interface UiState {
    object Loading: UiState
    object Success: UiState
    data class Error(val error: Throwable): UiState
    object Empty: UiState
}

sealed interface PaginationState {
    object Loading: PaginationState
    object Success: PaginationState
    data class Error(val error: Throwable): PaginationState
}