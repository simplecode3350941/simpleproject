package com.project2.partner.contract.impl.di

import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.core.network.BuildConfig
import com.project2.partner.contract.api.data.remote.api.PartnerEmployeesApi
import com.project2.partner.contract.api.domain.repository.PartnerEmployeesRepository
import com.project2.partner.contract.api.domain.usecase.ChangePasswordEmployeeUseCase
import com.project2.partner.contract.api.domain.usecase.DeleteEmployeeUseCase
import com.project2.partner.contract.api.domain.usecase.SaveEmployeeUseCase
import com.project2.partner.contract.api.domain.usecase.GetEmployeeDetailUseCase
import com.project2.partner.contract.api.domain.usecase.GetEmployeesUseCase
import com.project2.partner.contract.api.domain.usecase.UpdateEmployeeUseCase
import com.project2.partner.contract.impl.data.repository.PartnerEmployeesRepositoryImpl
import com.project2.partner.contract.impl.domain.ChangePasswordEmployeeUseCaseImpl
import com.project2.partner.contract.impl.domain.DeleteEmployeeUseCaseImpl
import com.project2.partner.contract.impl.domain.GetEmployeeDetailUseCaseImpl
import com.project2.partner.contract.impl.domain.GetEmployeesUseCaseImpl
import com.project2.partner.contract.impl.domain.SaveEmployeeUseCaseImpl
import com.project2.partner.contract.impl.domain.UpdateEmployeeUseCaseImpl
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class PartnerEmployeesModule {
    @Provides
    @Singleton
    fun providePartnerEmployeesApi(
        @Named("partner_api") okHttpClient: OkHttpClient,
        @Named("moshi_converter") moshiConverterFactory: MoshiConverterFactory
    ): PartnerEmployeesApi {
        val builder = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.project2_API_URL)
            .addConverterFactory(moshiConverterFactory)
            .build()

        return builder.create(PartnerEmployeesApi::class.java)
    }

    @Provides
    fun provideEmployeesRepository(
        employeesApi: PartnerEmployeesApi,
        @IoDispatcher dispatcher: CoroutineDispatcher
    ): PartnerEmployeesRepository {
        return PartnerEmployeesRepositoryImpl(employeesApi, dispatcher)
    }

    @Provides
    fun provideGetEmployeesListUseCase(
        repository: PartnerEmployeesRepository
    ): GetEmployeesUseCase {
        return GetEmployeesUseCaseImpl(repository)
    }

    @Provides
    fun provideGetEmployeeDetailInfoUseCase(
        repository: PartnerEmployeesRepository
    ): GetEmployeeDetailUseCase {
        return GetEmployeeDetailUseCaseImpl(repository)
    }

    @Provides
    fun provideSaveEmployeeUseCase(
        repository: PartnerEmployeesRepository
    ): SaveEmployeeUseCase {
        return SaveEmployeeUseCaseImpl(repository)
    }

    @Provides
    fun provideUpdateEmployeeUseCase(
        repository: PartnerEmployeesRepository
    ): UpdateEmployeeUseCase {
        return UpdateEmployeeUseCaseImpl(repository)
    }

    @Provides
    fun provideDeleteEmployeeUseCase(
        repository: PartnerEmployeesRepository
    ): DeleteEmployeeUseCase {
        return DeleteEmployeeUseCaseImpl(repository)
    }

    @Provides
    fun provideChangePasswordEmployeeUseCase(
        repository: PartnerEmployeesRepository
    ): ChangePasswordEmployeeUseCase {
        return ChangePasswordEmployeeUseCaseImpl(repository)
    }
}