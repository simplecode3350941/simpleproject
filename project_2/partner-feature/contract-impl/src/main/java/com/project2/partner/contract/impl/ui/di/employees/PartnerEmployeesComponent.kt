package com.project2.partner.contract.impl.ui.di.employees

import com.project2.core.presentation.di.ComponentDependenciesProviderModule
import com.project2.partner.contract.impl.ui.employees.create.PartnerEmployeeCreateFragment
import com.project2.partner.contract.impl.ui.employees.detail.PartnerEmployeeDetailInfoFragment
import com.project2.partner.contract.impl.ui.employees.edit.PartnerEmployeeEditFragment
import com.project2.partner.contract.impl.ui.employees.list.PartnerEmployeesFragment
import dagger.Component

@Component(
    modules = [ComponentDependenciesProviderModule::class],
    dependencies = [PartnerEmployeesDependencies::class]
)
interface PartnerEmployeesComponent {
    fun inject(employeesFragment: PartnerEmployeesFragment)

    fun inject(employeeDetailInfoFragment: PartnerEmployeeDetailInfoFragment)

    fun inject(employeeCreateFragment: PartnerEmployeeCreateFragment)

    fun inject(employeeEditFragment: PartnerEmployeeEditFragment)
}