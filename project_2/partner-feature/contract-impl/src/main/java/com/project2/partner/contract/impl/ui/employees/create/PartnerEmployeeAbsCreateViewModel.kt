package com.project2.partner.contract.impl.ui.employees.create

import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.partner.contract.impl.ui.employees.model.PartnerEmployeeUi
import com.project2.partner.point.selection.ui.select_service.communication.PointSelectionCommunication
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.filterNotNull

abstract class PartnerEmployeeAbsCreateViewModel: BaseViewModel() {

    private val _uiState = MutableStateFlow<UiState?>(null)
    val uiState = _uiState.asStateFlow().filterNotNull()

    abstract fun onSaveEmployee(employee: PartnerEmployeeUi)

    override fun onCleared() {
        super.onCleared()
        PointSelectionCommunication.clear()
    }

    fun resetUiState() {
        _uiState.value = null
    }

    protected fun updateState(state: UiState) {
        _uiState.value = state
    }
}

sealed interface UiState {
    object Loading: UiState
    object Success: UiState
    data class Error(val error: Throwable): UiState
}