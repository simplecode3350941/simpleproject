package com.project2.partner.contract.impl.ui.di.contract

import com.project2.core.presentation.di.ComponentDependencies
import com.project2.partner.contract.api.data.remote.api.PartnerContractApi
import com.project2.partner.contract.api.domain.repository.PartnerContractRepository
import com.project2.partner.contract.api.domain.usecase.GetPartnerContractUseCase

interface PartnerContractDependencies: ComponentDependencies {
    fun contractApi(): PartnerContractApi
    fun contractRepository(): PartnerContractRepository
    fun getContractUseCases(): GetPartnerContractUseCase

}
