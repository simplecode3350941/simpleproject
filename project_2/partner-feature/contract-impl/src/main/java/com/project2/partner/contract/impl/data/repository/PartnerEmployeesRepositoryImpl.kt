package com.project2.partner.contract.impl.data.repository

import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.partner.contract.api.data.remote.api.PartnerEmployeesApi
import com.project2.partner.contract.api.domain.models.EmployeeDomain
import com.project2.partner.contract.api.domain.repository.PartnerEmployeesRepository
import com.project2.partner.contract.impl.data.mapper.toData
import com.project2.partner.contract.impl.data.mapper.toDomain
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import timber.log.Timber
import javax.inject.Inject

class PartnerEmployeesRepositoryImpl @Inject constructor(
    private val employeesApi: PartnerEmployeesApi,
    @IoDispatcher
    private val dispatcher: CoroutineDispatcher
): PartnerEmployeesRepository {

    override suspend fun getEmployeesList(pageNumber: Int, pageSize: Int): Flow<List<EmployeeDomain>> {
        return flow {
            val employees = employeesApi.getEmployees(pageNumber, pageSize).content
            emit(employees)
        }
            .map { employees ->
                employees.map { it.toDomain() }
            }
            .flowOn(dispatcher)
    }

    override suspend fun getEmployeeDetailInfo(email: String): Flow<EmployeeDomain> {
        return flow {
            val employee = employeesApi.getEmployeeDetailInfo(email)
            emit(employee)
        }
            .map { employee -> employee.toDomain() }
            .flowOn(dispatcher)
    }

    override suspend fun saveEmployee(employee: EmployeeDomain): Flow<String> {
        return flow {
            val pointResponse = employeesApi.saveEmployee(employee.toData())
            emit(pointResponse)
        }
            .map { it.email }
            .flowOn(dispatcher)
    }

    override suspend fun updateEmployee(employee: EmployeeDomain): Flow<Boolean> {
        return flow {
            val pointResponse = employeesApi.updateEmployee(employee.toData())
            emit(pointResponse)
        }
            .map { true }
            .flowOn(dispatcher)
    }

    override suspend fun deleteEmployee(email: String): Flow<Boolean> {
        return flow {
            val employee = employeesApi.deleteEmployee(email)
            emit(employee)
        }
            .map { true }
            .flowOn(dispatcher)
    }

    override suspend fun changePasswordEmployee(email: String): Flow<Boolean> {
        return flow {
            val employee = employeesApi.changePasswordEmployee(email)
            emit(employee)
        }
            .map { true }
            .flowOn(dispatcher)
    }
}