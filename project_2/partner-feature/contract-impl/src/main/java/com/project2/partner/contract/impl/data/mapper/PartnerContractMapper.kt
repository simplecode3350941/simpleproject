package com.project2.partner.contract.impl.data.mapper

import com.project2.partner.contract.api.data.remote.models.PartnerContractResponse
import com.project2.partner.contract.api.domain.models.PartnerContractDomain
import com.project2.partner.points.api.data.remote.models.PointShortData
import com.project2.partner.points.api.domain.models.PointShortDomain

fun PartnerContractResponse.toDomain() = PartnerContractDomain(
    dailyRevenue = dailyRevenue,
    monthlyRevenue = monthlyRevenue,
    contractNumber = contractNumber,
    organizationName = organizationName,
    point = point?.toDomain()
)

fun PointShortData.toDomain() = PointShortDomain(
    id = id,
    name = name,
    address = address,
    pointTypes = types.map { it.localizedName },
    status = status
)