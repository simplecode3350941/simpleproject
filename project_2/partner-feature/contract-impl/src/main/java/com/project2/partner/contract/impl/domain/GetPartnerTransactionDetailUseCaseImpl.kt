package com.project2.partner.contract.impl.domain

import com.project2.partner.contract.api.domain.models.TransactionDetailDomain
import com.project2.partner.contract.api.domain.repository.PartnerTransactionsRepository
import com.project2.partner.contract.api.domain.usecase.GetPartnerTransactionDetailUseCase
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetPartnerTransactionDetailUseCaseImpl @Inject constructor(
    private val repository: PartnerTransactionsRepository
): GetPartnerTransactionDetailUseCase {

    override suspend fun invoke(id: String): Flow<TransactionDetailDomain> {
        return repository.getTransactionDetailInfo(id)
    }
}