package com.project2.partner.contract.impl.ui.employees.mapper

import com.project2.partner.contract.api.domain.models.EmployeeDomain
import com.project2.partner.contract.api.domain.models.EmployeePointDomain
import com.project2.partner.contract.impl.ui.employees.model.PartnerEmployeePointUi
import com.project2.partner.contract.impl.ui.employees.model.PartnerEmployeeUi

fun EmployeeDomain.toPresentation() = PartnerEmployeeUi(
    firstName = firstName,
    middleName = middleName,
    lastName = lastName,
    email = email,
    role = role,
    point = point.toPresentation()
)

fun EmployeePointDomain.toPresentation() = PartnerEmployeePointUi(
    id = id,
    name = name
)