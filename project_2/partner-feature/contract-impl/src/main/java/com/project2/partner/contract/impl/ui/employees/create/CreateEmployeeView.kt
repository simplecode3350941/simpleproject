package com.project2.partner.contract.impl.ui.employees.create

import com.project2.partner.contract.impl.ui.employees.model.PartnerEmployeeUi

interface CreateEmployeeView {

    fun onClickEmployeePoint()
    fun onClickEmployeeRole()
    fun onSaveEmployee(employee: PartnerEmployeeUi)
    fun onInitTextListenersAndViews()
    fun onSuccess()
    fun onSetFragmentResultListener()

    fun getEmployeeViewModel(): PartnerEmployeeAbsCreateViewModel
}