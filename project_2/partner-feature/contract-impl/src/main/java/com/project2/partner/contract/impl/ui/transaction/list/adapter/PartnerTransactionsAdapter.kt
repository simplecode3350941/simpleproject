package com.project2.partner.contract.impl.ui.transaction.list.adapter

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.core.utils.view.setOnClickDelayListener
import com.project2.partner.contract.impl.databinding.PartnerTransactionItemBinding
import com.project2.partner.contract.impl.ui.transaction.model.PartnerTransactionUi
import com.project2.partner.contract.impl.ui.transaction.model.allPointTypes
import com.project2.partner.contract.impl.ui.transaction.model.formattedAmount
import com.project2.partner.contract.impl.ui.transaction.model.getFriendlyDateTime

class PartnerTransactionsAdapter(
    private val onClickListener: (String) -> Unit
) : PagingDataAdapter<PartnerTransactionUi, PartnerTransactionsAdapter.ViewHolder>(
    DIFF_CALLBACK
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, onClickListener)
        }
    }

    inner class ViewHolder(parent: ViewGroup) :
        BindingViewHolder<PartnerTransactionItemBinding>(
            parent,
            PartnerTransactionItemBinding::inflate
        ) {

        fun bind(item: PartnerTransactionUi, onClickListener: (String) -> Unit) = with(binding) {
            textTransactionDate.text = item.getFriendlyDateTime(textTransactionDate.context)
            textTransactionPointName.text = item.pointName
            textTransactionPointType.text = item.allPointTypes
            textTransactionAmount.text = item.formattedAmount

            itemView.setOnClickDelayListener {
                onClickListener(item.id)
            }
        }
    }

    companion object {

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<PartnerTransactionUi>() {
            override fun areItemsTheSame(
                oldItem: PartnerTransactionUi,
                newItem: PartnerTransactionUi
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: PartnerTransactionUi,
                newItem: PartnerTransactionUi
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}