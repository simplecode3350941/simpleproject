package com.project2.partner.contract.impl.ui.contract

import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.partner.contract.impl.domain.GetPartnerContractWithProfileInteractor
import com.project2.partner.contract.impl.ui.contract.mapper.toPresentation
import com.project2.partner.contract.impl.ui.contract.models.ContractWithProfileUi
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import timber.log.Timber
import javax.inject.Inject

class PartnerContractViewModel @Inject constructor(
    private val contractInteractor: GetPartnerContractWithProfileInteractor,
    override val exceptionHandler: CoroutineExceptionHandler
): BaseViewModel() {

    private val _updateState = MutableStateFlow<UiState>(UiState.Loading)
    val updateState = _updateState.asStateFlow()

    private val _contract = MutableStateFlow<ContractWithProfileUi?>(null)
    val contractFlow = _contract.asStateFlow()
        .filterNotNull()

    init {
        getContract()
    }

    fun getContract() {
        launchOnViewModelScope {
            contractInteractor()
                .onStart { _updateState.value = UiState.Loading }
                .map { it.toPresentation() }
                .catch { _updateState.value = UiState.Error(it) }
                .collect {
                    Timber.d("collect - %s", it)
                    _contract.value = it
                    _updateState.value = UiState.Success
                }
        }
    }
}

sealed interface UiState {
    object Loading: UiState
    object Success: UiState
    data class Error(val error: Throwable): UiState
}