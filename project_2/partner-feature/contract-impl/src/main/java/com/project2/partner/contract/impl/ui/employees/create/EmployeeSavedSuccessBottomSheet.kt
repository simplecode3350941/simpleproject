package com.project2.partner.contract.impl.ui.employees.create

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.dialog.BaseBottomSheetDialogFragment
import com.project2.partner.contract.impl.R
import com.project2.partner.contract.impl.databinding.PartnerEmployeeCreatedSuccessfullBottomSheetBinding

class EmployeeSavedSuccessBottomSheet: BaseBottomSheetDialogFragment() {

    override val synthetic =
        SyntheticBinding(PartnerEmployeeCreatedSuccessfullBottomSheetBinding::inflate)

    private val employeeEmail: String
        get() = requireNotNull(arguments?.getString(PARTNER_EMPLOYEE_EMAIL))

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        dialog.setOnShowListener {
            (dialog.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout?)?.let {
                BottomSheetBehavior.from(it).state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        synthetic.binding.textViewCreateEmployeeMessage.text =
            getString(R.string.partner_employees_created_success_message, employeeEmail)

        synthetic.binding.buttonCreateEmployeeClose.setOnClickListener {
            sendResult()
        }

        (dialog as? BottomSheetDialog)?.let {
            it.setOnCancelListener { sendResult() }
        }
    }

    private fun sendResult() {
        setFragmentResult(REQUEST_KEY, bundleOf())
        dismiss()
    }

    companion object {
        const val TAG = "EmployeeSavedSuccessBottomSheet"
        private const val PARTNER_EMPLOYEE_EMAIL = "PARTNER_EMPLOYEE_EMAIL"
        const val REQUEST_KEY = "request_key"

        fun show(manager: FragmentManager, employeeEmail: String) {
            EmployeeSavedSuccessBottomSheet().apply {
                arguments = bundleOf(PARTNER_EMPLOYEE_EMAIL to employeeEmail)
            }.show(manager, TAG)
        }
    }
}