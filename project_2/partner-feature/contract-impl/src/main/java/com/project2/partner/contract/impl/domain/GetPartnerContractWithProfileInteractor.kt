package com.project2.partner.contract.impl.domain

import com.project2.partner.contract.api.domain.usecase.GetPartnerContractUseCase
import com.project2.partner.contract.impl.domain.models.ContractWithProfileDomain
import com.project2.partner.profile.api.domain.usecase.GetPartnerProfileUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

class GetPartnerContractWithProfileInteractor @Inject constructor(
    private val getContractUseCase: GetPartnerContractUseCase,
    private val getProfileUseCase: GetPartnerProfileUseCase
) {

    suspend operator fun invoke(): Flow<ContractWithProfileDomain> {
        return combine(getContractUseCase(), getProfileUseCase()) { contract, profile ->
            ContractWithProfileDomain(contract, profile)
        }
    }
}