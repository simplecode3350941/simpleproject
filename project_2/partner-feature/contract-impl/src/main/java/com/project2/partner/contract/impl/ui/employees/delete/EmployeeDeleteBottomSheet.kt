package com.project2.partner.contract.impl.ui.employees.delete

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.dialog.BaseMvvmBottomSheetDialogFragment
import com.project2.partner.contract.impl.R
import com.project2.partner.contract.impl.databinding.PartnerEmployeeDeleteBottomSheetBinding
import com.project2.partner.contract.impl.ui.employees.create.EmployeeSavedSuccessBottomSheet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber

class EmployeeDeleteBottomSheet: BaseMvvmBottomSheetDialogFragment() {

    override val synthetic =
        SyntheticBinding(PartnerEmployeeDeleteBottomSheetBinding::inflate)

    private val viewModel by viewModels<EmployeeDeleteViewModel> {
        viewModelFactory
    }

    private val employeeEmail: String
        get() = requireNotNull(arguments?.getString(PARTNER_EMPLOYEE_EMAIL))

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        dialog.setOnShowListener {
            (dialog.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout?)?.let {
                BottomSheetBehavior.from(it).state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        synthetic.binding.buttonCreateEmployeeCancel.setOnClickListener {
            dismiss()
        }

        synthetic.binding.buttonCreateEmployeeOk.setOnClickListener {
            viewModel.onDeleteEmployee(employeeEmail)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.updateState.collectLatest(::bindUiStates)
        }
    }
    private fun showToast(messages: String) {
        Toast.makeText(requireContext(), messages, Toast.LENGTH_LONG).show()
    }

    private fun bindUiStates(uiState: UiDeleteState) {
        when(uiState) {
            is UiDeleteState.Loading ->
                showHideLoading(true)
            is UiDeleteState.Error -> {
                Timber.e(uiState.error)
                showHideLoading(false)
                uiState.error.localizedMessage?.let { showToast(it) }
            }
            is UiDeleteState.Success -> {
                showHideLoading(false)
                showToast(getString(R.string.partner_employees_delete_success))
                sendResult()
            }
            is UiDeleteState.Empty -> {
            }
        }
    }

    private fun showHideLoading(show: Boolean) {
        synthetic.binding.layoutProgressBar.isVisible = show
    }

    private fun sendResult() {
        setFragmentResult(EmployeeSavedSuccessBottomSheet.REQUEST_KEY, bundleOf())
        dismiss()
    }

    companion object {
        const val TAG = "EMPLOYEE_DELETE_BOTTOM_SHEET"
        private const val PARTNER_EMPLOYEE_EMAIL = "PARTNER_EMPLOYEE_EMAIL"
        const val REQUEST_KEY = "request_key"

        fun show(manager: FragmentManager, employeeEmail: String) {
            EmployeeDeleteBottomSheet().apply {
                arguments = bundleOf(PARTNER_EMPLOYEE_EMAIL to employeeEmail)
            }.show(manager, TAG)
        }
    }
}