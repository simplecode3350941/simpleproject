package com.project2.partner.contract.impl.ui.di.transaction

import com.project2.core.presentation.di.ComponentDependencies
import com.project2.partner.contract.api.data.remote.api.PartnerTransactionsApi
import com.project2.partner.contract.api.domain.repository.PartnerTransactionsRepository
import com.project2.partner.contract.api.domain.usecase.GetPartnerTransactionDetailUseCase
import com.project2.partner.contract.api.domain.usecase.GetPartnerTransactionsUseCase

interface PartnerTransactionsDependencies: ComponentDependencies {

    fun transactionApi(): PartnerTransactionsApi

    fun transactionRepository(): PartnerTransactionsRepository

    fun getTransactionsUseCase(): GetPartnerTransactionsUseCase

    fun getTransactionDetailUseCase(): GetPartnerTransactionDetailUseCase
}