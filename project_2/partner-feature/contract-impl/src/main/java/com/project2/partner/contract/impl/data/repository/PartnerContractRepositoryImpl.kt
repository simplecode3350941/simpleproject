package com.project2.partner.contract.impl.data.repository

import com.project2.core.commons.di.scope.IoDispatcher
import com.project2.partner.contract.api.data.remote.api.PartnerContractApi
import com.project2.partner.contract.api.domain.models.PartnerContractDomain
import com.project2.partner.contract.api.domain.repository.PartnerContractRepository
import com.project2.partner.contract.impl.data.mapper.toDomain
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PartnerContractRepositoryImpl @Inject constructor(
    private val contractApi: PartnerContractApi,
    @IoDispatcher
    private val dispatcher: CoroutineDispatcher
) : PartnerContractRepository {

    override fun getContract(): Flow<PartnerContractDomain> {
        return flow {
            val contract = contractApi.getContract()
            emit(contract)
        }
            .map { it.toDomain() }
            .flowOn(dispatcher)
    }
}