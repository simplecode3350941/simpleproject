package com.project2.partner.contract.impl.ui.di.contract

import androidx.lifecycle.ViewModel
import com.project2.core.commons.di.viewmodule.ViewModelKey
import com.project2.partner.contract.impl.ui.contract.PartnerContractViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PartnerContractViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(PartnerContractViewModel::class)
    abstract fun partnerContractViewModel(viewModel: PartnerContractViewModel): ViewModel
}
