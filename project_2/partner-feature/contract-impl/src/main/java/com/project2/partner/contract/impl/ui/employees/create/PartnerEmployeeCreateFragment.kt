package com.project2.partner.contract.impl.ui.employees.create

import android.os.Bundle
import android.text.SpannableString
import android.widget.TextView
import androidx.annotation.CallSuper
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.ComponentDependenciesProvider
import com.project2.core.presentation.di.HasComponentDependencies
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.utils.common.isValidEmail
import com.project2.core.utils.common.lazyUnsafe
import com.project2.core.utils.view.getCompatColor
import com.project2.core.utils.view.tintLastSymbol
import com.project2.partner.contract.api.role.PartnerRole
import com.project2.partner.contract.impl.R
import com.project2.partner.contract.impl.databinding.FragmentPartnerEmployeeCreateBinding
import com.project2.partner.contract.impl.ui.di.employees.DaggerPartnerEmployeesComponent
import com.project2.partner.contract.impl.ui.employees.model.PartnerEmployeePointUi
import com.project2.partner.contract.impl.ui.employees.model.PartnerEmployeeUi
import com.project2.partner.point.selection.ui.PointSelectionUi
import com.project2.partner.point.selection.ui.select_service.communication.PointSelectionCommunication
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

open class PartnerEmployeeCreateFragment : BaseMvvmFragment(), CreateEmployeeView,
    HasComponentDependencies {

    override val synthetic = SyntheticBinding(FragmentPartnerEmployeeCreateBinding::inflate)

    @Inject
    override lateinit var dependencies: ComponentDependenciesProvider

    // TODO Delete init when role selection is realized.
    private val selectedRole: String?
        get() {
            return getString(R.string.partner_employees_operator)
        }

    private val viewModel: PartnerEmployeeCreateViewModel by viewModels {
        viewModelFactory
    }

    private val redColor by lazyUnsafe {
        requireContext().getCompatColor(R.color.red)
    }

    private val blackColor by lazyUnsafe {
        requireContext().getCompatColor(R.color.black)
    }

    private val greyColor by lazyUnsafe {
        requireContext().getCompatColor(R.color.grey_middle)
    }

    override fun getLayoutResId(): Int = R.layout.fragment_partner_employee_create

    override fun inject() {
        DaggerPartnerEmployeesComponent.builder()
            .partnerEmployeesDependencies(findComponentDependencies())
            .build()
            .inject(this)
    }

    override fun getEmployeeViewModel(): PartnerEmployeeAbsCreateViewModel = viewModel

    @CallSuper
    override fun initUi(savedInstanceState: Bundle?) {
        onInitBaseUi()
        onInitInputsHints()
        onInitTextListenersAndViews()
        onSetFragmentResultListener()

        onSetPointUi()
        onUpdatePointColor()

        // TODO remove to role subscription result fun when role selection is realized.
        onUpdateRoleUi()

        synthetic.binding.apply {

            buttonCreateEmployee.isVisible = true

            layoutEmployeePoint.setOnClickListener {
                onClickEmployeePoint()
            }
            layoutEmployeeRole.setOnClickListener {
                onClickEmployeeRole()
            }

            buttonCreateEmployee.setOnClickListener {
                saveEmployee()
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            getEmployeeViewModel().uiState.collectLatest {
                updateUiState(it)
            }
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.employeeSavedEmail.collectLatest(::showSuccessEmployeeCreatedBottomSheet)
        }

        viewLifecycleOwner.lifecycleScope.launch() {
            PointSelectionCommunication.subscribe {
                onUpdateSelectedPointData(it)
                onUpdatePointColor()
            }
        }
    }

    override fun onSetFragmentResultListener() {
        childFragmentManager.setFragmentResultListener(
            EmployeeSavedSuccessBottomSheet.REQUEST_KEY, viewLifecycleOwner
        ) { _, _ ->
            popBack()
        }
    }

    private fun onUpdateSelectedPointData(pointSelectionUi: PointSelectionUi) {
        if (pointSelectionUi.pointId.isNotEmpty() && pointSelectionUi.pointName.isNotEmpty()) {
            synthetic.binding.textViewEmployeePoint.text = pointSelectionUi.pointName
        }
    }

    override fun onClickEmployeePoint() {
        navigateTo(R.id.action_create_employees_to_point_selection)
    }

    override fun onClickEmployeeRole() {
        // TODO This when role selection is realized. Navigation to BS with role list
    }

    override fun onSaveEmployee(employee: PartnerEmployeeUi) {
        getEmployeeViewModel().onSaveEmployee(employee)
    }

    private fun onInitBaseUi() {
        synthetic.binding.apply {
            topbarPartnerEmployeeCreate.textViewNavbarTitle.text =
                getString(R.string.partner_employees_title)
            topbarPartnerEmployeeCreate.imgViewArrowBack.setOnClickListener {
                popBack()
            }
            // TODO Delete this when role selection is realized.
            imageViewEmployeeRoleArrow.isVisible = false
        }
    }

    private fun onInitInputsHints() {
        synthetic.binding.apply {
            inputLayoutEmployeeFirstName.hint =
                getHintColored(R.string.partner_employees_create_first_name_label)
            inputLayoutEmployeeLastName.hint =
                getHintColored(R.string.partner_employees_create_second_name_label)
            inputLayoutEmployeeEmail.hint =
                getHintColored(R.string.partner_employees_create_email_label)
        }
    }

    fun onUpdatePointColor() {
        val pointHint = getString(R.string.partner_employees_create_point_label)
        synthetic.binding.textViewEmployeePoint.setActiveColorIf {
            synthetic.binding.textViewEmployeePoint.text.isNotEmpty() &&
                    synthetic.binding.textViewEmployeePoint.text.toString() != pointHint
        }
    }

    private fun getCurrentPartnerEmployeeUi(): PartnerEmployeeUi? {
        var partnerEmployeeUi: PartnerEmployeeUi? = null
        synthetic.binding.apply {
            val role = when (selectedRole) {
                getString(R.string.partner_employees_operator) -> {
                    PartnerRole.OPERATOR
                }

                getString(R.string.partner_employees_owner) -> {
                    PartnerRole.OWNER
                }

                else -> null
            }
            val selectedPointName = textViewEmployeePoint.text.toString()
            val selectedPointId = PointSelectionCommunication.getPointId()
            if (selectedPointId.isNotEmpty() && selectedPointName.isNotEmpty() && role != null) {
                partnerEmployeeUi = PartnerEmployeeUi(
                    firstName = editTextEmployeeFirstName.text.toString(),
                    middleName = editTextEmployeeMiddleName.text.toString(),
                    lastName = editTextEmployeeLastName.text.toString(),
                    email = editTextEmployeeEmail.text.toString(),
                    point = PartnerEmployeePointUi(
                        id = selectedPointId,
                        name = selectedPointName
                    ),
                    role = role
                )
            }
        }
        return partnerEmployeeUi
    }

    private fun isRequiredFieldsFilled(): Boolean {
        synthetic.binding.apply {
            val firstName = editTextEmployeeFirstName.text.toString()
            val lastName = editTextEmployeeLastName.text.toString()
            val email = editTextEmployeeEmail.text.toString()

            val fieldsIsValid = mutableListOf<Boolean>()
            val errorMessage = getString(R.string.partner_employees_create_required_field_error)

            fieldsIsValid.add(firstName.isNotEmpty())
            if (firstName.isEmpty()) {
                inputLayoutEmployeeFirstName.error = errorMessage
            }

            fieldsIsValid.add(lastName.isNotEmpty())
            if (lastName.isEmpty()) {
                inputLayoutEmployeeLastName.error = errorMessage
            }

            val isEmailValid = isValidEmail(email)
            fieldsIsValid.add(email.isNotEmpty() && isEmailValid)
            if (email.isEmpty() || !isEmailValid) {
                inputLayoutEmployeeEmail.error = errorMessage
            }

            val selectedPointName = textViewEmployeePoint.text.toString()
            val selectedPointId = PointSelectionCommunication.getPointId()
            val pointIsValid = selectedPointId.isNotEmpty() && selectedPointName.isNotEmpty()
            fieldsIsValid.add(pointIsValid)
            textViewEmployeePointError.isVisible = !pointIsValid

            val roleIsValid = selectedRole != null
            fieldsIsValid.add(roleIsValid)
            textViewEmployeeRoleError.isVisible = !roleIsValid

            return !fieldsIsValid.contains(false)
        }
    }

    private fun onUpdateRoleUi() {
        synthetic.binding.textViewEmployeeRole.setActiveColorIf {
            selectedRole != null
        }

        synthetic.binding.textViewEmployeeRole.text =
            getHintColored(R.string.partner_employees_create_role_label)
        synthetic.binding.textViewEmployeeRoleError.isVisible = false

        if (selectedRole != null) {
            synthetic.binding.textViewEmployeeRole.text = selectedRole
        }
    }

    private fun saveEmployee() {
        if (isRequiredFieldsFilled()) {
            synthetic.binding.apply {
                val partnerEmployeeUi = getCurrentPartnerEmployeeUi()
                if (partnerEmployeeUi != null) {
                    onSaveEmployee(partnerEmployeeUi)
                }
            }
        }
    }

    private fun onSetPointUi() {
        val pointHintId = R.string.partner_employees_create_point_label
        if (synthetic.binding.textViewEmployeePoint.text == getString(pointHintId)) {
            synthetic.binding.textViewEmployeePoint.text =
                getHintColored(pointHintId)
            synthetic.binding.textViewEmployeePointError.isVisible = false
        }
    }

    override fun onInitTextListenersAndViews() {
        synthetic.binding.apply {
            editTextEmployeeFirstName.addTextChangedListener {
                if (!it.isNullOrEmpty()) {
                    inputLayoutEmployeeFirstName.error = null
                }
            }
            editTextEmployeeLastName.addTextChangedListener {
                if (!it.isNullOrEmpty()) {
                    inputLayoutEmployeeLastName.error = null
                }
            }
            editTextEmployeeEmail.addTextChangedListener {
                if (!it.isNullOrEmpty() && isValidEmail(it)) {
                    inputLayoutEmployeeEmail.error = null
                }
            }
        }
    }

    override fun onSuccess() {
        //Nothing to do
    }

    private fun showSuccessEmployeeCreatedBottomSheet(employeeEmail: String) {
        EmployeeSavedSuccessBottomSheet.show(childFragmentManager, employeeEmail)
    }

    private fun TextView.setActiveColorIf(predicate: () -> Boolean) {
        setTextColor(
            when {
                predicate() -> blackColor
                else -> greyColor
            }
        )
    }

    private fun getHintColored(@StringRes resId: Int): SpannableString {
        return getString(resId).tintLastSymbol(redColor)
    }

    private fun updateUiState(state: UiState) {
        Timber.d("state $state")
        when(state) {
            is UiState.Loading -> startLoading()
            is UiState.Error -> {
                stopLoading()
                showCommonErrorSnack(state.error)
                getEmployeeViewModel().resetUiState()
            }
            is UiState.Success -> {
                stopLoading()
                onSuccess()
            }
        }
    }

    protected fun navigateTo(@IdRes resId: Int) {
        findNavController().navigate(resId)
    }

    protected fun showHideProgressBar(show: Boolean) {
        val progressBar = synthetic.binding.progressBarEmployeeEdit
        progressBar.isVisible = show
    }
}