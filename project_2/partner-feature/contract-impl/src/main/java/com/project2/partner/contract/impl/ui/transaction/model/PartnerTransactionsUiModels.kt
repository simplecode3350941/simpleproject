package com.project2.partner.contract.impl.ui.transaction.model

import android.content.Context
import android.text.Spannable
import android.text.SpannableStringBuilder
import com.project2.core.utils.date.DateUtils
import com.project2.core.utils.formatter.FormatUtils
import com.project2.partner.contract.impl.R
import java.util.*

data class PartnerTransactionUi(
    val id: String,
    val pointName: String,
    val pointTypes: List<String>,
    val amount: Double,
    val date: String
)

data class PartnerTransactionDetailUi(
    val id: String,
    val point: TransactionDetailPointUi,
    val orderNumber: String,
    val pan: String,
    val amount: Double,
    val date: String,
    val services: List<TransactionServicesUi>
)

data class TransactionDetailPointUi(
    val id: String,
    val name: String,
    val types: List<String>,
    val address: String
)

data class TransactionServicesUi(
    val name: String,
    val price: Double,
    val quantity: Long,
    val serviceID: String
)

val PartnerTransactionDetailUi.allPointTypes: String
    get() {
        return getAllPointTypes(point.types)
    }

val PartnerTransactionUi.allPointTypes: String
    get() {
        return getAllPointTypes(pointTypes)
    }

val PartnerTransactionUi.formattedAmount: Spannable
    get() {

        return getFormattedAmount(amount = amount, hasPlus = true)
    }

val PartnerTransactionDetailUi.formattedAmount: Spannable
    get() {

        return getFormattedAmount(amount = amount, hasPlus = true, sizeOfFractional = 23)
    }

val TransactionServicesUi.priceWithQuantity: Spannable
    get() {
        return getFormattedAmount(amount = price, quantity = quantity, sizeOfFractional = 14)
    }

fun PartnerTransactionUi.getFriendlyDateTime(context: Context) : String {
    return DateUtils.toFriendlyDateTime(DateUtils.toDate(date), context)
}

fun PartnerTransactionDetailUi.getFriendlyDate(context: Context) : String {
    return DateUtils.toFriendlyDateTime(DateUtils.toDate(date), context)
}

private fun getAllPointTypes(pointTypes: List<String>): String {
    var allPointTypesResult = pointTypes.first()
    for (i in 1 until pointTypes.size) {
        allPointTypesResult = "$allPointTypesResult, ${pointTypes[i]}"
    }
    return allPointTypesResult
}

private fun getFormattedAmount(
    amount: Double,
    hasPlus: Boolean = false,
    quantity: Long = 0,
    sizeOfFractional: Int = 18
): Spannable {
    var quantityString = ""
    if (quantity > 1 ) {
        quantityString = "$quantity х "
    }
    var plusString = ""
    if (hasPlus) {
        plusString = "+"
    }
    return SpannableStringBuilder()
        .append(quantityString)
        .append(plusString)
        .append(
            FormatUtils.formattedFractionalPrice(
                price = amount,
                sizeOfFractional = sizeOfFractional
            )
        )
}