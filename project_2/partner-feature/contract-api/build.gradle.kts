@file:Suppress("UnstableApiUsage")

@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.kapt)
    alias(libs.plugins.kotlin.parcelize)
    alias(libs.plugins.navigation.safeargs)
}

android {
    namespace = "com.project2.partner.contract.api"
    compileSdk = libs.versions.compileSdk.get().toInt()

    defaultConfig {
        minSdk = libs.versions.minSdk.get().toInt()
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    packagingOptions {
        resources.excludes += "META-INF/notice.txt"
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_11.toString()
    }

    kapt {
        correctErrorTypes = true
        generateStubs = true
    }

    viewBinding {
        android.buildFeatures.viewBinding = true
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            consumerProguardFile("partner-contract-proguard-rules.pro")
        }
    }
}

dependencies {
    api(project(":partner-feature:points-api"))
    implementation(libs.retrofit)
    implementation(libs.kotlinx.coroutines.android)
}