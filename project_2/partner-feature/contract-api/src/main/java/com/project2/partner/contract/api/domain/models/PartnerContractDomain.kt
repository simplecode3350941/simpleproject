package com.project2.partner.contract.api.domain.models

import com.project2.partner.points.api.domain.models.PointShortDomain

class PartnerContractDomain(
    val dailyRevenue: Double,
    val monthlyRevenue: Double,
    val contractNumber: String,
    val organizationName: String,
    val point: PointShortDomain?
)