package com.project2.partner.contract.api.domain.usecase

import kotlinx.coroutines.flow.Flow

interface DeleteEmployeeUseCase {
    suspend operator fun invoke(email: String): Flow<Boolean>
}