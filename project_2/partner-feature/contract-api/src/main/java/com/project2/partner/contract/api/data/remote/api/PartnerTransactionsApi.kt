package com.project2.partner.contract.api.data.remote.api

import com.project2.partner.contract.api.data.remote.models.transactions.TransactionDetailResponse
import com.project2.partner.contract.api.data.remote.models.transactions.TransactionResponse
import com.project2.partner.contract.api.data.remote.models.transactions.Transactions
import retrofit2.http.GET
import retrofit2.http.Query

interface PartnerTransactionsApi {

    @GET("/partners/transactions/list")
    suspend fun getTransactions(
        @Query("page") pageNumber: Int,
        @Query("size") pageSize: Int
    ): Transactions<TransactionResponse>

    @GET("/partners/transactions/transaction")
    suspend fun getTransactionDetailInfo(@Query("id") id: String): TransactionDetailResponse
}