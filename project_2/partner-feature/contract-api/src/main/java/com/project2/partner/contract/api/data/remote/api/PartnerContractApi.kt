package com.project2.partner.contract.api.data.remote.api

import com.project2.partner.contract.api.data.remote.models.PartnerContractResponse
import retrofit2.http.GET

interface PartnerContractApi {

    @GET("/partners/transactions/revenue")
    suspend fun getContract(): PartnerContractResponse
}