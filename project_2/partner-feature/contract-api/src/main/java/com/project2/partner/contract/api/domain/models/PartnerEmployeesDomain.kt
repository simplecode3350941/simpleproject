package com.project2.partner.contract.api.domain.models

import com.project2.partner.contract.api.role.PartnerRole

class EmployeeDomain(
    val firstName: String,
    val middleName: String?,
    val lastName: String,
    val email: String,
    val role: PartnerRole,
    val point: EmployeePointDomain
)

class EmployeePointDomain(
    val id: String,
    val name: String
)