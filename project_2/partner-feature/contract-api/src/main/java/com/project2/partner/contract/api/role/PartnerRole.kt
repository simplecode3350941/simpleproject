package com.project2.partner.contract.api.role

import androidx.annotation.Keep

@Keep
enum class PartnerRole {
    OWNER,
    OPERATOR
}