 package com.project2.partner.contract.api.data.remote.models

import com.project2.partner.points.api.data.remote.models.PointShortData

class PartnerContractResponse(
    val dailyRevenue: Double,
    val monthlyRevenue: Double,
    val contractNumber: String,
    val organizationName: String,
    val point: PointShortData?
)