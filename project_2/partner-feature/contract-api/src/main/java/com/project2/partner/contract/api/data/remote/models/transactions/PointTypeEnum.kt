package com.project2.partner.contract.api.data.remote.models.transactions

enum class PointTypeEnum {
    TIREFITTING,
    CARWASH,
    CARSERVICE,
    HOTEL,
    PARKING
}