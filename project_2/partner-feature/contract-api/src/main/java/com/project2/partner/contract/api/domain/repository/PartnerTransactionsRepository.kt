package com.project2.partner.contract.api.domain.repository

import com.project2.partner.contract.api.domain.models.TransactionDetailDomain
import com.project2.partner.contract.api.domain.models.TransactionDomain
import kotlinx.coroutines.flow.Flow

interface PartnerTransactionsRepository {
    suspend fun getTransactionsList(pageNumber: Int, pageSize: Int): Flow<List<TransactionDomain>>

    suspend fun getTransactionDetailInfo(id: String): Flow<TransactionDetailDomain>
}