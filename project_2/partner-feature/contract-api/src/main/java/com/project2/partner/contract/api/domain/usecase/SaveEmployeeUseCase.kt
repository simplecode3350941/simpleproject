package com.project2.partner.contract.api.domain.usecase

import com.project2.partner.contract.api.domain.models.EmployeeDomain
import kotlinx.coroutines.flow.Flow

interface SaveEmployeeUseCase {
    suspend operator fun invoke(employee: EmployeeDomain): Flow<String>
}