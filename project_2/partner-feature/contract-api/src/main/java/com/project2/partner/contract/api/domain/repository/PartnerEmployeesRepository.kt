package com.project2.partner.contract.api.domain.repository

import com.project2.partner.contract.api.domain.models.EmployeeDomain
import kotlinx.coroutines.flow.Flow

interface PartnerEmployeesRepository {
    suspend fun getEmployeesList(pageNumber: Int, pageSize: Int): Flow<List<EmployeeDomain>>

    suspend fun getEmployeeDetailInfo(email: String): Flow<EmployeeDomain>

    suspend fun saveEmployee(employee: EmployeeDomain): Flow<String>

    suspend fun updateEmployee(employee: EmployeeDomain): Flow<Boolean>

    suspend fun deleteEmployee(email: String): Flow<Boolean>

    suspend fun changePasswordEmployee(email: String): Flow<Boolean>
}