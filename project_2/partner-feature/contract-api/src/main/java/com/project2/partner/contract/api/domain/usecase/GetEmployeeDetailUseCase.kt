package com.project2.partner.contract.api.domain.usecase

import com.project2.partner.contract.api.domain.models.EmployeeDomain
import kotlinx.coroutines.flow.Flow

interface GetEmployeeDetailUseCase {

    suspend operator fun invoke(email: String): Flow<EmployeeDomain>
}