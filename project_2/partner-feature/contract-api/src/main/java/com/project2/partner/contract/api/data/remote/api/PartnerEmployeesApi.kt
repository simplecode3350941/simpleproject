package com.project2.partner.contract.api.data.remote.api

import com.project2.partner.contract.api.data.remote.models.Employees
import com.project2.partner.contract.api.data.remote.models.EmployeeDto
import com.project2.partner.contract.api.data.remote.models.EmployeeResponse
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Query

interface PartnerEmployeesApi {

    @GET("/partners/employees/list")
    suspend fun getEmployees(
        @Query("page") page: Int,
        @Query("size") pageSize: Int
    ): Employees<EmployeeDto>

    @GET("/partners/employees/employee")
    suspend fun getEmployeeDetailInfo(@Query("email") email: String): EmployeeDto

    @POST("/partners/employees/employee")
    suspend fun saveEmployee(@Body body: EmployeeDto): EmployeeResponse

    @PUT("/partners/employees/employee")
    suspend fun updateEmployee(@Body body: EmployeeDto)

    @DELETE("/partners/employees/employee")
    suspend fun deleteEmployee(@Query("email") email: String)

    @POST("/partners/employees/employee/reset")
    suspend fun changePasswordEmployee(@Query("email") email: String)
}