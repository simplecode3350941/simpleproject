package com.project2.partner.contract.api.domain.repository

import com.project2.partner.contract.api.domain.models.PartnerContractDomain
import kotlinx.coroutines.flow.Flow

interface PartnerContractRepository {

    fun getContract(): Flow<PartnerContractDomain>
}