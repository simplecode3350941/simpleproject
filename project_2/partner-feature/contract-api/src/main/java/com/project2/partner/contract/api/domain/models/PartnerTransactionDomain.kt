package com.project2.partner.contract.api.domain.models

class TransactionDomain(
    val id: String,
    val pointName: String,
    val pointTypes: List<String>,
    val amount: Double,
    val date: String
)

class TransactionDetailDomain(
    val id: String,
    val point: TransactionDetailPointDomain,
    val orderNumber: String,
    val pan: String,
    val amount: Double,
    val date: String,
    val services: List<TransactionServicesDomain>
)

class TransactionDetailPointDomain(
    val id: String,
    val name: String,
    val types: List<String>,
    val address: String
)

class TransactionServicesDomain(
    val name: String,
    val price: Double,
    val quantity: Long,
    val serviceID: String
)