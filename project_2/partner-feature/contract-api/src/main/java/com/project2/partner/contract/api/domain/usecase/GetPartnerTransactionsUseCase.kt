package com.project2.partner.contract.api.domain.usecase

import com.project2.partner.contract.api.domain.models.TransactionDomain
import kotlinx.coroutines.flow.Flow

interface GetPartnerTransactionsUseCase {
    suspend operator fun invoke(pageNumber: Int, pageSize: Int): Flow<List<TransactionDomain>>
}