package com.project2.partner.contract.api.data.remote.models.transactions

class Transactions<T>(
    val content: List<T>
)

class TransactionResponse(
    val id: String,
    val pointName: String,
    val pointTypes: List<TransactionPointType>,
    val amount: Double,
    val date: String
)

class TransactionPointType(
    val localizedName: String,
    val type: String
)

class TransactionDetailResponse(
    val id: String,
    val point: TransactionDetailPoint,
    val orderNumber: String,
    val pan: String,
    val amount: Double,
    val date: String,
    val services: List<TransactionServices>
)

class TransactionDetailPoint(
    val id: String,
    val name: String,
    val types: List<TransactionPointType>,
    val address: String
)
 class TransactionServices(
     val name: String,
     val price: Double,
     val quantity: Long,
     val serviceID: String
 )