package com.project2.partner.contract.api.domain.usecase

import com.project2.partner.contract.api.domain.models.TransactionDetailDomain
import kotlinx.coroutines.flow.Flow

interface GetPartnerTransactionDetailUseCase {
    suspend operator fun invoke(id: String): Flow<TransactionDetailDomain>
}