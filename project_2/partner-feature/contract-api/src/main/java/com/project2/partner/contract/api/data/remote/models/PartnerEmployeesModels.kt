package com.project2.partner.contract.api.data.remote.models

import com.project2.partner.contract.api.role.PartnerRole

class Employees<T>(
    val content: List<T>
)

class EmployeeDto(
    val firstName: String,
    val middleName: String?,
    val lastName: String,
    val email: String,
    val role: PartnerRole,
    val point: EmployeePoint
)

class EmployeePoint(
    val id: String,
    val name: String
)

class EmployeeResponse(
    val firstName: String,
    val middleName: String?,
    val lastName: String,
    val email: String
)