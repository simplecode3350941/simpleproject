package com.project2.partner.contract.api.domain.usecase

import com.project2.partner.contract.api.domain.models.PartnerContractDomain
import kotlinx.coroutines.flow.Flow

interface GetPartnerContractUseCase {
    operator fun invoke(): Flow<PartnerContractDomain>
}