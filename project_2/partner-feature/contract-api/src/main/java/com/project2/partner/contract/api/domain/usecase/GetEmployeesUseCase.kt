package com.project2.partner.contract.api.domain.usecase

import com.project2.partner.contract.api.domain.models.EmployeeDomain
import kotlinx.coroutines.flow.Flow

interface GetEmployeesUseCase {
    suspend operator fun invoke(pageNumber: Int, pageSize: Int): Flow<List<EmployeeDomain>>
}