@file:Suppress("UnstableApiUsage")

@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.kapt)
    alias(libs.plugins.kotlin.parcelize)
    alias(libs.plugins.navigation.safeargs)
}

android {
    namespace = "com.project2.partner.navigation"
    compileSdk = libs.versions.compileSdk.get().toInt()

    defaultConfig {
        minSdk = libs.versions.minSdk.get().toInt()
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    packagingOptions {
        resources.excludes += "META-INF/notice.txt"
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_11.toString()
    }

    kapt {
        correctErrorTypes = true
        generateStubs = true
    }

    viewBinding {
        android.buildFeatures.viewBinding = true
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            consumerProguardFile("partner-navigation-proguard-rules.pro")
        }
    }
}

dependencies {

    implementation(project(":core:presentation"))
    implementation(project(":core:network"))
    implementation(project(":core:resources"))

    implementation(libs.navigation.fragment.ktx)
    implementation(libs.navigation.ui.ktx)
}