package com.project2.partner.auth.api.data.remote.models

class AuthRequestBody(val login: String,
                      val password: String,
                      val grantType: String? = null)