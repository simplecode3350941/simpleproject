package com.project2.partner.auth.api.domain.repository

import com.project2.partner.auth.api.domain.models.AuthTokensDomain
import kotlinx.coroutines.flow.Flow

interface PartnerAuthRepository {

    fun signIn(login: String, password: String): Flow<AuthTokensDomain>

    fun singOut(refreshToken: String): Flow<Unit>

    fun changePassword(
        username: String,
        oldPassword: String,
        newPassword: String
    ): Flow<Unit>
}
