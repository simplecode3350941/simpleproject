package com.project2.partner.auth.api.domain.models

class AuthTokensDomain(
    val accessToken: String,
    val refreshToken: String
)