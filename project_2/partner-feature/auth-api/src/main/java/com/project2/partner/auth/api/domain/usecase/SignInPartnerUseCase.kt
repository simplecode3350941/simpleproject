package com.project2.partner.auth.api.domain.usecase

import kotlinx.coroutines.flow.Flow

interface SignInPartnerUseCase {
    operator fun invoke(login: String, password: String): Flow<Boolean>
}