package com.project2.partner.auth.api.domain.usecase

import kotlinx.coroutines.flow.Flow

interface SignOutPartnerUseCase {

    operator fun invoke(): Flow<Boolean>

}