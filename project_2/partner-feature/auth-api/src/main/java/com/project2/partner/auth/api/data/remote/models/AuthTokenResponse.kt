package com.project2.partner.auth.api.data.remote.models

class AuthTokenResponse(
    val accessToken: String,
    val refreshToken: String
)