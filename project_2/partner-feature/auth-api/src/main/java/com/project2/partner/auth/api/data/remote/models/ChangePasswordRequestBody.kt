package com.project2.partner.auth.api.data.remote.models

class ChangePasswordRequestBody(
    val username: String,
    val oldPassword: String,
    val newPassword: String
)
