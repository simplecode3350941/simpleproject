package com.project2.partner.auth.api.domain.usecase

import kotlinx.coroutines.flow.Flow

interface ChangePasswordPartnerUseCase {
    operator fun invoke(
        username: String,
        oldPassword: String,
        newPassword: String
    ): Flow<Boolean>
}
