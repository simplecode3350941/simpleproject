package com.project2.partner.auth.api.data.remote.models

class SignOutRequestBody(val refreshToken: String)