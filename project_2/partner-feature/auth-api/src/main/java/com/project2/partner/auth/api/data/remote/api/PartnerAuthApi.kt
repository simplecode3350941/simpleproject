package com.project2.partner.auth.api.data.remote.api

import com.project2.core.network.call_adapter.ApiError
import com.project2.core.network.call_adapter.ApiResult
import com.project2.partner.auth.api.data.remote.models.AuthRequestBody
import com.project2.partner.auth.api.data.remote.models.AuthTokenResponse
import com.project2.partner.auth.api.data.remote.models.ChangePasswordRequestBody
import com.project2.partner.auth.api.data.remote.models.SignOutRequestBody
import retrofit2.http.Body
import retrofit2.http.POST

interface PartnerAuthApi {

    @POST("/partners/auth/login")
    suspend fun signIn(@Body authRequestBody: AuthRequestBody): ApiResult<AuthTokenResponse, ApiError>

    @POST("/partners/auth/signout")
    suspend fun signOut(@Body signOutRequestBody: SignOutRequestBody)

    @POST("/partners/auth/change/password")
    suspend fun changePassword(@Body changePasswordRequestBody: ChangePasswordRequestBody)
}
