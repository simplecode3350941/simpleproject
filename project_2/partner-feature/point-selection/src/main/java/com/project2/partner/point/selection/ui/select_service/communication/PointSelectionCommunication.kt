package com.project2.partner.point.selection.ui.select_service.communication

import com.project2.partner.point.selection.ui.PointSelectionUi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.updateAndGet

object PointSelectionCommunication {

    private val flow = MutableStateFlow(PointSelectionUi())
    private val pointSelectionFlow = flow.asStateFlow()

    fun clear() {
        flow.value = PointSelectionUi()
    }

    suspend fun subscribe(onStateChanged: (PointSelectionUi) -> Unit) {
        pointSelectionFlow.collectLatest {
            onStateChanged.invoke(it)
        }
    }

    suspend fun setPointSelectedIdAndName(pointId: String, pointName: String) {
        modifyAndEmit {
            it.copy(pointId = pointId, pointName = pointName)
        }
    }

    fun getPointId(): String = flow.value.pointId

    private suspend fun modifyAndEmit(onModifyPoint: (PointSelectionUi) -> PointSelectionUi) {
        val createPoint = flow.updateAndGet { onModifyPoint(it) }
        flow.emit(createPoint)
    }
}