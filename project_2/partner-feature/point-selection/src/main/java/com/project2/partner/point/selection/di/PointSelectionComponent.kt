package com.project2.partner.point.selection.di

import com.project2.core.presentation.di.ComponentDependenciesProviderModule
import com.project2.partner.point.selection.ui.select_service.PointSelectionFragment
import dagger.Component


@Component(
    modules = [ComponentDependenciesProviderModule::class],
    dependencies = [PointSelectionDependencies::class]
)
interface PointSelectionComponent {
    fun inject(pointSelectionFragment: PointSelectionFragment)
}