package com.project2.partner.point.selection.ui

import com.project2.partner.points.api.domain.models.PointShortDomain
import com.project2.partner.points.api.ui.list.models.MODERATION_STATUSES
import com.project2.partner.points.api.ui.list.models.PointShortUi
import com.project2.partner.points.api.ui.list.models.PointStatus

fun PointShortDomain.toPresentation() = PointShortUi(
    id = id,
    name = name,
    address = address,
    pointTypes = pointTypes,
    formattedPointTypes = pointTypes.joinToString(", "),
    status = status.toPointStatus()
)
fun String.toPointStatus(): PointStatus {
    return when (this) {
        "ACTIVE" -> PointStatus.ACTIVE
        "DECLINED" -> PointStatus.DECLINED
        "HIDDEN" -> PointStatus.HIDDEN
        in MODERATION_STATUSES -> PointStatus.MODERATION
        else -> PointStatus.ACTIVE
    }
}