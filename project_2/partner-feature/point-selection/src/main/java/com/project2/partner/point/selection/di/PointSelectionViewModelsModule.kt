package com.project2.partner.point.selection.di

import androidx.lifecycle.ViewModel
import com.project2.core.commons.di.viewmodule.ViewModelKey
import com.project2.partner.point.selection.ui.select_service.PointSelectionViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PointSelectionViewModelsModule {
    @Binds
    @IntoMap
    @ViewModelKey(PointSelectionViewModel::class)
    abstract fun partnerPointSelectionViewModel(
        viewModel: PointSelectionViewModel
    ): ViewModel
}