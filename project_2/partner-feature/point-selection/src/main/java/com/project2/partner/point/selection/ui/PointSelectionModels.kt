package com.project2.partner.point.selection.ui

data class PointSelectionUi(
    val pointId: String = "",
    val pointName: String = ""
)