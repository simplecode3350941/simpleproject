package com.project2.partner.point.selection.ui.select_service.adapter

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.project2.core.presentation.binding.BindingViewHolder
import com.project2.core.utils.view.setOnClickDelayListener
import com.project2.partner.point.selection.databinding.PointSelectionItemBinding
import com.project2.partner.points.api.ui.list.models.PointShortUi

class PartnerServiceAdapter(
    private val onClickListener: (String, String, String) -> Unit
): PagingDataAdapter<PointShortUi, PartnerServiceAdapter.ViewHolder>(
    DIFF_CALLBACK
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, onClickListener)
        }
    }

    inner class ViewHolder(parent: ViewGroup):
        BindingViewHolder<PointSelectionItemBinding>(parent, PointSelectionItemBinding::inflate)
    {

        fun bind(item: PointShortUi, onClickListener: (String, String, String) -> Unit) = with(binding) {
            textViewServiceName.text = item.name
            textViewServiceType.text = item.formattedPointTypes
            textViewServiceAddress.text = item.address

            itemView.setOnClickDelayListener {
                onClickListener(item.id, item.name, item.address)
            }
        }
    }

    companion object {

        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<PointShortUi>() {
            override fun areItemsTheSame(oldItem: PointShortUi, newItem: PointShortUi): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: PointShortUi, newItem: PointShortUi): Boolean {
                return oldItem == newItem
            }
        }
    }
}