package com.project2.partner.point.selection.ui.select_service

import android.os.Bundle
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import com.project2.core.presentation.binding.SyntheticBinding
import com.project2.core.presentation.di.ComponentDependenciesProvider
import com.project2.core.presentation.di.HasComponentDependencies
import com.project2.core.presentation.di.findComponentDependencies
import com.project2.core.presentation.fragment.BaseMvvmFragment
import com.project2.core.presentation.paging.FooterAdapter
import com.project2.core.presentation.paging.addOnInfiniteScrollListener
import com.project2.core.utils.common.lazyUnsafe
import com.project2.core.utils.view.dpToPx
import com.project2.core.utils.view.setOnClickDelayListener
import com.project2.partner.point.selection.ui.select_service.adapter.PartnerServiceAdapter
import com.project2.partner.point.selection.R
import com.project2.partner.point.selection.databinding.FragmentPointSelectionBinding
import com.project2.partner.point.selection.di.DaggerPointSelectionComponent
import com.project2.partner.point.selection.ui.select_service.communication.PointSelectionCommunication
import com.project2.partner.points.api.ui.list.models.PointShortUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class PointSelectionFragment: BaseMvvmFragment(), HasComponentDependencies {

    @Inject
    override lateinit var dependencies: ComponentDependenciesProvider

    override val synthetic =
        SyntheticBinding(FragmentPointSelectionBinding::inflate)

    private val viewModel: PointSelectionViewModel by viewModels {
        viewModelFactory
    }

    private val pointsAdapter: PartnerServiceAdapter by lazyUnsafe {
        PartnerServiceAdapter(::onPointClick)
    }

    private val footerAdapter: FooterAdapter by lazyUnsafe {
        FooterAdapter(onEndReached)
    }

    private val onEndReached: () -> Unit = { viewModel.onPaginate() }

    override fun inject() {
        DaggerPointSelectionComponent.builder()
            .pointSelectionDependencies(findComponentDependencies())
            .build()
            .inject(this)
    }

    override fun getLayoutResId(): Int = R.layout.fragment_point_selection

    override fun initUi(savedInstanceState: Bundle?) {
        synthetic.binding.includeViewSelectServiceAppbar.textViewNavbarTitle.text =
            getString(R.string.partner_order_select_service_title)

        synthetic.binding.includeViewSelectServiceAppbar.imgViewArrowBack.setOnClickListener {
            popBack()
        }

        showHideCancelTextView(false)

        synthetic.binding.rvViewSelectService.apply {
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = pointsAdapter.withLoadStateFooter(footerAdapter)
            setHasFixedSize(true)
            clearOnScrollListeners()
            addOnInfiniteScrollListener(onEndReached)
        }

        synthetic.binding.swipeViewSelectServiceLayout.apply {
            setOnRefreshListener {
                val searchQuery = synthetic.binding.editTextSearch.text.toString().trim()
                if (searchQuery.isEmpty()) {
                    viewModel.onRefreshData()
                } else {
                    viewModel.searchServices(searchQuery)
                }
                synthetic.binding.swipeViewSelectServiceLayout.isRefreshing = false
            }
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.updateState.collectLatest(::updateContentState)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.paginationState.collectLatest(::updatePaginationState)
        }

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.servicesFlow.collectLatest(::submitServices)
        }

        synthetic.binding.editTextSearch.doAfterTextChanged { text ->
            showHideCancelTextView(!text.isNullOrEmpty())
            if (!text.isNullOrEmpty()) {
                viewModel.searchServices(text.toString())
            }
        }

        synthetic.binding.textViewCancel.setOnClickDelayListener {
            synthetic.binding.editTextSearch.text = null
            showHideCancelTextView(false)
            viewModel.onRefreshData()
        }
    }

    private fun showHideCancelTextView(show: Boolean) {
        val cancelView = synthetic.binding.textViewCancel
        val searchView = synthetic.binding.editTextSearch
        if (show) {
            searchView.updateLayoutParams<ConstraintLayout.LayoutParams> {
                endToStart = cancelView.id
                marginEnd = requireContext().dpToPx(16F)
            }
            cancelView.isVisible = true
        } else {
            cancelView.isVisible = false
            searchView.updateLayoutParams<ConstraintLayout.LayoutParams> {
                marginEnd = 0
            }
        }
    }

    private fun onPointClick(id: String, name: String, address: String) {
       when (findNavController().previousBackStackEntry?.destination?.id) {
           R.id.partner_orders -> {
               findNavController().navigate(
                   R.id.action_pointSelection_to_priceList,
                   bundleOf(
                       PARTNER_POINT_ID_KEY to id,
                       PARTNER_POINT_NAME_KEY to name,
                       PARTNER_POINT_ADDRESS_KEY to address
                   )
               )
           }
           R.id.partner_employee_create, R.id.partner_employee_edit -> {
               viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
                   PointSelectionCommunication.setPointSelectedIdAndName(id, name)
                   findNavController().popBackStack()
               }
           }
       }
    }

    private fun updateContentState(uiState: UiState) {
        Timber.d("updateContentState $uiState")
        when(uiState) {
            is UiState.Loading -> {
                showHideLoading(true)
                showHideContent(false)
            }
            is UiState.Error -> {
                Timber.e(uiState.error)
                showHideLoading(false)
                showErrorView(uiState.error.localizedMessage)
                showHideContent(false)
            }
            is UiState.Empty -> {
                showHideLoading(false)
                showEmptyView()
                showHideContent(false)
            }
            is UiState.Success -> {
                showHideLoading(false)
                showHideContent(true)
            }
        }
    }

    private fun updatePaginationState(paginationState: PaginationState) {
        Timber.d("updatePaginationState $paginationState")
        val swipeRefreshLayout = synthetic.binding.swipeViewSelectServiceLayout

        when(paginationState) {
            is PaginationState.Loading -> footerAdapter.loadState = LoadState.Loading
            is PaginationState.Error -> {
                swipeRefreshLayout.isRefreshing = false
                footerAdapter.loadState = LoadState.Error(paginationState.error)
            }
            is PaginationState.Success -> {
                swipeRefreshLayout.isRefreshing = false
                footerAdapter.loadState = LoadState.NotLoading(false)
            }
        }
    }

    private fun showHideContent(show: Boolean) {
        synthetic.binding.rvViewSelectService.isVisible = show
    }

    private fun showHideLoading(show: Boolean) {
        val shimmer = synthetic.binding.includeViewServiceLoadingView.shimmerViewService
        val emptyView = synthetic.binding.includeViewServiceEmptyView.emptyView
        val swipeRefreshLayout = synthetic.binding.swipeViewSelectServiceLayout

        if (show) {
            emptyView.isVisible = false
            shimmer.showShimmer(true)
        } else {
            shimmer.hideShimmer()
        }
        swipeRefreshLayout.isVisible = !show
        shimmer.isVisible = show
    }

    private fun showErrorView(error: String?) {
        val errorView = synthetic.binding.includeViewSelectServiceErrorView.errorView
        if (!error.isNullOrEmpty()) {
            synthetic.binding.includeViewSelectServiceErrorView.textViewErrorMessage.text = error
        }
        errorView.isVisible = true
        synthetic.binding.swipeViewSelectServiceLayout.isVisible = false
        synthetic.binding.includeViewSelectServiceErrorView.buttonRefresh.setOnClickListener {
            errorView.isVisible = false
            viewModel.onRefreshData()
        }
    }

    private fun showEmptyView() {
        val emptyView = synthetic.binding.includeViewServiceEmptyView.emptyView
        emptyView.isVisible = true
        synthetic.binding.swipeViewSelectServiceLayout.isVisible = false
    }

    private fun submitServices(services: List<PointShortUi>) {
        Timber.d("showServices size ${services.size}")
        pointsAdapter.submitData(viewLifecycleOwner.lifecycle, PagingData.from(services))
    }

    companion object {
        const val PARTNER_POINT_ID_KEY = "partner_point_id_key"
        const val PARTNER_POINT_NAME_KEY = "partner_point_name_key"
        const val PARTNER_POINT_ADDRESS_KEY = "partner_point_address_key"
    }
}