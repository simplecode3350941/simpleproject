package com.project2.partner.point.selection.di

import com.project2.core.presentation.di.ComponentDependencies

interface PointSelectionDependencies: ComponentDependencies {
}