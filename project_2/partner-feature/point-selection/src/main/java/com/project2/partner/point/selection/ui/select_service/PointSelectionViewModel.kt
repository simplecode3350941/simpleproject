package com.project2.partner.point.selection.ui.select_service

import com.project2.core.presentation.paging.Paginator
import com.project2.core.presentation.viewmodel.BaseViewModel
import com.project2.core.utils.common.toImmutableList
import com.project2.partner.point.selection.ui.toPresentation
import com.project2.partner.points.api.domain.usecase.GetPartnerListPointsUseCase
import com.project2.partner.points.api.ui.list.models.PointShortUi
import com.project2.partner.points.api.ui.list.models.PointStatus
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

@OptIn(FlowPreview::class)
class PointSelectionViewModel @Inject constructor(
    private val getServices: GetPartnerListPointsUseCase,
    override val exceptionHandler: CoroutineExceptionHandler
) : BaseViewModel() {

    private val _updateState = MutableStateFlow<UiState>(UiState.Loading)
    val updateState = _updateState.asStateFlow()

    private val _paginationState = MutableStateFlow<PaginationState?>(null)
    val paginationState = _paginationState.asStateFlow().filterNotNull()

    private val _servicesFlow = MutableStateFlow<List<PointShortUi>>(listOf())
    val servicesFlow = _servicesFlow.asStateFlow()

    private val services: List<PointShortUi>
        get() = _servicesFlow.value.toImmutableList()

    private val paginator = Paginator()

    private val isPagination: Boolean
        get() = paginator.getCurrentPage() > 0

    private var searchFlow = MutableStateFlow("")

    private var jobForCancel: Job? = null

    init {
        onRefreshData()
    }

    fun onRefreshData() {
        searchFlow.value = ""
        jobForCancel?.cancel()
        paginator.reset()
        getServicesList()
    }

    fun onPaginate() {
        if (paginator.hasData()) {
            getServicesList()
        }
    }

    fun searchServices(query: String?) {
        val queryStr = query?.trim()
        paginator.reset()
        when {
            queryStr.isNullOrEmpty() -> {
                searchFlow.value = ""
                _servicesFlow.value = listOf()
            }
            else -> {
                searchFlow.value = queryStr
            }
        }
    }

    private fun getServicesList() {
        jobForCancel = launchOnViewModelScope {
            searchFlow
                .debounce(500)
                .distinctUntilChanged()
                .collectLatest { query ->
                    getServices(
                        paginator.getCurrentPage(),
                        paginator.pageSize,
                        PointStatus.ACTIVE.toString(),
                        query
                    )
                        .loadingState()
                        .map { services -> services.map { it.toPresentation() } }
                        .errorState()
                        .successState()
                }
        }
    }

    private fun <T> Flow<T>.loadingState(): Flow<T> {
        return onStart {
            when {
                isPagination -> _paginationState.value = PaginationState.Loading
                else -> _updateState.value = UiState.Loading
            }
        }
    }

    private fun <T> Flow<T>.errorState(): Flow<T> {
        return catch {
            when {
                isPagination -> _paginationState.value = PaginationState.Error(it)
                else -> _updateState.value = UiState.Error(it)
            }
        }
    }

    private suspend fun Flow<List<PointShortUi>>.successState() {
        collect {
            if (isPagination) {
                _paginationState.value = PaginationState.Success
                paginator.nextPage(it.size)
                _servicesFlow.value = (services + it).toImmutableList()
            } else {
                if (it.isEmpty()) {
                    _updateState.value = UiState.Empty
                } else {
                    paginator.nextPage(it.size)
                    _updateState.value = UiState.Success
                    _servicesFlow.value = it
                }
            }
        }
    }
}

sealed interface UiState {
    object Loading : UiState
    object Success : UiState
    data class Error(val error: Throwable) : UiState
    object Empty : UiState
}

sealed interface PaginationState {
    object Loading : PaginationState
    object Success : PaginationState
    data class Error(val error: Throwable) : PaginationState
}