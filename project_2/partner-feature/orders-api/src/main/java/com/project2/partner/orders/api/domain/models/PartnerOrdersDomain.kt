package com.project2.partner.orders.api.domain.models

import com.project2.partner.orders.api.enums.OrderStatus
import com.project2.partner.points.api.domain.models.PointType

class PartnerOrderDomainModel(
    val id: String,
    val date: String,
    val number: String,
    val status: OrderStatus,
    val amount: Double,
    val pointName: String
)

class PartnerOrderDetailDomainModel(
    val id: String,
    val date: String,
    val comment: String?,
    val number: String,
    val status: OrderStatus,
    val amount: Double,
    val pan: String?,
    val point: PointDomainModel,
    val services: List<ServicesDomainModel>
)

class PointDomainModel(
    val id: String,
    val name: String,
    val types: List<PointTypeDomainModel>,
    val address: String
)

class PointTypeDomainModel(
    val localizedName: String,
    val type: PointType
)

class ServicesDomainModel(
    val name: String,
    val price: Double,
    val quantity: Long,
    val serviceID: String
)

class CreateOrderRequestDomainModel(
    val pointID: String,
    val comment: String,
    val pointTypes: List<PointTypesDomainModel>
)

class PointTypesDomainModel(
    val staticID: String,
    val type: PointTypeDomainModel,
    val serviceTypes: List<ServiceTypesDomainModel>
)

class ServiceTypesDomainModel(
    val staticID: String,
    val name: String,
    val services: List<ServicesDomainModel>
)

class CreatedOrderDomainModel(
    val id: String,
    val date: String,
    val point: PointDomainModel,
    val comment: String,
    val number: String,
    val status: OrderStatus,
    val amount: Double,
    val pan: String?,
    val services: List<ServicesDomainModel>
)