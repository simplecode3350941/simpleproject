package com.project2.partner.orders.api.data.remote.models

import com.project2.partner.orders.api.enums.OrderStatus
import com.project2.partner.points.api.domain.models.PointType

class Response<T>(
    val content: List<T>
)

class PartnerOrderResponse(
    val id: String,
    val date: String,
    val number: String,
    val status: OrderStatus,
    val amount: Double,
    val pointName: String
)

class PartnerOrderDetailResponse(
    val id: String,
    val date: String,
    val comment: String?,
    val number: String,
    val status: OrderStatus,
    val amount: Double,
    val pan: String?,
    val point: PointResponse,
    val services: List<ServicesResponse>
)

class PointResponse(
    val id: String,
    val name: String,
    val types: List<PointTypeResponse>,
    val address: String
)

class ServicesResponse(
    val name: String,
    val price: Double,
    val quantity: Long,
    val serviceID: String
)

class PointTypeResponse(
    val localizedName: String,
    val type: PointType
)

class CreateOrderRequest(
    val pointID: String,
    val comment: String,
    val pointTypes: List<PointTypesDataModel>
)

class PointTypesDataModel(
    val staticID: String,
    val type: PointTypeResponse,
    val serviceTypes: List<ServiceTypesDataModel>
)

class ServiceTypesDataModel(
    val staticID: String,
    val name: String,
    val services: List<ServicesResponse>
)

class CreatedOrderResponse(
    val id: String,
    val date: String,
    val point: PointResponse,
    val comment: String,
    val number: String,
    val status: OrderStatus,
    val amount: Double,
    val pan: String?,
    val services: List<ServicesResponse>
)