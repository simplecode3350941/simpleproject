package com.project2.partner.orders.api.enums

enum class OrderStatus {
    AWAITING_PAYMENT,
    PAID,
    REFUND
}