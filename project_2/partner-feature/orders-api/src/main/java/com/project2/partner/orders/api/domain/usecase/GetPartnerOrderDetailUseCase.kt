package com.project2.partner.orders.api.domain.usecase

import com.project2.partner.orders.api.domain.models.PartnerOrderDetailDomainModel
import kotlinx.coroutines.flow.Flow

interface GetPartnerOrderDetailUseCase {
    suspend operator fun invoke(id: String): Flow<PartnerOrderDetailDomainModel>
}