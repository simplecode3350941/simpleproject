package com.project2.partner.orders.api.domain.usecase

import com.project2.partner.orders.api.domain.models.PartnerOrderDomainModel
import kotlinx.coroutines.flow.Flow

interface GetPartnerOrdersUseCase {
    suspend operator fun invoke(pageNumber: Int, pageSize: Int): Flow<List<PartnerOrderDomainModel>>
}