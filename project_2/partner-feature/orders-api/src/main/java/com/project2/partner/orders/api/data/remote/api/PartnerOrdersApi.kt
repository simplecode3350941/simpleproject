package com.project2.partner.orders.api.data.remote.api

import com.project2.partner.orders.api.data.remote.models.CreateOrderRequest
import com.project2.partner.orders.api.data.remote.models.CreatedOrderResponse
import com.project2.partner.orders.api.data.remote.models.PartnerOrderDetailResponse
import com.project2.partner.orders.api.data.remote.models.PartnerOrderResponse
import com.project2.partner.orders.api.data.remote.models.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface PartnerOrdersApi {

    @GET("/partners/orders/list")
    suspend fun getOrders(
        @Query("page") pageNumber: Int,
        @Query("size") pageSize: Int
    ): Response<PartnerOrderResponse>

    @GET("/partners/orders/order")
    suspend fun getOrderDetail(@Query("orderId") id: String): PartnerOrderDetailResponse

    @POST("/partners/orders/order")
    suspend fun createOrder(@Body types: CreateOrderRequest): CreatedOrderResponse

    @POST("/partners/payment/pay")
    suspend fun payOrder(
        @Query("orderId") id: String,
        @Query("qrCode") qrCode: String
    )
}