package com.project2.partner.orders.api.domain.repository

import com.project2.partner.orders.api.domain.models.CreateOrderRequestDomainModel
import com.project2.partner.orders.api.domain.models.CreatedOrderDomainModel
import com.project2.partner.orders.api.domain.models.PartnerOrderDetailDomainModel
import com.project2.partner.orders.api.domain.models.PartnerOrderDomainModel
import kotlinx.coroutines.flow.Flow

interface PartnerOrdersRepository {
    suspend fun getOrders(pageNumber: Int, pageSize: Int): Flow<List<PartnerOrderDomainModel>>
    suspend fun getOrderDetail(id: String): Flow<PartnerOrderDetailDomainModel>
    suspend fun createOrder(createOrderRequest: CreateOrderRequestDomainModel): Flow<CreatedOrderDomainModel>
    suspend fun payOrder(id: String, qrCode: String): Flow<Unit>
}