package com.project2.partner.orders.api.domain.usecase

import kotlinx.coroutines.flow.Flow

interface PayOrderUseCase {
    suspend operator fun invoke(id: String, qrCode: String): Flow<Boolean>
}