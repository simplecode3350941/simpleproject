package com.project2.partner.orders.api.domain.usecase

import com.project2.partner.orders.api.domain.models.CreateOrderRequestDomainModel
import com.project2.partner.orders.api.domain.models.CreatedOrderDomainModel
import kotlinx.coroutines.flow.Flow

interface CreateOrderUseCase {
    suspend operator fun invoke(
        createOrderRequest: CreateOrderRequestDomainModel
    ): Flow<CreatedOrderDomainModel>
}
