package com.fruktorum.data.di.quiz

import com.fruktorum.data.requests.quiz.QuizRepositoryImpl
import com.fruktorum.data.requests.quiz.local.QuizStorage
import com.fruktorum.data.requests.quiz.local.QuizStorageImpl
import com.fruktorum.data.requests.quiz.remote.QuizApi
import com.fruktorum.data.requests.quiz.remote.QuizApiImpl
import com.fruktorum.domain.game.quiz.QuizRepository
import com.google.firebase.database.DatabaseReference
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class QuizModule {

    @Singleton
    @Provides
    internal fun provideQuizApi(databaseRef: DatabaseReference): QuizApi =
        QuizApiImpl(databaseRef)

    @Singleton
    @Provides
    internal fun provideQuizStorage(): QuizStorage = QuizStorageImpl()

    @Singleton
    @Provides
    internal fun provideQuizRepository(api: QuizApi, storage: QuizStorage): QuizRepository =
        QuizRepositoryImpl(api, storage)
}