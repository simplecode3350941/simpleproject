package com.fruktorum.data.requests.config.remote.model

import com.google.gson.annotations.SerializedName

/**
 * @property mode 0 = base, 1 = event
 * @property modeTitle if mode=1, then the event name is here;
 * if mode=0, then here is the line "Games"
 * @property modeInfo events list
 */
internal class GetModeResponseDto(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("mode")
    val mode: Int,
    @SerializedName("mode_title")
    val modeTitle: String,
    @SerializedName("mode_info")
    val modeInfo: List<EventResponseDto>
)

/**
 * Event or games mode (no event actually, [date] and [prizes] is null)
 *
 * @property imageUrl the picture should be in proportion as for the screen of
 * the detailed description of the game.
 * Ex: https://picsum.photos/800/400, where 800 is the width, 400 is the height.
 */
internal class EventResponseDto(
    @SerializedName("id")
    val id: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("image_url")
    val imageUrl: String,
    @SerializedName("date")
    val date: String?,
    @SerializedName("prizes")
    val prizes: List<PrizeResponseDto>?
)

/**
 * @property place type (1-5)
 */
internal class PrizeResponseDto(
    @SerializedName("id")
    val id: String,
    @SerializedName("place")
    val place: Int,
    @SerializedName("description")
    val description: String
)