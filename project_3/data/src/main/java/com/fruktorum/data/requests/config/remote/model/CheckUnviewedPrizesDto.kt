package com.fruktorum.data.requests.config.remote.model

import com.google.gson.annotations.SerializedName

/**
 * @property hasUnviewed does the user have unviewed prizes
 */
internal class CheckUnviewedPrizesDto(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("is_show_prize")
    val hasUnviewed: Boolean
)