package com.fruktorum.data.requests.quiz

import com.fruktorum.data.requests.quiz.local.QuizStorage
import com.fruktorum.data.requests.quiz.remote.QuizApi
import com.fruktorum.data.requests.quiz.remote.model.toDomainModel
import com.fruktorum.domain.game.quiz.QuizRepository
import com.fruktorum.domain.game.quiz.model.Quiz

internal class QuizRepositoryImpl(
    private val api: QuizApi,
    private val storage: QuizStorage
) : QuizRepository {
    override suspend fun getQuiz(
        id: String,
        forceLoad: Boolean
    ): Quiz? {
        val savedQuiz = storage.getQuiz(id)

        return if (!forceLoad && savedQuiz != null) {
            savedQuiz
        } else {
            val quiz = api.getQuiz(id).toDomainModel()
            quiz?.let {
                storage.putQuiz(id, quiz)
            }
            quiz
        }
    }
}