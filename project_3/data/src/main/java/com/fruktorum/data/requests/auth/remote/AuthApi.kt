package com.fruktorum.data.requests.auth.remote

import com.fruktorum.data.requests.auth.remote.model.ResetPasswordDto
import com.fruktorum.data.base.models.SuccessResponseDto
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthApi {
    @POST("/api/v1/auth/reset-password")
    suspend fun sendRequestResetPassword(
        @Body requestResetPassword: ResetPasswordDto
    ): Response<SuccessResponseDto>
}