package com.fruktorum.data.requests.games.common

import com.fruktorum.core.coroutines.AppDispatchers
import com.fruktorum.data.base.BaseRepository
import com.fruktorum.data.requests.games.common.remote.GamesApi
import com.fruktorum.data.requests.games.common.remote.model.toRequestBody
import com.fruktorum.domain.base.DefaultResponseDomainModel
import com.fruktorum.domain.base.model.SuccessResponseDomainModel
import com.fruktorum.domain.game.common.GamesRepository
import com.fruktorum.domain.game.common.model.GetGameStatusDomainModel
import com.fruktorum.domain.game.common.model.GetGamesDomainModel
import com.fruktorum.domain.game.common.model.SetGameStatusDomainModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

internal class GamesRepositoryImpl
@Inject constructor(
    appDispatchers: AppDispatchers,
    private val api: GamesApi
) : BaseRepository(appDispatchers), GamesRepository {

    override suspend fun sendSetGameStatusRequest(
        model: SetGameStatusDomainModel
    ): Flow<DefaultResponseDomainModel<SuccessResponseDomainModel>> = flowRequest {
        val requestBody = model.toRequestBody()
        api.sendSetGameStatusRequest(requestBody)
    }

    override suspend fun getGameStatusRequest(
        id: String
    ): Flow<DefaultResponseDomainModel<GetGameStatusDomainModel>> = flowRequest {
        api.getGameStatusRequest(id)
    }

    override suspend fun getGamesRequest(): Flow<DefaultResponseDomainModel<GetGamesDomainModel>> =
        flowRequest {
            api.getGamesRequest()
        }
}