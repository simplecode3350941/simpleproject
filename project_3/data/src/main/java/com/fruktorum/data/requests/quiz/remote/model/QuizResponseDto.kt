package com.fruktorum.data.requests.quiz.remote.model

import com.fruktorum.domain.game.quiz.model.Answer
import com.fruktorum.domain.game.quiz.model.Question
import com.fruktorum.domain.game.quiz.model.Quiz
import com.fruktorum.domain.game.quiz.model.Round

internal data class QuizResponseDto(
    val rounds: List<RoundResponseDto?>? = null
)

internal data class RoundResponseDto(
    val number: Int? = null,
    val title: String? = null,
    val description: String? = null,
    val buttonText: String? = null,
    val timeAnswer: Int? = null,
    val score: Int? = null,
    val questions: List<QuestionResponseDto?>? = null
)

internal data class QuestionResponseDto(
    val number: Int? = null,
    val title: String? = null,
    val answers: List<AnswerResponseDto?>? = null
)

internal data class AnswerResponseDto(
    val correct: Boolean? = null,
    val number: Int? = null,
    val text: String? = null,
    val rates: List<Int?>? = null
)

internal fun QuizResponseDto.toDomainModel(): Quiz? {
    return try {
        Quiz(
            rounds = this.rounds?.filterNotNull()?.map {
                it.toDomainModel() ?: throw IllegalStateException()
            } ?: throw IllegalStateException()
        )
    } catch (e: IllegalStateException) {
        null
    }
}

private fun RoundResponseDto.toDomainModel(): Round? {
    return try {
        Round(
            number = this.number ?: throw IllegalStateException(),
            title = this.title ?: throw IllegalStateException(),
            description = this.description ?: throw IllegalStateException(),
            buttonText = this.buttonText ?: throw IllegalStateException(),
            timeAnswer = this.timeAnswer ?: throw IllegalStateException(),
            score = this.score ?: throw IllegalStateException(),
            questions = this.questions?.filterNotNull()?.map {
                it.toDomainModel() ?: throw IllegalStateException()
            } ?: throw IllegalStateException()
        )
    } catch (e: IllegalStateException) {
        null
    }
}

private fun QuestionResponseDto.toDomainModel(): Question? {
    return try {
        Question(
            number = this.number ?: throw IllegalStateException(),
            title = this.title ?: throw IllegalStateException(),
            answers = this.answers?.filterNotNull()?.map {
                it.toDomainModel() ?: throw IllegalStateException()
            } ?: throw IllegalStateException()
        )
    } catch (e: IllegalStateException) {
        null
    }
}

private fun AnswerResponseDto.toDomainModel(): Answer? {
    return try {
        Answer(
            number = this.number ?: throw IllegalStateException(),
            text = this.text ?: throw IllegalStateException(),
            isCorrect = this.correct ?: throw IllegalStateException(),
            rates = this.rates?.filterNotNull()
        )
    } catch (e: IllegalStateException) {
        null
    }
}