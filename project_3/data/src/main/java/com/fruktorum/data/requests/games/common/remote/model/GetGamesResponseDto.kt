package com.fruktorum.data.requests.games.common.remote.model

import com.google.gson.annotations.SerializedName

internal data class GetGamesResponseDto(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("games")
    val games: List<GameResponseDto>,
)

/**
 * @property imageUrl the picture should be in proportion as for the screen of
 * the detailed description of the game.
 * Ex: https://picsum.photos/800/400, where 800 is the width, 400 is the height.
 * @property userScore if mode = 1, then here is the number of points for the game (0-...),
 * if mode = 0, then here is null
 */
internal data class GameResponseDto(
    @SerializedName("id")
    val id: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("image_url")
    val imageUrl: String,
    @SerializedName("user_score")
    val userScore: Int
)