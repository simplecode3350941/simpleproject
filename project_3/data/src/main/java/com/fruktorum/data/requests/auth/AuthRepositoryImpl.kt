package com.fruktorum.data.requests.auth

import com.fruktorum.core.coroutines.AppDispatchers
import com.fruktorum.core.sharedPreference.SessionModel.sessionToken
import com.fruktorum.data.base.BaseRepository
import com.fruktorum.data.requests.auth.remote.AuthApi
import com.fruktorum.data.requests.auth.remote.model.ResetPasswordDto
import com.fruktorum.domain.base.DefaultResponseDomainModel
import com.fruktorum.domain.auth.AuthRepository
import com.fruktorum.domain.auth.model.ResetPasswordDomainModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val authApi: AuthApi,
    appDispatchers: AppDispatchers
) : BaseRepository(appDispatchers), AuthRepository {

    override suspend fun sendRequestResetPassword(
        email: String
    ): Flow<DefaultResponseDomainModel<ResetPasswordDomainModel>> = flowRequest {
        val requestBody = ResetPasswordDto(email)
        authApi.sendRequestResetPassword(requestBody)
    }

    override suspend fun saveToken(token: String) {
        withStorage {
            sessionToken = token
        }
    }
}