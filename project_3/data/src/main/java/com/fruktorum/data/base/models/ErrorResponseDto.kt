package com.fruktorum.data.base.models

import com.google.gson.annotations.SerializedName

data class ErrorResponseDto(
    @SerializedName("success")
    val success : Boolean,
    @SerializedName("errors")
    val errors : String
)