package com.fruktorum.data.requests.auth.remote.model

import com.google.gson.annotations.SerializedName

data class ResetPasswordDto(
    @SerializedName("email")
    val email: String
)
