package com.fruktorum.data.requests.games.common.remote

import com.fruktorum.data.base.models.SuccessResponseDto
import com.fruktorum.data.requests.games.common.remote.model.GetGameStatusResponseDto
import com.fruktorum.data.requests.games.common.remote.model.GetGamesResponseDto
import com.fruktorum.data.requests.games.common.remote.model.SetGameStatusRequestBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

internal interface GamesApi {
    @POST("/api/v1/games/send-status")
    suspend fun sendSetGameStatusRequest(
        @Body setGameStatusRequestBody: SetGameStatusRequestBody
    ): Response<SuccessResponseDto>

    @GET("/api/v1/games/get-status")
    suspend fun getGameStatusRequest(
        @Query("id") id: String
    ): Response<GetGameStatusResponseDto>

    @GET("/api/v1/games")
    suspend fun getGamesRequest(): Response<GetGamesResponseDto>
}