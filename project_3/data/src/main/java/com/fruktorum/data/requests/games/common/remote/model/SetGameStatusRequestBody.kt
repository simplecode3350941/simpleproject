package com.fruktorum.data.requests.games.common.remote.model

import com.fruktorum.domain.game.common.model.SetGameStatusDomainModel
import com.google.gson.annotations.SerializedName

internal data class SetGameStatusRequestBody(
    @SerializedName("id")
    val id: String,
    @SerializedName("user_score")
    val userScore: Int,
    @SerializedName("level")
    val level: Int?,
    @SerializedName("is_finished")
    val isFinished: Boolean
)

internal fun SetGameStatusDomainModel.toRequestBody(): SetGameStatusRequestBody {
    return SetGameStatusRequestBody(
        id = this.id,
        userScore = this.userScore,
        level = this.level,
        isFinished = this.isFinished
    )
}