package com.fruktorum.data.requests.quiz.local

import com.fruktorum.domain.game.quiz.model.Quiz

internal class QuizStorageImpl : QuizStorage {
    private var quizzes: MutableMap<String, Quiz> = mutableMapOf()

    override fun getQuiz(id: String): Quiz? {
        return quizzes[id]
    }

    override fun putQuiz(id: String, quiz: Quiz) {
        quizzes[id] = quiz
    }
}