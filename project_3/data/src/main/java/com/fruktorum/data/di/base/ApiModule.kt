package com.fruktorum.data.di.base

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.fruktorum.core.sharedPreference.ServerUrlsModel.backendServerUrl
import com.fruktorum.core.sharedPreference.SessionModel.sessionToken
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApiModule {
    @Provides
    fun provideOkHttpClient(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
            .connectTimeout(10L, TimeUnit.SECONDS)
            .writeTimeout(10L, TimeUnit.SECONDS)
            .readTimeout(30L, TimeUnit.SECONDS)
            .addInterceptor { chain ->
                val original = chain.request()
                val builder = original.newBuilder()
                val sessionToken = sessionToken
                builder.addHeader("Content-Type", "application/json")
                builder.addHeader("platform", "Android")
                if (sessionToken.isNotEmpty()) {
                    builder.addHeader("Session-Token", sessionToken)
                }
                chain.proceed(builder.build())
            }
    }

    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        return interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
    }
    @Singleton
    @Provides
    fun provideChuckerInterceptor(@ApplicationContext context: Context): ChuckerInterceptor {
        return ChuckerInterceptor.Builder(context)
            .collector(ChuckerCollector(context))
            .maxContentLength(250000L)
            .redactHeaders(emptySet())
            .alwaysReadResponseBody(false)
            .build()
    }


    @Singleton
    @Provides
    @Named("provideRetrofit")
    fun provideRetrofit(
        okHttpClientBuilder: OkHttpClient.Builder,
        interceptor: HttpLoggingInterceptor,
        chuckerInterceptor: ChuckerInterceptor
    ): Retrofit {
        val okHttpClient = okHttpClientBuilder
            .addInterceptor(interceptor)
            .addInterceptor(chuckerInterceptor)
            .build()

        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(backendServerUrl)
            .client(okHttpClient)
            .build()
    }
}