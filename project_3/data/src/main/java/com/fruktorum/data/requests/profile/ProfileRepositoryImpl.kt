package com.fruktorum.data.requests.profile

import com.fruktorum.data.requests.profile.remote.ProfileApi
import com.fruktorum.data.requests.profile.remote.mapper.ProfileMapper
import com.fruktorum.data.requests.profile.remote.model.ProfileInfoResponseDto
import com.fruktorum.domain.profile.ProfileRepository
import com.fruktorum.domain.profile.model.ProfileInfo

internal class ProfileRepositoryImpl(
    private val api: ProfileApi
) : ProfileRepository {
    override fun getProfileInfo(onSuccess: (ProfileInfo) -> Unit) {
        api.getProfileInfo(
            onSuccess = {
                onSuccess(ProfileMapper.map(it))
            }
        )
    }

    override suspend fun getProfileInfo(): ProfileInfo {
        return ProfileMapper.map(api.getProfileInfo() ?: ProfileInfoResponseDto())
    }
}
