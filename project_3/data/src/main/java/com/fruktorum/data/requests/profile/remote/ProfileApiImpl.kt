package com.fruktorum.data.requests.profile.remote

import android.util.Log
import com.fruktorum.data.requests.profile.remote.model.ProfileInfoResponseDto
import com.google.android.gms.tasks.Tasks
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.getValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private const val TAG = "PROFILE_API_IMPL"

internal class ProfileApiImpl(
    private val databaseRef: DatabaseReference
) : ProfileApi {

    override suspend fun getProfileInfo(): ProfileInfoResponseDto {
        return withContext(Dispatchers.IO) {
            val userId = "1"
            val task = databaseRef
                .child("user")
                .child(userId)
                .get()
            return@withContext toProfileInfo(Tasks.await(task))
        }
    }

    private fun toProfileInfo(document: DataSnapshot): ProfileInfoResponseDto {
        return document.getValue<ProfileInfoResponseDto>() ?: ProfileInfoResponseDto()
    }

    override fun getProfileInfo(onSuccess: (ProfileInfoResponseDto) -> Unit) {
        //TODO
        val userId = "1"

        databaseRef.child("user").child(userId).get()
            .addOnSuccessListener {
                val profileInfoResponseDto = it.getValue<ProfileInfoResponseDto>()
                profileInfoResponseDto?.let {
                    onSuccess(it)
                }
                Log.d(TAG, "Success getProfileInfo request")
            }
            .addOnFailureListener {
                Log.e(TAG, "Failed getProfileInfo request", it)
            }
            .addOnCanceledListener {
                Log.e(TAG, "Canceled getProfileInfo request")
            }
            .addOnCompleteListener {
                Log.d(TAG, "Completed getProfileInfo request")
            }
    }
}
