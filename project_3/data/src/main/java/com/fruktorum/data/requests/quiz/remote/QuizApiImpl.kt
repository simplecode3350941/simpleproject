package com.fruktorum.data.requests.quiz.remote

import com.fruktorum.data.requests.quiz.remote.model.QuizResponseDto
import com.fruktorum.data.requests.quiz.remote.model.RoundResponseDto
import com.google.android.gms.tasks.Tasks
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.getValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private const val TAG = "QUIZ_API_IMPL"

internal class QuizApiImpl(
    private val databaseRef: DatabaseReference
) : QuizApi {
    override suspend fun getQuiz(
        id: String
    ): QuizResponseDto {
        return withContext(Dispatchers.IO) {
            val task = databaseRef
                .child("game")
                .child("quiz")
                .child(id)
                .child("round")
                .get()

            val rounds = Tasks.await(task).getValue<List<RoundResponseDto>>()
            return@withContext QuizResponseDto(rounds)
        }
    }
}