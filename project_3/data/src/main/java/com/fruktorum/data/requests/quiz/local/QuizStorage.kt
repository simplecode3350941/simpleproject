package com.fruktorum.data.requests.quiz.local

import com.fruktorum.domain.game.quiz.model.Quiz

internal interface QuizStorage {
    fun getQuiz(
        id: String
    ): Quiz?

    fun putQuiz(
        id: String,
        quiz: Quiz
    )
}