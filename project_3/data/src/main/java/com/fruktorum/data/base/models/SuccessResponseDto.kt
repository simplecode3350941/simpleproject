package com.fruktorum.data.base.models

import com.google.gson.annotations.SerializedName

class SuccessResponseDto(
    @SerializedName("success")
    val success: Boolean
)