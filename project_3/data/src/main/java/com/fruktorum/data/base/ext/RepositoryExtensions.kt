package com.fruktorum.data.base.ext

import android.util.Log
import com.fruktorum.core.utils.ErrorHandler
import com.fruktorum.data.base.BaseRepository
import com.fruktorum.domain.base.DefaultResponseDomainModel
import retrofit2.Response
import java.util.concurrent.atomic.AtomicReference

suspend inline fun <reified T : Any, R> BaseRepository.getResponse(
    noinline request: suspend () -> Response<R>
): DefaultResponseDomainModel<T> {

    val atomicRef = AtomicReference<DefaultResponseDomainModel<T>>()

    runCatching<Response<R>> {
        withNetwork {
            request.invoke()
        }
    }.onFailure {
        atomicRef.set(
            DefaultResponseDomainModel(
                success = false,
                errorMessage = it.localizedMessage,
                data = null
            )
        )
    }.onSuccess { response ->

//////////////////////////////////////////////////////////////////////////
// TODO Перенести в док-цию к самой функции-расширению
        // T - во что нам надо преобразовать
        // R - что возвращает сервер

        // Тут мы делаем шаблонный хэш меп для теста,
        // в котором мы подсовываем примерный ответ сервера
        // На бою, чуть ниже, это генерик R
        val resultTemp: HashMap<*, *> = hashMapOf<Any, Any>().apply {
            this["errors"] = listOf(listOf("email", "invalid"))
            //this["errors"] = "Error string"
            this["success"] = false
        }

        // Далее мы находим поля нашего T класса
        val constructorTemp = T::class.java.kotlin.constructors.first()

        // Тут мы переносим наши поля из R в T
        // (То есть маппим из класса R в класс T)
        val argsTemp = constructorTemp
            .parameters.associateWith { resultTemp[it.name] }

        // Далее преобразуем в наш класс выхода T
        val result33 = constructorTemp.callBy(argsTemp)

        // Тут вывожу что все успешно прошло(переписал функцию toString в ResetPasswordModel)
        Log.d("TEST", result33.toString())

//////////////////////////////////////////////////////////////////////////////

        when (response.code()) {
            200 -> {
                val result = response.body() as HashMap<*, *>

                if (result["success"] == true) {
                    val constructor = T::class.java.kotlin.constructors.first()

                    val args = constructor
                        .parameters.associateWith { result[it.name] }

                    atomicRef.set(
                        DefaultResponseDomainModel(
                            success = true,
                            errorMessage = null,
                            data = constructor.callBy(args)
                        )
                    )
                } else {
                    val errorHandler = ErrorHandler(result["errors"])
                    atomicRef.set(
                        DefaultResponseDomainModel(
                            success = false,
                            errorMessage = errorHandler.getErrorMessage(),
                            data = null
                        )
                    )
                }
            }
            else -> {
                atomicRef.set(
                    DefaultResponseDomainModel(
                        success = false,
                        errorMessage = response.message(),
                        data = null
                    )
                )
            }
        }

    }
    return atomicRef.get()
}