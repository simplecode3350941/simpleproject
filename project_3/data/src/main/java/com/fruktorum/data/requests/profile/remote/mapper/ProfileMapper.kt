package com.fruktorum.data.requests.profile.remote.mapper

import com.fruktorum.data.requests.profile.remote.model.ProfileInfoResponseDto
import com.fruktorum.domain.profile.model.ProfileInfo

internal object ProfileMapper {

    fun map(profileInfoResponseDto: ProfileInfoResponseDto): ProfileInfo {
        return ProfileInfo(
            avatarUrl = profileInfoResponseDto.avatarUrl,
            nickname = profileInfoResponseDto.nickname,
            name = profileInfoResponseDto.name,
            surname = profileInfoResponseDto.surname,
            email = profileInfoResponseDto.email,
            birthdate = profileInfoResponseDto.birthdate,
            starsCount = profileInfoResponseDto.starsCount,
            prizesCount = profileInfoResponseDto.prizesCount
        )
    }
}
