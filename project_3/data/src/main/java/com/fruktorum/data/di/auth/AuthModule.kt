package com.fruktorum.data.di.auth

import com.fruktorum.core.coroutines.AppDispatchers
import com.fruktorum.data.requests.auth.AuthRepositoryImpl
import com.fruktorum.data.requests.auth.remote.AuthApi
import com.fruktorum.domain.auth.AuthRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AuthModule {

    @Provides
    @Singleton
    fun provideAuthApiService(@Named("provideRetrofit") retrofit: Retrofit): AuthApi {
        return retrofit.create(AuthApi::class.java)
    }

    @Singleton
    @Provides
    fun provideAuthRepository(
        authApi: AuthApi,
        appDispatchers: AppDispatchers,
    ): AuthRepository = AuthRepositoryImpl(
        authApi = authApi,
        appDispatchers = appDispatchers
    )
}