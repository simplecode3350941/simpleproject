package com.fruktorum.data.di.config

import com.fruktorum.core.coroutines.AppDispatchers
import com.fruktorum.data.requests.config.ConfigRepositoryImpl
import com.fruktorum.data.requests.config.remote.ConfigApi
import com.fruktorum.domain.config.ConfigRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal class ConfigModule {

    @Provides
    @Singleton
    internal fun provideConfigApi(
        @Named("provideRetrofit") retrofit: Retrofit
    ): ConfigApi {
        return retrofit.create(ConfigApi::class.java)
    }

    @Singleton
    @Provides
    internal fun provideConfigRepository(
        appDispatchers: AppDispatchers,
        api: ConfigApi
    ): ConfigRepository = ConfigRepositoryImpl(
        appDispatchers = appDispatchers,
        api = api
    )
}