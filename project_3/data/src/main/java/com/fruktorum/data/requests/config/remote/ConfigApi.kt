package com.fruktorum.data.requests.config.remote

import com.fruktorum.data.requests.config.remote.model.CheckUnviewedPrizesDto
import com.fruktorum.data.requests.config.remote.model.GetModeResponseDto
import retrofit2.Response
import retrofit2.http.GET

internal interface ConfigApi {

    /**
     * Get application mode information
     */
    @GET("/api/v1/info-mode")
    suspend fun getMode(): Response<GetModeResponseDto>

    /**
     * Check if a user has unviewed prizes
     */
    @GET("/api/v1/home/check-prizes")
    suspend fun checkUnviewedPrizes(): Response<CheckUnviewedPrizesDto>
}