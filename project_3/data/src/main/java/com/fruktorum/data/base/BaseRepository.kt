package com.fruktorum.data.base

import com.fruktorum.core.coroutines.AppDispatchers
import com.fruktorum.data.R
import com.fruktorum.data.base.ext.getResponse
import com.fruktorum.data.base.models.SuccessResponseDto
import com.fruktorum.domain.auth.model.ResetPasswordDomainModel
import com.fruktorum.domain.base.DefaultResponseDomainModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import retrofit2.Response

/**
 * Базовый класс репозитория.
 * Содержит функции, облегчающие переключение диспатчеров при работе с:
 * - БД и SharedPreferences [withStorage]
 * - Сетью [withNetwork]
 * - Обработкой данных [withComputing]
 **/
abstract class BaseRepository(
    protected val appDispatchers: AppDispatchers
) {

    suspend fun <T> withStorage(
        block: suspend CoroutineScope.() -> T
    ): T = withContext(appDispatchers.storage) { block() }

    suspend fun <T> withNetwork(
        block: suspend CoroutineScope.() -> T
    ): T = withContext(appDispatchers.network) { block() }

    suspend fun <T> withComputing(
        block: suspend CoroutineScope.() -> T
    ): T = withContext(appDispatchers.computing) { block() }

    inline fun <reified T : Any, R> flowRequest(noinline request: suspend () -> Response<R>): Flow<DefaultResponseDomainModel<T>> =
        flow {
            emit(DefaultResponseDomainModel.loading())
            val result = getResponse<T, R> {
                request.invoke()
            }
            emit(DefaultResponseDomainModel.success(result))
        }
}