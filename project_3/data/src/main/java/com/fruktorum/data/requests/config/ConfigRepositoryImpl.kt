package com.fruktorum.data.requests.config

import com.fruktorum.core.coroutines.AppDispatchers
import com.fruktorum.data.base.BaseRepository
import com.fruktorum.data.requests.config.remote.ConfigApi
import com.fruktorum.domain.base.DefaultResponseDomainModel
import com.fruktorum.domain.config.ConfigRepository
import com.fruktorum.domain.config.model.CheckUnviewedPrizesDomainModel
import com.fruktorum.domain.config.model.GetModeDomainModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

internal class ConfigRepositoryImpl
@Inject constructor(
    appDispatchers: AppDispatchers,
    private val api: ConfigApi
) : BaseRepository(appDispatchers), ConfigRepository {

    override suspend fun getMode(): Flow<DefaultResponseDomainModel<GetModeDomainModel>> =
        flowRequest {
            api.getMode()
        }

    override suspend fun checkUnviewedPrizes():
            Flow<DefaultResponseDomainModel<CheckUnviewedPrizesDomainModel>> = flowRequest {
        api.checkUnviewedPrizes()
    }
}