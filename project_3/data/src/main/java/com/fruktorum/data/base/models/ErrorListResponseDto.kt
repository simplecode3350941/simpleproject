package com.fruktorum.data.base.models

import com.google.gson.annotations.SerializedName

data class ErrorListResponseDto(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("errors")
    val errors: List<Error>
) {
    data class Error(
        @SerializedName("key")
        val key: String,
        @SerializedName("messages")
        val messages: String
    )
}