package com.fruktorum.data.requests.quiz.remote

import com.fruktorum.data.requests.quiz.remote.model.QuizResponseDto

internal interface QuizApi {
    suspend fun getQuiz(
        id: String
    ): QuizResponseDto
}