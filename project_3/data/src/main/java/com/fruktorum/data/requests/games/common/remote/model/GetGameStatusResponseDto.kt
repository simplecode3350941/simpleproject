package com.fruktorum.data.requests.games.common.remote.model

import com.google.gson.annotations.SerializedName

/**
 * @property success response result
 * @property id game id
 * @property level the level/round of the game the user hasn't played yet.
 * If is_finished = true, then null
 * @property isFinished true if game is complete
 */
internal data class GetGameStatusResponseDto(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("id")
    val id: String,
    @SerializedName("user_score")
    val userScore: Int,
    @SerializedName("level")
    val level: Int?,
    @SerializedName("is_finished")
    val isFinished: Boolean
)