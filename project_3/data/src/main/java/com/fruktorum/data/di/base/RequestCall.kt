package com.fruktorum.data.di.base

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.atomic.AtomicReference

class RequestCall<T>(private val delegate: Call<T>) : Call<T> by delegate {

    fun executeCall(): T? {
        val responseRef = AtomicReference<T?>()
        val failureRef = AtomicReference<Throwable?>()

        enqueue(object : Callback<T> {
            override fun onFailure(call: Call<T>, t: Throwable) {
                failureRef.set(t)
            }

            override fun onResponse(call: Call<T>, response: Response<T>) {
                responseRef.set(response.body())
            }
        })

        return responseRef.get()
    }
}