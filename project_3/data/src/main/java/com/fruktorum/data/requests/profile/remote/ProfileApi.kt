package com.fruktorum.data.requests.profile.remote

import com.fruktorum.data.requests.profile.remote.model.ProfileInfoResponseDto

internal interface ProfileApi {
    fun getProfileInfo(
        onSuccess: (ProfileInfoResponseDto) -> Unit
    )

    suspend fun getProfileInfo(): ProfileInfoResponseDto?
}
