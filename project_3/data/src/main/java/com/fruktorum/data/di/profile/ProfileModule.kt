package com.fruktorum.data.di.profile

import com.fruktorum.data.requests.profile.ProfileRepositoryImpl
import com.fruktorum.data.requests.profile.remote.ProfileApi
import com.fruktorum.data.requests.profile.remote.ProfileApiImpl
import com.fruktorum.domain.profile.ProfileRepository
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ProfileModule {

    @Singleton
    @Provides
    internal fun provideDatabaseRef(): DatabaseReference = Firebase.database.reference

    @Singleton
    @Provides
    internal fun provideProfileApi(databaseRef: DatabaseReference): ProfileApi =
        ProfileApiImpl(databaseRef)

    @Singleton
    @Provides
    internal fun provideProfileRepository(api: ProfileApi): ProfileRepository =
        ProfileRepositoryImpl(api)
}
