package com.fruktorum.data.di.games

import com.fruktorum.core.coroutines.AppDispatchers
import com.fruktorum.data.requests.games.common.GamesRepositoryImpl
import com.fruktorum.data.requests.games.common.remote.GamesApi
import com.fruktorum.domain.game.common.GamesRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal class GamesModule {

    @Provides
    @Singleton
    internal fun provideGamesApi(
        @Named("provideRetrofit") retrofit: Retrofit
    ): GamesApi {
        return retrofit.create(GamesApi::class.java)
    }

    @Singleton
    @Provides
    internal fun provideGamesRepository(
        appDispatchers: AppDispatchers,
        api: GamesApi
    ): GamesRepository = GamesRepositoryImpl(
        appDispatchers = appDispatchers,
        api = api
    )
}