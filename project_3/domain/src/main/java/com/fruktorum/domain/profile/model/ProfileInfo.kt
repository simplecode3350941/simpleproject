package com.fruktorum.domain.profile.model

data class ProfileInfo(
    val avatarUrl: String? = null,
    val nickname: String = "",
    val name: String = "",
    val surname: String = "",
    val email: String = "",
    val birthdate: String = "",
    val starsCount: Int = 0,
    val prizesCount: Int = 0
)
