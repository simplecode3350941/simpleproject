package com.fruktorum.domain.config.usecase

import com.fruktorum.domain.base.DefaultResponseDomainModel
import com.fruktorum.domain.config.ConfigRepository
import com.fruktorum.domain.config.model.CheckUnviewedPrizesDomainModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CheckUnviewedPrizesUseCase
@Inject constructor(
    private val repository: ConfigRepository
) {
    suspend operator fun invoke(): Flow<DefaultResponseDomainModel<CheckUnviewedPrizesDomainModel>> {
        return repository.checkUnviewedPrizes()
    }
}