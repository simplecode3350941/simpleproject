package com.fruktorum.domain.auth.usecase

import com.fruktorum.domain.auth.AuthRepository
import com.fruktorum.domain.auth.model.ResetPasswordDomainModel
import com.fruktorum.domain.base.DefaultResponseDomainModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SendRequestResetPassUseCase
@Inject constructor(
    private val repository: AuthRepository
) {
    suspend operator fun invoke(
        email: String
    ): Flow<DefaultResponseDomainModel<ResetPasswordDomainModel>> {
        return repository.sendRequestResetPassword(email)
    }
}