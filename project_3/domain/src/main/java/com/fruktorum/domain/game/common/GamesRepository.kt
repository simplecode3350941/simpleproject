package com.fruktorum.domain.game.common

import com.fruktorum.domain.base.DefaultResponseDomainModel
import com.fruktorum.domain.base.model.SuccessResponseDomainModel
import com.fruktorum.domain.game.common.model.GetGameStatusDomainModel
import com.fruktorum.domain.game.common.model.GetGamesDomainModel
import com.fruktorum.domain.game.common.model.SetGameStatusDomainModel
import kotlinx.coroutines.flow.Flow

interface GamesRepository {

    suspend fun sendSetGameStatusRequest(
        model: SetGameStatusDomainModel
    ): Flow<DefaultResponseDomainModel<SuccessResponseDomainModel>>

    /**
     * @param id game id
     */
    suspend fun getGameStatusRequest(
        id: String
    ): Flow<DefaultResponseDomainModel<GetGameStatusDomainModel>>

    suspend fun getGamesRequest(): Flow<DefaultResponseDomainModel<GetGamesDomainModel>>
}