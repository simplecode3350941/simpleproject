package com.fruktorum.domain.auth

import com.fruktorum.domain.base.DefaultResponseDomainModel
import com.fruktorum.domain.auth.model.ResetPasswordDomainModel
import kotlinx.coroutines.flow.Flow

interface AuthRepository {
    suspend fun sendRequestResetPassword(
        email: String
    ): Flow<DefaultResponseDomainModel<ResetPasswordDomainModel>>
    suspend fun saveToken(token: String)
}