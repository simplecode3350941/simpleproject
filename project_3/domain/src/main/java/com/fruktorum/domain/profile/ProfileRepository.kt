package com.fruktorum.domain.profile

import com.fruktorum.domain.profile.model.ProfileInfo

interface ProfileRepository {
    fun getProfileInfo(
        onSuccess: (ProfileInfo) -> Unit
    )

    suspend fun getProfileInfo(): ProfileInfo
}
