package com.fruktorum.domain.game.common.model

import com.fruktorum.domain.entity.Game

class GetGamesDomainModel(
    val success: Boolean,
    val games: List<Game>
)