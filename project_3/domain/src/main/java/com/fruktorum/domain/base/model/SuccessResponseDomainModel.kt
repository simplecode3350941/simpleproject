package com.fruktorum.domain.base.model

class SuccessResponseDomainModel(
    val success: Boolean,
) {
    override fun toString(): String {
        return "success: $success"
    }
}