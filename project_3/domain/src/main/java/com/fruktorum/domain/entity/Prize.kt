package com.fruktorum.domain.entity

/**
 * @property place type (1-5)
 */
class Prize(
    val id: String,
    val place: Int,
    val description: String
)