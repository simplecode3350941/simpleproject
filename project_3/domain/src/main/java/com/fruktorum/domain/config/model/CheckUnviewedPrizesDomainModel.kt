package com.fruktorum.domain.config.model

/**
 * @property hasUnviewed does the user have unviewed prizes
 */
class CheckUnviewedPrizesDomainModel(
    val success: Boolean,
    val hasUnviewed: Boolean
)