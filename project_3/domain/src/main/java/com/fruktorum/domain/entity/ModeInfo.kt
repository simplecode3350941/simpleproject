package com.fruktorum.domain.entity

/**
 * Event or games mode (no event actually, [date] and [prizes] is null)
 *
 * @property imageUrl the picture should be in proportion as for the screen of
 * the detailed description of the game.
 * Ex: https://picsum.photos/800/400, where 800 is the width, 400 is the height.
 */
class ModeInfo(
    val id: String,
    val title: String,
    val description: String,
    val imageUrl: String,
    val date: String?,
    val prizes: List<Prize>?
)