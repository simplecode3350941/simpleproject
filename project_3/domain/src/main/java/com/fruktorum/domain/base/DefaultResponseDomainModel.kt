package com.fruktorum.domain.base

/**
 * Универсальная модель для получения результатов от запросов.
 *
 * @param status            Внутренний статус запроса.
 * @param success           Статус запроса от бека.
 *                          true - запрос выполнился успешно,
 *                          false - запрос выполненлмя неуспешно
 * @param errorMessage      Текст ошибки.
 *                          При success=false - текст ошибки,
 *                          при success=true - null
 * @param data              Модель данных запроса
 **/

class DefaultResponseDomainModel<out T>(
    var status: RequestStatus = RequestStatus.LOADING,
    val success: Boolean,
    val errorMessage: String?,
    val data: T?
) {
    companion object {
        fun <T> success(data: DefaultResponseDomainModel<T>): DefaultResponseDomainModel<T> =
            data.apply {
                status = RequestStatus.SUCCESS
            }

        fun <T> error(message: String): DefaultResponseDomainModel<T> =
            DefaultResponseDomainModel(
                success = false,
                data = null,
                errorMessage = message,
                status = RequestStatus.ERROR
            )

        fun <T> loading(): DefaultResponseDomainModel<T> =
            DefaultResponseDomainModel(
                status = RequestStatus.LOADING,
                success = false,
                data = null,
                errorMessage = null
            )
    }
}

inline fun <T> DefaultResponseDomainModel<T>.onSuccess(crossinline f: T?.() -> Unit = {}): DefaultResponseDomainModel<T> {
    if (this.success && status == RequestStatus.SUCCESS) f.invoke(this.data)
    return this
}

inline fun <T> DefaultResponseDomainModel<T>.onError(crossinline f: String?.() -> Unit = {}): DefaultResponseDomainModel<T> {
    if (!this.success && status != RequestStatus.LOADING) f.invoke(this.errorMessage)
    return this
}

inline fun <T> DefaultResponseDomainModel<T>.onLoading(crossinline f: () -> Unit = {}): DefaultResponseDomainModel<T> {
    if (status == RequestStatus.LOADING) f.invoke()
    return this
}