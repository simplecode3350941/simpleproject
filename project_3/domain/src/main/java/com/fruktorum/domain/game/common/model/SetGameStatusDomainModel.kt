package com.fruktorum.domain.game.common.model

data class SetGameStatusDomainModel(
    val id: String,
    val userScore: Int,
    val level: Int?,
    val isFinished: Boolean
)