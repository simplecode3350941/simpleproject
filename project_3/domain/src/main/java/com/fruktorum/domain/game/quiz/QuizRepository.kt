package com.fruktorum.domain.game.quiz

import com.fruktorum.domain.game.quiz.model.Quiz

interface QuizRepository {
    suspend fun getQuiz(
        id: String,
        forceLoad: Boolean
    ): Quiz?
}