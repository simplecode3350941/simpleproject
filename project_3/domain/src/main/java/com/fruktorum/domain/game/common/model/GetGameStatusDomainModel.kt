package com.fruktorum.domain.game.common.model

data class GetGameStatusDomainModel(
    val success: Boolean,
    val id: String,
    val userScore: Int,
    val level: Int?,
    val isFinished: Boolean
)