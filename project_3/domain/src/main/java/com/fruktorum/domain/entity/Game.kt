package com.fruktorum.domain.entity

import android.util.Log

/**
 * @property imageUrl the picture should be in proportion as for the screen of
 * the detailed description of the game.
 * Ex: https://picsum.photos/800/400, where 800 is the width, 400 is the height.
 * @property userScore if mode = 1, then here is the number of points for the game (0-...),
 * if mode = 0, then here is null
 * @property imageRatio width to height in px
 */
data class Game(
    val id: String,
    val title: String,
    val description: String,
    val imageUrl: String,
    val userScore: Int?
) {
    val imageRatio = try {
        with(imageUrl.split("/")) {
            this[this.lastIndex - 1].toInt() to this.last().toInt()
        }
    } catch (e: Exception) {
        Log.e("Game entity", e.localizedMessage, e)
        1 to 1
    }
}