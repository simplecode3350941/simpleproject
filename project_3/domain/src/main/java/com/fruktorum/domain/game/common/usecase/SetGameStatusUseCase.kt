package com.fruktorum.domain.game.common.usecase

import com.fruktorum.domain.base.DefaultResponseDomainModel
import com.fruktorum.domain.base.model.SuccessResponseDomainModel
import com.fruktorum.domain.game.common.GamesRepository
import com.fruktorum.domain.game.common.model.SetGameStatusDomainModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SetGameStatusUseCase
@Inject constructor(
    private val repository: GamesRepository
) {
    suspend operator fun invoke(
        model: SetGameStatusDomainModel
    ): Flow<DefaultResponseDomainModel<SuccessResponseDomainModel>> {
        return repository.sendSetGameStatusRequest(model)
    }
}