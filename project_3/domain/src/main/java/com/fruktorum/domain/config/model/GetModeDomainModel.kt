package com.fruktorum.domain.config.model

import android.view.Display.Mode
import com.fruktorum.domain.entity.ModeInfo

/**
 * @property mode 0 = base, 1 = event
 * @property modeTitle if mode=1, then the event name is here;
 * if mode=0, then here is the line "Games"
 * @property modeInfo events list
 */
class GetModeDomainModel(
    val success: Boolean,
    val mode: Int,
    val modeTitle: String,
    val modeInfo: ModeInfo
)


