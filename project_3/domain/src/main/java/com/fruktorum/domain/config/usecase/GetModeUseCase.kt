package com.fruktorum.domain.config.usecase

import com.fruktorum.domain.base.DefaultResponseDomainModel
import com.fruktorum.domain.config.ConfigRepository
import com.fruktorum.domain.config.model.GetModeDomainModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetModeUseCase
@Inject constructor(
    private val repository: ConfigRepository
) {
    suspend operator fun invoke(): Flow<DefaultResponseDomainModel<GetModeDomainModel>> {
        return repository.getMode()
    }
}