package com.fruktorum.domain.game.quiz.model

data class Quiz(
    val rounds: List<Round>
)

data class Round(
    val number: Int,
    val title: String,
    val description: String,
    val buttonText: String,
    val timeAnswer: Int,
    val score: Int,
    val questions: List<Question>
)

data class Question(
    val number: Int,
    val title: String,
    val answers: List<Answer>
)

data class Answer(
    val number: Int,
    val text: String,
    val isCorrect: Boolean,
    val rates: List<Int>?
)