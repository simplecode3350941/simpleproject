package com.fruktorum.domain.profile.usecase

import com.fruktorum.domain.profile.ProfileRepository
import com.fruktorum.domain.profile.model.ProfileInfo
import javax.inject.Inject

class GetProfileInfoUseCase
@Inject constructor(
    private val repository: ProfileRepository
) {
    operator fun invoke(onSuccess: (ProfileInfo) -> Unit) {
        repository.getProfileInfo(onSuccess)
    }

    suspend operator fun invoke(): ProfileInfo {
        return repository.getProfileInfo()
    }
}
