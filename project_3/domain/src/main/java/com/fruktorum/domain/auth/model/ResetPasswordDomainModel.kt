package com.fruktorum.domain.auth.model

class ResetPasswordDomainModel(
    val success: Boolean,
) {
    override fun toString(): String {
        return "success: $success"
    }
}