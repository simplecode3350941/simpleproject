package com.fruktorum.domain.game.common.usecase

import com.fruktorum.domain.base.DefaultResponseDomainModel
import com.fruktorum.domain.game.common.GamesRepository
import com.fruktorum.domain.game.common.model.GetGamesDomainModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetGamesUseCase
@Inject constructor(
    private val repository: GamesRepository
) {
    suspend operator fun invoke(): Flow<DefaultResponseDomainModel<GetGamesDomainModel>> {
        return repository.getGamesRequest()
    }
}