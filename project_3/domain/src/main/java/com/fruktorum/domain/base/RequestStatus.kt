package com.fruktorum.domain.base

enum class RequestStatus {
    LOADING, SUCCESS, ERROR
}