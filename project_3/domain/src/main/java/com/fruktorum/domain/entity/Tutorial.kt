package com.fruktorum.domain.entity

import androidx.annotation.DrawableRes

data class Tutorial (
    val title: String,
    val description: String,
    @DrawableRes
    val picture: Int
)