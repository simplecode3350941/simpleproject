package com.fruktorum.domain.auth.usecase

import com.fruktorum.domain.auth.AuthRepository
import javax.inject.Inject

class SaveTokenUseCase
@Inject constructor(
    private val repository: AuthRepository
) {
    suspend operator fun invoke(token: String) {
        repository.saveToken(token)
    }
}