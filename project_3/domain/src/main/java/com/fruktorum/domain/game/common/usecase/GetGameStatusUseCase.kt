package com.fruktorum.domain.game.common.usecase

import com.fruktorum.domain.base.DefaultResponseDomainModel
import com.fruktorum.domain.game.common.GamesRepository
import com.fruktorum.domain.game.common.model.GetGameStatusDomainModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetGameStatusUseCase
@Inject constructor(
    private val repository: GamesRepository
) {
    suspend operator fun invoke(
        id: String
    ): Flow<DefaultResponseDomainModel<GetGameStatusDomainModel>> {
        return repository.getGameStatusRequest(id)
    }
}