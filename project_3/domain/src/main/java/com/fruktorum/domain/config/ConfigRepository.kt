package com.fruktorum.domain.config

import com.fruktorum.domain.base.DefaultResponseDomainModel
import com.fruktorum.domain.config.model.CheckUnviewedPrizesDomainModel
import com.fruktorum.domain.config.model.GetModeDomainModel
import kotlinx.coroutines.flow.Flow

interface ConfigRepository {

    /**
     * Get application mode information
     */
    suspend fun getMode(): Flow<DefaultResponseDomainModel<GetModeDomainModel>>

    /**
     * Check if a user has unviewed prizes
     */
    suspend fun checkUnviewedPrizes():
            Flow<DefaultResponseDomainModel<CheckUnviewedPrizesDomainModel>>
}