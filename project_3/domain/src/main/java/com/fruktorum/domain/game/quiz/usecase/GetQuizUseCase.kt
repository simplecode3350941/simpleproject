package com.fruktorum.domain.game.quiz.usecase

import com.fruktorum.domain.game.quiz.QuizRepository
import com.fruktorum.domain.game.quiz.model.Quiz
import javax.inject.Inject

class GetQuizUseCase
@Inject constructor(
    private val repository: QuizRepository
) {
    suspend operator fun invoke(
        id: String,
        forceLoad: Boolean = true
    ): Quiz? = repository.getQuiz(id, forceLoad)
}