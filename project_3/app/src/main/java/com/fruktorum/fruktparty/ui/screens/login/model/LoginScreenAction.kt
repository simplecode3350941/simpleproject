package com.fruktorum.fruktparty.ui.screens.login.model

internal sealed class LoginScreenAction {

    class NavigateTo(
        val route: String
    ) : LoginScreenAction()
}
