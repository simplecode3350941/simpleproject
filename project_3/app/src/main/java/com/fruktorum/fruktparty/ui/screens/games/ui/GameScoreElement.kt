package com.fruktorum.fruktparty.ui.screens.games.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.tooling.preview.Preview
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.fruktparty.R

@Composable
internal fun GameScoreElement(
    score: Int,
    modifier: Modifier = Modifier
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .wrapContentSize()
            .padding(
                start = dimensionResource(id = com.fruktorum.core.R.dimen.size_16_dp),
                bottom = dimensionResource(id = com.fruktorum.core.R.dimen.size_2_dp)
            )
    ) {
        Image(
            imageVector = ImageVector.vectorResource(id = R.drawable.ic_star),
            contentDescription = null
        )
        Text(
            text = score.toString(),
            style = MaterialTheme.typography.subtitle1,
            modifier = Modifier.padding(start = dimensionResource(id = com.fruktorum.core.R.dimen.size_4_dp))
        )
    }
}

@Composable
@Preview
private fun GamesScoreElementPreview() {
    FruktPartyTheme {
        GameScoreElement(
            score = 100
        )
    }
}