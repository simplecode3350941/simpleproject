package com.fruktorum.fruktparty.ui.screens.leaderboard.ui

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.fruktparty.R

@Composable
internal fun LeaderboardHeader() {
    Text(
        text = stringResource(R.string.leaderboard_screen_title),
        modifier = Modifier
            .fillMaxWidth()
            .statusBarsPadding()
            .padding(top = dimensionResource(com.fruktorum.core.R.dimen.size_19_dp)),
        textAlign = TextAlign.Center,
        style = MaterialTheme.typography.body1,
        color = Black000B2D
    )
}