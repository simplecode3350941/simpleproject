package com.fruktorum.fruktparty.ui.screens.profile.changepassword

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import com.fruktorum.core.ui.composables.textFields.PasswordTextField
import com.fruktorum.core.ui.composables.topBar.FruktPartyTopBar
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.core.ui.theme.WhiteFFFFFF
import com.fruktorum.fruktparty.R
import com.fruktorum.fruktparty.ui.screens.profile.changepassword.model.ChangePasswordField
import com.fruktorum.fruktparty.ui.screens.profile.changepassword.model.ChangePasswordScreenEvent
import com.fruktorum.fruktparty.ui.screens.profile.changepassword.model.ChangePasswordScreenState

/**
 * Change password screen
 */
@Composable
internal fun ChangePasswordScreen(
    navController: NavHostController,
    viewModel: ChangePasswordScreenViewModel,
    modifier: Modifier = Modifier
) {
    val state = viewModel.state.collectAsState(
        initial = ChangePasswordScreenState.EnteringValues()
    )

    when (val stateValue = state.value) {
        is ChangePasswordScreenState.EnteringValues -> {
            ChangePasswordScreen(
                content = stateValue,
                modifier = modifier,
                onBackClick = {
                    navController.popBackStack()
                },
                onChangeFieldValue = {
                    viewModel.onEvent(ChangePasswordScreenEvent.ChangeFieldValue(it))
                },
                onChangeFieldVisibility = {
                    viewModel.onEvent(ChangePasswordScreenEvent.ChangeFieldVisibility(it))

                },
                onChangePasswordClick = {
                    viewModel.onEvent(ChangePasswordScreenEvent.ChangePasswordClick)
                }
            )
        }
    }

    LaunchedEffect(true) {
        viewModel.action.collect { action ->
            when (action) {
                // TODO
                else -> Unit
            }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
private fun ChangePasswordScreen(
    content: ChangePasswordScreenState.EnteringValues,
    modifier: Modifier,
    onBackClick: () -> Unit,
    onChangeFieldValue: (ChangePasswordField) -> Unit,
    onChangeFieldVisibility: (ChangePasswordField) -> Unit,
    onChangePasswordClick: () -> Unit
) = Box(
    modifier = modifier
        // TODO delete this, get from theme
        .background(
            brush = WhiteFDFDFFGrayEEF2FF
        )
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
    ) {
        val focusManager = LocalFocusManager.current

        FruktPartyTopBar(
            title = stringResource(R.string.change_password_screen_change_password_text),
            modifier = Modifier.padding(top = dimensionResource(com.fruktorum.core.R.dimen.size_8_dp))
        ) {
            onBackClick()
        }

        val fieldModifier = Modifier
            .padding(
                horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_44_dp),
                vertical = dimensionResource(com.fruktorum.core.R.dimen.size_16_dp)
            )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)))

        PasswordTextField(
            value = content.oldPassword,
            modifier = fieldModifier,
            placeholder = stringResource(R.string.change_password_screen_old_password_placeholder),
            keyboardActions = KeyboardActions(
                onDone = { focusManager.moveFocus(FocusDirection.Down) }
            ),
            onValueChange = {
                onChangeFieldValue(ChangePasswordField.OldPassword(it))
            }
        )

        PasswordTextField(
            value = content.newPassword,
            modifier = fieldModifier,
            placeholder = stringResource(R.string.change_password_screen_new_password_placeholder),
            keyboardActions = KeyboardActions(
                onDone = { focusManager.moveFocus(FocusDirection.Down) }
            ),
            onValueChange = {
                onChangeFieldValue(ChangePasswordField.NewPassword(it))
            }
        )

        PasswordTextField(
            value = content.confirmNewPassword,
            modifier = fieldModifier,
            placeholder = stringResource(R.string.change_password_screen_confirm_new_password_placeholder),
            keyboardActions = KeyboardActions(
                onDone = { focusManager.clearFocus(force = true) }
            ),
            onValueChange = {
                onChangeFieldValue(ChangePasswordField.ConfirmNewPassword(it))
            }
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)))
    }

    Box(
        modifier = Modifier
            .align(Alignment.BottomCenter)
            .fillMaxWidth()
            .padding(
                horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_43_dp)
            )
            .padding(bottom = dimensionResource(com.fruktorum.core.R.dimen.size_40_dp))
            .background(
                brush = RedFF512FPinkDD2476,
                shape = RoundedCornerShape(
                    topStart = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp),
                    bottomEnd = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)
                )
            )
            .clip(
                shape = RoundedCornerShape(
                    topStart = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp),
                    bottomEnd = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)
                )
            )
            .clickable(onClick = onChangePasswordClick),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = stringResource(R.string.change_password_screen_change_password_text),
            modifier = Modifier
                .padding(vertical = dimensionResource(com.fruktorum.core.R.dimen.size_15_dp)),
            color = WhiteFFFFFF,
            style = MaterialTheme.typography.button
        )
    }
}

@Preview(
    showBackground = true
)
@Composable
private fun ChangePasswordScreenPreview() {
    FruktPartyTheme {
        ChangePasswordScreen(
            content = ChangePasswordScreenState.EnteringValues(),
            modifier = Modifier,
            onBackClick = {},
            onChangeFieldValue = {},
            onChangeFieldVisibility = {},
            onChangePasswordClick = {}
        )
    }
}
