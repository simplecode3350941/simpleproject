package com.fruktorum.fruktparty.ui.screens.login.model

internal sealed class LoginScreenDialogState {

    object None : LoginScreenDialogState()
}
