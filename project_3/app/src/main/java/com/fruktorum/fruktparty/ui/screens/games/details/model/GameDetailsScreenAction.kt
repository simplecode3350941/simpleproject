package com.fruktorum.fruktparty.ui.screens.games.details.model

internal sealed class GameDetailsScreenAction {

    class NavigateTo(
        val route: String
    ) : GameDetailsScreenAction()
}