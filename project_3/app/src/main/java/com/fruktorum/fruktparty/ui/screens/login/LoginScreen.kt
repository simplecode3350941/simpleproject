package com.fruktorum.fruktparty.ui.screens.login

import android.annotation.SuppressLint
import android.text.InputType
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatEditText
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.viewinterop.AndroidView
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.navigation.NavHostController
import androidx.navigation.NavOptions
import androidx.navigation.compose.rememberNavController
import com.fruktorum.core.ui.composables.buttons.TopStartBottomEndRoundedButton
import com.fruktorum.core.ui.composables.screen.LoadingScreen
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.Black0C002C
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.Gray9AA1B6
import com.fruktorum.fruktparty.R
import com.fruktorum.fruktparty.ui.screens.TwoLayerBasicScreen
import com.fruktorum.fruktparty.ui.screens.login.model.LoginScreenAction
import com.fruktorum.fruktparty.ui.screens.login.model.LoginScreenEvent
import com.fruktorum.fruktparty.ui.screens.login.model.LoginScreenState
import com.fruktorum.ftauth.FTAuth
import com.fruktorum.ftauth.customUI.auth.FTAuthEmailInputField
import com.fruktorum.ftauth.customUI.auth.FTAuthPasswordInputField

@Composable
internal fun LoginScreen(
    navController: NavHostController,
    viewModel: LoginScreenViewModel,
    modifier: Modifier = Modifier
) {

    val state = viewModel.state.collectAsState(initial = LoginScreenState.Loading)

    when (state.value) {
        LoginScreenState.Loading -> {
            LoadingScreen(modifier = modifier)
        }
        LoginScreenState.Content -> {
            LoginScreen(
                navController,
                onLoginClick = {
                    viewModel.onEvent(LoginScreenEvent.ClickOnLogin)
                },
                onLoginByGoogleClick = {
                    viewModel.onEvent(LoginScreenEvent.ClickOnLoginByGoogle)
                },
                onForgotPasswordClick = {
                    viewModel.onEvent(LoginScreenEvent.ClickOnForgotPassword)
                }
            )
        }
        LoginScreenState.Error -> {

        }
    }

    LaunchedEffect(true) {
        viewModel.onEvent(LoginScreenEvent.Start)

        viewModel.action.collect { action ->
            when (action) {
                is LoginScreenAction.NavigateTo -> {
                    val options = NavOptions
                        .Builder()
                        .setPopUpTo(navController.graph.startDestinationId, false)
                        .build()

                    navController.navigate(
                        route = action.route,
                        options
                    )
                }
            }
        }
    }
}

@SuppressLint("ClickableViewAccessibility")
@Composable
private fun LoginScreen(
    navController: NavHostController = rememberNavController(),
    onLoginClick: () -> Unit,
    onLoginByGoogleClick: () -> Unit,
    onForgotPasswordClick: () -> Unit
) {
    val context = LocalContext.current

    val iconCloseEye = AppCompatResources.getDrawable(context, R.drawable.ic_close_eye)
    val iconOpenEye = AppCompatResources.getDrawable(context, R.drawable.ic_open_eye)


    val compoundDrawablesListener = View.OnTouchListener { view, event ->
        val drawableRight = 2
        val ourInputField = (view as AppCompatEditText)

        if (event.action == MotionEvent.ACTION_UP) {
            if (event.rawX >=
                view.right - ourInputField.compoundDrawables[drawableRight].bounds.width()
            ) {
                if (ourInputField.inputType == InputType.TYPE_CLASS_TEXT) {
                    ourInputField.setCompoundDrawablesWithIntrinsicBounds(
                        null,
                        null,
                        iconCloseEye,
                        null
                    )
                    ourInputField.inputType =
                        InputType.TYPE_TEXT_VARIATION_PASSWORD or InputType.TYPE_CLASS_TEXT
                } else {
                    ourInputField.setCompoundDrawablesWithIntrinsicBounds(
                        null,
                        null,
                        iconOpenEye,
                        null
                    )
                    ourInputField.inputType = InputType.TYPE_CLASS_TEXT
                }
                return@OnTouchListener true
            }
        }
        false
    }

    val dimen0 = dimensionResource(id = com.fruktorum.core.R.dimen.size_0_dp)
    val dimen24 = dimensionResource(id = com.fruktorum.core.R.dimen.size_24_dp)
    val dimen40 = dimensionResource(id = com.fruktorum.core.R.dimen.size_40_dp)
    val dimen42 = dimensionResource(id = com.fruktorum.core.R.dimen.size_42_dp)
    val dimen52 = dimensionResource(id = com.fruktorum.core.R.dimen.size_52_dp)
    val dimen56 = dimensionResource(id = com.fruktorum.core.R.dimen.size_56_dp)

    var fieldLoginPassword = FTAuthPasswordInputField(context).apply {
        this.setInputFieldStyle(R.style.FTAuthDefaultInputField)
        this.inputField.setCompoundDrawablesWithIntrinsicBounds(
            null,
            null,
            iconCloseEye,
            null
        )
        this.inputField.setOnTouchListener(compoundDrawablesListener)
    }

    var fieldLoginEmail = FTAuthEmailInputField(context).apply {
        this.setInputFieldStyle(R.style.FTAuthDefaultInputField)
    }

    TwoLayerBasicScreen(
        title = stringResource(id = R.string.login_screen_title),
        onClickBack = {
            navController.popBackStack()
        }
    ) {
        ConstraintLayout(modifier = Modifier.fillMaxSize()) {
            val (email, pwd, forgot_password, or, google_auth, login_btn) = createRefs()

            AndroidView(
                factory = {
                    fieldLoginEmail
                },
                modifier = Modifier.constrainAs(email) {
                    width = Dimension.matchParent
                    height = Dimension.wrapContent
                    top.linkTo(parent.top, margin = dimen56)
                    start.linkTo(parent.start, margin = dimen42)
                    end.linkTo(parent.end, margin = dimen42)
                },
                update = {
                    fieldLoginEmail = it
                    FTAuth.authEmailInputField = fieldLoginEmail
                })

            AndroidView(
                factory = {
                    fieldLoginPassword
                },
                modifier = Modifier.constrainAs(pwd) {
                    width = Dimension.matchParent
                    top.linkTo(email.bottom, margin = dimen0)
                    start.linkTo(parent.start, margin = dimen42)
                    end.linkTo(parent.end, margin = dimen42)
                },
                update = {
                    fieldLoginPassword = it
                    FTAuth.authPasswordInputField = fieldLoginPassword
                }
            )

            Text(
                text = stringResource(id = R.string.login_screen_forgot_password),
                color = Black000B2D,
                style = MaterialTheme.typography.subtitle1,
                modifier = Modifier
                    .constrainAs(forgot_password) {
                        top.linkTo(pwd.bottom)
                        end.linkTo(parent.end, dimen42)
                    }
                    .clickable { onForgotPasswordClick.invoke() }
            )

            Text(
                text = stringResource(R.string.or),
                color = Gray9AA1B6,
                style = MaterialTheme.typography.body1,
                modifier = Modifier.constrainAs(or) {
                    top.linkTo(forgot_password.bottom, margin = dimen24)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
            )

            TopStartBottomEndRoundedButton(
                image = R.drawable.ic_gmail,
                text = stringResource(id = R.string.login_screen_gmail_login_text),
                textColor = Black000B2D,
                backgroundColor = Color.White,
                onClick = { onLoginByGoogleClick.invoke() },
                modifier = Modifier.constrainAs(google_auth) {
                    width = Dimension.fillToConstraints
                    height = Dimension.value(dimen52)
                    top.linkTo(or.bottom, margin = dimen24)
                    start.linkTo(parent.start, margin = dimen42)
                    end.linkTo(parent.end, margin = dimen42)
                }
            )

            TopStartBottomEndRoundedButton(
                text = stringResource(id = R.string.login_screen_login_button_text),
                onClick = {
                    fieldLoginEmail.validate()
                    fieldLoginPassword.validate()

                    if (fieldLoginEmail.isEmailValid && fieldLoginPassword.isPasswordValid) {
                        onLoginClick.invoke()
                    }
                },
                backgroundColor = Black0C002C,
                modifier = Modifier.constrainAs(login_btn) {
                    width = Dimension.fillToConstraints
                    height = Dimension.value(dimen52)
                    bottom.linkTo(parent.bottom, margin = dimen40)
                    start.linkTo(parent.start, margin = dimen42)
                    end.linkTo(parent.end, margin = dimen42)
                }
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun LoginScreenPreview() {
    FruktPartyTheme {
        LoginScreen(
            onForgotPasswordClick = {},
            onLoginByGoogleClick = {},
            onLoginClick = {}
        )
    }
}