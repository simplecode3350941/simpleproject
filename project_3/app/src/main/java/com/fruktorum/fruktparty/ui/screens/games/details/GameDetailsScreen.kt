package com.fruktorum.fruktparty.ui.screens.games.details

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.fruktorum.core.R
import com.fruktorum.core.ui.composables.buttons.TopStartBottomEndRoundedButton
import com.fruktorum.core.ui.composables.screen.LoadingScreen
import com.fruktorum.core.ui.composables.topBar.FruktPartyTopBar
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.domain.entity.Game
import com.fruktorum.fruktparty.R.string
import com.fruktorum.fruktparty.ui.screens.error.ErrorScreen
import com.fruktorum.fruktparty.ui.screens.games.details.model.GameDetailsScreenAction
import com.fruktorum.fruktparty.ui.screens.games.details.model.GameDetailsScreenEvent
import com.fruktorum.fruktparty.ui.screens.games.details.model.GameDetailsScreenState
import com.skydoves.landscapist.glide.GlideImage

@Composable
internal fun GameDetailsScreen(
    navController: NavHostController,
    viewModel: GameDetailsScreenViewModel,
    modifier: Modifier = Modifier,
    id: String,
    title: String,
    description: String,
    imageUrl: String
) {
    val state = viewModel.state.collectAsState(
        initial = GameDetailsScreenState.Loading
    ).value

    when (state) {
        GameDetailsScreenState.Loading -> {
            LoadingScreen(modifier)
        }
        is GameDetailsScreenState.Content -> {
            GameDetailsScreen(
                content = state,
                modifier = modifier,
                onClickBack = {
                    navController.popBackStack()
                },
                onClickPlay = {
                    navController.navigate(state.route)
                }
            )
        }
        is GameDetailsScreenState.ErrorGetGameStatus -> {
            ErrorScreen(
                modifier = modifier,
                text = state.error,
                onClickTryAgain = {
                    viewModel.onEvent(
                        GameDetailsScreenEvent.ClickOnTryAgainErrorGetGameStatus(
                            state.id,
                            state.title,
                            state.description,
                            state.imageUrl
                        )
                    )
                },
                onClickBack = {
                    navController.popBackStack()
                }
            )

        }
    }

    LaunchedEffect(true) {
        viewModel.onEvent(
            GameDetailsScreenEvent.Start(
                id = id,
                title = title,
                description = description,
                imageUrl = imageUrl
            )
        )
        viewModel.action.collect { action ->
            when (action) {
                is GameDetailsScreenAction.NavigateTo -> {
                    navController.navigate(
                        route = action.route
                    )
                }
            }
        }
    }
}

@Composable
private fun GameDetailsScreen(
    content: GameDetailsScreenState.Content,
    modifier: Modifier,
    onClickBack: () -> Unit,
    onClickPlay: () -> Unit
) {
    Column(
        modifier = modifier
            .fillMaxSize()
            //TODO delete this, get from theme
            .background(
                brush = WhiteFDFDFFGrayEEF2FF
            )
            .verticalScroll(
                rememberScrollState()
            )
    ) {
        FruktPartyTopBar(
            title = content.game.title,
            modifier = Modifier.padding(top = dimensionResource(R.dimen.size_8_dp)),
            onBackClick = onClickBack
        )

        Spacer(modifier = Modifier.size(dimensionResource(R.dimen.size_20_dp)))

        GlideImage(
            imageModel = content.game.imageUrl,
            contentDescription = content.game.title,
            contentScale = ContentScale.FillWidth,
            modifier = Modifier
                .align(CenterHorizontally)
                .fillMaxWidth()
                .heightIn(max = 300.dp)
                .padding(horizontal = dimensionResource(id = R.dimen.size_16_dp))
                .clip(shape = RoundedCornerShape(dimensionResource(id = R.dimen.size_24_dp)))
        )

        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    top = dimensionResource(id = R.dimen.size_16_dp),
                    start = dimensionResource(id = R.dimen.size_16_dp),
                    end = dimensionResource(id = R.dimen.size_16_dp),
                    bottom = dimensionResource(id = R.dimen.size_24_dp),
                )

        ) {
            Text(
                text = content.game.title,
                style = MaterialTheme.typography.h3,
                color = Black000B2D,
                maxLines = 1,
            )

            TopStartBottomEndRoundedButton(
                text = stringResource(string.play),
                onClick = onClickPlay,
                backgroundGradient = RedFF512FPinkDD2476,
                modifier = Modifier
                    .align(CenterVertically)
                    .width(104.dp)
            )
        }

        Text(
            text = content.game.description,
            style = MaterialTheme.typography.body1,
            color = Black000B2D,
            modifier = Modifier
                .padding(horizontal = dimensionResource(id = R.dimen.size_16_dp))
        )
    }
}

@Composable
@Preview(
    showBackground = true
)
private fun GameDetailsScreenPreview() {
    FruktPartyTheme {
        GameDetailsScreen(
            content = GameDetailsScreenState.Content(
                game = Game(
                    id = "1",
                    title = "Some game title",
                    description = "Some description title",
                    imageUrl = "https://picsum.photos/800/400",
                    userScore = 5642
                ),
                route = ""
            ),
            modifier = Modifier,
            onClickBack = {},
            onClickPlay = {}
        )
    }
}