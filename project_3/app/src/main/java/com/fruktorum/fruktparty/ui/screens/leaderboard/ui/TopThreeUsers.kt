package com.fruktorum.fruktparty.ui.screens.leaderboard.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.BlueC1D0FF
import com.fruktorum.core.ui.theme.Gray9AA1B6
import com.fruktorum.core.ui.theme.Green2ACEC4Green2ABACE
import com.fruktorum.core.ui.theme.PurpleA226CEViolet6626CE
import com.fruktorum.core.ui.theme.WhiteFFFFFF
import com.fruktorum.core.ui.theme.YellowFFBE15OrangeFF9315
import com.fruktorum.core.ui.theme.YellowFFC530
import com.fruktorum.fruktparty.R
import com.fruktorum.fruktparty.ui.screens.leaderboard.model.LeaderboardUser
import com.skydoves.landscapist.glide.GlideImage
import me.onebone.toolbar.CollapsingToolbarScope
import kotlin.math.abs

@Composable
internal fun CollapsingToolbarScope.TopThreeUsers(
    state: Float,
    firstUser: LeaderboardUser,
    secondUser: LeaderboardUser?,
    thirdUser: LeaderboardUser?,
) {
    val state = abs(state - 1)
    val localDensity = LocalDensity.current
    val configuration = LocalConfiguration.current

    val itemWidth = (configuration.screenWidthDp.dp / 3) * (0.9f + (state * 0.1).toFloat())

    var heightFirstUser by remember {
        mutableStateOf(0.dp)
    }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height((configuration.screenWidthDp * if (secondUser != null) 0.7 else 0.6).dp)
            .pin()
    )

    User(
        modifier = Modifier
            .width(itemWidth)
            .padding(
                start = (16 + (15 * abs(state - 1))).dp,
                end = (3 + (12 * state)).dp
            )
            .road(Alignment.CenterStart, Alignment.BottomStart),
        user = secondUser,
        state = state,
        number = 2,
        brush = PurpleA226CEViolet6626CE,
        isBrushLight = false
    )

    User(
        modifier = Modifier
            .width((configuration.screenWidthDp.dp / 3) * (1.2f - (state * 0.2).toFloat()))
            .padding(
                start = (3 + (12 * state)).dp,
                end = (3 + (12 * state)).dp
            )
            .road(Alignment.TopCenter, Alignment.TopCenter)
            .onGloballyPositioned {
                heightFirstUser = with(localDensity) {
                    it.size.height.toDp()
                }
            },
        user = firstUser,
        state = state,
        number = 1,
        brush = YellowFFBE15OrangeFF9315,
        isBrushLight = true
    )

    User(
        modifier = Modifier
            .width(itemWidth)
            .padding(
                start = (3 + (12 * state)).dp,
                end = (16 + (15 * abs(state - 1))).dp
            )
            .road(Alignment.CenterEnd, Alignment.BottomEnd),
        user = thirdUser,
        state = state,
        number = 3,
        brush = Green2ACEC4Green2ABACE,
        isBrushLight = false
    )
}

@Composable
private fun User(
    modifier: Modifier = Modifier,
    user: LeaderboardUser?,
    state: Float,
    number: Int,
    brush: Brush,
    isBrushLight: Boolean
) = Column(
    modifier = modifier.alpha(alpha = if (user != null) 1f else 0f),
    horizontalAlignment = Alignment.CenterHorizontally
) {
    ConstraintLayout(
        modifier = Modifier.fillMaxWidth()
    ) {

        val (image, numberBox) = createRefs()

        val borderWidth = dimensionResource(id = com.fruktorum.core.R.dimen.size_5_dp)

        GlideImage(
            imageModel = user?.avatarUrl,
            error = painterResource(id = R.drawable.ic_avatar_default),
            modifier = Modifier
                .clip(CircleShape)
                .background(BlueC1D0FF)
                .border(
                    width = borderWidth,
                    brush = brush,
                    shape = CircleShape
                )
                .constrainAs(image) {
                    start.linkTo(parent.start)
                    top.linkTo(parent.top)
                    end.linkTo(parent.end)
                    bottom.linkTo(parent.bottom)
                }
                .aspectRatio(1f)
        )

        val localDensity = LocalDensity.current
        var heightNumber by remember {
            mutableStateOf(0.dp)
        }

        Box(
            modifier = Modifier
                .width(heightNumber)
                .constrainAs(numberBox) {
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    top.linkTo(
                        parent.bottom,
                        margin = -(heightNumber / 2) - borderWidth
                    )
                }
                .clip(CircleShape)
                .background(brush)
                .onGloballyPositioned {
                    heightNumber = with(localDensity) {
                        it.size.height.toDp()
                    }
                },
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = number.toString(),
                modifier = Modifier,
                style = MaterialTheme.typography.h3,
                color = if (isBrushLight) Black000B2D else WhiteFFFFFF
            )
        }
    }

    Spacer(
        modifier = Modifier
            .size(
                dimensionResource(id = com.fruktorum.core.R.dimen.size_27_dp) * (abs(state - 1))
            )
    )

    Text(
        text = user?.nickname.toString(),
        modifier = Modifier
            .alpha(abs(state - 1)),
        color = Black000B2D,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis,
        style = MaterialTheme.typography.body2
    )

    Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_2_dp)))

    Row(
        modifier = Modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_star_16),
            contentDescription = null,
            tint = YellowFFC530
        )

        Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_4_dp)))

        Text(
            text = user?.starsCount.toString(),
            color = Gray9AA1B6,
            style = MaterialTheme.typography.subtitle1
        )
    }
}