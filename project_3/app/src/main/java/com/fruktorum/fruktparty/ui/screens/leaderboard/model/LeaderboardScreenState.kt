package com.fruktorum.fruktparty.ui.screens.leaderboard.model

internal sealed class LeaderboardScreenState {

    object Loading : LeaderboardScreenState()

    object NoEvent : LeaderboardScreenState()

    class Content(
        val users: List<LeaderboardUser>,
        val currentUser: LeaderboardUser
    ) : LeaderboardScreenState()

    object Error : LeaderboardScreenState()
}
