package com.fruktorum.fruktparty.ui.screens.leaderboard.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.fruktorum.core.ui.theme.BlueC1D0FF
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.GrayD8E1FF
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFFFFFF
import com.fruktorum.core.ui.theme.YellowFFBE15OrangeFF9315
import com.fruktorum.core.ui.theme.YellowFFC530
import com.fruktorum.fruktparty.R
import com.fruktorum.fruktparty.ui.screens.leaderboard.model.LeaderboardUser
import com.skydoves.landscapist.glide.GlideImage

@Composable
internal fun CurrentLeaderboardUser(
    modifier: Modifier = Modifier,
    user: LeaderboardUser,
    onClickLetsPlay: () -> Unit
) = Box(
    modifier = modifier
        .background(
            brush = RedFF512FPinkDD2476,
            shape = RoundedCornerShape(
                topStart = dimensionResource(com.fruktorum.core.R.dimen.size_36_dp),
                bottomEnd = dimensionResource(com.fruktorum.core.R.dimen.size_36_dp)
            )
        )
) {
    Row(
        modifier = Modifier
            .padding(
                horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_24_dp)
            ),
        verticalAlignment = Alignment.CenterVertically
    ) {
        user.rank?.let {
            Text(
                text = it.toString(),
                style = MaterialTheme.typography.h3,
                color = WhiteFFFFFF
            )

            Spacer(modifier = Modifier.width(dimensionResource(com.fruktorum.core.R.dimen.size_24_dp)))
        }

        GlideImage(
            imageModel = user.avatarUrl,
            error = painterResource(id = R.drawable.ic_avatar_default),
            modifier = Modifier
                .padding(vertical = dimensionResource(com.fruktorum.core.R.dimen.size_20_dp))
                .size(dimensionResource(id = com.fruktorum.core.R.dimen.size_48_dp))
                .clip(CircleShape)
                .background(BlueC1D0FF)
        )

        Spacer(modifier = Modifier.width(dimensionResource(com.fruktorum.core.R.dimen.size_6_dp)))

        Column {
            Text(
                text = stringResource(R.string.leaderboard_screen_you_text),
                style = MaterialTheme.typography.h3,
                color = WhiteFFFFFF
            )

            Row(
                modifier = Modifier,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_star_16),
                    contentDescription = null,
                    tint = YellowFFC530
                )

                Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_3_dp)))

                Text(
                    text = user.starsCount.toString(),
                    color = GrayD8E1FF,
                    style = MaterialTheme.typography.subtitle1
                )
            }
        }

        Spacer(modifier = Modifier.weight(1f))

        Box(
            modifier = Modifier
                .padding(vertical = dimensionResource(com.fruktorum.core.R.dimen.size_18_dp))
                .background(
                    brush = YellowFFBE15OrangeFF9315,
                    RoundedCornerShape(
                        topStart = dimensionResource(id = com.fruktorum.core.R.dimen.size_30_dp),
                        bottomEnd = dimensionResource(id = com.fruktorum.core.R.dimen.size_30_dp)
                    )
                )
                .clip(
                    shape = RoundedCornerShape(
                        topStart = dimensionResource(id = com.fruktorum.core.R.dimen.size_30_dp),
                        bottomEnd = dimensionResource(id = com.fruktorum.core.R.dimen.size_30_dp)
                    )
                )
                .clickable(onClick = onClickLetsPlay)
        ) {
            Text(
                modifier = Modifier
                    .padding(
                        horizontal = dimensionResource(id = com.fruktorum.core.R.dimen.size_12_dp),
                        vertical = dimensionResource(id = com.fruktorum.core.R.dimen.size_15_dp)
                    ),
                text = stringResource(R.string.leaderboard_screen_lets_play_btn_text),
                style = MaterialTheme.typography.button,
                color = WhiteFFFFFF
            )
        }
    }
}

@Composable
@Preview(
    showBackground = true
)
private fun CurrentLeaderboardUserPreview() {
    FruktPartyTheme {
        CurrentLeaderboardUser(
            modifier = Modifier.padding(dimensionResource(com.fruktorum.core.R.dimen.size_16_dp)),
            user = LeaderboardUser(
                avatarUrl = "https://picsum.photos/400/400",
                nickname = "nickname_nickname_nickname",
                rank = 1,
                starsCount = 1234
            ),
            onClickLetsPlay = {}
        )
    }
}