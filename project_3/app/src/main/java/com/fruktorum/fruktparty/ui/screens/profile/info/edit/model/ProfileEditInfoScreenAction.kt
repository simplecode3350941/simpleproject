package com.fruktorum.fruktparty.ui.screens.profile.info.edit.model

internal sealed class ProfileEditInfoScreenAction {

    class NavigateTo(
        val route: String
    ) : ProfileEditInfoScreenAction()
}
