package com.fruktorum.fruktparty.ui.screens.profile.dialog

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.fruktorum.core.ui.composables.dialog.FruktPartyDialog
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.fruktparty.R

@Composable
internal fun DeleteAccountDialog(
    onClose: () -> Unit,
    onClickDeleteAccount: () -> Unit
) = FruktPartyDialog(
    title = stringResource(R.string.delete_account_dialog_title),
    subtitle = stringResource(R.string.delete_account_dialog_subtitle),
    dismissButtonText = stringResource(R.string.delete_account_dialog_dismiss_button_text),
    actionButtonText = stringResource(R.string.delete_account_dialog_log_out_button_text),
    onClose = onClose,
    onClickAction = onClickDeleteAccount
)

@Preview
@Composable
private fun DeleteAccountDialogPreview() {
    FruktPartyTheme {
        DeleteAccountDialog(
            onClose = {},
            onClickDeleteAccount = {}
        )
    }
}
