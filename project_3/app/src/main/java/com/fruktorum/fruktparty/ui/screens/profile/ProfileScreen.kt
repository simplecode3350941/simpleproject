package com.fruktorum.fruktparty.ui.screens.profile

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import com.fruktorum.core.ui.composables.screen.LoadingScreen
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.Black1C0003
import com.fruktorum.core.ui.theme.BlueC1D0FF
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.Gray9AA1B6
import com.fruktorum.core.ui.theme.GrayD8E1FF
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.core.ui.theme.WhiteFFFFFF
import com.fruktorum.core.ui.theme.YellowFFC530
import com.fruktorum.fruktparty.R
import com.fruktorum.fruktparty.ui.screens.error.ErrorScreen
import com.fruktorum.fruktparty.ui.screens.profile.dialog.DeleteAccountDialog
import com.fruktorum.fruktparty.ui.screens.profile.dialog.LogOutDialog
import com.fruktorum.fruktparty.ui.screens.profile.model.ProfileScreenAction
import com.fruktorum.fruktparty.ui.screens.profile.model.ProfileScreenDialogState
import com.fruktorum.fruktparty.ui.screens.profile.model.ProfileScreenEvent
import com.fruktorum.fruktparty.ui.screens.profile.model.ProfileScreenState
import com.skydoves.landscapist.glide.GlideImage

/**
 * Profile Screen
 */
@Composable
internal fun ProfileScreen(
    navController: NavHostController,
    viewModel: ProfileScreenViewModel,
    modifier: Modifier = Modifier
) {
    val state = viewModel.state.collectAsState(initial = ProfileScreenState.Loading)
    val dialogState = viewModel.dialogState.collectAsState(initial = ProfileScreenDialogState.None)

    when (val stateValue = state.value) {
        ProfileScreenState.Loading -> {
            LoadingScreen(modifier = modifier)
        }

        is ProfileScreenState.Content -> {
            ProfileScreen(
                content = stateValue,
                modifier = modifier
                    .statusBarsPadding(),
                onClickSeePrizes = {
                    viewModel.onEvent(ProfileScreenEvent.ClickOnSeePrizes)
                },
                onClickSeeProfile = {
                    viewModel.onEvent(ProfileScreenEvent.ClickOnSeeProfile)
                },
                onClickChangePassword = {
                    viewModel.onEvent(ProfileScreenEvent.ClickOnChangePassword)
                },
                onClickAboutUs = {
                    viewModel.onEvent(ProfileScreenEvent.ClickOnAboutUs)
                },
                onClickLogOut = {
                    viewModel.onEvent(ProfileScreenEvent.ClickOnLogOut)
                },
                onClickDeleteAccount = {
                    viewModel.onEvent(ProfileScreenEvent.ClickOnDeleteAccount)
                }
            )
        }

        ProfileScreenState.Error -> {
            ErrorScreen(
                modifier = modifier,
                onClickTryAgain = {
                    //TODO
                }
            )
        }
    }

    when (val dialogStateValue = dialogState.value) {
        ProfileScreenDialogState.None -> Unit
        ProfileScreenDialogState.LogOut -> {
            LogOutDialog(
                onClose = {
                    viewModel.onEvent(ProfileScreenEvent.CloseDialog)
                },
                onClickLogOut = {
                    viewModel.onEvent(ProfileScreenEvent.LogoutDialog)
                    viewModel.onEvent(ProfileScreenEvent.CloseDialog)
                }
            )
        }

        ProfileScreenDialogState.DeleteAccount -> {
            DeleteAccountDialog(
                onClose = {
                    viewModel.onEvent(ProfileScreenEvent.CloseDialog)
                },
                onClickDeleteAccount = {
                    //TODO
                }
            )
        }
    }

    LaunchedEffect(true) {
        viewModel.onEvent(ProfileScreenEvent.Start)

        viewModel.action.collect { action ->
            when (action) {
                is ProfileScreenAction.NavigateTo -> {
                    navController.navigate(
                        route = action.route
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalTextApi::class)
@Composable
private fun ProfileScreen(
    content: ProfileScreenState.Content,
    modifier: Modifier,
    onClickSeePrizes: () -> Unit,
    onClickSeeProfile: () -> Unit,
    onClickChangePassword: () -> Unit,
    onClickAboutUs: () -> Unit,
    onClickLogOut: () -> Unit,
    onClickDeleteAccount: () -> Unit
) = Column(
    modifier = modifier
        .fillMaxSize()
        .verticalScroll(rememberScrollState())
        //TODO delete this, get from theme
        .background(
            brush = WhiteFDFDFFGrayEEF2FF
        ),
    horizontalAlignment = Alignment.CenterHorizontally
) {
    Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_19_dp)))

    //TODO typography doesn't match with Figma typography
    Text(
        text = stringResource(id = R.string.profile_screen_title),
        color = Black000B2D,
        style = MaterialTheme.typography.body1
    )

    Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_30_dp)))

    GlideImage(
        imageModel = content.avatarUrl,
        error = painterResource(id = R.drawable.ic_avatar_default),
        modifier = Modifier
            .size(dimensionResource(id = com.fruktorum.core.R.dimen.size_145_dp))
            .clip(CircleShape)
            .background(BlueC1D0FF)
            .border(
                width = dimensionResource(id = com.fruktorum.core.R.dimen.size_5_dp),
                brush = RedFF512FPinkDD2476,
                shape = CircleShape
            )
    )

    Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_12_dp)))

    Text(
        text = content.nickname,
        color = Black000B2D,
        textAlign = TextAlign.Center,
        style = MaterialTheme.typography.h3
    )

    Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_8_dp)))

    Row(
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_star_16),
            contentDescription = null,
            tint = YellowFFC530
        )

        Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_6_dp)))

        Text(
            text = content.starsCount.toString(),
            color = Gray9AA1B6,
            style = MaterialTheme.typography.body1
        )
    }

    Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_24_dp)))

    PrizesCard(
        prizesCount = content.prizesCount,
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                horizontal = dimensionResource(id = com.fruktorum.core.R.dimen.size_16_dp)
            ),
        onClickSeePrizes = onClickSeePrizes
    )

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                horizontal = dimensionResource(id = com.fruktorum.core.R.dimen.size_16_dp),
                vertical = dimensionResource(id = com.fruktorum.core.R.dimen.size_20_dp)
            )
    ) {
        val itemPadding = PaddingValues(
            start = dimensionResource(id = com.fruktorum.core.R.dimen.size_32_dp),
            top = dimensionResource(id = com.fruktorum.core.R.dimen.size_6_dp),
            end = dimensionResource(id = com.fruktorum.core.R.dimen.size_20_dp),
            bottom = dimensionResource(id = com.fruktorum.core.R.dimen.size_6_dp)
        )

        NavigateActionItem(
            modifier = Modifier
                .clickable(
                    onClick = onClickSeeProfile
                )
                .padding(itemPadding),
            text = stringResource(R.string.profile_screen_see_profile_button_text)
        )

        Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_6_dp)))

        NavigateActionItem(
            modifier = Modifier
                .clickable(
                    onClick = onClickChangePassword
                )
                .padding(itemPadding),
            text = stringResource(R.string.profile_screen_change_password_button_text)
        )

        Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_11_dp)))

        ItemsDivider()

        Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_11_dp)))

        NavigateActionItem(
            modifier = Modifier
                .clickable(
                    onClick = onClickAboutUs
                )
                .padding(itemPadding),
            text = stringResource(R.string.profile_screen_about_us_button_text)
        )

        Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_11_dp)))

        ItemsDivider()

        Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_11_dp)))

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .clickable(onClick = onClickLogOut)
                .padding(itemPadding),
            text = stringResource(R.string.profile_screen_log_out_button_text),
            style = MaterialTheme.typography.body1.copy(
                brush = RedFF512FPinkDD2476
            )
        )

        Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_6_dp)))

        Text(
            modifier = Modifier
                .fillMaxWidth()
                .clickable(onClick = onClickDeleteAccount)
                .padding(itemPadding),
            text = stringResource(R.string.profile_screen_delete_account_button_text),
            style = MaterialTheme.typography.body1.copy(
                color = Black1C0003
            )
        )
    }
}

@OptIn(ExperimentalTextApi::class)
@Composable
private fun PrizesCard(
    prizesCount: Int,
    modifier: Modifier = Modifier,
    onClickSeePrizes: () -> Unit
) = Box(
    modifier = modifier
        .background(
            brush = RedFF512FPinkDD2476,
            shape = RoundedCornerShape(
                topStart = dimensionResource(com.fruktorum.core.R.dimen.size_36_dp),
                bottomEnd = dimensionResource(com.fruktorum.core.R.dimen.size_36_dp)
            )
        )
) {
    Row(
        verticalAlignment = Alignment.CenterVertically
    ) {
        Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_24_dp)))

        Image(
            painter = painterResource(R.drawable.img_goblet),
            contentDescription = null,
            modifier = Modifier
                .padding(
                    vertical = dimensionResource(id = com.fruktorum.core.R.dimen.size_20_dp)
                ),
            contentScale = ContentScale.Crop
        )

        Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_9_dp)))

        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = prizesCount.toString(),
                color = WhiteFFFFFF,
                style = MaterialTheme.typography.h1
            )

            Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_2_dp)))

            Text(
                text = stringResource(R.string.profile_screen_prizes_text),
                modifier = Modifier
                    //TODO fix
                    .offset(y = -(dimensionResource(id = com.fruktorum.core.R.dimen.size_16_dp))),
                color = WhiteFFFFFF,
                style = MaterialTheme.typography.subtitle2
            )
        }

        Spacer(modifier = Modifier.weight(1f))

        Box(
            modifier = Modifier
                .background(
                    color = WhiteFFFFFF,
                    RoundedCornerShape(
                        //TODO 30?
                        topStart = dimensionResource(id = com.fruktorum.core.R.dimen.size_30_dp),
                        bottomEnd = dimensionResource(id = com.fruktorum.core.R.dimen.size_30_dp)
                    )
                )
                .clip(
                    shape = RoundedCornerShape(
                        //TODO 30?
                        topStart = dimensionResource(id = com.fruktorum.core.R.dimen.size_30_dp),
                        bottomEnd = dimensionResource(id = com.fruktorum.core.R.dimen.size_30_dp)
                    )
                )
                .clickable(onClick = onClickSeePrizes)
        ) {
            Text(
                modifier = Modifier
                    .padding(
                        horizontal = dimensionResource(id = com.fruktorum.core.R.dimen.size_12_dp),
                        vertical = dimensionResource(id = com.fruktorum.core.R.dimen.size_16_dp)
                    ),
                text = stringResource(R.string.profile_screen_see_prizes_button_text),
                style = MaterialTheme.typography.body2.copy(
                    brush = RedFF512FPinkDD2476
                )
            )
        }

        Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_24_dp)))
    }
}

@Composable
private fun NavigateActionItem(
    modifier: Modifier,
    text: String
) = Row(
    modifier = modifier,
    verticalAlignment = Alignment.CenterVertically
) {
    Text(
        text = text,
        color = Black1C0003,
        style = MaterialTheme.typography.body1
    )

    Spacer(modifier = Modifier.weight(1f))

    Icon(
        painter = painterResource(R.drawable.ic_small_right_arrow_6),
        contentDescription = null,
        tint = GrayD8E1FF
    )
}

@Composable
private fun ItemsDivider() = Divider(
    modifier = Modifier
        .padding(
            start = dimensionResource(id = com.fruktorum.core.R.dimen.size_32_dp),
            end = dimensionResource(id = com.fruktorum.core.R.dimen.size_22_dp)
        ),
    color = GrayD8E1FF,
    thickness = dimensionResource(id = com.fruktorum.core.R.dimen.size_2_dp)
)

@Preview(
    showBackground = true
)
@Composable
private fun ProfileScreenPreview() {
    FruktPartyTheme {
        ProfileScreen(
            content = ProfileScreenState.Content(
                avatarUrl = null,
                nickname = "Long nick_Name",
                starsCount = 3225,
                prizesCount = 5
            ),
            modifier = Modifier
                .background(
                    brush = WhiteFDFDFFGrayEEF2FF
                ),
            onClickSeePrizes = {},
            onClickSeeProfile = {},
            onClickChangePassword = {},
            onClickAboutUs = {},
            onClickLogOut = {},
            onClickDeleteAccount = {}
        )
    }
}
