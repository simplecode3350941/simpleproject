package com.fruktorum.fruktparty.ui.screens.games

import android.util.Log
import com.fruktorum.core.sharedPreference.ConfigModel
import com.fruktorum.core.ui.vm.MVIViewModel
import com.fruktorum.domain.base.onError
import com.fruktorum.domain.base.onLoading
import com.fruktorum.domain.base.onSuccess
import com.fruktorum.domain.config.model.CheckUnviewedPrizesDomainModel
import com.fruktorum.domain.config.model.GetModeDomainModel
import com.fruktorum.domain.config.usecase.CheckUnviewedPrizesUseCase
import com.fruktorum.domain.config.usecase.GetModeUseCase
import com.fruktorum.domain.entity.Game
import com.fruktorum.domain.game.common.model.GetGamesDomainModel
import com.fruktorum.domain.game.common.usecase.GetGamesUseCase
import com.fruktorum.fruktparty.ui.navigation.getGameDetailsScreenRoute
import com.fruktorum.fruktparty.ui.screens.games.model.GamesScreenAction
import com.fruktorum.fruktparty.ui.screens.games.model.GamesScreenDialogState
import com.fruktorum.fruktparty.ui.screens.games.model.GamesScreenEvent
import com.fruktorum.fruktparty.ui.screens.games.model.GamesScreenState
import com.fruktorum.fruktparty.ui.screens.games.model.Mode
import com.fruktorum.fruktparty.ui.screens.games.model.toMode
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.flattenMerge
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject

@HiltViewModel
internal class GamesScreenViewModel
@Inject
constructor(
    private val getMode: GetModeUseCase,
    private val getGames: GetGamesUseCase,
    private val checkUnviewedPrizes: CheckUnviewedPrizesUseCase
) : MVIViewModel<GamesScreenState, GamesScreenDialogState?, GamesScreenEvent, GamesScreenAction>() {

    override fun onEvent(event: GamesScreenEvent) {
        when (event) {
            GamesScreenEvent.Start -> handleStartEvent()
            is GamesScreenEvent.ClickOnGame -> handleClickOnGame(event.game)
            is GamesScreenEvent.ClickOnInfo -> handleClickOnInfoEvent(event.mode)
            GamesScreenEvent.CloseDialog -> handleCloseDialogEvent()
        }
    }

    /*private fun handleStartEvent() = launchJob(
        exceptionBlock = {
            setState(
                GamesScreenState.Error
            )
        }
    ) {
        getGames().collect {
            with(it) {
                onLoading {
                    setState(GamesScreenState.Loading)
                }
                onSuccess {
                    this?.let {
                        setState(
                            GamesScreenState.Content(
                                //TODO get mode from getMode
                                mode = Mode.Event(
                                    value = EventUiModel.testObject
                                ),
                                games = this.games,
                                hasUnviewedPrizes = false
                            )
                        )
                    } ?: setState(
                        GamesScreenState.Error
                    )
                }
            }
        }
    }*/

    @OptIn(FlowPreview::class)
    private fun handleStartEvent() {
        launchJob(
            exceptionBlock = {
                setState(
                    GamesScreenState.Error(null)
                )
            }
        ) {
            Log.d("12345", "1 launchJob")
            coroutineScope {
                Log.d("12345", "2 coroutineScope")
                val contentJob: Deferred<List<Any>> = async {
                    Log.d("12345", "3 async")
                    val resultModels = mutableListOf<Any>()
                    Log.d("12345", "init resultModels = $resultModels")
                    launchJob {
                        Log.d("12345", "4 launchJob")
                        flowOf(getGames(), getMode(), checkUnviewedPrizes())
                            // объединяем потоки в один
                            .flattenMerge()
                            // и берем каждый элемент нового единого потока
                            .collect {
                                Log.d("12345", "5 collect")
                                with(it) {
                                    onLoading {
                                        Log.d("12345", "onLoading")
                                        Log.d("12345", "onLoading it = $it")
                                        setState(GamesScreenState.Loading)
                                    }
                                    onSuccess {
                                        Log.d("12345", "onSuccess")
                                        Log.d("12345", "onSuccess it = $it")
                                        // добавляем модель от каждого запроса в список
                                        this?.let { model -> resultModels.add(model) }
                                    }
                                    onError {
                                        Log.d("12345", "onError")
                                        setState(GamesScreenState.Error(this))
                                    }
                                }
                            }
                    }.join()
                    Log.d("12345", "4 launchJob STOP")
                    Log.d("12345", "resultModels = $resultModels")
                    // возвращаем список с моделями
                    resultModels

                }

                // ожидаем выполнение корутины
                contentJob.join() // возможно, лишее если await() ниже тоже ожидает выполнение корутины
                Log.d("12345", "3 async STOP")
                // получаем список с моделями от запросов
                val content = contentJob.await()
                Log.d("12345", "content = $content")

                var mode: Mode? = null
                var games: List<Game> = listOf()
                var hasUnviewedPrizes = false

                if (content.isNotEmpty()) {
                    Log.d("12345", "if content")
                    content.forEach {
                        Log.d("12345", "it = $it")
                        when (it) {
                            is GetModeDomainModel -> {
                                mode = it.toMode()
                                ConfigModel.mode = it.mode
                                Log.d("12345", "mode = $mode")
                            }
                            is GetGamesDomainModel -> {
                                games = it.games
                                Log.d("12345", "games = $games")
                            }
                            is CheckUnviewedPrizesDomainModel -> {
                                hasUnviewedPrizes = it.hasUnviewed
                                Log.d("12345", "hasUnviewedPrizes = $hasUnviewedPrizes")
                            }
                        }
                    }
                }

                mode?.let {
                    Log.d("12345", "setState")
                    setState(
                        GamesScreenState.Content(
                            mode = it,
                            games = games,
                            hasUnviewedPrizes = hasUnviewedPrizes
                        )
                    )
                } ?: setState(GamesScreenState.Error(null))
            }
        }
    }

    private fun handleClickOnGame(game: Game) {
        emitAction(
            GamesScreenAction.NavigateTo(
                route = getGameDetailsScreenRoute(game),
            )
        )
    }

    private fun handleCloseDialogEvent() {
        setDialogState(null)
    }

    private fun handleClickOnInfoEvent(mode: Mode) {
        setDialogState(
            when (mode) {
                is Mode.Base -> GamesScreenDialogState.NoEventInfoDialog
                is Mode.Event -> GamesScreenDialogState.EventInfoDialog(event = mode.value)
            }
        )
    }
}