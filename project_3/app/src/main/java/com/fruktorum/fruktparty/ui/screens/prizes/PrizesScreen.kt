package com.fruktorum.fruktparty.ui.screens.prizes

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.foundation.lazy.staggeredgrid.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import com.fruktorum.core.ui.composables.screen.LoadingScreen
import com.fruktorum.core.ui.composables.topBar.FruktPartyTopBar
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.fruktparty.R
import com.fruktorum.fruktparty.ui.screens.prizes.model.Prize
import com.fruktorum.fruktparty.ui.screens.prizes.model.PrizesScreenAction
import com.fruktorum.fruktparty.ui.screens.prizes.model.PrizesScreenState
import com.skydoves.landscapist.glide.GlideImage

/**
 * Prizes Screen
 */
@Composable
internal fun PrizesScreen(
    navController: NavHostController,
    viewModel: PrizesScreenViewModel,
    modifier: Modifier = Modifier
) {
    val state = viewModel.state.collectAsState(
        initial = PrizesScreenState.Loading
    )

    when (val stateValue = state.value) {
        PrizesScreenState.Loading -> {
            LoadingScreen(modifier = modifier)
        }
        is PrizesScreenState.Content -> {
            PrizesScreen(
                content = stateValue,
                modifier = modifier,
                onBackClick = {
                    navController.popBackStack()
                }
            )
        }
        PrizesScreenState.Error -> {
            //TODO
        }
    }

    LaunchedEffect(true) {
        viewModel.action.collect { action ->
            when (action) {
                is PrizesScreenAction.NavigateTo -> {
                    navController.navigate(
                        route = action.route
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun PrizesScreen(
    content: PrizesScreenState.Content,
    modifier: Modifier,
    onBackClick: () -> Unit,
) = Column(
    modifier = modifier
        .fillMaxSize()
        //TODO delete this, get from theme
        .background(
            brush = WhiteFDFDFFGrayEEF2FF
        ),
    horizontalAlignment = Alignment.CenterHorizontally
) {
    FruktPartyTopBar(
        title = stringResource(R.string.prizes_screen_title),
        modifier = Modifier.padding(top = dimensionResource(com.fruktorum.core.R.dimen.size_8_dp))
    ) {
        onBackClick()
    }

    LazyVerticalStaggeredGrid(
        columns = StaggeredGridCells.Fixed(2),
        modifier = Modifier.fillMaxSize(),
        contentPadding = PaddingValues(dimensionResource(com.fruktorum.core.R.dimen.size_8_dp))
    ) {
        itemsIndexed(content.prizes) { index, prize ->
            PrizeItem(
                prize = prize,
                modifier = Modifier.padding(dimensionResource(com.fruktorum.core.R.dimen.size_8_dp))
            )
        }
    }
}

@Composable
private fun PrizeItem(
    prize: Prize,
    modifier: Modifier
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
    ) {
        GlideImage(
            imageModel = prize.imageUrl,
            modifier = Modifier
                .fillMaxWidth()
                .aspectRatio(prize.ratio)
                .clip(
                    RoundedCornerShape(dimensionResource(com.fruktorum.core.R.dimen.size_16_dp))
                ),
            contentScale = ContentScale.FillWidth
        )

        Spacer(modifier = Modifier.height(dimensionResource(com.fruktorum.core.R.dimen.size_8_dp)))

        Text(
            text = prize.title,
            style = MaterialTheme.typography.body1,
            textAlign = TextAlign.Start,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis
        )
    }
}

@Preview(
    showBackground = true
)
@Composable
private fun PrizesScreenPreview() {
    FruktPartyTheme {
        PrizesScreen(
            content = PrizesScreenState.Content(
                prizes = Prize.listOfTestPrizes
            ),
            modifier = Modifier,
            onBackClick = {}
        )
    }
}
