package com.fruktorum.fruktparty.ui.screens.profile.changepassword.model

internal sealed class ChangePasswordScreenState {

    data class EnteringValues(
        val oldPassword: String = "",
        val isVisibleOldPassword: Boolean = false,
        val newPassword: String = "",
        val isVisibleNewPassword: Boolean = false,
        val confirmNewPassword: String = "",
        val isVisibleConfirmNewPassword: Boolean = false,
    ) : ChangePasswordScreenState()
}
