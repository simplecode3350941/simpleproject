@file:OptIn(ExperimentalPagerApi::class)

package com.fruktorum.fruktparty.ui.screens.signUp

import android.annotation.SuppressLint
import android.text.InputType
import android.view.MotionEvent
import android.view.View.OnTouchListener
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatEditText
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavHostController
import com.fruktorum.core.ui.composables.buttons.TopStartBottomEndRoundedButton
import com.fruktorum.core.ui.composables.screen.LoadingScreen
import com.fruktorum.core.ui.composables.textFields.FruktPartyTextField
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.Black0C002C
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.Gray9AA1B6
import com.fruktorum.core.ui.theme.GrayD8E1FF
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.fruktparty.R
import com.fruktorum.fruktparty.ui.screens.TwoLayerBasicScreen
import com.fruktorum.fruktparty.ui.screens.signUp.model.SignUpScreenAction
import com.fruktorum.fruktparty.ui.screens.signUp.model.SignUpScreenEvent
import com.fruktorum.fruktparty.ui.screens.signUp.model.SignUpScreenField
import com.fruktorum.fruktparty.ui.screens.signUp.model.SignUpScreenPage
import com.fruktorum.fruktparty.ui.screens.signUp.model.SignUpScreenState
import com.fruktorum.fruktparty.ui.screens.tutorial.DotsIndicator
import com.fruktorum.ftauth.FTAuth
import com.fruktorum.ftauth.customUI.registration.FTRegistrationBirthdateField
import com.fruktorum.ftauth.customUI.registration.FTRegistrationConfirmPasswordInputField
import com.fruktorum.ftauth.customUI.registration.FTRegistrationEmailInputField
import com.fruktorum.ftauth.customUI.registration.FTRegistrationFirstNameInputField
import com.fruktorum.ftauth.customUI.registration.FTRegistrationLastNameInputField
import com.fruktorum.ftauth.customUI.registration.FTRegistrationNameInputField
import com.fruktorum.ftauth.customUI.registration.FTRegistrationPasswordInputField
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.PagerState
import com.google.accompanist.pager.rememberPagerState


/**
 * Sign up Screen
 */
@Composable
internal fun SignUpScreen(
    navController: NavHostController,
    viewModel: SignUpScreenViewModel,
    modifier: Modifier = Modifier
) {
    val state = viewModel.state.collectAsState(initial = SignUpScreenState.Content())

    val pagerState = rememberPagerState(
        SignUpScreenPage.values().first().ordinal
    )

    when (val stateValue = state.value) {
        SignUpScreenState.Loading -> {
            LoadingScreen(modifier = modifier)
        }
        is SignUpScreenState.Content -> {
            SignUpScreen(
                content = stateValue,
                modifier = modifier,
                pagerState = pagerState,
                onChangeFieldValue = {
                    viewModel.onEvent(
                        SignUpScreenEvent.FieldValueChange(it)
                    )
                },
                onClickBack = {
                    viewModel.onEvent(
                        SignUpScreenEvent.ClickOnBack
                    )
                },
                onClickOnContinueWithGmail = {
                    viewModel.onEvent(
                        SignUpScreenEvent.ClickOnContinueWithGmail
                    )
                },
                onClickNext = {
                    viewModel.onEvent(
                        SignUpScreenEvent.ClickOnNext
                    )
                },
                onClickSignUp = {
                    viewModel.onEvent(
                        SignUpScreenEvent.ClickOnSignUp
                    )
                }
            )
        }
        SignUpScreenState.Error -> Unit
    }

    LaunchedEffect(true) {
        viewModel.onEvent(SignUpScreenEvent.Start)

        viewModel.action.collect { action ->
            when (action) {
                is SignUpScreenAction.NavigateTo -> {
                    navController.navigate(
                        route = action.route
                    )
                }
                SignUpScreenAction.Back -> {
                    navController.popBackStack()
                }
                is SignUpScreenAction.ScrollToPage -> {
                    pagerState.animateScrollToPage(
                        page = action.page.ordinal
                    )
                }
            }
        }
    }
}

@SuppressLint("ClickableViewAccessibility")
@OptIn(ExperimentalPagerApi::class, ExperimentalTextApi::class, ExperimentalMaterialApi::class)
@Composable
private fun SignUpScreen(
    content: SignUpScreenState.Content,
    modifier: Modifier,
    pagerState: PagerState,
    onChangeFieldValue: (SignUpScreenField) -> Unit,
    onClickBack: () -> Unit,
    onClickOnContinueWithGmail: () -> Unit,
    onClickNext: () -> Unit,
    onClickSignUp: () -> Unit
) {
    val context = LocalContext.current
    val focusManager = LocalFocusManager.current
    val uriHandler = LocalUriHandler.current

    val iconCloseEye = AppCompatResources.getDrawable(context, R.drawable.ic_close_eye)
    val iconOpenEye = AppCompatResources.getDrawable(context, R.drawable.ic_open_eye)

    val compoundDrawablesListener = OnTouchListener { view, event ->
        val drawableRight = 2
        val ourInputField = (view as AppCompatEditText)

        if (event.action == MotionEvent.ACTION_UP) {
            if (event.rawX >= view.right - ourInputField.compoundDrawables[drawableRight].bounds.width()) {
                if (ourInputField.inputType == InputType.TYPE_CLASS_TEXT) {
                    ourInputField.setCompoundDrawablesWithIntrinsicBounds(
                        null,
                        null,
                        iconCloseEye,
                        null
                    )
                    ourInputField.inputType =
                        InputType.TYPE_TEXT_VARIATION_PASSWORD or InputType.TYPE_CLASS_TEXT
                } else {
                    ourInputField.setCompoundDrawablesWithIntrinsicBounds(
                        null,
                        null,
                        iconOpenEye,
                        null
                    )
                    ourInputField.inputType = InputType.TYPE_CLASS_TEXT
                }
                return@OnTouchListener true
            }
        }
        false
    }


    val hintNickname =
        stringResource(R.string.sign_up_screen_your_nickname_text_field_label)

    val hintRepeatPassword =
        stringResource(R.string.sign_up_screen_repeat_password_text_field_label)

    val hintSecondName =
        stringResource(R.string.sign_up_screen_second_name_text_field_label)

    val hintBirthdate =
        stringResource(id = R.string.sign_up_screen_birthday_text_field_label)


    var fieldRegistrationName = FTRegistrationNameInputField(context).apply {
        this.setInputFieldStyle(R.style.FTAuthDefaultInputField)
        this.inputField.hint = hintNickname
    }

    var fieldRegistrationEmail = FTRegistrationEmailInputField(context).apply {
        this.setInputFieldStyle(R.style.FTAuthDefaultInputField)
    }

    var fieldRegistrationPassword = FTRegistrationPasswordInputField(context).apply {
        this.setInputFieldStyle(R.style.FTAuthDefaultInputField)
        this.setInputFieldStyle(R.style.FTAuthDefaultInputField)
        this.inputField.setCompoundDrawablesWithIntrinsicBounds(null, null, iconCloseEye, null)
        this.inputField.setOnTouchListener(compoundDrawablesListener)
    }

    var fieldRegistrationRepeatPassword =
        FTRegistrationConfirmPasswordInputField(context).apply {
            this.setInputFieldStyle(R.style.FTAuthDefaultInputField)
            this.inputField.hint = hintRepeatPassword
            this.inputField.setCompoundDrawablesWithIntrinsicBounds(null, null, iconCloseEye, null)
            this.inputField.setOnTouchListener(compoundDrawablesListener)
        }

    var fieldRegistrationFirstName = FTRegistrationFirstNameInputField(context).apply {
        this.setInputFieldStyle(R.style.FTAuthDefaultInputField)
    }

    var fieldRegistrationLastName = FTRegistrationLastNameInputField(context).apply {
        this.setInputFieldStyle(R.style.FTAuthDefaultInputField)
        this.inputField.hint = hintSecondName
    }

    var fieldRegistrationBirthdate = FTRegistrationBirthdateField(context).apply {
        this.setInputFieldStyle(R.style.FTAuthDefaultInputField)
        this.inputField.hint = hintBirthdate
    }

    TwoLayerBasicScreen(
        title = stringResource(
            when (content.page) {
                SignUpScreenPage.PromoCode -> R.string.sign_up_screen_first_step_title
                SignUpScreenPage.Main,
                SignUpScreenPage.Final -> R.string.sign_up_screen_sign_up_title
            }
        ),
        onClickBack = onClickBack
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_40_dp)))

            val fieldModifierFirstStep = Modifier
                .fillMaxWidth()
                .padding(
                    horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_44_dp),
                    vertical = dimensionResource(com.fruktorum.core.R.dimen.size_16_dp)
                )

            val fieldModifierSecondStep = Modifier
                .fillMaxWidth()
                .padding(
                    horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_44_dp),
                    vertical = dimensionResource(com.fruktorum.core.R.dimen.size_0_dp)
                )

            val fieldModifierThirdStep = Modifier
                .fillMaxWidth()
                .padding(
                    horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_44_dp),
                    vertical = dimensionResource(com.fruktorum.core.R.dimen.size_0_dp)
                )

            HorizontalPager(
                count = SignUpScreenPage.values().size,
                modifier = Modifier
                    .weight(1f),
                state = pagerState,
                userScrollEnabled = false
            ) { page ->
                when (page) {
                    SignUpScreenPage.PromoCode.ordinal -> {
                        Column(
                            modifier = Modifier
                                .fillMaxSize()
                                .verticalScroll(rememberScrollState())
                        ) {
                            FruktPartyTextField(
                                value = content.promoCode,
                                onValueChange = {
                                    onChangeFieldValue(
                                        SignUpScreenField.PromoCode(
                                            it
                                        )
                                    )
                                },
                                modifier = fieldModifierFirstStep,
                                keyboardOptions = KeyboardOptions.Default.copy(
                                    capitalization = KeyboardCapitalization.Characters,
                                    autoCorrect = false,
                                    imeAction = ImeAction.Done
                                ),
                                keyboardActions = KeyboardActions(
                                    onDone = { focusManager.clearFocus(force = true) }
                                ),
                                placeholder = stringResource(R.string.sign_up_screen_put_a_promo_code_text_field_label)
                            )

                            Row(
                                modifier = fieldModifierFirstStep
                            ) {
                                Image(
                                    painter = painterResource(R.drawable.ic_star_12),
                                    contentDescription = null,
                                    modifier = Modifier
                                        .padding(
                                            top = dimensionResource(com.fruktorum.core.R.dimen.size_4_dp),
                                            end = dimensionResource(com.fruktorum.core.R.dimen.size_5_dp)
                                        )
                                        .size(dimensionResource(com.fruktorum.core.R.dimen.size_12_dp))
                                )

                                Text(
                                    text = stringResource(R.string.sign_up_screen_promo_code_description),
                                    style = MaterialTheme.typography.body2,
                                    color = Black000B2D
                                )
                            }
                        }
                    }
                    SignUpScreenPage.Main.ordinal -> {
                        Column(
                            modifier = Modifier
                                .fillMaxSize()
                                .verticalScroll(rememberScrollState())
                        ) {
                            AndroidView(
                                factory = {
                                    fieldRegistrationName
                                },
                                modifier = fieldModifierSecondStep,
                                update = {
                                    fieldRegistrationName = it
                                    FTAuth.registerNameInputField = fieldRegistrationName
                                }
                            )

                            AndroidView(
                                factory = {
                                    fieldRegistrationEmail
                                },
                                modifier = fieldModifierSecondStep,
                                update = {
                                    fieldRegistrationEmail = it
                                    FTAuth.registerEmailInputField = fieldRegistrationEmail
                                }
                            )

                            AndroidView(
                                factory = {
                                    fieldRegistrationPassword
                                },
                                modifier = fieldModifierSecondStep,
                                update = {
                                    fieldRegistrationPassword = it
                                    FTAuth.registerPasswordInputField = fieldRegistrationPassword
                                }
                            )

                            AndroidView(
                                factory = {
                                    fieldRegistrationRepeatPassword
                                },
                                modifier = fieldModifierSecondStep,
                                update = {
                                    fieldRegistrationRepeatPassword = it
                                    FTAuth.registerConfirmPasswordInputField =
                                        fieldRegistrationRepeatPassword
                                }
                            )

                            Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_8_dp)))

                            Text(
                                text = stringResource(R.string.or),
                                color = Gray9AA1B6,
                                style = MaterialTheme.typography.body1,
                                modifier = Modifier
                                    .align(Alignment.CenterHorizontally)
                            )

                            TopStartBottomEndRoundedButton(
                                image = R.drawable.ic_gmail,
                                text = stringResource(R.string.login_screen_gmail_login_text),
                                textColor = Black000B2D,
                                backgroundColor = Color.White,
                                onClick = onClickOnContinueWithGmail,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(
                                        horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_44_dp),
                                        vertical = dimensionResource(com.fruktorum.core.R.dimen.size_24_dp)
                                    )
                            )
                        }
                    }
                    SignUpScreenPage.Final.ordinal -> {
                        Column(
                            modifier = Modifier
                                .fillMaxSize()
                                .verticalScroll(rememberScrollState())
                        ) {

                            AndroidView(
                                factory = {
                                    fieldRegistrationFirstName
                                },
                                modifier = fieldModifierThirdStep,
                                update = {
                                    fieldRegistrationFirstName = it
                                    FTAuth.registerFirstNameInputField = fieldRegistrationFirstName
                                }
                            )

                            AndroidView(
                                factory = {
                                    fieldRegistrationLastName
                                },
                                modifier = fieldModifierThirdStep,
                                update = {
                                    fieldRegistrationLastName = it
                                    FTAuth.registerLastNameInputField = fieldRegistrationLastName
                                }
                            )

                            AndroidView(
                                factory = {
                                    fieldRegistrationBirthdate
                                },
                                modifier = fieldModifierThirdStep,
                                update = {
                                    fieldRegistrationBirthdate = it
                                    FTAuth.registerBirthdateInputField = fieldRegistrationBirthdate
                                }
                            )

                            Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_8_dp)))

                            Row(
                                modifier = fieldModifierThirdStep
                            ) {
                                Image(
                                    painter = painterResource(R.drawable.ic_star_12),
                                    contentDescription = null,
                                    modifier = Modifier
                                        .padding(
                                            top = dimensionResource(com.fruktorum.core.R.dimen.size_4_dp),
                                            end = dimensionResource(com.fruktorum.core.R.dimen.size_5_dp)
                                        )
                                        .size(dimensionResource(com.fruktorum.core.R.dimen.size_12_dp))
                                )

                                val uriTag = "URI"
                                val annotatedString = buildAnnotatedString {
                                    val text =
                                        stringResource(R.string.sign_up_screen_agreements)
                                    val textTermsOfUse =
                                        stringResource(R.string.sign_up_screen_agreements_terms_of_use_clickable_text)
                                    val textPrivacyPolicy =
                                        stringResource(R.string.sign_up_screen_agreements_privacy_policy_clickable_text)

                                    withStyle(
                                        style = SpanStyle(
                                            color = Black000B2D
                                        )
                                    ) {
                                        append(text)
                                    }
                                    addStyle(
                                        style = SpanStyle(
                                            brush = RedFF512FPinkDD2476
                                        ),
                                        start = text.indexOf(textTermsOfUse),
                                        end = text.length
                                    )
                                    addStyle(
                                        style = SpanStyle(
                                            brush = Brush.horizontalGradient(
                                                listOf(
                                                    Black000B2D,
                                                    Black000B2D
                                                )
                                            )
                                        ),
                                        start = text.indexOf(textTermsOfUse) + textTermsOfUse.length,
                                        end = text.indexOf(textPrivacyPolicy)
                                    )
                                    addStringAnnotation(
                                        tag = uriTag,
                                        annotation = stringResource(R.string.sign_up_screen_agreements_terms_of_use_clickable_text_link),
                                        start = text.indexOf(textTermsOfUse),
                                        end = text.indexOf(textTermsOfUse) + textTermsOfUse.length
                                    )
                                    addStringAnnotation(
                                        tag = uriTag,
                                        annotation = stringResource(R.string.sign_up_screen_agreements_privacy_policy_clickable_text_link),
                                        start = text.indexOf(textPrivacyPolicy),
                                        end = text.indexOf(textPrivacyPolicy) + textPrivacyPolicy.length
                                    )
                                }

                                ClickableText(
                                    text = annotatedString,
                                    modifier = Modifier
                                        .weight(1f),
                                    style = MaterialTheme.typography.body2,
                                    onClick = { index ->
                                        val annotations = annotatedString.getStringAnnotations(
                                            uriTag,
                                            start = index,
                                            end = index
                                        )
                                        annotations.firstOrNull()?.let {
                                            uriHandler.openUri(it.item)
                                        }
                                    }
                                )
                            }
                        }
                    }
                }
            }

            Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_16_dp)))

            DotsIndicator(
                totalDots = SignUpScreenPage.values().size,
                selectedIndex = content.page.ordinal,
                selectedColor = RedFF512FPinkDD2476,
                unSelectedColor = GrayD8E1FF,
                modifier = Modifier
            )

            Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_16_dp)))

            TextButton(
                shape = RoundedCornerShape(
                    topStart = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp),
                    bottomEnd = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_44_dp)
                    )
                    .background(
                        Black0C002C,
                        RoundedCornerShape(
                            topStart = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp),
                            bottomEnd = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)
                        )
                    ),
                contentPadding = PaddingValues(
                    all = dimensionResource(com.fruktorum.core.R.dimen.size_15_dp)
                ),
                onClick = {
                    when (content.page) {
                        SignUpScreenPage.PromoCode -> {
                            onClickNext.invoke()
                        }
                        SignUpScreenPage.Main -> {
                            fieldRegistrationEmail.validate()
                            fieldRegistrationName.validate()
                            fieldRegistrationPassword.validate()
                            fieldRegistrationRepeatPassword.validate()

                            if (fieldRegistrationEmail.isEmailValid
                                && fieldRegistrationName.isNameValid
                                && fieldRegistrationPassword.isPasswordValid
                                && fieldRegistrationRepeatPassword.isPasswordValid
                            ) {
                                onClickNext.invoke()
                            }
                        }
                        SignUpScreenPage.Final -> {
                            fieldRegistrationFirstName.validate()
                            fieldRegistrationLastName.validate()
                            fieldRegistrationBirthdate.validate()
                            if (fieldRegistrationFirstName.isFirstNameValid
                                && fieldRegistrationLastName.isLastNameValid
                                && fieldRegistrationBirthdate.isBirthdateValid
                            ) {
                                onClickSignUp()
                            }
                        }
                    }
                }
            ) {
                Text(
                    text = stringResource(
                        when (content.page) {
                            SignUpScreenPage.PromoCode,
                            SignUpScreenPage.Main -> R.string.sign_up_screen_next_button_text
                            SignUpScreenPage.Final -> R.string.sign_up_screen_sign_up_button_text
                        }
                    ),
                    style = MaterialTheme.typography.body1,
                    color = Color.White
                )
            }

            Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_40_dp)))
        }
    }
}

@Preview(
    showBackground = true
)
@Composable
private fun SignUpScreenPreview() {
    FruktPartyTheme {
        SignUpScreen(
            content = SignUpScreenState.Content(),
            modifier = Modifier,
            pagerState = rememberPagerState(),
            onChangeFieldValue = {},
            onClickBack = {},
            onClickOnContinueWithGmail = {},
            onClickNext = {},
            onClickSignUp = {}
        )
    }
}