package com.fruktorum.fruktparty.ui.screens.welcome

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.asPaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBars
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.fruktorum.core.ui.theme.Black0C002C
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.fruktparty.R
import com.fruktorum.fruktparty.ui.navigation.LOGIN_SCREEN
import com.fruktorum.fruktparty.ui.navigation.SIGN_UP_SCREEN

@Composable
@Preview
fun WelcomeScreen(
    navController: NavHostController = rememberNavController()
) {
    Scaffold {
        FruktPartyTheme {
            ConstraintLayout(
                modifier = Modifier
                    .background(RedFF512FPinkDD2476)
                    .padding(it)
                    .fillMaxSize()
            ) {
                val (image, content) = createRefs()
                val statusBarHeight =
                    WindowInsets.statusBars.asPaddingValues().calculateTopPadding()
                val dimen16 = dimensionResource(id = com.fruktorum.core.R.dimen.size_16_dp)
                val dimen20 = dimensionResource(id = com.fruktorum.core.R.dimen.size_20_dp)
                val dimen32 = dimensionResource(id = com.fruktorum.core.R.dimen.size_32_dp)
                val dimen40 = dimensionResource(id = com.fruktorum.core.R.dimen.size_40_dp)
                val dimen43 = dimensionResource(id = com.fruktorum.core.R.dimen.size_43_dp)
                val dimen52 = dimensionResource(id = com.fruktorum.core.R.dimen.size_52_dp)
                val dimen64 = dimensionResource(id = com.fruktorum.core.R.dimen.size_64_dp)

                Image(
                    painter = painterResource(id = R.drawable.welcome_image),
                    contentDescription = "",
                    contentScale = ContentScale.FillWidth,
                    alignment = Alignment.Center,
                    modifier = Modifier.constrainAs(image) {
                        top.linkTo(parent.top, margin = dimen16 + statusBarHeight)
                        start.linkTo(parent.start, margin = dimen20)
                        end.linkTo(parent.end, margin = dimen20)
                        width = Dimension.fillToConstraints
                    }
                )

                Box(
                    modifier = Modifier
                        .constrainAs(content) {
                            top.linkTo(image.bottom, margin = -dimen52)
                            bottom.linkTo(parent.bottom)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                            width = Dimension.fillToConstraints
                            height = Dimension.fillToConstraints
                        }
                        .background(
                            brush = WhiteFDFDFFGrayEEF2FF,
                            RoundedCornerShape(topStart = dimen64)
                        )
                ) {
                    ConstraintLayout(modifier = Modifier.fillMaxSize()) {
                        val (title, login, signup) = createRefs()

                        Text(
                            text = stringResource(id = R.string.welcome_title),
                            color = Color.Black,
                            textAlign = TextAlign.Center,
                            style = MaterialTheme.typography.h1,
                            modifier = Modifier.constrainAs(title) {
                                top.linkTo(parent.top, margin = dimen64)
                                start.linkTo(parent.start)
                                end.linkTo(parent.end)
                                width = Dimension.wrapContent
                                height = Dimension.wrapContent
                            }
                        )

                        TextButton(
                            shape = RoundedCornerShape(topStart = dimen32, bottomEnd = dimen32),
                            modifier = Modifier
                                .constrainAs(login) {
                                    top.linkTo(title.bottom, margin = dimen40)
                                    start.linkTo(parent.start, margin = dimen43)
                                    end.linkTo(parent.end, margin = dimen43)
                                    width = Dimension.fillToConstraints
                                    height = Dimension.value(dimen52)
                                }
                                .background(
                                    RedFF512FPinkDD2476,
                                    RoundedCornerShape(topStart = dimen32, bottomEnd = dimen32)
                                ),
                            onClick = { navController.navigate(LOGIN_SCREEN) }
                        ) {
                            Text(
                                text = stringResource(id = R.string.welcome_log_in),
                                style = MaterialTheme.typography.body1,
                                color = Color.White
                            )
                        }

                        TextButton(
                            shape = RoundedCornerShape(topStart = dimen32, bottomEnd = dimen32),
                            modifier = Modifier
                                .constrainAs(signup) {
                                    top.linkTo(login.bottom, margin = dimen16)
                                    start.linkTo(parent.start, margin = dimen43)
                                    end.linkTo(parent.end, margin = dimen43)
                                    width = Dimension.fillToConstraints
                                    height = Dimension.value(dimen52)
                                }
                                .background(
                                    Black0C002C,
                                    RoundedCornerShape(topStart = dimen32, bottomEnd = dimen32)
                                ),
                            onClick = {
                                navController.navigate(
                                    route = SIGN_UP_SCREEN
                                )
                            }
                        ) {
                            Text(
                                text = stringResource(id = R.string.welcome_sign_up),
                                style = MaterialTheme.typography.body1,
                                color = Color.White
                            )
                        }
                    }
                }
            }
        }
    }
}