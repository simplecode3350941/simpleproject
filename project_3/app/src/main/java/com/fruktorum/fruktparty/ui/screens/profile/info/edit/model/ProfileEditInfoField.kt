package com.fruktorum.fruktparty.ui.screens.profile.info.edit.model

internal sealed class ProfileEditInfoField(
    val value: String
) {
    class AvatarUrl(value: String) : ProfileEditInfoField(value)

    class Name(value: String) : ProfileEditInfoField(value)

    class Surname(value: String) : ProfileEditInfoField(value)

    class Nickname(value: String) : ProfileEditInfoField(value)

    class Birthdate(value: String) : ProfileEditInfoField(value)
}
