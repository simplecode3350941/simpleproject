package com.fruktorum.fruktparty.ui.screens.signUp.model

internal sealed class SignUpScreenField(
    val value: String
) {

    class PromoCode(value: String) : SignUpScreenField(value)

    class Nickname(value: String) : SignUpScreenField(value)

    class Email(value: String) : SignUpScreenField(value)

    class Password(value: String) : SignUpScreenField(value)

    class RepeatPassword(value: String) : SignUpScreenField(value)

    class Name(value: String) : SignUpScreenField(value)

    class Surname(value: String) : SignUpScreenField(value)

    class Birthdate(value: String) : SignUpScreenField(value)
}
