package com.fruktorum.fruktparty.ui.screens.signUp.model

internal sealed class SignUpScreenAction {

    class NavigateTo(
        val route: String
    ) : SignUpScreenAction()

    object Back : SignUpScreenAction()

    class ScrollToPage(
        val page: SignUpScreenPage
    ) : SignUpScreenAction()
}
