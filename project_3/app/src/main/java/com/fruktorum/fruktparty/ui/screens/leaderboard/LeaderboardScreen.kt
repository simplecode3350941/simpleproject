package com.fruktorum.fruktparty.ui.screens.leaderboard

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.fruktorum.core.ui.composables.screen.LoadingScreen
import com.fruktorum.core.ui.theme.GrayD8E1FF
import com.fruktorum.core.ui.theme.WhiteEEF2FF
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.fruktparty.ui.screens.leaderboard.model.LeaderboardScreenAction
import com.fruktorum.fruktparty.ui.screens.leaderboard.model.LeaderboardScreenDialogState
import com.fruktorum.fruktparty.ui.screens.leaderboard.model.LeaderboardScreenEvent
import com.fruktorum.fruktparty.ui.screens.leaderboard.model.LeaderboardScreenState
import com.fruktorum.fruktparty.ui.screens.leaderboard.ui.CurrentLeaderboardUser
import com.fruktorum.fruktparty.ui.screens.leaderboard.ui.LeaderboardHeader
import com.fruktorum.fruktparty.ui.screens.leaderboard.ui.LeaderboardUser
import com.fruktorum.fruktparty.ui.screens.leaderboard.ui.NoEventLeaderboardScreen
import com.fruktorum.fruktparty.ui.screens.leaderboard.ui.NoPlayersLeaderboardScreen
import com.fruktorum.fruktparty.ui.screens.leaderboard.ui.OnlyCurrentUserPlayedScreen
import com.fruktorum.fruktparty.ui.screens.leaderboard.ui.TopThreeUsers
import com.fruktorum.fruktparty.ui.screens.leaderboard.ui.dialog.InfoAboutGiftsDialog
import me.onebone.toolbar.CollapsingToolbarScaffold
import me.onebone.toolbar.ScrollStrategy
import me.onebone.toolbar.rememberCollapsingToolbarScaffoldState

@Composable
internal fun LeaderboardScreen(
    navController: NavHostController,
    viewModel: LeaderboardScreenViewModel,
    modifier: Modifier = Modifier
) {
    val state = viewModel.state.collectAsState(initial = LeaderboardScreenState.Loading)
    val dialogState = viewModel.dialogState.collectAsState(
        initial = LeaderboardScreenDialogState.None
    )

    when (val stateValue = state.value) {
        LeaderboardScreenState.Loading -> {
            LoadingScreen(modifier = modifier)
        }
        is LeaderboardScreenState.Content -> {
            if (stateValue.users.isNotEmpty()) {
                if (stateValue.users.size == 1 && stateValue.users.first() == stateValue.currentUser) {
                    OnlyCurrentUserPlayedScreen(
                        modifier = modifier,
                        onClickPlayMore = {
                            //TODO
                        },
                        stars = stateValue.currentUser.starsCount
                    )
                } else {
                    LeaderboardScreen(
                        content = stateValue,
                        modifier = modifier,
                        onClickInfoIcon = {
                            //TODO
                        },
                        onClickGetGift = {
                            //TODO
                        },
                        onClickLetsPlay = {
                            viewModel.onEvent(LeaderboardScreenEvent.ClickOnLetsPlay)
                        }
                    )
                }
            } else {
                NoPlayersLeaderboardScreen(
                    modifier = modifier,
                    onClickLetsStartToPlay = {
                        //TODO
                    }
                )
            }
        }
        LeaderboardScreenState.NoEvent -> {
            NoEventLeaderboardScreen(
                modifier = modifier,
                onClickPlayMore = {
                    //TODO
                }
            )
        }
        LeaderboardScreenState.Error -> {
            //TODO
        }
    }

    when (val dialogStateValue = dialogState.value) {
        LeaderboardScreenDialogState.None -> Unit
        LeaderboardScreenDialogState.InfoAboutGifts -> {
            InfoAboutGiftsDialog(
                onClose = {
                    viewModel.onEvent(LeaderboardScreenEvent.CloseDialog)
                },
                onClickLetsPlay = {
                    //TODO
                }
            )
        }
        LeaderboardScreenDialogState.Gift -> {
            //TODO
        }
    }

    LaunchedEffect(true) {
        viewModel.action.collect { action ->
            when (action) {
                is LeaderboardScreenAction.NavigateTo -> {
                    navController.navigate(
                        route = action.route
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun LeaderboardScreen(
    content: LeaderboardScreenState.Content,
    modifier: Modifier,
    onClickInfoIcon: () -> Unit,
    onClickGetGift: () -> Unit,
    onClickLetsPlay: () -> Unit
) = Column(
    modifier = modifier
        .fillMaxSize()
        //TODO delete this, get from theme
        .background(
            brush = WhiteFDFDFFGrayEEF2FF
        )
) {
    LeaderboardHeader()

    Spacer(modifier = Modifier.height(dimensionResource(com.fruktorum.core.R.dimen.size_30_dp)))

    val state = rememberCollapsingToolbarScaffoldState()

    CollapsingToolbarScaffold(
        modifier = Modifier.fillMaxSize(),
        state = state,
        scrollStrategy = ScrollStrategy.ExitUntilCollapsed,
        toolbarModifier = Modifier
            .fillMaxWidth()
            .padding(
                top = (state.toolbarState.progress * 6).dp
            ),
        toolbar = {
            TopThreeUsers(
                state = state.toolbarState.progress,
                firstUser = content.users.find { it.rank == 1 } ?: throw IllegalStateException(),
                secondUser = content.users.find { it.rank == 2 },
                thirdUser = content.users.find { it.rank == 3 }
            )
        }
    ) {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
        ) {
            stickyHeader {
                CurrentLeaderboardUser(
                    modifier = Modifier
                        .background(WhiteEEF2FF)
                        .padding(dimensionResource(com.fruktorum.core.R.dimen.size_16_dp)),
                    user = content.currentUser,
                    onClickLetsPlay = onClickLetsPlay
                )
            }
            itemsIndexed(
                content.users.filter {
                    it.rank != null && it.rank >= 4
                }.sortedBy { it.rank }
            ) { index, user ->
                LeaderboardUser(
                    user = user,
                    modifier = Modifier
                        .background(
                            if (user.rank != null && user.rank <= 20) GrayD8E1FF
                            else Color.Transparent
                        )
                        .padding(
                            horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_16_dp),
                            vertical = dimensionResource(com.fruktorum.core.R.dimen.size_8_dp)
                        )
                )
            }
        }
    }
}