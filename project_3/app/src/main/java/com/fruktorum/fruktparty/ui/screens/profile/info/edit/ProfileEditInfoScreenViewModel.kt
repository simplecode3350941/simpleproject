package com.fruktorum.fruktparty.ui.screens.profile.info.edit

import com.fruktorum.core.ui.vm.MVIViewModel
import com.fruktorum.fruktparty.ui.screens.profile.info.edit.model.ProfileEditInfoField
import com.fruktorum.fruktparty.ui.screens.profile.info.edit.model.ProfileEditInfoScreenAction
import com.fruktorum.fruktparty.ui.screens.profile.info.edit.model.ProfileEditInfoScreenEvent
import com.fruktorum.fruktparty.ui.screens.profile.info.edit.model.ProfileEditInfoScreenState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
internal class ProfileEditInfoScreenViewModel
@Inject
constructor(

) : MVIViewModel<
        ProfileEditInfoScreenState,
        Nothing,
        ProfileEditInfoScreenEvent,
        ProfileEditInfoScreenAction>() {

    override fun onEvent(event: ProfileEditInfoScreenEvent) {
        when (event) {
            is ProfileEditInfoScreenEvent.Start -> handleStartEvent(
                avatarUrl = event.avatarUrl,
                name = event.name,
                surname = event.surname,
                nickname = event.nickname,
                birthdate = event.birthdate
            )
            is ProfileEditInfoScreenEvent.FieldValueChange -> handleFieldValueChange(
                event.field
            )
            ProfileEditInfoScreenEvent.ClickOnEditPhoto -> handleClickOnEditPhoto()
            ProfileEditInfoScreenEvent.ClickOnConfirm -> handleClickOnConfirm()
        }
    }

    private fun handleStartEvent(
        avatarUrl: String?,
        name: String,
        surname: String,
        nickname: String,
        birthdate: String,
    ) {
        setState(
            ProfileEditInfoScreenState.Content(
                avatarUrl = avatarUrl,
                name = name,
                surname = surname,
                nickname = nickname,
                birthdate = birthdate
            )
        )
    }

    private fun handleFieldValueChange(
        field: ProfileEditInfoField
    ) {
        val state = getStateValueOrNull()
        if (state !is ProfileEditInfoScreenState.Content) return

        setState(
            with(state) {
                when (field) {
                    is ProfileEditInfoField.AvatarUrl -> {
                        copy(avatarUrl = field.value)
                    }
                    is ProfileEditInfoField.Name -> {
                        copy(name = field.value)
                    }
                    is ProfileEditInfoField.Surname -> {
                        copy(surname = field.value)
                    }
                    is ProfileEditInfoField.Nickname -> {
                        copy(nickname = field.value)
                    }
                    is ProfileEditInfoField.Birthdate -> {
                        copy(birthdate = field.value)
                    }
                }
            }
        )
    }

    private fun handleClickOnEditPhoto() {
        // TODO
    }

    private fun handleClickOnConfirm() {
        // TODO
    }
}
