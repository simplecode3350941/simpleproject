package com.fruktorum.fruktparty.ui.navigation

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.height
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithCache
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.fruktorum.core.sharedPreference.ConfigModel
import com.fruktorum.core.ui.theme.BlueC1D0FF
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.fruktparty.quiz_game.root.ui.navigation.QuizRoutes
import com.google.accompanist.systemuicontroller.rememberSystemUiController

@Composable
@Preview
fun BottomNavigation(navController: NavController = rememberNavController()) {
    val items = if (ConfigModel.mode == 0) {
        BottomTabs.values()
    } else {
        arrayOf(BottomTabs.GAMES, BottomTabs.PROFILE)
    }

    // val items = BottomTabs.values()
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val systemUiController = rememberSystemUiController()
    val currentRoute = navBackStackEntry?.destination?.route
    val isShowBottomBar = remember { mutableStateOf(false) }
    val isLightStatusBar = remember { mutableStateOf(true) }

    when (currentRoute) {
        TUTORIAL_SCREEN_ROUTE -> {
            isShowBottomBar.value = false
            isLightStatusBar.value = false
        }
        WELCOME_SCREEN_ROUTE,
        LOGIN_SCREEN,
        SIGN_UP_SCREEN,
        RESET_PASSWORD_SCREEN,
        RESET_PASSWORD_SUCCESS_SCREEN,
        ABOUT_US_SCREEN,
        PRIZES_SCREEN,
        PROFILE_EDIT_INFO_SCREEN,
        PROFILE_INFO_SCREEN,
        CHANGE_PASSWORD_SCREEN,
        QuizRoutes.root,
        QuizRoutes.start,
        QuizRoutes.quiz,
        QuizRoutes.result,
        QuizRoutes.answers -> isShowBottomBar.value = false
        else -> {
            isShowBottomBar.value = true
            isLightStatusBar.value = false
        }
    }

    if (currentRoute?.startsWith(QuizRoutes.root) == true) {
        isShowBottomBar.value = false
        isLightStatusBar.value = true
    }

    if (currentRoute?.startsWith(GAME_DETAILS_SCREEN) == true) {
        isShowBottomBar.value = false
        isLightStatusBar.value = false
    }

    systemUiController.systemBarsDarkContentEnabled = !isLightStatusBar.value

    if (isShowBottomBar.value) {
        BottomNavigation(
            modifier = Modifier
                .background(brush = WhiteFDFDFFGrayEEF2FF)
                .height(dimensionResource(id = com.fruktorum.core.R.dimen.size_60_dp)),
            elevation = dimensionResource(id = com.fruktorum.core.R.dimen.size_0_dp),
            backgroundColor = Color.Transparent
        ) {

            items.forEach { item ->
                BottomNavigationItem(
                    icon = {
                        Icon(painterResource(id = item.icon),
                            stringResource(id = item.title),
                            modifier = Modifier
                                .graphicsLayer(alpha = 0.99f)
                                .drawWithCache {
                                    onDrawWithContent {
                                        drawContent()
                                        if (item.innerRoutes.contains(currentRoute))
                                            drawRect(
                                                RedFF512FPinkDD2476,
                                                blendMode = BlendMode.SrcAtop
                                            )
                                        else
                                            drawRect(BlueC1D0FF, blendMode = BlendMode.SrcAtop)
                                    }
                                }
                        )
                    },
                    alwaysShowLabel = false,
                    selected = item.innerRoutes.contains(currentRoute),
                    onClick = {
                        navController.navigate(item.route) {
                            navController.graph.startDestinationRoute?.let { screen_route ->
                                popUpTo(screen_route) {
                                    saveState = true
                                }
                            }
                            launchSingleTop = true
                            restoreState = true
                        }
                    }
                )
            }
        }
    }
}