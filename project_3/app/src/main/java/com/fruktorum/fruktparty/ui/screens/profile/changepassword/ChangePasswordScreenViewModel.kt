package com.fruktorum.fruktparty.ui.screens.profile.changepassword

import com.fruktorum.core.ui.vm.MVIViewModel
import com.fruktorum.fruktparty.ui.screens.profile.changepassword.model.ChangePasswordField
import com.fruktorum.fruktparty.ui.screens.profile.changepassword.model.ChangePasswordScreenAction
import com.fruktorum.fruktparty.ui.screens.profile.changepassword.model.ChangePasswordScreenEvent
import com.fruktorum.fruktparty.ui.screens.profile.changepassword.model.ChangePasswordScreenState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
internal class ChangePasswordScreenViewModel
@Inject
constructor(

) : MVIViewModel<ChangePasswordScreenState, Nothing, ChangePasswordScreenEvent, ChangePasswordScreenAction>() {

    init {
        setState(ChangePasswordScreenState.EnteringValues())
    }

    override fun onEvent(event: ChangePasswordScreenEvent) {
        when (event) {
            is ChangePasswordScreenEvent.ChangeFieldValue ->
                handleChangeFieldValue(event.field)
            is ChangePasswordScreenEvent.ChangeFieldVisibility ->
                handleChangeFieldVisibility(event.field)
            ChangePasswordScreenEvent.ChangePasswordClick ->
                handleChangePasswordClick()
        }
    }

    private fun handleChangeFieldValue(
        field: ChangePasswordField
    ) {
        val state = getStateValueOrNull()
        if (state !is ChangePasswordScreenState.EnteringValues) return

        setState(
            with(state) {
                when (field) {
                    is ChangePasswordField.OldPassword -> {
                        copy(oldPassword = field.value)
                    }
                    is ChangePasswordField.NewPassword -> {
                        copy(newPassword = field.value)
                    }
                    is ChangePasswordField.ConfirmNewPassword -> {
                        copy(confirmNewPassword = field.value)
                    }
                }
            }
        )
    }

    private fun handleChangeFieldVisibility(
        field: ChangePasswordField
    ) {
        // TODO
    }

    private fun handleChangePasswordClick() {
        // TODO
    }
}
