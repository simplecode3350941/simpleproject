package com.fruktorum.fruktparty.ui.screens.games.details

import com.fruktorum.core.ui.vm.MVIViewModel
import com.fruktorum.domain.base.onLoading
import com.fruktorum.domain.base.onSuccess
import com.fruktorum.domain.entity.Game
import com.fruktorum.domain.game.common.usecase.GetGameStatusUseCase
import com.fruktorum.fruktparty.quiz_game.root.ui.navigation.getStartQuizRoute
import com.fruktorum.fruktparty.ui.screens.games.details.model.GameDetailsScreenAction
import com.fruktorum.fruktparty.ui.screens.games.details.model.GameDetailsScreenEvent
import com.fruktorum.fruktparty.ui.screens.games.details.model.GameDetailsScreenState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
internal class GameDetailsScreenViewModel
@Inject
constructor(
    private val getGameStatus: GetGameStatusUseCase
) : MVIViewModel<
        GameDetailsScreenState,
        Nothing,
        GameDetailsScreenEvent,
        GameDetailsScreenAction>() {

    override fun onEvent(event: GameDetailsScreenEvent) {
        when (event) {
            is GameDetailsScreenEvent.Start ->
                handleStartEvent(
                    id = event.id,
                    title = event.title,
                    description = event.description,
                    imageUrl = event.imageUrl
                )
            is GameDetailsScreenEvent.ClickOnTryAgainErrorGetGameStatus ->
                handleStartEvent(
                    id = event.id,
                    title = event.title,
                    description = event.description,
                    imageUrl = event.imageUrl
                )
        }
    }

    private fun handleStartEvent(
        id: String,
        title: String,
        description: String,
        imageUrl: String
    ) = launchJob(
        exceptionBlock = {
            setState(
                GameDetailsScreenState.ErrorGetGameStatus(
                    error = it.localizedMessage,
                    id = id,
                    title = title,
                    description = description,
                    imageUrl = imageUrl
                )
            )
        }
    ) {
        getGameStatus(id).collect {
            with(it) {
                onLoading {
                    setState(GameDetailsScreenState.Loading)
                }
                onSuccess {
                    setState(
                        GameDetailsScreenState.Content(
                            game = Game(
                                id = this?.id ?: throw Exception(),
                                title = title,
                                description = description,
                                imageUrl = imageUrl,
                                userScore = this.userScore
                            ),
                            route = getGameRoute(
                                id = this.id,
                                level = this.level,
                                userScore = this.userScore
                            )
                        )
                    )
                }
            }
        }
    }

    @Throws(IllegalStateException::class)
    private fun getGameRoute(
        id: String,
        level: Int?,
        userScore: Int?
    ): String {
        return when (id) {
            //TODO
            "1" -> getStartQuizRoute(
                id = id,
                round = level,
                userScore = userScore
            )
            else -> throw IllegalStateException()
        }
    }
}