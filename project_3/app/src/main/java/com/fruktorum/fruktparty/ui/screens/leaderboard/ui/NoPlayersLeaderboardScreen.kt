package com.fruktorum.fruktparty.ui.screens.leaderboard.ui

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import com.fruktorum.core.ui.composables.screen.StatusScreen
import com.fruktorum.core.ui.composables.screen.StatusScreenContent
import com.fruktorum.core.ui.theme.Green2ACEC4Green2ABACE
import com.fruktorum.fruktparty.R

@Composable
internal fun NoPlayersLeaderboardScreen(
    modifier: Modifier = Modifier,
    onClickLetsStartToPlay: () -> Unit
) {
    StatusScreen(
        modifier = modifier,
        mainContent = {
            LeaderboardHeader()

            Spacer(modifier = Modifier.height(dimensionResource(com.fruktorum.core.R.dimen.size_44_dp)))

            StatusScreenContent(
                imgRes = R.drawable.img_metaverse_2,
                backgroundShapeColor = Green2ACEC4Green2ABACE
            )
        },
        title = stringResource(R.string.leaderboard_screen_no_players_title),
        text = stringResource(R.string.leaderboard_screen_no_players_description),
        buttonText = stringResource(R.string.leaderboard_screen_no_players_btn_text),
        onButtonClick = onClickLetsStartToPlay
    )
}