package com.fruktorum.fruktparty.ui.screens.profile.aboutUs

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import com.fruktorum.core.ext.composeEmail
import com.fruktorum.core.ui.composables.topBar.FruktPartyTopBar
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.fruktparty.R

/**
 * About Us Screen
 */
@Composable
internal fun AboutUsScreen(
    navController: NavHostController,
    modifier: Modifier = Modifier
) {
    val context = LocalContext.current

    AboutUsScreen(
        modifier = modifier,
        onClickBack = {
            navController.popBackStack()
        },
        onClickContactUs = {
            context.composeEmail(
                arrayOf(context.getString(R.string.about_us_screen_mail))
            )
        }
    )
}

@OptIn(ExperimentalTextApi::class)
@Composable
private fun AboutUsScreen(
    modifier: Modifier,
    onClickBack: () -> Unit,
    onClickContactUs: () -> Unit
) = Column(
    modifier = modifier
        .fillMaxSize()
        //TODO delete this, get from theme
        .background(
            brush = WhiteFDFDFFGrayEEF2FF
        )
) {
    FruktPartyTopBar(
        title = stringResource(R.string.about_us_screen_title),
        modifier = Modifier.padding(top = dimensionResource(com.fruktorum.core.R.dimen.size_8_dp)),
        onBackClick = onClickBack
    )

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .weight(1f)
            .verticalScroll(rememberScrollState())
            .padding(
                horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_16_dp)
            ),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_24_dp)))

        Text(
            text = stringResource(R.string.about_us_screen_fruktorum_text),
            style = MaterialTheme.typography.h2.copy(
                brush = RedFF512FPinkDD2476
            ),
            textAlign = TextAlign.Center
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_16_dp)))

        Text(
            text = stringResource(R.string.about_us_screen_description_1),
            style = MaterialTheme.typography.body1,
            textAlign = TextAlign.Center,
            color = Black000B2D
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_12_dp)))

        Text(
            text = stringResource(R.string.about_us_screen_description_2),
            style = MaterialTheme.typography.body1,
            textAlign = TextAlign.Center,
            color = Black000B2D
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_24_dp)))

        Text(
            text = stringResource(R.string.about_us_screen_completed_projects_count_text),
            style = MaterialTheme.typography.h2,
            textAlign = TextAlign.Center,
            color = Black000B2D
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_8_dp)))

        Text(
            text = stringResource(R.string.about_us_screen_completed_projects_text),
            style = MaterialTheme.typography.body1,
            textAlign = TextAlign.Center,
            color = Black000B2D
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_24_dp)))

        Text(
            text = stringResource(R.string.about_us_screen_countries_of_our_partners_count_text),
            style = MaterialTheme.typography.h2,
            textAlign = TextAlign.Center,
            color = Black000B2D
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_8_dp)))

        Text(
            text = stringResource(R.string.about_us_screen_countries_of_our_partners_text),
            style = MaterialTheme.typography.body1,
            textAlign = TextAlign.Center,
            color = Black000B2D
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_24_dp)))

        Text(
            text = stringResource(R.string.about_us_screen_year_of_company_foundation_count_text),
            style = MaterialTheme.typography.h2,
            textAlign = TextAlign.Center,
            color = Black000B2D
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_8_dp)))

        Text(
            text = stringResource(R.string.about_us_screen_year_of_company_foundation_text),
            style = MaterialTheme.typography.body1,
            textAlign = TextAlign.Center,
            color = Black000B2D
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_48_dp)))

        Text(
            text = stringResource(R.string.about_us_screen_contact_us_text),
            style = MaterialTheme.typography.body1,
            textAlign = TextAlign.Center,
            color = Black000B2D
        )

        Text(
            text = stringResource(R.string.about_us_screen_mail),
            style = MaterialTheme.typography.h3.copy(
                brush = RedFF512FPinkDD2476
            ),
            textAlign = TextAlign.Center,
            modifier = Modifier
                .clickable(onClick = onClickContactUs)
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_24_dp)))
    }
}

@Preview(
    showBackground = true
)
@Composable
private fun AboutUsScreenPreview() {
    FruktPartyTheme {
        AboutUsScreen(
            modifier = Modifier,
            onClickBack = {},
            onClickContactUs = {}
        )
    }
}
