package com.fruktorum.fruktparty.ui.screens.leaderboard.model

internal sealed class LeaderboardScreenAction {

    class NavigateTo(
        val route: String
    ) : LeaderboardScreenAction()
}
