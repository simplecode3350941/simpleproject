package com.fruktorum.fruktparty.ui.screens.profile.model

internal sealed class ProfileScreenEvent {

    object Start : ProfileScreenEvent()

    object ClickOnSeePrizes : ProfileScreenEvent()

    object ClickOnSeeProfile : ProfileScreenEvent()

    object ClickOnChangePassword : ProfileScreenEvent()

    object ClickOnAboutUs : ProfileScreenEvent()

    object ClickOnLogOut : ProfileScreenEvent()

    object ClickOnDeleteAccount : ProfileScreenEvent()

    object CloseDialog : ProfileScreenEvent()

    object LogoutDialog: ProfileScreenEvent()
}
