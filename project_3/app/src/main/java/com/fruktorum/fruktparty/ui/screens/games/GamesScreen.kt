package com.fruktorum.fruktparty.ui.screens.games

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.asPaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBars
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.navigation.NavHostController
import com.fruktorum.core.ext.toDp
import com.fruktorum.core.ui.composables.grid.StaggeredVerticalGrid
import com.fruktorum.core.ui.composables.screen.LoadingScreen
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.BlueC1D0FF
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.domain.entity.Game
import com.fruktorum.fruktparty.R
import com.fruktorum.fruktparty.ui.screens.error.ErrorScreen
import com.fruktorum.fruktparty.ui.screens.games.dialog.EventInfoDialog
import com.fruktorum.fruktparty.ui.screens.games.dialog.NoEventInfoDialog
import com.fruktorum.fruktparty.ui.screens.games.model.GamesScreenAction
import com.fruktorum.fruktparty.ui.screens.games.model.GamesScreenDialogState
import com.fruktorum.fruktparty.ui.screens.games.model.GamesScreenEvent
import com.fruktorum.fruktparty.ui.screens.games.model.GamesScreenState
import com.fruktorum.fruktparty.ui.screens.games.model.Mode
import com.fruktorum.fruktparty.ui.screens.games.ui.GameItem

@Composable
internal fun GamesScreen(
    navController: NavHostController,
    viewModel: GamesScreenViewModel,
    modifier: Modifier = Modifier
) {
    val state = viewModel.state.collectAsState(
        initial = GamesScreenState.Loading
    ).value

    val dialogState = viewModel.dialogState.collectAsState(
        initial = null
    ).value

    when (state) {
        GamesScreenState.Loading -> {
            LoadingScreen(modifier)
        }
        is GamesScreenState.Content -> {

            GamesScreen(
                state = state,
                onClickInfo = {
                    viewModel.onEvent(GamesScreenEvent.ClickOnInfo(state.mode))
                },
                onClickGame = {
                    viewModel.onEvent(GamesScreenEvent.ClickOnGame(it))
                }
            )
        }
        is GamesScreenState.Error -> {
            ErrorScreen(
                modifier = modifier,
                text = state.message,
                onClickTryAgain = {
                    viewModel.onEvent(
                        GamesScreenEvent.Start
                    )
                }
            )
        }
    }

    when (dialogState) {
        is GamesScreenDialogState.EventInfoDialog -> {
            EventInfoDialog(
                event = dialogState.event,
                onClose = {
                    viewModel.onEvent(GamesScreenEvent.CloseDialog)
                }
            )
        }
        GamesScreenDialogState.NoEventInfoDialog -> {
            NoEventInfoDialog(
                onClose = {
                    viewModel.onEvent(GamesScreenEvent.CloseDialog)
                }
            )
        }
        null -> Unit
    }

    LaunchedEffect(true) {
        viewModel.onEvent(GamesScreenEvent.Start)

        viewModel.action.collect { action ->
            when (action) {
                is GamesScreenAction.NavigateTo -> {
                    navController.navigate(
                        route = action.route
                    )
                }
            }
        }
    }
}

@Composable
private fun GamesScreen(
    state: GamesScreenState.Content,
    onClickInfo: () -> Unit,
    onClickGame: (Game) -> Unit
) {
    ConstraintLayout(
        modifier = Modifier
            .background(WhiteFDFDFFGrayEEF2FF)
            .fillMaxSize()
    ) {
        val (title_tv, info, games) = createRefs()

        val statusBarHeight = WindowInsets.statusBars.asPaddingValues().calculateTopPadding()
        val columnHeightDiff: MutableState<Int> = remember { mutableStateOf(0) }

        val dimen0 = dimensionResource(id = com.fruktorum.core.R.dimen.size_0_dp)
        val dimen8 = dimensionResource(id = com.fruktorum.core.R.dimen.size_8_dp)
        val dimen16 = dimensionResource(id = com.fruktorum.core.R.dimen.size_16_dp)
        val dimen19 = dimensionResource(id = com.fruktorum.core.R.dimen.size_19_dp)
        val dimen24 = dimensionResource(id = com.fruktorum.core.R.dimen.size_24_dp)

        Text(
            text = when (val mode = state.mode) {
                is Mode.Base -> mode.modeTitle//stringResource(id = R.string.games_screen_title)
                is Mode.Event -> mode.value.title
            },
            style = MaterialTheme.typography.body1,
            color = Black000B2D,
            modifier = Modifier.constrainAs(title_tv) {
                top.linkTo(parent.top, margin = dimen19 + statusBarHeight)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            }
        )

        IconButton(
            modifier = Modifier.constrainAs(info) {
                top.linkTo(title_tv.top)
                bottom.linkTo(title_tv.bottom)
                end.linkTo(parent.end)
            },
            onClick = onClickInfo
        ) {
            Image(
                imageVector = ImageVector.vectorResource(id = R.drawable.ic_info),
                contentDescription = stringResource(
                    id = R.string.games_screen_info_button_content_description
                )
            )
        }

        Box(
            modifier = Modifier.constrainAs(games) {
                height = Dimension.fillToConstraints
                width = Dimension.fillToConstraints
                top.linkTo(title_tv.bottom, margin = dimen8)
                bottom.linkTo(parent.bottom)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            }
        ) {
            StaggeredVerticalGrid(
                columnsHeightDiff = columnHeightDiff,
                lastFixedItemIndex = state.games.lastIndex,
                clipToPadding = dimen16,
                modifier = Modifier
                    .background(color = Color.Transparent)
                    .padding(top = dimen16, start = dimen8, end = dimen8)
                    .verticalScroll(rememberScrollState())
            ) {
                state.games.forEachIndexed { index, game ->
                    val topPadding = if (index in 0..1) dimen0 else dimen24
                    GameItem(
                        game = game,
                        topPadding = topPadding,
                        onClick = {
                            onClickGame(game)
                        }
                    )
                }

                if (state.games.size % 2 != 0) {
                    Column(
                        verticalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier
                            .height(columnHeightDiff.value.toDp)
                            .fillMaxWidth()
                            .padding(top = dimen24, start = dimen8, end = dimen8)
                    ) {
                        Box(
                            contentAlignment = Center,
                            modifier = Modifier
                                .fillMaxWidth()
                                .weight(1f, true)
                                .clip(RoundedCornerShape(dimen16))
                                .background(BlueC1D0FF)
                        ) {
                            Image(
                                imageVector = ImageVector.vectorResource(
                                    id = R.drawable.ic_coming_soon
                                ),
                                contentDescription = null,
                                contentScale = ContentScale.Inside,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .clip(RoundedCornerShape(dimen16))
                            )
                        }
                        Text(
                            text = state.games.last().title,
                            style = MaterialTheme.typography.body1,
                            color = Color.Transparent,
                            textAlign = TextAlign.Start,
                            maxLines = 2,
                            overflow = TextOverflow.Ellipsis,
                            modifier = Modifier
                                .fillMaxWidth()
                                .wrapContentHeight()
                                .padding(top = dimen8)
                        )
                    }
                }
            }
        }
    }
}