package com.fruktorum.fruktparty.ui.screens.profile.info.edit.model

internal sealed class ProfileEditInfoScreenEvent {

    class Start(
        val avatarUrl: String?,
        val name: String,
        val surname: String,
        val nickname: String,
        val birthdate: String
    ) : ProfileEditInfoScreenEvent()

    data class FieldValueChange(
        val field: ProfileEditInfoField
    ) : ProfileEditInfoScreenEvent()

    object ClickOnEditPhoto : ProfileEditInfoScreenEvent()

    object ClickOnConfirm : ProfileEditInfoScreenEvent()
}
