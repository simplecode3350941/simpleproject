package com.fruktorum.fruktparty.ui.screens.tutorial

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.asPaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBars
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import androidx.navigation.NavHostController
import com.fruktorum.core.sharedPreference.ConfigModel
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.Black0C002C
import com.fruktorum.core.ui.theme.GrayD8E1FF
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.domain.entity.Tutorial
import com.fruktorum.fruktparty.R
import com.fruktorum.fruktparty.ui.navigation.TUTORIAL_SCREEN_ROUTE
import com.fruktorum.fruktparty.ui.navigation.WELCOME_SCREEN_ROUTE

@Composable
fun TutorialScreens(navController: NavHostController){
    val context = LocalContext.current
    val tutorials = getPrizes(context)
    ConstraintLayout (modifier = Modifier
        .fillMaxSize()
        .background(WhiteFDFDFFGrayEEF2FF)
    ) {
        val statusBarHeight = WindowInsets.statusBars.asPaddingValues().calculateTopPadding()
        val (skip, image, indicator, title, description, button) = createRefs()
        val slideState = remember { mutableStateOf(0)}
        val dimen45 = dimensionResource(id = com.fruktorum.core.R.dimen.size_45_dp)
        val dimen36 = dimensionResource(id = com.fruktorum.core.R.dimen.size_36_dp)
        val dimen32 = dimensionResource(id = com.fruktorum.core.R.dimen.size_32_dp)
        val dimen25 = dimensionResource(id = com.fruktorum.core.R.dimen.size_25_dp)
        val dimen16 = dimensionResource(id = com.fruktorum.core.R.dimen.size_16_dp)
        val dimen8 = dimensionResource(id = com.fruktorum.core.R.dimen.size_8_dp)

        Text(
            text = stringResource(id = R.string.tutorial_skip),
            color = Black0C002C,
            style = MaterialTheme.typography.body2,
            modifier = Modifier
                .constrainAs(skip) {
                    top.linkTo(parent.top, margin = dimen32 + statusBarHeight)
                    end.linkTo(parent.end, margin = dimen16)
                }
                .clickable {
                    ConfigModel.isTutorialViewed = true
                    navController.navigate(WELCOME_SCREEN_ROUTE) {
                        popUpTo(TUTORIAL_SCREEN_ROUTE) { inclusive = true }
                    }
                }
        )

        Image(
            painter = painterResource(id = tutorials[slideState.value].picture),
            contentDescription = tutorials[slideState.value].description,
            contentScale = ContentScale.FillWidth,
            modifier = Modifier.constrainAs(image){
                top.linkTo(skip.bottom, margin = dimen16)
                start.linkTo(parent.start, margin = dimen25)
                end.linkTo(parent.end, margin = dimen25)
                width = Dimension.fillToConstraints
            },
            alignment = Alignment.Center
        )

        DotsIndicator(
            totalDots = 3,
            selectedIndex = slideState.value,
            selectedColor = RedFF512FPinkDD2476,
            unSelectedColor = GrayD8E1FF,
            modifier = Modifier.constrainAs(indicator) {
                top.linkTo(image.bottom, margin = dimen32)
                start.linkTo(image.start)
                end.linkTo(image.end)
            }
        )

        Text(
            text = tutorials[slideState.value].title,
            color = Black000B2D,
            style = MaterialTheme.typography.h2,
            modifier = Modifier.constrainAs(title) {
                top.linkTo(indicator.bottom, margin = dimen45)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            }
        )

        Text(
            text = tutorials[slideState.value].description,
            color = Black000B2D,
            style = MaterialTheme.typography.body1,
            textAlign = TextAlign.Center,
            modifier = Modifier.constrainAs(description) {
                width = Dimension.fillToConstraints
                top.linkTo(title.bottom, margin = dimen8)
                start.linkTo(parent.start, margin = dimen32)
                end.linkTo(parent.end, margin = dimen32)
            }
        )

        IconButton(
            onClick = {
                if (slideState.value < tutorials.size - 1) {
                    slideState.value = slideState.value + 1
                } else {
                    ConfigModel.isTutorialViewed = true
                    navController.navigate(WELCOME_SCREEN_ROUTE) {
                        popUpTo(TUTORIAL_SCREEN_ROUTE) { inclusive = true }
                    }
                }
            },
            modifier = Modifier.constrainAs(button) {
                top.linkTo(description.bottom, margin = dimen32)
                bottom.linkTo(parent.bottom, margin = dimen36)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            }
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_tutorial_next_btn),
                contentDescription = stringResource(id = R.string.tutorial_next_image_description),
                modifier = Modifier
                    .width(dimensionResource(id = com.fruktorum.core.R.dimen.size_80_dp))
                    .height(dimensionResource(id = com.fruktorum.core.R.dimen.size_52_dp))
            )
        }
    }
}

fun getPrizes(context: Context): List<Tutorial> {
    val tutorials = ArrayList<Tutorial>()
    tutorials.add(Tutorial(
        context.getString(R.string.tutorial_play_title),
        context.getString(R.string.tutorial_play_description),
        R.drawable.tutorial_play)
    )
    tutorials.add(Tutorial(
        context.getString(R.string.tutorial_win_title),
        context.getString(R.string.tutorial_win_description),
        R.drawable.tutorial_win)
    )
    tutorials.add(Tutorial(
        context.getString(R.string.tutorial_prizes_title),
        context.getString(R.string.tutorial_prizes_description),
        R.drawable.tutorial_get_prizes)
    )
    return tutorials
}

@Composable
fun DotsIndicator(
    totalDots : Int,
    selectedIndex : Int,
    selectedColor: Brush,
    unSelectedColor: Color,
    modifier: Modifier = Modifier
){

    LazyRow(
        modifier = modifier
    ) {

        items(totalDots) { index ->
            if (index == selectedIndex) {
                Box(
                    modifier = Modifier
                        .size(dimensionResource(id = com.fruktorum.core.R.dimen.size_8_dp))
                        .clip(CircleShape)
                        .background(selectedColor)
                )
            } else {
                Box(
                    modifier = Modifier
                        .size(dimensionResource(id = com.fruktorum.core.R.dimen.size_8_dp))
                        .clip(CircleShape)
                        .background(unSelectedColor)
                )
            }

            if (index != totalDots - 1) {
                Spacer(
                    modifier = Modifier.padding(
                        horizontal = dimensionResource(id = com.fruktorum.core.R.dimen.size_4_dp)
                    )
                )
            }
        }
    }
}