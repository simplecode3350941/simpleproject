package com.fruktorum.fruktparty.ui.screens.leaderboard.model

internal sealed class LeaderboardScreenEvent {

    object CloseDialog : LeaderboardScreenEvent()

    object ClickOnInfoIcon : LeaderboardScreenEvent()

    object ClickOnGetGift : LeaderboardScreenEvent()

    object ClickOnLetsPlay : LeaderboardScreenEvent()
}
