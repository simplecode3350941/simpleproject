package com.fruktorum.fruktparty.ui.screens.leaderboard

import com.fruktorum.core.ui.vm.MVIViewModel
import com.fruktorum.fruktparty.ui.screens.leaderboard.model.LeaderboardScreenAction
import com.fruktorum.fruktparty.ui.screens.leaderboard.model.LeaderboardScreenDialogState
import com.fruktorum.fruktparty.ui.screens.leaderboard.model.LeaderboardScreenEvent
import com.fruktorum.fruktparty.ui.screens.leaderboard.model.LeaderboardScreenState
import com.fruktorum.fruktparty.ui.screens.leaderboard.model.LeaderboardUser
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
internal class LeaderboardScreenViewModel
@Inject
constructor(

) : MVIViewModel<
        LeaderboardScreenState,
        LeaderboardScreenDialogState,
        LeaderboardScreenEvent,
        LeaderboardScreenAction>() {

    init {
        val user = LeaderboardUser(
            avatarUrl = "https://picsum.photos/400/400",
            nickname = "nickname_nickname_nickname",
            rank = 1,
            starsCount = 1234
        )
        setState(
            LeaderboardScreenState.Content(
                users = listOf(
                    user.copy(rank = 1),
                    user.copy(rank = 2),
                    user.copy(rank = 3),
                    user.copy(rank = 4),
                    user.copy(rank = 5),
                    user.copy(rank = 6),
                    user.copy(rank = 7),
                    user.copy(rank = 8),
                    user.copy(rank = 9),
                    user.copy(rank = 10),
                    user.copy(rank = 11),
                    user.copy(rank = 12),
                    user.copy(rank = 13),
                    user.copy(rank = 14),
                    user.copy(rank = 15),
                    user.copy(rank = 16),
                    user.copy(rank = 17),
                    user.copy(rank = 18),
                    user.copy(rank = 19),
                    user.copy(rank = 20),
                    user.copy(rank = 21),
                    user.copy(rank = 22),
                    user.copy(rank = 23),
                    user.copy(rank = null),
                ),
                currentUser = user.copy(rank = 2)
            )
        )
    }

    override fun onEvent(event: LeaderboardScreenEvent) {
        when (event) {
            LeaderboardScreenEvent.CloseDialog -> setDialogState(LeaderboardScreenDialogState.None)
            LeaderboardScreenEvent.ClickOnInfoIcon -> handleClickOnInfoIcon()
            LeaderboardScreenEvent.ClickOnGetGift -> handleClickOnGetGift()
            LeaderboardScreenEvent.ClickOnLetsPlay -> handleClickOnLetsPlay()
        }
    }

    private fun handleClickOnInfoIcon() {
        //TODO
    }

    private fun handleClickOnGetGift() {
        //TODO
    }

    private fun handleClickOnLetsPlay() {
        setDialogState(LeaderboardScreenDialogState.InfoAboutGifts)
    }
}
