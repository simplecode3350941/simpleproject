package com.fruktorum.fruktparty.ui.screens.signUp.model

internal sealed class SignUpScreenEvent {

    object Start : SignUpScreenEvent()

    data class FieldValueChange(
        val field: SignUpScreenField
    ) : SignUpScreenEvent()

    object ClickOnBack : SignUpScreenEvent()

    object ClickOnContinueWithGmail : SignUpScreenEvent()

    object ClickOnNext : SignUpScreenEvent()

    object ClickOnSignUp : SignUpScreenEvent()
}
