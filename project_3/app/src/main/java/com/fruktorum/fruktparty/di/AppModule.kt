package com.fruktorum.fruktparty.di

import com.fruktorum.core.coroutines.AppDispatchers
import com.fruktorum.core.coroutines.JvmAppDispatchers
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Singleton
    @Provides
    fun provideAppDispatchers(): AppDispatchers {
        return JvmAppDispatchers()
    }
}