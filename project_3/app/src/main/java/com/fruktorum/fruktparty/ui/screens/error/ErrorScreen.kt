package com.fruktorum.fruktparty.ui.screens.error

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.fruktorum.core.ui.composables.screen.StatusScreen
import com.fruktorum.core.ui.composables.screen.StatusScreenContent
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.PurpleA226CEViolet6626CE
import com.fruktorum.fruktparty.R

@Composable
fun ErrorScreen(
    modifier: Modifier = Modifier,
    text: String? = null,
    onClickTryAgain: () -> Unit,
    onClickBack: (() -> Unit)? = null
) {
    text ?: stringResource(R.string.error_screen_text)
    StatusScreen(
        modifier = modifier,
        mainContent = {
            StatusScreenContent(
                imgRes = com.fruktorum.core.R.drawable.img_error_screen_foreground,
                backgroundShapeColor = PurpleA226CEViolet6626CE
            )
        },
        title = stringResource(R.string.error_screen_title),
        text = text ?: stringResource(R.string.error_screen_text),
        buttonText = stringResource(R.string.error_screen_main_button_text),
        onButtonClick = onClickTryAgain,
        onClickBack = onClickBack
    )
}

@Preview(
    showBackground = true
)
@Composable
private fun ErrorScreenPreview() {
    FruktPartyTheme {
        ErrorScreen(
            onClickTryAgain = {}
        )
    }
}
