package com.fruktorum.fruktparty.ui.screens.profile.info.model

internal sealed class ProfileInfoScreenEvent {

    class Start(
        val avatarUrl: String?,
        val name: String,
        val nickname: String,
        val birthdate: String,
        val email: String
    ) : ProfileInfoScreenEvent()

    object ClickOnEditProfile : ProfileInfoScreenEvent()
}
