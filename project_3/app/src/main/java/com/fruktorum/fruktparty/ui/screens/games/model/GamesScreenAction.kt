package com.fruktorum.fruktparty.ui.screens.games.model

internal sealed class GamesScreenAction {
    class NavigateTo(
        val route: String
    ) : GamesScreenAction()
}