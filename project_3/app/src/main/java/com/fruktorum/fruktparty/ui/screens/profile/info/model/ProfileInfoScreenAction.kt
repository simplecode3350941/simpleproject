package com.fruktorum.fruktparty.ui.screens.profile.info.model

internal sealed class ProfileInfoScreenAction {

    class NavigateTo(
        val route: String
    ) : ProfileInfoScreenAction()
}
