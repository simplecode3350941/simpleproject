package com.fruktorum.fruktparty.ui.screens.profile.info

import com.fruktorum.core.ui.vm.MVIViewModel
import com.fruktorum.fruktparty.ui.navigation.PROFILE_EDIT_INFO_SCREEN
import com.fruktorum.fruktparty.ui.screens.profile.info.model.ProfileInfoScreenAction
import com.fruktorum.fruktparty.ui.screens.profile.info.model.ProfileInfoScreenEvent
import com.fruktorum.fruktparty.ui.screens.profile.info.model.ProfileInfoScreenState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
internal class ProfileInfoScreenViewModel
@Inject
constructor(

) : MVIViewModel<
        ProfileInfoScreenState,
        Nothing,
        ProfileInfoScreenEvent,
        ProfileInfoScreenAction>() {

    override fun onEvent(event: ProfileInfoScreenEvent) {
        when (event) {
            is ProfileInfoScreenEvent.Start -> handleStartEvent(
                avatarUrl = event.avatarUrl,
                name = event.name,
                nickname = event.nickname,
                birthdate = event.birthdate,
                email = event.email,
            )
            ProfileInfoScreenEvent.ClickOnEditProfile -> handleClickOnEditProfile()
        }
    }

    private fun handleStartEvent(
        avatarUrl: String?,
        name: String,
        nickname: String,
        birthdate: String,
        email: String
    ) {
        setState(
            ProfileInfoScreenState.Content(
                avatarUrl = avatarUrl,
                name = name,
                nickname = nickname,
                birthdate = birthdate,
                email = email
            )
        )
    }

    private fun handleClickOnEditProfile() {
        emitAction(
            ProfileInfoScreenAction.NavigateTo(
                route = PROFILE_EDIT_INFO_SCREEN
            )
        )
    }
}
