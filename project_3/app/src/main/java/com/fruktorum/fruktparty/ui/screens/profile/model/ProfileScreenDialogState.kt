package com.fruktorum.fruktparty.ui.screens.profile.model

internal sealed class ProfileScreenDialogState {

    object None : ProfileScreenDialogState()

    object LogOut : ProfileScreenDialogState()

    object DeleteAccount : ProfileScreenDialogState()
}
