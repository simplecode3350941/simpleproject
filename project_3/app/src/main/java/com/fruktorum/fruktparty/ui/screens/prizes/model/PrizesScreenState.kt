package com.fruktorum.fruktparty.ui.screens.prizes.model

internal sealed class PrizesScreenState {

    object Loading : PrizesScreenState()

    class Content(
        val prizes: List<Prize>
    ) : PrizesScreenState()

    object Error : PrizesScreenState()
}
