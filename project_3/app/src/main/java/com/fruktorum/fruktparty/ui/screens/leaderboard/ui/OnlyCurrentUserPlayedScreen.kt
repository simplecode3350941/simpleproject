package com.fruktorum.fruktparty.ui.screens.leaderboard.ui

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import com.fruktorum.core.ui.composables.screen.StatusScreen
import com.fruktorum.core.ui.composables.screen.StatusScreenContent
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.Gray9AA1B6
import com.fruktorum.core.ui.theme.YellowFFBE15OrangeFF9315
import com.fruktorum.core.ui.theme.YellowFFC530
import com.fruktorum.fruktparty.R

@Composable
internal fun OnlyCurrentUserPlayedScreen(
    modifier: Modifier = Modifier,
    stars: Int,
    onClickPlayMore: () -> Unit
) {
    StatusScreen(
        modifier = modifier,
        mainContent = {
            LeaderboardHeader()

            Spacer(modifier = Modifier.height(dimensionResource(com.fruktorum.core.R.dimen.size_44_dp)))

            StatusScreenContent(
                imgRes = R.drawable.img_metaverse_3,
                backgroundShapeColor = YellowFFBE15OrangeFF9315
            )
        },
        title = stringResource(R.string.leaderboard_screen_only_current_user_played_title),
        text = {
            Text(
                text = stringResource(R.string.leaderboard_screen_only_current_user_played_description),
                style = MaterialTheme.typography.body1,
                color = Black000B2D,
                textAlign = TextAlign.Center
            )

            Row(
                modifier = Modifier.padding(top = dimensionResource(com.fruktorum.core.R.dimen.size_4_dp)),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_star_16),
                    contentDescription = null,
                    tint = YellowFFC530
                )

                Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_6_dp)))

                Text(
                    text = stars.toString(),
                    color = Gray9AA1B6,
                    style = MaterialTheme.typography.subtitle1
                )
            }
        },
        buttonText = stringResource(R.string.leaderboard_screen_only_current_user_played_btn_text),
        onButtonClick = onClickPlayMore
    )
}