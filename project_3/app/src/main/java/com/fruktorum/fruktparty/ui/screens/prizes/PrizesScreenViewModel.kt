package com.fruktorum.fruktparty.ui.screens.prizes

import com.fruktorum.core.ui.vm.MVIViewModel
import com.fruktorum.fruktparty.ui.screens.prizes.model.Prize.Companion.listOfTestPrizes
import com.fruktorum.fruktparty.ui.screens.prizes.model.PrizesScreenAction
import com.fruktorum.fruktparty.ui.screens.prizes.model.PrizesScreenEvent
import com.fruktorum.fruktparty.ui.screens.prizes.model.PrizesScreenState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
internal class PrizesScreenViewModel
@Inject
constructor(

) : MVIViewModel<PrizesScreenState, Nothing, PrizesScreenEvent, PrizesScreenAction>() {

    init {
        setState(
            PrizesScreenState.Content(
                prizes = listOf(
                    listOfTestPrizes.random(),
                    listOfTestPrizes.random(),
                    listOfTestPrizes.random(),
                    listOfTestPrizes.random(),
                    listOfTestPrizes.random(),
                    listOfTestPrizes.random(),
                    listOfTestPrizes.random(),
                    listOfTestPrizes.random(),
                    listOfTestPrizes.random(),
                    listOfTestPrizes.random()
                )
            )
        )
    }

    override fun onEvent(event: PrizesScreenEvent) {
        when (event) {
            else -> {}
        }
    }
}
