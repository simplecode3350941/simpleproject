package com.fruktorum.fruktparty.ui.screens.resetPassword.model

internal sealed class ResetPasswordScreenAction {

    class NavigateTo(
        val route: String
    ) : ResetPasswordScreenAction()
}
