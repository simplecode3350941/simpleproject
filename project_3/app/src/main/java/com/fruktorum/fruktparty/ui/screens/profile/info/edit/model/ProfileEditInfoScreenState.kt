package com.fruktorum.fruktparty.ui.screens.profile.info.edit.model

internal sealed class ProfileEditInfoScreenState {

    object Loading : ProfileEditInfoScreenState()

    data class Content(
        val avatarUrl: String?,
        val name: String,
        val surname: String,
        val nickname: String,
        val birthdate: String
    ) : ProfileEditInfoScreenState()
}
