package com.fruktorum.fruktparty.ui.screens.profile.info

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import com.fruktorum.core.ui.composables.screen.LoadingScreen
import com.fruktorum.core.ui.composables.textFields.FruktPartyTextField
import com.fruktorum.core.ui.composables.topBar.FruktPartyTopBar
import com.fruktorum.core.ui.theme.BlueC1D0FF
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.core.ui.theme.WhiteFFFFFF
import com.fruktorum.fruktparty.R
import com.fruktorum.fruktparty.ui.screens.profile.info.model.ProfileInfoScreenAction
import com.fruktorum.fruktparty.ui.screens.profile.info.model.ProfileInfoScreenEvent
import com.fruktorum.fruktparty.ui.screens.profile.info.model.ProfileInfoScreenState

/**
 * Profile Info Screen
 */
@Composable
internal fun ProfileInfoScreen(
    navController: NavHostController,
    viewModel: ProfileInfoScreenViewModel,
    avatarUrl: String?,
    name: String,
    nickname: String,
    birthdate: String,
    email: String,
    modifier: Modifier = Modifier
) {
    val state = viewModel.state.collectAsState(initial = ProfileInfoScreenState.Loading)

    when (val stateValue = state.value) {
        ProfileInfoScreenState.Loading -> {
            LoadingScreen(modifier = modifier)
        }
        is ProfileInfoScreenState.Content -> {
            ProfileInfoScreen(
                content = stateValue,
                modifier = modifier,
                onBackClick = {
                    navController.popBackStack()
                },
                onClickEditProfile = {
                    viewModel.onEvent(
                        ProfileInfoScreenEvent.ClickOnEditProfile
                    )
                }
            )
        }
    }

    LaunchedEffect(true) {
        viewModel.onEvent(
            ProfileInfoScreenEvent.Start(
                avatarUrl = avatarUrl,
                name = name,
                nickname = nickname,
                birthdate = birthdate,
                email = email
            )
        )

        viewModel.action.collect { action ->
            when (action) {
                is ProfileInfoScreenAction.NavigateTo -> {
                    navController.navigate(
                        route = action.route
                    )
                }
            }
        }
    }
}

@Composable
private fun ProfileInfoScreen(
    content: ProfileInfoScreenState.Content,
    modifier: Modifier,
    onBackClick: () -> Unit,
    onClickEditProfile: () -> Unit
) = Column(
    modifier = modifier
        .fillMaxSize()
        // TODO delete this, get from theme
        .background(
            brush = WhiteFDFDFFGrayEEF2FF
        )
) {
    FruktPartyTopBar(
        title = stringResource(R.string.profile_info_screen_title),
        modifier = Modifier.padding(top = dimensionResource(com.fruktorum.core.R.dimen.size_8_dp))
    ) {
        onBackClick()
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .weight(1f)
            .verticalScroll(rememberScrollState())
            .padding(
                horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_43_dp)
            )
    ) {
        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_19_dp)))

        Image(
            // TODO content.avatarUrl or if(content.avatarUrl == null) avatar_default
            painter = painterResource(id = R.drawable.ic_avatar_default),
            contentDescription = null,
            modifier = Modifier
                .size(dimensionResource(id = com.fruktorum.core.R.dimen.size_145_dp))
                .clip(CircleShape)
                .background(BlueC1D0FF)
                .border(
                    width = dimensionResource(id = com.fruktorum.core.R.dimen.size_5_dp),
                    brush = RedFF512FPinkDD2476,
                    shape = CircleShape
                )
                .align(Alignment.CenterHorizontally)
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_35_dp)))

        FruktPartyTextField(
            value = content.name,
            modifier = Modifier
                .fillMaxWidth(),
            readOnly = true,
            label = stringResource(R.string.profile_info_screen_name_text_field_label)
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)))

        FruktPartyTextField(
            value = content.nickname,
            modifier = Modifier
                .fillMaxWidth(),
            readOnly = true,
            label = stringResource(R.string.profile_info_screen_nickname_text_field_label)
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)))

        FruktPartyTextField(
            value = content.birthdate,
            modifier = Modifier
                .fillMaxWidth(),
            readOnly = true,
            label = stringResource(R.string.profile_info_screen_birthday_text_field_label)
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)))

        FruktPartyTextField(
            value = content.email,
            modifier = Modifier
                .fillMaxWidth(),
            readOnly = true,
            label = stringResource(R.string.profile_info_screen_email_text_field_label)
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_35_dp)))
    }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_43_dp)
            )
            .padding(bottom = dimensionResource(com.fruktorum.core.R.dimen.size_40_dp))
            .background(
                brush = RedFF512FPinkDD2476,
                shape = RoundedCornerShape(
                    topStart = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp),
                    bottomEnd = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)
                )
            )
            .clip(
                shape = RoundedCornerShape(
                    topStart = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp),
                    bottomEnd = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)
                )
            )
            .clickable(onClick = onClickEditProfile),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = stringResource(R.string.profile_info_screen_edit_profile_button_text),
            modifier = Modifier
                .padding(vertical = dimensionResource(com.fruktorum.core.R.dimen.size_15_dp)),
            color = WhiteFFFFFF,
            style = MaterialTheme.typography.button
        )
    }
}

@Preview(
    showBackground = true
)
@Composable
private fun ProfileInfoScreenPreview() {
    FruktPartyTheme {
        ProfileInfoScreen(
            content = ProfileInfoScreenState.Content(
                avatarUrl = null,
                name = "name surname",
                nickname = "nickname",
                birthdate = "25.01.2022",
                email = "email@email.com"
            ),
            modifier = Modifier
                .background(
                    brush = WhiteFDFDFFGrayEEF2FF
                ),
            onBackClick = {},
            onClickEditProfile = {}
        )
    }
}
