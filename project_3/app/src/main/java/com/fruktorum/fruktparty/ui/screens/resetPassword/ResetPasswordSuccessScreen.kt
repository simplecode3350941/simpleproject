package com.fruktorum.fruktparty.ui.screens.resetPassword

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.Black0C002C
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.PurpleA226CEViolet6626CE
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.fruktparty.R
import com.fruktorum.fruktparty.ui.navigation.LOGIN_SCREEN

@Composable
internal fun ResetPasswordSuccessScreen(
    navController: NavHostController,
    modifier: Modifier = Modifier
) = Column(
    modifier = modifier
        .fillMaxSize()
        //TODO delete this, get from theme
        .background(
            brush = WhiteFDFDFFGrayEEF2FF
        )
        .systemBarsPadding(),
    horizontalAlignment = Alignment.CenterHorizontally
) {
    BackHandler {
        popToLogInScreen(navController)
    }

    Box(
        modifier = Modifier
            .weight(1f)
            .padding(
                top = dimensionResource(com.fruktorum.core.R.dimen.size_64_dp)
            ),
        contentAlignment = Alignment.Center
    ) {
        Box(
            modifier = Modifier
                .rotate(-15f)
                .fillMaxWidth()
                .aspectRatio(1.36f)
                .padding(
                    horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_24_dp)
                )
                .padding(
                    bottom = dimensionResource(com.fruktorum.core.R.dimen.size_48_dp)
                )
                .background(
                    brush = PurpleA226CEViolet6626CE,
                    shape = RoundedCornerShape(
                        topStart = dimensionResource(com.fruktorum.core.R.dimen.size_100_dp),
                        bottomEnd = dimensionResource(com.fruktorum.core.R.dimen.size_100_dp)
                    )
                )
                .clip(
                    shape = RoundedCornerShape(
                        topStart = dimensionResource(com.fruktorum.core.R.dimen.size_100_dp),
                        bottomEnd = dimensionResource(com.fruktorum.core.R.dimen.size_100_dp)
                    )
                )
        )

        Image(
            painter = painterResource(id = R.drawable.img_mailbox),
            contentDescription = null,
            modifier = Modifier
                .fillMaxWidth()
                .aspectRatio(1f),
            contentScale = ContentScale.FillWidth
        )
    }

    Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_64_dp)))

    Text(
        text = stringResource(R.string.reset_password_success_screen_title),
        style = MaterialTheme.typography.h2,
        color = Black000B2D,
        textAlign = TextAlign.Center,
        modifier = Modifier
            .padding(
                horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_16_dp)
            )
    )

    Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_16_dp)))

    Text(
        text = stringResource(R.string.reset_password_success_screen_subtitle),
        style = MaterialTheme.typography.body1,
        color = Black000B2D,
        textAlign = TextAlign.Center,
        modifier = Modifier
            .padding(
                horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_16_dp)
            )
    )

    Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_64_dp)))

    TextButton(
        shape = RoundedCornerShape(
            topStart = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp),
            bottomEnd = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)
        ),
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_44_dp)
            )
            .padding(bottom = dimensionResource(com.fruktorum.core.R.dimen.size_40_dp))
            .background(
                Black0C002C,
                RoundedCornerShape(
                    topStart = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp),
                    bottomEnd = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)
                )
            ),
        contentPadding = PaddingValues(
            all = dimensionResource(com.fruktorum.core.R.dimen.size_15_dp)
        ),
        onClick = {
            popToLogInScreen(navController)
        }
    ) {
        Text(
            text = stringResource(R.string.reset_password_success_screen_go_to_log_in_button_text),
            style = MaterialTheme.typography.body1,
            color = Color.White
        )
    }
}

private fun popToLogInScreen(
    navController: NavHostController
) {
    val isPopped = navController.popBackStack(LOGIN_SCREEN, inclusive = false)
    if (!isPopped) navController.navigate(LOGIN_SCREEN)
}

@Preview(
    showBackground = true,
    device = Devices.PIXEL
)
@Composable
private fun ResetPasswordSuccessScreenPreview() {
    FruktPartyTheme {
        ResetPasswordSuccessScreen(
            navController = rememberNavController(),
            modifier = Modifier
        )
    }
}
