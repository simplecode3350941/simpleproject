package com.fruktorum.fruktparty.ui.screens.leaderboard.ui

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import com.fruktorum.core.ui.composables.screen.StatusScreen
import com.fruktorum.core.ui.composables.screen.StatusScreenContent
import com.fruktorum.core.ui.theme.YellowFFBE15OrangeFF9315
import com.fruktorum.fruktparty.R

@Composable
internal fun NoEventLeaderboardScreen(
    modifier: Modifier = Modifier,
    onClickPlayMore: () -> Unit
) {
    StatusScreen(
        modifier = modifier,
        mainContent = {
            LeaderboardHeader()

            Spacer(modifier = Modifier.height(dimensionResource(com.fruktorum.core.R.dimen.size_44_dp)))

            StatusScreenContent(
                imgRes = R.drawable.img_metaverse_1,
                backgroundShapeColor = YellowFFBE15OrangeFF9315
            )
        },
        title = stringResource(R.string.leaderboard_screen_no_event_title),
        text = stringResource(R.string.leaderboard_screen_no_event_description),
        buttonText = stringResource(R.string.leaderboard_screen_no_event_btn_text),
        onButtonClick = onClickPlayMore
    )
}