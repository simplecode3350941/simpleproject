package com.fruktorum.fruktparty.ui.screens.games.dialog

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.fruktorum.core.ui.composables.buttons.TopStartBottomEndRoundedButton
import com.fruktorum.core.ui.composables.dialog.BaseDialog
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.Gray9AA1B6
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.domain.entity.Prize
import com.fruktorum.fruktparty.R
import com.fruktorum.fruktparty.ui.screens.games.model.EventUiModel
import com.skydoves.landscapist.glide.GlideImage
import com.fruktorum.core.R as RCore

@Composable
internal fun EventInfoDialog(
    event: EventUiModel,
    onClose: () -> Unit
) {
    BaseDialog(
        onDismissRequest = onClose
    ) {
        Column(
            horizontalAlignment = Alignment.End
        ) {
            Image(
                painter = painterResource(RCore.drawable.ic_close_dialog_24),
                contentDescription = null,
                modifier = Modifier
                    .size(dimensionResource(id = RCore.dimen.size_24_dp))
                    .clickable(onClick = onClose)
            )

            Spacer(modifier = Modifier.size(dimensionResource(id = RCore.dimen.size_16_dp)))

            Column(
                modifier = Modifier
                    .background(
                        brush = WhiteFDFDFFGrayEEF2FF,
                        shape = RoundedCornerShape(dimensionResource(id = RCore.dimen.size_36_dp))
                    )
                    .clip(RoundedCornerShape(dimensionResource(id = RCore.dimen.size_36_dp))),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                GlideImage(
                    imageModel = event.imageUrl,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(
                            all = dimensionResource(id = RCore.dimen.size_16_dp)
                        ),
                    contentScale = ContentScale.FillWidth
                )

                Row(
                    modifier = Modifier
                        .padding(
                            horizontal = dimensionResource(id = RCore.dimen.size_20_dp)
                        )
                ) {
                    Text(
                        text = event.title,
                        style = MaterialTheme.typography.h3
                    )

                    Text(
                        //TODO date format?
                        text = event.date ?: "",
                        style = MaterialTheme.typography.subtitle1,
                        color = Gray9AA1B6,
                        modifier = Modifier.weight(1f)
                    )
                }

                Spacer(modifier = Modifier.height(dimensionResource(id = RCore.dimen.size_10_dp)))

                Text(
                    text = event.description,
                    style = MaterialTheme.typography.body2,
                    modifier = Modifier
                        .padding(
                            horizontal = dimensionResource(id = RCore.dimen.size_20_dp)
                        )
                )

                Spacer(modifier = Modifier.height(dimensionResource(id = RCore.dimen.size_10_dp)))

                if (event.prizes != null) {
                    LazyColumn(
                        modifier = Modifier
                            .padding(
                                horizontal = dimensionResource(id = RCore.dimen.size_18_dp)
                            ),
                        userScrollEnabled = false
                    ) {
                        items(event.prizes) {
                            PrizeItem(
                                prize = it,
                                modifier = Modifier.padding(
                                    vertical = dimensionResource(id = RCore.dimen.size_6_dp)
                                )
                            )
                        }
                    }
                }
                TopStartBottomEndRoundedButton(
                    text = stringResource(id = R.string.event_info_dialog_back_button_text),
                    modifier = Modifier
                        .padding(
                            horizontal = dimensionResource(id = RCore.dimen.size_52_dp)
                        )
                        .padding(
                            top = dimensionResource(id = RCore.dimen.size_18_dp),
                            bottom = dimensionResource(id = RCore.dimen.size_24_dp)
                        ),
                    onClick = onClose
                )
            }
        }
    }
}

@Composable
private fun PrizeItem(
    prize: Prize,
    modifier: Modifier = Modifier
) = Row(modifier) {
    Image(
        painter = painterResource(
            id = when (prize.place) {
                1 -> R.drawable.first_place
                2 -> R.drawable.second_place
                3 -> R.drawable.third_place
                4 -> R.drawable.fourth_place
                5 -> R.drawable.last_place
                else -> throw IllegalStateException("Unknown place ${prize.place}")
            }
        ),
        contentDescription = null
    )

    Column() {
        Text(
            text = stringResource(
                id = when (prize.place) {
                    1 -> R.string.event_info_dialog_first_place_title
                    2 -> R.string.event_info_dialog_second_place_title
                    3 -> R.string.event_info_dialog_third_place_title
                    4 -> R.string.event_info_dialog_fourth_place_title
                    5 -> R.string.event_info_dialog_last_place_title
                    else -> throw IllegalStateException("Unknown place ${prize.place}")
                }
            ),
            style = MaterialTheme.typography.body2,
            color = Gray9AA1B6
        )

        Text(
            text = prize.description,
            style = MaterialTheme.typography.body2
        )
    }
}

@Composable
@Preview(showBackground = true)
private fun EventInfoDialogPreview() {
    FruktPartyTheme {
        EventInfoDialog(
            event = EventUiModel.testObject,
            onClose = {}
        )
    }
}