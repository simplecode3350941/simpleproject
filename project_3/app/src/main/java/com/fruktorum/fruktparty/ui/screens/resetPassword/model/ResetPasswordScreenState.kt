package com.fruktorum.fruktparty.ui.screens.resetPassword.model

internal sealed class ResetPasswordScreenState {

    object Loading : ResetPasswordScreenState()

    data class Error(
        val message: String?,
        val email: String
    ) : ResetPasswordScreenState()

    object Content :ResetPasswordScreenState()

}
