package com.fruktorum.fruktparty.ui.screens.profile.info.edit

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import com.fruktorum.core.ui.composables.screen.LoadingScreen
import com.fruktorum.core.ui.composables.textFields.DatePickerTextField
import com.fruktorum.core.ui.composables.textFields.FruktPartyTextField
import com.fruktorum.core.ui.composables.topBar.FruktPartyTopBar
import com.fruktorum.core.ui.theme.BlueC1D0FF
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.Gray9AA1B6
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.core.ui.theme.WhiteFFFFFF
import com.fruktorum.fruktparty.R
import com.fruktorum.fruktparty.ui.screens.profile.info.edit.model.ProfileEditInfoField
import com.fruktorum.fruktparty.ui.screens.profile.info.edit.model.ProfileEditInfoScreenAction
import com.fruktorum.fruktparty.ui.screens.profile.info.edit.model.ProfileEditInfoScreenEvent
import com.fruktorum.fruktparty.ui.screens.profile.info.edit.model.ProfileEditInfoScreenState

/**
 * Profile Edit Info Screen
 */
@Composable
internal fun ProfileEditInfoScreen(
    navController: NavHostController,
    viewModel: ProfileEditInfoScreenViewModel,
    avatarUrl: String?,
    name: String,
    surname: String,
    nickname: String,
    birthdate: String,
    modifier: Modifier = Modifier
) {
    val state = viewModel.state.collectAsState(initial = ProfileEditInfoScreenState.Loading)

    when (val stateValue = state.value) {
        ProfileEditInfoScreenState.Loading -> {
            LoadingScreen(modifier = modifier)
        }
        is ProfileEditInfoScreenState.Content -> {
            ProfileEditInfoScreen(
                content = stateValue,
                modifier = modifier,
                onBackClick = {
                    navController.popBackStack()
                },
                onClickEditPhoto = {
                    viewModel.onEvent(
                        ProfileEditInfoScreenEvent.ClickOnEditPhoto
                    )
                },
                onValueChange = {
                    viewModel.onEvent(
                        ProfileEditInfoScreenEvent.FieldValueChange(
                            field = it
                        )
                    )
                },
                onClickConfirm = {
                    viewModel.onEvent(
                        ProfileEditInfoScreenEvent.ClickOnConfirm
                    )
                }
            )
        }
    }

    LaunchedEffect(true) {
        viewModel.onEvent(
            ProfileEditInfoScreenEvent.Start(
                avatarUrl = avatarUrl,
                name = name,
                surname = surname,
                nickname = nickname,
                birthdate = birthdate
            )
        )

        viewModel.action.collect { action ->
            when (action) {
                is ProfileEditInfoScreenAction.NavigateTo -> {
                    navController.navigate(
                        route = action.route
                    )
                }
            }
        }
    }
}

@Composable
private fun ProfileEditInfoScreen(
    content: ProfileEditInfoScreenState.Content,
    modifier: Modifier,
    onBackClick: () -> Unit,
    onClickEditPhoto: () -> Unit,
    onValueChange: (ProfileEditInfoField) -> Unit,
    onClickConfirm: () -> Unit
) = Column(
    modifier = modifier
        .fillMaxSize()
        // TODO delete this, get from theme
        .background(
            brush = WhiteFDFDFFGrayEEF2FF
        )
) {
    val context = LocalContext.current
    val focusManager = LocalFocusManager.current

    FruktPartyTopBar(
        title = stringResource(R.string.profile_edit_info_screen_title),
        modifier = Modifier.padding(top = dimensionResource(com.fruktorum.core.R.dimen.size_8_dp))
    ) {
        onBackClick()
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .weight(1f)
            .verticalScroll(rememberScrollState())
            .padding(
                horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_43_dp)
            )
    ) {
        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)))

        Image(
            // TODO content.avatarUrl or if(content.avatarUrl == null) avatar_default
            painter = painterResource(id = R.drawable.ic_avatar_default),
            contentDescription = null,
            modifier = Modifier
                .size(dimensionResource(id = com.fruktorum.core.R.dimen.size_135_dp))
                .clip(CircleShape)
                .background(BlueC1D0FF)
                .align(Alignment.CenterHorizontally)
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_8_dp)))

        Text(
            text = stringResource(R.string.profile_edit_info_screen_edit_photo_button_text),
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .clickable(onClick = onClickEditPhoto),
            color = Gray9AA1B6,
            style = MaterialTheme.typography.body2
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_24_dp)))

        FruktPartyTextField(
            value = content.name,
            onValueChange = {
                onValueChange(
                    ProfileEditInfoField.Name(
                        it
                    )
                )
            },
            modifier = Modifier
                .fillMaxWidth(),
            keyboardOptions = KeyboardOptions.Default.copy(
                capitalization = KeyboardCapitalization.Sentences,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(
                onDone = { focusManager.clearFocus(force = true) }
            ),
            label = stringResource(R.string.profile_edit_info_screen_first_name_text_field_label)
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)))

        FruktPartyTextField(
            value = content.surname,
            onValueChange = {
                onValueChange(
                    ProfileEditInfoField.Surname(
                        it
                    )
                )
            },
            modifier = Modifier
                .fillMaxWidth(),
            keyboardOptions = KeyboardOptions.Default.copy(
                capitalization = KeyboardCapitalization.Sentences,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(
                onDone = { focusManager.clearFocus(force = true) }
            ),
            label = stringResource(R.string.profile_edit_info_screen_second_name_text_field_label)
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)))

        FruktPartyTextField(
            value = content.nickname,
            onValueChange = {
                onValueChange(
                    ProfileEditInfoField.Nickname(
                        it
                    )
                )
            },
            modifier = Modifier
                .fillMaxWidth(),
            keyboardActions = KeyboardActions(
                onDone = { focusManager.clearFocus(force = true) }
            ),
            keyboardOptions = KeyboardOptions.Default.copy(
                imeAction = ImeAction.Done
            ),
            label = stringResource(R.string.profile_edit_info_screen_nickname_text_field_label)
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)))

        DatePickerTextField(
            context = context,
            value = content.birthdate,
            onValueChange = {
                onValueChange(
                    ProfileEditInfoField.Birthdate(
                        it
                    )
                )
            },
            modifier = Modifier
                .fillMaxWidth(),
            label = stringResource(R.string.profile_edit_info_screen_birthday_text_field_label)
        )

        Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_35_dp)))
    }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_43_dp)
            )
            .padding(bottom = dimensionResource(com.fruktorum.core.R.dimen.size_40_dp))
            .background(
                brush = RedFF512FPinkDD2476,
                shape = RoundedCornerShape(
                    topStart = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp),
                    bottomEnd = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)
                )
            )
            .clip(
                shape = RoundedCornerShape(
                    topStart = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp),
                    bottomEnd = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)
                )
            )
            .clickable(onClick = onClickConfirm),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = stringResource(R.string.profile_edit_info_screen_confirm_button_text),
            modifier = Modifier
                .padding(vertical = dimensionResource(com.fruktorum.core.R.dimen.size_15_dp)),
            color = WhiteFFFFFF,
            style = MaterialTheme.typography.button
        )
    }
}

@Preview(
    showBackground = true
)
@Composable
private fun ProfileEditInfoScreenPreview() {
    FruktPartyTheme {
        ProfileEditInfoScreen(
            content = ProfileEditInfoScreenState.Content(
                avatarUrl = null,
                name = "name",
                surname = "surname",
                nickname = "nickname",
                birthdate = "25.01.2022"
            ),
            modifier = Modifier
                .background(
                    brush = WhiteFDFDFFGrayEEF2FF
                ),
            onBackClick = {},
            onClickEditPhoto = {},
            onValueChange = {},
            onClickConfirm = {}
        )
    }
}
