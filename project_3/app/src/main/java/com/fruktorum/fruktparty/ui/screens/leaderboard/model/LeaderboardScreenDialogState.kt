package com.fruktorum.fruktparty.ui.screens.leaderboard.model

internal sealed class LeaderboardScreenDialogState {

    object None : LeaderboardScreenDialogState()

    object InfoAboutGifts : LeaderboardScreenDialogState()

    object Gift : LeaderboardScreenDialogState()
}
