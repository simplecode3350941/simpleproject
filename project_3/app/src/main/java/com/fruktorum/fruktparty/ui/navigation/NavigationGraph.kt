package com.fruktorum.fruktparty.ui.navigation

import android.util.Base64
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.fruktorum.domain.entity.Game
import com.fruktorum.fruktparty.quiz_game.root.ui.navigation.quizGraph
import com.fruktorum.fruktparty.ui.screens.games.GamesScreen
import com.fruktorum.fruktparty.ui.screens.games.GamesScreenViewModel
import com.fruktorum.fruktparty.ui.screens.games.details.GameDetailsScreen
import com.fruktorum.fruktparty.ui.screens.games.details.GameDetailsScreenViewModel
import com.fruktorum.fruktparty.ui.screens.leaderboard.LeaderboardScreen
import com.fruktorum.fruktparty.ui.screens.leaderboard.LeaderboardScreenViewModel
import com.fruktorum.fruktparty.ui.screens.login.LoginScreen
import com.fruktorum.fruktparty.ui.screens.login.LoginScreenViewModel
import com.fruktorum.fruktparty.ui.screens.prizes.PrizesScreen
import com.fruktorum.fruktparty.ui.screens.prizes.PrizesScreenViewModel
import com.fruktorum.fruktparty.ui.screens.profile.ProfileScreen
import com.fruktorum.fruktparty.ui.screens.profile.ProfileScreenViewModel
import com.fruktorum.fruktparty.ui.screens.profile.aboutUs.AboutUsScreen
import com.fruktorum.fruktparty.ui.screens.profile.changepassword.ChangePasswordScreen
import com.fruktorum.fruktparty.ui.screens.profile.changepassword.ChangePasswordScreenViewModel
import com.fruktorum.fruktparty.ui.screens.profile.info.ProfileInfoScreen
import com.fruktorum.fruktparty.ui.screens.profile.info.ProfileInfoScreenViewModel
import com.fruktorum.fruktparty.ui.screens.profile.info.edit.ProfileEditInfoScreen
import com.fruktorum.fruktparty.ui.screens.profile.info.edit.ProfileEditInfoScreenViewModel
import com.fruktorum.fruktparty.ui.screens.resetPassword.ResetPasswordScreen
import com.fruktorum.fruktparty.ui.screens.resetPassword.ResetPasswordScreenViewModel
import com.fruktorum.fruktparty.ui.screens.resetPassword.ResetPasswordSuccessScreen
import com.fruktorum.fruktparty.ui.screens.signUp.SignUpScreen
import com.fruktorum.fruktparty.ui.screens.signUp.SignUpScreenViewModel
import com.fruktorum.fruktparty.ui.screens.tutorial.TutorialScreens
import com.fruktorum.fruktparty.ui.screens.welcome.WelcomeScreen

@Composable
fun NavigationGraph(navController: NavHostController, modifier: Modifier, startDestination: String) {

    NavHost(navController, startDestination = startDestination, modifier = modifier) {

        composable(TUTORIAL_SCREEN_ROUTE) {
            TutorialScreens(navController = navController)
        }

        composable(WELCOME_SCREEN_ROUTE) {
            WelcomeScreen(navController = navController)
        }

        composable(LOGIN_SCREEN) {
            val loginViewModel = hiltViewModel<LoginScreenViewModel>()
            LoginScreen(navController = navController, viewModel = loginViewModel)
        }

        composable(RESET_PASSWORD_SCREEN) {
            val viewModel = hiltViewModel<ResetPasswordScreenViewModel>()
            ResetPasswordScreen(navController = navController, viewModel = viewModel)
        }

        composable(RESET_PASSWORD_SUCCESS_SCREEN) {
            ResetPasswordSuccessScreen(navController = navController)
        }

        composable(SIGN_UP_SCREEN) {
            val viewModel = hiltViewModel<SignUpScreenViewModel>()
            SignUpScreen(navController = navController, viewModel = viewModel)
        }

        composable(BottomTabs.LEADERBOARD.route) {
            val viewModel = hiltViewModel<LeaderboardScreenViewModel>()
            LeaderboardScreen(
                navController = navController,
                viewModel = viewModel
            )
        }

        composable(BottomTabs.GAMES.route) {
            val gamesViewModel = hiltViewModel<GamesScreenViewModel>()
            GamesScreen(viewModel = gamesViewModel, navController = navController)
        }

        composable(BottomTabs.PROFILE.route) {
            val viewModel = hiltViewModel<ProfileScreenViewModel>()
            ProfileScreen(
                navController = navController,
                viewModel = viewModel
            )
        }

        composable(CHANGE_PASSWORD_SCREEN) {
            val viewModel = hiltViewModel<ChangePasswordScreenViewModel>()
            ChangePasswordScreen(
                navController = navController,
                viewModel = viewModel
            )
        }

        composable(PROFILE_INFO_SCREEN) {
            val viewModel = hiltViewModel<ProfileInfoScreenViewModel>()
            ProfileInfoScreen(
                navController = navController,
                viewModel = viewModel,
                avatarUrl = null,
                name = "Name Surname",
                nickname = "nickname",
                birthdate = "25.01.2022",
                email = "email@email.com"
            )
        }

        composable(PROFILE_EDIT_INFO_SCREEN) {
            val viewModel = hiltViewModel<ProfileEditInfoScreenViewModel>()
            ProfileEditInfoScreen(
                navController = navController,
                viewModel = viewModel,
                avatarUrl = null,
                name = "Name",
                surname = "Surname",
                nickname = "nickname",
                birthdate = "25.01.2022"
            )
        }

        composable(ABOUT_US_SCREEN) {
            AboutUsScreen(
                navController = navController
            )
        }

        composable(PRIZES_SCREEN) {
            val viewModel = hiltViewModel<PrizesScreenViewModel>()
            PrizesScreen(
                navController = navController,
                viewModel = viewModel
            )
        }

        composable(
            route = GAME_DETAILS_SCREEN +
                    "/{id}" +
                    "/{title}" +
                    "/{description}" +
                    "/{encodedImageUrl}"
        ) { backStackEntry ->
            val viewModel = hiltViewModel<GameDetailsScreenViewModel>()

            val id = backStackEntry.arguments?.getString("id")
                ?: throw IllegalStateException()

            val title = backStackEntry.arguments?.getString("title")
                ?: throw IllegalStateException()

            val description = backStackEntry.arguments?.getString("description")
                ?: throw IllegalStateException()

            val encodedImageUrl = backStackEntry.arguments?.getString("encodedImageUrl")
                ?: throw IllegalStateException()

            val imageUrl = String(Base64.decode(encodedImageUrl, Base64.URL_SAFE))

            GameDetailsScreen(
                navController = navController,
                viewModel = viewModel,
                id = id,
                title = title,
                description = description,
                imageUrl = imageUrl
            )
        }

        quizGraph(navController)
    }
}

internal fun getGameDetailsScreenRoute(game: Game): String {
    return GAME_DETAILS_SCREEN +
            "/${game.id}" +
            "/${game.title}" +
            "/${game.description}" +
            "/${Base64.encodeToString(game.imageUrl.toByteArray(), Base64.URL_SAFE)}"
}