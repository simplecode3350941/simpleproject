package com.fruktorum.fruktparty.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.fruktorum.core.ui.composables.topBar.FruktPartyTopBar
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.TopStartCorneredShape
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.fruktparty.R

@Composable
fun TwoLayerBasicScreen(
    title: String,
    onClickBack: () -> Unit,
    content: @Composable (BoxScope.() -> Unit)
) {
    val dimen16 = dimensionResource(id = com.fruktorum.core.R.dimen.size_16_dp)
    val dimen24 = dimensionResource(id = com.fruktorum.core.R.dimen.size_24_dp)
    val dimen42 = dimensionResource(id = com.fruktorum.core.R.dimen.size_42_dp)

    ConstraintLayout(
        modifier = Modifier
            .fillMaxSize()
            .background(brush = RedFF512FPinkDD2476)
    ) {
        val (back_button, title_text, main_content) = createRefs()

        FruktPartyTopBar(
            modifier = Modifier.constrainAs(back_button) {
                top.linkTo(parent.top, margin = dimen16)
                start.linkTo(parent.start)
            },
            onBackClick = onClickBack
        )

        Text(
            text = title,
            style = MaterialTheme.typography.h1,
            color = Color.White,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(
                    horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_22_dp)
                )
                .constrainAs(title_text) {
                    top.linkTo(back_button.bottom, margin = dimen42)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
        )

        Box(
            content = content,
            modifier = Modifier
                .background(brush = WhiteFDFDFFGrayEEF2FF, shape = TopStartCorneredShape)
                .constrainAs(main_content) {
                    width = Dimension.fillToConstraints
                    height = Dimension.fillToConstraints
                    top.linkTo(title_text.bottom, margin = dimen24)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
        )
    }
}

@Preview(
    showBackground = true
)
@Composable
private fun TwoLayerBasicScreenPreview() {
    FruktPartyTheme {
        TwoLayerBasicScreen(
            title = "Log in",
            onClickBack = {}
        ) {
            Text(
                text = "Some content",
                modifier = Modifier
                    .align(Alignment.TopCenter)
                    .padding(top = dimensionResource(id = com.fruktorum.core.R.dimen.size_64_dp))
            )
        }
    }
}
