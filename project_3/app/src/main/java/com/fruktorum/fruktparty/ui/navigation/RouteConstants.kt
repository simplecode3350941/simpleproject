package com.fruktorum.fruktparty.ui.navigation

/* Tutorial screen*/
const val TUTORIAL_SCREEN_ROUTE = "TUTORIAL_SCREEN"

/* Welcome screen */
const val WELCOME_SCREEN_ROUTE = "WELCOME_SCREEN"

/* Games Tabs */
const val GAMES_ROUTE = "GAMES"

/* Leaderboard Tabs */
const val LEADERBOARD_ROUTE = "LEADERBOARD"

/* Profile tabs */
const val PROFILE_TAB = "PROFILE"

/* Login Screen */
const val LOGIN_SCREEN = "LOGIN"

/* Reset password Screen */
const val RESET_PASSWORD_SCREEN = "RESET_PASSWORD_SCREEN"

/* Reset password success Screen */
const val RESET_PASSWORD_SUCCESS_SCREEN = "RESET_PASSWORD_SUCCESS_SCREEN"

/* Sign up Screen */
const val SIGN_UP_SCREEN = "SIGN_UP"

/* Change password screen */
const val CHANGE_PASSWORD_SCREEN = "CHANGE_PASSWORD_SCREEN"

/* Profile info screen */
const val PROFILE_INFO_SCREEN = "PROFILE_INFO_SCREEN"

/* Profile edit info screen */
const val PROFILE_EDIT_INFO_SCREEN = "PROFILE_EDIT_INFO_SCREEN"

/* About Us screen */
const val ABOUT_US_SCREEN = "ABOUT_US_SCREEN"

/* Prizes Screen */
const val PRIZES_SCREEN = "PRIZES_SCREEN"

/* Game Details Screen */
const val GAME_DETAILS_SCREEN = "GAME_DETAILS_SCREEN"