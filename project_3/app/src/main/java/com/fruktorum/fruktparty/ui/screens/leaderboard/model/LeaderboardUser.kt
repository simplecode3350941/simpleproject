package com.fruktorum.fruktparty.ui.screens.leaderboard.model

data class LeaderboardUser(
    val avatarUrl: String?,
    val nickname: String,
    val rank: Int?,
    val starsCount: Int,
)