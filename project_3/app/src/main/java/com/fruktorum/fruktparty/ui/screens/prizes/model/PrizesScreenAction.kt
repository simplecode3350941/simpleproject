package com.fruktorum.fruktparty.ui.screens.prizes.model

internal sealed class PrizesScreenAction {

    class NavigateTo(
        val route: String
    ) : PrizesScreenAction()
}
