package com.fruktorum.fruktparty.ui.screens.signUp

import android.content.Context
import com.fruktorum.core.ui.vm.MVIViewModel
import com.fruktorum.domain.auth.usecase.SaveTokenUseCase
import com.fruktorum.fruktparty.ui.navigation.GAMES_ROUTE
import com.fruktorum.fruktparty.ui.screens.signUp.model.SignUpScreenAction
import com.fruktorum.fruktparty.ui.screens.signUp.model.SignUpScreenEvent
import com.fruktorum.fruktparty.ui.screens.signUp.model.SignUpScreenField
import com.fruktorum.fruktparty.ui.screens.signUp.model.SignUpScreenPage
import com.fruktorum.fruktparty.ui.screens.signUp.model.SignUpScreenState
import com.fruktorum.ftauth.FTAuth
import com.fruktorum.ftauth.data.auth.TypeElement
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

@HiltViewModel
internal class SignUpScreenViewModel @Inject constructor(
    @ApplicationContext private val appContext: Context,
    private val saveToken: SaveTokenUseCase
) : MVIViewModel<
        SignUpScreenState,
        Nothing,
        SignUpScreenEvent,
        SignUpScreenAction>() {

    init {
        setupFTRegisterParams()
    }

    override fun onEvent(event: SignUpScreenEvent) {
        when (event) {
            SignUpScreenEvent.Start -> handleStartEvent()
            is SignUpScreenEvent.FieldValueChange -> handleFieldValueChange(event.field)
            SignUpScreenEvent.ClickOnBack -> handleClickOnBack()
            SignUpScreenEvent.ClickOnContinueWithGmail -> handleClickOnContinueWithGmail()
            SignUpScreenEvent.ClickOnNext -> handleClickOnNext()
            SignUpScreenEvent.ClickOnSignUp -> handleClickOnSignUp()
        }
    }

    private fun setupFTRegisterParams() {
        FTAuth.getInstance().requiredElements =
            listOf(
                TypeElement.EMAIL,
                TypeElement.PASSWORD,
                TypeElement.CONFIRM_PASSWORD,
                TypeElement.NAME,
                TypeElement.LAST_NAME,
                TypeElement.FIRST_NAME,
                TypeElement.BIRTHDATE
            )

        FTAuth.getInstance().onRegistrationSuccess = {
            handleSignUpSuccess()
        }

        FTAuth.getInstance().onRegistrationFailure = {
            handleSignUpError()
        }
    }

    private fun handleStartEvent() {
        setState(
            SignUpScreenState.Content()
        )
    }

    private fun handleFieldValueChange(
        field: SignUpScreenField
    ) {
        val state = getStateValueOrNull()
        if (state !is SignUpScreenState.Content) return

        setState(
            with(state) {
                when (field) {
                    is SignUpScreenField.PromoCode -> {
                        copy(promoCode = field.value)
                    }
                    is SignUpScreenField.Nickname -> {
                        copy(nickname = field.value)
                    }
                    is SignUpScreenField.Email -> {
                        copy(email = field.value)
                    }
                    is SignUpScreenField.Password -> {
                        copy(password = field.value)
                    }
                    is SignUpScreenField.RepeatPassword -> {
                        copy(repeatPassword = field.value)
                    }
                    is SignUpScreenField.Name -> {
                        copy(name = field.value)
                    }
                    is SignUpScreenField.Surname -> {
                        copy(surname = field.value)
                    }
                    is SignUpScreenField.Birthdate -> {
                        copy(birthdate = field.value)
                    }
                }
            }
        )
    }

    private fun handleClickOnBack() {
        val state = getStateValueOrNull()
        if (state !is SignUpScreenState.Content) return

        when (state.page) {
            SignUpScreenPage.PromoCode -> {
                emitAction(
                    SignUpScreenAction.Back
                )
            }
            SignUpScreenPage.Main -> {
                setState(
                    state.copy(
                        page = SignUpScreenPage.PromoCode
                    )
                )
                emitAction(
                    SignUpScreenAction.ScrollToPage(
                        page = SignUpScreenPage.PromoCode
                    )
                )
            }
            SignUpScreenPage.Final -> {
                setState(
                    state.copy(
                        page = SignUpScreenPage.Main
                    )
                )
                emitAction(
                    SignUpScreenAction.ScrollToPage(
                        page = SignUpScreenPage.Main
                    )
                )
            }
        }
    }

    private fun handleClickOnContinueWithGmail() {
        FTAuth.getInstance().loginByGoogle(appContext)
    }

    private fun handleClickOnNext() {
        val state = getStateValueOrNull()
        if (state !is SignUpScreenState.Content) return

        when (state.page) {
            SignUpScreenPage.PromoCode -> {
                setState(
                    state.copy(
                        page = SignUpScreenPage.Main
                    )
                )
                emitAction(
                    SignUpScreenAction.ScrollToPage(
                        page = SignUpScreenPage.Main
                    )
                )
            }
            SignUpScreenPage.Main -> {
                setState(
                    state.copy(
                        page = SignUpScreenPage.Final
                    )
                )
                emitAction(
                    SignUpScreenAction.ScrollToPage(
                        page = SignUpScreenPage.Final
                    )
                )
            }
            SignUpScreenPage.Final -> Unit
        }
    }

    private fun handleClickOnSignUp() {
        setState(SignUpScreenState.Loading)
        FTAuth.getInstance().registration()
    }

    private fun handleSignUpError() {
        setState(SignUpScreenState.Error)
        // TODO #110
    }

    private fun handleSignUpSuccess() {
        saveTokenIfPossible()
        emitAction(
            SignUpScreenAction.NavigateTo(
                route = GAMES_ROUTE
            )
        )
    }
    private fun saveTokenIfPossible() {
        launchJob {
            val token = FTAuth.getInstance().getSessionToken()
            if (token.isNotEmpty()) {
                saveToken(token)
            } else {
                handleSignUpError()
            }
        }
    }
}