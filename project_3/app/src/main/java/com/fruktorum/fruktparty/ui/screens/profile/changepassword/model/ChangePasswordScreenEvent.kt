package com.fruktorum.fruktparty.ui.screens.profile.changepassword.model

internal sealed class ChangePasswordScreenEvent {

    data class ChangeFieldValue(
        val field: ChangePasswordField
    ) : ChangePasswordScreenEvent()

    class ChangeFieldVisibility(
        val field: ChangePasswordField
    ) : ChangePasswordScreenEvent()

    object ChangePasswordClick : ChangePasswordScreenEvent()
}
