package com.fruktorum.fruktparty.ui.screens.profile.info.model

internal sealed class ProfileInfoScreenState {

    object Loading : ProfileInfoScreenState()

    data class Content(
        val avatarUrl: String?,
        val name: String,
        val nickname: String,
        val birthdate: String,
        val email: String
    ) : ProfileInfoScreenState()
}
