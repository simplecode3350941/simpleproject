package com.fruktorum.fruktparty.ui.screens.games.model

import com.fruktorum.domain.entity.Game

internal sealed class GamesScreenEvent {

    object Start : GamesScreenEvent()

    class ClickOnInfo(
        val mode: Mode
    ) : GamesScreenEvent()

    class ClickOnGame(
        val game: Game
    ) : GamesScreenEvent()

    object CloseDialog : GamesScreenEvent()
}