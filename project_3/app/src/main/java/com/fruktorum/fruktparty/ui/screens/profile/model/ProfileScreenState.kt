package com.fruktorum.fruktparty.ui.screens.profile.model

internal sealed class ProfileScreenState {

    object Loading : ProfileScreenState()

    data class Content(
        val avatarUrl: String?,
        val nickname: String,
        val starsCount: Int,
        val prizesCount: Int
    ) : ProfileScreenState()

    object Error : ProfileScreenState()
}
