package com.fruktorum.fruktparty.ui.screens.profile

import androidx.lifecycle.viewModelScope
import com.fruktorum.core.ui.vm.MVIViewModel
import com.fruktorum.domain.profile.usecase.GetProfileInfoUseCase
import com.fruktorum.fruktparty.ui.navigation.ABOUT_US_SCREEN
import com.fruktorum.fruktparty.ui.navigation.CHANGE_PASSWORD_SCREEN
import com.fruktorum.fruktparty.ui.navigation.GAMES_ROUTE
import com.fruktorum.fruktparty.ui.navigation.PRIZES_SCREEN
import com.fruktorum.fruktparty.ui.navigation.PROFILE_INFO_SCREEN
import com.fruktorum.fruktparty.ui.navigation.WELCOME_SCREEN_ROUTE
import com.fruktorum.fruktparty.ui.screens.login.model.LoginScreenAction
import com.fruktorum.fruktparty.ui.screens.profile.model.ProfileScreenAction
import com.fruktorum.fruktparty.ui.screens.profile.model.ProfileScreenDialogState
import com.fruktorum.fruktparty.ui.screens.profile.model.ProfileScreenEvent
import com.fruktorum.fruktparty.ui.screens.profile.model.ProfileScreenState
import com.fruktorum.ftauth.FTAuth
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
internal class ProfileScreenViewModel
@Inject
constructor(
    private val getProfileInfo: GetProfileInfoUseCase
) : MVIViewModel<ProfileScreenState, ProfileScreenDialogState, ProfileScreenEvent, ProfileScreenAction>() {

    init {
        setupFTAuthLogout()
    }

    override fun onEvent(event: ProfileScreenEvent) {
        when (event) {
            ProfileScreenEvent.Start -> handleStartEvent()
            ProfileScreenEvent.ClickOnSeePrizes -> handleClickOnSeePrizesEvent()
            ProfileScreenEvent.ClickOnSeeProfile -> handleClickOnSeeProfileEvent()
            ProfileScreenEvent.ClickOnChangePassword -> handleClickOnChangePasswordEvent()
            ProfileScreenEvent.ClickOnAboutUs -> handleClickOnAboutUsEvent()
            ProfileScreenEvent.ClickOnLogOut -> handleClickOnLogOutEvent()
            ProfileScreenEvent.ClickOnDeleteAccount -> handleClickOnDeleteAccountEvent()
            ProfileScreenEvent.CloseDialog -> handleCloseDialog()
            ProfileScreenEvent.LogoutDialog -> handleLogoutDialog()
        }
    }

    private fun handleStartEvent() {
        CoroutineScope(Dispatchers.IO).launch {
            val profileInfo = getProfileInfo.invoke()
            profileInfo.let {
                setState(
                    ProfileScreenState.Content(
                        avatarUrl = it.avatarUrl,
                        nickname = it.nickname,
                        starsCount = it.starsCount,
                        prizesCount = it.prizesCount
                    )
                )
            }
        }
//        getProfileInfo(
//            onSuccess = {
//                setState(
//                    ProfileScreenState.Content(
//                        avatarUrl = it.avatarUrl,
//                        nickname = it.nickname,
//                        starsCount = it.starsCount,
//                        prizesCount = it.prizesCount
//                    )
//                )
//            }
//        )
    }

    private fun setupFTAuthLogout() {
        FTAuth.getInstance().onLogOutFailure = ::handleLogoutError

        FTAuth.getInstance().onLogOutSuccess = ::handleLogoutSuccess
    }

    private fun handleLogoutError(error: Throwable) {
        setState(ProfileScreenState.Error)
        //TODO
    }

    private fun handleLogoutSuccess() {
        emitAction(
            ProfileScreenAction.NavigateTo(
                route = WELCOME_SCREEN_ROUTE
            )
        )
    }

    private fun handleLogoutDialog() {
        FTAuth.getInstance().logOut()
        setState(ProfileScreenState.Loading)
    }

    private fun handleClickOnSeePrizesEvent() {
        emitAction(
            ProfileScreenAction.NavigateTo(
                route = PRIZES_SCREEN
            )
        )
    }

    private fun handleClickOnSeeProfileEvent() {
        emitAction(
            ProfileScreenAction.NavigateTo(
                route = PROFILE_INFO_SCREEN
            )
        )
    }

    private fun handleClickOnChangePasswordEvent() {
        emitAction(
            ProfileScreenAction.NavigateTo(
                route = CHANGE_PASSWORD_SCREEN
            )
        )
    }

    private fun handleClickOnAboutUsEvent() {
        emitAction(
            ProfileScreenAction.NavigateTo(
                route = ABOUT_US_SCREEN
            )
        )
    }

    private fun handleClickOnLogOutEvent() {
        setDialogState(ProfileScreenDialogState.LogOut)
    }

    private fun handleClickOnDeleteAccountEvent() {
        setDialogState(ProfileScreenDialogState.DeleteAccount)
    }

    private fun handleCloseDialog() {
        setDialogState(ProfileScreenDialogState.None)
    }
}
