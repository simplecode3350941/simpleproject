package com.fruktorum.fruktparty.ui.screens.games.model

internal sealed class GamesScreenDialogState {
    class EventInfoDialog(
        val event: EventUiModel
    ) : GamesScreenDialogState()

    object NoEventInfoDialog : GamesScreenDialogState()
}