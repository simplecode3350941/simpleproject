package com.fruktorum.fruktparty.ui.screens.games.dialog

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.fruktorum.core.ui.composables.dialog.FruktPartyDialog
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.GrayD8E1FF
import com.fruktorum.fruktparty.R

@Composable
internal fun NoEventInfoDialog(
    onClose: () -> Unit
) = FruktPartyDialog(
    topContent = {
        Image(
            painter = painterResource(R.drawable.img_no_event_info_dialog),
            contentDescription = null,
            modifier = Modifier
                .padding(bottom = dimensionResource(com.fruktorum.core.R.dimen.size_16_dp))
                .background(
                    color = GrayD8E1FF,
                    shape = CircleShape
                )
                .clip(CircleShape)
        )
    },
    title = stringResource(R.string.no_event_info_dialog_title),
    subtitle = stringResource(R.string.no_event_info_dialog_description),
    actionButtonText = stringResource(R.string.no_event_info_dialog_back_button_text),
    onClose = onClose,
    onClickAction = onClose
)

@Preview
@Composable
private fun NoEventInfoDialogPreview() {
    FruktPartyTheme {
        NoEventInfoDialog(
            onClose = {}
        )
    }
}