package com.fruktorum.fruktparty.ui.screens.games.model

import com.fruktorum.domain.entity.Game

internal sealed class GamesScreenState {
    object Loading : GamesScreenState()

    class Content(
        val mode: Mode,
        val games: List<Game>,
        val hasUnviewedPrizes: Boolean
    ) : GamesScreenState()

    class Error(
        val message: String?
    ) : GamesScreenState()
}