package com.fruktorum.fruktparty.ui.screens.resetPassword.model

internal sealed class ResetPasswordScreenEvent {

    class ClickOnConfirm(
        val email: String
    ) : ResetPasswordScreenEvent()
}
