package com.fruktorum.fruktparty.ui.screens.login

import android.content.Context
import com.fruktorum.core.ui.vm.MVIViewModel
import com.fruktorum.domain.auth.usecase.SaveTokenUseCase
import com.fruktorum.fruktparty.ui.navigation.GAMES_ROUTE
import com.fruktorum.fruktparty.ui.navigation.RESET_PASSWORD_SCREEN
import com.fruktorum.fruktparty.ui.screens.login.model.LoginScreenAction
import com.fruktorum.fruktparty.ui.screens.login.model.LoginScreenDialogState
import com.fruktorum.fruktparty.ui.screens.login.model.LoginScreenEvent
import com.fruktorum.fruktparty.ui.screens.login.model.LoginScreenState
import com.fruktorum.ftauth.FTAuth
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

@HiltViewModel
internal class LoginScreenViewModel @Inject constructor(
    @ApplicationContext private val appContext: Context,
    private val saveToken: SaveTokenUseCase
) : MVIViewModel<LoginScreenState, LoginScreenDialogState, LoginScreenEvent, LoginScreenAction>() {

    init {
        setupFTAuthLogin()
    }

    override fun onEvent(event: LoginScreenEvent) {
        when (event) {
            LoginScreenEvent.Start -> handleStartEvent()
            LoginScreenEvent.ClickOnLogin -> {
                setState(LoginScreenState.Loading)
                FTAuth.getInstance().login()
            }
            LoginScreenEvent.ClickOnLoginByGoogle -> FTAuth.getInstance().loginByGoogle(appContext)
            LoginScreenEvent.ClickOnForgotPassword -> handleClickResetPassword()
        }
    }

    private fun handleStartEvent() {
        setState(LoginScreenState.Content)
    }

    private fun handleLoginSuccess() {
        saveTokenIfPossible()
        emitAction(
            LoginScreenAction.NavigateTo(
                route = GAMES_ROUTE
            )
        )
    }

    private fun handleLoginError() {
        setState(LoginScreenState.Error)
        // TODO #110
    }

    private fun handleClickResetPassword() {
        emitAction(
            LoginScreenAction.NavigateTo(
                route = RESET_PASSWORD_SCREEN
            )
        )
    }

    private fun setupFTAuthLogin() {
        FTAuth.getInstance().onLoginSuccess = {
            handleLoginSuccess()
        }

        FTAuth.getInstance().onLoginFailure = {
            handleLoginError()
        }
    }

    private fun saveTokenIfPossible() {
        launchJob {
            val token = FTAuth.getInstance().getSessionToken()
            if (token.isNotEmpty()) {
                saveToken(token)
            } else {
                handleLoginError()
            }
        }
    }
}