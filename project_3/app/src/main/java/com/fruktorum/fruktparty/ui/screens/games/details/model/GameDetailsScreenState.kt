package com.fruktorum.fruktparty.ui.screens.games.details.model

import com.fruktorum.domain.entity.Game

internal sealed class GameDetailsScreenState {

    object Loading : GameDetailsScreenState()

    class Content(
        val game: Game,
        val route: String
    ) : GameDetailsScreenState()

    class ErrorGetGameStatus(
        val error: String?,
        val id: String,
        val title: String,
        val description: String,
        val imageUrl: String
    ) : GameDetailsScreenState()
}