package com.fruktorum.fruktparty.ui.screens.games.details.model

internal sealed class GameDetailsScreenEvent {

    class Start(
        val id: String,
        val title: String,
        val description: String,
        val imageUrl: String
    ) : GameDetailsScreenEvent()

    class ClickOnTryAgainErrorGetGameStatus(
        val id: String,
        val title: String,
        val description: String,
        val imageUrl: String
    ) : GameDetailsScreenEvent()
}