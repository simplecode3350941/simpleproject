package com.fruktorum.fruktparty.ui.screens.games.model

import com.fruktorum.domain.config.model.GetModeDomainModel

sealed class Mode {

    class Base(
        val modeTitle: String
    ) : Mode()

    class Event(
        val value: EventUiModel
    ) : Mode()

    companion object {
        const val EVENT_MODE = 1
    }
}

/*class Mode(
    val mode: Int,
    val modeTitle: String,
    val modeInfo: ModeInfo
)*/

fun GetModeDomainModel.toMode(): Mode {
    return if (this.mode ==  Mode.EVENT_MODE) {
        Mode.Event(
            value = EventUiModel(
                id = this.modeInfo.id,
                title = this.modeInfo.title,
                description = this.modeInfo.description,
                imageUrl = this.modeInfo.imageUrl,
                date = this.modeInfo.date,
                prizes = this.modeInfo.prizes
            )
        )
    } else {
        Mode.Base(
            modeTitle = this.modeTitle
        )
    }
}