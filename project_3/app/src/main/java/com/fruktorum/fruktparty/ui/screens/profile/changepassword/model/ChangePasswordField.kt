package com.fruktorum.fruktparty.ui.screens.profile.changepassword.model

internal sealed class ChangePasswordField(
    val value: String
) {
    class OldPassword(value: String) : ChangePasswordField(value)

    class NewPassword(value: String) : ChangePasswordField(value)

    class ConfirmNewPassword(value: String) : ChangePasswordField(value)
}
