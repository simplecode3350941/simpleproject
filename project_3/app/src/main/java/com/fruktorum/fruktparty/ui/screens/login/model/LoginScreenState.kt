package com.fruktorum.fruktparty.ui.screens.login.model

internal sealed class LoginScreenState {

    object Loading : LoginScreenState()

    object Content : LoginScreenState()

    object Error : LoginScreenState()
}
