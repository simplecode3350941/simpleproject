package com.fruktorum.fruktparty.ui.screens.leaderboard.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.BlueC1D0FF
import com.fruktorum.core.ui.theme.Gray9AA1B6
import com.fruktorum.core.ui.theme.YellowFFC530
import com.fruktorum.fruktparty.R
import com.fruktorum.fruktparty.ui.screens.leaderboard.model.LeaderboardUser
import com.skydoves.landscapist.glide.GlideImage

@Composable
internal fun LeaderboardUser(
    modifier: Modifier = Modifier,
    user: LeaderboardUser
) = Row(
    modifier = modifier,
    verticalAlignment = Alignment.CenterVertically
) {
    user.rank?.let {
        Text(
            text = it.toString(),
            modifier = Modifier
                .widthIn(min = dimensionResource(com.fruktorum.core.R.dimen.size_24_dp)),
            style = MaterialTheme.typography.body1,
            color = Black000B2D
        )

        Spacer(modifier = Modifier.width(dimensionResource(com.fruktorum.core.R.dimen.size_12_dp)))
    }

    GlideImage(
        imageModel = user.avatarUrl,
        error = painterResource(id = R.drawable.ic_avatar_default),
        contentDescription = null,
        modifier = Modifier
            .size(dimensionResource(id = com.fruktorum.core.R.dimen.size_35_dp))
            .clip(CircleShape)
            .background(BlueC1D0FF)
    )

    Spacer(modifier = Modifier.width(dimensionResource(com.fruktorum.core.R.dimen.size_5_dp)))

    Text(
        text = user.nickname,
        modifier = Modifier
            .padding(end = dimensionResource(id = com.fruktorum.core.R.dimen.size_16_dp))
            .weight(1f),
        style = MaterialTheme.typography.body1,
        color = Black000B2D,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis
    )

    Icon(
        painter = painterResource(id = R.drawable.ic_star_16),
        contentDescription = null,
        tint = YellowFFC530
    )

    Spacer(modifier = Modifier.size(dimensionResource(id = com.fruktorum.core.R.dimen.size_3_dp)))

    Text(
        text = user.starsCount.toString(),
        color = Gray9AA1B6,
        style = MaterialTheme.typography.body2
    )
}