package com.fruktorum.fruktparty.ui.screens.games.model

import com.fruktorum.domain.entity.Prize

class EventUiModel(
    val id: String,
    val title: String,
    val description: String,
    val imageUrl: String,
    val date: String?,
    val prizes: List<Prize>?
) {
    companion object {
        val testObject = EventUiModel(
            id = "0",
            title = "Title",
            description = "Description description description description description description",
            imageUrl = "https://picsum.photos/800/400",
            date = "31.12",
            prizes = listOf(
                Prize(
                    "0",
                    place = 1,
                    description = "250\$ coupon at Timberland store"
                ),
                Prize(
                    "1",
                    place = 2,
                    description = "150\$ coupon at Nike"
                ),
                Prize(
                    "2",
                    place = 3,
                    description = "50\$ coupon at Grab food"
                ),
                Prize(
                    "3",
                    place = 4,
                    description = "5\$ coupon at Grab food"
                ),
                Prize(
                    "4",
                    place = 5,
                    description = "3\$ coupon at Grab food"
                )
            )
        )
    }
}