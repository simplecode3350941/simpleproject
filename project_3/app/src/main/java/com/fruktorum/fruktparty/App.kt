package com.fruktorum.fruktparty

import android.app.Application
import com.fruktorum.ftauth.FTAuth
import dagger.hilt.android.HiltAndroidApp
import com.chibatching.kotpref.Kotpref
import com.fruktorum.core.sharedPreference.ServerUrlsModel.authServerUrl
import com.fruktorum.core.sharedPreference.ServerUrlsModel.backendServerUrl

@HiltAndroidApp
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        initKotpref()
        initFtAuth()
        saveUrlsToSharedPrefs()
    }


    private fun initKotpref() {
        Kotpref.init(applicationContext)
    }
    private fun initFtAuth() {
        FTAuth
            .Companion
            .Builder(this)
            .setServerUrl(BuildConfig.AUTH_SERVER_URL)
            .build()
    }

    private fun saveUrlsToSharedPrefs() {
        authServerUrl = BuildConfig.AUTH_SERVER_URL
        backendServerUrl = BuildConfig.SERVER_URL

    }
}