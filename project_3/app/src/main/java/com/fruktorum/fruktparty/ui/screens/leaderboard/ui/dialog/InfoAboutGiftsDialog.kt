package com.fruktorum.fruktparty.ui.screens.leaderboard.ui.dialog

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.fruktorum.core.ui.composables.dialog.FruktPartyDialog
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.GrayD8E1FF
import com.fruktorum.fruktparty.R

@Composable
internal fun InfoAboutGiftsDialog(
    onClose: () -> Unit,
    onClickLetsPlay: () -> Unit
) = FruktPartyDialog(
    topContent = {
        Image(
            painter = painterResource(R.drawable.img_gift),
            contentDescription = null,
            modifier = Modifier
                .padding(bottom = dimensionResource(id = com.fruktorum.core.R.dimen.size_16_dp))
                .background(
                    color = GrayD8E1FF,
                    shape = CircleShape
                )
                .clip(CircleShape)
        )
    },
    title = stringResource(R.string.leaderboard_screen_info_about_gifts_dialog_title),
    subtitle = stringResource(R.string.leaderboard_screen_info_about_gifts_dialog_text),
    actionButtonText = stringResource(R.string.leaderboard_screen_info_about_gifts_dialog_btn_text),
    onClose = onClose,
    onClickAction = onClickLetsPlay
)

@Preview
@Composable
private fun InfoAboutGiftsDialogPreview() {
    FruktPartyTheme {
        InfoAboutGiftsDialog(
            onClose = {},
            onClickLetsPlay = {}
        )
    }
}
