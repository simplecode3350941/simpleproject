package com.fruktorum.fruktparty.ui.screens.resetPassword

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import com.fruktorum.core.ext.isValidEmail
import com.fruktorum.core.ui.composables.loading.LoadingTransparentLayer
import com.fruktorum.core.ui.composables.textFields.FruktPartyTextField
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.Black0C002C
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.fruktparty.R
import com.fruktorum.fruktparty.ui.screens.TwoLayerBasicScreen
import com.fruktorum.fruktparty.ui.screens.error.ErrorScreen
import com.fruktorum.fruktparty.ui.screens.resetPassword.model.ResetPasswordScreenAction
import com.fruktorum.fruktparty.ui.screens.resetPassword.model.ResetPasswordScreenEvent
import com.fruktorum.fruktparty.ui.screens.resetPassword.model.ResetPasswordScreenState

/**
 * Reset password screen
 */
@Composable
internal fun ResetPasswordScreen(
    navController: NavHostController,
    viewModel: ResetPasswordScreenViewModel,
    modifier: Modifier = Modifier
) {
    val state = viewModel.state.collectAsState(initial = ResetPasswordScreenState.Content)

    val resetScreen = @Composable {
        ResetPasswordScreen(
            modifier = modifier,
            onClickBack = {
                navController.popBackStack()
            },
            onClickConfirm = {
                viewModel.onEvent(
                    ResetPasswordScreenEvent.ClickOnConfirm(it)
                )
            }
        )
    }

    when (val stateValue = state.value) {
        ResetPasswordScreenState.Loading -> {
            LoadingTransparentLayer { resetScreen.invoke() }
        }
        is ResetPasswordScreenState.Error -> {
            ErrorScreen(
                modifier = modifier,
                text = stateValue.message,
                onClickTryAgain = {
                    viewModel.onEvent(
                        ResetPasswordScreenEvent.ClickOnConfirm(stateValue.email)
                    )
                }
            )
        }
        ResetPasswordScreenState.Content -> {
            resetScreen.invoke()
        }
    }

    LaunchedEffect(true) {
        viewModel.action.collect { action ->
            when (action) {
                is ResetPasswordScreenAction.NavigateTo -> {
                    navController.navigate(
                        route = action.route
                    )
                }
            }
        }
    }
}

@Composable
private fun ResetPasswordScreen(
    modifier: Modifier,
    onClickBack: () -> Unit,
    onClickConfirm: (String) -> Unit,
) {
    val focusManager = LocalFocusManager.current

    var email by remember {
        mutableStateOf("")
    }

    var isError by remember {
        mutableStateOf(false)
    }


    TwoLayerBasicScreen(
        title = stringResource(R.string.reset_password_screen_title),
        onClickBack = onClickBack
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(modifier = Modifier.size(dimensionResource(com.fruktorum.core.R.dimen.size_40_dp)))

            val fieldModifier = Modifier
                .fillMaxWidth()
                .padding(
                    horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_44_dp),
                    vertical = dimensionResource(com.fruktorum.core.R.dimen.size_16_dp)
                )

            FruktPartyTextField(
                value = email,
                onValueChange = {
                    email = it
                    isError = !it.isValidEmail()
                },
                modifier = fieldModifier,
                keyboardOptions = KeyboardOptions.Default.copy(
                    keyboardType = KeyboardType.Email,
                    autoCorrect = false,
                    imeAction = ImeAction.Done
                ),
                isError = isError,
                keyboardActions = KeyboardActions(
                    onDone = { focusManager.clearFocus(force = true) }
                ),
                placeholder = stringResource(R.string.reset_password_screen_email_text_field_placeholder)
            )

            Row(
                modifier = fieldModifier
            ) {
                Image(
                    painter = painterResource(R.drawable.ic_star_12),
                    contentDescription = null,
                    modifier = Modifier
                        .padding(
                            top = dimensionResource(com.fruktorum.core.R.dimen.size_4_dp),
                            end = dimensionResource(com.fruktorum.core.R.dimen.size_5_dp)
                        )
                        .size(dimensionResource(com.fruktorum.core.R.dimen.size_12_dp))
                )

                Text(
                    text = stringResource(R.string.reset_password_screen_description),
                    style = MaterialTheme.typography.body2,
                    color = Black000B2D
                )
            }
        }

        TextButton(
            shape = RoundedCornerShape(
                topStart = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp),
                bottomEnd = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)
            ),
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.BottomCenter)
                .padding(
                    horizontal = dimensionResource(com.fruktorum.core.R.dimen.size_44_dp)
                )
                .padding(bottom = dimensionResource(com.fruktorum.core.R.dimen.size_40_dp))
                .background(
                    Black0C002C,
                    RoundedCornerShape(
                        topStart = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp),
                        bottomEnd = dimensionResource(com.fruktorum.core.R.dimen.size_32_dp)
                    )
                ),
            contentPadding = PaddingValues(
                all = dimensionResource(com.fruktorum.core.R.dimen.size_15_dp)
            ),
            onClick = {
                if (email.isValidEmail()) {
                    onClickConfirm(email)
                }
            }
        ) {
            Text(
                text = stringResource(R.string.reset_password_screen_confirm_button_text),
                style = MaterialTheme.typography.body1,
                color = Color.White
            )
        }
    }
}

@Preview(
    showBackground = true
)
@Composable
private fun ResetPasswordScreenPreview() {
    FruktPartyTheme {
        ResetPasswordScreen(
            modifier = Modifier,
            onClickBack = {},
            onClickConfirm = {}
        )
    }
}
