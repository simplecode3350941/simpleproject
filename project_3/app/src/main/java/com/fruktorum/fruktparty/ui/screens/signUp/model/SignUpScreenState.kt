package com.fruktorum.fruktparty.ui.screens.signUp.model

internal sealed class SignUpScreenState {

    object Loading : SignUpScreenState()

    data class Content(
        val page: SignUpScreenPage = SignUpScreenPage.PromoCode,
        val promoCode: String = "",
        val nickname: String = "",
        val email: String = "",
        val password: String = "",
        val repeatPassword: String = "",
        val name: String = "",
        val surname: String = "",
        val birthdate: String = ""
    ) : SignUpScreenState()

    object Error : SignUpScreenState()
}

internal enum class SignUpScreenPage {
    PromoCode,
    Main,
    Final
}
