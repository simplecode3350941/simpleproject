package com.fruktorum.fruktparty.ui.screens.resetPassword

import com.fruktorum.core.ui.vm.MVIViewModel
import com.fruktorum.domain.auth.usecase.SendRequestResetPassUseCase
import com.fruktorum.domain.base.onError
import com.fruktorum.domain.base.onLoading
import com.fruktorum.domain.base.onSuccess
import com.fruktorum.fruktparty.ui.navigation.RESET_PASSWORD_SUCCESS_SCREEN
import com.fruktorum.fruktparty.ui.screens.resetPassword.model.ResetPasswordScreenAction
import com.fruktorum.fruktparty.ui.screens.resetPassword.model.ResetPasswordScreenEvent
import com.fruktorum.fruktparty.ui.screens.resetPassword.model.ResetPasswordScreenState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
internal class ResetPasswordScreenViewModel
@Inject
constructor(
    private val sendRequestResetPassword: SendRequestResetPassUseCase
) : MVIViewModel<ResetPasswordScreenState, Nothing, ResetPasswordScreenEvent, ResetPasswordScreenAction>() {

    override fun onEvent(event: ResetPasswordScreenEvent) {
        when (event) {
            is ResetPasswordScreenEvent.ClickOnConfirm ->
                handleClickOnConfirm(event.email)
        }
    }

    private fun handleClickOnConfirm(
        email: String
    ) {
        launchJob {
            sendRequestResetPassword(email).collect {
                with(it) {
                    onSuccess {
                        emitAction(
                            ResetPasswordScreenAction.NavigateTo(
                                route = RESET_PASSWORD_SUCCESS_SCREEN
                            )
                        )
                    }
                    onError {
                        setState(
                            ResetPasswordScreenState.Error(
                                message = this,
                                email = email
                            )
                        )
                    }
                    onLoading {
                        setState(ResetPasswordScreenState.Loading)
                    }
                }
            }
        }
    }
}