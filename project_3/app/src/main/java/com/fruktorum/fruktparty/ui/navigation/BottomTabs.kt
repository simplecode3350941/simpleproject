package com.fruktorum.fruktparty.ui.navigation

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.fruktorum.fruktparty.R

enum class BottomTabs(
    @StringRes
    val title: Int,
    @DrawableRes
    val icon: Int,
    val route: String,
    val innerRoutes: List<String>
) {
    GAMES(R.string.games_tab_title, R.drawable.ic_games_tab, GAMES_ROUTE, listOf(GAMES_ROUTE)),
    LEADERBOARD(R.string.leader_board_tab_title, R.drawable.ic_leader_board_tab, LEADERBOARD_ROUTE, listOf(LEADERBOARD_ROUTE)),
    PROFILE(R.string.profile_tab_title, R.drawable.ic_profile_tab, PROFILE_TAB, listOf(PROFILE_TAB))
}