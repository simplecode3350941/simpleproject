package com.fruktorum.fruktparty.ui.screens.prizes.model

internal class Prize(
    val imageUrl: String,
    val title: String
) {
    val ratio = try {
        with(imageUrl.split("/")) {
            this[this.lastIndex - 1].toInt() / this.last().toFloat()
        }
    } catch (e: Exception) {
        1f
    }

    companion object {
        val listOfTestPrizes = arrayListOf(
            Prize(
                imageUrl = "https://picsum.photos/200/300",
                title = "Title"
            ),
            Prize(
                imageUrl = "https://picsum.photos/300/300",
                title = "Title Title"
            ),
            Prize(
                imageUrl = "https://picsum.photos/400/300",
                title = "Title Title Title"
            ),
            Prize(
                imageUrl = "https://picsum.photos/300/200",
                title = "Title Title Title Title"
            ),
            Prize(
                imageUrl = "https://picsum.photos/400/400",
                title = "Title Title Title Title Title"
            ),
            Prize(
                imageUrl = "https://picsum.photos/200/400",
                title = "Title Title Title Title Title Title"
            )
        )
    }
}