package com.fruktorum.fruktparty.ui.screens.profile.model

internal sealed class ProfileScreenAction {

    class NavigateTo(
        val route: String
    ) : ProfileScreenAction()
}
