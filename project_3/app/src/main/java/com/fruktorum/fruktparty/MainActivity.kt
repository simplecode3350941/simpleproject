package com.fruktorum.fruktparty

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.navigation.compose.rememberNavController
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.animateLottieCompositionAsState
import com.airbnb.lottie.compose.rememberLottieComposition
import com.fruktorum.core.sharedPreference.ConfigModel
import com.fruktorum.core.sharedPreference.SessionModel
import com.fruktorum.core.ui.composables.snackbar.SnackbarErrorNetworkConnection
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.fruktparty.ui.navigation.BottomNavigation
import com.fruktorum.fruktparty.ui.navigation.BottomTabs
import com.fruktorum.fruktparty.ui.navigation.NavigationGraph
import com.fruktorum.fruktparty.ui.navigation.TUTORIAL_SCREEN_ROUTE
import com.fruktorum.fruktparty.ui.navigation.WELCOME_SCREEN_ROUTE
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val networkRequest = NetworkRequest.Builder()
        .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
        .build()

    private val networkConnectivityState = mutableStateOf(true)

    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        // network is available for use
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            networkConnectivityState.value = true
        }

        // lost network connection
        override fun onLost(network: Network) {
            super.onLost(network)
            networkConnectivityState.value = false
        }
    }

    @OptIn(ExperimentalAnimationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        val splashScreen = installSplashScreen()

        super.onCreate(savedInstanceState)

        //TODO refactoring deprecated code
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            statusBarColor = getColor(R.color.transparent)
            actionBar.let { it?.hide() }
        }

        splashScreen.setKeepOnScreenCondition { false }

        val connectivityManager = getSystemService(ConnectivityManager::class.java)
                as ConnectivityManager
        connectivityManager.requestNetwork(networkRequest, networkCallback)

        setContent {
            FruktPartyTheme {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                ) {
                    MainScreenView()

                    val isNetworkConnected by remember {
                        networkConnectivityState
                    }

                    AnimatedVisibility(
                        visible = !isNetworkConnected,
                        modifier = Modifier.align(Alignment.TopCenter),
                        enter = scaleIn(),
                        exit = scaleOut()
                    ) {
                        SnackbarErrorNetworkConnection()
                    }
                }

                LaunchedEffect(true) {
                    val currentNetwork = connectivityManager.activeNetwork
                    networkConnectivityState.value = currentNetwork != null
                }
            }
        }
    }
}

@Composable
@Preview
private fun MainScreenView() {
    val navController = rememberNavController()

    var isPrizeFloatingButtonVisible by remember {
        mutableStateOf(false)
    }
    navController.addOnDestinationChangedListener { _, destination, _ ->
        val isCurrentDestinationMainScreen = listOf(
            BottomTabs.GAMES.route,
            BottomTabs.LEADERBOARD.route,
            BottomTabs.PROFILE.route
        ).contains(destination.route)

        isPrizeFloatingButtonVisible = isCurrentDestinationMainScreen
    }

    Scaffold(
        modifier = Modifier
            .fillMaxSize(),
        bottomBar = {
            BottomNavigation(navController = navController)
        },
        floatingActionButton = {
            if (isPrizeFloatingButtonVisible) {
                val composition by rememberLottieComposition(
                    LottieCompositionSpec.RawRes(R.raw.wave)
                )
                val progress by animateLottieCompositionAsState(
                    composition = composition,
                    iterations = LottieConstants.IterateForever
                )
                LottieAnimation(
                    modifier = Modifier
                        .size(dimensionResource(com.fruktorum.core.R.dimen.size_100_dp)),
                    composition = composition,
                    progress = { progress }
                )
            }
        }
    ) { innerPadding ->
        val startDestination = if (SessionModel.sessionToken.isNotEmpty()) {
            BottomTabs.GAMES.route
        } else if (ConfigModel.isTutorialViewed) {
            WELCOME_SCREEN_ROUTE
        } else {
            TUTORIAL_SCREEN_ROUTE
        }
        NavigationGraph(
            navController = navController,
            modifier = Modifier
                .fillMaxSize()
                .padding(innerPadding),
            startDestination = startDestination
        )
    }
}