package com.fruktorum.fruktparty.ui.screens.login.model

internal sealed class LoginScreenEvent {

    object Start: LoginScreenEvent()

    object ClickOnLogin : LoginScreenEvent()

    object ClickOnLoginByGoogle : LoginScreenEvent()

    object ClickOnForgotPassword : LoginScreenEvent()
}