package com.fruktorum.fruktparty.ui.screens.games.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import com.fruktorum.core.R
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.domain.entity.Game
import com.skydoves.landscapist.glide.GlideImage

@Composable
internal fun GameItem(
    game: Game,
    topPadding: Dp = dimensionResource(id = com.fruktorum.core.R.dimen.size_8_dp),
    onClick: () -> Unit = {}
) {
    Column(
        modifier = Modifier
            .padding(
                top = topPadding,
                start = dimensionResource(id = com.fruktorum.core.R.dimen.size_8_dp),
                end = dimensionResource(id = com.fruktorum.core.R.dimen.size_8_dp)
            )
            .clickable(onClick = onClick)
    ) {
        val width = game.imageRatio.first
        val height = game.imageRatio.second

        var modifier = Modifier
            .fillMaxWidth()
            .clip(RoundedCornerShape(dimensionResource(id = com.fruktorum.core.R.dimen.size_16_dp)))

        var contentScale = ContentScale.FillWidth

        if (height / width < 0.5) {
            // width more than height is 2+ times
            modifier = Modifier
                .fillMaxWidth()
                .aspectRatio(2 / 1F)
                .clip(RoundedCornerShape(dimensionResource(id = com.fruktorum.core.R.dimen.size_16_dp)))

            contentScale = ContentScale.Crop
        }

        if (width / height < 0.5) {
            // height more than width is 2+ times
            modifier = Modifier
                .fillMaxWidth()
                .aspectRatio(1 / 2F)
                .clip(RoundedCornerShape(dimensionResource(id = com.fruktorum.core.R.dimen.size_16_dp)))
            contentScale = ContentScale.Crop
        }

        GlideImage(
            imageModel = game.imageUrl,
            modifier = modifier,
            alignment = Alignment.Center,
            contentScale = contentScale,
            contentDescription = game.title
        )

        Row(
            verticalAlignment = Alignment.Top,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = dimensionResource(id = R.dimen.size_8_dp))
        ) {
            Text(
                text = game.title,
                style = MaterialTheme.typography.body1,
                color = Black000B2D,
                textAlign = TextAlign.Start,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier
                    .weight(6f, true)
            )

            game.userScore?.let {
                GameScoreElement(
                    score = it,
                    modifier = Modifier
                        .wrapContentWidth()
                        .padding(top = dimensionResource(id = R.dimen.size_2_dp))
                )
            }
        }
    }
}

@Composable
@Preview
private fun GameItemPreview() {
    FruktPartyTheme {
        GameItem(
            game = Game(
                id = "1",
                title = "Some game title",
                description = "Some description title",
                imageUrl = "https://picsum.photos/800/400",
                userScore = 5642
            )
        )
    }
}