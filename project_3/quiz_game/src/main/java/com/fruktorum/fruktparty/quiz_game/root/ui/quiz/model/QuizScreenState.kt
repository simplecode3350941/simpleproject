package com.fruktorum.fruktparty.quiz_game.root.ui.quiz.model

import com.fruktorum.domain.game.quiz.model.Answer
import com.fruktorum.domain.game.quiz.model.Question

internal sealed class QuizScreenState {

    object Loading : QuizScreenState()

    data class Content(
        val isLoading: Boolean = false,
        val round: Int,
        val totalRounds: Int,
        val questions: List<Question>,
        val currentQuestionNumber: Int? = null,
        val currentChosenAnswer: Answer? = null,
        val currentChosenRates: Map<Answer, Int?>? = null
    ) : QuizScreenState()

    data class ErrorSendResult(
        val result: Int,
        val correctCount: Int,
        val totalCount: Int,
        val round: Int,
        val totalRounds: Int
    ) : QuizScreenState()

    object Error : QuizScreenState()
}