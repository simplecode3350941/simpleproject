package com.fruktorum.fruktparty.quiz_game.root.ui.quiz

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.fruktorum.core.ui.composables.buttons.TopStartBottomEndRoundedButton
import com.fruktorum.core.ui.composables.loading.LoadingTransparentLayer
import com.fruktorum.core.ui.composables.screen.LoadingScreen
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.domain.game.quiz.model.Answer
import com.fruktorum.domain.game.quiz.model.Question
import com.fruktorum.fruktparty.quiz_game.R
import com.fruktorum.fruktparty.quiz_game.root.ui.elements.QuestionBlock
import com.fruktorum.fruktparty.quiz_game.root.ui.elements.RoundCircle
import com.fruktorum.fruktparty.quiz_game.root.ui.elements.TimerRow
import com.fruktorum.fruktparty.quiz_game.root.ui.error.ErrorScreen
import com.fruktorum.fruktparty.quiz_game.root.ui.quiz.model.QuizScreenAction
import com.fruktorum.fruktparty.quiz_game.root.ui.quiz.model.QuizScreenEvent
import com.fruktorum.fruktparty.quiz_game.root.ui.quiz.model.QuizScreenState

@Composable
internal fun QuizScreen(
    navController: NavController,
    viewModel: QuizScreenViewModel,
    id: String,
    round: Int,
    startUserScore: Int,
    modifier: Modifier = Modifier
) {
    val state = viewModel.state.collectAsState(initial = QuizScreenState.Loading)

    when (val stateValue = state.value) {
        QuizScreenState.Loading -> {
            LoadingScreen(modifier)
        }
        is QuizScreenState.Content -> {
            QuizScreen(
                content = stateValue,
                timerState = viewModel.timerState.collectAsState().value,
                modifier = modifier,
                onClickAnswer = {
                    viewModel.onEvent(
                        QuizScreenEvent.ClickAnswer(it)
                    )
                },
                onClickRate = { answer, rate ->
                    viewModel.onEvent(
                        QuizScreenEvent.ClickRate(answer, rate)
                    )
                },
                onClickNext = {
                    viewModel.onEvent(
                        QuizScreenEvent.ClickNext
                    )
                }
            )

            BackHandler(true) {
                // disable back click
            }
        }
        is QuizScreenState.ErrorSendResult -> {
            ErrorScreen(
                round = round,
                onClickClose = {
                    navController.popBackStack(
                        //TODO hardcode
                        route = "GAMES",
                        inclusive = false
                    )
                },
                onClickTryAgain = {
                    viewModel.onEvent(
                        QuizScreenEvent.ClickOnTryAgainSendResult(
                            id = id,
                            result = stateValue.result,
                            correctCount = stateValue.correctCount,
                            totalCount = stateValue.totalCount,
                            round = stateValue.round,
                            totalRounds = stateValue.totalRounds
                        )
                    )
                }
            )

            BackHandler {
                navController.popBackStack(
                    //TODO hardcode
                    route = "GAMES",
                    inclusive = false
                )
            }
        }
        QuizScreenState.Error -> {
            //TODO
        }
    }

    LaunchedEffect(true) {
        viewModel.onEvent(QuizScreenEvent.Start(id, round, startUserScore))

        viewModel.action.collect { action ->
            when (action) {
                is QuizScreenAction.NavigateTo -> {
                    navController.navigate(
                        route = action.route
                    )
                }
                is QuizScreenAction.NavigateToWithClearBackstack -> {
                    navController.navigate(
                        route = action.route
                    ) {
                        popUpTo(route = "GAMES")
                    }
                }
            }
        }
    }
}

@Composable
internal fun QuizScreen(
    content: QuizScreenState.Content,
    timerState: Float,
    modifier: Modifier = Modifier,
    onClickAnswer: (Answer) -> Unit,
    onClickRate: (Answer, Int) -> Unit,
    onClickNext: () -> Unit,
) = Box(
    modifier = modifier
) {
    Image(
        painter = painterResource(
            when (content.round) {
                //TODO get from content
                1 -> R.drawable.bg_quiz_first_round
                2 -> R.drawable.bg_quiz_second_round
                else -> R.drawable.bg_quiz_third_round
            }
        ),
        contentDescription = null,
        modifier = Modifier
            .fillMaxSize(),
        contentScale = ContentScale.Crop
    )

    Column(
        modifier = Modifier
            .fillMaxSize()
            .statusBarsPadding()
    ) {
        RoundCircle(
            round = content.round,
            modifier = Modifier
                .align(Alignment.Start)
                .padding(
                    start = 12.dp,
                    top = 16.dp
                )
                .size(62.dp)
        )

        Spacer(modifier = Modifier.size(18.dp))

        TimerRow(
            modifier = Modifier
                .padding(
                    horizontal = 43.dp
                ),
            state = timerState
        )

        Spacer(modifier = Modifier.size(16.dp))

        content.questions.find {
            it.number == content.currentQuestionNumber
        }?.let { question ->
            QuestionBlock(
                modifier = Modifier
                    .weight(1f)
                    .padding(
                        horizontal = 43.dp
                    ),
                question = question,
                totalCount = content.questions.size,
                chosenAnswer = content.currentChosenAnswer,
                chosenRates = content.currentChosenRates,
                onClickAnswer = onClickAnswer,
                onClickRate = onClickRate
            )
        } ?: Spacer(
            modifier = Modifier
                .weight(1f)
        )

        TopStartBottomEndRoundedButton(
            text = stringResource(R.string.quiz_screen_next_btn_text),
            onClick = onClickNext,
            backgroundGradient = RedFF512FPinkDD2476,
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    horizontal = 43.dp,
                    vertical = 32.dp
                )
        )
    }

    if (content.isLoading) {
        LoadingTransparentLayer()
    }
}

@Preview(
    showBackground = true
)
@Composable
private fun QuizScreenPreview() {
    FruktPartyTheme {
        QuizScreen(
            content = QuizScreenState.Content(
                round = 3,
                totalRounds = 3,
                questions = listOf(
                    Question(
                        number = 1,
                        title = "Вопрос 1",
                        answers = listOf(
                            Answer(
                                number = 1,
                                text = "Ответ 1",
                                isCorrect = false,
                                rates = null
                            ),
                            Answer(
                                number = 2,
                                text = "Ответ 2",
                                isCorrect = false,
                                rates = null
                            ),
                            Answer(
                                number = 3,
                                text = "Ответ 3",
                                isCorrect = true,
                                rates = null
                            ),
                            Answer(
                                number = 4,
                                text = "Ответ 4",
                                isCorrect = false,
                                rates = listOf(1, 2)
                            )
                        )
                    )
                ),
                currentQuestionNumber = 1,
                currentChosenAnswer = null
            ),
            timerState = 0.5f,
            modifier = Modifier,
            onClickAnswer = {},
            onClickRate = { answer, rate -> },
            onClickNext = {}
        )
    }
}