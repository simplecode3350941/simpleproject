package com.fruktorum.fruktparty.quiz_game.root.ui.quiz

import com.fruktorum.core.ui.vm.MVIViewModel
import com.fruktorum.domain.base.onError
import com.fruktorum.domain.base.onLoading
import com.fruktorum.domain.base.onSuccess
import com.fruktorum.domain.game.common.model.SetGameStatusDomainModel
import com.fruktorum.domain.game.common.usecase.SetGameStatusUseCase
import com.fruktorum.domain.game.quiz.model.Answer
import com.fruktorum.domain.game.quiz.model.Question
import com.fruktorum.domain.game.quiz.usecase.GetQuizUseCase
import com.fruktorum.fruktparty.quiz_game.root.ui.navigation.getQuizResultRoute
import com.fruktorum.fruktparty.quiz_game.root.ui.quiz.model.AnsweredQuestion
import com.fruktorum.fruktparty.quiz_game.root.ui.quiz.model.QuizScreenAction
import com.fruktorum.fruktparty.quiz_game.root.ui.quiz.model.QuizScreenEvent
import com.fruktorum.fruktparty.quiz_game.root.ui.quiz.model.QuizScreenState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject
import com.fruktorum.fruktparty.quiz_game.root.ui.quiz.model.Answer as QuizAnswer

private const val timerMillis = 10_000f
private const val timerThreshold = 32L

@HiltViewModel
internal class QuizScreenViewModel
@Inject
constructor(
    private val getQuizContent: GetQuizUseCase,
    private val setGameStatus: SetGameStatusUseCase
) : MVIViewModel<
        QuizScreenState,
        Nothing,
        QuizScreenEvent,
        QuizScreenAction>() {

    private var timerJob: Job? = null

    private val _timerState = MutableStateFlow(1f)
    val timerState: StateFlow<Float> = _timerState

    private val answers: MutableList<AnsweredQuestion> = mutableListOf()

    // Game id
    private var _id: String? = null

    private var _startUserScore = 0

    override fun onEvent(event: QuizScreenEvent) {
        when (event) {
            is QuizScreenEvent.Start -> handleStart(event.id, event.round, event.startUserScore)
            is QuizScreenEvent.ClickAnswer -> handleClickAnswer(event.value)
            is QuizScreenEvent.ClickRate -> handleClickRate(event.answer, event.rate)
            QuizScreenEvent.ClickNext -> handleClickNext()
            is QuizScreenEvent.ClickOnTryAgainSendResult -> handleClickOnTryAgain(
                id = event.id,
                result = event.result,
                correctCount = event.correctCount,
                totalCount = event.totalCount,
                round = event.round,
                totalRounds = event.totalRounds
            )
        }
    }

    private fun handleStart(id: String, round: Int, startUserScore: Int) = launchJob(
        exceptionBlock = {
            setState(
                QuizScreenState.Error
            )
        }
    ) {
        _id = id
        _startUserScore = startUserScore

        val quiz = getQuizContent(
            id = id,
            forceLoad = false
        )

        quiz?.let {
            val questions = quiz.rounds.find {
                it.number == round
            }?.questions

            questions?.let {
                setState(
                    QuizScreenState.Content(
                        round = round,
                        totalRounds = quiz.rounds.size,
                        questions = it
                    )
                )

                nextQuestion()
            } ?: throw IllegalStateException()
        } ?: throw IllegalStateException()
    }

    private fun handleClickRate(answer: Answer, rate: Int) {
        val state = getStateValueOrNull()
        if (state !is QuizScreenState.Content) return


        val newCurrentChosenRates = state.currentChosenRates.orEmpty().mapValues {
            if (it.key == answer) {
                if (it.value != rate) {
                    rate
                } else null
            } else it.value
        }

        setState(
            state.copy(
                currentChosenRates = newCurrentChosenRates
            )
        )
    }

    private fun handleClickAnswer(value: Answer) {
        val state = getStateValueOrNull()
        if (state !is QuizScreenState.Content) return

        setState(
            state.copy(
                currentChosenAnswer = value
            )
        )
    }

    private fun handleClickNext() {
        val state = getStateValueOrNull()
        if (state !is QuizScreenState.Content) return

        if (state.currentChosenAnswer == null) return //TODO show alert?

        saveAnswers()
        nextQuestion()
    }

    private fun handleClickOnTryAgain(
        id: String,
        result: Int,
        correctCount: Int,
        totalCount: Int,
        round: Int,
        totalRounds: Int
    ) = launchJob {
        setGameStatus(
            getSetGameStatusDomainModel(
                id = id,
                result = result,
                round = round,
                totalRounds = totalRounds
            )
        ).collect {
            with(it) {
                onLoading {
                    setState(QuizScreenState.Loading)
                }
                onSuccess {
                    emitAction(
                        QuizScreenAction.NavigateToWithClearBackstack(
                            route = getQuizResultRoute(
                                id = id,
                                result = result,
                                correctCount = correctCount,
                                totalCount = totalCount,
                                round = round,
                                totalRounds = totalRounds,
                                userScore = _startUserScore + result
                            )
                        )
                    )
                }
                onError {
                    setState(
                        QuizScreenState.ErrorSendResult(
                            result = result,
                            correctCount = correctCount,
                            totalCount = totalCount,
                            round = round,
                            totalRounds = totalRounds
                        )
                    )
                }
            }
        }
    }

    private fun nextQuestion() {
        val state = getStateValueOrNull()
        if (state !is QuizScreenState.Content) return

        timerJob?.cancel()

        if (state.currentQuestionNumber == state.questions.last().number) {
            endQuiz()
            return
        }

        val newCurrentQuestionIndex = state.currentQuestionNumber?.plus(1) ?: 1
        val newCurrentChosenRates: Map<Answer, Int?> =
            state.questions.find {
                it.number == newCurrentQuestionIndex
            }?.answers?.associateWith { null } ?: throw IllegalStateException()

        setState(
            state.copy(
                currentQuestionNumber = newCurrentQuestionIndex,
                currentChosenAnswer = null,
                currentChosenRates = newCurrentChosenRates
            )
        )

        timerJob = launchJob {
            var i = timerMillis
            while (i > 0) {
                delay(timerThreshold)
                i -= timerThreshold
                _timerState.value = i / timerMillis
            }
            saveAnswers()
            nextQuestion()
        }
    }

    private fun saveAnswers() {
        val state = getStateValueOrNull()
        if (state !is QuizScreenState.Content) return

        val currentQuestionNumber = state.currentQuestionNumber ?: throw IllegalStateException()

        answers.add(
            AnsweredQuestion(
                number = currentQuestionNumber,
                answers = state.questions.find {
                    it.number == currentQuestionNumber
                }?.answers?.map {
                    QuizAnswer(
                        number = it.number,
                        rate = state.currentChosenRates?.get(it)
                    )
                } ?: throw IllegalStateException(),
                chosenAnswerNumber = state.currentChosenAnswer?.number
            )
        )
    }

    private fun endQuiz() = launchJob {
        val state = getStateValueOrNull()
        if (state !is QuizScreenState.Content) throw IllegalStateException("state != Content")

        setState(state.copy(isLoading = true))

        val (result, correctCount) = calculateResult(state.questions, answers)
        val id = _id

        if (id != null) {
            setGameStatus(
                getSetGameStatusDomainModel(
                    id = id,
                    result = result,
                    round = state.round,
                    totalRounds = state.totalRounds
                )
            ).collect {
                with(it) {
                    onSuccess {
                        emitAction(
                            QuizScreenAction.NavigateToWithClearBackstack(
                                route = getQuizResultRoute(
                                    id = id,
                                    result = result,
                                    correctCount = correctCount,
                                    totalCount = state.questions.size,
                                    round = state.round,
                                    totalRounds = state.totalRounds,
                                    userScore = _startUserScore + result
                                )
                            )
                        )
                    }
                    onError {
                        setState(
                            QuizScreenState.ErrorSendResult(
                                result = result,
                                correctCount = correctCount,
                                totalCount = state.questions.size,
                                round = state.round,
                                totalRounds = state.totalRounds
                            )
                        )
                    }
                }
            }
        } else {
            setState(
                QuizScreenState.ErrorSendResult(
                    result = result,
                    correctCount = correctCount,
                    totalCount = state.questions.size,
                    round = state.round,
                    totalRounds = state.totalRounds
                )
            )
        }
    }

    private fun calculateResult(
        questions: List<Question>,
        answers: List<AnsweredQuestion>
    ): Pair<Int, Int> {
        var result = 0
        var correctCount = 0
        answers.forEach { answeredQuestion ->
            val correctAnswerNumber = questions.find { question ->
                question.number == answeredQuestion.number
            }?.answers?.find {
                // only one answer is correct!
                it.isCorrect
            }?.number ?: throw IllegalStateException()

            if (answeredQuestion.chosenAnswerNumber == correctAnswerNumber) {
                result++
                correctCount++
            }

            answeredQuestion.answers.forEach {
                it.rate?.let { rate ->
                    if (it.number == correctAnswerNumber) {
                        result += rate
                    } else {
                        result -= rate
                    }
                }
            }
        }
        if (result < 0) result = 0
        return result to correctCount
    }

    private fun getSetGameStatusDomainModel(
        id: String,
        result: Int,
        round: Int,
        totalRounds: Int
    ): SetGameStatusDomainModel {
        val isFinished = totalRounds == round

        return SetGameStatusDomainModel(
            id = id,
            userScore = _startUserScore + result,
            level = if (isFinished) null else round + 1,
            isFinished = isFinished
        )
    }
}