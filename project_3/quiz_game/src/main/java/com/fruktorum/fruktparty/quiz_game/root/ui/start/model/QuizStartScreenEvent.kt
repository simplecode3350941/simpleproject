package com.fruktorum.fruktparty.quiz_game.root.ui.start.model

internal sealed class QuizStartScreenEvent {

    class Start(
        val id: String,
        val round: Int?,
        val userScore: Int
    ) : QuizStartScreenEvent()

    class ClickOnStart(
        val id: String,
        val round: Int,
        val userScore: Int
    ) : QuizStartScreenEvent()
}