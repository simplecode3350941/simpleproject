package com.fruktorum.fruktparty.quiz_game.root.ui.start.model

internal sealed class QuizStartScreenState {

    data class Loading(
        val round: Int?
    ) : QuizStartScreenState()

    data class Content(
        val id: String,
        val round: Int,
        val userScore: Int,
        val title: String,
        val description: String,
        val buttonText: String
    ) : QuizStartScreenState()

    data class ContentFinishedGame(
        val userScore: Int
    ) : QuizStartScreenState()

    object Error : QuizStartScreenState()
}