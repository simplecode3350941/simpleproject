package com.fruktorum.fruktparty.quiz_game.root.ui.result.model

internal sealed class ResultScreenState {

    object Loading : ResultScreenState()

    data class Content(
        val round: Int,
        val totalRounds: Int,
        val result: Int,
        val correctCount: Int,
        val totalCount: Int
    ) : ResultScreenState() {
        val isLastRound = round == totalRounds
    }

    object Error : ResultScreenState()
}