package com.fruktorum.fruktparty.quiz_game.root.ui.elements

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.WhiteFFFFFF
import com.fruktorum.fruktparty.quiz_game.R

@Composable
internal fun RoundCircle(
    round: Int,
    modifier: Modifier = Modifier
) = Column(
    modifier = modifier
        .background(
            color = WhiteFFFFFF.copy(alpha = 0.75f),
            CircleShape
        )
        .clip(
            shape = CircleShape
        ),
    horizontalAlignment = Alignment.CenterHorizontally
) {
    Text(
        text = round.toString(),
        modifier = Modifier.padding(top = 7.dp),
        style = MaterialTheme.typography.h5,
        color = Color.Black
    )

    Text(
        text = stringResource(R.string.quiz_screen_round_text),
        modifier = Modifier.offset(y = (-4).dp),
        style = MaterialTheme.typography.overline,
        color = Color.Black
    )
}

@Preview
@Composable
private fun RoundCirclePreview() {
    FruktPartyTheme {
        RoundCircle(
            round = 1,
            modifier = Modifier.size(62.dp)
        )
    }
}