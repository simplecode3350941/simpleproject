package com.fruktorum.fruktparty.quiz_game.root.ui.navigation

import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import com.fruktorum.fruktparty.quiz_game.root.ui.answers.AnswersScreen
import com.fruktorum.fruktparty.quiz_game.root.ui.answers.AnswersScreenViewModel
import com.fruktorum.fruktparty.quiz_game.root.ui.quiz.QuizScreen
import com.fruktorum.fruktparty.quiz_game.root.ui.quiz.QuizScreenViewModel
import com.fruktorum.fruktparty.quiz_game.root.ui.result.ResultScreen
import com.fruktorum.fruktparty.quiz_game.root.ui.result.ResultScreenViewModel
import com.fruktorum.fruktparty.quiz_game.root.ui.start.QuizStartScreen
import com.fruktorum.fruktparty.quiz_game.root.ui.start.QuizStartScreenViewModel

fun NavGraphBuilder.quizGraph(navController: NavController) {

    navigation(startDestination = QuizRoutes.start, route = QuizRoutes.root) {

        composable(
            route = QuizRoutes.start +
                    "/{id}" +
                    "/{round}" +
                    "/{userScore}"
        ) { backStackEntry ->
            val viewModel = hiltViewModel<QuizStartScreenViewModel>()

            val id = backStackEntry.arguments?.getString("id")
                ?: throw IllegalStateException()

            val round = backStackEntry.arguments?.getString("round")?.toInt()

            val userScore = backStackEntry.arguments?.getString("userScore")?.toInt()
                ?: throw IllegalStateException()

            QuizStartScreen(
                navController = navController,
                viewModel = viewModel,
                id = id,
                round = round,
                userScore = userScore
            )
        }

        composable(
            route = QuizRoutes.quiz +
                    "/{id}" +
                    "/{round}" +
                    "/{userScore}"
        ) { backStackEntry ->
            val viewModel = hiltViewModel<QuizScreenViewModel>()

            val id = backStackEntry.arguments?.getString("id")
                ?: throw IllegalStateException()

            val round = backStackEntry.arguments?.getString("round")?.toInt()
                ?: throw IllegalStateException()

            val startUserScore = backStackEntry.arguments?.getString("userScore")?.toInt()
                ?: throw IllegalStateException()

            QuizScreen(
                navController = navController,
                viewModel = viewModel,
                id = id,
                round = round,
                startUserScore = startUserScore
            )
        }

        composable(
            route = QuizRoutes.result +
                    "/{id}" +
                    "/{result}" +
                    "/{correctCount}" +
                    "/{totalCount}" +
                    "/{round}" +
                    "/{totalRounds}" +
                    "/{userScore}"
        ) { backStackEntry ->
            val viewModel = hiltViewModel<ResultScreenViewModel>()

            val id = backStackEntry.arguments?.getString("id")
                ?: throw IllegalStateException()

            val result = backStackEntry.arguments?.getString("result")?.toInt()
                ?: throw IllegalStateException()

            val correctCount = backStackEntry.arguments?.getString("correctCount")?.toInt()
                ?: throw IllegalStateException()

            val totalCount = backStackEntry.arguments?.getString("totalCount")?.toInt()
                ?: throw IllegalStateException()

            val round = backStackEntry.arguments?.getString("round")?.toInt()
                ?: throw IllegalStateException()

            val totalRounds = backStackEntry.arguments?.getString("totalRounds")?.toInt()
                ?: throw IllegalStateException()

            val userScore = backStackEntry.arguments?.getString("userScore")?.toInt()
                ?: throw IllegalStateException()

            ResultScreen(
                navController = navController,
                id = id,
                result = result,
                correctCount = correctCount,
                totalCount = totalCount,
                round = round,
                totalRounds = totalRounds,
                userScore = userScore,
                viewModel = viewModel
            )
        }

        composable(
            route = QuizRoutes.answers +
                    "/{id}" +
                    "/{round}"
        ) { backStackEntry ->
            val viewModel = hiltViewModel<AnswersScreenViewModel>()

            val id = backStackEntry.arguments?.getString("id")
                ?: throw IllegalStateException()

            val round = backStackEntry.arguments?.getString("round")?.toInt()
                ?: throw IllegalStateException()

            AnswersScreen(
                navController = navController,
                viewModel = viewModel,
                id = id,
                round = round
            )
        }
    }
}

fun getStartQuizRoute(id: String, round: Int?, userScore: Int?): String {
    return QuizRoutes.start +
            "/$id" +
            "/$round" +
            "/$userScore"
}

internal fun getQuizRoute(id: String, round: Int, userScore: Int): String {
    return QuizRoutes.quiz +
            "/$id" +
            "/$round" +
            "/$userScore"
}

internal fun getQuizResultRoute(
    id: String,
    result: Int,
    correctCount: Int,
    totalCount: Int,
    round: Int,
    totalRounds: Int,
    userScore: Int
): String {
    return QuizRoutes.result +
            "/$id" +
            "/$result" +
            "/$correctCount" +
            "/$totalCount" +
            "/$round" +
            "/$totalRounds" +
            "/$userScore"
}

internal fun getQuizCorrectAnswersRoute(id: String, round: Int): String {
    return QuizRoutes.answers +
            "/$id" +
            "/$round"
}