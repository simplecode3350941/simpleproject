package com.fruktorum.fruktparty.quiz_game.root.ui.quiz.model

internal sealed class QuizScreenAction {

    class NavigateTo(
        val route: String
    ) : QuizScreenAction()

    class NavigateToWithClearBackstack(
        val route: String
    ) : QuizScreenAction()
}