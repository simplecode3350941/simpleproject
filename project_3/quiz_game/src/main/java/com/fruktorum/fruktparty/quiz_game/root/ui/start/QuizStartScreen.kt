package com.fruktorum.fruktparty.quiz_game.root.ui.start

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.fruktorum.core.ui.composables.buttons.TopStartBottomEndRoundedButton
import com.fruktorum.core.ui.composables.loading.LoadingTransparentLayer
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFFFFFF
import com.fruktorum.fruktparty.quiz_game.R
import com.fruktorum.fruktparty.quiz_game.root.ui.elements.CloseButton
import com.fruktorum.fruktparty.quiz_game.root.ui.elements.ScoresCircle
import com.fruktorum.fruktparty.quiz_game.root.ui.start.model.QuizStartScreenAction
import com.fruktorum.fruktparty.quiz_game.root.ui.start.model.QuizStartScreenEvent
import com.fruktorum.fruktparty.quiz_game.root.ui.start.model.QuizStartScreenState

@Composable
internal fun QuizStartScreen(
    navController: NavController,
    viewModel: QuizStartScreenViewModel,
    id: String,
    round: Int?,
    userScore: Int,
    modifier: Modifier = Modifier
) {
    val state = viewModel.state.collectAsState(
        initial = QuizStartScreenState.Loading(round)
    ).value

    when (state) {
        is QuizStartScreenState.Loading -> {
            QuizStartScreenLoading(
                modifier = modifier
                    .fillMaxSize(),
                state = state,
                onClickClose = {
                    navController.popBackStack()
                }
            )
        }
        is QuizStartScreenState.Content -> {
            QuizStartScreen(
                modifier = modifier
                    .fillMaxSize(),
                state = state,
                onClickStart = {
                    viewModel.onEvent(
                        QuizStartScreenEvent.ClickOnStart(
                            id = state.id,
                            round = state.round,
                            userScore = state.userScore
                        )
                    )
                },
                onClickClose = {
                    navController.popBackStack()
                }
            )
        }
        is QuizStartScreenState.ContentFinishedGame -> {
            QuizStartScreenFinishedGame(
                state = state,
                modifier = modifier
                    .fillMaxSize(),
                onClickClose = {
                    navController.popBackStack()
                }
            )
        }
        QuizStartScreenState.Error -> {
            ///TODO
        }
    }

    LaunchedEffect(true) {
        viewModel.onEvent(QuizStartScreenEvent.Start(id, round, userScore))

        viewModel.action.collect { action ->
            when (action) {
                is QuizStartScreenAction.NavigateTo -> {
                    navController.navigate(
                        route = action.route
                    )
                }
            }
        }
    }
}

@Composable
private fun QuizStartScreenLoading(
    modifier: Modifier = Modifier,
    state: QuizStartScreenState.Loading,
    onClickClose: () -> Unit
) = Box(
    modifier = modifier
) {
    Image(
        painter = painterResource(
            when (state.round) {
                //TODO get from content
                1 -> R.drawable.bg_quiz_first_round_start
                2 -> R.drawable.bg_quiz_second_round_start
                else -> R.drawable.bg_quiz_third_round_start
            }
        ),
        contentDescription = null,
        modifier = Modifier
            .fillMaxSize(),
        contentScale = ContentScale.Crop
    )

    LoadingTransparentLayer()

    CloseButton(
        modifier = Modifier
            .statusBarsPadding()
            .align(Alignment.TopEnd)
            .padding(10.dp),
        onClick = onClickClose
    )
}

@Composable
private fun QuizStartScreen(
    modifier: Modifier = Modifier,
    state: QuizStartScreenState.Content,
    onClickStart: () -> Unit,
    onClickClose: () -> Unit
) = Box(
    modifier = modifier
) {
    Image(
        painter = painterResource(
            when (state.round) {
                //TODO get from content
                1 -> R.drawable.bg_quiz_first_round_start
                2 -> R.drawable.bg_quiz_second_round_start
                else -> R.drawable.bg_quiz_third_round_start
            }
        ),
        contentDescription = null,
        modifier = Modifier
            .fillMaxSize(),
        contentScale = ContentScale.Crop
    )

    Column(
        modifier = Modifier
            .fillMaxSize()
            .statusBarsPadding()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.size(86.dp))

        Text(
            text = state.title,
            modifier = Modifier,
            style = MaterialTheme.typography.h1,
            color = WhiteFFFFFF,
            textDecoration = TextDecoration.Underline
        )

        Spacer(modifier = Modifier.size(30.dp))

        Text(
            text = state.description.replace("\\n", "\n"),
            modifier = Modifier,
            style = MaterialTheme.typography.h6,
            color = WhiteFFFFFF,
            textAlign = TextAlign.Center
        )

        Spacer(modifier = Modifier.size(44.dp))

        TopStartBottomEndRoundedButton(
            text = state.buttonText,
            onClick = onClickStart,
            backgroundGradient = RedFF512FPinkDD2476,
            modifier = Modifier
                .widthIn(min = dimensionResource(com.fruktorum.core.R.dimen.size_104_dp))
        )
    }

    CloseButton(
        modifier = Modifier
            .statusBarsPadding()
            .align(Alignment.TopEnd)
            .padding(10.dp),
        onClick = onClickClose
    )
}

@Composable
internal fun QuizStartScreenFinishedGame(
    state: QuizStartScreenState.ContentFinishedGame,
    modifier: Modifier = Modifier,
    onClickClose: () -> Unit
) = Box(
    modifier = modifier
) {
    Image(
        painter = painterResource(R.drawable.bg_quiz_third_round),
        contentDescription = null,
        modifier = Modifier
            .fillMaxSize(),
        contentScale = ContentScale.Crop
    )

    CloseButton(
        modifier = Modifier
            .statusBarsPadding()
            .align(Alignment.TopEnd)
            .padding(10.dp),
        onClick = onClickClose
    )

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .statusBarsPadding()
            .padding(43.dp)
            .align(Alignment.Center),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(R.string.quiz_start_screen_finished_game_title),
            modifier = Modifier,
            style = MaterialTheme.typography.h2,
            color = WhiteFFFFFF,
            textDecoration = TextDecoration.Underline
        )

        Spacer(modifier = Modifier.size(28.dp))

        Text(
            text = stringResource(R.string.quiz_start_screen_finished_game_description),
            modifier = Modifier,
            style = MaterialTheme.typography.body1,
            color = WhiteFFFFFF,
            textAlign = TextAlign.Center
        )

        Spacer(modifier = Modifier.size(32.dp))

        ScoresCircle(
            value = state.userScore,
            modifier = Modifier.fillMaxWidth(0.4f)
        )

        Spacer(modifier = Modifier.size(40.dp))

        TopStartBottomEndRoundedButton(
            text = stringResource(R.string.quiz_start_screen_finished_game_quit_button_text),
            onClick = onClickClose,
            backgroundGradient = RedFF512FPinkDD2476,
            modifier = Modifier
                .fillMaxWidth()
        )
    }
}

@Preview
@Composable
private fun QuizStartScreenPreview() {
    FruktPartyTheme {
        QuizStartScreen(
            state = QuizStartScreenState.Content(
                id = "1",
                round = 1,
                userScore = 25,
                title = "1 РАУНД",
                description = "У вас есть 30 секунд для ответа.\nЗа каждый правильный ответ дается 1 балл.",
                buttonText = "Старт"
            ),
            onClickClose = {},
            onClickStart = {}
        )
    }
}