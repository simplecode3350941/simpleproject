package com.fruktorum.fruktparty.quiz_game.root.ui.result.model

internal sealed class ResultScreenAction {

    class NavigateTo(
        val route: String
    ) : ResultScreenAction()

    class NavigateToWithClearBackstack(
        val route: String
    ) : ResultScreenAction()
}