package com.fruktorum.fruktparty.quiz_game.root.ui.result.model

internal sealed class ResultScreenEvent {

    data class Start(
        val result: Int,
        val correctCount: Int,
        val totalCount: Int,
        val round: Int,
        val totalRounds: Int
    ) : ResultScreenEvent()

    data class ClickCorrectAnswers(
        val id: String,
        val round: Int
    ) : ResultScreenEvent()

    data class ClickNextRound(
        val id: String,
        val round: Int,
        val userScore: Int
    ) : ResultScreenEvent()
}