package com.fruktorum.fruktparty.quiz_game.root.ui.answers.model

internal sealed class AnswersScreenEvent {

    data class Start(
        val id: String,
        val round: Int
    ) : AnswersScreenEvent()
}