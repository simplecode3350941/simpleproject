package com.fruktorum.fruktparty.quiz_game.root.ui.elements

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFFFFFF
import com.fruktorum.domain.game.quiz.model.Answer
import com.fruktorum.domain.game.quiz.model.Question

@Composable
internal fun QuestionBlock(
    modifier: Modifier = Modifier,
    question: Question,
    totalCount: Int,
    chosenAnswer: Answer? = null,
    chosenRates: Map<Answer, Int?>? = null,
    onClickAnswer: (Answer) -> Unit,
    onClickRate: (Answer, Int) -> Unit
) = Box(
    modifier = modifier
) {
    Column(
        modifier = Modifier
            .widthIn(min = 60.dp)
            .align(Alignment.TopEnd)
            .padding(end = 24.dp)
            .background(
                color = WhiteFFFFFF.copy(alpha = 0.8f),
                RoundedCornerShape(
                    topStartPercent = 100,
                    topEndPercent = 100
                )
            ),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "${question.number}/$totalCount",
            modifier = Modifier
                .padding(
                    start = 12.dp,
                    top = 12.dp,
                    end = 12.dp
                ),
            style = MaterialTheme.typography.caption,
            color = Color.Black
        )
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 28.dp)
            .background(
                color = WhiteFFFFFF.copy(alpha = 0.8f),
                RoundedCornerShape(10.dp)
            )
            .clip(
                shape = RoundedCornerShape(10.dp)
            )
            .padding(
                all = 24.dp
            )
    ) {
        Spacer(modifier = Modifier.size(8.dp))

        Text(
            text = question.title,
            modifier = Modifier,
            style = MaterialTheme.typography.body1,
            color = Color.Black
        )

        Spacer(modifier = Modifier.size(32.dp))

        LazyColumn {
            items(question.answers) {
                Answer(
                    answer = it,
                    modifier = Modifier
                        .padding(
                            vertical = 8.dp
                        ),
                    isChosen = it == chosenAnswer,
                    isChosenRate = chosenRates?.get(it),
                    onClick = {
                        onClickAnswer(it)
                    },
                    onClickRate = { rate ->
                        onClickRate(it, rate)
                    }
                )
            }
        }
    }
}

@Composable
private fun Answer(
    answer: Answer,
    modifier: Modifier = Modifier,
    isChosen: Boolean,
    isChosenRate: Int?,
    onClick: () -> Unit,
    onClickRate: (Int) -> Unit
) = Box(
    modifier = modifier
        .fillMaxWidth()
        .heightIn(min = 52.dp)
        .border(
            width = 2.dp,
            brush =
            if (isChosen) {
                RedFF512FPinkDD2476
            } else {
                Brush.horizontalGradient(
                    listOf(
                        Color(0xFF74A0CD),
                        Color(0xFFFDAFFF)
                    )
                )
            },
            shape = RoundedCornerShape(10.dp)
        )
        .clip(RoundedCornerShape(10.dp))
        .clickable {
            onClick()
        },
    contentAlignment = Alignment.CenterStart
) {
    Row(modifier = Modifier.fillMaxWidth()) {
        Text(
            text = answer.text,
            modifier = Modifier
                .weight(1f)
                .padding(16.dp),
            style = MaterialTheme.typography.body2,
            color = Color.Black
        )

        answer.rates?.forEach {
            Rate(
                modifier = Modifier
                    .padding(vertical = 14.dp)
                    .padding(end = 16.dp),
                rate = it,
                isChosen = isChosenRate == it,
                onClick = {
                    onClickRate(it)
                }
            )
        }
    }
}

@Composable
private fun Rate(
    modifier: Modifier,
    rate: Int,
    isChosen: Boolean,
    onClick: () -> Unit
) = Box(
    modifier = modifier
        .sizeIn(minWidth = 24.dp, minHeight = 24.dp)
        .background(
            brush = if (isChosen) RedFF512FPinkDD2476
            else SolidColor(Color(0xFFC1D0FF)),
            shape = RoundedCornerShape(4.dp)
        )
        .clip(RoundedCornerShape(4.dp))
        .clickable(onClick = onClick),
    contentAlignment = Alignment.Center
) {
    Text(
        text = rate.toString(),
        color = WhiteFFFFFF,
        style = MaterialTheme.typography.caption
    )
}

@Preview
@Composable
private fun QuestionBlockPreview() {
    FruktPartyTheme {
        QuestionBlock(
            modifier = Modifier
                .fillMaxWidth(),
            question = Question(
                number = 21,
                title = "Вопрос 1",
                answers = listOf(
                    Answer(
                        number = 1,
                        text = "Ответ 1",
                        isCorrect = false,
                        rates = null
                    ),
                    Answer(
                        number = 2,
                        text = "Ответ 2",
                        isCorrect = false,
                        rates = null
                    ),
                    Answer(
                        number = 3,
                        text = "Ответ 3",
                        isCorrect = true,
                        rates = null
                    ),
                    Answer(
                        number = 4,
                        text = "Ответ 4",
                        isCorrect = false,
                        rates = listOf(1, 2)
                    )
                )
            ),
            totalCount = 25,
            chosenAnswer = Answer(
                number = 3,
                text = "Ответ 3",
                isCorrect = true,
                rates = null
            ),
            chosenRates = mapOf(
                Answer(
                    number = 4,
                    text = "Ответ 4",
                    isCorrect = false,
                    rates = listOf(1, 2)
                ) to 2
            ),
            onClickAnswer = {},
            onClickRate = { answer, rate -> }
        )
    }
}