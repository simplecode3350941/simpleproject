package com.fruktorum.fruktparty.quiz_game.root.ui.start

import com.fruktorum.core.ui.vm.MVIViewModel
import com.fruktorum.domain.game.quiz.usecase.GetQuizUseCase
import com.fruktorum.fruktparty.quiz_game.root.ui.navigation.getQuizRoute
import com.fruktorum.fruktparty.quiz_game.root.ui.start.model.QuizStartScreenAction
import com.fruktorum.fruktparty.quiz_game.root.ui.start.model.QuizStartScreenEvent
import com.fruktorum.fruktparty.quiz_game.root.ui.start.model.QuizStartScreenState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
internal class QuizStartScreenViewModel
@Inject
constructor(
    private val getQuizContent: GetQuizUseCase
) : MVIViewModel<
        QuizStartScreenState,
        Nothing,
        QuizStartScreenEvent,
        QuizStartScreenAction
        >() {

    override fun onEvent(event: QuizStartScreenEvent) {
        when (event) {
            is QuizStartScreenEvent.Start -> handleStart(event.id, event.round, event.userScore)
            is QuizStartScreenEvent.ClickOnStart ->
                handleClickOnStart(event.id, event.round, event.userScore)
        }
    }

    private fun handleStart(
        id: String,
        roundNumber: Int?,
        userScore: Int
    ) = launchJob(
        exceptionBlock = {
            setState(
                QuizStartScreenState.Error
            )
        }
    ) {
        if (roundNumber != null) {
            val quiz = getQuizContent(
                id = id,
                forceLoad = false
            )

            quiz?.let {
                val round = quiz.rounds.find {
                    it.number == roundNumber
                } ?: throw IllegalStateException()

                setState(
                    QuizStartScreenState.Content(
                        id = id,
                        round = roundNumber,
                        userScore = userScore,
                        title = round.title,
                        description = round.description,
                        buttonText = round.buttonText
                    )
                )
            } ?: throw IllegalStateException()
        } else {
            setState(
                QuizStartScreenState.ContentFinishedGame(userScore)
            )
        }
    }

    private fun handleClickOnStart(
        id: String,
        round: Int,
        userScore: Int
    ) {
        emitAction(
            QuizStartScreenAction.NavigateTo(
                route = getQuizRoute(
                    id = id,
                    round = round,
                    userScore = userScore
                )
            )
        )
    }
}