package com.fruktorum.fruktparty.quiz_game.root.ui.answers.model


internal sealed class AnswersScreenState {

    object Loading : AnswersScreenState()

    data class Content(
        val answers: Map<String, String>,
        val round: Int
    ) : AnswersScreenState()

    object Error : AnswersScreenState()
}