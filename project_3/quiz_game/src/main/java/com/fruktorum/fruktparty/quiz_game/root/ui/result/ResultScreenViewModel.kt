package com.fruktorum.fruktparty.quiz_game.root.ui.result

import com.fruktorum.core.ui.vm.MVIViewModel
import com.fruktorum.fruktparty.quiz_game.root.ui.navigation.getQuizCorrectAnswersRoute
import com.fruktorum.fruktparty.quiz_game.root.ui.navigation.getStartQuizRoute
import com.fruktorum.fruktparty.quiz_game.root.ui.result.model.ResultScreenAction
import com.fruktorum.fruktparty.quiz_game.root.ui.result.model.ResultScreenEvent
import com.fruktorum.fruktparty.quiz_game.root.ui.result.model.ResultScreenState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
internal class ResultScreenViewModel
@Inject
constructor(

) : MVIViewModel<
        ResultScreenState,
        Nothing,
        ResultScreenEvent,
        ResultScreenAction>() {

    override fun onEvent(event: ResultScreenEvent) {
        when (event) {
            is ResultScreenEvent.Start -> handleStart(
                event.result,
                event.totalCount,
                event.correctCount,
                event.round,
                event.totalRounds
            )
            is ResultScreenEvent.ClickCorrectAnswers ->
                handleClickCorrectAnswers(event.id, event.round)
            is ResultScreenEvent.ClickNextRound ->
                handleClickNextRound(event.id, event.round, event.userScore)
        }
    }

    private fun handleStart(
        result: Int,
        totalCount: Int,
        correctCount: Int,
        round: Int,
        totalRounds: Int
    ) {
        setState(
            ResultScreenState.Content(
                round = round,
                totalRounds = totalRounds,
                result = result,
                correctCount = correctCount,
                totalCount = totalCount
            )
        )
    }

    private fun handleClickNextRound(id: String, round: Int, userScore: Int) {
        val nextRound = round + 1
        emitAction(
            ResultScreenAction.NavigateToWithClearBackstack(
                route = getStartQuizRoute(id, nextRound, userScore)
            )
        )
    }

    private fun handleClickCorrectAnswers(id: String, round: Int) {
        emitAction(
            ResultScreenAction.NavigateTo(
                route = getQuizCorrectAnswersRoute(id, round)
            )
        )
    }
}