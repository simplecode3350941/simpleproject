package com.fruktorum.fruktparty.quiz_game.root.ui.elements

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.GrayD8E1FF
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFFFFFF
import com.fruktorum.fruktparty.quiz_game.R

@Composable
internal fun TimerRow(
    modifier: Modifier = Modifier,
    state: Float
) = Row(
    modifier = modifier,
    verticalAlignment = Alignment.CenterVertically
) {
    Icon(
        painter = painterResource(R.drawable.ic_timer),
        contentDescription = null,
        tint = GrayD8E1FF
    )

    Spacer(modifier = Modifier.size(7.dp))

    Box(
        modifier = Modifier
            .height(8.dp)
            .weight(1f)
            .background(
                color = WhiteFFFFFF.copy(alpha = 0.75f),
                CircleShape
            )
            .clip(
                shape = CircleShape
            )
    ) {
        Box(
            modifier = Modifier
                .height(8.dp)
                .fillMaxWidth(state)
                .background(
                    brush = RedFF512FPinkDD2476,
                    CircleShape
                )
                .clip(
                    shape = CircleShape
                )
        )
    }
}

@Preview
@Composable
private fun TimerRowPreview() {
    FruktPartyTheme {
        Box(Modifier.background(Color.Blue)) {
            TimerRow(
                modifier = Modifier.fillMaxWidth(),
                state = 0.5f
            )
        }
    }
}