package com.fruktorum.fruktparty.quiz_game.root.ui.navigation

object QuizRoutes {
    const val root = "root"
    const val start = "$root/start"
    const val quiz = "$root/quiz"
    const val result = "$root/result"
    const val answers = "$root/answers"
}