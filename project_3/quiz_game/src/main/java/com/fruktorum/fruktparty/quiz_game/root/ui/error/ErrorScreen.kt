package com.fruktorum.fruktparty.quiz_game.root.ui.error

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.fruktorum.core.ui.composables.buttons.TopStartBottomEndRoundedButton
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFFFFFF
import com.fruktorum.fruktparty.quiz_game.R
import com.fruktorum.fruktparty.quiz_game.root.ui.elements.CloseButton

@Composable
internal fun ErrorScreen(
    modifier: Modifier = Modifier,
    round: Int,
    onClickClose: () -> Unit,
    onClickTryAgain: () -> Unit
) = Box(
    modifier = modifier
) {
    Image(
        painter = painterResource(
            when (round) {
                //TODO get from content
                1 -> R.drawable.bg_quiz_first_round
                2 -> R.drawable.bg_quiz_second_round
                else -> R.drawable.bg_quiz_third_round
            }
        ),
        contentDescription = null,
        modifier = Modifier
            .fillMaxSize(),
        contentScale = ContentScale.Crop
    )

    CloseButton(
        modifier = Modifier
            .statusBarsPadding()
            .align(Alignment.TopEnd)
            .padding(10.dp),
        onClick = onClickClose
    )

    Column(
        modifier = Modifier
            .fillMaxSize()
            .statusBarsPadding()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.size(86.dp))

        Text(
            text = stringResource(R.string.quiz_error_screen_title),
            modifier = Modifier,
            style = MaterialTheme.typography.h2,
            color = WhiteFFFFFF,
            textDecoration = TextDecoration.Underline
        )

        Spacer(modifier = Modifier.size(30.dp))

        Text(
            text = stringResource(R.string.quiz_error_screen_subtitle),
            modifier = Modifier,
            style = MaterialTheme.typography.body1,
            color = WhiteFFFFFF,
            textAlign = TextAlign.Center
        )

        Spacer(modifier = Modifier.weight(1f))

        TopStartBottomEndRoundedButton(
            text = stringResource(R.string.quiz_error_screen_try_again_btn_text),
            onClick = onClickTryAgain,
            backgroundGradient = RedFF512FPinkDD2476,
            modifier = Modifier
                .fillMaxWidth()
        )

        Spacer(modifier = Modifier.weight(1f))
    }
}

@Preview
@Composable
private fun ErrorScreenPreview() {
    FruktPartyTheme {
        ErrorScreen(
            modifier = Modifier,
            round = 1,
            onClickClose = {},
            onClickTryAgain = {}
        )
    }
}