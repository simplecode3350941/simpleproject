package com.fruktorum.fruktparty.quiz_game.root.ui.quiz.model

internal data class AnsweredQuestion(
    val number: Int,
    val answers: List<Answer>,
    val chosenAnswerNumber: Int?
)

internal data class Answer(
    val number: Int,
    val rate: Int?
)