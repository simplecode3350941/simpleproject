package com.fruktorum.fruktparty.quiz_game.root.ui.quiz.model

import com.fruktorum.domain.game.common.model.SetGameStatusDomainModel

data class SetGameStatusUiModel(
    val id: String,
    val userScore: Int,
    val level: Int,
    val isFinished: Boolean
)

fun SetGameStatusUiModel.toDomain(): SetGameStatusDomainModel {
    return SetGameStatusDomainModel(
        id = this.id,
        userScore = this.userScore,
        level = this.level,
        isFinished = this.isFinished
    )
}