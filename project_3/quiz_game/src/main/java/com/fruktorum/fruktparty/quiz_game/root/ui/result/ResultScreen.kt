package com.fruktorum.fruktparty.quiz_game.root.ui.result

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.fruktorum.core.ui.composables.buttons.TopStartBottomEndRoundedButton
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.PurpleA226CEViolet6626CE
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFFFFFF
import com.fruktorum.core.ui.theme.YellowFFBE15OrangeFF9315
import com.fruktorum.fruktparty.quiz_game.R
import com.fruktorum.fruktparty.quiz_game.root.ui.elements.CloseButton
import com.fruktorum.fruktparty.quiz_game.root.ui.elements.ScoresCircle
import com.fruktorum.fruktparty.quiz_game.root.ui.result.model.ResultScreenAction
import com.fruktorum.fruktparty.quiz_game.root.ui.result.model.ResultScreenEvent
import com.fruktorum.fruktparty.quiz_game.root.ui.result.model.ResultScreenState

@Composable
internal fun ResultScreen(
    navController: NavController,
    viewModel: ResultScreenViewModel,
    id: String,
    result: Int,
    correctCount: Int,
    totalCount: Int,
    round: Int,
    totalRounds: Int,
    userScore: Int,
    modifier: Modifier = Modifier
) {
    val state = viewModel.state.collectAsState(initial = ResultScreenState.Loading)

    when (val stateValue = state.value) {
        ResultScreenState.Loading -> {
            //TODO
        }
        is ResultScreenState.Content -> {
            ResultScreen(
                content = stateValue,
                modifier = modifier,
                onClickClose = {
                    navController.popBackStack()
                },
                onClickCorrectAnswers = {
                    viewModel.onEvent(
                        ResultScreenEvent.ClickCorrectAnswers(id, round)
                    )
                },
                onClickNextRound = {
                    viewModel.onEvent(
                        ResultScreenEvent.ClickNextRound(id, round, userScore)
                    )
                },
                onClickEndGame = {
                    navController.popBackStack()
                }
            )
        }
        ResultScreenState.Error -> {
            //TODO
        }
    }

    LaunchedEffect(true) {
        viewModel.onEvent(
            ResultScreenEvent.Start(
                result = result,
                correctCount = correctCount,
                totalCount = totalCount,
                round = round,
                totalRounds = totalRounds
            )
        )

        viewModel.action.collect { action ->
            when (action) {
                is ResultScreenAction.NavigateTo -> {
                    navController.navigate(
                        route = action.route
                    )
                }
                is ResultScreenAction.NavigateToWithClearBackstack -> {
                    navController.navigate(
                        route = action.route
                    ) {
                        popUpTo(route = "GAMES")
                    }
                }
            }
        }
    }
}

@Composable
internal fun ResultScreen(
    content: ResultScreenState.Content,
    modifier: Modifier = Modifier,
    onClickClose: () -> Unit,
    onClickCorrectAnswers: () -> Unit,
    onClickNextRound: () -> Unit,
    onClickEndGame: () -> Unit
) = Box(
    modifier = modifier
) {
    Image(
        painter = painterResource(
            when (content.round) {
                //TODO get from content
                1 -> R.drawable.bg_quiz_first_round
                2 -> R.drawable.bg_quiz_second_round
                else -> R.drawable.bg_quiz_third_round
            }
        ),
        contentDescription = null,
        modifier = Modifier
            .fillMaxSize(),
        contentScale = ContentScale.Crop
    )

    CloseButton(
        modifier = Modifier
            .statusBarsPadding()
            .align(Alignment.TopEnd)
            .padding(10.dp),
        onClick = onClickClose
    )

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .statusBarsPadding()
            .padding(43.dp)
            .align(Alignment.Center),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(
                when (content.round) {
                    //TODO get from content
                    1 -> R.string.quiz_result_screen_first_round_title
                    2 -> R.string.quiz_result_screen_second_round_title
                    else -> R.string.quiz_result_screen_third_round_title
                }
            ),
            modifier = Modifier,
            style = MaterialTheme.typography.h2,
            color = WhiteFFFFFF,
            textDecoration = TextDecoration.Underline
        )

        Spacer(modifier = Modifier.size(28.dp))

        Text(
            text = stringResource(R.string.quiz_result_screen_description),
            modifier = Modifier,
            style = MaterialTheme.typography.body1,
            color = WhiteFFFFFF,
            textAlign = TextAlign.Center
        )

        Text(
            text = "${content.correctCount}/${content.totalCount}",
            modifier = Modifier
                .padding(top = 16.dp),
            style = MaterialTheme.typography.body1,
            color = WhiteFFFFFF,
            textAlign = TextAlign.Center
        )

        Spacer(modifier = Modifier.size(32.dp))

        ScoresCircle(
            value = content.result,
            modifier = Modifier.fillMaxWidth(0.4f)
        )

        Spacer(modifier = Modifier.size(40.dp))

        TopStartBottomEndRoundedButton(
            text = stringResource(R.string.quiz_result_screen_correct_answers_btn_text),
            onClick = onClickCorrectAnswers,
            backgroundGradient = RedFF512FPinkDD2476,
            modifier = Modifier
                .fillMaxWidth()
        )

        Spacer(modifier = Modifier.size(16.dp))

        TopStartBottomEndRoundedButton(
            text = stringResource(
                if (content.isLastRound) {
                    R.string.quiz_result_screen_end_game_btn_text
                } else {
                    R.string.quiz_result_screen_next_round_btn_text
                }
            ),
            onClick = if (content.isLastRound) {
                onClickEndGame
            } else {
                onClickNextRound
            },
            backgroundGradient = if (content.isLastRound) {
                YellowFFBE15OrangeFF9315
            } else {
                PurpleA226CEViolet6626CE
            },
            modifier = Modifier
                .fillMaxWidth()
        )
    }
}

@Preview(
    showBackground = true
)
@Composable
private fun ResultScreenPreview() {
    FruktPartyTheme {
        ResultScreen(
            content = ResultScreenState.Content(
                round = 1,
                totalRounds = 3,
                result = 20,
                correctCount = 20,
                totalCount = 25
            ),
            onClickClose = {},
            onClickCorrectAnswers = {},
            onClickNextRound = {},
            onClickEndGame = {}
        )
    }
}