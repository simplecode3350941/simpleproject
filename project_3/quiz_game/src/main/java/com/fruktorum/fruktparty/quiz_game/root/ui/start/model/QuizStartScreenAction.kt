package com.fruktorum.fruktparty.quiz_game.root.ui.start.model

internal sealed class QuizStartScreenAction {
    class NavigateTo(
        val route: String
    ) : QuizStartScreenAction()
}