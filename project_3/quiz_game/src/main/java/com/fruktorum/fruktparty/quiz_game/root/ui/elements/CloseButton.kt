package com.fruktorum.fruktparty.quiz_game.root.ui.elements

import androidx.compose.material.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import com.fruktorum.core.ui.theme.WhiteFFFFFF
import com.fruktorum.fruktparty.quiz_game.R

@Composable
internal fun CloseButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit
) = IconButton(
    onClick = onClick,
    modifier = modifier
) {
    Icon(
        painter = painterResource(R.drawable.ic_close_round_20),
        contentDescription = null,
        tint = WhiteFFFFFF.copy(alpha = 0.8f)
    )
}