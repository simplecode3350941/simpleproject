package com.fruktorum.fruktparty.quiz_game.root.ui.answers

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.fruktorum.core.ui.composables.buttons.BackRoundButton
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFFFFFF
import com.fruktorum.fruktparty.quiz_game.R
import com.fruktorum.fruktparty.quiz_game.root.ui.answers.model.AnswersScreenEvent
import com.fruktorum.fruktparty.quiz_game.root.ui.answers.model.AnswersScreenState

@Composable
internal fun AnswersScreen(
    navController: NavController,
    viewModel: AnswersScreenViewModel,
    modifier: Modifier = Modifier,
    id: String,
    round: Int
) {
    val state = viewModel.state.collectAsState(initial = AnswersScreenState.Loading)

    when (val stateValue = state.value) {
        AnswersScreenState.Loading -> {
            //TODO
        }
        is AnswersScreenState.Content -> {
            AnswersScreen(
                content = stateValue,
                modifier = modifier,
                onClickBack = {
                    navController.popBackStack()
                }
            )
        }
        AnswersScreenState.Error -> {
            //TODO
        }
    }

    LaunchedEffect(true) {
        viewModel.onEvent(AnswersScreenEvent.Start(id, round))
    }
}

@OptIn(ExperimentalTextApi::class)
@Composable
private fun AnswersScreen(
    modifier: Modifier = Modifier,
    content: AnswersScreenState.Content,
    onClickBack: () -> Unit
) = Box(
    modifier = modifier
) {
    Image(
        painter = painterResource(
            when (content.round) {
                //TODO get from content
                1 -> R.drawable.bg_quiz_first_round
                2 -> R.drawable.bg_quiz_second_round
                else -> R.drawable.bg_quiz_third_round
            }
        ),
        contentDescription = null,
        modifier = Modifier
            .fillMaxSize(),
        contentScale = ContentScale.Crop
    )

    BackRoundButton(
        modifier = Modifier.align(Alignment.TopStart),
        onClick = onClickBack
    )

    Column(
        modifier = Modifier
            .fillMaxSize()
            .statusBarsPadding()
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
                .padding(horizontal = 43.dp, vertical = 73.dp)
                .background(
                    color = WhiteFFFFFF.copy(alpha = 0.8f),
                    RoundedCornerShape(10.dp)
                )
                .clip(
                    shape = RoundedCornerShape(10.dp)
                )
                .padding(
                    all = 20.dp
                )
        ) {
            Text(
                text = stringResource(R.string.quiz_answers_screen_title),
                modifier = Modifier,
                style = MaterialTheme.typography.h5,
                color = Color(0xFF000B2D)
            )

            Spacer(modifier = Modifier.size(24.dp))

            LazyColumn {
                itemsIndexed(content.answers.toList()) { index, answer ->
                    Row(
                        modifier = Modifier.padding(vertical = 8.dp)
                    ) {
                        Text(
                            text = "${index + 1}. ",
                            modifier = Modifier
                                .widthIn(min = 24.dp),
                            style = MaterialTheme.typography.body2,
                            textAlign = TextAlign.End
                        )

                        Column() {
                            Text(
                                text = answer.first
                            )

                            Text(
                                text = answer.second,
                                style = MaterialTheme.typography.body2.copy(
                                    brush = RedFF512FPinkDD2476
                                )
                            )
                        }
                    }
                }
            }
        }
    }
}

@Preview(
    showBackground = true
)
@Composable
private fun AnswersScreenPreview() {
    FruktPartyTheme {
        AnswersScreen(
            modifier = Modifier,
            content = AnswersScreenState.Content(
                answers = mapOf(
                    "Вопрос А Вопрос А Вопрос А Вопрос Вопрос Вопрос Вопрос" to "Ответ",
                    "Вопрос B Вопрос B Вопрос B Вопрос" to "Нет",
                    "Вопрос C Вопрос C Вопрос C Вопрос" to "Да",
                    "Вопрос D Вопрос D Вопрос D Вопрос" to "Ответ Ответ Ответ Ответ Ответ Ответ Ответ",
                    "Вопрос E Вопрос E Вопрос E Вопрос" to "Ответ",
                    "Вопрос F Вопрос F Вопрос F Вопрос" to "Ответ",
                    "Вопрос G Вопрос G Вопрос G Вопрос" to "Ответ",
                    "Вопрос H Вопрос H Вопрос H Вопрос" to "Ответ",
                    "Вопрос Z Вопрос Z Вопрос Z Вопрос" to "Ответ",
                    "Вопрос A Вопрос C Вопрос C Вопрос" to "Ответ",
                    "Вопрос B Вопрос C Вопрос C Вопрос" to "Ответ",
                    "Вопрос C Вопрос C Вопрос C Вопрос" to "Ответ",
                ),
                round = 1
            ),
            onClickBack = {}
        )
    }
}