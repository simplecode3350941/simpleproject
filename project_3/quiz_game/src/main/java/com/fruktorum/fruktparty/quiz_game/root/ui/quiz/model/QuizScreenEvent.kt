package com.fruktorum.fruktparty.quiz_game.root.ui.quiz.model

import com.fruktorum.domain.game.quiz.model.Answer

internal sealed class QuizScreenEvent {

    data class Start(
        val id: String,
        val round: Int,
        val startUserScore: Int
    ) : QuizScreenEvent()

    data class ClickAnswer(
        val value: Answer
    ) : QuizScreenEvent()

    data class ClickRate(
        val answer: Answer,
        val rate: Int
    ) : QuizScreenEvent()

    object ClickNext : QuizScreenEvent()

    data class ClickOnTryAgainSendResult(
        val id: String,
        val result: Int,
        val correctCount: Int,
        val totalCount: Int,
        val round: Int,
        val totalRounds: Int
    ) : QuizScreenEvent()
}