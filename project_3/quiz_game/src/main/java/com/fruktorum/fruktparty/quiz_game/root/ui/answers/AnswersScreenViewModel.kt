package com.fruktorum.fruktparty.quiz_game.root.ui.answers

import com.fruktorum.core.ui.vm.MVIViewModel
import com.fruktorum.domain.game.quiz.usecase.GetQuizUseCase
import com.fruktorum.fruktparty.quiz_game.root.ui.answers.model.AnswersScreenEvent
import com.fruktorum.fruktparty.quiz_game.root.ui.answers.model.AnswersScreenState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
internal class AnswersScreenViewModel
@Inject
constructor(
    private val getQuizContent: GetQuizUseCase
) : MVIViewModel<
        AnswersScreenState,
        Nothing,
        AnswersScreenEvent,
        Nothing
        >() {

    override fun onEvent(event: AnswersScreenEvent) {
        when (event) {
            is AnswersScreenEvent.Start -> handleStart(event.id, event.round)
        }
    }

    private fun handleStart(id: String, round: Int) = launchJob(
        exceptionBlock = {
            setState(
                AnswersScreenState.Error
            )
        }
    ) {
        val quiz = getQuizContent(
            id = id,
            forceLoad = false
        )

        quiz?.let {
            val questions = quiz.rounds.find {
                it.number == round
            }?.questions

            questions?.let {
                setState(
                    AnswersScreenState.Content(
                        answers = it.associate { question ->
                            question.title to question.answers.find { answer ->
                                answer.isCorrect
                            }?.text.orEmpty()
                        },
                        round = round
                    )
                )
            } ?: throw IllegalStateException()
        } ?: throw IllegalStateException()
    }
}