package com.fruktorum.fruktparty.quiz_game.root.ui.elements

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFFFFFF
import com.fruktorum.fruktparty.quiz_game.R

@OptIn(ExperimentalTextApi::class)
@Composable
internal fun ScoresCircle(
    value: Int,
    modifier: Modifier = Modifier
) = Column(
    modifier = modifier
        .background(
            color = WhiteFFFFFF.copy(alpha = 0.8f),
            CircleShape
        )
        .clip(
            shape = CircleShape
        )
        .aspectRatio(1f),
    horizontalAlignment = Alignment.CenterHorizontally
) {
    Text(
        text = value.toString(),
        modifier = Modifier.padding(top = 7.dp),
        style = MaterialTheme.typography.h1.copy(
            brush = RedFF512FPinkDD2476
        ),
        color = Color.Black
    )

    Text(
        text = stringResource(R.string.quiz_result_screen_scores_text),
        modifier = Modifier.offset(y = (-12).dp),
        style = MaterialTheme.typography.body2,
        color = Color(0xFF9AA1B6)
    )
}

@Preview
@Composable
private fun ScoresCirclePreview() {
    FruktPartyTheme {
        Column(Modifier.fillMaxWidth()) {
            ScoresCircle(
                value = 20,
                modifier = Modifier.fillMaxWidth(0.4f)
            )
        }
    }
}