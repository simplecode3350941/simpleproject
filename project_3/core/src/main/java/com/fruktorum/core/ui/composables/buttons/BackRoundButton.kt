package com.fruktorum.core.ui.composables.buttons

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.material.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.fruktorum.core.R
import com.fruktorum.core.ui.theme.WhiteFFFFFF

@Composable
fun BackRoundButton(
    modifier: Modifier = Modifier,
    tint: Color = WhiteFFFFFF.copy(alpha = 0.8f),
    onClick: () -> Unit
) = IconButton(
    onClick = onClick,
    modifier = modifier
        .statusBarsPadding()
        .padding(10.dp)
) {
    Icon(
        painter = painterResource(R.drawable.ic_back_round_20),
        contentDescription = stringResource(id = R.string.back_button_description),
        tint = tint
    )
}