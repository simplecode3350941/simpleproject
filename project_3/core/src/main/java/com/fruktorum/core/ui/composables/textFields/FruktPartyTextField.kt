package com.fruktorum.core.ui.composables.textFields

import androidx.compose.foundation.interaction.Interaction
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.selection.LocalTextSelectionColors
import androidx.compose.foundation.text.selection.TextSelectionColors
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.TextFieldDefaults.indicatorLine
import androidx.compose.material.Typography
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import com.fruktorum.core.R
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.Gray9AA1B6
import com.fruktorum.core.ui.theme.GrayD8E1FF

/**
 * Основное поле ввода текста, на основе BasicTextField и TextField.
 *
 * @param value the input [String] text to be shown in the text field
 * @param onValueChange the callback that is triggered when the input service updates the text. An
 * updated text comes as a parameter of the callback
 * @param modifier optional [Modifier] for this text field.
 * @param enabled controls the enabled state of the [BasicTextField]. When `false`, the text
 * field will be neither editable nor focusable, the input of the text field will not be selectable
 * @param readOnly controls the editable state of the [BasicTextField]. When `true`, the text
 * field can not be modified, however, a user can focus it and copy text from it. Read-only text
 * fields are usually used to display pre-filled forms that user can not edit
 * @param textStyle Style configuration that applies at character level such as color, font etc.
 * @param label the optional label to be displayed inside the text field container. The default
 * text style for internal [Text] is [Typography.caption] when the text field is in focus and
 * [Typography.subtitle1] when the text field is not in focus
 * @param placeholder the optional placeholder to be displayed when the text field is in focus and
 * the input text is empty. The default text style for internal [Text] is [Typography.subtitle1]
 * @param leadingIcon the optional leading icon to be displayed at the beginning of the text field
 * container
 * @param trailingIcon the optional trailing icon to be displayed at the end of the text field
 * container
 * @param isError indicates if the text field's current value is in error. If set to true, the
 * label, bottom indicator and trailing icon by default will be displayed in error color
 * @param keyboardOptions software keyboard options that contains configuration such as
 * [KeyboardType] and [ImeAction].
 * @param keyboardActions when the input service emits an IME action, the corresponding callback
 * is called. Note that this IME action may be different from what you specified in
 * [KeyboardOptions.imeAction].
 * @param singleLine when set to true, this text field becomes a single horizontally scrolling
 * text field instead of wrapping onto multiple lines. The keyboard will be informed to not show
 * the return key as the [ImeAction]. Note that [maxLines] parameter will be ignored as the
 * maxLines attribute will be automatically set to 1.
 * @param maxLines the maximum height in terms of maximum number of visible lines. Should be
 * equal or greater than 1. Note that this parameter will be ignored and instead maxLines will be
 * set to 1 if [singleLine] is set to true.
 * @param visualTransformation The visual transformation filter for changing the visual
 * representation of the input. By default no visual transformation is applied.
 * @param interactionSource the [MutableInteractionSource] representing the stream of
 * [Interaction]s for this TextField. You can create and pass in your own remembered
 * [MutableInteractionSource] if you want to observe [Interaction]s and customize the
 * appearance / behavior of this TextField in different [Interaction]s.
 * @param cursorBrush [Brush] to paint cursor with. If [SolidColor] with [Color.Unspecified]
 * provided, there will be no cursor drawn
 * @param decorationBox Composable lambda that allows to add decorations around text field, such
 * as icon, placeholder, helper messages or similar, and automatically increase the hit target area
 * of the text field. To allow you to control the placement of the inner text field relative to your
 * decorations, the text field implementation will pass in a framework-controlled composable
 * parameter "innerTextField" to the decorationBox lambda you provide. You must call
 * innerTextField exactly once.
 */
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun FruktPartyTextField(
    value: String,
    onValueChange: (String) -> Unit = {},
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    textStyle: TextStyle = MaterialTheme.typography.body1,
    label: String? = null,
    placeholder: String? = null,
    leadingIcon: @Composable (() -> Unit)? = null,
    trailingIcon: @Composable (() -> Unit)? = null,
    isError: Boolean = false,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions(),
    singleLine: Boolean = false,
    maxLines: Int = Int.MAX_VALUE,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    cursorBrush: Brush = SolidColor(
        // TODO add error color
        if (isError) Black000B2D else Black000B2D
    )
//    shape: Shape =
//        MaterialTheme.shapes.small.copy(bottomEnd = ZeroCornerSize, bottomStart = ZeroCornerSize),
//    decorationBox: @Composable (innerTextField: @Composable () -> Unit) -> Unit =
//        @Composable { innerTextField -> innerTextField() }
) {
    // TODO if(enabled) Black000B2D else ...
    val textColor = Black000B2D

    val mergedTextStyle = textStyle.merge(TextStyle(color = textColor))

    val customTextSelectionColors = TextSelectionColors(
        handleColor = Black000B2D,
        backgroundColor = GrayD8E1FF.copy(alpha = 0.4f)
    )

    CompositionLocalProvider(LocalTextSelectionColors provides customTextSelectionColors) {
        BasicTextField(
            value = value,
            onValueChange = onValueChange,
            modifier = modifier
//            .background(colors.backgroundColor(enabled).value, shape)
                .indicatorLine(
                    enabled = enabled,
                    isError = isError,
                    interactionSource = interactionSource,
                    colors = TextFieldDefaults.textFieldColors(
                        cursorColor = Black000B2D,
                        backgroundColor = Color.Transparent,
                        unfocusedIndicatorColor = GrayD8E1FF,
                        focusedIndicatorColor = GrayD8E1FF
                    ),
                    focusedIndicatorLineThickness = dimensionResource(id = R.dimen.size_2_dp),
                    unfocusedIndicatorLineThickness = dimensionResource(id = R.dimen.size_2_dp)
                ),
            enabled = enabled,
            readOnly = readOnly,
            textStyle = mergedTextStyle,
            keyboardOptions = keyboardOptions,
            keyboardActions = keyboardActions,
            singleLine = singleLine,
            maxLines = maxLines,
            visualTransformation = visualTransformation,
            interactionSource = interactionSource,
            cursorBrush = cursorBrush
        ) { innerTextField ->
            TextFieldDefaults.TextFieldDecorationBox(
                value = value,
                innerTextField = innerTextField,
                enabled = enabled,
                singleLine = singleLine,
                visualTransformation = visualTransformation,
                interactionSource = interactionSource,
                isError = isError,
                label = label?.let {
                    {
                        Text(
                            text = it,
                            color = Gray9AA1B6,
                            style = MaterialTheme.typography.subtitle1
                        )
                    }
                },
                placeholder = placeholder?.let {
                    {
                        Text(
                            text = it,
                            color = Gray9AA1B6,
                            style = MaterialTheme.typography.body1
                        )
                    }
                },
                leadingIcon = leadingIcon,
                trailingIcon = trailingIcon,
                colors = TextFieldDefaults.textFieldColors(
                    textColor = Black000B2D,
                    backgroundColor = Color.Transparent,
                    cursorColor = Black000B2D,
                    unfocusedIndicatorColor = GrayD8E1FF,
                    focusedIndicatorColor = GrayD8E1FF
                ),
                contentPadding = if (label != null) {
                    PaddingValues(
                        top = dimensionResource(R.dimen.size_20_dp),
                        bottom = dimensionResource(R.dimen.size_4_dp)
                    )
                } else {
                    PaddingValues(
                        bottom = dimensionResource(R.dimen.size_4_dp)
                    )
                }
            )
        }
    }
}

@Preview(
    showBackground = true
)
@Composable
private fun FruktPartyTextFieldPreview() {
    FruktPartyTheme {
        Column(
            modifier = Modifier
                .padding(dimensionResource(R.dimen.size_16_dp))
        ) {
            FruktPartyTextField(
                value = ""
            )
            Spacer(modifier = Modifier.size(dimensionResource(R.dimen.size_16_dp)))

            FruktPartyTextField(
                value = "value"
            )
            Spacer(modifier = Modifier.size(dimensionResource(R.dimen.size_16_dp)))

            FruktPartyTextField(
                value = "",
                label = "Label"
            )
            Spacer(modifier = Modifier.size(dimensionResource(R.dimen.size_16_dp)))

            FruktPartyTextField(
                value = "value",
                label = "Label"
            )
            Spacer(modifier = Modifier.size(dimensionResource(R.dimen.size_16_dp)))

            FruktPartyTextField(
                value = "",
                placeholder = "Placeholder"
            )
            Spacer(modifier = Modifier.size(dimensionResource(R.dimen.size_16_dp)))

            FruktPartyTextField(
                value = "value",
                placeholder = "Placeholder"
            )
            Spacer(modifier = Modifier.size(dimensionResource(R.dimen.size_16_dp)))

            FruktPartyTextField(
                value = "",
                label = "Label",
                placeholder = "Placeholder"
            )
            Spacer(modifier = Modifier.size(dimensionResource(R.dimen.size_16_dp)))

            FruktPartyTextField(
                value = "valuevaluevaluevaluevaluevalue",
                label = "Label",
                placeholder = "Placeholder"
            )
            Spacer(modifier = Modifier.size(dimensionResource(R.dimen.size_16_dp)))
        }
    }
}
