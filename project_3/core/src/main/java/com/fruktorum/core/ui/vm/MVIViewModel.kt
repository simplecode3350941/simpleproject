package com.fruktorum.core.ui.vm

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fruktorum.core.coroutines.BaseJobContainer
import com.fruktorum.core.utils.MediatorSingleLiveEvent
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow

abstract class MVIViewModel<S, DS, E, A> : ViewModel(), BaseJobContainer {

    override var coroutineScope = viewModelScope
    override val error = MediatorSingleLiveEvent<Throwable?>()
    override val loading = MutableLiveData<Boolean>()
    override val alert = MutableLiveData<String>()

    val state: SharedFlow<S> by lazy { stateFlow }

    val dialogState: SharedFlow<DS> by lazy { dialogStateFlow }

    val action: SharedFlow<A> by lazy { actionFlow }

    private val stateFlow =
        MutableSharedFlow<S>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_OLDEST
        )

    private val dialogStateFlow =
        MutableSharedFlow<DS>(
            replay = 1,
            onBufferOverflow = BufferOverflow.DROP_OLDEST
        )

    private val actionFlow = MutableSharedFlow<A>(
        replay = 0,
        onBufferOverflow = BufferOverflow.DROP_OLDEST,
        extraBufferCapacity = 1
    )

    abstract fun onEvent(event: E)

    protected fun setState(state: S) = stateFlow.tryEmit(state)

    protected fun getStateValueOrNull() = state.replayCache.firstOrNull()

    protected fun setDialogState(dialogState: DS) = dialogStateFlow.tryEmit(dialogState)

    protected fun emitAction(action: A) = actionFlow.tryEmit(action)
}