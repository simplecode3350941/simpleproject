package com.fruktorum.core.ui.composables.textFields

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.TextFieldDefaults.indicatorLine
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.FocusRequester.Companion.createRefs
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.fruktorum.core.R
import com.fruktorum.core.ui.theme.Gray9AA1B6
import com.fruktorum.core.ui.theme.GrayD8E1FF

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun PasswordTextField(
    password: MutableState<String>,
    modifier: Modifier = Modifier,
    focusRequester: FocusRequester? = null
) {
    PasswordTextField(
        value = password.value,
        modifier = modifier,
        placeholder = stringResource(id = R.string.enter_your_password),
        focusRequester = focusRequester,
        keyboardActions = KeyboardActions(
            onDone = { focusRequester?.requestFocus() }
        ),
        onValueChange = { password.value = it }
    )
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun PasswordTextField(
    value: String,
    modifier: Modifier = Modifier,
    placeholder: String,
    focusRequester: FocusRequester? = null,
    keyboardActions: KeyboardActions = KeyboardActions(),
    keyboardOptions: KeyboardOptions = KeyboardOptions(
        keyboardType = KeyboardType.Password,
        imeAction = ImeAction.Done
    ),
    onValueChange: (String) -> Unit
) {
    val pwdTransformation: MutableState<VisualTransformation> =
        remember { mutableStateOf(PasswordVisualTransformation()) }
    val interactionSource = remember { MutableInteractionSource() }
    val imageRes =
        if (pwdTransformation.value is PasswordVisualTransformation) R.drawable.ic_eye_closed
        else R.drawable.ic_eye_opened

    ConstraintLayout(modifier = modifier) {
        val (text, show_pwd_icon) = createRefs()

        BasicTextField(
            value = value,
            onValueChange = onValueChange,
            visualTransformation = pwdTransformation.value,
            singleLine = true,
            keyboardOptions = keyboardOptions,
            keyboardActions = keyboardActions,
            textStyle = MaterialTheme.typography.body1,
            modifier = Modifier
                .constrainAs(text) {
                    width = androidx.constraintlayout.compose.Dimension.matchParent
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
                .indicatorLine(
                    enabled = true,
                    isError = false,
                    focusedIndicatorLineThickness = dimensionResource(id = R.dimen.size_2_dp),
                    unfocusedIndicatorLineThickness = dimensionResource(id = R.dimen.size_2_dp),
                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = Color.Transparent,
                        unfocusedIndicatorColor = GrayD8E1FF,
                        focusedIndicatorColor = GrayD8E1FF
                    ),
                    interactionSource = MutableInteractionSource()
                )
                .focusRequester(focusRequester ?: FocusRequester())
        ) { innerTextField ->
            TextFieldDefaults.TextFieldDecorationBox(
                value = value,
                innerTextField = innerTextField,
                visualTransformation = pwdTransformation.value,
                singleLine = true,
                enabled = true,
                interactionSource = interactionSource,
                contentPadding = PaddingValues(
                    bottom = dimensionResource(R.dimen.size_4_dp)
                ),
                placeholder = {
                    Text(
                        text = placeholder,
                        color = Gray9AA1B6,
                        style = MaterialTheme.typography.body1
                    )
                },
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent,
                    unfocusedIndicatorColor = GrayD8E1FF,
                    focusedIndicatorColor = GrayD8E1FF
                )
            )
        }

        Image(
            imageVector = ImageVector.vectorResource(id = imageRes),
            contentDescription = null,
            modifier = Modifier
                .constrainAs(show_pwd_icon) {
                    bottom.linkTo(text.bottom)
                    end.linkTo(text.end)
                }
                .clickable {
                    pwdTransformation.value =
                        if (pwdTransformation.value is PasswordVisualTransformation) VisualTransformation.None
                        else PasswordVisualTransformation()
                }
        )
    }
}
