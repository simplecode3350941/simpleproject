package com.fruktorum.core.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import com.fruktorum.core.R

// Set of Material typography styles to start with
val Typography = Typography(
    /** style H1 from design */
    h1 = TextStyle(
        fontFamily = FontFamily(Font(R.font.alice)),
        fontWeight = FontWeight.Normal,
        fontStyle = FontStyle.Normal,
        fontSize = 56.sp,
        lineHeight = 64.sp
    ),
    /** style H2 from design */
    h2 = TextStyle(
        fontFamily = FontFamily(Font(R.font.alice)),
        fontWeight = FontWeight.Normal,
        fontStyle = FontStyle.Normal,
        fontSize = 38.sp,
        lineHeight = 42.sp
    ),
    /** style H3 from design */
    h3 = TextStyle(
        fontFamily = FontFamily(Font(R.font.alice)),
        fontWeight = FontWeight.Normal,
        fontStyle = FontStyle.Normal,
        fontSize = 26.sp,
        lineHeight = 28.sp
    ),
    /** style BODY 1 from design */
    body1 = TextStyle(
        fontFamily = FontFamily(Font(R.font.inter_medium)),
        fontWeight = FontWeight.Normal,
        fontStyle = FontStyle.Normal,
        fontSize = 16.sp,
        lineHeight = 22.sp
    ),
    /** style BODY 2 from design */
    body2 = TextStyle(
        fontFamily = FontFamily(Font(R.font.inter_medium)),
        fontWeight = FontWeight.Normal,
        fontStyle = FontStyle.Normal,
        fontSize = 14.sp,
        lineHeight = 20.sp,
        // TODO .em crash
        letterSpacing = 0.02.sp
    ),
    /** style BODY 3 from design */
    subtitle1 = TextStyle(
        fontFamily = FontFamily(Font(R.font.inter_medium)),
        fontWeight = FontWeight.Medium,
        fontStyle = FontStyle.Normal,
        fontSize = 12.sp,
        lineHeight = 16.sp,
        // TODO .em crash
        letterSpacing = 0.02.sp
    ),
    /** style BODY 4 from design */
    subtitle2 = TextStyle(
        fontFamily = FontFamily(Font(R.font.inter_medium)),
        fontWeight = FontWeight.Medium,
        fontStyle = FontStyle.Normal,
        fontSize = 10.sp,
        lineHeight = 14.sp,
        // TODO .em crash
        letterSpacing = 0.02.sp
    ),
    /** style BUTTON from design */
    button = TextStyle(
        fontFamily = FontFamily(Font(R.font.inter_medium)),
        fontWeight = FontWeight.Medium,
        fontStyle = FontStyle.Normal,
        fontSize = 16.sp,
        lineHeight = 22.sp
    )
)
val AliceFont =  FontFamily(Font(R.font.alice))
val InterNormalFont = FontFamily(Font(R.font.inter_medium))
