package com.fruktorum.core.sharedPreference

import com.chibatching.kotpref.KotprefModel

object SessionModel : KotprefModel() {
    var sessionToken by stringPref(default = "")
}