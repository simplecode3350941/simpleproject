package com.fruktorum.core.ui.composables.grid

import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.fruktorum.core.ext.toInt
import kotlin.math.ceil

/**
 * Шахматная вертикальная сетка
 *
 * У LazyVerticalStaggeredGrid из сompose было замечено, что элементы при скролле списка скачут.
 * Проверить в стабильной версии Compose (1.3.0)
 * @see [androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid]
 */
@Composable
fun StaggeredVerticalGrid(
    modifier: Modifier = Modifier,
    maxColumnWidth: Dp = (LocalConfiguration.current.screenWidthDp / 2).dp,
    clipToPadding: Dp = 0.dp,
    columnsHeightDiff: MutableState<Int>? = null,
    lastFixedItemIndex: Int? = null,
    content: @Composable () -> Unit
) {
    Layout(
        content = content,
        modifier = modifier
    ) { measurables, constraints ->
        check(constraints.hasBoundedWidth) {
            "Unbounded width not supported"
        }
        val columns = ceil(constraints.maxWidth / maxColumnWidth.toPx()).toInt()
        val columnWidth = constraints.maxWidth / columns
        val itemConstraints = constraints.copy(maxWidth = columnWidth)
        var itemFixed = 0
        val colHeights = IntArray(columns) { 0 } // track each column's height
        val placeables = measurables.map { measurable ->
            columnsHeightDiff?.let {
                it.value = columnHeightDiff(colHeights)
            }
            val column = if (itemFixed == lastFixedItemIndex && lastFixedItemIndex % 2 == 0) 0
            else shortestColumn(colHeights)
            val placeable = measurable.measure(itemConstraints)
            colHeights[column] += placeable.height
            itemFixed++
            placeable
        }
        for (i in 0..colHeights.lastIndex) {
            colHeights[i] += clipToPadding.toInt
        }

        val height = colHeights.maxOrNull()?.coerceIn(constraints.minHeight, constraints.maxHeight)
            ?: constraints.minHeight
        layout(
            width = constraints.maxWidth,
            height = height
        ) {
            var fixedItem = 0
            val colY = IntArray(columns) { 0 }
            placeables.forEach { placeable ->
                val column = if (fixedItem == lastFixedItemIndex && lastFixedItemIndex % 2 == 0) 0
                else shortestColumn(colY)
                fixedItem++
                placeable.place(
                    x = columnWidth * column,
                    y = colY[column]
                )
                colY[column] += placeable.height
            }
        }
    }
}

private fun shortestColumn(colHeights: IntArray): Int {
    var minHeight = Int.MAX_VALUE
    var column = 0
    colHeights.forEachIndexed { index, height ->
        if (height < minHeight) {
            minHeight = height
            column = index
        }
    }
    return column
}

private fun columnHeightDiff(colHeights: IntArray): Int {
    var minHeight = Int.MAX_VALUE
    var maxHeight = Int.MIN_VALUE

    colHeights.forEach { height ->
        if (height < minHeight) minHeight = height
        if (height > maxHeight) maxHeight = height
    }

    return maxHeight - minHeight
}
