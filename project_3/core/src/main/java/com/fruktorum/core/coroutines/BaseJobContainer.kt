package com.fruktorum.core.coroutines

import android.util.Log
import androidx.annotation.AnyThread
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlin.coroutines.AbstractCoroutineContextElement
import kotlin.coroutines.CoroutineContext

interface BaseJobContainer {

    var coroutineScope: CoroutineScope
    val error: MutableLiveData<Throwable?>
    val loading: MutableLiveData<Boolean>
    val alert: MutableLiveData<String>

    fun launchLoadingErrorJob(
        onError: ((Throwable) -> Unit)? = null,
        jobToDo: suspend () -> Unit
    ): Job {
        return coroutineScope.launch(BaseCoroutineExceptionHandler(error, true, onError)) {
            try {
                postLoading(true)
                jobToDo.invoke()
            } finally {
                postLoading(false)
            }
        }
    }

    fun launchErrorJob(
        onError: ((Throwable) -> Unit)? = null,
        jobToDo: suspend () -> Unit
    ): Job {
        return coroutineScope.launch(BaseCoroutineExceptionHandler(error, true, onError)) {
            postError(null)
            jobToDo.invoke()
        }
    }

    fun launchLoadingJob(
        onError: ((Throwable) -> Unit)? = null,
        start: CoroutineStart = CoroutineStart.DEFAULT,
        jobToDo: suspend () -> Unit
    ): Job {
        return coroutineScope.launch(
            BaseCoroutineExceptionHandler(error, false, onError),
            start
        ) {
            try {
                postLoading(true)
                jobToDo()
            } finally {
                postLoading(false)
            }
        }
    }

    fun launchJob(
        exceptionBlock: ((Throwable) -> Unit)? = null,
        block: suspend () -> Unit
    ): Job {
        return coroutineScope.launch(
            BaseCoroutineExceptionHandler(error, false, exceptionBlock)
        ) {
            block.invoke()
        }
    }

    @Suppress("DeferredIsResult")
    fun <T> asyncCatching(
        onError: ((Throwable) -> Unit)? = ::postError,
        start: CoroutineStart = CoroutineStart.DEFAULT,
        block: suspend CoroutineScope.() -> T
    ): Deferred<T?> {
        return coroutineScope.async(start = start) {
            runCatching {
                block.invoke(this)
            }.onFailure { error ->
                onError?.invoke(error)
            }.getOrNull()
        }
    }

    @AnyThread
    fun postError(throwable: Throwable?) {
        error.postValue(throwable)
    }

    @AnyThread
    fun postLoading(isLoading: Boolean) {
        loading.postValue(isLoading)
    }

    @AnyThread
    fun postAlert(message: String) {
        alert.postValue(message)
    }

    fun createExceptionHandler(exceptionBlock: ((Throwable) -> Unit)? = null): CoroutineExceptionHandler {
        return BaseCoroutineExceptionHandler(
            errorData = error,
            postToView = true,
            callback = exceptionBlock
        )
    }

    private class BaseCoroutineExceptionHandler(
        private val errorData: MutableLiveData<Throwable?>,
        private val postToView: Boolean,
        private val callback: ((Throwable) -> Unit)?
    ) : AbstractCoroutineContextElement(CoroutineExceptionHandler), CoroutineExceptionHandler {

        override fun handleException(context: CoroutineContext, exception: Throwable) {
            exception.localizedMessage?.let { Log.d("TAG", it) }
            callback?.invoke(exception)
            if (postToView) {
                errorData.postValue(exception)
            }
        }
    }
}