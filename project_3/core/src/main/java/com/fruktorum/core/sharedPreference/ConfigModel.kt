package com.fruktorum.core.sharedPreference

import com.chibatching.kotpref.KotprefModel

object ConfigModel : KotprefModel() {
    var mode: Int by intPref(default = 0)
    var isTutorialViewed: Boolean by booleanPref(default = false)
}