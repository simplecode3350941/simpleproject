package com.fruktorum.core.ui.composables.snackbar

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.fruktorum.core.R
import com.fruktorum.core.ui.theme.Green2ACEC4Green2ABACE

@Composable
fun SnackbarErrorNetworkConnection() {
    Snackbar(
        title = stringResource(R.string.error_popup_default_title),
        text = stringResource(R.string.error_network_connection_popup_default_text),
        icon = R.drawable.ic_popup_error_hexagon,
        backgroundIcon = Green2ACEC4Green2ABACE
    )
}
