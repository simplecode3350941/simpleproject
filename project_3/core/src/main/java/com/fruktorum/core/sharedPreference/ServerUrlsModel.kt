package com.fruktorum.core.sharedPreference

import com.chibatching.kotpref.KotprefModel

object ServerUrlsModel : KotprefModel() {
    var authServerUrl by stringPref()
    var backendServerUrl by stringPref()
}