package com.fruktorum.core.ui.composables.dialog

import android.annotation.SuppressLint
import android.view.WindowManager
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.window.Dialog
import com.fruktorum.core.ext.getActivity

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun BaseDialog(
    onDismissRequest: () -> Unit,
    content: @Composable () -> Unit
) {
    Dialog(
        onDismissRequest = onDismissRequest
    ) {
        val context = LocalContext.current
        val activity = context.getActivity()
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)

        Box(
            contentAlignment = Alignment.Center
        ) {
            content()
        }
    }
}

@Composable
@Preview
private fun BaseDialogPreview() {
    BaseDialog(
        onDismissRequest = {}
    ) {
        Text(
            text = "Some",
            modifier = Modifier.background(White)
        )
    }
}