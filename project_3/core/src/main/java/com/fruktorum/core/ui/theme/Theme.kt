package com.fruktorum.core.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import com.fruktorum.core.ui.theme.BlueC1D0FF
import com.fruktorum.core.ui.theme.Gray9AA1B6
import com.fruktorum.core.ui.theme.WhiteEEF2FF

private val DarkColorPalette = darkColors(
    primary = WhiteEEF2FF,
    primaryVariant = BlueC1D0FF,
    secondary = Gray9AA1B6
)

private val LightColorPalette = lightColors(
    primary = WhiteEEF2FF,
    primaryVariant = BlueC1D0FF,
    secondary = Gray9AA1B6
)

@Composable
fun FruktPartyTheme(darkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}