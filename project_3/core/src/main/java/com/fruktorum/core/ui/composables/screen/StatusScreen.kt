package com.fruktorum.core.ui.composables.screen

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.fruktorum.core.R
import com.fruktorum.core.ui.composables.buttons.BackRoundButton
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.PurpleA226CEViolet6626CE
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.core.ui.theme.WhiteFFFFFF

@Composable
fun StatusScreen(
    modifier: Modifier = Modifier,
    mainContent: @Composable ColumnScope.() -> Unit,
    title: String,
    text: @Composable () -> Unit,
    buttonText: String,
    onButtonClick: () -> Unit,
    onClickBack: (() -> Unit)? = null
) = Box(
    modifier = modifier
) {
    Column(
        modifier = Modifier
            .background(
                brush = WhiteFDFDFFGrayEEF2FF
            )
            .fillMaxSize()
            .padding(dimensionResource(R.dimen.size_24_dp)),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceEvenly
    ) {
        mainContent()

        Text(
            text = title,
            style = MaterialTheme.typography.h1,
            color = Black000B2D,
            textAlign = TextAlign.Center
        )

        Spacer(modifier = Modifier.size(dimensionResource(R.dimen.size_8_dp)))

        text()

        Spacer(modifier = Modifier.size(dimensionResource(R.dimen.size_40_dp)))

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    horizontal = dimensionResource(R.dimen.size_24_dp)
                )
                .background(
                    brush = RedFF512FPinkDD2476,
                    shape = RoundedCornerShape(
                        topStart = dimensionResource(R.dimen.size_32_dp),
                        bottomEnd = dimensionResource(R.dimen.size_32_dp)
                    )
                )
                .clip(
                    shape = RoundedCornerShape(
                        topStart = dimensionResource(R.dimen.size_32_dp),
                        bottomEnd = dimensionResource(R.dimen.size_32_dp)
                    )
                )
                .clickable(onClick = onButtonClick),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = buttonText,
                modifier = Modifier
                    .padding(vertical = dimensionResource(R.dimen.size_15_dp)),
                color = WhiteFFFFFF,
                style = MaterialTheme.typography.button
            )
        }
    }

    onClickBack?.let {
        BackRoundButton(
            modifier = Modifier.align(Alignment.TopStart),
            tint = Black000B2D.copy(alpha = 0.8f),
            onClick = it
        )
    }
}

@Composable
fun StatusScreen(
    modifier: Modifier = Modifier,
    mainContent: @Composable ColumnScope.() -> Unit,
    title: String,
    text: String,
    buttonText: String,
    onButtonClick: () -> Unit,
    onClickBack: (() -> Unit)? = null
) = StatusScreen(
    modifier = modifier,
    mainContent = mainContent,
    title = title,
    text = {
        Text(
            text = text,
            style = MaterialTheme.typography.body1,
            color = Black000B2D,
            textAlign = TextAlign.Center
        )
    },
    buttonText = buttonText,
    onButtonClick = onButtonClick,
    onClickBack = onClickBack
)

@Composable
fun ColumnScope.StatusScreenContent(
    @DrawableRes
    imgRes: Int,
    backgroundShapeColor: Brush
) {
    Box(
        modifier = Modifier.weight(1f),
        contentAlignment = Alignment.Center
    ) {
        Box(
            modifier = Modifier
                .rotate(-15f)
                .fillMaxWidth()
                .aspectRatio(1.36f)
                .padding(
                    horizontal = dimensionResource(R.dimen.size_24_dp)
                )
                .padding(
                    bottom = dimensionResource(R.dimen.size_48_dp)
                )
                .background(
                    brush = backgroundShapeColor,
                    shape = RoundedCornerShape(
                        topStart = dimensionResource(R.dimen.size_100_dp),
                        bottomEnd = dimensionResource(R.dimen.size_100_dp)
                    )
                )
                .clip(
                    shape = RoundedCornerShape(
                        topStart = dimensionResource(R.dimen.size_100_dp),
                        bottomEnd = dimensionResource(R.dimen.size_100_dp)
                    )
                )
        )

        Image(
            painter = painterResource(imgRes),
            contentDescription = null
        )
    }
}

@Preview(
    showBackground = true
)
@Composable
private fun StatusScreenPreview() {
    FruktPartyTheme {
        StatusScreen(
            mainContent = {
                StatusScreenContent(
                    imgRes = R.drawable.img_error_screen_foreground,
                    backgroundShapeColor = PurpleA226CEViolet6626CE
                )
            },
            title = "Title",
            text = "some text",
            buttonText = "button",
            onButtonClick = {}
        )
    }
}