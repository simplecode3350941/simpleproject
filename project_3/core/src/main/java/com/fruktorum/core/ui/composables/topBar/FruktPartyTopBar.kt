package com.fruktorum.core.ui.composables.topBar

import android.inputmethodservice.Keyboard
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import com.fruktorum.core.R
import com.fruktorum.core.ui.composables.buttons.BackButton
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.FruktPartyTheme
import org.w3c.dom.Text

@Composable
fun FruktPartyTopBar(
    title: String? = null,
    modifier: Modifier = Modifier,
    onBackClick: () -> Unit
) = Row(
    modifier = modifier
        .statusBarsPadding()
        .fillMaxWidth()
        .heightIn(
            min = dimensionResource(R.dimen.size_44_dp)
        )
) {
    BackButton(
        modifier = Modifier
            .padding(
                horizontal = dimensionResource(R.dimen.size_16_dp)
            )
            .align(Alignment.CenterVertically)
    ) {
        onBackClick()
    }

    title?.let {
        Text(
            text = it,
            modifier = Modifier
                .weight(1f)
                .padding(top = dimensionResource(R.dimen.size_11_dp)),
            textAlign = TextAlign.Center,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            style = MaterialTheme.typography.body1,
            color = Black000B2D
        )
    } ?: Spacer(modifier = Modifier.weight(1f))

    Spacer(
        modifier = Modifier
            .width(dimensionResource(R.dimen.size_56_dp))
            .height(dimensionResource(R.dimen.size_44_dp))
    )
}

@Preview(
    showBackground = true
)
@Composable
private fun FruktPartyTopBarPreview() {
    FruktPartyTheme {
        Column {
            FruktPartyTopBar(
                title = "FruktPartyTopBarPreview",
                onBackClick = {}
            )

            FruktPartyTopBar(
                onBackClick = {}
            )
        }
    }
}
