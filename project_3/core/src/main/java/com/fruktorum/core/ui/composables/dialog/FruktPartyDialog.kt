package com.fruktorum.core.ui.composables.dialog

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.fruktorum.core.R
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.Green2ACEC4Green2ABACE
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476
import com.fruktorum.core.ui.theme.WhiteFDFDFFGrayEEF2FF
import com.fruktorum.core.ui.theme.WhiteFFFFFF

@Composable
fun FruktPartyDialog(
    @DrawableRes
    image: Int? = null,
    title: String,
    titleTextStyle: TextStyle = MaterialTheme.typography.h2,
    subtitle: String? = null,
    dismissButtonText: String? = null,
    actionButtonText: String,
    onClose: () -> Unit,
    onClickDismiss: () -> Unit = onClose,
    onClickAction: () -> Unit
) = FruktPartyDialog(
    topContent = image?.let {
        {
            Image(
                painter = painterResource(id = it),
                contentDescription = "",
                modifier = Modifier.padding(bottom = dimensionResource(id = R.dimen.size_16_dp))
            )
        }
    } ?: { },
    title = title,
    titleTextStyle = titleTextStyle,
    subtitle = subtitle,
    dismissButtonText = dismissButtonText,
    actionButtonText = actionButtonText,
    onClose = onClose,
    onClickDismiss = onClickDismiss,
    onClickAction = onClickAction
)

@Composable
fun FruktPartyDialog(
    topContent: @Composable () -> Unit,
    title: String,
    titleTextStyle: TextStyle = MaterialTheme.typography.h2,
    subtitle: String? = null,
    dismissButtonText: String? = null,
    actionButtonText: String,
    onClose: () -> Unit,
    onClickDismiss: () -> Unit = onClose,
    onClickAction: () -> Unit
) = BaseDialog(
    onDismissRequest = onClose
) {
    Column(
        horizontalAlignment = Alignment.End
    ) {
        Image(
            painter = painterResource(R.drawable.ic_close_dialog_24),
            contentDescription = null,
            modifier = Modifier
                .size(dimensionResource(id = R.dimen.size_24_dp))
                .clickable(onClick = onClose)
        )

        Spacer(modifier = Modifier.size(dimensionResource(id = R.dimen.size_16_dp)))

        Column(
            modifier = Modifier
                .background(
                    brush = WhiteFDFDFFGrayEEF2FF,
                    shape = RoundedCornerShape(dimensionResource(id = R.dimen.size_36_dp))
                )
                .clip(RoundedCornerShape(dimensionResource(id = R.dimen.size_36_dp)))
                .padding(
                    horizontal = dimensionResource(id = R.dimen.size_32_dp),
                    vertical = dimensionResource(id = R.dimen.size_40_dp)
                ),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            topContent()

            Text(
                text = title,
                style = titleTextStyle,
                color = Black000B2D,
                textAlign = TextAlign.Center
            )

            subtitle?.let {
                Spacer(modifier = Modifier.size(dimensionResource(id = R.dimen.size_16_dp)))

                Text(
                    text = it,
                    style = MaterialTheme.typography.body2,
                    color = Black000B2D,
                    textAlign = TextAlign.Center
                )
            }

            Spacer(modifier = Modifier.size(dimensionResource(id = R.dimen.size_32_dp)))

            dismissButtonText?.let {
                DialogButton(
                    modifier = Modifier
                        .padding(
                            horizontal = dimensionResource(R.dimen.size_20_dp)
                        ),
                    text = it,
                    color = Green2ACEC4Green2ABACE,
                    onClick = onClickDismiss
                )

                Spacer(modifier = Modifier.size(dimensionResource(id = R.dimen.size_16_dp)))
            }

            DialogButton(
                modifier = Modifier
                    .padding(
                        horizontal = dimensionResource(R.dimen.size_20_dp)
                    ),
                text = actionButtonText,
                color = RedFF512FPinkDD2476,
                onClick = onClickAction
            )
        }
    }
}

@Composable
private fun DialogButton(
    modifier: Modifier = Modifier,
    text: String,
    color: Brush,
    onClick: () -> Unit
) = Box(
    modifier = modifier
        .fillMaxWidth()
        .background(
            brush = color,
            RoundedCornerShape(
                topStart = dimensionResource(id = R.dimen.size_32_dp),
                bottomEnd = dimensionResource(id = R.dimen.size_32_dp)
            )
        )
        .clip(
            shape = RoundedCornerShape(
                topStart = dimensionResource(id = R.dimen.size_32_dp),
                bottomEnd = dimensionResource(id = R.dimen.size_32_dp)
            )
        )
        .clickable(onClick = onClick),
    contentAlignment = Alignment.Center
) {
    Text(
        modifier = Modifier
            .padding(
                horizontal = dimensionResource(id = R.dimen.size_16_dp),
                vertical = dimensionResource(id = R.dimen.size_15_dp)
            ),
        text = text,
        color = WhiteFFFFFF,
        style = MaterialTheme.typography.body1,
        textAlign = TextAlign.Center
    )
}

@Preview
@Composable
private fun FruktPartyDialogPreviewWithImage() {
    FruktPartyTheme {
        FruktPartyDialog(
            image = R.drawable.happy_new_year_event_image,
            title = "Title",
            titleTextStyle = MaterialTheme.typography.h3,
            subtitle = "Subtitle Subtitle Subtitle Subtitle Subtitle",
            dismissButtonText = "Cancel",
            actionButtonText = "Do it!",
            onClose = {},
            onClickDismiss = {},
            onClickAction = {}
        )
    }
}

@Preview
@Composable
private fun FruktPartyDialogPreview() {
    FruktPartyTheme {
        FruktPartyDialog(
            image = R.drawable.happy_new_year_event_image,
            title = "Title",
            subtitle = "Subtitle Subtitle Subtitle Subtitle Subtitle",
            dismissButtonText = "Cancel",
            actionButtonText = "Do it!",
            onClose = {},
            onClickDismiss = {},
            onClickAction = {}
        )
    }
}

@Preview
@Composable
private fun FruktPartyDialogBasePreview() {
    FruktPartyTheme {
        FruktPartyDialog(
            title = "Title",
            actionButtonText = "Do it! Do it! Do it! Do it! Do it! Do it! Do it!",
            onClose = {},
            onClickAction = {}
        )
    }
}
