package com.fruktorum.core.ui.composables.buttons

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import com.fruktorum.core.R
import com.fruktorum.core.ui.theme.RedFF512FPinkDD2476

@Composable
fun TopStartBottomEndRoundedButton(
    text: String,
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    @DrawableRes
    image: Int? = null,
    imageDescription: String? = null,
    backgroundColor: Color,
    textStyle: TextStyle = MaterialTheme.typography.body1,
    textColor: Color = Color.White
) = TopStartBottomEndRoundedButton(
    text = text,
    modifier = modifier,
    onClick = onClick,
    image = image,
    imageDescription = imageDescription,
    backgroundGradient = SolidColor(backgroundColor),
    textStyle = textStyle,
    textColor = textColor,
)

@Composable
fun TopStartBottomEndRoundedButton(
    text: String,
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    @DrawableRes
    image: Int? = null,
    imageDescription: String? = null,
    backgroundGradient: Brush = RedFF512FPinkDD2476,
    textStyle: TextStyle = MaterialTheme.typography.body1,
    textColor: Color = Color.White
) {
    Button(
        modifier = modifier.background(
            brush = backgroundGradient,
            shape = RoundedCornerShape(
                topStart = dimensionResource(id = R.dimen.size_24_dp),
                bottomEnd = dimensionResource(id = R.dimen.size_24_dp)
            )
        ),
        colors = ButtonDefaults.buttonColors(backgroundColor = Color.Transparent),
        elevation = ButtonDefaults.elevation(
            defaultElevation = dimensionResource(id = R.dimen.size_0_dp)
        ),
        contentPadding = PaddingValues(
            horizontal = dimensionResource(id = R.dimen.size_15_dp)
        ),
        onClick = onClick
    ) {
        image?.let {
            Image(
                imageVector = ImageVector.vectorResource(id = it),
                contentDescription = imageDescription,
                modifier = Modifier.padding(
                    end = dimensionResource(id = R.dimen.size_8_dp)
                )
            )
        }
        Text(
            text = text,
            color = textColor,
            style = textStyle,
            textAlign = TextAlign.Center,
            maxLines = 1,
            modifier = Modifier
                .padding(
                    top = dimensionResource(id = R.dimen.size_11_dp),
                    bottom = dimensionResource(id = R.dimen.size_11_dp)
                ),
            overflow = TextOverflow.Ellipsis
        )
    }
}