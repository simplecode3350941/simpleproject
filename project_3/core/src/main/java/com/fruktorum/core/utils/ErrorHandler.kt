package com.fruktorum.core.utils

import android.content.res.Resources
import com.fruktorum.core.R
import javax.inject.Inject

class ErrorHandler @Inject constructor(
    private val error: Any?
) {
    val resources: Resources = Resources.getSystem()
    fun getErrorMessage(): String {
        var errorMessage = resources.getString(R.string.unexpected_server_error)
        when (error) {
            is String -> {
                errorMessage = error.toString()
                if (errorMessage == resources.getString(R.string.external_server_error_key)) {
                    errorMessage = resources.getString(R.string.external_server_error)
                }
            }
            is List<*> -> {
                try {
                    val errorList = error as List<List<String>>
                    val key = errorList[0][0]
                    val message = errorList[0][1]

                    errorMessage = when (key) {
                        resources.getString(R.string.email) -> {
                            when (message) {
                                resources.getString(R.string.required) ->
                                    resources.getString(R.string.email_required)
                                resources.getString(R.string.invalid) ->
                                    resources.getString(R.string.email_invalid)
                                resources.getString(R.string.exists) ->
                                    resources.getString(R.string.email_exists)
                                else -> resources.getString(R.string.server_error, key, message)
                            }
                        }
                        resources.getString(R.string.password) ->
                            when (message) {
                                resources.getString(R.string.required) ->
                                    resources.getString(R.string.password_required)
                                resources.getString(R.string.invalid) ->
                                    resources.getString(R.string.password_invalid)
                                resources.getString(R.string.too_short) ->
                                    resources.getString(R.string.password_too_short)
                                else -> resources.getString(R.string.server_error, key, message)
                            }
                        else -> resources.getString(R.string.server_error, key, message)
                    }
                } catch (e: Exception) {
                    errorMessage = e.localizedMessage?.toString()
                        ?: resources.getString(R.string.unexpected_server_error)
                }
            }
        }
        return errorMessage
    }
}