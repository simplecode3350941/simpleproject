package com.fruktorum.core.ui.composables.textFields

import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.TextFieldDefaults.indicatorLine
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.material.Text
import com.fruktorum.core.R
import com.fruktorum.core.ui.theme.Gray9AA1B6
import com.fruktorum.core.ui.theme.GrayD8E1FF

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun EmailTextField(
    emailInput: MutableState<String>,
    modifier: Modifier = Modifier,
    focusRequester: FocusRequester? = null
) {
    val interactionSource = remember { MutableInteractionSource() }

    BasicTextField(
        value = emailInput.value,
        onValueChange = { emailInput.value = it },
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Email,
            imeAction = ImeAction.Done
        ),
        keyboardActions = KeyboardActions(onDone = { focusRequester?.requestFocus() }),
        textStyle = MaterialTheme.typography.body1,
        modifier = modifier
            .indicatorLine(
                enabled = true,
                isError = false,
                focusedIndicatorLineThickness = dimensionResource(id = R.dimen.size_2_dp),
                unfocusedIndicatorLineThickness = dimensionResource(id = R.dimen.size_2_dp),
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent,
                    unfocusedIndicatorColor = GrayD8E1FF,
                    focusedIndicatorColor = GrayD8E1FF
                ),
                interactionSource = interactionSource
            )
    ) { innerTextField ->
        TextFieldDefaults.TextFieldDecorationBox(
            value = emailInput.value,
            visualTransformation = VisualTransformation.None,
            innerTextField = innerTextField,
            singleLine = true,
            enabled = true,
            interactionSource = interactionSource,
            contentPadding = PaddingValues(4.dp), // this is how you can remove the padding
            placeholder = {
                Text(
                    text = stringResource(id = R.string.enter_your_email_address),
                    color = Gray9AA1B6,
                    style = MaterialTheme.typography.body1
                )
            },
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Color.Transparent,
                unfocusedIndicatorColor = GrayD8E1FF,
                focusedIndicatorColor = GrayD8E1FF
            ),
        )
    }
}