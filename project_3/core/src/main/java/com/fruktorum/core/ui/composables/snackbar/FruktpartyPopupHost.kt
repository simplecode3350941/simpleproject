package com.fruktorum.core.ui.composables.snackbar

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.SnackbarHost
import androidx.compose.material.SnackbarHostState
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import com.fruktorum.core.R
import com.fruktorum.core.ui.theme.Black000B2D
import com.fruktorum.core.ui.theme.FruktPartyTheme
import com.fruktorum.core.ui.theme.Gray9AA1B6
import com.fruktorum.core.ui.theme.GrayD8E1FF
import com.fruktorum.core.ui.theme.Green2ACEC4Green2ABACE
import com.fruktorum.core.ui.theme.PurpleA226CEViolet6626CE
import com.fruktorum.core.ui.theme.YellowFFBE15OrangeFF9315

enum class PopupErrorType {
    Purple,
    Orange
}

interface PopupManager {

    suspend fun showInfo(text: String)

    suspend fun showError(
        text: String = "",
        type: PopupErrorType = PopupErrorType.Orange
    )
}

/**
 * Механизм для показа попапов (snackbar-ов) в верхней части экрана.
 *
 * Для использования необходимо добавить его в иерархию Composable экрана и в лямбде вызывать соответсвующие функции для отображения с помощью менеджера, передаваемого в scope.
 *
 * Вызовы методов отображения нужно делать в CoroutineScope.
 *
 * Пример:
 *
 * Screen {
 *
 *      ...
 *
 *      FruktpartyPopupHost { popupManager ->
 *
 *          LaunchedEffect(true) {
 *
 *              someSharedFlow.collect {
 *
 *                  when (it) {
 *
 *                      ... -> popupManager.showError()
 *
 *                  }
 *
 *              }
 *
 *          }
 *
 *      }
 *
 * }
 */
@Composable
fun FruktpartyPopupHost(
    scope: @Composable (PopupManager) -> Unit
) {
    val stateInfo = remember {
        SnackbarHostState()
    }

    val stateErrorPurple = remember {
        SnackbarHostState()
    }

    val stateErrorOrange = remember {
        SnackbarHostState()
    }

    scope(object : PopupManager {
        override suspend fun showInfo(text: String) {
            stateInfo.showSnackbar(
                message = text
            )
        }

        override suspend fun showError(
            text: String,
            type: PopupErrorType
        ) {
            when (type) {
                PopupErrorType.Purple -> {
                    stateErrorPurple.showSnackbar(
                        message = text
                    )
                }
                PopupErrorType.Orange -> {
                    stateErrorOrange.showSnackbar(
                        message = text
                    )
                }
            }
        }
    })

    SnackbarHost(
        hostState = stateInfo,
        snackbar = {
            Snackbar(
                title = stringResource(R.string.info_popup_default_title),
                text = it.message,
                icon = R.drawable.ic_popup_error_hexagon,
                backgroundIcon = Green2ACEC4Green2ABACE
            )
        }
    )

    SnackbarHost(
        hostState = stateErrorPurple,
        snackbar = {
            Snackbar(
                title = stringResource(R.string.error_popup_default_title),
                text = it.message.ifEmpty {
                    stringResource(R.string.error_popup_default_text)
                },
                icon = R.drawable.ic_popup_error_triangle,
                backgroundIcon = PurpleA226CEViolet6626CE
            )
        }
    )

    SnackbarHost(
        hostState = stateErrorOrange,
        snackbar = {
            Snackbar(
                title = stringResource(R.string.error_popup_default_title),
                text = it.message.ifEmpty {
                    stringResource(R.string.error_popup_default_text)
                },
                icon = R.drawable.ic_popup_error_rhombus,
                backgroundIcon = YellowFFBE15OrangeFF9315
            )
        }
    )
}

@Composable
fun Snackbar(
    title: String,
    text: String,
    @DrawableRes
    icon: Int,
    backgroundIcon: Brush
) = Row(
    modifier = Modifier
        .fillMaxWidth()
        .statusBarsPadding()
        .padding(
            top = dimensionResource(R.dimen.size_24_dp)
        )
        .padding(
            horizontal = dimensionResource(R.dimen.size_43_dp)
        )
        .background(
            color = GrayD8E1FF,
            shape = RoundedCornerShape(dimensionResource(R.dimen.size_16_dp))
        )
        .clip(
            shape = RoundedCornerShape(dimensionResource(R.dimen.size_16_dp))
        )
        .padding(dimensionResource(R.dimen.size_12_dp))
) {
    Image(
        painter = painterResource(icon),
        contentDescription = null,
        modifier = Modifier
            .size(dimensionResource(R.dimen.size_48_dp))
            .background(
                brush = backgroundIcon,
                shape = RoundedCornerShape(
                    dimensionResource(R.dimen.size_8_dp)
                )
            )
            .clip(
                RoundedCornerShape(
                    dimensionResource(R.dimen.size_8_dp)
                )
            )
            .padding(dimensionResource(R.dimen.size_12_dp))
    )

    Spacer(modifier = Modifier.size(dimensionResource(R.dimen.size_8_dp)))

    Column(
        modifier = Modifier,
        verticalArrangement = Arrangement.Center
    ) {
        Text(
            text = title,
            style = MaterialTheme.typography.body1.copy(
                color = Black000B2D
            )
        )

        Spacer(modifier = Modifier.size(dimensionResource(R.dimen.size_4_dp)))

        Text(
            text = text,
            style = MaterialTheme.typography.subtitle1.copy(
                color = Gray9AA1B6
            ),
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )
    }
}

@Preview(
    showBackground = true
)
@Composable
private fun SnackbarPreview() {
    FruktPartyTheme {
        Column(
            modifier = Modifier.padding(dimensionResource(R.dimen.size_16_dp))
        ) {
            Snackbar(
                title = "Info",
                text = "some info",
                icon = R.drawable.ic_popup_error_hexagon,
                backgroundIcon = Green2ACEC4Green2ABACE
            )

            Spacer(modifier = Modifier.size(dimensionResource(R.dimen.size_16_dp)))

            Snackbar(
                title = "Ops..",
                text = "Try later",
                icon = R.drawable.ic_popup_error_triangle,
                backgroundIcon = PurpleA226CEViolet6626CE
            )

            Spacer(modifier = Modifier.size(dimensionResource(R.dimen.size_16_dp)))

            Snackbar(
                title = "Ops..",
                text = "Try later",
                icon = R.drawable.ic_popup_error_rhombus,
                backgroundIcon = YellowFFBE15OrangeFF9315
            )
        }
    }
}
