package com.fruktorum.core.ui.composables.buttons

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.navigation.NavHostController
import com.fruktorum.core.R

@Composable
fun BackButton(navController: NavHostController, modifier: Modifier = Modifier) {
    Image(
        imageVector = ImageVector.vectorResource(id = R.drawable.ic_back),
        contentDescription = stringResource(id = R.string.back_button_description),
        modifier = modifier
            .clickable { navController.popBackStack() }
    )
}

@Composable
fun BackButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit
) {
    Image(
        imageVector = ImageVector.vectorResource(id = R.drawable.ic_back),
        contentDescription = stringResource(id = R.string.back_button_description),
        modifier = modifier
            .clickable(onClick = onClick)
    )
}
