package com.fruktorum.core.ext

import android.content.res.Resources
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

val Int.toDp get() = (this / Resources.getSystem().displayMetrics.density).dp
val Dp.toInt get() = (this.value * Resources.getSystem().displayMetrics.density).toInt()