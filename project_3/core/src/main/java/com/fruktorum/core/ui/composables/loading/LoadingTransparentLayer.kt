package com.fruktorum.core.ui.composables.loading

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import com.fruktorum.core.R
import com.fruktorum.core.ui.theme.RedFF512F

@Composable
fun LoadingTransparentLayer(screen: @Composable (() -> Unit)? = null) {
    screen?.invoke()

    Box(
        modifier = Modifier
            .background(
                color = Color.Transparent.copy(alpha = 0.8f)
            )
            .fillMaxSize()
            .clickable(enabled = false) {},
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator(
            modifier = Modifier
                .size(dimensionResource(R.dimen.size_40_dp)),
            color = RedFF512F,
            strokeWidth = dimensionResource(R.dimen.size_4_dp)
        )
    }
}